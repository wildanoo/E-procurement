<script>
	$(function() {

		$('#start_date, #end_date').datepicker({
	          changeMonth: true, changeYear: true,
	   		  dateFormat: "yy-mm-dd"
	     });

	    $('#ck_all').on('click', function(){
			$(this).closest('table').find(':checkbox').prop('checked', this.checked);
		});


	});

	function load_data()
	{
		$('#main_container').load(baseUrl+'redlist/browse');
	}

</script>
<div id="main_container">

<div id="debug"></div>
<div class="page-header"><h3>Vendor <?php echo $title; ?></h3></div>

<?php echo form_open('redlist/export_excel',array('id'=>'myform')); ?>


<?php echo $this->session->flashdata("message"); ?>
<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
        <tr>
            <th>No</th>
            <th>Vendor Num.</th>
            <th>Vendor Name</th>
            <th>Vendor Type</th>
            <th>Total Change Request</th>
            <th>#</th>
        </tr>
	</thead>
    <tbody>
<?php
// echo form_hidden('total',count($browse));
if($browse){ $i=1;
	foreach($browse as $row){ ?>
			<tr>
				<td style="text-align: center;"><?php  echo $num = $num+1; ?></td>
        <td><?php echo (!empty($row->vendor_num)) ? $row->vendor_num : '-';  ?></td>
				<td><?php echo $row->vendor_name ?></td>
				<td><?php echo $row->vendor_type ?></td>
				<td><span class="badge"><?= $this->m_change_request->get_total_changed($row->id) ?></span></td>
				<td><a class="btn btn-success" href="<?=base_url('change_request/detail').'/?id='.$row->id ?>"> <span class="glyphicon glyphicon-search" aria-hidden="true"></span> view detail</a>
</td>
			</tr>
<?php	$i++;  } ?>
<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php  } else { ?>
	<tr><td colspan="9" align="center">-</td></tr>
<?php } ?>
    </tbody>
</table>

<?php echo form_close();?>

</div>
