<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="FaberNainggolan">
    <title>eProcurement Garuda Indonesia</title>
    <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/ui/jquery-ui.js"></script> 
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script> 
    <script src="<?php echo base_url() ?>assets/js/bootstrap-dialog.min.js"></script>    
     
    <link href="<?php echo base_url()  ?>assets/css/bootstrap.min.css" rel="stylesheet"> 
    <link href="<?php echo base_url()  ?>assets/css/style.css" rel="stylesheet"> 
    <link href="<?php echo base_url()  ?>assets/css/jquery-ui.css" rel="stylesheet">
    <link href="<?php echo base_url()  ?>assets/css/bootstrap-dialog.min.css" rel="stylesheet">     
      
    <!--script src="<?php echo base_url() ?>assets/js/jquery-2.1.4.min.js"></script>   
    <script src="<?php echo base_url() ?>assets/js/jquery-1.10.2.js"></script-->	
  </head>
  <body>
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">eProcurement</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo base_url(); ?>"><i class="glyphicon glyphicon-home"></i> Home</a></li>
            <!--li><a href="#about"><i class="glyphicon glyphicon-info-sign"></i> About</a></li-->            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <i class="glyphicon glyphicon-info-sign"></i> CMS<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">                
                <li class="dropdown-header">Contents...</li> 
                <li class="divider"></li>
                <li><?php  echo anchor('cms/','Home'); ?></li>                
                <li><?php  echo anchor('cms/contact','Contact'); ?></li>
                <li><?php  echo anchor('#','FAQs'); ?></li>
              </ul>
            </li>                       
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <i class="glyphicon glyphicon-wrench"></i>Master Data <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">                
                <li class="dropdown-header">Category..</li> 
                <li class="divider"></li>
                <li><?php  echo anchor('category/supplier_category','Browse'); ?></li>
                <li><?php  echo anchor('category','Category'); ?></li>
                <li><?php  echo anchor('category/subcategory','Sub Category'); ?></li>
                <li class="divider"></li> 
                <li class="dropdown-header">Branch Offices..</li> 
                <li class="divider"></li> 
                <li><?php  echo anchor('offices','Browse'); ?></li> 
                <li><?php  echo anchor('region','Region'); ?></li> 
                <li><?php  echo anchor('country','Country'); ?></li> 
                <li><?php  echo anchor('department','Department'); ?></li> 
                <li class="dropdown-header">Others..</li> 
                <li class="divider"></li>                
                <li><?php  echo anchor('reg_status','Register Status'); ?></li> 
                <li><?php  echo anchor('level','Level'); ?></li> 
              </ul>
            </li>
            <li><a href="<?php echo base_url(); ?>announce"><i class="glyphicon glyphicon-th-list"></i> Announcement</a></li>
            <li><a href="<?php echo base_url(); ?>register"><i class="glyphicon glyphicon-info-sign"></i> Registration</a></li>  
            <!--li><a href="<?php echo base_url(); ?>member"><i class="glyphicon glyphicon-th-list"></i> Members</a></li>              
            <!--li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <i class="glyphicon glyphicon-th-list"></i> Vendor<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">             	
                <li><?php  echo anchor('#','Registration'); ?></li>
                <li><?php  echo anchor('#','Supplier'); ?></li>
              </ul>
            </li-->		
            			 
          </ul>
          <ul class="nav navbar-nav navbar-right">            
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <i class="glyphicon glyphicon-user"></i>&nbsp;user:
              	<?php echo ucfirst($username); ?><span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu" style="font-size: 13px;">
                <li><?php echo anchor('/auth/logout/', 'Logout'); ?></li>               
              </ul>
            </li>           
          </ul>
        </div>
      </div>
    </nav>
