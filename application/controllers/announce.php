<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Announce extends Controller {

	function __construct()
	{
		parent::__construct();		
		$this->load->helper('file');
		$this->load->model(array('m_announce','codec'));
		$this->permit = new stdClass();

		/* get function islogged_in() from helpers - to check if user already logged in */
		islogged_in();
		/* ===== */

		$this->permit = $this->crud->get_permissions('9');
		$this->getContentSubcontentBySess();

		define(EPROC_IMAGE_PATH, "./uploads/images/");
		define(EPROC_ATTACHMENT_PATH, "./uploads/attach/");
		define(EPROC_ATTACHMENT_TEMP_PATH, "./uploads/tmp_attach/");
		define(EPROC_RFP_PATH, "./uploads/rfp/");
		define(EPROC_CONTENT_DESC_PATH, "./uploads/contents/");
		define(EPROC_AANWIJZING_PATH, "./uploads/aanwijzing/");
	}


	////// * Main Methods * //////

	function index(){
	 		
	 	/* get data from defined function for table view */
		$extract = $this->getDataTables(10);
		/* ==== */

		/* preparing data for display */		
		$data['user_id']  = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();

		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];

		$data['category'] 	= $this->getCategories();
		$data['status']   	= $this->getStatusCondition();
		$data['content'] 	= $this->getAllContentTypes();
		$data['subcontent'] = $this->getAllSubcontentTypes();
		$data['controller'] = $this->getControllerBySubcontentId();
			
		$data['deptId']		= afterloginGetDeptId();

		if ($this->permit->cc){
			// retrieve allowed content for user
		 	$content_type = $this->getContentTypes($identifier);
		 	if(!$content_type) $content_type = array();
		 	$select = array(''=>'-- Select --');
		 	$data['content_type']    = $select + $content_type;

		 	// retrieve allowed subcontent for user
		 	$content_subtype = $this->getSubcontentTypes($identifier);
		 	if(!$content_subtype) $content_subtype = array();
		 	$select = array(''=>'-- Select --');
		 	$data['subcontent_type']    = $select + $content_subtype;
	 	}

		$data['view'] = "announce/browse";

		$this->load->view('layout/template',$data);
		/* ==== */

	}

	function search(){
	 		
	 	/* initiate search inputs */
	 	$get_date_ranges = $this->session->userdata('search_dateranges');
	 	$get_conditions  = $this->session->userdata('search_conditions');

 	 	if (isset($_POST['start_date']) && isset($_POST['end_date'])) {
 	 		if ($_POST['start_date'] != $get_date_ranges[1] || $_POST['end_date'] != $get_date_ranges[2]) {
 		 		$this->session->unset_userdata('search_dateranges');
 		 		$get_date_ranges[1] = $_POST['start_date'];
 		 		$get_date_ranges[2] = $_POST['end_date'];
 		 	}
 	 	}

 	 	if (isset($_POST['search_term'])) {
 	 		if ($_POST['search_term'] != $get_conditions['code']) {
 		 		$this->session->unset_userdata('search_conditions');
 		 		$this->session->unset_userdata('search_term');
 		 		$get_conditions['code'] 	  = $_POST['search_term'];
 		 		$get_conditions['title_ind'] = $_POST['search_term'];
 		 		$get_conditions['title_eng'] = $_POST['search_term'];
 		 	} 		 		
 	 	} 	 		

		$search_dateranges = array(
								'publish_date',
								!$get_date_ranges[1]?$_POST['start_date']:$get_date_ranges[1],
								!$get_date_ranges[2]?$_POST['end_date']:$get_date_ranges[2]);

		$search_conditions = array(
								'code'		=> !$get_conditions['code']?$_POST['search_term']:$get_conditions['code'],
								'title_ind' => !$get_conditions['title_ind']?$_POST['search_term']:$get_conditions['title_ind'],
								'title_eng' => !$get_conditions['title_eng']?$_POST['search_term']:$get_conditions['title_eng']
							  );

		$where_conditions  = $this->search_input($search_dateranges,$search_conditions);
		/* ==== */

		/* get data from defined function for table view */
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
		/* ==== */

		/* preparing data for display */
		$data['user_id']  = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();

		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];

		$data['category']   = $this->getCategories();
		$data['content'] 	= $this->getAllContentTypes();
		$data['subcontent'] = $this->getAllSubcontentTypes();
		$data['status']     = $this->getStatusCondition();
		$data['controller'] = $this->getControllerBySubcontentId();
			
		$data['deptId']		= afterloginGetDeptId();

		if ($this->permit->cc){
			// retrieve allowed content for user
		 	$content_type = $this->getContentTypes($identifier);
		 	if(!$content_type) $content_type = array();
		 	$select = array(''=>'-- Select --');
		 	$data['content_type']    = $select + $content_type;

		 	// retrieve allowed subcontent for user
		 	$content_subtype = $this->getSubcontentTypes($identifier);
		 	if(!$content_subtype) $content_subtype = array();
		 	$select = array(''=>'-- Select --');
		 	$data['subcontent_type']    = $select + $content_subtype;
	 	}

		$data['view'] = "announce/browse";

		$this->load->view('layout/template',$data);
		/* ==== */

	}
 
	function preview(){

	 	$param = $this->uri->segment(3);

		$data['username'] = $this->tank_auth->get_username();
		$data['view'] 	  = "announce/preview";

		$joins[0][0] = 'subcontent_type';
		$joins[0][1] = 'subcontent_type.id = announce.subcontent_type';
		$joins[0][2] = 'left';

		$where 		 = "announce.id = '".$param."'";
		$select 	 = "announce.id,remarks_reject,file,id_cat,id_subcat,code,content_type,subcontent_type,subcontent_type.subtype_name,title_eng,title_ind,detail_eng,detail_ind,first_valdate,end_valdate,publish_date,subcontent_type.behavior_id,status,announce.created_date,announce.created_id,id_departement";
		$browse_temp = $this->crud->browse_join("","announce","","","false",$select,$joins,$where);

		foreach ($browse_temp as $browse) {}

		if ($browse_temp) {

			$curdatetime = date('Y-m-d H:i:s');
			$startdate   = $browse->first_valdate;
			$endate      = $browse->end_valdate;
			$data['deptId'] = $deptId = afterloginGetDeptId();

			$data['browse'] = $browse;

			if($browse->file){

				$ctpath_eng  = "./uploads/contents/".$browse->file."_eng.txt";
				$ctpath_ind  = "./uploads/contents/".$browse->file."_ind.txt";
				$rfppath  	 = "./uploads/rfp/".$browse->file.".pdf";
				
		 		$data['contents_eng'] = file_exists($ctpath_eng) ? file_get_contents($ctpath_eng) : "-";
		 		$data['contents_ind'] = file_exists($ctpath_ind) ? file_get_contents($ctpath_ind) : "-";
		 		$data['rfpfile']	  = file_exists($rfppath) ? true : false;
		 		
		 		if ($browse->content_type != "1") {
		 			$data['attachment'] = $this->getAttachmentByFilename($browse->file);
		 		}else{
		 			$data['attachment'] = array(0 => $browse->file);
		 		}
		 			
		 	}

		 	$data['category'] 	 	 = globalGetCategory();
			$data['subcategory'] 	 = globalGetSubcategory();
			$data['subcontent']  	 = globalGetSubtype();
		 	$data['btn_permissions'] = $this->button_privileges_preview_menu($browse->status);

		 	if ($browse->content_type == '1') {
		 		if (date('Y-m-d H:i:s') >= $browse->publish_date) {
			 		$data['param_unpublish'] = true;
			 	}else{
			 		$data['param_unpublish'] = false;
			 	}

			 	if ($browse->subcontent_type == '2') {
			 		$data['view'] = "announce/previewonelanguage";
			 	}
		 	}else{
		 		if (date('Y-m-d H:i:s') >= $browse->publish_date && date('Y-m-d H:i:s') <= $browse->end_valdate) {
			 		$data['param_unpublish'] = true;
			 	}else{
			 		$data['param_unpublish'] = false;
			 	}

			 	if ($browse->subcontent_type == '5' OR $browse->subcontent_type == '6') {
			 		$data['view'] = "announce/previewonelanguage";
			 	}
		 	}

		 	if ($deptId->id_dept == $browse->id_departement && (date('Y-m-d H:i:s') <= $browse->publish_date OR $browse->status == '11')) { // temporary		 			
		 		$data['param_appject'] = true;
		 	}else{
		 		$data['param_appject'] = false;
		 	}

			$this->load->view('layout/template',$data);
		}else{
			echo "not-found";
		}
	}

	function portal_create(){
	 	$subcontent_id 	  = $_POST['id_subtype'];
	 	$controller_lists = $this->getControllerBySubcontentId();
	 	$controller 	  = array_key_exists($subcontent_id, $controller_lists)?$controller_lists[$subcontent_id]:false;

	 	return $controller!=false?redirect($controller.'/form_create'):'';
	}

	function preview_update(){
	 	$id 	   		= $_POST['id'];
	 	$subcontent_id 	= $_POST['subcontent_type'];
	 		
	 	$curr_date 		= date('Y-m-d H:i:s');
	 	$userID    		= $this->tank_auth->get_user_id();
	 	$remarks_reject = null;

	 	$reg_num = null;

	 	$browse_status = $this->crud->browse("","announce","id",$id,"false","status,code,file");

	 	if ($_POST['button-process'] == 'approve') {
	 		$status_input = '3';

	 		if ($browse_status->status != '2') {
	 			sendNotiv_Email_toVendor_rfp_aanwijding($browse_status->file, $id);
	 			$reg_num['runningcode'] = $browse_status->code;	 				
	 		}else{
	 			$reg_num = $this->codec->announce($subcontent_id);	 				
	 		}
	 		
	 		if ($this->isNeedVendor($subcontent_id)) {
	 			emailSendInfoToVendors($id,$subcontent_id);
	 			if (!in_array($subcontent_id,array(1,2))) {
	 				globalRedlistReducer($id);
	 			}
	 		}

	 		/* send email */
		 	emailSendContentApproved($id,$status_input);
			/* ---------------- */
	 	}elseif ($_POST['button-process'] == 'reject') {
	 		$status_input   = '6';
	 		$remarks_reject = $_POST['remarks_reject'];
	 		/* send email */
		 	emailSendContentApproved($id,$status_input,$remarks_reject);
			/* ---------------- */
	 	}elseif ($_POST['button-process'] == 'rejlang') {
	 		$status_input   = '10';
	 		$remarks_reject = $_POST['remarks_reject_lang'];
		 	Send_ContentUpdateToAsyst($id,'reupdate',$remarks_reject);
	 	}elseif ($_POST['button-process'] == 'unpublish') {
	 		$status_input = '99';
	 	}elseif ($_POST['button-process'] == 'edit') {
	 		$this->portal_edit($subcontent_id,$id);
	 	}elseif ($_POST['button-process'] == 'back') {
	 		redirect('announce/');
	 	}

	 	$dept = afterloginGetDeptId();
	 	$data = array('creator'		   => $_POST['creator'],
					  'status'		   => $status_input,
					  'created_id'	   => $userID,
					  'remarks_reject' => $remarks_reject,
					  'code'		   => $reg_num['runningcode'],
					  'counter' 	   => $reg_num['increment'],
					  'last_updated'   => $curr_date);

	 	$this->crud->update("","announce","id",$_POST['id'],$data);

	 	redirect('announce/');
	}
 
	function delete(){
	 	
	 	$id   = $this->uri->segment(3);
	 	$data = $this->crud->browse("","announce ","id",$id,"false","file,content_type,subcontent_type");

	 	$where = 'subcontent_type.id = '.$data->subcontent_type;
	 	$content_subtypes_get = $this->crud->browse("","subcontent_type","","","false","document_req_id",$where);
	 	foreach ($content_subtypes_get as $content_subtype_get) {}

	 	$fk = $content_subtype_get->document_req_id?$content_subtype_get->document_req_id:'" "'; 				
	 	$where = 'id IN ('.$fk.')';
		$content_subtypes = $this->crud->browse("","document_req","","","false","id",$where);

		$counter = count($content_subtypes);	

	 	$category = $this->crud->browse("","subcontent_type","id",$data->subcontent_type,"false","id,subtype_name,document_req_id,need_vendor,behavior_id");
	 	
	 	if (count($category)>0) {
	 		if ($category->need_vendor==1) {
				$this->crud->delete("","announce_join_participant","id_announce",$id);
			}

			if ($category->behavior_id==1) {
				$this->crud->delete("","announce_news_detail","id_announce",$id);
			}
	 	}		

	 	$this->crud->delete("","announce","id",$id);
	 	
	 	$ext = array('contents'=>'txt','attach'=>'pdf','document_requirements'=>'pdf','policy_agreement'=>'txt','aanwijzing'=>'txt','1'=>'jpg','2'=>'gif','3'=>'png');
	 	foreach($ext as $key=>$val){
	 		$fold = is_numeric($key) ? "images" :$key;
	 		
	 		if ($key =='document_requirements') {
	 			for ($i = 0;$i<$counter;$i++) {
					$path = "./uploads/$fold/".$data->file."_".($i+1).".$val";
	 				if (file_exists($path)) unlink($path);
	 			}
	 		}elseif ($val=='txt') {
	 			for ($i = 0;$i<2;$i++) {
	 				if ($i==0) {
	 					$path = "./uploads/$fold/".$data->file."_ind.$val";
	 				}else{
	 					$path = "./uploads/$fold/".$data->file."_eng.$val";
	 				}
	 				if (file_exists($path)) unlink($path);
	 			}
	 		}else{
	 			$path = "./uploads/$fold/$data->file.$val";
	 			if (file_exists($path)) unlink($path);
	 		}
	 	}

		$this->session->set_flashdata('message','1 data success deleted');
	 	redirect('announce/','refresh');
	 	
	}

	/////////////////////////////////


	////// * Required Methods * //////

	function open_file(){

		$param_offset = 2;
		$params 	  = array_slice($this->uri->rsegment_array(), $param_offset);

		$filename  = $params[0];			
		$explode   = explode('_', $filename);
		// $check 	   = $this->crud->get_total_record('','document_requirement','filename ="'.$explode[0].'"');
		// if ($check > 0) {
		// 	$path = EPROC_ATTACHMENT_PATH;
		// }else{
			if(file_exists(EPROC_ATTACHMENT_TEMP_PATH.$filename.'.'.$extension)){
				$path = EPROC_ATTACHMENT_TEMP_PATH;
			}else{
				$path = EPROC_ATTACHMENT_PATH;	
			}
		// }
		// $path 	   = $check>0 ? EPROC_ATTACHMENT_PATH : EPROC_ATTACHMENT_TEMP_PATH;
		$extension = 'pdf';
		$file 	   = $path.$filename.'.'.$extension;
		
		redirect(base_url($file));

	}

	function openFileRFP(){

		$param_offset = 2;
		$params 	  = array_slice($this->uri->rsegment_array(), $param_offset);

		$filename  = $params[0];
			
		$path 	   = EPROC_RFP_PATH;
		$extension = 'pdf';
		$file 	   = $path.$filename.'.'.$extension;
		
		redirect(base_url($file));
	}

	function get_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'code','sort'=>'ASC'); $select = "code as name";
		$result = $this->crud->autocomplete("","announce",$select,"code",$term,"",$order);
		echo json_encode($result);
	}
 
	function set_browse_session(){
		$code     = $_POST['search'];
		$announce = $this->crud->browse("","announce","code",$code,"false","id");
		if($announce) $this->session->set_flashdata('anID',$announce->id);
	}
 
	function set_sess_category(){
	 	
		$id_cat = $_POST['id_cat'];

		if($id_cat!=0) 	{ $this->session->set_userdata('sess_cat',$id_cat);
		} else { $this->session->unset_userdata('sess_cat'); }
	 	
	}

	public function getSubcontent($flag = '') {

		$type =  $_POST['data'];

		$allowed_key = implode(',', array_keys($this->session->userdata('subcontent_allowed')));
		$allowed_val = implode(',', $this->session->userdata('subcontent_allowed'));

		if ($this->session->userdata('subcontent_allowed')) {
			$where = 'content_id = '.$type.' AND id IN ('.$allowed_key.')';
		}else{
			$where = 'content_id = '.$type;
		}
		$details = $this->crud->browse("","subcontent_type","","","true","id, subtype_name, need_vendor",$where);
		$result  = $details;

		if ($flag != '') {
			$result  = array();
			if ($flag == 'bidding') {
				$result  = $this->crud->browse("","subcontent_type","","","true","id, subtype_name, need_vendor","id IN ('3','5','6') AND content_id = ".$type);
			}elseif ($flag == 'sourcing') {
				$result  = $this->crud->browse("","subcontent_type","","","true","id, subtype_name, need_vendor","id IN ('4') AND content_id = ".$type);
			}
		}
			
	    echo json_encode($result);
	}

	/////////////////////////////////


	////// * Utility Methods * //////

	private function button_privileges_preview_menu($status = null){

		$data['approve']   = $this->permit->arc   && ($status == '2' OR $status == '11')?true:false;
		$data['reject']    = $this->permit->arc   && ($status == '2' OR $status == '11')?true:false;
		$data['unpublish'] = $this->permit->upubc && $status == '3'?true:false;
		$data['edit'] 	   = $this->permit->uc ? true:false;
		///// added by dedi rudiyanto /////
		$data['rejlang']   = $this->permit->arl   && ($status == '2' OR $status == '11')?true:false;
		///////////////////////////////////

		return $data;

	}

	private function search_input($search_dateranges = array(),$search_conditions = array(),$search_key = array()){

		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$search_dateranges = $this->session->userdata('search_dateranges');
		}

		if ($search_conditions != $this->session->userdata('search_conditions')) {
			$this->session->set_userdata('search_conditions',$search_conditions);
			$this->session->set_userdata('search_term',$search_conditions['code']);
		}else{
			$search_conditions = $this->session->userdata('search_conditions');
		}

		$getData = array($search_dateranges,$search_conditions);

		return $getData;
	}

	private function getDataTables($limit = NULL)
	{			

		$user_id = $this->tank_auth->get_user_id();

		$sess_cat = $this->session->userdata('sess_cat');
		$sessID   = $this->session->flashdata('anID');

		$field  = $sess_cat ? "id_cat" : "";
		$id_cat = $sess_cat ? $sess_cat  : "";

		if($sess_cat){	$where = "((t1.subcontent_type = ".$sess_cat." AND (t1.status != '5')) OR (t1.subcontent_type = ".$sess_cat." AND (t1.status = '6') AND t1.created_id = ".$user_id."))";
		} else { $where = "((".$get_subcontents." (t1.status != '5')) OR (".$get_subcontents." (t1.status = '6') AND t1.created_id = ".$user_id."))";}

		if ($this->session->userdata('code_groups') == 'adm_asyst') {
			$where .= ' AND t1.status = "10"';
		}

		/* get data for table view from database with pagination */
		$table 	     = "announce t1";
		$select 	 = "t1.id,code,t4.username,content_type,subcontent_type,title_ind,title_eng,detail_eng,detail_ind,first_valdate,id_cat,end_valdate,publish_date,t1.status,t1.created_date,t1.id_departement";
		$order 		 = array('field'=>'t1.created_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);

		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);

		$joins[0][0] = 'announcement_level_approval t2';
		$joins[0][1] = 't2.id_announcement = t1.id';
		$joins[0][2] = 'left';
		$joins[1][0] = 'level t3';
		$joins[1][1] = 't2.id_level = t3.id';
		$joins[1][2] = 'left';
		$joins[2][0] = 'users t4';
		$joins[2][1] = 't4.id = t1.created_id';
		$joins[2][2] = 'left';

		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,$order,$per_page,$offset,"t1.id");

		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'announce/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);

		/** setup for pagination **/
		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();
		/** ===== **/
		/* ===== */

		/* setup variable to used in another functions */
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
		/* ===== */

		return $data;

	}

	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$where_or = array())
	{

		$user_id = $this->tank_auth->get_user_id();

		$sess_cat = $this->session->userdata('sess_cat');
		$sessID   = $this->session->flashdata('anID');
			
		$field    = $sess_cat ? "id_cat" : "";
		$id_cat   = $sess_cat ? $sess_cat  : "";			

		if($sess_cat){	$where = "(t1.subcontent_type = ".$sess_cat." AND (t1.status != '5')) OR (t1.subcontent_type = ".$sess_cat." AND (t1.status = '6') AND t1.created_id = ".$user_id.")";
		} else { $where = "(".$get_subcontents." (t1.status != '5')) OR (".$get_subcontents." (t1.status = '6') AND t1.created_id = ".$user_id.")";}

		/* get data for table view from database with pagination */
		$table 	     = "announce t1";
		$select 	 = "t1.id,code,t4.username,content_type,subcontent_type,title_ind,title_eng,detail_eng,detail_ind,first_valdate,id_cat,end_valdate,publish_date,t1.status,t1.created_date,t1.id_departement";
		$order 		 = array('field'=>'t1.publish_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);

		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$where_or);

		$joins[0][0] = 'announcement_level_approval t2';
		$joins[0][1] = 't2.id_announcement = t1.id';
		$joins[0][2] = 'left';
		$joins[1][0] = 'level t3';
		$joins[1][1] = 't2.id_level = t3.id';
		$joins[1][2] = 'left';
		$joins[2][0] = 'users t4';
		$joins[2][1] = 't4.id = t1.created_id';
		$joins[2][2] = 'left';

		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,$where,$dateranges,$where_or,$order,$per_page,$offset,"t1.id");

		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'announce/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);

		/** setup for pagination **/
		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();
		/** ===== **/
		/* ===== */

		/* setup variable to used in another functions */
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
		/* ===== */

		return $data;

	}

 	private function getAttachmentByFilename($filename = ''){
	 	$lists   = $this->crud->browse('','document_requirement','filename',$filename,'true');
	 	$attpath = array();

	 	foreach ($lists as $val) {
	 		array_push($attpath, $val->filename.'_'.$val->counter);
	 	}
	 		
	 	return $attpath;
	}

	private function portal_edit($subcontent_id,$announce_id){
	 	$controller_lists = $this->getControllerBySubcontentId();
	 	$controller 	  = array_key_exists($subcontent_id, $controller_lists)?$controller_lists[$subcontent_id]:false;

	 	return $controller!=false?redirect($controller.'/form_update/'.$announce_id):'';
	}

	private function getControllerBySubcontentId(){
	 	$controller_lists = array(
			 			'1'=>'news_public',
			 			'2'=>'news_blast',
			 			'3'=>'open_bidding',
			 			'4'=>'open_sourcing',
			 			'5'=>'direct_selection',
			 			'6'=>'limited_bidding'
			 			);

	 	return $controller_lists;
 	}

	private function isNeedVendor($id) {
		$details = $this->crud->browse("","subcontent_type","id",$id,"true","need_vendor");

		if (count($details)>0) {
			foreach ($details as $detail) {}
			if ($detail->need_vendor == 1) {
				return true;
			}else{
				return false;
			}
		}

		return false;
	}

	private function isNotRedlistOn($id_vendor) {
		$details = $this->crud->browse("","a","id",$id,"true","need_vendor");

		if (count($details)>0) {
			foreach ($details as $detail) {}
			if ($detail->need_vendor == 1) {
				return true;
			}else{
				return false;
			}
		}

		return false;
	}

	// private function getParticipantsForNotificationsbyAnnounceId($id_announce,$id_subcontent) {
	// 	$joins[0][0] = 'vendor v';
	// 	$joins[0][1] = 'ap.id_vendor = v.id';
	// 	$joins[0][2] = 'left';

	// 	if ($id_subcontent == '6') {
	// 		$where = 'v.vendor_type IN ("shortlist","unregister")';
	// 	}else{
	// 		$where = '';
	// 	}

	// 	$details = $this->crud->browse_join("","announce_join_participant ap","id_announce",$id_announce,"true","id_vendor,v.email,v.register_num",$joins,$where);
			
	// 	if (count($details)>0) {
	// 		return $details;
	// 	}			

	// 	return false;
	// }

	private function getStatusCondition(){
		$data = array();

		$data = array(
					1  => 'Create',
					2  => 'New',
					3  => 'Approve',
					4  => 'Published',
					5  => 'Draft',
					6  => 'Rejected',
					7  => 'Closed',
					8  => 'Canceled',
					9  => 'Expired',
					10 => 'English Completion',
					11 => 'Update Needs Resend Email',
					99 => 'Unpublish'
				);

		return $data;
	}

	private function getCategories()
	{
		/* get categories data from database */
		$select = array(''=>'All');

		$iterations = $this->crud->browse("","category","","","true","id,category");

		if(!$iterations) $iterations = array();
		foreach($iterations as $val){ $iteration[$val->id] = $val->category; } 
		$getData = $select+$iteration;
		/* ===== */

		return $getData;
	}

	private function getContentSubcontentBySess(){
		$subtemp = $subcontent_allowed = $content_allowed = array();

	 	$sublevels = $this->crud->browse('','level','id_group',$this->session->userdata('user_group'),'true','id_subcontent');
	 	foreach ($sublevels as $sublevel) {
	 		array_push($subtemp, $sublevel->id_subcontent);
	 	}	 		
	 	
	 	if (count($subtemp) > 0) {
	 		$where    = 't1.id IN ('.implode(',', $subtemp).')';

	 		$joins[0][0] = 'content_type t2';
	 		$joins[0][1] = 't1.content_id = t2.id';
	 		$joins[0][2] = 'left';

	 		$sublists = $this->crud->browse_join('','subcontent_type t1','','','true','t1.id,content_id,subtype_name,type_name',$joins,$where);

		    if (count($sublists) > 0) {
				foreach ($sublists as $sublist) {
					!array_key_exists($sublist->id, $subcontent_allowed) ? $subcontent_allowed[$sublist->id] = $sublist->subtype_name:false;
					!array_key_exists($sublist->content_id, $content_allowed) ? $content_allowed[$sublist->content_id] = $sublist->type_name:false;
		    	}		    	
		    }
	 	}

	 	$this->session->set_userdata('subcontent_allowed',$subcontent_allowed);
	 	$this->session->set_userdata('content_allowed',$content_allowed);	 		
	}

	private function getContentTypes($flag = '')
	{

		/* get categories data from database */
		$getData = array();
		if ($this->session->userdata('content_allowed')) {
			$select  = array(''=>'All');

			$allowed_key = implode(',', array_keys($this->session->userdata('content_allowed')));
			$allowed_val = implode(',', $this->session->userdata('content_allowed'));

			$where   = $this->session->userdata('content_allowed')?'id IN ('.$allowed_key.')':'';

			$iterations = $this->crud->browse("","content_type","","","true","id,type_name",$where);

			if(!$iterations) $iterations = array();
			foreach($iterations as $val){ $iteration[$val->id] = $val->type_name; } 
			$getData = $select+$iteration;
			$result  = $getData;

			if ($flag != '') {
				$result  = array();
				if ($flag == 'bidding') {
					$temps  = $this->crud->browse("","content_type","","","false","id,type_name",'id IN ("2","3")');
				}elseif ($flag == 'sourcing') {
					$temps  = $this->crud->browse("","content_type","","","false","id,type_name",'id IN ("2")');
				}
				if(!$temps) $temps = array();
				foreach($temps as $val){ $result[$val->id] = $val->type_name; }
			}
		}
		/* ===== */

		return $result;
	}

	private function getAllContentTypes()
	{

		/* get categories data from database */
		$getData = array();

		$iterations = $this->crud->browse("","content_type","","","true","id,type_name");

		if(!$iterations) $iterations = array();
		foreach($iterations as $val){ $iteration[$val->id] = $val->type_name; } 
		$getData = $iteration;
		/* ===== */

		return $getData;

	}

	private function getSubcontentTypes($flag = '')
	{

		/* get categories data from database */
		$getData = array();

		if ($this->session->userdata('subcontent_allowed')) {
			$select = array(''=>'All');

			$allowed_key = implode(',', array_keys($this->session->userdata('subcontent_allowed')));
			$allowed_val = implode(',', $this->session->userdata('subcontent_allowed'));

			$where  = $this->session->userdata('subcontent_allowed')?'id IN ('.$allowed_key.')':'';

			$iterations = $this->crud->browse("","subcontent_type","","","true","id,subtype_name",$where);

			if(!$iterations) $iterations = array();
			foreach($iterations as $val){ $iteration[$val->id] = $val->subtype_name; } 
			$getData = $select+$iteration;
			$result  = $getData;

			if ($flag != '') {
				$result  = array();
				if ($flag == 'bidding') {
					$temps  = $this->crud->browse("","subcontent_type","","","true","id,subtype_name","id IN ('3','5','16')");
				}elseif ($flag == 'sourcing') {
					$temps  = $this->crud->browse("","subcontent_type","","","true","id,subtype_name","id IN ('4')");
				}
				if(!$temps) $temps = array();
				foreach($temps as $val){ $result[$val->id] = $val->subtype_name; }
			}
		}
		/* ===== */

		return $result;
	}

	private function getAllSubcontentTypes()
	{

		/* get categories data from database */
		$getData = array();

		$iterations = $this->crud->browse("","subcontent_type","","","true","id,subtype_name");

		if(!$iterations) $iterations = array();
		foreach($iterations as $val){ $iteration[$val->id] = $val->subtype_name; } 
		$getData = $iteration;
		/* ===== */

		return $getData;
	}

	//////////////   Dedi Rudiyanto  ///////////////

	// private function send_invitation_limitedbidding($stremail,$regNum,$announce_id){
	//  	$expire = date('Y-m-d', strtotime("+3 days"));
	// 	$token = uniqid();

	// 	$userID 	 = $this->tank_auth->get_user_id();
	// 	$curr_date 	 = date('Y-m-d H:i:s');
	// 	$idx  		 = $this->crud->browse("","vendor","register_num",$regNum,"false","id");

	// 	$data_session = array('id'		  => null,
	// 						'session'	  => $token,
	// 						'id_vendor'	  => $idx->id,	
	// 						'expired_date'=> $expire,
	// 						'created_id'  => $userID,			 	
	// 				 	  	'created_date'=> $curr_date,
	// 				 	  	'last_updated'=> $curr_date);

	// 	$this->crud->insert("","session_verification",$data_session);

	//  	$pattern = '/^[A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z]{2,4}$/';
	// 	// if(preg_match($pattern, $stremail) && $stremail!='' )
	// 	if(filter_var($stremail, FILTER_VALIDATE_EMAIL) && $stremail!='' )
	// 	{		
	// 		$subject =  "Limited Invitation - E-procurement Garuda Indonesia";
	// 		$to 	= $stremail ;
	// 		$unixcode = md5( $announce_id . $token ) ; 
	// 		$unixcode = substr($unixcode,5,1) . $unixcode . substr($unixcode,-15,5) ;
	// 		//////////////////////////////////////////
	// 		// message
	// 		$message	= '
	// 		<!DOCTYPE html>
	// 		<html>
	// 		<head>
	// 		<title></title>
	// 		<meta http-equiv="Content-Type" content="text/html; charset = utf-8"/>
	// 		</head>
	// 		<body><div align="center"><div align="justify" style="width:600px;"><br/><br/>

	// 		<b>Kepada Yth.</b><br/>
	// 		Calon rekanan PT. Garuda Indonesia.<br/><br/>

	// 		Dengan hormat,<br/><br/>

	// 		Bersama ini kami sampaikan undangan terbatas <br/>

	// 		<b><a href="'.base_url('register/form_register_invitation/'.$announce_id.'/'.$token.'/'.$unixcode).'">Link Registrasi</a></b><br/><br/>

			

	// 		<b>PT Garuda Indonesia (Persero) Tbk </b><br/> 
	// 		<i>
	// 		Business Support / JKTIBGA <br/>
	// 		Gedung Management Garuda, Lantai Dasar <br/>
	// 		Garuda City,  Bandara Soekarno Hatta <br/>
	// 		Cengkareng 19120, PO BOX 1004 TNG BUSH
	// 		</i><br/><br/>
	// 		</div></div>
	// 		</body>
	// 		</html>
	// 		';
				
	// 		$this->crud->sendMail($to,$subject,$message);
	// 	}
			
	// }

	/////////////////////////////////
	 
 
}
?>