

<div class="panel panel-warning text-danger">
<div class="panel-heading">Detail Redlist</div>
	<div class="panel-body">
		<table class="table table-hover">
			<tbody>
				<tr>
					<td>Name</td>
					<td>:</td>
					<td><?=$vendor->vendor_name ?></td>
				</tr>
				<tr>
					<td>Register Number</td>
					<td>:</td>
					<td><?=$vendor->register_num ?></td>
				</tr>
				<tr>
					<td>Type</td>
					<td>:</td>
					<td><?=$vendor->vendor_type ?></td>
				</tr>
				<tr>
					<td>Status</td>
					<td>:</td>
					<td><?=$vendor->status ?></td>
				</tr>
				<tr>
					<td>Reason</td>
					<td>:</td>
					<td><?=$vendor->remark ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="panel-footer"></div>
</div>