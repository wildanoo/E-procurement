<script type="text/javascript">
	$(function() {
		var baseUrl   = "<?php echo base_url(); ?>";
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>';
		var token     = '<?php echo $this->security->get_csrf_hash(); ?>';

		$('#idsubcat').change(function(){
			$('#form_search').submit();
		});
		$('#sortstatus').change(function(){
			$('#form_search').submit();
		});

		$('#form_search').submit(function(){
			var sdata=$(this).serialize();
			var search=$('#search_term').val();
			var v_url = baseUrl+'data_avl/sorting';
			$.post(v_url, sdata, function(data){
				$('#v_data').html(data);
			});
		});

		$('#start_date, #end_date').datepicker({
			changeMonth: true, changeYear: true, dateFormat:"yy-mm-dd"
		});

		$('#checkall').click(function(){
			$(this).closest('table').find(':checkbox').prop('checked', this.checked);
		});


	});

	function load_index(){
		var baseUrl   = "<?php echo base_url(); ?>";
		$('#form_search')[0].reset();
		$('#v_data').load(baseUrl+'data_avl');
	}


</script>

<div id="main_container">
	<div class="page-header">
		<h3>Vendor List</h3>
	</div>
	<div class="table">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-search"></i> <strong>Search Section</strong>
			</div>
			<style type="text/css">
				.list-inline li{
					vertical-align: middle !important;
				}
				.btn-srch{
					margin-bottom: 0;
				}
			</style>
			<?php echo form_open('
data_avl/sorting',array('id'=>'form_search','novalidate'=>'true')); ?>
			<div class="panel-body">
				<ul class="list-inline">
					<li style="width:24%">
						<div class="input-group">
							<input type="date" value="" placeholder="date start" class="form-control date_picker" id="start_date" name="start_date">
							<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
						</div>
					</li>
					<li style="width:2%">to</li>
					<li style="width:24%">
						<div class="input-group">
							<input type="date" value="" placeholder="date end" class="form-control date_picker" id="end_date" name="end_date">
							<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
						</div>
					</li>
					<li style="width:34%"><div class="input-group" style="width:100%"><input placeholder="search by vendor name..." type="text" value="" class="form-control" id="search_term" name="search_term"></div></li>
					<li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search" id="searchbtn">Search</button></div></li>
					<li style="width:7%"><div class="input-group"><a href="<?php echo base_url() ?>
data_avl" class="btn btn-srch btn-warning btn-sm">View All</a></div></li>
				</ul>
			</div>

		</div>
	</div>
	<hr>

	<!-- search part -->

	<div class="col-md-3">
		<select name="subcat" id="idsubcat" class="form-control">
			<option value="all">- All Subcategories -</option>
			<?php foreach($subcat as $sc){ ?>
				<option  value="<?php echo $sc->idsub  ?>"<?php if($sc->idsub==$this->session->userdata('id')){echo "selected";} ?> ><?php echo $sc->category.' - '.$sc->subcategory; ?></option>
				<?php } ?>
			</select>
		</div>

		<?php echo form_close(); ?>
		<?php echo form_open('
data_avl/export_excel',array('id'=>'myform')); ?>
		<button class="btn btn-primary pull-right" type="submit"  title="Export Vendor List">Export Excel</button>

		<table class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%w">
			<thead>
				<tr>
					<th>No</th>
					<th><input type="checkbox" id="checkall" value="1"></th>
					<th>Registration Num.</th>
					<th>Vendor Num.</th>
					<th width="200">Vendor Name</th>
					<th width="300">Category - Subcategory</th>
					<th>Type</th>
					<th>Vendor Status</th>
					<th>Created Date</th>
				</tr>
			</thead>
			<tbody id="v_data">
				<?php require_once('list_data.php'); ?>
			</tbody>
		</table>
		<?php echo form_close(); ?>
	</div>
