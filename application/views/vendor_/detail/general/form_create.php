<script type="text/javascript">
 $(function () {
   	var baseUrl  = "<?php echo base_url(); ?>";
   	
   	$('#btn_create1').click(function(){
    	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		var values  = $("#form2").serializeArray();
				values.push({name: csrf,value: token});
				values = jQuery.param(values); 
						$.ajax({
						    url  : baseUrl + 'vendor/add_contact',
						    type : "POST",              		               
						    data : values,
						    success: function(resp){
						        //$('#debug').html(resp);
						        $("#container").load(baseUrl+'vendor/load_general');
						    	dialogRef.close(); 
						    }
						});							     	
     }); 
     
     $('#btn_cancel1').click(function(){          	
   		 $("#container").load(baseUrl+'vendor/load_general');   		
     });            		
		 	   
 });
 </script>
<?php echo form_open('#',array('id'=>'form2')); ?>
<table width="100%">
<tr>
	<td><label>Vendor Name</label></td><td>:</td>
	<td><?php  echo $vendor->name; ?> </td>	
</tr>
<tr>
	<td><label>Contact Name</label></td><td>:</td>
	<td><input type="text" name="name" id="name" placeholder="Contact Name" class="form-control" required/></td>	
</tr>
<tr>
	<td><label>Position</label></td><td>:</td>
	<td><input type="text" name="position" id="position" placeholder="Position" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Mobile</label></td><td>:</td>
	<td><input type="text" name="mobile" id="mobile" placeholder="Mobile" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Email</label></td><td>:</td>
	<td><input type="text" name="email" id="email" placeholder="Email" class="form-control" required/></td>
</tr>
<tr>
	<td colspan="3">
		<input type="button" id="btn_cancel1" value="Cancel" class="btn btn-default btn-sm"/>
		<input type="button" id="btn_create1" value="Submit" class="btn btn-primary btn-sm"/>		
	</td>	
</tr>
</table>
<?php echo  form_close(); ?>
