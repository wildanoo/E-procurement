<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Sap_login extends Controller {

	function __construct()
	{
		parent::__construct();			
		$this->load->model('m_user');
		islogged_in();
		$this->m_user->authenticate(array(41));
	}	
		
	function index(){

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			
			$sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
				
			
			$field      = $sess_field != '' ? $sess_field : "division_name";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
			
			if($sess_cat != ''){	$where = array('division_name'=>$sess_cat,'status'=>'1');
			} else { $where = array('status'=>'1');	}
			
	 		$table 	    = "division";		
			$page       = $this->uri->segment(3);
			$per_page   = 10;		
			$offset     = $this->crud->set_offset($page,$per_page,$where);
			$total_rows = $this->crud->get_total_record("",$table,$where);
			$set_config = array('base_url'=> base_url().'/division/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
			$config     = $this->crud->set_config($set_config);		
				 
			$this->load->library('pagination');			 
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();			
				
			$order 			     = array('field'=>'created_date','order'=>'DESC');
			$data['pagination']  = $paging;
			$data['num']         = $offset; 
			$select 			 = "id,(SELECT office_name FROM offices WHERE id=id_office)as id_office,status,division_code,division_name,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
			
			$sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",'status','1',"true",$select,"",$order,$config['per_page'],$offset);
			$data['browse'] = $browse;
			
			$division = $this->crud->browse("","division","","","true","id,division_name,division_code");
			if(!$division) $division = array();
			$options = array();
			$select = array(''=>'All');
			foreach($division as $val){ $options[$val->id] = $val->division_name; }
			$data['division'] = $select + $options;
			
	 		$data['view']	= "sap/browse"; 		
 			$this->load->view('layout/template',$data);

	}
}