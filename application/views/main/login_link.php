<script type="text/javascript">
    $(document).ready(function(){
        $('#create').click(function(){
            var baseUrl   = "<?php echo base_url(); ?>";
            var login     = $('#login').val();
            var password  = $('#password').val();
            var remember    = $('#remember').val();
            var sess_token  = $('#sess_token').val();
            var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>';
            var token     = '<?php echo $this->security->get_csrf_hash(); ?>';

            $.ajax({
                url  : baseUrl + 'auth/validate_login',
                type : "POST",
                data : csrf +'='+ token +'&login='+login+'&password='+password+'&remember='+remember,
                success: function(resp){
                    // $('#debug').html(resp);
                    var json = $.parseJSON(resp);
                    if(json.status=="false"){
                    	var text = json.errors;
                    	var msg  = text.split('_').join(' ');
                    	$("#message").addClass("alert alert-warning");
                        $("#message").html('<strong>Warning</strong>!, ' + msg);
                    } else {
                        document.location.href= baseUrl + 'authenticate/login/'+sess_token;
                    }
                }
            });
        });
    });
</script>


<div id="debug"></div>
<div class="about-content-wrap pull-left" id="#">
	<div class="head" style="margin-top: 25px;">
		<h1 style="color:#fff;font-size: 35px;font-family: Cabin, sans-serif;font-weight: 300;text-transform: uppercase;">Login</h1>
		<a href="#" class="next-section about animate2">
			<i class="fa fa-angle-double-down"></i>
		</a>
	</div>
	<div class="container">
		<div class="col-md-10 col-md-push-1 content-tabs-wrap wow fadeInUp animated" data-wow-delay="1.7s" data-wow-offset="200">
			<div class="col-xs-12 col-sm-12 col-md-12 content-tabs no-pad">
				<div class="tab-content">
					<div class="tab-pane fade in active" id="tab-home">
						<div class = "row">
							<div class="col-md-12 tab-content">
								<div class="box2 box-info2 tab-pane fade in active" id="registrasi-step-1">
									<div class="form-horizontal">
										<div class="box-body">
											<div class = "container2">
												<div class = "row2" style="margin-top:20px;">
													<div class="appointment-form col-xs-12 col-md-12 no-pad wow fadeInRight animated" data-wow-delay="1s" data-wow-offset="200">
														<div class="row background-form">
															<div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>
															<?php echo $this->lang->line('section5_sub1');?></div>
				<div style="background-color:#fff;float: left;background: #fff;padding: 30px 25px 15px 25px;border: 1px solid #e6e7e8;border-top: none;margin-bottom: 10px;">
				
				<?php
                    $login = array( 'type'          => 'email',
                                    'placeholder'   => 'Username',
                                    'name'          => 'login',
                                    'id'            => 'login',
                                    'value'         => set_value('login'),
                                    'maxlength'     => 80,                                   
                                    'size'          => 30,);
                    if ($login_by_username AND $login_by_email) {
                        $login_label = 'Email or login';
                    } else if ($login_by_username) {
                        $login_label = 'Login';
                    } else {
                        $login_label = 'Email';
                    }
                    $password = array(  'type'          => 'password',
                                        'placeholder'   => 'Password',
                                        'name'          => 'password',
                                        'id'            => 'password',
                                        'size'          => 30,);
                    $remember = array(
                        'type'  => 'checkbox',
                        'name'  => 'remember',
                        'id'    => 'remember',
                        'value' => 1,
                        'checked'   => set_value('remember'),
                        'class' => 'icheckbox_square-blue',
                    );
                   
                ?>
                
                <?php echo form_open("#",array('id'=>'contact-form')); ?>                    
                    <div class="text-fields">
                        <div class="float-input">
                        	<input type="hidden" name="sess_token" id="sess_token" value="<?php echo $token; ?>"/>
                            <?php echo form_input($login); ?>
                            <span><i class="fa fa-user"></i></span>
                        </div>
                        <?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
                        <div class="float-input">
                            <?php echo form_password($password); ?>
                            <span><i class="fa fa-lock"></i></span>
                        </div>
                        <?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
                        <div style="text-align: right;">
                        <?php echo anchor("auth/forgot_password/","Forgot Password ?",array('style'=>'font-size:12px; text-decoration: none;'));?></div>
                        <div id="message"></div>
                        <input type="button" id="create" class="main-button" value="Login">                        
                    </div>
                <?php echo form_close(); ?>
            </div>



																	
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
</div>
	




