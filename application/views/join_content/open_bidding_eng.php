<script type="text/javascript"> 
    $(function(){
        var baseUrl   = "<?php echo base_url(); ?>";
        var path      = window.location.pathname.split( '/' );

        // $('#btnCheckNext').click(function(event){
        //     event.preventDefault();
        //     var vend    = $('#vendor_name').val();
        //     var addr    = $('#vendor_address').val(); 
        //     var phone   = $('#phone').val(); 
        //     var fax     = $('#fax').val(); 
        //     var email   = $('#email').val();
        //     var npwp    = $('#npwp').val();
        //     var cp_name = $('#cp_name').val(); 
        //     var cp_mobil= $('#cp_mobile').val();
        //     var cp_email= $('#cp_email').val(); 

        //     if( vend=='' || addr=='' || phone=='' || fax=='' || email=='' || npwp=='' || cp_name=='' || cp_mobil=='' || cp_email=='' )
        //     {   alert('Any Your Input may be problematic, Check Your Registration Form');
        //         return false;
        //     }else{
        //         return true;
        //     }
        // });

        $('#btnCheckNext').click(function(event){
            event.preventDefault();
            // if(document.getElementById('i_accept').checked && document.getElementById("file_loi").files.length != 0)
            // {
                var values  = $("#form").serializeArray();
                var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
                var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
                values.push({name: csrf,value: token}); values = jQuery.param(values);          
                $.ajax({
                   url  : baseUrl + 'joinbiddingsourcing/validate_join',
                   type : "POST",                                  
                   data : values,
                   success: function(resp){ //$('#debug').html(resp);
                        var json = $.parseJSON(resp); 
                         if(json.status=="false"){
                            $('#msg1').html(json.msg1); $('#msg6').html(json.msg6);
                            $('#msg2').html(json.msg2); $('#msg7').html(json.msg7);
                            $('#msg3').html(json.msg3);
                            $('#msg8').html(json.msg8);
                            $('#msg4').html(json.msg4);
                            if (json.msg9!=1 || json.msg9!=2  || json.msg9!=4) {$('#msg9').html(json.msg9);}
                            $('#msg5').html(json.msg5); $('#msg10').html(json.msg10);
                            $('#msg11').html(json.msg11);
                            // alert('Any Your Input may be problematic, Check Your Registration Form');
                        } else { if(json.msg9==2)
                            { 
                                var r = confirm("Your NPWP or COD have exist on our system \nWould you want to continue with existed data on our database?");
                                if (r == true) {
                                    $("#btn_next").trigger("click");
                                } 
                            }else if( json.msg9==1){
                                var re = confirm("Your NPWP have existed on our system. Please login to join.");
                                var count = path.length;
                                var num = parseInt(count) - parseInt(1) ;
                                link = '';
                                for(var i = 1; i < num; ++i){
                                    link = link + '/' + path[i]; 
                                }
                                if (re == true) {
                                    window.location = link;
                                } 
                            } else {
                                $("#btn_next").trigger("click");
                            }
                        }
                   }
                });

            // }else{
            //     alert('Please include your LOI document');
            //     return false;
            // }
        });

        $('#btnsubmit').click(function(){
            if(!document.getElementById('i_accept').checked){
                alert(' Please accept term and condition ');
            }else if(document.getElementById("file_loi").files.length == 0){
                alert('Please include your LOI document');
            }
            else{
                $("#form").submit();
            }
            return false;
        });

        $("input[type=file]").change(function(){
            var filename = $(this).val() ;
            filex = filename.split(".");
            tipe = filex[filex.length-1] ;

            if(tipe!='pdf') { $(this).val(""); alert('file must pdf by type'); }
        });
        
    }); 
</script>

<div class="about-content-wrap pull-left" id="#">
    <div class="head" style="margin-top: 25px;">
        <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;">Registration</h1>
        <a href="#" class="next-section about animate2">
            <i class="fa fa-angle-double-down"></i>
        </a>
    </div>
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 content-tabs-wrap wow fadeInUp animated" data-wow-delay="1.7s" data-wow-offset="200">
            <div class="col-xs-12 col-sm-12 col-md-12 content-tabs no-pad">            
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#" data-toggle="tab"> Open Bidding </a></li>
                  <li><a>&nbsp;</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-home">
                        <div class = "row">
                            <div class="col-md-12 tab-content">
                                <div class="box2 box-info2 tab-pane fade in active" id="registrasi-step-1">
                                    <div class="box-body">
                                        <div class="about-intro-wrap pull-left">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 faq-tabs-wrap">
                                                    <div class="tabbable tabs-left">
                                                        <div class="tab-content col-md-12 col-sm-12 col-xs-12">
                                                            <div class="tab-pane active" id="a">
                                                                <div class="dept-title-tabs">Vendor Policy</div>
                                                                <div class="background-vendor">
                                                                    <div style="padding:20px;">
                                                                        <?php 
                                                                        $root = base_url();   $path = $root."uploads/policy_agreement/".$policy_agreements->file."_".$this->session->userdata('lang').".txt";
                                                                        if( file_exists($path)) {   echo file_get_contents($path);
                                                                        } else { echo file_get_contents($path); }
                                                                        echo "<hr/>";
                                                                        
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-left" style="margin-right:30px;">
                                            <button type="button" class="btn button-custom" data-toggle="tab" href="#registrasi-step-2">Next</button>
                                        </div> 
                                    </div>
                                </div>
                                <div class="box2 box-info2 tab-pane fade" id="registrasi-step-2">
                                    <div class="box-body">
                                        <div class="dept-title-tabs">Registration Form</div>
                                        <div class = "container2">
                                            <div class = "row2" style="margin-top:20px;">
                                                <div class="appointment-form col-xs-12 col-md-12 no-pad wow fade in right animated" data-wow-delay="1s" data-wow-offset="200">
                                                    <div class="row background-form">
                                                        <div class="appointment-form-title"><i class="fa fa-building fa-1x" style="color: #fff;margin: 0 20px;"></i>Company</div>
                                                        <?php echo form_open_multipart('joinbiddingsourcing/insert_vendor_from_open_bidding_announcement',array('id'=>'form')); ?>

                                                        <input type="hidden" name="unikform" value="<?php echo $unikform ;?>" />
                                                        <input type="hidden" name="id_subcat" value="<?php echo $subcategoryx->id ;?>" />

                                                        <div>
                                                            <div class="appt-form">                                                                
                                                                <div id="msg1" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        Supplier Name
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="appt-form-txt" id="vendor_name" name="vendor_name" placeholder="Supplier Name" />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        Category
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="appt-form-txt" value="<?php echo $categoryx->category; ?> -> Subcategory <?php echo $subcategoryx->subcategory; ?>" style="background-color:#EEF" readonly/>
                                                                    </div>
                                                                </div>

                                                                <div id="msg2" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        Supplier Address
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="appt-form-txt" placeholder="Supplier Address" id="vendor_address" name="vendor_address" />
                                                                    </div>
                                                                </div>
                                                                
                                                                <div id="msg6" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        Phone Number
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="appt-form-txt" placeholder="Phone Number" id="phone" name="phone" onkeypress="return isNumber(event)" />
                                                                    </div>
                                                                </div>
                                                                
                                                                <div id="msg7" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        Fax Number
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="appt-form-txt" placeholder="Fax Number" id="fax" name="fax" />
                                                                    </div>
                                                                </div>

                                                                <div id="msg8" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        Email Address
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="email" class="appt-form-txt" placeholder="Email Address" id="email" name="email" />
                                                                    </div>
                                                                </div>
                                                                
                                                                <div id="msg9" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        NPWP or COD
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="appt-form-txt"placeholder="NPWP or COD" id="npwp" name="npwp" />
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="appointment-form-title">
                                                            <i class="icon-hospital2 appointment-form-icon"></i>Contact Person
                                                        </div>
                                                        <div class="appt-form">
                                                            <div id="msg5" style="color: red; font-style: italic; "></div>
                                                            <div class="row">
                                                                <div class="col-sm-2" style="top:5px">
                                                                    Contact Person
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="appt-form-txt" placeholder="Contact Person" id="cp_name" name="cp_name" />
                                                                </div>
                                                            </div>

                                                            <div id="msg10" style="color: red; font-style: italic; "></div>
                                                            <div class="row">
                                                                <div class="col-sm-2" style="top:5px">
                                                                    Mobile Phone
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="appt-form-txt" placeholder="Mobile Phone" id="cp_mobile" name="cp_mobile" />
                                                                </div>
                                                            </div>

                                                            <div id="msg11" style="color: red; font-style: italic; "></div>
                                                            <div class="row">
                                                                <div class="col-sm-2" style="top:5px">
                                                                    Email
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <input type="email" class="appt-form-txt" placeholder="Email" id="cp_email" name="cp_email" />
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <?php 
                                                            $blank_post = array('id_lang','id_country','id_region','district','postcode','city',
                                                                                'street','house_num','sprefix','ssuffix','building','floor',
                                                                                'room','bank_name','branch_detail','bckey','acc_holder','acc_num','bank_region');                       
                                                            foreach($blank_post as $field){ echo form_hidden($field,""); }
                                                        ?>
                                                        
                                                    
                                                    </div>
                                                    <button type="button" id="btn_next" data-toggle="tab" style="visibility:hidden;" href="#registrasi-step-3" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-left" style="margin-top:30px;">
                                            <button type="button" class="btn button-custom-back" data-toggle="tab" href="#registrasi-step-1">Back</button>
                                            <button type="button" class="btn button-custom" id="btnCheckNext">Next</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="box2 box-info2 tab-pane fade" id="registrasi-step-3">
                                    <div class="row background-terms">
                                        <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>Terms and Condition</div>
                                        <div class="terms-custom">
                                            <div class="form-group">
                                                
                                                This Privacy Policy governs the manner in which Garuda Indonesia collects, uses, maintains and discloses information collected from users (each, a "User") of the https://www.eproc.garuda-indonesia.com/ website ("Site"). This privacy policy applies to the Site and all products and services offered by Garuda Indonesia
                                                <br>
                                                <br>
                                                <label>
                                                <input type="checkbox" name="i_accept" id="i_accept" style="margin-right:10px;">I accept the Terms and Condition
                                                </label>
                                                <br/>
                                                <br>
                                                <ul class="list-inline">
                                                    
                                                    <li style="width:155px">LOI Attachment <i>(*.pdf)</i></li>
                                                    <li><input type="file" name="file_loi" id="file_loi" style="margin-right:10px;"></li>
                                                    
                                                </ul>
                                                <br>
                                                <br>
                                                </span>
                                                <?php echo form_close(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-left" style="margin-top:10px;">
                                        <button type="button" class="btn button-custom-back" data-toggle="tab" href="#registrasi-step-2">Back</button>
                                        <button type="button" class="btn button-custom" id="btnsubmit" href="#">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade no-pad" id="tab-profile">
                        <div class="row background-terms">
                            <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>Tracking Registration Status</div>
                            <div class="col-md-12 col-sm-12 terms-custom">
                                <div class="form-group terms-text">
                                    Please Enter Registration Number Then Click "Search"
                                    <br>
                                    <br>
                                    <input type="text" class="appt-form-txt" placeholder="Registration Number" style="width:40%;" /><button type="button" class="btn button-search" data-toggle="tab" href="#registrasi-step-6">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </div>
</div>