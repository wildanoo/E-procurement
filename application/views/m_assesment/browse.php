<script type="text/javascript">
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";
});

	function load_data()
	{
		$('#debug').load(baseUrl+'m_assesment');
	}

	function form_create(id)
	{
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>';
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
        var sdata = {'id':id, 'name':csrf, 'value':token};
		$.post(baseUrl+'m_assesment/form_assesment', sdata, function(data, textStatus, xhr) {
			$('#debug').html(data);
		});
	}

	function inactivate_data(id)
	{
		var cek = confirm('Anda ingin menghapus data ini ?');
		if(cek)
		{
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>';
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	        var sdata = {'id':id, 'name':csrf, 'value':token};
			$.post(baseUrl+'m_assesment/inactivate', sdata, function(data, textStatus, xhr) {
		  	  var obj = JSON.parse(data);
		      if(obj.state == 'true')
		      {
		  		  load_data();
		      }
		      else
		      {
		        var err_msg = $('<span class="label label-warning"></span>').text(obj.msg);
		        $('#fc_msg').html(err_msg);
		      }

		  	});
		}
	}

</script>

<div id="debug">


<div id="list-data">
	<?php require_once('list.php'); ?>
</div>

</div> <!-- debug -->
