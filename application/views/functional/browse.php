<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";		

$('#btn_create').click(function(){  form_create();  });	
});

function form_create(){
			var baseUrl = "<?php echo base_url(); ?>";		 
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
			BootstrapDialog.show({
			title: 'Create Functional',
            message: $('<div></div>').load(baseUrl + "functional/form_create/"),
            buttons: [{ label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){
		                	 var code = $("#code").val();
		                	 //var functional = $("#functional").val(); 

		                	 $.post( baseUrl + 'functional/validate_code', { csrf: token , code : code })
							 .done(function( resp ) {

							var json = $.parseJSON(resp);
							if (json.status=='true'){
			                	var values = $("#form").serializeArray();
			                	values.push({ name: csrf,value: token });
								values = jQuery.param(values);	
								 $.ajax({
						               url  : baseUrl + 'functional/create',
						               type : "POST",              		               
						               data : values,
						               success: function(resp){
						               		document.location.href= baseUrl + 'functional/';	               				               		  
								       }
						            });   
							} else { 	
								$('#msg1').html('Functional Name has been used or Empty !')
							}

							});   
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'functional/';
				                    dialogRef.close(); 
				                }
			        }]
        });
	}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Functional',
            message: $('<div></div>').load(baseUrl + "functional/form_update/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){
		                	var code = $("#code").val(); 
		                	var functional = $("#functional").val();    
		                	//var id = $("#remark").val();
							//alert(remark);
							
		                	 $.post( baseUrl + 'functional/is_exist', { csrf: token , functional : functional , code : code })
							 .done(function( resp ) {

							var json = $.parseJSON(resp);
							if (json.status=='true'){
			                	var values = $("#form").serializeArray();
			                	values.push({ name: csrf,value: token });
								values = jQuery.param(values);	
								 $.ajax({
						               url  : baseUrl + 'functional/update',
						               type : "POST",              		               
						               data : values,
						               success: function(resp){

						            	   //$("#debug").html(resp);
						            	   //dialogRef.close()
						               	   document.location.href= baseUrl + 'functional/';	               				               		  
								       }
						            });   
							} else { 	
								 $('#msg1').html('Employee Number has been used or Empty !')
							}

							});   
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'functional/';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>
<script src="<?php echo base_url(); ?>assets/js/sorttable.js"></script>
</head>
<body><div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Functional</h3>
              </div><!-- /.box-header -->
              <div class="box-body">
                <br>
                    <?php if($this->session->flashdata('msg_warning')!=null) { ?>
                	<div class="alert alert-warning">
  						<strong>Warning!</strong> <?php echo $this->session->flashdata('msg_warning'); ?>
					</div>
					<?php } ?>
                <div class="col-md-2 pull-left">
					<input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Functional" value="New Functional" style="width:120px;" title="Create New Functional"/>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
					<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>	
				</div>
                    
				<div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('functional/search',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>functional" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		        </div>
		
		        </div>
                    
<div class="box-body">
<table id="example2" class="table table-bordered table-hover sortable">
	<thead>
		<tr>
            <th class="sorttable_sorted_reverse">No</th>
            <th class="">Code</th>
            <th class="">Functional</th>
            <th class="">Created Date</th>
            <th class="">Updated Date</th>
            <th class="">Updated By</th>
            <th>Action</th>
        </tr>
     </thead>
     <tbody>
 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td><?php echo $num = $num+1; ?></td>
			<td><?php echo $row->code; ?></td>	
			<td><?php echo $row->functional; ?></td>
			<!--td><?php echo $row->status==1 ? "Active" : "Not Active"; ?></td>-->
			<!-- td><?php //echo $row->remark; ?></td> -->
			<td align="center"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>	
 			<td align="center"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>
 			<td align="center"><?php echo $row->created_id; ?></td>							
				
				<td>
				<?php
					echo  '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/>&nbsp;&nbsp;';
					$trash  = '<i class="fa fa-trash"></i>';
					$js = "if(confirm('Are you sure to delete record ?')){ return true; } else {return false; }";
					echo anchor('functional/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete','onclick'=>$js));			
				?>				
				</td>	
										
		</tr>
<?php	$i++;  } ?>	
</table>
<tr><div colspan="9" style="text-align: center;"><?php echo $pagination;?></div></tr>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>  
	</table>  
                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->
