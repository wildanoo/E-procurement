<?php 

require_once APPPATH.'./libraries/PHPExcel.php';
$objPHPExcel = new PHPExcel();
// Set properties
$objPHPExcel->getProperties()->setCreator("Emade Haryo Kuncoro");
$objPHPExcel->getProperties()->setLastModifiedBy("Emade Haryo Kuncoro");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Redlist Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Redlist Document");
$objPHPExcel->getProperties()->setDescription("Redlist document for Office 2007 XLSX, generated using PHP classes.");

// Set fonts
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFont()->setBold(true);

// Set column widths
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);

// Add some data
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Vendor Redlist');
$objPHPExcel->getActiveSheet()->SetCellValue('A4', 'No');
$objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Registration No.');
$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Vendor No');
$objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Vendor Name');
$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Start Date');
$objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Counter');
$objPHPExcel->getActiveSheet()->SetCellValue('G4', 'Reason');
$objPHPExcel->getActiveSheet()->SetCellValue('H4', 'Status');

$no=1;
$numb = 5;
foreach ($redlist as $row) 
{

	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$numb, $no);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$numb, $row->register_num);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$numb, $row->vendor_num);
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$numb, $row->vendor_name);
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$numb, $row->start_date);
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$numb, $row->counter);
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$numb, $row->remark);
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$numb, ucwords($row->red_state) );

	$numb++;
	$no++;
}

//set border
$limit = count($redlist);
$limit += 4;
$objPHPExcel->getActiveSheet()->getStyle('A4:H'.$limit)->getBorders()->applyFromArray(
         array(
             'allborders'  => array(
                 'style'   => PHPExcel_Style_Border::BORDER_THIN,
                 'color'   => array(
                       'rgb' => '000000'
                 )
             )
         )
 );


$objPHPExcel->setActiveSheetIndex(0);
$filename='vendor_redlist_'.date('YmdHis').'.xlsx';

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//send to browser
$objWriter->save('php://output');
//save to local folder
// $objWriter->save(str_replace('excel.php', $filename, __FILE__));

exit;


 ?>