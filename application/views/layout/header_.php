<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>E-Procurement Garuda Indonesia</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/bootstrap-admin.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/bootstrap-responsive.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/custom/afterlogin.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/custom/afterloginstyle.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/pages/dashboard.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/datepicker3.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/layout/css/form.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/datetimepicker/jquery.datetimepicker.css"/>
<style type="text/css">
	#datetimepicker9 .bootstrap-datetimepicker-widget{
	top: 36px !important;
}
</style>

<script src="<?php echo base_url() ?>assets_/js/jquery-1.9.1.min.js"></script> 

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/ui/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/jquery-ui.css"/>

<script src="<?php echo base_url() ?>assets_/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url() ?>assets_/js/excanvas.min.js"></script> 
<script src="<?php echo base_url() ?>assets_/js/chart.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url() ?>assets_/js/bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets_/js/full-calendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/base.js"></script>
<script src="<?php echo base_url() ?>assets_/datetimepicker/build/jquery.datetimepicker.full.js"></script>

<script src="<?php echo base_url() ?>assets/js/bootstrap-dialog.min.js"></script> 
<link href="<?php echo base_url()  ?>assets/css/bootstrap-dialog.min.css" rel="stylesheet"> 
 

</head>
<body>
<div class="navbar navbar-fixed-top">
<div class="navbar-inner">
  <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
    <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a>
    <a class="brand" href="#"><img src = "<?php echo base_url(); ?>assets_/favicon-white.png" style="margin-right:10px;"/>E-Procurement Garuda Indonesia</a>
    <div class="nav-collapse">
      <ul class="nav pull-right">
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 12px; font-weight: 600;"><i
                          class="icon-user"></i><?php  echo ucfirst($username);  ?> <b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-login">
            <li class="user-header">
              <img src="<?php echo base_url(); ?>assets/images/user.png" class="img-circle" alt="User Image">
              <p>
                <?php  echo ucfirst($username);  ?>
              </p>
            </li>
            <li class="user-footer">
              <div class="col-md-6 col-xs-6">
                <a href="<?php echo base_url(); ?>profiles" style="margin-left: 6px;" class="btn btn-default btn-flat pull-left">Profile</a>
              </div>
              <div class="col-md-6 col-xs-6">
                <a href="<?php echo base_url(); ?>auth/logout" class="btn btn-default btn-flat pull-right">Sign out</a>
              </div>
              <!-- <div class="pull-left"> -->
                <!-- <a href="<?php //echo base_url(); ?>auth/profile" style="margin-left: 6px;" class="btn btn-default btn-flat pull-left">Profile</a> -->
              <!-- </div> -->
              <!-- <div class="pull-right"> -->
                <!-- <a href="<?php //echo base_url(); ?>auth/logout" class="btn btn-default btn-flat pull-right">Sign out</a> -->
              <!-- </div> -->
            </li>
          </ul>
        </li>
      </ul>
      <form class="navbar-search pull-right">
        <input type="text" class="search-query" placeholder="Search">
      </form>
    </div>
    <!--/.nav-collapse --> 
  </div>
  <!-- /container --> 
</div>
<!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
<div class="subnavbar-inner">
  <div class="container">
    <ul class="nav navbar-nav">
    <?php if($this->session->userdata('cek_vendor_auth')){ ?>
      <li class="active"><a href="<?php echo base_url(); ?>avl/dashboard"><i class="fa fa-bullhorn fa-2x"></i><span>Dashboard</span> </a> </li>
      <li><a href="<?php echo base_url(); ?>avl/news"><i class="fa fa-bullhorn fa-2x"></i><span>News</span> </a> </li>
      <li><a href="<?php echo base_url(); ?>avl/vendor"><i class="fa fa-bullhorn fa-2x"></i><span>Vendor Profile</span> </a> </li>
      <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-info-circle fa-2x"></i><span>Bidding Transaction Summary</span> <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url(); ?>avl/current_bidding">Current Incoming Bidding</a></li>
          <li><a href="<?php echo base_url(); ?>avl/history_bidding">Historical Bidding</a></li>
        </ul>
      </li>
      <li><a href="<?php echo base_url(); ?>announce/"><i class="fa fa-bullhorn fa-2x"></i><span>SAP Transaction</span></a></li>
      <li><a href="<?php echo base_url(); ?>announce/"><i class="fa fa-bullhorn fa-2x"></i><span>Mailbox</span></a></li>
    <?php }else{ ?>
      <li class="active"><a href="<?php echo base_url(); ?>announce/"><i class="fa fa-bullhorn fa-2x"></i><span>Content Management</span> </a> </li>
      <li><a href="<?php echo base_url(); ?>bidding/"><i class="fa fa-bullhorn fa-2x"></i><span>Bidding Management</span> </a> </li>
      <li><a href="<?php echo base_url(); ?>sourcing/"><i class="fa fa-bullhorn fa-2x"></i><span>Sourcing Management</span> </a> </li>
      <!-- li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
      <!-- i class="fa fa-info-circle fa-2x"></i><span>CMS</span> <b class="caret"></b></a>  -->
        <!-- ul class="dropdown-menu" data-toggle="dropdown"> -->
        <ul class="dropdown-menu first-node">
          <li class="menu-item dropdown dropdown-submenu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Content</a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo base_url(); ?>cms/">Home</a></li>
              <li><a href="<?php echo base_url(); ?>cms/contact/">Contact</a></li>
              <!-- li><a href="<?php //echo base_url(); ?>cms/email">Email</a></li> -->
              <li><a href="<?php echo base_url(); ?>cms/faq/">FAQ</a></li>
              <li><a href="<?php echo base_url(); ?>cms/aboutus/">About Us</a></li>
            </ul>
          </li>
        </ul>
      </li>
      <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-wrench fa-2x"></i><span>Master Data</span> <b class="caret"></b></a>
          <ul class="dropdown-menu first-node">
          	  <li class="menu-item dropdown dropdown-submenu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Company</a>
                <ul class="dropdown-menu">
                  <li><a href="<?php  echo base_url(); ?>country/">Country</a></li>
                  <li><a href="<?php  echo base_url(); ?>region/">Region</a></li>
                  <li><a href="<?php  echo base_url(); ?>offices/">Offices</a></li>
                  <li><a href="<?php  echo base_url(); ?>department/">Department</a></li>
                </ul>
              </li>
          	  <li class="menu-item dropdown dropdown-submenu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Users</a>
                <ul class="dropdown-menu">
                  <li><a href="<?php  echo base_url(); ?>user/">Manage Users</a></li>
                  <li><a href="<?php  echo base_url(); ?>user/groups/">Groups</a></li>
                  <li><a href="<?php  echo base_url(); ?>user/permissions/">Permissions</a></li>
                  <li><a href="<?php  echo base_url(); ?>level/">Level</a></li>
                </ul>
              </li>
              <li class="menu-item dropdown dropdown-submenu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Category</a>
                <ul class="dropdown-menu">
                  <!-- li><a href="<?php  echo base_url(); ?>category/supplier_category">Vendor Category</a></li> -->
                  <li><a href="<?php  echo base_url(); ?>category/">Category</a></li>
                  <li><a href="<?php  echo base_url(); ?>category/subcategory/">Sub Category</a></li>
                </ul>
              </li>
              <li class="menu-item dropdown dropdown-submenu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Others</a>
                <ul class="dropdown-menu">
                  <li><a href="<?php  echo base_url(); ?>reg_status/">Status Register</a></li>
                  <li><a href="<?php  echo base_url(); ?>content/">Content Type</a></li>
                  <li><a href="<?php  echo base_url(); ?>content/subcontent/">Sub Content Type</a></li>
                  <li><a href="<?php  echo base_url(); ?>document_req/">Document Requirement</a></li>
                  <!--li><a href="#">Offices</a></li-->
                </ul>
              </li>
              <li class="menu-item dropdown dropdown-submenu">
              	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Web</a>
              	<ul class="dropdown-menu">
	              <li><a href="<?php echo base_url(); ?>cms/">Home</a></li>
	              <li><a href="<?php echo base_url(); ?>cms/contact/">Contact</a></li>
	              <li><a href="<?php echo base_url(); ?>cms/faq/">FAQ</a></li>
	              <li><a href="<?php echo base_url(); ?>cms/aboutus/">About Us</a></li>
                  <li><a href="<?php  echo base_url(); ?>policy_agreement/">Vendor Policy</a></li>
              	</ul>
              </li> 
          </ul>
      </li>   
      
      <li><a href="<?php echo base_url(); ?>vendor/">
      	<i class="glyphicon glyphicon-folder-open"></i><span>Vendor Management</span> </a></li>
      <?php } ?>
    </ul>
  </div>  
</div>
</div>

<!-- /subnavbar -->
<div class="main">
<div class="main-inner">
  <div class="container">
    
   

   

  
