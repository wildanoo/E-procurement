<?php 
class m_menu extends CI_Model{
 function __construct(){
	parent::__construct();
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	$this->load->database();
	$this->load->model('crud');
 }
	
 function is_authorize($groups){
	$groups     = explode(",",$groups); //print_r($groups); exit;
	$user_group = $this->session->userdata('user_group');	
	if(in_array($user_group,$groups)) {	return true;
	} return false;
 	
 } 
 
 function classified($level){
	if($level=="3"){
		$table      = "menu3";
		$select     = "id,id_up,menu3,url,jid";
		$list_menu  = array('id'=>'id','id_up'=>'id_up','menu_name'=>'menu3','url_menu'=>'url','jid'=>'jid');
	} elseif($level=="2"){
		$table      = "menu2";
		$select     = "id,id_up,menu2,url,class";
		$list_menu  = array('id'=>'id','id_up'=>'id_up','menu_name'=>'menu2','url_menu'=>'url','class'=>'class');
	} elseif($level=="1"){
		$table     = "menu1";
		$select    = "id,menu1,url,class";
		$list_menu = array('id'=>'id','menu_name'=>'menu1','url_menu'=>'url','class'=>'class');	}	
	
	$result = array('table'=>$table,'select'=>$select,'list_menu'=>$list_menu);
 	return $result;
 } 
 
 function get_data($level){
 	
 	$field     = $this->classified($level);
	$featmenu  = $this->crud->browse("","menu_feature","level",$level,"true");	
 	foreach($featmenu as $row){ 		  		
 		if($this->is_authorize($row->groups)){
 			$menu    = $this->crud->browse("",$field['table'],"id",$row->id_menu,"false",$field['select']); 
			foreach($field['list_menu'] as $key=>$val){ $list[$menu->id][$key] = $menu->$val; }	    
 		} 
 	}
 	
 	return $list; 	
 }
 
 function list_menu(){
 	$main = $this->get_data("1"); $sub2 = $this->get_data("2"); 
 	$sub3 = $this->get_data("3");	

	foreach($sub3 as $row3){		
		$key = $row3['id_up']; $is_exist = $sub2[$key];		
		if(!$is_exist) {
			$select = "id,id_up,menu2 as menu_name,url as url_menu,class";
			$menu   = $this->crud->browse("","menu2","id",$key,"false",$select); 
			$sub2[$menu->id] = (array)$menu;
		}		
		$sub2[$key]['submenu'][] = $row3;					
	}
	
	foreach($sub2 as $row2){		
		$key = $row2['id_up']; 	$is_exist = $main[$key];
		if(!$is_exist) {
			$select = "id,menu1 as menu_name,url as url_menu,class";
			$menu   = $this->crud->browse("","menu1","id",$key,"false",$select); 
			$main[$menu->id] = (array)$menu;
		}		
		$main[$key]['submenu'][] = $row2;					
	}	
 	//echo count($main);
 	return $main; 	
 }

}	
?>