<script>
	$(function(){
		var baseUrl   = "<?php echo base_url(); ?>";
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token     = '<?php echo $this->security->get_csrf_hash(); ?>';

		var id = $(this).find('.venid').first().attr('value');
		var v_url = baseUrl+'vendorlist/category_hover';

		$('#checkall').click(function(){
			$(this).closest('table').find(':checkbox').prop('checked', this.checked);
		});
	});

	function export_excel(id){
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token     = '<?php echo $this->security->get_csrf_hash(); ?>';
		var baseUrl = "<?php echo base_url() ?>";
		var exportex = baseUrl+'vendorlist/export_excel/'+id;
		$.post(exportex, { csrf: token , id: id }, function(){

		});
	}

</script>
<table id="v_tab" class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
		<tr>
			<th>No</th>
			<th><input type="checkbox" id="checkall" value="1"></th>
			<th>Registration Num.</th>
			<th>Vendor Num.</th>
			<th width="200">Vendor Name</th>
			<th width="300">Category - Subcategory</th>
			<th>Type</th>
			<th>Vendor Status</th>
			<th>Remark</th>
			<th>Created Date</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody id="v_data">
		<?php
		$i=1;
		$detail_category = '';
		foreach($browse as $row){
			$process = $this->m_vendorlist->vend_process($row->id);
			if($process == 'blacklist'){ $remark = "<span class='label label-inverse'>Waiting for ".$process."</span>";}
			else if($process == 'redlist'){ $remark = "<span class='label label-danger'>Waiting for ".$process."</span>";}
			else if($process == 'active'){ $remark = "<span class='label label-success'>Waiting for ".$process."</span>";}
			else if($process == 'inactive'){ $remark = "<span class='label label-warning'>Waiting for ".$process."</span>";}
			else if($process == 'delete'){ $remark = "<span class='label label-default'>Waiting for ".$process."</span>";}
			else if($process == 'deletenow'){ $remark = "<span class='label label-default'>Ready for delete</span>";}
			else{$remark ="<span class='label label-primary'>Normal</span>";}
			foreach ($detail as $key => $value){
				if($row->id == $key){
					$detail_category = $value;
				}
			}
			?>
			<tr>
				<td><?php echo $num = $num +1 ?></td>
				<td><input type="checkbox" class="ck_ex" name="ck_list[]" id="ck<?=$i?>" value="<?=$row->id ?>">
					<input type="hidden" name="idsubcat[]" value="<?php echo $row->id_subcat ?>">
				</td>
				<td><?php echo $row->register_num ?></td>
				<td><?php if($row->vendor_num) echo $row->vendor_num; else echo "-"; ?></td>
				<td><?php echo $row->vendor_name ?></td>
				<td><?php echo $detail_category; ?></td>
				<td><?php if($row->vendor_type == 'avl'){echo strtoupper($row->vendor_type);}else{echo ucfirst($row->vendor_type);} ?></td>
				<td><?php echo ucfirst($row->status) ?></td>
				<td><?php echo $remark; ?></td>
				<td><?php echo date('M d, Y',strtotime($row->approved_date)); ?></td>
				<td>
					<div class="btn-group dropup">
						<button class="btn btn-sm btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action <span class="caret"></span></button>
						<ul class="dropdown-menu" role="menu">
							<?php  if($this->permit->vlbas){?><li><a href="<?php echo base_url().'vendorlog/search/'.$row->id; ?>">View Log</a></li><?php  }?>
							<li><a href="<?php echo base_url('register/set_vendor_session/'.$row->id); ?>">View Detail</a></li>

							
							<?php  if($this->permit->vlbas){?>
								<?php if((($row->vendor_type == 'avl') && ($row->status == 'active')) || (($row->vendor_type == 'shortlist') && ($row->status == 'active')))
								{ if($row->inaStat != 'request'){?>
									<li><a href="javascript:void(0)" onclick="inactive(<?= $row->id ?>)" >Inactive</a></li>
									<?php } }?>

									<?php if((($row->vendor_type == 'avl') && ($row->status == 'inactive')) || (($row->vendor_type == 'shortlist') && ($row->status == 'inactive')))
									{ if($row->inaStat != 'request'){?>
										<li><a href="javascript:void(0)" onclick="active(<?php echo $row->id ?>)">Active</a></li>
										<?php  }}?>

										<?php if($row->vendor_type == 'shortlist' && $row->status == 'active'){?><li><a href="javascript:void(0)" onclick="prom_avl(<?php echo $row->id ?>)" ;>Promote to AVL</a></li><?php  } ?>
										<?php if($row->delStat == NULL && $row->status == 'inactive'){ ?><li><a href="javascript:void(0)" onclick="delete_ven(<?php echo $row->id ?>)">Request Delete</a></li><?php } ?>
										<?php if($row->delStat == 'approved' || $row->delStat == 'start'){?><li><a href="<?php echo base_url('vendordelete/detail/'.$row->id); ?>">Delete Vendor</a></li><?php } ?>
										<li><a href="<?php echo base_url() .'vendorlist/export_excel/'. $row->id ?>">Export to excel</a></li>
										<?php } ?>
									</ul>
								</div>
							</td>
						</tr>
						<?php
						$i++;
					}
					?>
					<tr>
						<td colspan="11" style="text-align: center;"><?php echo $pagination ; ?></td>
					</tr>
				</tbody>
			</table>