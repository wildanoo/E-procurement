<?php $this->load->view('notifications/header');?>

<br>Dengan hormat,<br>
Bapak/Ibu <?php echo $data_recipients->fullname;?> / <?php echo $data_recipients->dept_name;?><br>
<br>
Berikut kami sampaikan konten yang akan kami dipublish, dimohon Admin Asyst untuk membantu dalam <i>English Completion</i>.<br>
<br>
<table border="0">
<tr>
<td style="width:150px">Judul</td>
<td>: <?php echo $data->subcontent_type!='2'?$data->title_ind:$data->title_eng;?></td>
</tr>
<tr> <td>Jenis konten</td>
<td>: <?php echo $data_subcontents[$data->subcontent_type];?></td>
</tr>
<tr>
<?php
$date=date_create($data->publish_date);
?>
<td>Tanggal Publish</td>
<td>: <?php echo date_format($date,"d F Y").' Pukul. '.date_format($date,"H:i");?></td>
</tr> 
</table>
<br>
Belum dapat disetujui dan mohon untuk diperbaiki sesuai catatan di bawah ini:<br>
<br> 
<?php echo $remarks_reject;?><br>
<br>
<a href="<?php echo $link;?>">Link</a><br>
<br> 
Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br>
<br> 
<?php echo $sender;?>.
    
<?php $this->load->view('notifications/footer');?>