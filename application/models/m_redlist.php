<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_redlist extends CI_Model {
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function is_duplicate($venID)
	{
		$id = $this->db->get_where('vendor_redlist', array('id_vendor'=>$venID) )->row()->id;

		if(!empty($id))
		{

				$where = "id_redlist = $id";
				$check = $this->crud->browse("","redlist_docs","","","true","",$where);

				if(!empty($check))
				{
					foreach ($check as $key => $value) 
					{
					$path = "./uploads/redlist/".$value->filename;
					
					unlink($path);
					$this->crud->delete("","redlist_docs","id",$value->id);
					}
				}

				$this->db->delete('vendor_redlist', array('id'=>$id)) ;
		}

	}

	function get_vendor()
	{
		$this->db->where(array('vendor_type'=>'avl'));
		$this->db->where_not_in('status', array('redlist','blacklist'));
		return $q = $this->db->get('vendor')->result();
	}

	function create_zip($files = array(),$destination = '',$overwrite = false) {
			//if the zip file already exists and overwrite is false, return false
		if(file_exists($destination) && !$overwrite) { return false; }
		//vars
		$valid_files = array();
		//if files were passed in...
		if(is_array($files)) {
		//cycle through each file
			foreach($files as $file) {
		//make sure the file exists
				if(file_exists($file)) {
					$valid_files[] = $file;
				}
			}
		}
		//if we have good files...
		if(count($valid_files)) {
		//create the archive
			$zip = new ZipArchive();
			if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
				return false;
			}
		//add the files
			foreach($valid_files as $file) {
				$zip->addFile($file,$file);
			}
		//debug
		//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

		//close the zip -- done!
			$zip->close();

		//check to make sure the file exists
			return file_exists($destination);
		}
		else
		{
			return false;
		}
	}

	function authenticate(){      	
		$allowed = array(56);	
		$permit  = $this->session->userdata('permissions');
		$permit  = explode(",",$permit);
		foreach($permit as $str){
			$permissions[] = preg_replace("/[^0-9]/","",$str);}
			$result        = array_intersect($allowed, $permissions);

			if( $this->tank_auth->is_logged_in() 
				&& $result ){ return true;
			} return false;
	}

	function create_log($table,$data)
	{
		$this->db->insert($table,$data);
	}

	function get_group_id($userID)
	{
		$q = $this->db->get_where('users_groups', array('user_id'=>$userID) );
		return $q->row()->group_id;
	}

	function is_exist($db,$table,$field,$where){
		if(!$db)$db="db";
		$this->$db->db_select();

		foreach($where as $key=>$value){	
			$param[]  = " $key='$value'";									
		}

		if(count($param)>1){
			$AND  = implode(" AND ",$param);   unset($param);
		} else { $AND = $param[0]; }

		$state  = "WHERE $AND ";
	$query  = "SELECT EXISTS(SELECT $field FROM $table $state) as jum ";  		//echo $query; exit;
	$result =  $this->$db->query($query); 
	$hasil  =  $result->row()->jum;	
	// echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>';
	if ($hasil > 0){ return true;
	} else {  return false;  }	
	
	}

	function create_session_link($require){
 	
 	$expired = date('Y-m-d H:i:s', strtotime("+3 days")); $session = md5(uniqid());
 	$insert  = array( 'token'		 => $session,
					  'email'    	 => $require->email,
					  'username'     => $require->username,						 
					  'request'      => $require->request,
					  'parameter'    => $require->param,					  
					  'expired_date' => $expired,
					  'status' 		 => "1",
					  'created_id'   => $this->tank_auth->get_user_id(),
					  'created_date' => date('Y-m-d H:i:s'),
					  'last_updated' => date('Y-m-d H:i:s'));
					  
	$this->crud->insert("","session_notification",$insert);				
	$result   = base_url()."authenticate/session/".$session;	
	
	return $result;							  
 }  

}

/* End of file m_redlist.php */
/* Location: ./application/models/m_redlist.php */