<script type="text/javascript"> 
	$(function(){
		var baseUrl   = "<?php echo base_url(); ?>";
		
		$('#btn_check').click(function(){  
	   		var reg_num = $('#reg_num').val(); 
	   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	            $.ajax({
	               url  : baseUrl + 'register/register_validation',
	               type : "POST",              		               
	               data : csrf +'='+ token +'&reg_num='+reg_num,
	               success: function(resp){      	                	  
	               	  var json = $.parseJSON(resp);
	               	  if(json.status=="false"){
					  	 $("#msg").html(json.msg);
					  } else { 	
					  	$.post( baseUrl + 'register/set_default_vendor', { csrf: token, reg_num: reg_num })
					  		.done(function( resp2 ) {
						    document.location.href= baseUrl + 'register/status';	 
						});
					  }

	               }
	            });
	    }); 

}); 
</script>

<div id="debug"></div>

<?php echo form_open('#',array('id'=>'form_check')); ?>

Register Number : <input type="text" name="reg_num" id="reg_num" class="form-control" placeholder="Register Number"/><br/>
<div id="msg" style="color: red;"></div>
<button type="button" id="btn_check" class="btn-sm btn-info">Check</button>

<?php echo form_close(); ?>
				   			      	