<script>
	$(function(){
		$('#idsubcat').change(function(){
			$('#form_search').submit();
		});

		$('#form_search').submit(function(){
			var sdata = $(this).serialize();
			var v_url = baseUrl+'active/sorting';
			$.post(v_url, sdata, function(data){
				$('#v_tab').html(data);
			});
		});

		$('#start_date, #end_date').datepicker({
			changeMonth: true, changeYear: true, dateFormat:"yy-mm-dd"
		});

		$('#excelbtn').click(function(){
				var fields = $("input[name='ck_list[]']").serializeArray(); 
				if (fields.length === 0){ 
					alert('Select one or more data!'); 
					return false;
				}else{ 
					$('#myform').submit();
				}
			});
	});

	function form_ina_app(id){
		$("#main_container").load(baseUrl+'active/ina_approve_form/'+id);
	}
</script>
<div id="main_container">
	<div class="page-header">
		<h3>Active Vendor</h3>
	</div>
	<?php echo form_open('',array('id'=>'form_search','novalidate'=>'true','onsubmit'=>'return false')); ?>
	<div class="table">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-search"></i> <strong>Search Section</strong>
			</div>
			<style type="text/css">
				.list-inline li{
					vertical-align: middle !important;
				}
				.btn-srch{
					margin-bottom: 0;
				}
			</style>
			
			<div class="panel-body">
				<ul class="list-inline">
					<li style="width:24%">
						<div class="input-group">
							<input type="date" value="" placeholder="date start" class="form-control date_picker" id="start_date" name="start_date">
							<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
						</div>
					</li>
					<li style="width:2%">to</li>
					<li style="width:24%">
						<div class="input-group">
							<input type="date" value="" placeholder="date end" class="form-control date_picker" id="end_date" name="end_date">
							<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
						</div>
					</li>
					<li style="width:34%"><div class="input-group" style="width:100%"><input placeholder="search by vendor name..." type="text" value="" class="form-control" id="search_term" name="search_term"></div></li>
					<li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search" id="searchbtn">Search</button></div></li>
					<li style="width:7%"><div class="input-group"><a href="<?php echo base_url() ?>active" class="btn btn-srch btn-warning btn-sm">View All</a></div></li>
				</ul>
			</div>
			
		</div>
	</div>
	<hr>

	

	<div class="col-md-3">
		<select name="subcat" id="idsubcat" class="form-control">
			<option value="all">- All Subcategories -</option>
			<?php 
			$idcategory = '';
			foreach($subcat as $sc){ if($idcategory != $sc->id){?>
					<optgroup label="<?php echo $sc->category ?>"><?php } ?>
					<option  value="<?php echo $sc->idsub  ?>"<?php if($sc->idsub==$this->session->userdata('id')){echo "selected";} ?> ><?php echo $sc->subcategory; ?></option>
				<?php $idcategory = $sc->id;} ?>
			</select>
		</div>

		<?php echo form_close(); ?>
		<?php echo form_open('active/export_excel',array('id'=>'myform')); ?>
		<button class="btn btn-success pull-right" type="button"  id="excelbtn" title="Export Vendor List"><i class="fa fa-file-excel-o" style="color:#FFFFFF"></i>Export Excel</button>

		<!-- tabelcontent -->
		<?php require_once('v_data.php'); ?>

		<?php echo form_close(); ?>


	</div>