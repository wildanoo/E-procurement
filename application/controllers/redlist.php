<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Redlist extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('m_vendor','m_announce','m_user'));	islogged_in();
		$this->load->model('m_redlist');
		$this->permit = new stdClass();
		$this->permit = $this->crud->get_permissions("3");

	}

	function index()
	{
		if ($this->m_redlist->authenticate())
		{
			$this->kill_search();

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			$data['view'] = "vendor/redlist/home";
	 		$this->load->view('layout/template',$data);
		}
		else
		{
			$this->session->set_flashdata('message','user not authorized');
			redirect('/auth/login/');
		}
	}


	function browse()
	{
		$this->kill_search();

		$this->session->set_userdata('feature', 'redlist');
		$user_id = $this->tank_auth->get_user_id();

 		$table 	     = "vendor_redlist a";
 		$select 	 = "a.id AS id_redlist,a.start_date,a.remark,a.status as red_state,a.counter, b.*, (select COUNT(*) from redlist_docs where id_redlist=a.id) as doc_num ";
 		$order 		 = array('field'=>'a.start_date','order'=>'DESC');
 		$joins[0][0] = 'vendor b';
		$joins[0][1] = 'a.id_vendor = b.id';
		$joins[0][2] = 'left';

		$page       = $this->uri->segment(3);
		$per_page   = 10;
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table,""); //print_r($where);
		$set_config = array('base_url'=>base_url().'redlist/browse','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();

		$order 			     = array('field'=>'a.created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;
		
		
		$browse = $this->crud->browse_join_with_paging("",$table,"","","true",$select,$joins,$where,$order,$per_page,$offset,"");

		$data['title']    = 'Redlist';
		$data['browse']   = $browse;

 		$data['view'] = "vendor/redlist/browse";

		$select2 	 = "a.id AS idcat, a.category, b.id AS idsub, b.subcategory";
		$order2 	 = array('field'=>'a.id','order'=>'ASC');
		$joins2 	 = array();
		$joins2[0][0]= 'category a';
		$joins2[0][1]= 'a.id = b.id_cat';
		$joins2[0][2]= 'left';

		$data['category'] = $this->crud->browse_join($db="","subcategory b","a.status","1","true",$select2,$joins2,$where="",$order2,$group_by="",$like="");


 		if($this->input->is_ajax_request())
 		{
	 		$this->load->view($data['view'], $data);
 		}
 		else
 		{
 			$this->load->view('layout/template',$data);
 		}

	}



	function browse_logs()
	{
		$user_id = $this->tank_auth->get_user_id();
		$red_id = $this->input->post('red_id');
		$ven_id = $this->input->post('ven_id')?$this->input->post('ven_id'):$this->session->userdata('ven_id');

		$this->session->set_userdata( array('ven_id'=>$ven_id) );
 		$table 	     = "vendor_logs a";
 		$select 	 = "*,a.created_date as  log_date, (select c.username from users c where c.id=a.user_id) as username, (select d.name from groups d where d.id=a.group_id) as group_name ";
 		$order 		 = array('field'=>'a.created_date','order'=>'DESC');
 		$joins[0][0] = 'vendor b';
		$joins[0][1] = 'a.id_vendor = b.id';
		$joins[0][2] = 'left';
		if(!empty($ven_id))
		{
			$where = array('a.id_vendor'=>$ven_id);
		}
		$page       = $this->uri->segment(3);
		$per_page   = 10;
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table,$where); //print_r($where);
		$set_config = array('base_url'=>base_url().'redlist/browse_logs','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();

		$order 			     = array('field'=>'a.created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;

		$browse = $this->crud->browse_join_with_paging("",$table,"","","true",$select,$joins,$where,$order,$per_page,$offset,"");

		$data['title']    = 'Redlist Logs';
		$data['browse']   = $browse;

 		$data['view'] = "vendor/redlist/data_logs";

 		if($this->input->is_ajax_request())
 		{
	 		$this->load->view($data['view'], $data);
 		}
 		else
 		{
 			$this->load->view('layout/template',$data);
 		}

	}

	function form_create()
	{
		$id = $this->uri->segment(3);
		if(!empty($id))
		{
			$table 	    = "vendor_redlist a";
	 		$select 	= "a.id AS id_redlist,a.start_date,a.remark,a.status as red_state,a.counter,a.doc_name,a.doc_type, b.* ";
	 		$order 		 = array('field'=>'a.start_date','order'=>'DESC');
	 		$joins[0][0] = 'vendor b';
			$joins[0][1] = 'a.id_vendor = b.id';
			$joins[0][2] = 'left';

			$redlist = $this->crud->browse_join("",$table,"a.id",$id,"false",$select,$joins,$where,"","","");
			$redlist_docs = $this->db->get_where('redlist_docs', array('id_redlist'=>$id))->result();
	 		$data = array('redlist' => $redlist, 'redlist_docs' => $redlist_docs );
		}

		$data['vendor'] = $this->m_redlist->get_vendor();
 		$this->load->view('vendor/redlist/form_create',$data);
	}

	function create()
	{
		$this->m_redlist->is_duplicate($this->input->post('ven_id'));

		$curr_date 	= date('Y-m-d H:i:s');
	 	$userID     = $this->tank_auth->get_user_id();
	 	$data 		= array('id'			=> null,
						 	'id_vendor'		=> $this->input->post('ven_id'),
						 	'start_date'	=> $this->input->post('start_date'),
						 	'remark'		=> $this->input->post('remark'),
						 	'counter'		=> $this->input->post('counter'),
						 	// 'doc_name'		=> $filename,
						 	// 'doc_type'		=> $file_type,
						 	'status'		=> ($this->input->post('status')) ? ($this->input->post('status')) : 'wait' ,
						 	'created_id'	=> $userID,
						 	'created_date'	=> $curr_date,
						 	'last_updated'	=> $curr_date);

	 	$this->crud->insert("","vendor_redlist",$data);
	 	$id_redlist = $this->db->insert_id();

		$path  = './uploads/redlist/'; 
		$data_files  = $this->m_vendor->arrange_files($_FILES); 
		$i=1;
	 	foreach($data_files as $key3=>$file)
	 	{
	 		$_FILES['userfile'] = $file;
	 		$ext = explode(".",$file['name']); 
	 		$ext = $ext[1];	
			$filename = strtolower(date('YmdHis').$file['name']); 
			$savename = str_replace(' ', '_', $filename ); 

			$files[] = $filename;
			$config   = $this->m_vendor->set_config($filename,$path,$ext); 		 
	 	 	$this->load->library('upload', $config);
	 	 	$this->upload->initialize($config); 
	 	 	
	 	 	$msg['error'] = "";  $msg['success']   = array();		
			if ( ! $this->upload->do_upload())
			{
				$msg['error']   = $this->upload->display_errors();
			}else
			{
				$msg['success'] = $this->upload->data(); 
				$data2  = array('id'=>null, 'id_redlist'=>$id_redlist, 'filename'=>$savename, 'status'=>1);
				$this->crud->insert("","redlist_docs",$data2);
			}	 

			$i++; 
		}

		$logs 		= array('id'	=> null,
					 	'id_vendor'		=> $this->input->post('ven_id'),
					 	'user_id'		=> $userID,
					 	// 'group_id'		=> $this->m_redlist->get_group_id($userID),
					 	'description'	=> 'add vendor to redlist',
					 	'reason'		=> $this->input->post('remark'),
					 	'status'		=> 1,
					 	'action'		=> 'redlist',
					 	'created_date'	=> $curr_date );
			$this->m_redlist->create_log('vendor_logs', $logs);

			// send mail form admin src to sm ibs
			$this->sendMailRedlist($data);
			redirect('redlist');


	}


	function update()
	{
		$id = $this->input->post('red_id');
		$del_file = $this->input->post('deleted_file');
		$del_file = implode( ", ", $del_file);
		if(!empty($del_file))
		{

				$where = "id_redlist = $id AND id IN($del_file)";
				$check = $this->crud->browse("","redlist_docs","","","true","",$where);

				if(!empty($check))
				{
					foreach ($check as $key => $value) 
					{
					$path = "./uploads/redlist/".$value->filename;
					
					unlink($path);
					$this->crud->delete("","redlist_docs","id",$value->id);
					}
				}
		}

		// exit();
		$curr_date 	= date('Y-m-d H:i:s');
	 	$userID     = $this->tank_auth->get_user_id();
	 	$data_upd 		= array(
						 	'id_vendor'		=> $this->input->post('ven_id'),
						 	'start_date'	=> $this->input->post('start_date'),
						 	'remark'		=> $this->input->post('remark'),
						 	'counter'		=> $this->input->post('counter'),
						 	'status'		=> ($this->input->post('status')) ? ($this->input->post('status')) : 'wait' ,
						 	'created_id'	=> $userID,
						 	'created_date'	=> $curr_date,
						 	'last_updated'	=> $curr_date);
	 	$this->crud->update("","vendor_redlist","id",$id, $data_upd);

		$path  = './uploads/redlist/'; 
		$data  = $this->m_vendor->arrange_files($_FILES); 
		$i=1;
	 	foreach($data as $key3=>$file)
	 	{
	 		$_FILES['userfile'] = $file;
	 		$ext      = explode(".",$file['name']); 
	 		$ext = $ext[1];	
			$filename = strtolower(date('YmdHis').$file['name']);
			$savename = str_replace(' ', '_', $filename );  
			$files[] = $filename;
			$config   = $this->m_vendor->set_config($filename,$path,$ext); 		 
	 	 	$this->load->library('upload', $config);
	 	 	$this->upload->initialize($config); 
	 	 	
	 	 	$msg['error'] = "";  $msg['success']   = array();		
			if ( ! $this->upload->do_upload())
			{
				$msg['error']   = $this->upload->display_errors();
			}else
			{
				$msg['success'] = $this->upload->data(); 
				$data2  = array('id'=>null, 'id_redlist'=>$id, 'filename'=>$savename, 'status'=>1);
				$this->crud->insert("","redlist_docs",$data2);
			}	 

			$i++; 
		}

		

			//create logs
			$logs 		= array('id'	=> null,
					 	'id_vendor'		=> $this->input->post('ven_id'),
					 	'user_id'		=> $userID,
					 	// 'group_id'		=> $this->m_redlist->get_group_id($userID),
					 	'description'	=> 'update data redlist vendor',
					 	'reason'		=> $this->input->post('remark'),
					 	'status'		=> 1,
					 	'action'		=> 'redlist',
					 	'created_date'	=> $curr_date );
			$this->m_redlist->create_log('vendor_logs', $logs);
			redirect('redlist');


	}


	function create_old()
	{
		$id = $this->input->post('red_id');
		$upload_file = $_FILES['userfile']['name'];
		$file_type = $this->input->post('doc_type');
		$filename = $this->input->post('doc_name');
		if($upload_file)
		{
			if(empty($filename))
			{
				$filename = 'redlist_'.$this->input->post('ven_id').date('YmdHis');
			}

			$file_type = $this->m_vendor->get_file_type($_FILES);

			$path = "./uploads/redlist/".$this->input->post('doc_name').'.'.$this->input->post('doc_type');
			if(file_exists($path)) unlink($path);
		}

		$curr_date 	= date('Y-m-d H:i:s');
	 	$userID     = $this->tank_auth->get_user_id();
	 	$data 		= array('id'			=> null,
						 	'id_vendor'		=> $this->input->post('ven_id'),
						 	'start_date'	=> $this->input->post('start_date'),
						 	'remark'		=> $this->input->post('remark'),
						 	'counter'		=> $this->input->post('counter'),
						 	'doc_name'		=> $filename,
						 	'doc_type'		=> $file_type,
						 	'status'		=> ($this->input->post('status')) ? ($this->input->post('status')) : 'wait' ,
						 	'created_id'	=> $userID,
						 	'created_date'	=> $curr_date,
						 	'last_updated'	=> $curr_date);


		$where    = array('doc_name'=>$filename,'id_vendor' => $data['id_vendor']);
 		if(!empty($id))
 		{
 			$where += array('id !' => $id);
 		}

 		$is_exist = $this->m_redlist->is_exist("","vendor_redlist","id",$where);

 		if($_FILES && !$is_exist)
		{

	 	    $where3    = array('type'=>$file_type);
	 	    $is_exist3 = $this->crud->is_exist("","file_type","id",$where3);
	 	    if(!$is_exist3) $this->crud->insert("","file_type",array('type'=>$file_type));
	 	    $ftype = $this->crud->browse("","file_type","","","true","type");
	 	    foreach($ftype as $val){ $type[] =  $val->type;	}
	 	    $type = implode("|",$type);
	 		$filename =	$filename.".".$file_type;
	 		$path 	  = './uploads/redlist/';
	 		$config   = $this->m_announce->set_config($filename,$path,$type);
	 		$config['allowed_types'] = '*';
	 		$this->load->library('upload', $config);
	 		$this->upload->initialize($config);

	 		$msg['error']     = array();
	 		$msg['success']   = array();
			if ( ! $this->upload->do_upload()){
			$msg['error']   = $this->upload->display_errors();
			} else {
			$msg['success'] =' $this->upload->data()'; }
	 	}


		if(empty($id))
		{
			$cek = $this->crud->insert("","vendor_redlist",$data);
			//create logs
			$logs 		= array('id'	=> null,
					 	'id_vendor'		=> $this->input->post('ven_id'),
					 	'user_id'		=> $userID,
					 	// 'group_id'		=> $this->m_redlist->get_group_id($userID),
					 	'description'	=> 'add vendor to redlist',
					 	'reason'		=> $this->input->post('remark'),
					 	'status'		=> 1,
					 	'action'		=> 'redlist',
					 	'created_date'	=> $curr_date );
			$this->m_redlist->create_log('vendor_logs', $logs);
			// send mail form admin src to sm ibs
			$this->sendMailRedlist($data);

		}
		else
		{
			unset($data['id']);

			$cek = $this->crud->update("","vendor_redlist","id",$id, $data);

			//create logs
			$logs 		= array('id'	=> null,
					 	'id_vendor'		=> $this->input->post('ven_id'),
					 	'user_id'		=> $userID,
					 	// 'group_id'		=> $this->m_redlist->get_group_id($userID),
					 	'description'	=> 'update data redlist vendor',
					 	'reason'		=> $this->input->post('remark'),
					 	'status'		=> 1,
					 	'action'		=> 'redlist',
					 	'created_date'	=> $curr_date );
			$this->m_redlist->create_log('vendor_logs', $logs);
		}


		redirect('redlist');

	}

	function delete()
	{
		$id = $this->input->post('id');
		$doc = $this->crud->browse("", 'vendor_redlist', 'id', $id, 'false', '','','','','');

		if(!empty($id))
		{
			$path = "./uploads/redlist/".$doc->doc_name.'.'.$doc->doc_type;
			if(file_exists($path)) unlink($path);
		}

		$this->crud->delete("","vendor_redlist","id",$id);

	}

	function kill_search()
	{
		$search_vals = array(
			'start_date'=>'',
			'end_date'=>'',
			'term'=>'',
			'subcat'=>'',
			'sess_auth'=>''
		);

		$this->session->set_userdata( $search_vals );
	}

	function search()
	{
		//input
		$start_date = ($this->input->post('start_date') ) ? $this->input->post('start_date') : $this->session->userdata('start_date');
		$end_date = ($this->input->post('end_date'))? $this->input->post('end_date') : $this->session->userdata('end_date');

		$term  = ($this->input->post('search_term'))? $this->input->post('search_term') : $this->session->userdata('term');
		$subcat = ($this->input->post('subcat'))? $this->input->post('subcat') : $this->session->userdata('subcat');

		$search_vals = array('start_date' => $start_date, 'end_date' => $end_date, 'term'=> $term, 'subcat'=>$subcat );
		$this->session->set_userdata($search_vals);

 		$table 	    = "vendor_redlist a";
 		$select 	 = "a.id AS id_redlist,a.start_date,a.remark,a.status as red_state,a.counter, b.*, (select COUNT(*) from redlist_docs where id_redlist=a.id) as doc_num ";
 		$order 		 = array('field'=>'a.start_date','order'=>'DESC');
 		$joins[0][0] = 'vendor b';
		$joins[0][1] = 'a.id_vendor = b.id';
		$joins[0][2] = 'left';
		$joins[1][0] = 'vendor_category c';
		$joins[1][1] = 'c.id_vendor = b.id';
		$joins[1][2] = 'left';

		if(!empty($subcat))
		{
			if($subcat!='all')
			{
				if(!empty($start_date) && !empty($end_date))
				{
					$where = "c.id_subcat = $subcat and a.start_date between '$start_date' and '$end_date' ";
					if(!empty($term))
					{
						$where .= "and b.vendor_name like '%$term%' or b.vendor_num like '%$term%' ";
					}
				}
				elseif(empty($start_date) && empty($end_date) && !empty($term))
				{
						$where = "c.id_subcat = $subcat and b.vendor_name like '%$term%' or b.vendor_num like '%$term%' ";

				}
				else
				{
					$where = "c.id_subcat = $subcat";
				}
			}
		}

		if(!empty($start_date) && !empty($end_date))
		{
			$where = "a.start_date between '$start_date' and '$end_date' ";
			if(!empty($term))
			{
				$where .= "and b.vendor_name like '%$term%' or b.vendor_num like '%$term%' ";
			}
		}

		if(empty($start_date) && empty($end_date) && !empty($term))
		{
				$where = "b.vendor_name like '%$term%' or b.vendor_num like '%$term%' ";

		}

		$page       = $this->uri->segment(3);
		$per_page   = 10;
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = count($this->crud->browse_join("",$table,"","","true",$select,$joins,$where,$order,'b.id'));
		$set_config = array('base_url'=>base_url().'redlist/search','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();

		$order 			     = array('field'=>'a.created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;

		$sess_auth = $this->session->userdata('sess_auth');
		
		if(!empty($sess_auth))
		{
			$where = array('a.id_vendor'=>$sess_auth);
		}
		
		$browse = $this->crud->browse_join_with_paging("",$table,"","","true",$select,$joins,$where,$order,$per_page,$offset,'b.id');

		$data['title']    = 'Redlist';
		$data['browse']   = $browse;
		$data['id_subcat'] = $subcat;
 		$data['view'] = "vendor/redlist/browse";
 	
		$select2 	 = "a.id AS idcat, a.category, b.id AS idsub, b.subcategory";
		$order2 	 = array('field'=>'a.id','order'=>'ASC');
		$joins2 	 = array();
		$joins2[0][0]= 'category a';
		$joins2[0][1]= 'a.id = b.id_cat';
		$joins2[0][2]= 'left';

		$data['category'] = $this->crud->browse_join($db="","subcategory b","a.status","1","true",$select2,$joins2,"",$order2);
		// echo $this->db->last_query();
 		$this->load->view('layout/template',$data);

	}

	function export_excel()
	{
		$ck = $this->input->post('my_ck');
		$filter = implode(',', $ck);
		$table 	     = "vendor_redlist a";
 		$select      = "a.id AS id_redlist,a.start_date,a.remark,a.status as red_state,a.counter, b.* ";
 		$order 		 = array('field'=>'a.start_date','order'=>'DESC');
 		$joins[0][0] = 'vendor b';
		$joins[0][1] = 'a.id_vendor = b.id';
		$joins[0][2] = 'left';

		$where = "a.id IN($filter)";

		$order  = array('field'=>'a.created_date','order'=>'DESC');

 		if(!empty($ck))
 		{
			$data['redlist'] = $this->crud->browse_join("",$table,"","","true",$select,$joins,$where,$order,"","");
 			$this->load->view('vendor/redlist/excel', $data );
 		}
 		else
 		{
 			redirect('redlist');
 		}

	}

	function form_approve()
	{
		$id = $this->uri->segment(3);
		if(!empty($id))
		{
			$table 	    = "vendor_redlist a";
	 		$select 	= "a.id AS id_redlist,a.start_date,a.remark,a.status,a.counter,a.doc_name,a.doc_type, b.* ";
	 		$order 		 = array('field'=>'a.start_date','order'=>'DESC');
	 		$joins[0][0] = 'vendor b';
			$joins[0][1] = 'a.id_vendor = b.id';
			$joins[0][2] = 'left';

			$redlist = $this->crud->browse_join("",$table,"a.id",$id,"false",$select,$joins,$where,"","","");

	 		$data = array('redlist' => $redlist );
		}

		$data['vendor'] = $this->crud->browse($db="",'vendor','vendor_type','avl',true,"",$where="","","","");
		$data['redlist_docs'] = $this->db->get_where('redlist_docs', array('id_redlist'=>$id))->result();
 		$this->load->view('vendor/redlist/form_approve',$data);
	}

	function form_reject()
	{
		$id = $this->uri->segment(3);
		if(!empty($id))
		{
			$table 	    = "vendor_redlist a";
	 		$select 	= "a.id AS id_redlist,a.start_date,a.remark,a.status,a.counter,a.doc_name,a.doc_type, b.* ";
	 		$order 		 = array('field'=>'a.start_date','order'=>'DESC');
	 		$joins[0][0] = 'vendor b';
			$joins[0][1] = 'a.id_vendor = b.id';
			$joins[0][2] = 'left';

			$redlist = $this->crud->browse_join("",$table,"a.id",$id,"false",$select,$joins,$where,"","","");

	 		$data = array('redlist' => $redlist );
		}

		$data['vendor'] = $this->crud->browse($db="",'vendor','vendor_type','avl',true,"",$where="","","","");
		$data['redlist_docs'] = $this->db->get_where('redlist_docs', array('id_redlist'=>$id))->result();
 		$this->load->view('vendor/redlist/form_reject',$data);
	}

	function approve()
	{
 		$id     = $this->input->post('red_id');
	 	$venID  = $this->input->post('ven_id');
	 	$reason = $this->input->post('remark');
	 	$userID = $this->tank_auth->get_user_id();
 		$data 	= array('status'=>'approved');
		$this->crud->update("","vendor_redlist","id",$id,$data);
		$this->crud->update("","vendor","id",$venID,array('status'=>'redlist'));

		//create logs
		$logs 		= array('id'	=> null,
				 	'id_vendor'		=> $this->input->post('ven_id'),
				 	'user_id'		=> $userID,
				 	// 'group_id'		=> $this->m_redlist->get_group_id($userID),
				 	'description'	=> 'approved redlist vendor',
				 	'reason'		=> $this->input->post('remark'),
				 	'status'		=> 1,
				 	'action'		=> 'redlist',
				 	'created_date'	=> date('Y-m-d H:i:s') );
		$this->m_redlist->create_log('vendor_logs', $logs);

		$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
					(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";
 		$cat 	 = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2);

		// NOTIFIKASI PERSETUJUAN VENDOR REDLIST TO Contact Person FROM ADM SRC

		$select  = "register_num as num,vendor_name as name,email";
		$vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);
		$subject =  "Notification Redlist E-procurement Garuda";

		$vendor_mail = $this->db->get_where('users', array('id_vendor'=>$venID) )->result();
		//send_mail to vendor_mail
		foreach ($vendor_mail as $val) 
		{
			if(filter_var($val->email, FILTER_VALIDATE_EMAIL))
			{ 

				$user    = $this->users->get_user_by_email($val->email);
				if($user)
				{			 			
				$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'redlist_detail','param'=>$id);	
					
				$link    = $this->m_redlist->create_session_link((object) $require);
				

				$to   	 =  $val->email;
				$param   =  array('reg_num'		=> $vendor->num,
								  'vendor_name' => $vendor->name,
								  'reason'		=> $reason,
								  'link'		=> $link,
								  'to'			=> $val->username);
				$param['view']  = NOTIF_PATH."EPCGA018"; 		
			 	$message 		= $this->load->view(NOTIF_TMPL,$param,true); 
				$this->crud->sendMail($to,$subject,$message);
				}
			}
		}

		//adm_src
		$adm_src = $this->m_user->get_userdata_group('as');
		foreach($adm_src as $vals)
		{
			if(filter_var($vals->email, FILTER_VALIDATE_EMAIL))
			{ 
				$user    = $this->users->get_user_by_email($vals->email);
				// print_r($user);

				if(!empty($user))
				{			 			
				$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'redlist','param'=>$venID);	
				
				$link    = $this->m_redlist->create_session_link((object) $require);
				
				$to   	 =  $vals->email;
				$param   =  array('reg_num'		=> $vendor->num,
									'vendor_name' => $vendor->name,
									'category'	=> $cat,
									'reason'		=> $reason,
								  	'link'		=> $link,
									'to'			=> $vals->username);
				$param['view']  = NOTIF_PATH."EPCGA019"; 		
		 		$message 		= $this->load->view(NOTIF_TMPL,$param,true);
				$this->crud->sendMail($to,$subject,$message);
				}
			}
		}

		redirect('redlist');

 	}

 	function cancel_redlist()
	{
	 	$red_id  = $this->input->post('red_id');
	 	$venID  = $this->input->post('ven_id');
	 	$userID = $this->tank_auth->get_user_id();
 		$data 	= array('status'=>'cancel');
		$this->crud->update("","vendor_redlist","id",$red_id, $data);

		//create logs
		$logs 		= array('id'	=> null,
				 	'id_vendor'		=> $venID,
				 	'user_id'		=> $userID,
				 	// 'group_id'		=> $this->m_redlist->get_group_id($userID),
				 	'description'	=> 'cancelled redlist vendor',
				 	'reason'		=> 'cancelled redlist vendor',
				 	'status'		=> 1,
				 	'action'		=> 'redlist',
				 	'created_date'	=> date('Y-m-d H:i:s') );
		$this->m_redlist->create_log('vendor_logs', $logs);

		//NOTIFIKASI PEMBATALAN REDLIST KE SM IBS DARI ADM SRC
		$select  = "register_num as num,vendor_name as name,email";
		$vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);
		$subject =  "Notification Redlist E-procurement Garuda";
		//sm_ibs
		$sm_ibs = $this->m_user->get_userdata_group('sm_ibs');

		foreach($sm_ibs as $value)
		{
			if(filter_var($value->email, FILTER_VALIDATE_EMAIL))
			{ 
				$user    = $this->users->get_user_by_email($value->email);
				if($user)
				{			 			
				$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'redlist','param'=>$venID);	
					
				$link    = $this->m_redlist->create_session_link((object) $require);
				
				$to   	 =  $value->email;
				$param   =  array('reg_num'		=> $vendor->num,
								  'vendor_name' => $vendor->name,
								  'category'	=> $cat,
								  'link'		=> $link,
								  'reason'		=> $reason,
								  'to'			=> $value->username);
				$param['view']  = NOTIF_PATH."EPCGA051"; 		
		 		$message 		= $this->load->view(NOTIF_TMPL,$param,true);
				// $message =  $this->load->view('vendor/redlist/notification/form3',$param,true);			//echo $message."<br/><br/>";
				$this->crud->sendMail($to,$subject,$message);
				}
			}
			
		}

 	}

 	function reject()
	{
		$id     = $this->input->post('red_id');
	 	$venID  = $this->input->post('ven_id');
	 	$reason = $this->input->post('remark');
	 	$userID = $this->tank_auth->get_user_id();
 		$data 	= array('status'=>'rejected');
		$this->crud->update("","vendor_redlist","id",$id,$data);

		//create logs
		$logs 		= array('id'	=> null,
				 	'id_vendor'		=> $this->input->post('ven_id'),
				 	'user_id'		=> $userID,
				 	// 'group_id'		=> $this->m_redlist->get_group_id($userID),
				 	'description'	=> 'rejected redlist vendor',
				 	'reason'		=> $this->input->post('remark'),
				 	'status'		=> 1,
				 	'action'		=> 'redlist',
				 	'created_date'	=> date('Y-m-d H:i:s') );
		$this->m_redlist->create_log('vendor_logs', $logs);
		//NOTIF REJECTED VENDOR REDLIST TO ADMIN SRC FROM SM IBS
		$select  = "register_num as num,vendor_name as name,email";
		$vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);
		$subject =  "Notification Redlist E-procurement Garuda";

		$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
					(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";
		$cat 	 = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2);

		//adm_src
		$adm_src = $this->m_user->get_userdata_group('as');
		foreach($adm_src as $value)
		{
			if(filter_var($value->email, FILTER_VALIDATE_EMAIL))
			{ 
				$user    = $this->users->get_user_by_email($value->email);
				if($user)
				{			 			
				$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'redlist','param'=>$venID);	
					
				$link    = $this->m_redlist->create_session_link((object) $require);
				
				$to   	 =  $value->email;
				$param   =  array('reg_num'			=> $vendor->num,
									'vendor_name' 	=> $vendor->name,
									'category'		=> $cat,
									'reason'		=> $reason,
									'link'			=> $link,
									'to'			=> $value->username);
				$param['view']  = NOTIF_PATH."EPCGA020"; 		
		 		$message 		= $this->load->view(NOTIF_TMPL,$param,true);
				// $message =  $this->load->view('vendor/redlist/notification/form4',$param,true);
				$this->crud->sendMail($to,$subject,$message);
				}
			}
		}
		redirect('redlist');

 	}

 	function sendMailRedlist($data)
 	{
 		$venID  = $data['id_vendor'];
 		$reason  = $data['remark'];

		//NOTIFIKASI PERSETUJUAN VENDOR REDLIST TO adm_src FROM IBS

		$select  = "register_num as num,vendor_name as name,email";
		$vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);
		$subject =  "Notification Redlist E-procurement Garuda";

		$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
					(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";
 		$cat 	 = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2);

		//sm_ibs
		$sm_ibs = $this->m_user->get_userdata_group('sm_ibs');
		
		foreach($sm_ibs as $value)
		{
			if(filter_var($value->email, FILTER_VALIDATE_EMAIL))
			{ 
				$user    = $this->users->get_user_by_email($value->email);
				if($user)
				{			 			
					$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'redlist','param'=>$data['id_vendor']);	
					
					$link    = $this->m_redlist->create_session_link((object) $require);
					
					$to   	 =  $value->email;
					$param   =  array('reg_num'		=> $vendor->num,
									  'vendor_name' => $vendor->name,
									  'category'	=> $cat,
									  'reason'		=> $reason,
									  'link'		=> $link,
									  'to'			=> $value->username);

					
					$param['view']  = NOTIF_PATH."EPCGA017"; 		
				 	$message 		= $this->load->view(NOTIF_TMPL,$param,true); 
					$this->crud->sendMail($to,$subject,$message);
				}

			}
		}
 	}

 	function download()
 	{ 
 		$id_redlist = $this->uri->segment(3); 
 		$path  = 'uploads/redlist/'; 
 		// $red_id = $this->uri->segment(3);
 		$data = $this->db->query("select filename from redlist_docs where id_redlist = $id_redlist")->result();
 		if(empty($data))
 		{
 			redirect('redlist');
 			exit();
 		}
 		foreach ($data as $key => $value) 
 		{
 			$data[] = $path.$value->filename;	
 		}
 		$filename = 'archive_redlist.zip';
 		unlink($path.$filename);
 		$this->m_redlist->create_zip($data,'uploads/redlist/'.$filename);

 		$this->load->helper('download');
 		$data = file_get_contents("uploads/redlist/".$filename);  
		force_download($filename, $data); 


 	}

 	function detail_redlist()
 	{
 		$id = $this->uri->segment(3);
 		$table 	    = "vendor_redlist a";
 		$select 	= "a.id AS id_redlist,a.start_date,a.remark,a.status as red_state,a.counter,a.doc_name,a.doc_type, b.* ";
 		$order 		 = array('field'=>'a.start_date','order'=>'DESC');
 		$joins[0][0] = 'vendor b';
		$joins[0][1] = 'a.id_vendor = b.id';
		$joins[0][2] = 'left';

		$data['vendor'] = $this->crud->browse_join("",$table,"a.id",$id,"false",$select,$joins,$where,"","","");
		
 		$data['view'] = "vendor/redlist/detail_index";
	 	$this->load->view('layout/template',$data);
 	}

 	function validate_vendor()
 	{
 		$venID = $this->input->post('id_ven');
 		print $chcek = $this->m_user->is_request_allowed('1', $venID);
 	}

 	



}

/* End of file redlist.php */
/* Location: ./application/controllers/redlist.php */
