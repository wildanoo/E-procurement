<script>
	$(function(){
		var baseUrl   = "<?php echo base_url(); ?>";
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>';
		var token     = '<?php echo $this->security->get_csrf_hash(); ?>';
		});

	function export_excel(id){
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>';
		var token     = '<?php echo $this->security->get_csrf_hash(); ?>';
		var baseUrl = "<?php echo base_url() ?>";
		var exportex = baseUrl+'data_vendor/export_excel/'+id;
		$.post(exportex, { csrf: token , id: id }, function(){

		});
	}

</script>

<?php
$i=1;
$detail_category = '';
foreach($browse as $row){
	foreach ($detail as $key => $value){
		if($row->id == $key){
			$detail_category = $value;
		}
	}
	?>
	<tr>
		<td><?php echo $num = $num +1 ?></td>
		<td><input type="checkbox" class="ck_ex" name="ck_list[]" id="ck<?=$i?>" value="<?=$row->id ?>">
			<input type="hidden" name="idsubcat[]" value="<?php echo $row->id_subcat ?>">
		</td>
		<td><?php echo $row->register_num ?></td>
		<td><?php if($row->vendor_num) echo $row->vendor_num; else echo "-"; ?></td>
		<td><?php echo $row->vendor_name; ?></td>
		<td><?php echo $detail_category ?></td>
		<td><?php echo $row->vendor_type ?></td>
		<td><?php echo $row->status ?></td>
		<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
		<td>
			<div class="btn-group dropup">
				<button aria-expanded="false" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
					Action
					<span class="caret"></span>
				</button>

				<ul class="dropdown-menu">
					<li>
						<a href="<?=base_url('data_vendor/getList').'/'.$row->id;?>">View Detail</a>
					</li>
				</ul>
			</div>
		</td>
	</tr>
	<?php
	$detail_category = '';
	$i++;
}
?>
<tr>
	<td colspan="11" style="text-align: center;"><?php echo $pagination ; ?></td>
</tr>
