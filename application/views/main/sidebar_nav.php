<div class="page-sidebar navbar-collapse collapse">
    <!-- <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
        <li class="nav-item start active open">
            <a href="<?php echo base_url()?>auth/login#top" class="nav-link nav-toggle">
                <i class="fa fa-home"></i>
                <span class="title"><?php echo $this->lang->line('global_home_tag');?></span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="<?php echo base_url()?>auth/login#news" class="nav-link nav-toggle">
                <i class="fa fa-bullhorn"></i>
                <span style="word-wrap: break-word;" class="title"><?php echo $this->lang->line('global_newsannouncement_tag');?></span>
                <span class="arrow"></span>
            </a>
        </li>  
        <li class="nav-item  ">
            <a href="<?php echo base_url()?>auth/login#faq" class="nav-link nav-toggle">
                <i class="fa fa-question-circle"></i>
                <span class="title">FAQs</span>
                <span class="arrow"></span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="<?php echo base_url()?>auth/login#contact" class="nav-link nav-toggle">
                <i class="fa fa-map-marker"></i>
                <span class="title"><?php echo $this->lang->line('global_contactus_tag');?></span>
                <span class="arrow"></span>
            </a>
        </li>
    </ul> -->

    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu background-baru" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
        <li class="nav-item start">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class=""></i>
                <span class="title"></span>
                <span class="arrow"></span>
            </a>
            
        </li>

        <li class="nav-item start active open">
            <a href="<?php echo base_url()?>auth/login#top" class="nav-link nav-toggle">
                <i class="fa fa-home"></i>
                <span class="title"><?php echo $this->lang->line('global_home_tag');?></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu" style="display: none;">
                <li class="nav-item start ">
                    <a href="<?php echo base_url()?>auth/login#news" class="nav-link ">
                        <i class="sub-menu-icon fa fa-bullhorn"></i>
                        <span class="title"><?php echo $this->lang->line('global_newsannouncement_tag');?></span>
                    </a>
                </li>
                <li class="nav-item start ">
                    <a href="<?php echo base_url()?>auth/login#faq" class="nav-link ">
                        <i class="fa fa-question-circle"></i>
                        <span class="title">FAQ</span>
                    </a>
                </li>
            </ul>
        </li>
        <!-- <li class="nav-item  ">
            <a href="<?php echo base_url()?>auth/registration" class="nav-link nav-toggle">
                <i class="fa fa-pencil-square-o"></i>
                <span class="title"><?php echo $this->lang->line('global_registration_tag');?></span>
                <span class="arrow"></span>
            </a>
        </li>  -->
        <li class="nav-item  ">
            <a href="<?php echo base_url()?>auth/registration" class="nav-link nav-toggle">
                <i class="fa fa-pencil-square-o"></i>
                <span class="sidebar-position title" id="registration" class="title"><?php echo $this->lang->line('global_registration_tag');?></span>
                <span class="arrow"></span>
            </a>
            <style type="text/css">
            #meh{
                margin-top: 0px;
            }
            </style>
            <ul class="sub-menu" style="display: none;" id="meh">
                <li class="nav-item start " id="registercoba">
                    <a href="<?php echo base_url()?>auth/registration#regist" class="nav-link ">
                        <i class="sub-menu-icon fa fa-edit"></i>
                        <span class="title"><?php echo $this->lang->line('global_register');?>Register</span>
                    </a>
                </li>
                <li class="nav-item start " id="statuscoba">
                    <a href="<?php echo base_url()?>auth/registration#check" class="nav-link ">
                        <i class="fa fa-search"></i>
                        <span class="title"><?php echo $this->lang->line('global_check_registration');?>Check Status</span>
                    </a>
                </li>
            </ul>
        </li>   
        <li class="nav-item  ">
            <a href="<?php echo base_url()?>auth/vendor" class="nav-link nav-toggle">
                <i class="fa fa-gavel"></i>
                <span class="title"><?php echo $this->lang->line('global_vendor_tag');?></span>
                <span class="arrow"></span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="<?php echo base_url()?>auth/login#contact" class="nav-link nav-toggle">
                <i class="fa fa-map-marker"></i>
                <span class="title"><?php echo $this->lang->line('global_contactus_tag');?></span>
                <span class="arrow"></span>
            </a>
        </li>
    </ul>
</div>