<script type="text/javascript">
	function delete_document(id,idDoc,tabID){

		var baseUrl  = "<?php echo base_url(); ?>";
		var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token    = '<?php echo $this->security->get_csrf_hash(); ?>';
		if (confirm('Are you sure you want to delete this?')) {
			$.post( baseUrl+ "document/delete_document", { csrf: token, id: id, doc: idDoc, tab: tabID })
			.done(function( resp ) {
				$("#main_container").load(baseUrl+'document/load_document');
			});
		}	
	}
</script>
<div id="main_container">
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Document Details</h3>
		</div>
		<div class="box-body">
			<a class="btn btn-srch btn-primary btn-sm pull-right" href="<?php echo base_url() ?>document">Back</a>
			<table class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
				<thead>
					<tr>
						<th width="40">No</th>
						<th>File Name</th>            
						<th width="100">File Type</th>
						<th width="100">File Size</th>
						<th width="200">Remark</th>
						<th width="80">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php  
					foreach($browse as $row){?>
						<tr>
							<td><?php echo $num = $num+1?></td>
							<td><?php echo $row->filename?></td>
							<td><?php echo $row->filetype?></td>
							<td><?php echo $this->m_document->convert_kb($row->filesize) ;?></td>
							<td><?php echo $row->table?></td>
							<td><i class="fa fa-trash" onclick="delete_document(<?php echo $row->venDoc_id.','.$row->id_docs.','.$row->id_table ;?>)"></i></td>
						</tr>
						<?php }?>
						<tr>
							<td colspan="6" style="text-align: center;"><?php echo $pagination ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>