<div id="#main_container">
  
<script type="text/javascript">
$(document).ready(function(){
   var baseUrl  = "<?php echo base_url(); ?>";
   $(function () {    		
    $(".js-example-basic-single").select2();     
		$(".js-example-basic-multiple").select2();	   

   });
   
   $('#sd').datepicker({  
          changeMonth: true, changeYear: true, 
      dateFormat: "yy-mm-dd", minDate: 0
     });

    
}); 
</script>
<div class="page-header">
  <h3>Vendor Redlist</h3>
</div> 
<div id="debug"></div>
<?php echo form_open_multipart('redlist/create',array('id'=>'form', 'class'=>'form-horizontal','role'=>'form'));	?>
  <input type="hidden" name="red_id" value="<?php echo (isset($redlist->id_redlist))?$redlist->id_redlist:''; ?>">
  <input type="hidden" name="status" value="<?php echo (isset($redlist->red_state))?$redlist->red_state:''; ?>">
  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Select Vendor</label>
    <div class="col-sm-8">
    	<select name="ven_id" id="ven_id" class="form-control js-example-basic-single" required="required">
        
        <?php 
        if(isset($redlist->id_redlist))
        {
          echo "<option value='$redlist->id'>$redlist->vendor_name</option>";
        }
        else
        {
      		  echo "<option value=''>--Select Vendor--</option>";
          foreach ($vendor as $ven) 
          {
            echo "<option value='$ven->id'>$ven->vendor_name</option>";
          } 
        }

        ?>
    	</select>
    </div>
  </div>
  
  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Start Date</label>
    <div class="col-sm-8">
      <input type="text" value="<?php echo (isset($redlist->start_date))?$redlist->start_date:''; ?>" required name="start_date" id="sd" class="form-control" placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Counter</label>
    <div class="col-sm-8">
      <input style="border:none;" readonly type="text" value="<?php echo (isset($redlist->counter))?$redlist->counter: 2 ; ?>" required name="counter" class="form-control"  placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Reason</label>
    <div class="col-sm-8">
    	<textarea required name="remark" id="remark" class="form-control" rows="3"><?php echo (isset($redlist->remark))?$redlist->remark:''; ?></textarea>
    </div>
  </div>

  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Document</label>
    <div class="col-sm-8">
    <?php 
      
      $data  = array('type'=>'file','name'=> 'userfile','id' => 'file_upload','value'=>'true','content'=>'Import','style'=>$style); 
      echo form_input($data); 
      if(!empty($redlist->doc_name) ) 
      { 
        echo form_hidden('doc_name', $redlist->doc_name);
        if(!empty($redlist->doc_type) )
        {
          echo form_hidden('doc_type', $redlist->doc_type);
          echo "<em>$redlist->doc_name.$redlist->doc_type</em>";
        }
       }

      ?>  
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <a type="button" href="javascript:void(0)" onclick="load_data()" class="btn btn-warning">Back</a>
      <button type="submit" class="btn btn-primary">Save</button>
    </div>
  </div>
<?php echo form_close(); ?>

</div>
