<?php $this->load->view('notifications/header');?>

<br>
Dengan hormat,
<br>
<b><?php echo $vendorname ;?></b>
<br>
<br>
Pertama-tama kami mengucapkan terima kasih atas minat Bapak/Ibu telah mendaftar sebagai rekanan PT Garuda Indonesia (Persero) Tbk ("Garuda") untuk <b>Kategori <?php echo $category ;?></b>

<?php 
	if($link!='')
	{
		echo '<br><br>';
		echo 'Berikut kami sampaikan surat keterangan mengenai status join perusahaan Bapak/Ibu yang dapat diakases di';
		echo '<br>';
		echo $link ;
	}
?>

<br>
<br>
Kami sampaikan terima kasih atas partisipasi Perusahaan Bapak/Ibu, semoga kita dapat bekerja sama untuk  kesempatan mendatang.
<br>
<br>

<?php $this->load->view('notifications/footer');?>