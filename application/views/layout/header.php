<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>e-Procurement Garuda Indonesia</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/bootstrap-admin.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/bootstrap-responsive.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/custom/afterlogin.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/custom/afterloginstyle.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/pages/dashboard.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/datepicker3.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/layout/css/form.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/plugins/datetimepicker/jquery.datetimepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/uploadfile.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/select2.min.css"/>

<style type="text/css">
	#datetimepicker9 .bootstrap-datetimepicker-widget{
	top: 36px !important;
}
</style>

<script src="<?php echo base_url() ?>assets_/js/jquery-1.9.1.min.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/ui/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/jquery-ui.css"/>

<script src="<?php echo base_url() ?>assets_/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url() ?>assets_/js/excanvas.min.js"></script> 
<script src="<?php echo base_url() ?>assets_/js/chart.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url() ?>assets_/js/bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets_/js/full-calendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/base.js"></script>
<script src="<?php echo base_url() ?>assets_/plugins/datetimepicker/build/jquery.datetimepicker.full.js"></script>
<script src="<?php echo base_url() ?>assets_/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script src="<?php echo base_url() ?>assets/js/select2.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/jquery.quicksearch.js"></script>
<script src="<?php echo base_url() ?>assets_/js/jquery.uploadfile.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/jquery.numeric.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/bootstrap-dialog.min.js"></script> 
<link href="<?php echo base_url()  ?>assets/css/bootstrap-dialog.min.css" rel="stylesheet">

<script type="text/javascript">

function updateClock (){
 	var currentTime = new Date ( );
  	var currentHours = currentTime.getHours ( );
  	var currentMinutes = currentTime.getMinutes ( );
  	var currentSeconds = currentTime.getSeconds ( );
  	// Pad the minutes and seconds with leading zeros, if required
  	currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
  	currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;
  	// Choose either "AM" or "PM" as appropriate
  	var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";
  	// Convert the hours component to 12-hour format if needed
  	currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;
  	// Convert an hours component of "0" to "12"
  	currentHours = ( currentHours == 0 ) ? 12 : currentHours;
  	// Compose the string for display
  	var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;  	
   	$("#clock").html(currentTimeString);   	  	
 }

$(document).ready(function(){
   setInterval('updateClock()', 1000);
});

</script> 

</head>
<body>
<div class="navbar navbar-fixed-top">
<div class="navbar-inner">
  <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
    <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a>
    <a class="brand" href="#"><img src = "<?php echo base_url(); ?>assets_/favicon-white.png" style="margin-right:10px;"/>e-Procurement Garuda Indonesia</a>
    <div class="nav-collapse">
      <ul class="nav pull-right">
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 12px; font-weight: 600;"><i
                          class="icon-user"></i><?php if ($this->crud->browse("","ga_employee","userID",$this->tank_auth->get_user_id())->fullname) {
                          						echo ($this->crud->browse("","ga_employee","userID",$this->tank_auth->get_user_id())->fullname); 
												} else { echo ucfirst($username); } ?> <b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-login">
            <li class="user-header">
              <!-- img src="
              <?php echo base_url(); ?>assets_/images/user.png" class="img-circle" alt="User Image"> -->
              <?php
                            	$userID  = $this->tank_auth->get_user_id();
                            	$images    = array('jpg','gif','png'); $imgfile = "false";
						 		foreach($images as $imgext){
						 			$imgpath  = "./uploads/images/".$userID.".".$imgext;
									if(file_exists($imgpath)){	
									$imgfile = $userID.".".$imgext; 	break;	}
								}                            
                                if($imgfile=="false") $path = base_url()."assets/images/avatar.png"; 
                            	else $path = base_url()."uploads/images/".$imgfile;               	
                            
                            ?>
              <img class = "img-circle" src = "<?php echo $path; ?>"/>
              <p><?php if ($this->crud->browse("","ga_employee","userID",$this->tank_auth->get_user_id())->fullname) {
                        echo ($this->crud->browse("","ga_employee","userID",$this->tank_auth->get_user_id())->fullname); 
						} else { echo ucfirst($username); } ?>
              </p>
            </li>
            <li class="user-footer">
              <div class="col-md-6 col-xs-6">
                <a href="<?php echo base_url(); ?>profiles" style="margin-left: 6px;" class="btn btn-default btn-flat pull-left">Profile</a>
              </div>
              <div class="col-md-6 col-xs-6">
                <a href="<?php echo base_url(); ?>auth/logout" class="btn btn-default btn-flat pull-right">Sign Out</a>
              </div>
              <!-- <div class="pull-left"> -->
                <!-- <a href="<?php //echo base_url(); ?>auth/profile" style="margin-left: 6px;" class="btn btn-default btn-flat pull-left">Profile</a> -->
              <!-- </div> -->
              <!-- <div class="pull-right"> -->
                <!-- <a href="<?php //echo base_url(); ?>auth/logout" class="btn btn-default btn-flat pull-right">Sign out</a> -->
              <!-- </div> -->
            </li>
          </ul>
        </li>
      </ul>
      <form class="navbar-search pull-right">
        <input type="text" class="search-query" placeholder="Search">
        <div id="clock" style="font-size: 13px;color: #fff;display: inline-block;font-weight: 600;margin-top: 2px !important;margin-right: -25px;margin-left: 4px;"></div>
      </form>
    </div>
    <!--/.nav-collapse --> 
  </div>
  <!-- /container --> 
</div>
<!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
<div class="subnavbar-inner">
  <div class="container">
    <ul class="nav navbar-nav">
    <?php    	
    	
      	$details = $this->m_menu->list_menu(); $i=1;        //if($this->tank_auth->get_user_id()=="18")   { print_r($details); }		
        foreach($details as $val){ 
        	$submenu = $val['submenu']; 
        	$active  = ($i==1) ? "active": "dropdown";  
        	$default = base_url().$val['url_menu'];
        	$url 	 = $submenu ? "javascript:;" : $default;  
        	$toggle1 = $submenu ? "data-toggle='dropdown'" : "" ; 	?> 
        	       	
        	<li class="<?php echo $active; ?>"><a href="<?php echo $url; ?>" class="dropdown-toggle" <?php echo $toggle1; ?>>
        	<i class="<?php  echo $val['class']; ?>"></i><span><?php  echo $val['menu_name']; ?></span><b class="caret"></b></a>
        	<?php  if($submenu){ //print_r($submenu); ?>
        		<ul class="dropdown-menu first-node">
        			<?php foreach($submenu as $val2){ 
        					$submenu2 = $val2['submenu'];  
        					$after    = !$submenu2 ? "menu-item dropdown dropdown-submenu2" : "menu-item dropdown dropdown-submenu"; ?>
        			<li class="<?php echo $after ?>">
        			<?php  $toggle2 = $submenu2 ? "data-toggle='dropdown'" : ""; ?>       
	                <a href="<?php echo base_url().$val2['url_menu']; ?>" class="dropdown-toggle" <?php echo $toggle2; ?>><?php echo $val2['menu_name']; ?></a>
	                <?php  if($submenu2){ ?>
		                <ul class="dropdown-menu">
		                  <?php foreach($submenu2 as $val3){  ?>
		                  	<li><a href="<?php echo base_url().$val3['url_menu']; ?>"><?php  echo $val3['menu_name']; ?></a></li>	                  
		                  <?php } ?>
		                </ul>
	                <?php } ?>
	              </li>
	              <?php } ?>
        		</ul>        		
        	<?php } ?>
        	</li>        	
      <?php $i++; } ?>    	
    
    </ul>
 <div id="clock" style="font-size: 20px;"></div>
  </div>  
</div>
</div>

<!-- /subnavbar -->
<div class="main">
<div class="main-inner">
  <div class="container">
    
   

   

  
