<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";   
        
     $('#btn_cancel3').click(function(){ 
   		 $("#container3").load(baseUrl+'due_dilligence/load_assesment');
     }); 
     
     $('#btn_create3').click(function(){ 
     	var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	    
	    var values   = $("#form_conclu").serializeArray();
		values.push({name: csrf,value: token});
		values = jQuery.param(values);

		 	$.post( baseUrl+ "due_dilligence/save_conclutions", values )
			  .done(function( resp ) {
			    	$("#container3").load(baseUrl+'due_dilligence/load_assesment');
			    	//$("#debug3").html(resp);
			});
     	
     });    
	   
   });

</script>
<div id="debug3"></div>
<?php echo form_open('#',array('id'=>'form_conclu')); $current_date = date('Y-m-d'); ?>
<table width="50%">
<tr>
	<td style="vertical-align: top;"><label>Conclutions</label></td><td style="vertical-align: top;">:</td>
	<td><textarea name="conclutions" id="conclutions" cols="20" rows="5" placeholder="Conclutions"  class="form-control" required>
		<?php echo $default->conclutions; ?>
	</textarea></td>	
</tr>
<tr>
	<td colspan="3">
		<input type="button" id="btn_cancel3" value="Cancel" class="btn btn-default btn-sm"/>
		<?php if($this->permit->md3){ ?><input type="button" id="btn_create3" value="Create" class="btn btn-primary btn-sm"/><?php } ?>		
	</td>	
</tr>
</table><br/><br/>
<?php echo  form_close(); ?>


