<p>In order to meet the needs of Business Process Outsourcing in Head Office and Domestic Branch Office Location, PT Garuda Indonesia (Persero) Tbk. invites Companies who engaged in the contract work service to participate in the procurement process.</p>

<p>General requirements for Procurement participants are:</p>

<p>1. Have business license and published official website<br />
2. Be willing to comply with all applicable employment Rules in Indonesia<br />
3. Have official license from Labour Department to each site where labor placed<br />
4. Currently have experienced placed labor minimal in 10 (ten) major cities in Indonesia<br />
5. Have representative offices in major cities to supervise and oversee the personnel stationed in each site<br />
6. Be willing to comply with all applicable procurement procedures in Garuda Indonesia<br />
7. Not being related to legal cases that could lead to failure in work completion.</p>

<p>Company who interested in participating in this Procurement shall submit documents:</p>

<p>1. Letter of Interest signed by Chairman of the Company<br />
2. List of labor that exists today with the details of each city placement/site<br />
3. Proof of membership of BPJS employment and BPJS health of the entire existing labor</p>

<p>All documents above shall be submitted in the form of soft copy (scanned document with a maximum size of 5 MB per email) and sent via electronic mail to the email address: <a href="mailto:business-support@garuda-indonesia.com">business-support@garuda-indonesia.com</a> &nbsp;cc: <a href="mailto:sandi.aditya@garuda-indonesia.com">sandi.aditya@garuda-indonesia.com</a> &nbsp;no later than Tuesday, August 16, 2016 at 16:00 pm.&nbsp;</p>

<p>Procurement Committee will send Document Request For Proposal (&quot;RFP&quot;) for companies who have submitted documents via email.</p>

<p>Garuda Indonesia does not collect any fees from participants in each procurement process.</p>

<p>Jakarta, 12 August 2016&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

<p>PT Garuda Indonesia (Persero) Tbk.</p>
