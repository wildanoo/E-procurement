<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Faq extends Controller {

	function __construct()
	{
		parent::__construct();	
// 		$this->load->model('m_user');
// 		$this->load->model('m_category');
// 		islogged_in();		
// 		$this->m_user->authenticate(array(41));
	}
	
	function index(){
		//if ($this->tank_auth->is_logged_in()) {
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
			
		//$where =  array('status'=>'1');
			
		//$sess_like  = $this->session->flashdata('like');
	
 		$sess_cat   = $this->session->userdata('sess_cat');
 		$sess_field   = $this->session->userdata('sess_field');
			
	
 		$field      = $sess_field != '' ? $sess_field : "question_ind";
 		$id_cat     = $sess_cat != '' ? $sess_cat  : "";
	
 		if($sess_cat != ''){	$where = array('question_ind'=>$sess_cat,'status'=>'1');
 		} else { $where = array('status'=>'1');	}
			
		$table 	    = "faq";
		$page       = $this->uri->segment(3);
		$per_page   = 10;
		$offset     = $this->crud->set_offset($page,$per_page,$where);
		$total_rows = $this->crud->get_total_record("",$table,$where);
		$set_config = array('base_url'=> base_url().'/faq/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);
			
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();
	
		$order 			     = array('field'=>'created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;
		$select 			 = "id,question_eng,question_ind,answer_eng,answer_ind,status,
								    (SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$sessID  = $this->session->flashdata('anID');
		$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
	
		if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
		else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
		$data['browse'] = $browse;
	
// 		$office = $this->crud->browse("","faq","","","true","id,office_name");
// 		if(!$office) $office = array();
// 		$select = array(''=>'All');
// 		foreach($office as $val){ $options[$val->id] = $val->office_name; }
// 		$data['office'] = $select + $options;
			
		$data['view']	= "faq/browse";
		$this->load->view('layout/template',$data);
			

	}
	
	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
		$search_conditions = array(
				'question_eng'		=> $_POST['search_term'],
				'question_ind'		=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		$data['view'] = "faq/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{
		/* get data for table view from database with pagination */
	
		$table 	     = "faq t1";
		$select 	 = "id,question_ind,question_eng,answer_eng,answer_ind,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'faq/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
	
		/* get data for table view from database with pagination */
	
		$table 	     = "faq t1";
		$select 	 = "id,question_ind,question_eng,answer_ind,answer_eng,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,$where,$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'faq/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	function form_create(){
		
		$data ['user_id'] = $this->tank_auth->get_user_id();
		$data ['username'] = $this->tank_auth->get_username();
		$data['view'] = 'faq/form_create';
		$this->load->view('layout/template',$data);
		
	}
	
	function create(){
		$curr_date =  date('Y-m-d H:i:s'); 
		$userID = $this->tank_auth->get_user_id();
		$editor   = $_POST['editor'];
		$question_ind = $_POST['question_ind'];
		$question_eng = $_POST['question_eng'];
		$data     = array(
				'question_ind'=>$question_ind,
				'question_eng'=>$question_eng,
				'answer_ind'  =>$editor[0],
				'answer_eng'  =>$editor[1],
				'status'	  =>'1',
				'created_id'  =>$userID,
				'created_date'=>$curr_date,
				'last_updated'=>$curr_date);
		
	$where   = array('question_ind'=>$_POST['question_ind'],'status'=>'0');
	$checked = $this->crud->is_exist("","faq","id",$where);
	
	if ($checked){ 
		$id = $this->crud->browse("","faq","question_ind",$_POST['question_ind'],"false","id")->id;
		$update = array('status'=>'1','last_updated'=>$curr_date);
		$this->crud->update("","faq","id",$id,$update);
	} else {	$id = $this->crud->insert("","faq",$data);	}
	$this->session->set_flashdata('message','1 data success insert');
	redirect('faq/','refresh');
	}
	
	function delete(){
		$id = $this->uri->segment(3);
		$status = array('status'=>'0');
		$this->crud->update('','faq','id',$id,$status);
		redirect('faq/','refresh');
	}
	
	function form_update(){

		$data ['user_id'] = $this->tank_auth->get_user_id();
		$data ['username'] = $this->tank_auth->get_username();
		$id = $this->uri->segment(3);
		$select = 'id,question_eng,question_ind,answer_eng,answer_ind,answer_eng';
		$def = $this->crud->browse('','faq','id',$id,$select);
		$data['def'] = $def;
		$contents[0] = $def->answer_ind;
		$contents[1] = $def->answer_eng;
		$data['contents'] = !$contents ? "-" : $contents;
		$data['view'] = 'faq/form_update';
		$this->load->view('layout/template',$data);		
		
	}
	
	function update(){
		$id = $_POST['id'];
		$curr_date =  date('Y-m-d H:i:s');
		$userID = $this->tank_auth->get_user_id();
		$editor   = $_POST['editor'];
		$question_ind = $_POST['question_ind'];
		$question_eng = $_POST['question_eng'];
		$data     = array(
				'question_ind'=>$question_ind,
				'question_eng'=>$question_eng,
				'answer_ind'  =>$editor[0],
				'answer_eng'  =>$editor[1],
				'status'	  =>'1',
				'created_id'  =>$userID,
				'last_updated'=>$curr_date);
		//print_r($data);exit;
		$this->crud->update('','faq','id',$id,$data);
		$this->session->set_flashdata('message','1 data success updated');
		redirect('faq/','refresh');
	}
	
		function download_faq(){
	

			ob_start();
			$data['title'] = "FAQ";
			$select = "id,question_eng,question_ind,answer_eng,answer_ind";
			$db_faq = $this->crud->browse("","faq","","","false",$select);
			$data['faq'] = $db_faq;
			$this->load->view('faq/form', $data);
			$html = ob_get_contents();
			ob_end_clean();
	
			require_once('./application/libraries/html2pdf/html2pdf.class.php');
			$pdf = new HTML2PDF('P','A4','en');
			$pdf->WriteHTML($html);
			$pdf->Output('faq.pdf', 'D');
		
	
	}
}