<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Email_portal extends Controller {

	function __construct()
	{
		parent::__construct();
	}


	////// * Main Methods * //////

	function index(){			

	 	$token = isset($_GET['token'])?$_GET['token']:redirect('email_portal/notFound');
	 	$data  = $this->crud->browse('','session_email','token',$token,'false');
	 		
	 	if (count($data)>0) {
		 	if ($this->tank_auth->is_logged_in() && $this->session->userdata('username') == $data->username) {
		 		$redirect = $data->controller.'/'.$data->method.'/'.$data->parameter;		 			
		 		redirect($redirect);
		 	}else{
		 		$data_challenge['view'] = 'email_portal/password_challenge';
		 		$data_challenge['data'] = $data;
		 		$this->load->view('layout/template',$data_challenge);
		 	}
		 }else{
		 	redirect('email_portal/notFound');
		 }

	}

	function auth_attempt(){
		$errors = array();
		$data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND $this->config->item('use_username', 'tank_auth'));
		$data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth');

		$this->form_validation->set_rules('login', 'Login', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		
		if ($this->config->item('login_count_attempts', 'tank_auth') AND ($login = $this->input->post('login'))) {
			$login = $this->security->xss_clean($login);
		} else { $login = ''; }
		
		$array['status']  =  "false";
		$usrname = $_POST['login'];
		$passwd  = $_POST['password'];
		if(!$usrname || !$passwd) $array['errors']= "username or password required";
		
		if ($this->form_validation->run()) {								// validation ok
			if ($this->tank_auth->login(
				$this->form_validation->set_value('login'),
				$this->form_validation->set_value('password'),
				$this->form_validation->set_value('remember'),
				$data['login_by_username'],
				$data['login_by_email'],
				$join_content)) {
			    $array['status']  =  "true";
			} else {
				$errors = $this->tank_auth->get_error_message(); 			// fail
				foreach ($errors as $k => $v) {
				    if (isset($errors['not_activated'])) { $v = $k; }
				    $msg[] = $v;
				}
				$msg = implode(",",$msg);
				$array['status']  =  "false";
				$array['errors']  =  $msg;
			}
		}

		echo json_encode($array);

	}

	/////////////////////////////////


	////// * Required Methods * //////

	function notFound(){
		$data['view'] = 'email_portal/not-found';
 		$this->load->view('layout/template',$data);
	}

	/////////////////////////////////
 
}
?>