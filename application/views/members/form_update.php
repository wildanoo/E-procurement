
<script type="text/javascript"> 	
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";		 
	  $(function(){  });
	  
	  $('#id_country').change(function(){  
   		var id_country = $('#id_country').val(); 
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
           $.ajax({
               url  : baseUrl + 'member/get_office',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_country='+id_country,
               success: function(resp){
               	  $('#id_office').html(resp);
               }
            });
     });
});	  
</script>

<?php echo form_open('member/update',array('id'=>'form')); ?>
<?php echo form_hidden("memID",$def->id); ?>
<table cellpadding="2" cellspacing="2">
<tr>
	<td><label>First Name</label></td><td>:</td>
	<td><input type="text" name="firstname" id="firstname" class="form-control" value="<?php echo $def->firstname; ?>" required/></td><td id="msg1"></td>	
</tr>
<tr>
	<td><label>Last Name</label></td><td>:</td>
	<td><input type="text" name="lastname" id="lastname" class="form-control" value="<?php echo $def->lastname; ?>" required/></td><td id="msg2"></td>	
</tr>
<tr>
	<td><label>Country</label></td><td>:</td>
	<td><?php 
		$prop_country = 'class="form-control" id="id_country" style="width:220px"';
		echo form_dropdown('id_country',$country,"1",$prop_country); ?>			
	</td><td id="msg3"></td>	
</tr>
<tr>
	<td><label>Office</label></td><td>:</td>
	<td>		
		<?php 
		$prop_office = 'class="form-control" id="id_office" style="width:220px"';
		echo form_dropdown('id_office',$office,"default",$prop_office); ?>		
	</td><td id="msg4"></td>		
</tr>
<tr>
	<td><label>Departemen</label></td><td>:</td>
	<td><?php 
		$prop_dept = 'class="form-control" id="id_dept" style="width:220px"';
		echo form_dropdown('id_dept',$dept,$def->id_dept,$prop_dept); ?>			
	</td><td id="msg5"></td>	
</tr>
<tr>
	<td><label>Level</label></td><td>:</td>
	<td><?php 
		$prop_level = 'class="form-control" id="id_level" style="width:220px"';
		echo form_dropdown('id_level',$level,$def->id_level,$prop_level); ?>			
	</td><td id="msg6"></td>	
</tr>

</table>