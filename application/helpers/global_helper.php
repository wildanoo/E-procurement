<?php

if(!function_exists('globalRecentPost')) {

	function indo_date($date) { //reformat from yyyy-mm-dd to dd mon yyyy
		
		$newdate = new DateTime($date);
		$pcs = explode("-", $date);
		$y = $newdate->format('Y');
		$m = $newdate->format('n');
		$d = $newdate->format('j');
		$wk = $newdate->format('w');
		
		$getbulan = array ();
		$getbulan[1] = 'Januari';
		$getbulan[2] = 'Februari';
		$getbulan[3] = 'Maret';
		$getbulan[4] = 'April';
		$getbulan[5] = 'Mei';
		$getbulan[6] = 'Juni';
		$getbulan[7] = 'Juli';
		$getbulan[8] = 'Agustus';
		$getbulan[9] = 'September';
		$getbulan[10] = 'Oktober';
		$getbulan[11] = 'November';
		$getbulan[12] = 'Desember';

		$gethari = array ();
		$gethari[0] = 'Minggu';
		$gethari[1] = 'Senin';$gethari[2] = 'Selasa';$gethari[3] = 'Rabu';
		$gethari[4] = 'Kamis';$gethari[5] = 'Jumat';$gethari[6] = 'Sabtu';

		return $gethari[$wk]. ", ". $d ." ". $getbulan[$m] ." ". $y;
	}

	function globalRecentPost() {
		 
		$CI = get_instance();
		$CI->load->model('crud');

		if ($CI->session->userdata('lang') == 'eng') {
			$title  = "title_eng";
			$detail = "detail_eng";
		}elseif ($CI->session->userdata('lang') == 'ind') {
			$title  = "title_ind";
			$detail = "detail_ind";
		}else {
			$title  = "title_eng";
			$detail = "detail_eng";
		}

		$joins[0][0] = 'subcontent_type';
		$joins[0][1] = 'subcontent_type.id = announce.subcontent_type';
		$joins[0][2] = 'left';

		$order_by['field']='publish_date';
		$order_by['order']='DESC';
		$num="5";
		$offset="0";
		$where = "status = '3' AND (CURDATE() BETWEEN DATE_FORMAT(publish_date, '%Y-%m-%d') AND DATE_FORMAT(end_valdate, '%Y-%m-%d'))";
		$select = "announce.id,file,code,content_type,subcontent_type,subcontent_type.subtype_name,".$title." AS title,".$title." AS detail,first_valdate,end_valdate,publish_date,status,announce.created_date";
		$recent_post = $CI->crud->browse_join_with_paging("","announce","","","true",$select,$joins,$where,$order_by,$num,$offset);			

		return $recent_post;

	}
}

if(!function_exists('globalNeedAanwidzing')) {
	function globalNeedAanwidzing() {
		 
		$CI = get_instance();
		$CI->load->model('crud');

		$array = array();

		$where = 'behavior_id IN (2,4) ';
		$details = $CI->crud->browse("","subcontent_type","","","true","id",$where);
		if (count($details) > 0) 
		{
			foreach ($details as $detail)
			{array_push($array, $detail->id);}
		}

		return implode(',', $array);

	}
}

if(!function_exists('globalRedlistReducer')) {
	function globalRedlistReducer($id_announce = "") {
		 
		$CI = get_instance();
		$CI->load->model('crud');

		$result = $getData = array();

		$joins[0][0] = 'vendor v';
		$joins[0][1] = 'ap.id_vendor = v.id';
		$joins[0][2] = 'left';
		$joins[1][0] = 'users u';
		$joins[1][1] = 'u.id_vendor = ap.id_vendor';
		$joins[1][2] = 'LEFT';
		$where 		 = 'v.status = "redlist"';

		$details = $CI->crud->browse_join("","announce_join_participant ap","id_announce",$id_announce,"false","v.id",$joins,$where);
			
		foreach ($details as $detail) {
			$counter = $CI->crud->browse("","vendor_redlist","id_vendor",$detail,"false","counter");				
			$data    = array('counter' => (($counter->counter)-1));
			$where1  = 'id_vendor = "'.$detail.'"';
			$CI->crud->multiple_update("","vendor_redlist",$where1,$data);

			$counter = $CI->crud->browse("","vendor_redlist","id_vendor",$detail,"false","counter");				
			if ($counter->counter == 0) {
				$data2 = array('status' => 'active');
				$CI->crud->update("","vendor","id",$detail,$data2);
			}
		}
	}
}

if(!function_exists('globalGetType')) {
	function globalGetType() {
		 
		$CI = get_instance();
		$CI->load->model('crud');

		$array = array();

		$details = $CI->crud->browse("","content_type","","","false","id, type_name");
		if (count($details) > 0) 
		{
			foreach ($details as $detail)
			{$array[$detail->id] = $detail->type_name;}

			$array[0] = "-";
		}		

		return $array;

	}
}

if(!function_exists('globalGetSubtype')) {
	function globalGetSubtype() {
		 
		$CI = get_instance();
		$CI->load->model('crud');

		$array = array();

		$details = $CI->crud->browse("","subcontent_type","","","false","id, subtype_name");
		if (count($details) > 0) 
		{
			foreach ($details as $detail)
			{$array[$detail->id] = $detail->subtype_name;}

			$array[0] = "-";
		}		

		return $array;

	}
}

if(!function_exists('globalGetCategory')) {
	function globalGetCategory() {
		 
		$CI = get_instance();
		$CI->load->model('crud');

		$array = array();

		$details = $CI->crud->browse("","category","","","false","id, category");
		if (count($details) > 0) 
		{
			foreach ($details as $detail)
			{$array[$detail->id] = $detail->category;}

			$array[0] = "-";
		}		

		return $array;

	}
}

if(!function_exists('globalGetSubcategory')) {
	function globalGetSubcategory() {
		 
		$CI = get_instance();
		$CI->load->model('crud');

		$array = array();

		$details = $CI->crud->browse("","subcategory","","","false","id, subcategory");
		if (count($details) > 0) 
		{
			foreach ($details as $detail)
			{$array[$detail->id] = $detail->subcategory;}

			$array[0] = "-";
		}		

		return $array;

	}
}

if(!function_exists('globalDefineBehaviorBySubtype')) {
	function globalDefineBehaviorBySubtype($arg) {
		
		$CI = get_instance();
		$CI->load->model('crud');

		$array = array();

		$details = $CI->crud->browse("","subcontent_type","behavior_id",$arg,"false","behavior_id");
		if (count($details) > 0) 
		{
			foreach ($details as $detail){}
		}		

		return $detail->behavior_id;

	}
}

if(!function_exists('globalAllSubcontentTypes')) {
	function globalAllSubcontentTypes()
	{
		/* get categories data from database */

		$CI = get_instance();
		$CI->load->model('crud');

		$getData = array();

		$iterations = $CI->crud->browse("","subcontent_type","","","true","id,subtype_name");

		if(!$iterations) $iterations = array();
		foreach($iterations as $val){ $iteration[$val->id] = $val->subtype_name; } 
		$getData = $iteration;

		/* ===== */

		return $getData;
	}
}

if(!function_exists('globalGetAnnouncementStatus')) {
	function globalGetAnnouncementStatus($id_status = '', $publish_date = '', $start_date = '', $end_date = '')
	{
		/* get categories data from database */

		$CI = get_instance();
		$CI->load->model('crud');

		if ($id_status == '4') {
			return array('id' => 4,'string'=>'Closed');
		}elseif ($id_status == '99') {
			return array('id' => 99,'string'=>'Cancel');
		}elseif (($publish_date != '' && $publish_date <= date("Y-m-d H:i:s")) && ($start_date != '' && $start_date >= date("Y-m-d H:i:s"))) {
			return array('id' => 1,'string'=>'Published');
		}elseif (($start_date != '' && $start_date <= date("Y-m-d H:i:s")) && ($end_date != '' && $end_date >= date("Y-m-d H:i:s"))) {
			return array('id' => 2,'string'=>'Open for Joining');
		}elseif (($end_date != '' && $end_date < date("Y-m-d H:i:s")) && $id_status != '4') {
			return array('id' => 3,'string'=>'Ongoing');
		}

		/* ===== */

		return array('id' => 0,'string'=>'New');
	}
}

if(!function_exists('is_valid_email')) {

	function is_valid_email($email) {		
		$pattern  = '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'; 		if(preg_match($pattern,$email)) return true;
		else return false;		
	}
	
}

?>