<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Profiles extends Controller {

	function __construct()
	{
		parent::__construct();	
		islogged_in();
		$this->load->model('m_user');		
	}	
		
	function index(){
		if ($this->tank_auth->is_logged_in()) {

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			
			$id 				= $this->tank_auth->get_user_id();
			$userID 			= $this->tank_auth->get_user_id();
			
			$def 				= $this->crud->browse('','users','id',$userID,'false','username,email,last_ip');
			$data['def'] 		= $def;
			
			$pilih 				= 'nopeg,fullname,(SELECT position_name FROM position WHERE id=id_position) as id_position,
								  (SELECT dept_name FROM departemen WHERE id=id_dept) as id_dept,';
			$data['profiles'] 	= $this->crud->browse("","ga_employee","userID",$id,"false",$pilih);
			
			$choose 			= '(SELECT name FROM groups WHERE id=group_id) as group_id';
			$data['group'] 		= $this->crud->browse('','users_groups','user_id',$id,'false',$choose);

			$data['view']		= "profiles/browse";
			$this->load->view('layout/template',$data);
 		
	 		
		} else {  
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/'); 	 }
	}
	
	function change_picture(){
	
	
		if($_FILES['userfile']){
	
			$userID  = $this->tank_auth->get_user_id();
			$allow_type = array('jpg','gif','png');
			$file = $_FILES['userfile'];
			$file = explode(".",$file['name']);
			$jum  = COUNT($file) - 1 ;
			$type = $file[$jum];
			 
			if(in_array($type,$allow_type)){
				 
		 		
				$images    = array('jpg','gif','png'); $imgfile = "false";
				foreach($images as $imgext){
					$imgpath  = "./uploads/images/".$userID.".".$imgext;
					if(file_exists($imgpath)){
						unlink($imgpath); 	break;	}
				}
				 
	
				$path 	 = './uploads/images/';
				$filename= $userID.".".$type;
				$config  = $this->m_user->set_config($filename,$path,'jpg|gif|png');
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
	
				$msg['error_img'] = "";  $msg['success_img']   = array();
				if ( ! $this->upload->do_upload()){
					$msg['error_img']   = $this->upload->display_errors();
				} else {
					$msg['success_img'] = $this->upload->data(); }
			}
		}
		//print_r($_FILES);exit;
		redirect('profiles','refresh');
	
	
	}
	
}