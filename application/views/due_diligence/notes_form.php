<script type="text/javascript">
  $(function () { 
   	  var baseUrl  = "<?php echo base_url(); ?>"; 
   	  
   	   $('#btn_cancel').click(function(){ 
   			$("#container11").load(baseUrl+'due_diligence/load_view_notes');   		
   	   }); 
   	   
   	   $('#btn_notes').click(function(){   	  
   	  		var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	    
		    var values   = $("#form_notes").serializeArray();
			values.push({name: csrf,value: token});
			values = jQuery.param(values);
			 	$.post( baseUrl+ "due_diligence/save_notes", values )
				  .done(function( resp ) {
				    	var id_visit   = $("#id_visit").val();
				    	$("#container11").load(baseUrl+'due_diligence/load_view_notes/'+ id_visit); 
				});
	     	
	     });     
  });

</script>

<div id="debug11"></div>

<?php echo form_open('#',array('id'=>'form_notes')); ?>
<input type="hidden" name="id_visit" id="id_visit" value="<?php echo $id_visit; ?>"/>
<table width="60%">
	<tr>
		<td style="vertical-align: top;"><label>Notes</label></td><td style="vertical-align: top;">:</td>
		<td><textarea name="notes" id="notes" cols="20" rows="5" class="form-control" placeholder="Notes" required></textarea></td>
	</tr>
	<tr>
		<td align="right" colspan="3">
			<button type="button" id="btn_cancel" class="btn btn-default">Cancel</button>
			<?php if($this->permit->md3) {  ?><button type="button" id="btn_notes" class="btn btn-info btn-primary">Create</button><?php } ?>			
		</td>
	</tr>
</table>

