<?php $this->load->view('notifications/header');?>

<br>
Dengan hormat,
<br>
<b><?php echo $vendorname ;?></b>
<br>
<br>
Dengan ini kami informasikan bahwa perusahaan Bapak/Ibu telah berhasil terpilih sebagai pemenang pengadaaan di PT. Garuda Indonesia (Persero). Tbk sebagai vendor untuk <b>Kategori <?php echo $category ;?></b>
<br>
<br>
Berikut kami sampaikan surat pemenang bidding yang dapat diakases di
<br>
<?php echo $link ;?>
<br>
<br>
Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih
<br>
<br>

<?php $this->load->view('notifications/footer');?>