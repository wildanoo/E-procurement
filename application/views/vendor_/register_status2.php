<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   
	   $('#btn_change').click(function(){ 	
	   			   		
	   		var baseUrl = "<?php echo base_url(); ?>";
		 	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
			var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
			var opt     = $("input:radio[name=status]:checked").val();						
			$.ajax({
					url  : baseUrl + 'vendor/change_register_status',
					type : "POST",              		               
					data : csrf + '=' + token +'&opt=' + opt,
					success: function(resp){
						//alert(opt);
						$("#info_status").load(baseUrl+'vendor/load_status_info');
					}
			});		
	   		
	   });
	
   });
    
</script>

<div id="debug_temp"></div>

<div class = "panel panel-info">
   <div class = "panel-heading">
      <h3 class = "panel-title"><?php echo ucwords($vendor->reg_sts); ?></h3>
   </div>
   
   <div class = "panel-body">      
      <?php     
      	foreach($status as $val){
      		if($val->id <= 4){
      			 $next    = $vendor->id_stsreg + 1;
	      		 $checked = ($val->id == $vendor->reg_sts) ? true : false;
	      		 if($val->id == $next) echo form_radio('status', $val->id, true); 				 
				 echo "&nbsp;";
				 if($checked) echo "<b>".ucwords($val->status)."</b>"; 
				 else echo ucwords($val->status);
				 if($val->id != 4) echo "&nbsp;&nbsp;&nbsp;<i class='glyphicon glyphicon-forward'></i>&nbsp;&nbsp;&nbsp;";
			} 
		}      
      ?>
      <input type="button" id="btn_change" value="Submit" class="btn btn-warning btn-sm"/>
   </div>
</div>

