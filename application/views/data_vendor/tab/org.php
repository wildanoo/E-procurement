<div id="container7">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	
   
   $('#btn_create7').click(function(){  
   		$("#container7").load(baseUrl+'data_vendor/form_org');   		
   });             	
   
 }); 
 
 function update_person(id){
 	
 	$("#container7").load(baseUrl+'data_vendor/form_update_org/'+id); 
 }
 
 function delete_person(id){
 	var baseUrl = "<?php echo base_url(); ?>";
 	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
 	
 	if (confirm('Are you sure you want to delete person?')) {	
		$.post( baseUrl + "data_vendor/delete_person", { csrf: token, id: id } );
		$("#container7").load(baseUrl+'data_vendor/load_org', function(){$('#org_msg').html('Data akan berubah setelah proses approval disetujui');});		
	}
	
 } 
  
 </script>
<div id="org_msg" class="bg-warning text-center text-danger"></div>
<?php if($this->permit->upven){ ?> 

<div style="text-align: right;">
<button type="button" id="btn_create7" class="btn btn-info btn-primary">Add New</button>
</div>
<?php } ?>
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Name</th>
		<th>Address</th>
		<th>Phone</th>
		<th>Position</th>
		<th>Remark</th>
		<?php if($this->permit->upven){ ?><th>Action</th><?php } ?>
	</tr>
	</thead>
	<tbody>
 <?php  //$org = array();
 if($org){ $i=1;
 	foreach($org as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->name; ?></td>	
			<td><?php echo $row->address; ?></td>
			<td><?php echo $row->phone; ?></td>
			<td><?php echo $row->position; ?></td>			
			<td><?php echo $row->remark ?></td>
			<?php if($this->permit->upven){ ?>
			<td style="text-align: center;">
			<?php
					echo '<a href="javascript:void(0)"><i class="fa fa-pencil" onclick="update_person('.$row->id.')"></i></a> | '; 				
					echo '<a href="javascript:void(0)"><i class="fa fa-trash"  onclick="delete_person('.$row->id.')"></i></a>';	
			?></td>	
			<?php } ?>										
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>