<div style="margin-top:-85px;position:absolute;">&nbsp;
    <a name="news"><span></a>
</div>
<div class="head">
    <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;"><?php echo $this->lang->line('section2');?></h1>
    <a href="#news" class="next-section about animate2">
        <i class="fa fa-angle-double-down"></i>
    </a>
</div>
<div class="container">
  <div class="col-xs-12 col-sm-10 col-md-10 col-md-push-1 content-tabs-wrap wow fadeInUp animated" data-wow-delay="1.7s" data-wow-offset="200">
    <div class="content-tabs">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab-home" data-toggle="tab"><?php echo $this->lang->line('section2_sub1');?></a></li>
        <li><a href="#tab-profile" data-toggle="tab"><?php echo $this->lang->line('section2_sub2');?></a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab-home">
          <div class="latest-post-wrap pull-left2 wow fadeInLeft" data-wow-delay="0.5s" data-wow-offset="100">
          <?php if (isset($browse[1]) && count($browse[1]) > 0) {
            foreach ($browse[1] as $content) { ?>
            <div class="post-item-wrap pull-left col-sm-12 col-md-12 col-xs-12">
              <!-- <img src="<?php //echo base_url()?>assets/images/news-1.jpg" class="img-responsive post-author-img" alt="" /> -->
              <div class="post-content1 col-md-9 col-sm-9 col-xs-8">
                <div class="post-title pull-left"><a href="news-detail.html"><?php echo $content->title; ?></a></div>
                <div class="post-meta-top pull-left">
                  <ul>
                    <li>
                      <i class="icon-calendar"></i>
                      <?php
                        $date = str_replace('/', '-', $content->publish_date);
                        echo date('M d, Y h:i:s A', strtotime($date));
                      ?>
                    </li>
                  </ul>
                </div>
              </div>
              <!-- <div class="post-content2 pull-left"> -->
              <div class="post-content2">
                <p><?php echo $content->detail; ?><br />
                <span class="post-meta-bottom"><a href="<?php echo base_url();?>webcontent/page/<?php echo $content->id;?>"><?php echo $this->lang->line('section2_sub3');?></a></span></p>
              </div>
            </div>
          <?php } ?> 
            <div class="post-item-wrap pull-left col-sm-12 col-md-12 col-xs-12">
               <a href="<?php echo base_url()?>webcontent/archive/news" class="dept-details-butt posts-showall2"><?php echo $this->lang->line('section2_sub5');?></a>
            </div>
          <?php }else{ echo "<div class='post-item-wrap pull-left col-sm-12 col-md-12 col-xs-12'>No Event(s) yet.</div>"; }?>
          </div>
        </div>
        <div class="tab-pane fade" id="tab-profile">
          <div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active" id="ui-accordion-imedica-dep-accordion-panel-0" aria-labelledby="ui-accordion-imedica-dep-accordion-header-0" role="tabpanel" aria-expanded="true" aria-hidden="false" style="display: block;">                    
            <?php if (isset($browse[2]) && count($browse[2]) > 0) {
            foreach ($browse[2] as $content) { ?>
              <!-- <img src="<?php //echo base_url()?>assets/images/dep-dummy.jpg" class="img-responsive dept-author-img-desk col-md-4" alt=""> -->
              <div class="post-item-wrap pull-left2 col-md-12 col-lg-12">
                <div class="dept-title pull-left"><?php echo $content->title; ?></div>
                <div class="post-meta-top pull-left">
                  <ul>
                    <li>
                      <i class="icon-calendar"></i>
                      <?php
                        $date = str_replace('/', '-', $content->publish_date);
                        echo date('M d, Y h:i:s A', strtotime($date));
                      ?>
                    </li>
                  </ul>
                </div>
                <p><?php echo $content->detail; ?></p>
                <a class="dept-details-butt" href="<?php echo base_url();?>webcontent/page/<?php echo $content->id;?>"><?php echo $this->lang->line('section2_sub4');?></a>
                <div class="vspacer"></div>
              </div>
            <?php } ?> 
              <div class="post-item-wrap pull-left col-sm-12 col-md-12 col-xs-12">
                 <a href="<?php echo base_url()?>webcontent/archive/announcement" class="dept-details-butt posts-showall2"><?php echo $this->lang->line('section2_sub5');?></a>
              </div>
            <?php }else{ echo "<div class='post-item-wrap pull-left col-sm-12 col-md-12 col-xs-12'>No Event(s) yet.</div>"; }?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>