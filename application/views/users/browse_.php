 <script type="text/javascript"> 
	$(function(){
		var baseUrl   = "<?php echo base_url(); ?>";	
		
		$("#srch-term").autocomplete({
			source: baseUrl + 'user/get_tags',
			minLength:1
		});		
		
 	});			
</script>
<script src="<?php echo base_url()?>assets/js/sorttable.js"></script>
 
 <div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Users</h3>
              </div><!-- /.box-header -->
              
                <br>
                <div class="col-md-2 pull-left">
                    <!--a href="<?php echo base_url(); ?>auth/register"><button class="btn btn-sm btn-primary" id = "btn-pd">New User</button></a-->
                    <?php  $prop = array('class'=>'btn btn-sm btn-primary','title'=>'New User'); ?>					 
					<?php  echo anchor('auth/register','New User',$prop); ?>
                    
                </div>
                
                <div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('user/search',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>user" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		            <?php echo form_close();?>
		        </div>
		        </div>        
                

                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover text-center sortable">
                    <thead>
                      <tr>
                        <!--th><input type="checkbox" name="cba[]" id="cba" /></th-->
                        <th class="table-sorted-reverse">No</th>
                        <th class="">Username</th>
                        <th class="">Email</th>
                        <th class="">Group</th>                       
                        <!--th>Is Admin</th-->
                        <th class="">Created Date</th>
                        <th class="">Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>

                    <tbody>
                                        <?php
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td><?php  echo $num = $num+1;; ?></td>
				<td style="text-align: left;"><?php echo ucwords($row->username); ?></td>
				<td><?php echo $row->email; ?></td>	
				<td><?php 
						$group_name = !$row->group_name ? "-" : $row->group_name;
						echo $group_name;
				?></td>				
				<!--td><?php   
					/*$title = $row->is_admin==1 ? "Y" : "N";
					$qst   = $row->is_admin==0 ? "user as Admin" : "user as general";
					$js    = "if(confirm('$qst ?')){ return true; } else {return false; }";					
					echo anchor('user/as_admin/'.$row->id, $title,array('class'=>'default_link','title'=>'Change Admin','onclick'=>$js));*/
				?></td-->						
				<td><?php echo $row->created; ?></td>
				<td><?php   
					$title = $row->activated==1 ? "Active" : "Not Active";
					$qst   = $row->activated=="1" ? "Unactivated" : "Activated";
					$js    = "if(confirm('$qst User ?')){ return true; } else {return false; }";					
					echo anchor('user/activated/'.$row->id, $title,array('class'=>'default_link','title'=>'Change Status','onclick'=>$js));
				?></td>			
				<td><?php			
										
					$trash  = '<i class="fa fa-trash"></i>';
					$plus   = '<i class="fa fa-plus"></i>';
					$js     = "if(confirm('Are you sure to delete user ?')){ return true; } else {return false; }";
					echo anchor('user/set_user_sess/'.$row->id, $plus,array('class'=>'default_link','title'=>'Select Group'));
					echo anchor('user/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete User','onclick'=>$js));?>
				</td>
				
			</tr>
<?php	$i++;  } ?>	
</table>
<tr><div colspan="9" style="text-align: center;"><?php echo $pagination;?></div></tr>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>  
	</table>  
                </div><!-- /.box-body -->
              </form>
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->