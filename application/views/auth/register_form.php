<?php $contentID = $this->session->userdata("contentID"); 
	  $check     = !$contentID ? "1" : $contentID;
	  //1=GA 2=Others
?>
<script type="text/javascript">
$(document).ready(function(){
   var baseUrl  = "<?php echo base_url(); ?>";	
   $(function () { //$("#ga_emp").hide();
   			var check = '<?php echo $check; ?>';
   			if(check=="2") {
				$("#ga_emp").hide();
				$('input[name=content][value="2"]').prop('checked',true);
			}   			
   			
   			$("#nopeg").prop('required',true); 
			$("#fullname").prop('required',true);
			$("#id_office").prop('required',true);
			$("#id_division").prop('required',true);	
			$("#id_dept").prop('required',true);
			$("#id_position").prop('required',true);	
   });
   
   $("input:radio[name=content]").click(function () { 
		var opt = $("input:radio[name=content]:checked").val(); 
		if (opt == '2'){ $("#ga_emp").hide();
				$("#nopeg").prop('required',false); 
				$("#fullname").prop('required',false);
				$("#id_office").prop('required',false);
				$("#id_division").prop('required',false);	
				$("#id_dept").prop('required',false);
				$("#id_position").prop('required',false);	 
		} else {	
			$("#ga_emp").show();		
			$("#nopeg").prop('required',true); 
			$("#fullname").prop('required',true);
			$("#id_office").prop('required',true);
			$("#id_division").prop('required',true);	
			$("#id_dept").prop('required',true);
			$("#id_position").prop('required',true);			
		}
    });

   $('#id_office').val('');
   $('#id_position').val('');

   $('#id_office').change(function(){  
	   		var id_office = $('#id_office').val(); //alert(id_office);
	   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	           $.ajax({
	               url  : baseUrl + 'auth/get_division',
	               type : "POST",              		               
	               data : csrf +'='+ token +'&id_office='+id_office,
	               success: function(resp){
	               	  $('#id_division').html(resp);
	               	  //$('#debug').html(resp);
	               }
	            });
	     });

   $('#id_division').change(function(){  
	   		var id_division = $('#id_division').val(); 
	   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	           $.ajax({
	               url  : baseUrl + 'auth/get_department',
	               type : "POST",              		               
	               data : csrf +'='+ token +'&id_division='+id_division,
	               success: function(resp){
	               	  $('#id_dept').html(resp);  
	               	  //$('#debug').html(resp);             	  
	               }
	            });
	     });
	     
   $('#btn_confirm').click(function(){ 
			var nopeg   = $('#nopeg').val();
			var id_func = $('#id_functional').val();
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	        var opt     = $("input:radio[name=content]:checked").val();
	        if (opt == '1'){
	        $.post( baseUrl + 'auth/validate_nopeg', { csrf: token, nopeg : nopeg }).done(function( resp ) {
			    	var json = $.parseJSON(resp);	
					if(json.status=="false"){
						$('#msg_nopeg').html('Employee Number has been used or Empty !')
					} else { 	$("#btn_submit").trigger("click");	}	
			});
			} else {
				if(id_func!=0) { 
					$("#btn_submit").trigger("click"); 
				} else { alert('user functional must be specific role !');	} 	
			}
	});

   $('#btn_cancel').click(function(){  
   		document.location.href= baseUrl + 'user/';
   });

}); 
</script> 
<div id="debug"></div> 

<?php
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'class' => 'form-control',
		'placeholder' => 'Username',
		'required' => true,
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
		'size'	=> 30,
	);
}
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'class' => 'form-control',
	'placeholder' => 'Email',
	'required' => true,
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
);

$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'class' => 'form-control',
	'placeholder' => 'Password',
	'required' => true,
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'class' => 'form-control',
	'placeholder' => 'Confirm Password',
	'required' => true,
	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
?>
 
 <h1>Create New User</h1><hr />
 
<?php echo form_open($this->uri->uri_string(),array('class'=>'form-horizontal','id'=>'form_register')); ?>
<table width="100%">
	<tr>
		<td style="width:250px;"><?php echo form_label('Username', $username['id']); ?> </td>
		<td><?php echo form_input($username); ?></td>
		<td>
			<?php echo form_error($username['name']); ?>
            <?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?>
		</td>
	</tr>
	<tr>
		<td><?php echo form_label('Email Address', $email['id']); ?></td>
		<td><?php echo form_input($email); ?></td>
		<td>
			<?php echo form_error($email['name']); ?>
            <?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?>
		</td>
	</tr>	
	<tr>
		<td><?php echo form_label('Password', $password['id']); ?></td>
		<td colspan="2">
			<?php echo form_password($password); ?>
            <?php echo form_error($password['name']); ?>            	
        </td>		
	</tr>	
	<tr>
		<td><?php echo form_label('Confirm Password', $confirm_password['id']); ?></td>
		<td colspan="2">
			<?php echo form_password($confirm_password); ?>
            <?php echo form_error($confirm_password['name']); ?>            	
        </td>		
	</tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
		<td>Employee</td>
		<td colspan="2">
			 <input type="radio" name="content" value="1" checked>&nbsp;&nbsp;GA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  	 <input type="radio" name="content" value="2">&nbsp;&nbsp;Others<br>
		</td>		
	</tr>	
</table><br/>

<table id="ga_emp" width="100%" style="font-weight: bold;">
	<tr>
		<td style="width:250px;">Employee Number</td>
		<td><input type="text" class="form-control" id="nopeg" name="nopeg" placeholder="Employee Number"></td>
	</tr>
	<tr><td>&nbsp;</td><td colspan="2" id="msg_nopeg">&nbsp;</td></tr>	
	<tr>
		<td>Employee Name</td>
		<td colspan="2"><input type="text" class="form-control" id="fullname" name="fullname" placeholder="Employee Name"></td>
	</tr>	
	<tr>
		<td>Office</td>
		<td colspan="2">
			<?php $prop_office = 'id="id_office" class="form-control"';
          	echo form_dropdown("id_office",$office,'default',$prop_office);
          	?>
		</td>		
	</tr>	
	<tr>
		<td>Division</td>
		<td colspan="2">
			 <?php $prop_div = 'id="id_division" class="form-control"';
          	 echo form_dropdown("id_division",$division,'default',$prop_div);?>
		</td>		
	</tr>	
	<tr>
		<td>Departmen</td>
		<td colspan="2">
			<?php $prop_dept = 'id="id_dept" class="form-control"';
          	echo form_dropdown("id_dept",$department,'default',$prop_dept);?>
		</td>		
	</tr>	
	<tr>
		<td>Position</td>
		<td colspan="2">
			<?php $prop_position = 'id="id_position" class="form-control"';
          	echo form_dropdown("id_position",$position,'default',$prop_position);?>
		</td>		
	</tr>	
</table>

<table width="100%">
	<tr>
		<td style="width:250px;">Functional</td>
		<td colspan="2">
			<?php $prop_functional = 'id="id_functional" class="form-control"';
          	echo form_dropdown("id_functional",$functional,'default',$prop_functional);?>
		</td>		
	</tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
		<td colspan="3" style="text-align: right;">			 	
			 <?php echo form_submit('register', 'Register','class="btn btn-info btn-danger" style="visibility:hidden;" id="btn_submit" '); ?>
             <input type="button" id="btn_confirm" name="btn_confirm" style="width:100px;" class="btn btn-primary btn-sm" value="Submit" />
             <input type="button" id="btn_cancel" name="btn_confirm" style="width:100px;" class="btn btn-default btn-sm" value="Cancel" />

		</td>		
	</tr>
</table>
<?php echo form_close(); ?>











 
 