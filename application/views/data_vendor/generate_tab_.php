<?php
	$ses_tab = $this->session->userdata('tab');
	$tab = !$ses_tab ? "home" : $ses_tab ;
?>

<script type="text/javascript">
   $(document).ready(function(){

		activaTab('<?php echo $tab; ?>');
   });

   function activaTab(tab){
    	$('.nav-tabs a[href="#' + tab + '"]').tab('show');
   };

  function view_logs(id)
  {
    var purl = baseUrl+'data_vendor/view_logs2';
    $.post(purl, {'tb_name': id}, function(data, textStatus, xhr) {
      console.log(data);
      $('#tabs_container').html(data);
    });

  }


</script>
<div class="panel panel-default">
<div class="panel-heading"></div>
  <div class="panel-body">
     <div class="form-group">
      <label for="inputEmail3" class="col-xs-6 control-label">Supplier Name</label>
      <div class="col-xs-6">
        <label for="">: <?=$vendor->name?></label>
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail3" class="col-xs-6 control-label">Registration Code</label>
      <div class="col-xs-6">
        <label for="">: <?=$vendor->reg_num?></label>
      </div>
    </div>
  </div>
</div>
<div id="tabs_container">

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">General information</a></li>
  <li><a data-toggle="tab" href="#menu1">Bank Account</a></li>
  <li><a data-toggle="tab" href="#menu2">Akta Pendirian</a></li>
  <li><a data-toggle="tab" href="#menu8">Categories</a></li>
</ul>

<?php $this->session->unset_userdata('tab');  ?>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <h3>General Information</h3>
    <p><?php  $this->load->view('data_vendor/tab/general'); ?></p>
  </div>
  <div id="menu1" class="tab-pane fade">
    <h3>Bank Account</h3>
    <p><?php  $this->load->view('data_vendor/tab/bank_account'); ?></p>
  </div>
  <div id="menu2" class="tab-pane fade">
    <h3>Akta Pendirian</h3>
    <p><?php  $this->load->view('data_vendor/tab/akta'); ?></p>
  </div>
 <div id="menu8" class="tab-pane fade">
    <h3>Categories</h3>
    <p><?php  $this->load->view('data_vendor/tab/categories'); ?></p>
  </div>
</div>

</div>


