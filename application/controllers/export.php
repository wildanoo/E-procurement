<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export extends CI_Controller {
	
	
	public function __construct()
	{
		parent::__construct();	islogged_in();	
    }

	public function index(){
		header('location: '.base_url());
	}

	function create_data_vendor($ID){ 
		
		$getID   = $this->uri->segment(3);
		$venID   = !$ID ? $getID : $ID;
		$select	 = "id,register_num as regNum, vendor_name as name,vendor_type as type,vendor_address as address,					
 					postcode,web,phone,fax,email,npwp,npwp_address,npwp_postcode"; 					
		$vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);	

		$this->load->library('PHPExcel');	$obj = new PHPExcel();
		
		$objWorkSheet   = $obj->createSheet(0);
		$headerStylenya = new PHPExcel_Style();
		$bodyStylenya   = new PHPExcel_Style();		
		$objWorkSheet->setShowGridlines(false);
		$objWorkSheet->setTitle("General Information");

		$objWorkSheet	->setCellValue('A1', 'General Information')		 				
		 				->setCellValue('A3', 'Registration Num.')
		 				->setCellValue('B3', 'Vendor Name')
		 				->setCellValue('C3', 'Vendor Type')
		 				->setCellValue('D3', 'Address')
		 				->setCellValue('E3', 'Post Code')
		 				->setCellValue('F3', 'NPWP')
		 				->setCellValue('G3', 'NPWP Address')
		 				->setCellValue('H3', 'NPWP Post Code')
		 				->setCellValue('I3', 'Phone')
		 				->setCellValue('J3', 'Fax')
		 				->setCellValue('K3', 'Email')
		 				->setCellValue('L3', 'Web')

		 				->setCellValue('A4', $reg_num)
		 				->setCellValue('B4', $vendor->name)
		 				->setCellValue('C4', $vendor->type)
		 				->setCellValue('D4', $vendor->address)
		 				->setCellValue('E4', $vendor->postcode)
		 				->setCellValue('F4', '\''.$vendor->npwp)
		 				->setCellValue('G4', $vendor->npwp_address)
		 				->setCellValue('H4', $vendor->npwp_postcode)
		 				->setCellValue('I4', '\''.$vendor->phone)
		 				->setCellValue('J4', '\''.$vendor->fax)
		 				->setCellValue('K4', $vendor->email)
		 				->setCellValue('L4', $vendor->web);
		//----------------------------------------------------
		$objWorkSheet = $obj->createSheet(1);
		$objWorkSheet->setTitle("Contact Person");
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(25);
		$objWorkSheet->getColumnDimension('C')->setWidth(20);
		$objWorkSheet->getColumnDimension('D')->setWidth(16);
		$objWorkSheet->getColumnDimension('E')->setWidth(30);
		
		$browse = $this->crud->browse("","contact_person","id_vendor",$venID,"true");
		
		$objWorkSheet	->setCellValue('A1', 'Contact Person')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Name')
		 				->setCellValue('C4', 'Position')
		 				->setCellValue('D4', 'Mobile')
		 				->setCellValue('F4', 'Email');

		 if($browse){ $i=1;
 			foreach($browse as $row){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row->fullname)
		 						->setCellValue('C'.(4 + $i), $row->position)
		 						->setCellValue('D'.(4 + $i), '\''.$row->mobile)
		 						->setCellValue('E'.(4 + $i), $row->email);
 				$i++;  
 			}
 		}
		//----------------------------------------------------
		$objWorkSheet = $obj->createSheet(2);
		$objWorkSheet->setTitle("Bank Account");
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(20);
		$objWorkSheet->getColumnDimension('C')->setWidth(25);
		$objWorkSheet->getColumnDimension('D')->setWidth(40);
		$objWorkSheet->getColumnDimension('E')->setWidth(25);
		$objWorkSheet->getColumnDimension('F')->setWidth(25);		 
			
		$bank 	= $this->crud->browse("","bank_account","id_vendor",$venID,"true");	
		$objWorkSheet	->setCellValue('A1', 'Bank Account')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Account Number')
		 				->setCellValue('C4', 'Account Name')
		 				->setCellValue('D4', 'Account Address')
		 				->setCellValue('E4', 'Bank Name')
		 				->setCellValue('F4', 'Branch Name');

		if($bank){ $i=1;
 			foreach($bank as $row1){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), '\''.$row1->acc_number)
		 						->setCellValue('C'.(4 + $i), $row1->acc_name)
		 						->setCellValue('D'.(4 + $i), $row1->acc_address)
		 						->setCellValue('E'.(4 + $i), $row1->bank_name)
		 						->setCellValue('F'.(4 + $i), $row1->branch_name);
 				$i++;  
 			}
 		} 		
		//----------------------------------------------------
		$objWorkSheet = $obj->createSheet(3);
		$objWorkSheet->setTitle("Akta Pendirian");
		$objWorkSheet->getColumnDimension('A')->setWidth(5);
		$objWorkSheet->getColumnDimension('B')->setWidth(25);
		$objWorkSheet->getColumnDimension('C')->setWidth(40);
		$objWorkSheet->getColumnDimension('D')->setWidth(20);
		$objWorkSheet->getColumnDimension('E')->setWidth(20);
		$objWorkSheet->getColumnDimension('F')->setWidth(20);
		$objWorkSheet->getColumnDimension('G')->setWidth(20);

		$objWorkSheet->setSharedStyle($headerStylenya, "A4:G4");
		$objWorkSheet->setSharedStyle($bodyStylenya, "A5:G6");
		$objWorkSheet->setShowGridlines(false);

 		$none_akta 		= array('notaris_name'		=> "none",
							 	'notaris_address'	=> "none",
							 	'notaris_phone'		=> "none",
							 	'notaris_number'	=> "none",
							 	'notaris_date'		=> date('Y-m-d'),
							 	'remark'			=> "none");
 		
 		$akta  = $this->crud->browse("","akta","id_vendor",$venID,"false");	
 		$akta2   = !$akta ? (object) $none_akta : $akta;	 

		$objWorkSheet	->setCellValue('A1', 'Akta Pendirian')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)		 				
		 				->setCellValue('A4', 'No.')
		 				->setCellValue('B4', 'Notaris Name')
		 				->setCellValue('C4', 'Notaris Address')
		 				->setCellValue('D4', 'Notaris Phone')
		 				->setCellValue('E4', 'Notaris Number')
		 				->setCellValue('F4', 'Notaris Date')
		 				->setCellValue('G4', 'Remark')
		 				->setCellValue('A5', '1')
		 				->setCellValue('B5', $akta2->notaris_name)
		 				->setCellValue('C5', $akta2->notaris_address)
		 				->setCellValue('D5', '\''.$akta2->notaris_phone)
		 				->setCellValue('E5', '\''.$akta2->notaris_number)
		 				->setCellValue('F5', $akta2->notaris_date)
		 				->setCellValue('G5', $akta2->remark);
		 				

		$objWorkSheet = $obj->createSheet(4);
		$objWorkSheet->setTitle("Owner");
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(25);
		$objWorkSheet->getColumnDimension('C')->setWidth(40);
		$objWorkSheet->getColumnDimension('D')->setWidth(20);
		$objWorkSheet->getColumnDimension('E')->setWidth(20);
		$objWorkSheet->getColumnDimension('F')->setWidth(20);
		$objWorkSheet->getColumnDimension('G')->setWidth(20);
		 
		$owner = $this->crud->browse("","owner","id_vendor",$venID,"true");			

		 $objWorkSheet	->setCellValue('A1', 'Owner')
		 				->setCellValue('A2', 'Vendor Name : '.$vendor->name)		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Owner Name')
		 				->setCellValue('C4', 'Owner Address')
		 				->setCellValue('D4', 'Owner Phone')
		 				->setCellValue('E4', 'Owner Position')
		 				->setCellValue('F4', 'Owner Shared')
		 				->setCellValue('G4', 'Remark');

		if($owner){ $i=1;
 			foreach($owner as $row2){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row2->owner_name)
		 						->setCellValue('C'.(4 + $i), $row2->owner_address)
		 						->setCellValue('D'.(4 + $i), '\''.$row2->owner_phone)
		 						->setCellValue('E'.(4 + $i), $row2->owner_position)
		 						->setCellValue('F'.(4 + $i), $row2->owner_shared)
		 						->setCellValue('G'.(4 + $i), $row2->remark);
 				$i++;  
 			}
 		}
		//----------------------------------------------------
		$objWorkSheet = $obj->createSheet(5);
		$objWorkSheet->setTitle("Pengurus");
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(25);
		$objWorkSheet->getColumnDimension('C')->setWidth(40);
		$objWorkSheet->getColumnDimension('D')->setWidth(20);
		$objWorkSheet->getColumnDimension('E')->setWidth(20);
		$objWorkSheet->getColumnDimension('F')->setWidth(20);
		 
		$org = $this->crud->browse("","organization","id_vendor",$venID,"true");	

		 $objWorkSheet	->setCellValue('A1', 'Pengurus')
		 				->setCellValue('A2', 'Vendor Name : '.$vendor->name)		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Name')
		 				->setCellValue('C4', 'Address')
		 				->setCellValue('D4', 'Phone')
		 				->setCellValue('E4', 'Position')
		 				->setCellValue('F4', 'Remark');

		if($org){ $i=1;
 			foreach($org as $row3){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row3->name)
		 						->setCellValue('C'.(4 + $i), $row3->address)
		 						->setCellValue('D'.(4 + $i), '\''.$row3->phone)
		 						->setCellValue('E'.(4 + $i), $row3->position)
		 						->setCellValue('F'.(4 + $i), $row3->remark);
 				$i++;  
 			}
 		}
		//----------------------------------------------------
		$objWorkSheet = $obj->createSheet(6);
		$objWorkSheet->setTitle("Correspondences");
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(40);
		$objWorkSheet->getColumnDimension('C')->setWidth(20);
		$objWorkSheet->getColumnDimension('D')->setWidth(40);
		$objWorkSheet->getColumnDimension('E')->setWidth(10);
		$objWorkSheet->getColumnDimension('F')->setWidth(10);
		$objWorkSheet->getColumnDimension('G')->setWidth(20);
		 
		$correspondence = $this->crud->browse("","correspondence","id_vendor",$venID,"true");		
		$objWorkSheet	->setCellValue('A1', 'Correspondence')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)
		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Document Name')
		 				->setCellValue('C4', 'Document Number')
		 				->setCellValue('D4', 'Description')
		 				->setCellValue('E4', 'Effective Date')
		 				->setCellValue('F4', 'Expired Date')
		 				->setCellValue('G4', 'Remark');

		if($correspondence){ $i=1;
 			foreach($correspondence as $row4){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row4->doc_name)
		 						->setCellValue('C'.(4 + $i), '\''.$row4->doc_num)
		 						->setCellValue('D'.(4 + $i), $row4->desc)
		 						->setCellValue('E'.(4 + $i), $row4->eff_date)
		 						->setCellValue('F'.(4 + $i), $row4->exp_date)
		 						->setCellValue('G'.(4 + $i), $row4->remark);
 				$i++;  
 			}
 		}
		//----------------------------------------------------
		$objWorkSheet = $obj->createSheet(7);
		$objWorkSheet->setTitle("Reference");
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(25);
		$objWorkSheet->getColumnDimension('C')->setWidth(20);
		$objWorkSheet->getColumnDimension('D')->setWidth(10);
		$objWorkSheet->getColumnDimension('E')->setWidth(10);
		$objWorkSheet->getColumnDimension('F')->setWidth(20);
		 
		$ref = $this->crud->browse("","references","id_vendor",$venID,"true");		
		$objWorkSheet	->setCellValue('A1', 'Reference')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Customer Name')
		 				->setCellValue('C4', 'Project')
		 				->setCellValue('D4', 'Point')
		 				->setCellValue('E4', 'Date')
		 				->setCellValue('F4', 'Remark');

		if($ref){ $i=1;
 			foreach($ref as $row5){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row5->cust_name)
		 						->setCellValue('C'.(4 + $i), $row5->project)
		 						->setCellValue('D'.(4 + $i), $row5->point)
		 						->setCellValue('E'.(4 + $i), $row5->date)
		 						->setCellValue('F'.(4 + $i), $row5->remark);
 				$i++; 
 			}
 		}
		//----------------------------------------------------
		$objWorkSheet = $obj->createSheet(8);
		$objWorkSheet->setTitle("Affiliate");
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(30);
		$objWorkSheet->getColumnDimension('C')->setWidth(40);
		$objWorkSheet->getColumnDimension('D')->setWidth(20);
		$objWorkSheet->getColumnDimension('E')->setWidth(20);
		 
		$aff = $this->crud->browse("","affiliates","id_vendor",$venID,"true");		

		$objWorkSheet	->setCellValue('A1', 'Affiliate')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Company Name')
		 				->setCellValue('C4', 'Competency')
		 				->setCellValue('D4', 'Contact')
		 				->setCellValue('E4', 'Remark');

		if($aff){ $i=1;
 			foreach($aff as $row6){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row6->company_name)
		 						->setCellValue('C'.(4 + $i), $row6->competency)
		 						->setCellValue('D'.(4 + $i), $row6->contact)
		 						->setCellValue('E'.(4 + $i), $row6->remark);
 				$i++;  
 			}
 		}		 
		//----------------------------------------------------
		$objWorkSheet = $obj->createSheet(9);
		$objWorkSheet->setTitle("Categories");
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(40);
		$objWorkSheet->getColumnDimension('C')->setWidth(40);
		 
		$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
			   		(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";					
 		$categories = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2);		
		$objWorkSheet	->setCellValue('A1', 'Categories')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Category')
		 				->setCellValue('C4', 'Sub Category');

		if($categories){ $i=1;
 			foreach($categories as $row6){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row6->category)
		 						->setCellValue('C'.(4 + $i), $row6->subcategory);
 				$i++;  
 			}
 		}
		//----------------------------------------------------
		$obj->setActiveSheetIndex(0);
		$filename= $vendor->regNum.'.xlsx';		
		
		if($ID){	
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			$objWriter->save(str_replace(__FILE__,'temp_excel/'.$filename,__FILE__)); 
		} else {		
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename='.$filename);
			header('Cache-Control: max-age=0');
			 
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			$objWriter->save('php://output');				
		}
	}

    function multivendor(){
    	
		$i=0;
		foreach($_POST['selected'] as $val){	//echo $val;  		
			$this->create_data_vendor($val);
			$vendor   = $this->crud->browse("","vendor","id",$val,"false","register_num as regNum");
			$filename = $vendor->regNum.'.xlsx';
			$valid_files[$i] = $filename ; 
		$i++; }			
		
		$nama = uniqid();
		$zip  = new ZipArchive();
		$dir  = 'temp_excel/';
		$filename = $dir . $nama .'.zip';

		if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
			    exit("cannot open <$filename>\n");	}
		
		for ($n = 0; $n < $i; $n++){
            $zip->addFile($dir . $valid_files[$n], $valid_files[$n]);
        }
		$zip->close();
		
		for ($n = 0; $n < $i; $n++){ @unlink($dir.$valid_files[$n]); }
				
		$url = base_url($filename);
		
		$array['url']  =  $url;	
		$array['path']  =  $filename;	
		echo json_encode($array);		
	}
	
	function clean_file(){
		$path = $this->input->post('path');
		//f (file_exists($path)) unlink($path);				
	}


}