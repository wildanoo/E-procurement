<?php $ses_clps = $this->session->flashdata('clps'); 
	  $clps     = !$ses_clps ? "collapse1" : $ses_clps ; ?>

<script type="text/javascript">
$(function () { 
	 var clps  = "<?php echo $clps; ?>";
	 if(clps!="collapse1"){
	 	$('#collapse1').collapse('hide');
		$('#'+ clps ).collapse('show');
	 }		
}); 
</script>

<div class="bs-example">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">1. Category</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p><?php  $this->load->view('vendor/detail/categories'); ?></p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">2. Contact Person</a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('vendor/detail/contact'); ?></p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">3. Bank Account</a>
                </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('vendor/detail/bank_account'); ?></p>
                </div>
            </div>
        </div>
		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">4. Akta Pendirian</a>
                </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('vendor/detail/akta'); ?></p>
                </div>
            </div>
        </div>
		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">5. Owner</a>
                </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('vendor/detail/owner'); ?></p>
                </div>
            </div>
        </div>
		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">6. Pengurus</a>
                </h4>
            </div>
            <div id="collapse6" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('vendor/detail/org'); ?></p>
                </div>
            </div>
        </div>
		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">7. Surat Legalitas</a>
                </h4>
            </div>
            <div id="collapse7" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('vendor/detail/correspondence'); ?></p>
                </div>
            </div>
        </div>
		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">8. Referensi</a>
                </h4>
            </div>
            <div id="collapse8" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('vendor/detail/reference'); ?></p>
                </div>
            </div>
        </div>
		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">9. Afiliasi</a>
                </h4>
            </div>
            <div id="collapse9" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('vendor/detail/affiliate'); ?></p>
                </div>
            </div>
        </div>
		<?php if($this->global->register=="6"){  ?>		
		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">10. Accounting Information</a>
                </h4>
            </div>
            <div id="collapse10" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('vendor/verification/list'); ?></p>
                </div>
            </div>
        </div>
		<?php } ?>
    </div>
	<p><strong>Note:</strong> Click on the linked heading text to expand or collapse accordion panels.</p>
</div>