<div class="page-header">
  <h3>Content Detail</h3>
</div> 
<section class="form_tab_content" width="75%;">
  <div style="margin-top: -5px;">
    <ul class="nav nav-tabs test2">
        <li class="active"><a data-toggle="tab" href="#t_idn" aria-expanded="true">Indonesia</a></li>
        <li class=""><a data-toggle="tab" href="#t_eng" aria-expanded="false">English</a></li>
    </ul>
    <?php echo form_open_multipart('announce/preview_update',array('id'=>'form'));?>
    <?php echo form_hidden('id',$browse->id); ?>
    <?php echo form_hidden('subcontent_type',$browse->subcontent_type); ?>
    <?php echo form_hidden("creator",$username); ?>
    <div class="tab-content">
        <div id="t_idn" class="tab-pane fade active in" style="padding:15px;">
            <div class="page-header col-md-12" style="border: none;">
              <h2><?php echo $browse->title_ind; ?></h2>
              <h6 style="font-size: 13px; text-transform: none;"><?php echo $browse->subtype_name; ?> - <?php 
              $date = str_replace('/', '-', $browse->publish_date);
              echo date('M d, Y H:i A', strtotime($date));
              echo '';?></h6>
            </div>
            <div class="col-md-12 page-content">
              <div class="thumbnail col-md-8" style="margin: 0 auto !important;float: none !important;">
                  <img alt="" src="<?php echo base_url(); ?>uploads/images/<?php echo $browse->file;?>.jpg">
              </div>
              <br>
              <p><?php echo isset($contents_ind)? $contents_ind:'[no description]' ; ?></p>
              <br>
            </div>
        </div>
        <div id="t_eng" class="tab-pane fade" style="padding:15px;">
          <div class="page-header col-md-12" style="border: none;">
            <h2><?php echo $browse->title_eng; ?></h2>
            <h6 style="font-size: 13px; text-transform: none;"><?php echo $browse->subtype_name; ?> - <?php 
            $date = str_replace('/', '-', $browse->publish_date);
            echo date('M d, Y H:i A', strtotime($date));
            echo '';?></h6>
          </div>
          <div class="col-md-12 page-content">
            <div class="thumbnail col-md-8" style="margin: 0 auto !important;float: none !important;">
                <img alt="" src="<?php echo base_url(); ?>uploads/images/<?php echo $browse->file;?>.jpg">
            </div>
            <br>
            <p><?php echo isset($contents_eng)? $contents_eng:'[no description]' ; ?></p>
            <br>
          </div>
        </div>
        <div class="tab-content">
        <?php if ($rfpfile) { ?>
          <hr />
          <section>
            <div class="col-md-2"><strong>RFP: </strong></div>
            <div class="col-md-10 row">
              <ul class="list-inline">
                  <li><a href="<?php echo base_url().'announce/openFileRFP/'.$browse->file ?>" target="_blank"><?php echo $browse->file.'.pdf'; ?></a></li>
              </ul>
            </div>
          </section>
          <br>
          <hr />
        <?php }else{echo '<hr/>';} ?>
        <?php if (count($attachment)>0) { ?>
          <hr />
          <section>
            <div class="col-md-2"><strong>Attachment(s): </strong></div>
            <div class="col-md-10 row">
              <ul class="list-inline">
                <?php foreach ($attachment as $val) { ?>
                  <li><a href="<?php echo base_url().'announce/open_file/'.$val ?>" target="_blank"><?php echo $val.'.pdf'; ?></a></li>
                <?php } ?>
              </ul>
            </div>
          </section>
          <br>
          <hr />
        <?php }else{echo '<hr/>';} if ($browse->status == '6') { ?>
          <hr />
          <section style="padding-bottom: 20px;">
            <div class="col-md-2"><strong>Remarks for Rejection: </strong></div>
            <div class="col-md-10 text-left">
              <p><?php echo $browse->remarks_reject?$browse->remarks_reject:'-';?></p>
            </div>
          </section>
          <br>
          <hr />
        <?php }else{echo '<hr/>';}?>
        </div>
        <div class="col-md-12 page-button-section text-right">
          <?php if ($btn_permissions['approve'] && $param_appject) { ?>
            <button style="vertical-align: baseline;" type="submit" value="approve" name="button-process" class="btn btn-success btn-sm"/>Approve</button>
          <?php }?>
      <!-- added by Dedi Rudiyanto  -->
      <?php if ($btn_permissions['rejlang'] && $param_appject) { ?>
       <button data-toggle="modal" data-target="#modalNoLang" style="vertical-align: baseline;" type="button" class="btn btn-warning btn-sm"/>Reject Language</button>
      <?php }?>
      <!------------------------------>
          <?php if ($btn_permissions['reject'] && $param_appject) { ?>
          <button data-toggle="modal" data-target="#modalNo" style="vertical-align: baseline;" type="button" class="btn btn-danger btn-sm"/>Reject</button>
          <?php }?>
          <?php if ($btn_permissions['unpublish'] && $param_unpublish && $param_appject) { ?>
          <button style="vertical-align: baseline;" type="submit" value="unpublish" name="button-process" class="btn btn-primary btn-sm"/>Unpublish</button>
          <?php }?>
          <?php if ($browse->status != '7' AND $btn_permissions['edit'] && ($deptId->id_dept == $browse->id_departement OR ($this->session->userdata('code_groups') == "adm_asyst"))) { ?>
          <button style="vertical-align: baseline;" type="submit" value="edit" name="button-process" class="btn btn-primary btn-sm"/>Edit</button>
          <?php }?>
          <button style="vertical-align: baseline;" type="submit" value="back" name="button-process" class="btn btn-primary btn-sm"/>Back</button>
          <br>
          <br>
        </div>
    </div>
  </div>
  <div id="modalNo" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Remarks for Rejection</h4>
        </div>
        <div class="modal-body">
          <label for="remarks_void"><!-- <strong>Remarks for Rejection:</strong> --></label>
          <textarea name="remarks_reject" id="remarks_void" maxlength="250" cols="20" rows="5" class="form-control" required>&nbsp;</textarea>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" name="button-process" value="reject" style="margin-bottom:0;">Submit</button>
          <button type="button" class="btn btn-danger" style="margin-bottom:0;" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <div id="modalNoLang" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Remarks for Reject English Completion</h4>
        </div>
        <div class="modal-body">
          <label for="remarks_void"><!-- <strong>Remarks for Reject English Completion</strong> --></label>
          <textarea name="remarks_reject_lang" id="remarks_void" maxlength="250" cols="20" rows="5" class="form-control" required>&nbsp;</textarea>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" name="button-process" value="rejlang" style="margin-bottom:0;">Submit</button>
          <button type="button" class="btn btn-danger" style="margin-bottom:0;" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <?php echo form_close();?>
</section>