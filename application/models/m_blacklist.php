<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_blacklist extends CI_Model {

	function authenticate(){      	
	 	$allowed = array(70);	
		$permit  = $this->session->userdata('permissions');
		$permit  = explode(",",$permit);
		foreach($permit as $str){
		$permissions[] = preg_replace("/[^0-9]/","",$str);}
	 	$result        = array_intersect($allowed, $permissions);
	 	
		if( $result ){ return true;
		} return false;
	}

	function record_activity($table,$data)
	{
		$this->db->insert($table,$data);
	}

	function create_zip($files = array(),$destination = '',$overwrite = false) {
		
		if(file_exists($destination) && !$overwrite) { return false; }
		$valid_files = array();

		if(is_array($files))
		{
			foreach($files as $file) 
			{
				if(file_exists($file)) 
				{
					$valid_files[] = $file;
				}
			}
		}

		if(count($valid_files)) 
		{
			$zip = new ZipArchive();
			if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) 
			{
				return false;
			}

			foreach($valid_files as $file)
			{
				$zip->addFile($file,$file);
			}

			$zip->close();

			return file_exists($destination);
		}
		else
		{
			return false;
		}
	}

}