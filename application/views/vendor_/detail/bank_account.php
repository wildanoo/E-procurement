<div id="container2">

<script type="text/javascript">
 $(function () {
   	var baseUrl  = "<?php echo base_url(); ?>";
   	$('#update2').hide();
  	
   	$('#change2').click(function(){	
   	
   		$("#acc_number").css("background","#ffffff");
	    $("#acc_number").css("color","#827e85");             
	    $("#acc_number").prop('readonly',false);
	    
	    $("#acc_name").css("background","#ffffff");
	    $("#acc_name").css("color","#827e85");             
	    $("#acc_name").prop('readonly',false);
	    
	    $("#acc_address").css("background","#ffffff");
	    $("#acc_address").css("color","#827e85");             
	    $("#acc_address").prop('readonly',false);
	    
	    $("#bank_name").css("background","#ffffff");
	    $("#bank_name").css("color","#827e85");             
	    $("#bank_name").prop('readonly',false);
	    
	    
	    $("#branch_name").css("background","#ffffff");
	    $("#branch_name").css("color","#827e85");             
	    $("#branch_name").prop('readonly',false);
	    
	    
   	
   		$('#update2').show();
   		$('#change2').hide();
   		
   	});
   	
   	$('#update2').click(function(){	  	
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
   	    var values = $("#form3").serializeArray();
		values.push({name: csrf,value: token});
		values = jQuery.param(values); 
			$.ajax({
               url  : baseUrl + 'vendor/bank_update',
               type : "POST",              		               
               data : values,
               success: function(resp){
               	  $('#debug2').html(resp);
               	  $("#container2").load(baseUrl+'vendor/load_bank_account');
               }
            });
			
   		$('#update2').hide();
   		$('#change2').show();
   	});
	 	   
 });
 
 </script>
 
<div id="debug2"></div>
<?php echo form_open('#',array('id'=>'form3')); ?>
<table width="100%">
<tr>
<td><label>Account Number</label></td><td>:</td>
<td>	
	<?php $acc_number = array( 'name' => "acc_number" ,
								'id'   => "acc_number", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> ucfirst($bank->acc_number)); 
	 echo form_input($acc_number);	?>
</td>
</tr>
<tr>
<td><label>Account Name</label></td><td>:</td>
<td>
	<?php $acc_name = array( 'name' => "acc_name" ,
							'id'   => "acc_name", 
							'class'=> "form-control", 
							'readonly'=> "true", 
							'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> ucfirst($bank->acc_name)); 
	 echo form_input($acc_name);	?>
</td>
</tr>
<tr>
<td><label>Account Address</label></td><td>:</td>
<td>
	<?php $acc_address = array( 'name' => "acc_address" ,
							'id'   => "acc_address", 
							'class'=> "form-control", 
							'readonly'=> "true", 
							'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> ucfirst($bank->acc_address)); 
	 echo form_input($acc_address);	?>	
</td>
</tr>
<tr>
<td><label>Bank Name</label></td><td>:</td>
<td>
	<?php $bank_name = array( 'name' => "bank_name" ,
							'id'   => "bank_name", 
							'class'=> "form-control", 
							'readonly'=> "true", 
							'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> ucfirst($bank->bank_name)); 
	 echo form_input($bank_name);	?>	
</td>
</tr>
<tr>
<td><label>Branch Name</label></td><td>:</td>
<td><?php $branch_name = array( 'name' => "branch_name" ,
							'id'   => "branch_name", 
							'class'=> "form-control", 
							'readonly'=> "true", 
							'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> ucfirst($bank->branch_name)); 
	 echo form_input($branch_name);	?></td>
</tr>
<tr><td colspan="3">
	 <?php  //$prop = array('class'=>'btn btn-default','title'=>'Cancel'); ?>					 
	 <?php  //echo anchor('glp/','Cancel',$prop); ?>	
	<button type="button" id="change2" class="btn btn-info btn-primary">Change</button> 
    <button type="button" id="update2" class="btn btn-info btn-primary">Update</button>
</td></tr>
</table>
<?php echo  form_close(); ?>
<br/>
</div>