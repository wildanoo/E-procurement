<script type="text/javascript">
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";
	  $(function(){  });

		$('#country').change(function(){
			var country = $('#country').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'country/set_sess_country',
               data : 'field=id&country_name='+country,
               success: function(data){
                   //alert(category);
					 //$('#debug').html(resp);
					 //dialogRef.close();
				 	document.location.href= baseUrl + 'country/';
			   }
            });

		});

	  $("#srch-term").autocomplete({
			source: baseUrl + 'country/get_country_tags',
			minLength:1
	  });

	  $("#btn_search").click(function(){
            var search = $("#srch-term").val();
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>';
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'country/set_sess_search',
               type : "POST",
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){
				   document.location.href= baseUrl + 'country/';
               }
            });
        });

	  $('#btn_create').click(function(){  form_create2();  });
});

	function load_data()
	{
		$('#debug').load(baseUrl+'assesment');
	}

	function form_create2()
	{

		$('#debug').load(baseUrl+'assesment/form_create');

	}

	function form_create(){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>';
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
			BootstrapDialog.show({
			title: 'Create Assesment',
            message: $('<div></div>').load(baseUrl + "assesment/form_create/"),
            buttons: [{ label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',
		                action: function(dialogRef){
		                	 var description = $("#description").val();

		                	 $.post( baseUrl + 'assesment/is_exist', { csrf: token , description : description })
							 .done(function( resp ) {

							var json = $.parseJSON(resp);
							if (json.status=='true'){
			                	var values = $("#form").serializeArray();
			                	values.push({ name: csrf,value: token });
								values = jQuery.param(values);
								 $.ajax({
						               url  : baseUrl + 'assesment/create',
						               type : "POST",
						               data : values,
						               success: function(resp){
						               		document.location.href= baseUrl + 'assesment/';
								       }
						            });
							} else {
								 $("#msg1").html(json.msg);
							}

							});
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'assesment/';
				                    dialogRef.close();
				                }
			        }]
        });
	}

	function form_update2(id)
	{
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>';
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
        var sdata = {'id':id, 'name':csrf, 'value':token};
		$.post(baseUrl+'assesment/form_update', sdata, function(data, textStatus, xhr) {
			$('#debug').html(data);
		});
	}

	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>';
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>';

			BootstrapDialog.show({
			title: 'Update Assesment',
            message: $('<div></div>').load(baseUrl + "assesment/form_update/" + id),
            buttons: [{
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',
		                action: function(dialogRef){
		                	var description = $("#description").val();
		                	//var id = $("#remark").val();
							//alert(remark);

// 		                	 $.post( baseUrl + 'assesment/is_exist', { csrf: token , description : description })
// 							 .done(function( resp )
							{

// 							var json = $.parseJSON(resp);
// 							if (json.status=='true'){
			                	var values = $("#form").serializeArray();
			                	values.push({ name: csrf,value: token });
								values = jQuery.param(values);
								 $.ajax({
						               url  : baseUrl + 'assesment/update',
						               type : "POST",
						               data : values,
						               success: function(resp){

						            	   //$("#debug").html(resp);
						            	   //dialogRef.close()
						               	   document.location.href= baseUrl + 'assesment/';
								       }
						            });
							}
// 							else {
// 								 $("#msg1").html(json.msg);
// 							}

// 							});
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'assesment/';
				                    dialogRef.close();
				                }
			        }]
        	});
	}


</script>
<script src="<?php echo base_url(); ?>assets/js/sorttable.js"></script>
</head>
<body>
<div id="debug">


<div id="list-data">
	<?php require_once('list.php'); ?>
</div>

</div> <!-- debug -->
