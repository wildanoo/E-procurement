<div id="ref">
<div id="ref_msg" class="text-center text-danger bg-warning"></div>	

<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Customer Name</th>
		<th>Project</th>
		<th>Point</th>
		<th>Date</th>
		<th>Remark</th>
		<th>Approval</th>
		<th>Status</th>
		<th>#</th>
	</tr>
	</thead>
	<tbody>

 <?php  //$browse = array();
 if($ref){ $i=1;
 	foreach($ref as $row){ $stat = ($row->status == '0') ? 'request':'approved'; ?>
 		<tr>
 			<td><?= $i; ?></td>
			<td><?= $row->cust_name; ?></td>	
			<td><?= $row->project; ?></td>
			<td><?= $row->point; ?></td>
			<td><?= $row->date; ?></td>
			<td><?= $row->remark; ?></td>
			<td><?= $row->approval; ?></td>
			<td><span class="label label-warning"><?= $stat; ?></span></td>
			<td style="text-align: center;">
			<?php 
					echo '<button onclick="approve_ref([\''.$row->id.'\',\''.$row->approval.'\',\''.$row->id_tbl.'\'])" type="button" class="btn btn-default"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>';
					echo '<button onclick="reject_temp_table('.$row->id.',\'temp_references\')" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>';
				?>
			</td>	
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="9">-</td></tr>
<?php } ?>
     </tbody>
</table>
</div>
