<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>"; 
   
   $('#btn_recmd15').hide(); 
   
   $('#type').attr('disabled', true);	
   
   $('#btn_change15').click(function(){ 
   		$('#btn_recmd15').show(); 
   		$('#btn_change15').hide();
   		
   		$("#note").css("background","#ffffff");
	    $("#note").css("color","#827e85");             
	    $("#note").prop('readonly',false); 
	    
	    $('#type').attr('disabled', false);
   
   });
        
   $('#btn_cancel15').click(function(){ 
   		 $("#container15").load(baseUrl+'vendor/load_due_dilligence');
     });     
	   
   });

</script>
<h2>Vendor Recommendation</h2>
<div id="debug15"></div>
<?php echo form_open('vendor/add_recommend',array('id'=>'form_recmd')); $current_date = date('Y-m-d'); ?>
<table width="50%">
<tr>
	<td><label>Recommendation</label></td><td>:</td>
	<td><?php 
		$options = array('0'=>'-- Select --','avl'=>'AVL','shortlist'=>'Shortlist','reject'=>'Reject');
		$prop = 'class="form-control" id="type" style="width:220px"';
		echo form_dropdown('type',$options,$recmd->type,$prop); ?></td>
	<td id="msg1">&nbsp;</td>	
</tr>
<tr>
	<td style="vertical-align: top;"><label>Note</label></td><td style="vertical-align: top;">:</td>
	<td><textarea name="note" id="note" cols="20" rows="5" placeholder="Note"  class="form-control" required readonly><?php echo $recmd->note; ?></textarea></td>
</tr>
<tr>
	<td colspan="3">
		
		<input type="button" id="btn_cancel15" value="Cancel" class="btn btn-default btn-sm"/>
		<input type="button" id="btn_change15" value="Change" class="btn btn-primary btn-sm"/>
		<input type="submit" id="btn_recmd15" value="Submit" class="btn btn-primary btn-sm"/>		
	</td>	
</tr>
</table><br/><br/>
<?php echo  form_close(); ?>


