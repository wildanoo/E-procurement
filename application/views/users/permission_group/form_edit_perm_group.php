<div class="page-header">
  <h3>Update Document Evaluation</h3>
</div>
<?php echo form_open('user/edit_perm_group',array('id'=>'form')); ?>
<?php echo form_hidden("id",$def->id); ?>
<table cellpadding="2" cellspacing="2">
<tr>
	<td><label>Permission Group</label></td><td>:</td>
	<td><input type="text" name="function" id="function" value="<?php echo $def->function; ?>" class="form-control" required/>		
	</td>	
</tr>
<tr>	
	<td colspan="3" style="text-align: right;">
                <div class="box-footer" style="padding-left: 30px;">
                    <?php  echo form_submit('submit', 'Update','class="btn btn-sm btn-primary"'); ?>
                    <?php  $prop = array('class'=>'btn btn-sm btn-default','title'=>'Cancel'); ?>					 
					<?php  echo anchor('user/permission_group/','Cancel',$prop); ?>
                </div>
	</td>
</tr>
</table>
<?php echo form_close(); ?>