<html>
<head>
<title></title>
<meta name="" content="">
</head>
<body>

Dengan hormat,<br/>
<b><?php echo $vendor_name; ?></b><br/><br/>

Dengan ini kami informasikan, dokumen administrasi perusahaan Bapak/Ibu telah berhasil kami terima. 
Selanjutnya berdasarkan ketentuan internal yang berlaku akan ada tahapan evaluasi yang akan dilalui.<br/>

Untuk pengecekan status registasi dapat mengakses :<br/>
(<?php echo anchor($link); ?>)<br/><br/>

Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/>
<b>PT Garuda Indonesia (Persero) Tbk</b>  



</body>
</html>