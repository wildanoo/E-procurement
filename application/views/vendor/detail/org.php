<div id="container7">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	
   
   $('#btn_create7').click(function(){  
   		$("#container7").load(baseUrl+'vendor/form_org');   		
   });             	
   
 }); 
 
 function update_person(id){
 	
 	$("#container7").load(baseUrl+'vendor/form_update_prsn/'+id); 
 }
 
 function delete_person(id){
 	var baseUrl = "<?php echo base_url(); ?>";
 	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
 	
 	if (confirm('Are you sure you want to delete person?')) {	
		$.post( baseUrl + "vendor/delete_person", { csrf: token, id: id } );
		$("#container7").load(baseUrl+'vendor/load_org');		
	}
	
 } 
  
 </script>
<?php if($this->global->active){ ?> 
<div style="text-align: right;">
<button type="button" id="btn_create7" class="btn btn-info btn-primary">Add New</button>
</div>
<?php } ?>
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Name</th>
		<th>Address</th>
		<th>Phone</th>
		<th>Position</th>
		<th>Remark</th>
		<?php if($this->global->active){ ?><th>Action</th><?php } ?>
	</tr>
	</thead>
	<tbody>
 <?php  //$org = array();
 if($org){ $i=1;
 	foreach($org as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->name; ?></td>	
			<td><?php echo $row->address; ?></td>
			<td><?php echo $row->phone; ?></td>
			<td><?php echo $row->position; ?></td>			
			<td><?php echo $row->remark ?></td>
			<?php if($this->global->active){ ?>
			<td style="text-align: center;">
			<?php
					echo '<i class="fa fa-pencil" onclick="update_person('.$row->id.')"></i>'; echo '&nbsp;&nbsp;&nbsp;';					
					echo '<i class="fa fa-trash"  onclick="delete_person('.$row->id.')"></i>';	
			?></td>	
			<?php } ?>										
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>