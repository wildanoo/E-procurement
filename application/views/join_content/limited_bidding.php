<script type="text/javascript"> 
    $(function(){
        var baseUrl   = "<?php echo base_url(); ?>";
        
        $('#id_category').change(function(){  
            var id_cat = $('#id_category').val(); //alert(id_cat);
            var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
                $.ajax({
                   url  : baseUrl + 'register/get_subcategory',
                   type : "POST",                                  
                   data : csrf +'='+ token +'&id_cat='+id_cat,
                   success: function(resp){
                      //$('#debug').html(resp);
                      $('#id_subcat').html(resp);
                   }
                });
        }); 
        
        $('#btn_register').click(function(){
            var values  = $("#form").serializeArray();          
            var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
            values.push({name: csrf,value: token}); values = jQuery.param(values);          
                $.ajax({
                   url  : baseUrl + 'joinbiddingsourcing/validate_join_sourcing',
                   type : "POST",                                  
                   data : values,
                   success: function(resp){ //$('#debug').html(resp);
                        var json = $.parseJSON(resp); 
                         if(json.status=="false"){
                            $('#msg1').html(json.msg1); $('#msg6').html(json.msg6);
                            $('#msg2').html(json.msg2); $('#msg7').html(json.msg7);
                            //$('#msg3').html(json.msg3);
                            $('#msg8').html(json.msg8);
                            $('#msg4').html(json.msg4); $('#msg9').html(json.msg9);
                            $('#msg5').html(json.msg5); $('#msg10').html(json.msg10);
                            $('#msg11').html(json.msg11);                       
                         } else {   $("#form").submit(); }
                   }
                });
        });


        $('#btnsubmit').click(function(event){
            event.preventDefault();
            if(document.getElementById('i_accept').checked && document.getElementById("file_loi").files.length != 0)
            {
                $("#form").submit();
                //alert('OK');
            }else{
                alert('Please check your Terms and Condition and your LOI Document');
                return false;
            }
        });

        $("input[type=file]").change(function(){
            var filename = $(this).val() ;
            filex = filename.split(".");
            tipe = filex[filex.length-1] ;
            if(tipe!='pdf') alert('file must pdf by type');
        });
        
    }); 
</script>

<div class="about-content-wrap pull-left" id="#">
    <div class="head" style="margin-top: 25px;">
        <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;">Registration</h1>
        <a href="#" class="next-section about animate2">
            <i class="fa fa-angle-double-down"></i>
        </a>
    </div>
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 content-tabs-wrap wow fadeInUp animated" data-wow-delay="1.7s" data-wow-offset="200">
            <div class="col-xs-12 col-sm-12 col-md-12 content-tabs no-pad">            
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#" data-toggle="tab"> Invitation </a></li>
                  <li><a>&nbsp;</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-home">
                        <div class = "row">
                            <div class="col-md-12 tab-content">
                                <div class="box2 box-info2 tab-pane fade in active" id="registrasi-step-1">
                                    <div class="box-body">
                                        <div class="about-intro-wrap pull-left">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 faq-tabs-wrap">
                                                    <div class="tabbable tabs-left">
                                                        <div class="tab-content col-md-12 col-sm-12 col-xs-12">
                                                            <div class="tab-pane active" id="a">
                                                                <div class="dept-title-tabs">Announcement Detail</div>
                                                                <div class="">
                                                                    <div style="padding:5px;">

                                                                        <div class="col-sm-12">
                                                                            <h3><?php echo $announce->title_ind; ?></h3>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <span class="control-label" style="color:red"><strong>Bidding Invitation</strong></span>
                                                                        </div>

                                                                        <div class="col-sm-12">
                                                                            <span class="control-label col-sm-7"><?php echo $announce->code; ?></span>
                                                                            <div class="col-sm-5">
                                                                                <span class="control-label col-sm-4">Publish Date</span>
                                                                                <span class="control-label col-sm-8"  style="color:red">: <?php echo date('M d, Y',strtotime($announce->publish_date)); ?> </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-7">
                                                                                <span class="control-label col-sm-4">Category</span>
                                                                                <span class="control-label col-sm-8" style="color:red">: <?php echo $categoryx; ?> </span>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <span class="control-label col-sm-4">Start Date</span>
                                                                                <span class="control-label col-sm-8" style="color:red">: <?php echo date('M d, Y',strtotime($announce->first_valdate)); ?> </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-7">
                                                                                <span class="control-label col-sm-4">Sub Category</span>
                                                                                <span class="control-label col-sm-8" style="color:red">: <?php echo $subcategoryx; ?> </span>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <span class="control-label col-sm-4">End Date</span>
                                                                                <span class="control-label col-sm-8" style="color:red">: <?php echo date('M d, Y',strtotime($announce->end_valdate)); ?> </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-7">
                                                                                <span class="control-label col-sm-4">Status</span>
                                                                                <span class="control-label col-sm-8">: <span class="label label-info"><?php echo array_key_exists($announce->status, $status_ref)?$status_ref[$announce->status]:'-'; ?></span> </span>
                                                                            </div>
                                                                        </div>


                                                                        <p>&nbsp;</p>

                                                                        <div class="col-sm-12" align="justify">
                                                                            <span>
                                                                                <?php echo $announce->detail_ind; ?>
                                                                            </span>
                                                                        </div>

                                                                        <p>&nbsp;</p>

                                                                        <div class="col-sm-12">
                                                                            <?php 
                                                                                if(($announce->file)!=null) { $filename = '<a href="#"><i class="fa fa-download"> '.$announce->file.'</i></a>';} else { $filename = ''; }
                                                                            ?>
                                                                            <span >
                                                                                Attachment : <?php echo $filename; ?>
                                                                            </span>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-left" style="margin-right:30px;">
                                            <button type="button" class="btn button-custom" data-toggle="tab" href="#registrasi-step-2">Next</button>
                                        </div> 
                                    </div>
                                </div>
                                <div class="box2 box-info2 tab-pane fade" id="registrasi-step-2">
                                    <div class="box-body">
                                        <div class="about-intro-wrap pull-left">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 faq-tabs-wrap">
                                                    <div class="tabbable tabs-left">
                                                        <div class="tab-content col-md-12 col-sm-12 col-xs-12">
                                                            <div class="tab-pane active" id="a">
                                                                <div class="dept-title-tabs">Vendor Policy</div>
                                                                <div class="background-vendor">
                                                                    <div style="padding:20px;">
                                                                        <?php foreach ($policy_agreements as $policy_agreement) {
                                                                            if ($policy_agreement->type != 0) {
                                                                                echo "<h2><strong>".$policy_agreement->subtype_name."</strong></h2>";
                                                                            }else{
                                                                                echo "<h2><strong>General</strong></h2>";
                                                                            }
                                                                              /*$root = $_SERVER['DOCUMENT_ROOT'];   $path = $root."/uploads/policy_agreement/open_bidding_".$this->session->userdata('lang').".txt";
                                                                            if( file_exists($path)) {   echo file_get_contents($path);          
                                                                            } else { echo ""; } ----> ini yg bener */
                                                                            $root = base_url();   $path = $root."uploads/policy_agreement/".$policy_agreement->file."_".$this->session->userdata('lang').".txt";
                                                                            if( file_exists($path)) {   echo file_get_contents($path);
                                                                            } else { echo file_get_contents($path); }
                                                                            echo "<hr/>";
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-left" style="margin-right:30px;">
                                            <button type="button" class="btn button-custom-back" data-toggle="tab" href="#registrasi-step-1">Back</button>
                                            <button type="button" class="btn button-custom" data-toggle="tab" href="#registrasi-step-3">Next</button>
                                        </div> 
                                    </div>
                                </div>
                                <div class="box2 box-info2 tab-pane fade" id="registrasi-step-3">
                                    <div class="box-body">
                                        <div class="dept-title-tabs">Registration Form</div>
                                        <div class = "container2">
                                            <div class = "row2" style="margin-top:20px;">
                                                <div class="appointment-form col-xs-12 col-md-12 no-pad wow fadeInRight animated" data-wow-delay="1s" data-wow-offset="200">
                                                    <div class="row background-form">
                                                        <div class="appointment-form-title"><i class="fa fa-building fa-1x" style="color: #fff;margin: 0 20px;"></i>Company</div>
                                                        <?php echo form_open('joinbiddingsourcing/update_unregistered_vendor',array('id'=>'form')); ?>

                                                        <input type="hidden" name="id_vendor"    value="<?php echo $id_vendor ;?>" />
                                                        <input type="hidden" name="fixed_subcat" value="<?php echo $id_subcat ;?>" />
                                                        <input type="hidden" name="token"        value="<?php echo $token ;?>" />

                                                        <div>
                                                            <div class="appt-form">
                                                                <input type="text" class="appt-form-txt" name="vendor_name" placeholder="Vendor Name" />
                                                                <div id="msg1" style="color: red; font-style: italic; "></div>
                                                                
                                                                <?php foreach($category as $group=>$row) { foreach ($row as $val) { if($id_subcat==($val->id)) {$category=$val->subcategory;} } } ?>                                

                                                                <input type="text" class="appt-form-txt" name="subcat" value="Category -> <?php echo $category; ?>" style="color:#FFF;background-color:#107FC9;" readonly/>

                                                                <!--
                                                                <select class="appt-form-select" id="public-vendor-select" name="id_subcat[]" multiple="multiple">
                                                                <?php //foreach($category as $group=>$row){ ?>
                                                                    <optgroup label="<?php //echo $group; ?>">
                                                                    <?php //foreach ($row as $val){ if($id_subcat==($val->id)) {$selected='selected="selected" disabled';} else {$selected='';} ?>                                
                                                                        <option value="<?php //echo $val->id; ?>" <?php //echo $selected; ?> ><?php //echo $val->subcategory; ?></option>                              
                                                                    <?php //} ?>                                
                                                                    </optgroup>
                                                                <?php //} ?>
                                                                </select>
                                                                -->

                                                                <div id="msg4" style="color: red; font-style: italic; "></div><br/>
                                                                <input type="text" class="appt-form-txt" placeholder="Supplier Address" name="vendor_address" />
                                                                <div id="msg2" style="color: red; font-style: italic; "></div>
                                                                <input type="text" class="appt-form-txt" placeholder="Phone Number" name="phone" />
                                                                <div id="msg6" style="color: red; font-style: italic; "></div>
                                                                <input type="text" class="appt-form-txt" placeholder="Fax Number" name="fax" />
                                                                <div id="msg7" style="color: red; font-style: italic; "></div>
                                                                
                                                                <?php 
                                                                if($email!=null) 
                                                                {
                                                                    echo '<input type="email" class="appt-form-txt" placeholder="Email Address" name="email" value="'.$email.'" style="color:#FFF;background-color:#107FC9;" disabled />';
                                                                }
                                                                else
                                                                {
                                                                    echo '<input type="email" class="appt-form-txt" placeholder="Email Address" name="email" />';
                                                                }
                                                                ?>

                                                                <div id="msg8" style="color: red; font-style: italic; "></div>
                                                                <input type="text" class="appt-form-txt"placeholder="NPWP" name="npwp" />
                                                                <div id="msg9" style="color: red; font-style: italic; "></div>
                                                            </div>
                                                        </div>
                                                        <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>Contact Person</div>
                                                        <div class="appt-form">
                                                            <input type="text" class="appt-form-txt" placeholder="Contact Person" name="cp_name" />
                                                            <div id="msg5" style="color: red; font-style: italic; "></div>
                                                            <input type="text" class="appt-form-txt" placeholder="Mobile Phone" name="cp_mobile" />
                                                            <div id="msg10" style="color: red; font-style: italic; "></div>
                                                            <input type="email" class="appt-form-txt" placeholder="Email" name="cp_email" />
                                                            <div id="msg11" style="color: red; font-style: italic; "></div>
                                                        </div>
                                                        <?php 
                                                            $blank_post = array('id_lang','id_country','id_region','district','postcode','city',
                                                                                'street','house_num','sprefix','ssuffix','building','floor',
                                                                                'room','bank_name','branch_detail','bckey','acc_holder','acc_num','bank_region');                       
                                                            foreach($blank_post as $field){ echo form_hidden($field,""); }
                                                        ?>
                                                        
                                                    <?php echo form_close(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-left" style="margin-top:30px;">
                                            <button type="button" class="btn button-custom-back" data-toggle="tab" href="#registrasi-step-2">Back</button>
                                            <button type="button" class="btn button-custom" data-toggle="tab" href="#registrasi-step-4">Next</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="box2 box-info2 tab-pane fade" id="registrasi-step-4">
                                    <div class="row background-terms">
                                        <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>Terms and Condition</div>
                                        <div class="terms-custom">
                                            <div class="form-group">
                                                
                                                This Privacy Policy governs the manner in which Garuda Indonesia collects, uses, maintains and discloses information collected from users (each, a "User") of the https://www.eproc.garuda-indonesia.com/ website ("Site"). This privacy policy applies to the Site and all products and services offered by Garuda Indonesia
                                                <br>
                                                <br>
                                                <label>
                                                <input type="checkbox" name="i_accept" id="i_accept" style="margin-right:10px;">I accept the Terms and Condition
                                                </label>
                                                <br/>
                                                <br>
                                                <ul class="list-inline">
                                                    <li style="width:155px">LOI Attachment <i>(*.pdf)</i></li>
                                                    <li><input type="file" name="file_loi" id="file_loi" style="margin-right:10px;"></li>
                                                </ul>
                                                <br>
                                                <br>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-left" style="margin-top:10px;">
                                        <button type="button" class="btn button-custom-back" data-toggle="tab" href="#registrasi-step-3">Back</button>
                                        <button type="button" class="btn button-custom" id="btnsubmit" href="#">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade no-pad" id="tab-profile">
                        <div class="row background-terms">
                            <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>Tracking Registration Status</div>
                            <div class="col-md-12 col-sm-12 terms-custom">
                                <div class="form-group terms-text">
                                    Please Enter Registration Number Then Click "Search"
                                    <br>
                                    <br>
                                    <input type="text" class="appt-form-txt" placeholder="Registration Number" style="width:40%;" /><button type="button" class="btn button-search" data-toggle="tab" href="#registrasi-step-6">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </div>
</div>