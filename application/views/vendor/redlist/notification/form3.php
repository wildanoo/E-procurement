<html>
<head>
<title></title>
<meta name="" content="">
</head>
<body>

Dengan hormat,<br/>
<b>Bapak/Ibu <?php echo $to; ?></b><br/>

Bersama ini kami sampaikan terkait pembatalan proses redlist vendor terhadap <?=$vendor_name?> .

<br/><br/>
Maka menindaklanjuti hal tersebut, <?=$vendor_name?> telah diperbolehkan mengikuti
proses pengadaaan pada kategori dimana rekanan terdaftar.
<br/>
<br/>

Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/><br/>

(&nbsp;<b><?php echo ucfirst( $this->tank_auth->get_username()); ?></b>&nbsp;)


<br/><br/>
PT.Garuda Indonesia Persero Tbk. <br/>
Business support/JKTIBGA <br/>
Gedung Management Garuda, Lantai Dasar <br/>
Garuda City, Bandara Soekarno Hatta <br/>
Cengkareng 19120, PO BOX 1004 TNG BUSH
</body>
</html>