++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ REDLIST ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Halaman User Adminsourcing
	> halaman add to red list
		- F_ tidak ada "counter"
		- F_ "document", harusnya "attachment"
		- multiple attachment
	> halaman Vendor Red List
		- F_ berikan notifikasi untuk harus men-cheklist dulu bila ingin men-generate to excel. (contoh dari vendor registration)
		- F_ button "create" seharusnya "Add Vendor" dan sejajar dengan dropdown "All Categories" dan button Export to Excel.
		- F_ Pada Search Section, ketika tanggal hanya terisi salah satu maka keluarkan alert untuk mengisi yang kosong.

Halaman pada user SMIBK
	> F_ tabel pada vendor list-red list
		- kurang kolom attachment
	> F_ # harusnya "Action"
			
D_ E-mail tak sampai setelah proses Submit, Accept, Reject.

F_ Konfirmasi isi kolom Action.

Kenapa ada banyak PT.Vendor di redlist yg wait?

F_ Kalo di-cancel, apa lagi prosesnya?
	-> data di redlist tetap ada, tp statusnya 'Cancel'
	
**NOTE
	> E-mail notifikasi untuk vendor diberikan kepada dua e-mail yang terdapat pada data vendor (cp dan e-mail vendor)
	> Berikan record pengiriman e-mail.
	
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ BLACKLIST ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Halaman User Adminsourcing
	> halaman add to red list
		- field tidak dapat di input
		- tidak ada "Counter"
		- multiple attachment
		- tombol "Submit"
		(samakan seperti yang di redlist)
	> Detail Vendor Blacklist msh bisa diedit, saat "wait" dan "approved"

Halaman pada user SMIBK
	> tabel pada vendor list-black list
		- kurang kolom attachment
		- kurang kolom For Category
	> F_ # harusnya "Action"
	
Attachment ga bisa dibuka "404 Page Not Found"

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++REGISTRATION+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++