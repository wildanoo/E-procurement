<script>
	
	var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	var token     = '<?php echo $this->security->get_csrf_hash(); ?>';

	$(function() {
		$('#start_date, #end_date').datepicker({  
	          changeMonth: true, changeYear: true, 
	   		  dateFormat: "yy-mm-dd"
	    });

	    $('#ck_all').on('click', function(){
			$(this).closest('table').find(':checkbox').prop('checked', this.checked);
		});

		$('#id_cat').change(function(){
			$('#form-select-category').submit(); 
		});

		$('#btnexport').on('click', function(){
			$('#myform').submit(); 
		});
			
	});


	function load_data()
	{
		$('#main_container').load(baseUrl+'blacklist/browse/<?php echo $page; ?>');
	}

	function form_add()
	{
		$("#main_container").load(baseUrl+'blacklist/form_add');
	}

	function edit_blacklist(id)
	{
		$("#main_container").load(baseUrl+'blacklist/form_add/'+id);
	}

	function view_detail_blacklist(id)
	{
		$("#main_container").load(baseUrl+'blacklist/view_detail_blacklist/'+id);
	}

	function view_log(idx,idv)
	{
		var p_url = baseUrl+'blacklist/browse_logs/';
		$.post(p_url, { csrf: token , idx: idx , idv: idv }, function(data, textStatus, xhr) {
				$("#main_container").html(data);
		});
	}


	function reject_blacklist(id)
	{
		var cek = confirm('Sure ?');
		if(cek)
		{
			var p_url = baseUrl+'blacklist/reject/'+id;
			$.post(p_url, { csrf: token , id: id }, function(data, textStatus, xhr) {
				load_data();
			});
		}
	}

	function approve_blacklist(id)
	{ 
		var page_url = baseUrl+'blacklist/approv/'+id;
		$.post(page_url, { csrf: token , id: id }, function() {
			load_data();
		});
		
	}

	function cancel_blacklist(id)
	{
		var cek2 = confirm('Sure ?');
		if(cek2)
		{
			var pa_url = baseUrl+'blacklist/remove/'+id;
			$.post(pa_url, { csrf: token , id: id }, function(data, textStatus, xhr) {
				load_data();
			});
		}
	}

	

	
	
</script>

<div id="main_container">

<div id="debug"></div>
<div class="page-header"><h3>Vendor <?php echo $title; ?></h3></div>


<!-- Search Term -->
<div class="table">
	<div class="panel panel-default">
	    <div class="panel-heading">
	        <i class="fa fa-search"></i> <strong>Search Section</strong>
	    </div>
	    <style type="text/css">
	    .list-inline li{
	    	vertical-align: middle !important;
	    }
	    .btn-srch{
	    	margin-bottom: 0;
	    }
	    </style>
	    <?php echo form_open('blacklist/search',array('id'=>'form-search','novalidate' => 'true'));	//$current_date = date('Y/m/d')?>
	    <div class="panel-body">
	    	<ul class="list-inline">
	    		<li style="width:24%">
	    			<div class="input-group">
	    			<input type="text" value="" placeholder="date start" class="form-control date_picker" id="start_date" name="start_date" autocomplete="off" readonly>
	    			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	    			</div>
	    		</li>
	    		<li style="width:2%">to</li>
	    		<li style="width:24%">
	    			<div class="input-group">
	    			<input type="text" value="" placeholder="date end" class="form-control date_picker" id="end_date" name="end_date" autocomplete="off" readonly>
	    			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	    			</div>
	    		</li>
	    		<li style="width:34%"><div class="input-group" style="width:100%"><input placeholder="search by code or title..." type="text" value="" class="form-control" id="search_term" name="search_term" autocomplete="off"></div></li>
	    		<li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search">Search</button></div></li>
	    		<li style="width:7%"><div class="input-group"><a href="<?php echo base_url()?>blacklist" class="btn btn-srch btn-warning btn-sm">Show All</a></div></li>
	    	</ul>
	    </div>
	    <?php echo form_close()?>
	</div>
</div>
<hr/>
<!-- Search Term -->





<ul class="list-inline">
	<?php if($this->permit->add) { ?>
	<li style="width:110px">	
		<a class="btn btn-primary pull-left" href="javascript:void(0);" onclick="form_add()">Add Vendor</a>
	</li>
	<?php } ?>
	<li style="width:24%">
		<?php echo form_open('blacklist/subcategory',array('id'=>'form-select-category','novalidate' => 'true'));	//$current_date = date('Y/m/d')?>
		<select name="id_cat" id="id_cat" class="form-control" style="font-size:12px; width: 200px;">	
		    <option value="all">-- All Category --</option>
		    <?php
		    $idcategory = '';
		    foreach ($category as $cate) 
		    {
		    	if($idcategory != $cate->idcat)
		    		echo '<optgroup label="'.$cate->category.'">';
		    	if($id_subcat==($cate->idsub)) {$selected='selected';} else {$selected='';}
		    		echo '<option value="'.$cate->idsub.'" '.$selected.'>'.$cate->subcategory.'</option>';
		    		$idcategory = $cate->idcat;
		    }
		    ?>
		</select>
		<?php echo form_close()?>
	</li>
	<li style="float:right">
		<button class="btn btn-success" type="button" id="btnexport"><i class="fa fa-file-excel-o" style="color:#FFFFFF"></i> Export To Excel</button>
	</li>
</ul>

<?php echo form_open('blacklist/export_excel',array('id'=>'myform')); ?>

<?php echo $this->session->flashdata("message"); ?>
<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
        <tr>
            <th>No</th>
            <th style="text-align: center;"><input type="checkbox" id="ck_all" value="1"></th>
            <th>Registration Num.</th> 
            <th>Vendor Num.</th>            
            <th>Vendor Name</th> 
            <th>Start Date</th> 
            <th>Reason</th>
            <th>Status</th>
            <th><i class="glyphicon glyphicon-paperclip"></i></th>
            <th>action</th> 
        </tr>       
	</thead>
    <tbody>
<?php	
//echo form_open('#',array('id'=>'form'));
if($browse){ $i=1;
	foreach($browse as $row){ 
		$ap = $row->approv ;
		$sttcolor = 'warning' ;
		if(($row->stt)=='Rejected') $sttcolor = 'danger' ;
		if(($row->stt)=='Cancel') $sttcolor = 'info' ;

		if(($row->stt)=='Approved')
		{
			$approv = '<span class="label label-success" >Approved</span>';
		}
		elseif(($row->stt)=='Rejected')
		{
			$approv = '<span class="label label-danger">'.$row->stt.'</span>';
		}
		else 
		{	$wait = $row->stt.'ing';
			if( $ap <=2 ) $approv = '<i class="fa fa-check-circle" style="color:#00a65a"></i><i class="fa fa-check-circle" style="color:#00a65a"></i>';
			if( $ap <=2 && $this->permit->vpib) $approv = '<i class="fa fa-check-circle" style="color:#00a65a"></i> <i class="fa fa-check-circle" style="color:#00a65a"></i> <span class="label label-'.$sttcolor.'">'.$wait.'</span>';
			
			if( $ap <=1 ) $approv = '<i class="fa fa-check-circle" style="color:#00a65a"></i>';
			if( $ap <=1 && $this->permit->smibk) $approv = '<i class="fa fa-check-circle" style="color:#00a65a"></i> <span class="label label-'.$sttcolor.'">'.$wait.'</span>';

			if( $ap ==0 ) $approv = '<span class="label label-'.$sttcolor.'">'.$wait.'</span>';
		}

		$action ='<div class="btn-group">
		        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
		          Action <span class="caret"></span>
		        </button>
		        <ul class="dropdown-menu" role="menu" style="background:#DDDDDD;">
		          	<li><a  href="javascript:void(0)" onclick="view_detail_blacklist('.$row->id_blacklist.')">View Detail Blacklist</a></li>';
		if($this->permit->viewv)
		$action .='<li><a  href="'.base_url('blacklist/view_detail_vendor/'.$row->id_vendor).'">View Detail Vendor</a></li>';

		$action .='<li><a  href="'.base_url('vendorlog/search/'.$row->id.'/blacklist').'">View Log</a></li>';

		if($this->permit->editb )
		$action .='<li><a  href="javascript:void(0)" onclick="edit_blacklist('.$row->id_blacklist.')">Edit Blacklist</a></li>';

		if($this->permit->cancelb && $row->approv==0)
		$action .='<li><a  href="javascript:void(0)" onclick="cancel_blacklist('.$row->id_blacklist.')">Cancel Blacklist</a></li>';

		/// Approve ///
		if($this->permit->vpib && $ap <=2)
		{
			if($this->permit->approv)
				$action .='<li><a  href="javascript:void(0)" onclick="approve_blacklist('.$row->id_blacklist.')">Approve</a></li>';
		}
		elseif($this->permit->smibk && $ap <=1)
		{
			if($this->permit->approv)
				$action .='<li><a  href="javascript:void(0)" onclick="approve_blacklist('.$row->id_blacklist.')">Approve</a></li>';
		}
		elseif($ap <=0)
		{	if($this->permit->approv)
			$action .='<li><a  href="javascript:void(0)" onclick="approve_blacklist('.$row->id_blacklist.')">Approve</a></li>';
		}

		/// Reject ///
		if($this->permit->vpib && $ap <=3 && ($row->stt)!='Rejected')
		{
			if($this->permit->reject)
				$action .='<li><a  href="javascript:void(0)" onclick="reject_blacklist('.$row->id_blacklist.')">Reject</a></li>';
		}
		elseif($this->permit->smibk && $ap <=2 && ($row->stt)!='Rejected')
		{
			if($this->permit->reject)
				$action .='<li><a  href="javascript:void(0)" onclick="reject_blacklist('.$row->id_blacklist.')">Reject</a></li>';
		}
		elseif($ap <=1 && ($row->stt)!='Rejected')
		{	if($this->permit->reject)
			$action .='<li><a  href="javascript:void(0)" onclick="reject_blacklist('.$row->id_blacklist.')">Reject</a></li>';
		}

		$attac = $row->doc_name; $attactment = '';
		if($attac!='0' && !empty($attac))
		{
			$attac 		= explode(',',$attac);
			$attactment = '<a title="download attachment" href="'.base_url('blacklist/download/'.date(s).$row->id_blacklist).'/'.md5(date(s).$row->id_blacklist).'"><span class="label label-primary">'.count($attac).' <i class="glyphicon glyphicon-paperclip"></i></span></a>';
		}
		?>	
			<tr>
				<td style="text-align: center;"><?php  echo $num = $num+1; ?></td>
				<td style="text-align: center;"><input type="checkbox" class="ck_ex" name="mycheckbox[]" id="ck<?=$i?>" value="<?php echo $row->id_blacklist; ?>"></td>
				<td><?php echo $row->register_num ?></td>
				<td><?php if($row->vendor_num) echo $row->vendor_num; else echo "-"; ?></td>		
				<td><?php echo $row->vendor_name ?></td>
				<td><?php echo date('M d, Y',strtotime($row->start_date)); ?></td>
				<td><?php if(strlen($row->remark)>=15) {echo substr($row->remark,0,14).'...';}else{echo $row->remark;} ?></td>
				<td><?php echo $approv ; ?></td>
				<td><?= $attactment ?></td>
				<td style="text-align: center;">
					<?php echo $action ; ?>
      			</td>
			</tr>
<?php $i++;  } ?>
<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php  } else { ?>
	<tr><td colspan="9" align="center">-</td></tr>
<?php } ?>
    </tbody>
</table>

<?php echo form_close();?>

</div>

