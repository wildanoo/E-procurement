<script type="text/javascript">
  $(function () { 
   var baseUrl = "<?php echo base_url(); ?>";
   var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
   var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
   
	   $('#btn_change').click(function(){ 	   		
			var opt     = $("input:radio[name=status]:checked").val();						
			$.ajax({
					url  : baseUrl + 'vendor/change_register_status',
					type : "POST",              		               
					data : csrf + '=' + token +'&opt=' + opt,
					success: function(resp){
						$("#info_status").load(baseUrl+'vendor/load_status_info');
					}
			});		
	   		
	   });

	   $('#btn_reject').click(function(){ 	   		
								
			$.ajax({
					url  : baseUrl + 'vendor/rejected_vendor',
					type : "POST",              		               
					data : csrf + '=' + token,
					success: function(resp){
						$("#info_status").load(baseUrl+'vendor/load_status_info');
					}
			});		
	   		
	   });
	   
	   $('#btn_approve').click(function(){ 
	   		$("#tabs_container").load(baseUrl+'vendor/form_approved');
	   });
	
   });
    
</script>

<div id="debug_temp"></div>
<table width="100%">
	<tr><td>Supplier Name</td><td>:</td><td><h1><?php  echo $vendor->name;  ?></h1></td></tr>
	<tr><td>Supplier Type</td><td>:</td><td><h1><?php  echo strtoupper($vendor->type);  ?></h1></td></tr>
	<tr><td>Registration Code</td><td>:</td><td><h1><?php  echo $vendor->reg_num;  ?></h1></td></tr>
	<tr><td>Registration Status</td><td>:</td><td>
		<?php     
      	foreach($status as $val){
      			 $next    = $vendor->id_stsreg + 1;
	      		 $checked = ($val->id == $vendor->reg_sts) ? true : false;
	      		 if($val->id == $next)  {
	      		 	$prop = array(  'name'        => 'status',
								    'id'          => 'status',
								    'value'       => $val->id,
								    'checked'     => TRUE,
								    'style'       => 'margin:10px;display:none;' );
	      		 	echo form_radio($prop); 
	      		 }				 
				 echo "&nbsp;";
				 if($val->id <= $next){ 
					 if($val->id < $next) echo "<b>".ucwords($val->status)."</b>"; 
					 if($val->id == $next) $lbl = ucwords($val->status);
					 if($val->id != $next) echo "&nbsp;&nbsp;&nbsp;<i class='glyphicon glyphicon-forward'></i>&nbsp;&nbsp;&nbsp;";
				 }	

		} 		
      ?>      
      <?php if($vendor->id_stsreg <= "3" && $this->permit->avdc && $vendor->type!="fail") { ?> 
	  	<input type="button" id="btn_change" value="<?php echo $lbl; ?>" class="btn btn-warning btn-sm"/>
	  	<input type="button" id="btn_reject" value="Rejected" class="btn btn-warning btn-sm"/>
	  	
	  <?php } else if($this->permit->avr && $vendor->id_stsreg <= "4" && $vendor->type!="fail") {  ?>
	  	 <input type="button" id="btn_approve" value="Confirm" class="btn btn-warning btn-sm"/><?php } ?>
		
	</td></tr>
	<tr><td>Supplier Number</td><td>:</td><td><h1><?php  if($vendor->num) echo $vendor->num; else echo "-";  ?></h1></td></tr>
</table>
<br/>



