<script type="text/javascript"> 
	var baseUrl   = "<?php echo base_url(); ?>";	
	$(function(){
	
		var baseUrl   = "<?php echo base_url(); ?>";
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	    var token     = '<?php echo $this->security->get_csrf_hash(); ?>';

		$('.checkall').on('click', function(){
			$(this).closest('table').find(':checkbox').prop('checked', this.checked);
		});						 
	
		$("#srch-term").autocomplete({
			source: baseUrl + 'register/get_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'register/set_browse_session',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){                		            
				    document.location.href= baseUrl + 'register/';
               }
            });
        });	  

		$("#btn_change_status").click(function(){
	            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
	            var values = $("#form").serializeArray();

				if (values.length > 1) {
					values.push({name: csrf,value: token});
					values = jQuery.param(values);

				BootstrapDialog.show({
					title: 'Update Status',
		            message: $('<div></div>').load(baseUrl + "register/form_update/" ),  
		            buttons: [{			            	
				                label: 'Confirm',
				                cssClass: 'btn btn-primary btn-sm',		               
				                action: function(dialogRef){
				                	var result = $("input:radio[name=status]:checked").val(); 
				                	var values = $("#form").serializeArray();
				                	values.push({ name: "result", value: result });
				                	values.push({ name: csrf,value: token });
				                	values = jQuery.param(values);
				                	$.ajax({
							               url  : baseUrl + 'register/update',
							               type : "POST",              		               
							               data : values,
							               success: function(resp){
							               		document.location.href= baseUrl + 'register/';     
									       }
							            });
 				                }
				            }, {
				            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
						                action: function(dialogRef) {
						                    dialogRef.close(); 
						                }
					        }]
		        	}); } else alert('Please check at least one of the options.');
		  });  
	   
	});
	
	function form_update(id){
				var baseUrl = "<?php echo base_url(); ?>";
				var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 

				BootstrapDialog.show({
				title: 'Change Status',
	            message: $('<div></div>').load(baseUrl + "register/form_update/" + id),  
	            buttons: [{			            	
			                label: 'Confirm',
			                cssClass: 'btn btn-primary btn-sm',		               
			                action: function(dialogRef){
			                	var status = $("input:radio[name=status]:checked").val();
			                	 $.post( baseUrl + 'register/update', { csrf: token , status : status , id : id })
								 .done(function( resp ){  
									 document.location.href= baseUrl + 'register/'; 
			               		 });  
			                }
			            }, {
			            	label: 'Cancel',cssClass: 'btn btn-primary btn-sm',
					                action: function(dialogRef) {
					                    dialogRef.close(); 
					                }
				        }]
	        	});
		}

	function review(id){
		var baseUrl = "<?php echo base_url(); ?>";
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 

		BootstrapDialog.show({
		title: 'Review',
        message: $('<div></div>').load(baseUrl + "register/review/" + id),  
        buttons: [{		
	            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
			                action: function(dialogRef) {
			                    dialogRef.close(); 
			                }
		        }]
    	});
}

</script>

<div id="debug"></div>
<div class="page-header"><h3>Registration</h3></div>
<input type="button" id="btn_change_status" class="btn btn-primary btn-sm" value="Change Status" style="width:130px;"/>
  
		<div class="col-sm-3 col-md-3 pull-right">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" id="btn_search" ><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        </div>

<?php echo $this->session->flashdata("message"); ?>
<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
        <tr>
            <th style="text-align: center;" >No</th>
            <th style="text-align: center;"><input type="checkbox" class="checkall">All</th>
            <th>Reg. Num</th>            
            <th>Vendor Name</th>  
            <th>Category</th>  
            <th>Sub Category</th>        
            <th>Status</th>
            <th>Created Date</th>     
            <th>Action</th>  
        </tr>       
	</thead>
    <tbody>
<?php	
echo form_open('#',array('id'=>'form'));
echo form_hidden('total',count($browse));
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td style="text-align: center;"><?php  echo $num = $num+1;; ?></td>
				<td style="text-align: center; width:10%;">
				    <?php $checked = /*$row->status=="1" ? "checked" :*/ "";  ?>
					<input type="checkbox" <?php echo $checked; ?> name="<?php echo "select".$i;?>" id="<?php echo "select".$i;?>" value="<?php echo $row->id;?> ">
					<!-- input type="hidden" name="<?php //echo "matID".$i;?>" id="<?php //echo "matID".$i;?>" value="<?php // echo $row->id; ?>"/> -->	
				</td>
				<td><?php echo $row->reg_num ?></td>		
				<td><?php echo $row->vendor_name ?></td>
				<td><?php echo $row->category; ?></td>	
				<td><?php echo $row->subcat; ?></td>			
				<td align="center"><?php echo $status[$row->reg_sts]; ?></td>						
				<td style="text-align: center;"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>		
				<td style="text-align: center;">
				<?php echo  '<i class="glyphicon glyphicon-eye-open" onclick="review('.$row->id.')"></i>'; ?> &nbsp;&nbsp;
				<?php echo  '<i class="glyphicon glyphicon-refresh" onclick="form_update('.$row->id.')"></i>';?>				
				</td>
			</tr>
<?php	$i++;  } ?>	
<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?>
	<tr><td colspan="9" align="center">-</td></tr>
<?php } ?>
    </tbody>
    <?php echo form_close();?>
</table>

