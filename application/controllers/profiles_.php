<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Profiles extends Controller {

	function __construct()
	{
		parent::__construct();			
	}	
		
	function index(){
		if ($this->tank_auth->is_logged_in()) {

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			$id = $this->tank_auth->get_user_id();
			//$profID   = $this->session->userdata("profID");
			//$id = $this->uri->segment(3);
			$select = "id,userID,name,nopeg,id_office,id_dept,
				  phone,mobile,status,created_id,created_date,last_updated";
			$data['def'] =$this->crud->browse("","profiles","id",$id,"false",$select);
			
			$office = $this->crud->browse("","offices","","","true","id,office_name");
			if(!$office) $office = array();
			foreach($office as $val){ $option_office[$val->id] = $val->office_name; }
			$data['offices'] = $option_office;
			
			$department = $this->crud->browse("","departemen","","","true","id,dept_name");
			if(!$department) $department = array();
			foreach($department as $val){ $option_dept[$val->id] = $val->dept_name; }
			$data['department'] = $option_dept;
			$data['profiles'] = $this->crud->browse("","profiles","id",$id,"false",$select);
			//print_r($data);
			$none_profile = array('id'			=> "none",
					'name'		=> "none",
					'nopeg'		=> "none",
					'id_office'	=> "none",
					'id_dept'	=> "none",
					'phone'		=> "none",
					'mobile'  	=> "none",
			);
			$profile           = $this->crud->browse("","profiles","id",$id,"false");
			$data['profiles']   = !$profile ? (object) $none_profile : $profile;
			
			$data['view']	= "profiles/form_update";
			$this->load->view('layout/template',$data);
 		
	 		
		} else {  
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/'); 	 }
	}
	
	function load_profiles(){
		$id = $this->tank_auth->get_user_id();
		//$id = $this->uri->segment(3);
		//$profID   = $this->session->userdata("profID");
		$select	 = "id,name,nopeg,phone,mobile,
 					(SELECT id_office FROM offices WHERE id=id_office) as id_office,
					(SELECT id_dept FROM departemen WHERE id=id_dept) as id_dept,
					status,created_id,created_date,last_updated";
	
		//$data['profiles'] = $this->crud->browse("","profiles","id",$id,"false",$select);
		
		$office = $this->crud->browse("","offices","","","true","id,office_name");
		if(!$office) $office = array();
		foreach($office as $val){ $option_office[$val->id] = $val->office_name; }
		$data['offices'] = $option_office;
		
		$department = $this->crud->browse("","departemen","","","true","id,dept_name");
		if(!$department) $department = array();
		foreach($department as $val){ $option_dept[$val->id] = $val->dept_name; }
		$data['department'] = $option_dept;
		$data['profiles'] = $this->crud->browse("","profiles","id",$id,"false",$select);
		//print_r($data['department']);print_r($data['offices']);exit;
		//$data['browse'] = $this->crud->browse("","contact_person","id_vendor",$venID,"true");
		$this->load->view('profiles/form_update',$data);
	}
	
	function profiles_update(){
		$curr_date =  date('Y-m-d H:i:s');		
		$venID      = $this->session->userdata("id");
		$where      = array('id'=>$venID);
		$is_exist   = $this->crud->is_exist("","profiles","id",$where);
		$userID     = $this->tank_auth->get_user_id();
		//$venID = $this->session->userdata("venID");
		$data = array('id'			=>$venID,
					  'name'		=>$_POST['name'],
					  'nopeg'		=>$_POST['nopeg'],
				      'id_office'	=>$_POST['id_office'],
					  'id_dept'		=>$_POST['id_dept'],
				      'phone'		=>$_POST['phone'],
					  'mobile'		=>$_POST['mobile'],
					  'userID'		=>$userID,
					  'created_id'  =>$userID,
					  'last_updated'=>$curr_date);
			
		//$this->crud->update("","profiles","id",$_POST['id'],$post);
		
		if($is_exist){
			unset($data['id']); unset($data['created_id']); unset($data['created_date']);
			$this->crud->update("","profiles","id",$venID,$data);
		} else { $this->crud->insert("","profiles",$data); }
	}
	
	function update(){
		$curr_date =  date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data = array('name'		=>$_POST['name'],
					  'nopeg'		=>$_POST['nopeg'],
					  'id_office'	=>$_POST['id_office'],
					  'id_dept'		=>$_POST['id_dept'],
					  'phone'		=>$_POST['phone'],
				      'mobile'		=>$_POST['mobile'],
					  'last_updated'=>$curr_date);
		$this->crud->update("","profiles","id",$_POST['id'],$data);
		//var_dump($data);die;
		$this->session->set_flashdata('message','1 data success update');
		redirect('profiles/','refresh');
	}
	
}