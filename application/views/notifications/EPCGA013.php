Dengan hormat,<br/>
<b>Bapak/Ibu <?php echo $to; ?></b><br/>

Sehubungan dengan telah dilakukan tahap evaluasi proses sourcing, dengan ini disampaikan :<br/><br/>

Company Name :<?php echo $vendor_name; ?>	<br/>
No. Registrastion:<?php echo $reg_num ?> <br/>

<table cellpadding="2" cellspacing="2" width="50%">
	<tr>	
		<th style="text-align: left;">Category</th>	
		<th style="text-align: left;">Sub Category</th>	
	</tr>
	
	<?php if($category){ $i=1;
 		  foreach($category as $row){ ?>
 	<tr>		
		<td><?php echo $row->category; ?></td>	
		<td><?php echo $row->subcategory; ?></td>		
	</tr>
	<?php	$i++;  } ?>	
	<?php } else { ?>
		<tr><td colspan="8">-</td></tr>
	<?php } ?>	
</table>
<br/><br/>

Telah dilakukan validasi ID pada sistem.<br/>
Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/><br/>
