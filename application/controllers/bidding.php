<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Bidding extends Controller {

	function __construct()
	{
		parent::__construct();		
		$this->load->helper('file');
		$this->load->model(array('m_content','codec','m_announce'));
		islogged_in();

		$this->permit = $this->crud->get_permissions('9');

		define(EPROC_RFP_PATH, "./uploads/rfp/");
		define(EPROC_LOI_PATH, "./uploads/LOI/");
			
	} 

	function index(){

		/* get data from defined function for table view */

		$extract = $this->getDataTables(10);

		/* ==== */

		/* preparing data for display */

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];

		$data['category'] 		 = $this->getCategories();
		$data['subcategory'] 	 = $this->getSubcategories();
		// $data['subcontent_type'] = $this->getSubcontentTypes();
		$data['content_type'] 	 = $this->getContentTypes('bidding');
		$data['subcontent_type'] = $this->getSubcontentTypes('bidding');
			
			
		// $data['controller']  = $this->getControllerBySubcontentId();
		$data['status_ref'] 	 = $this->getStatusCondition();

		$data['view'] = "bidding/browse";

		$this->load->view('layout/template',$data);

		/* ==== */

	}

	private function search_input($search_dateranges = array(),$search_conditions = array()){

		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');

			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}

		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');

			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}

		$getData = array($search_dateranges,$search_conditions);

		return $getData;
	}

	function search(){

		/* initiate search inputs */

		$search_dateranges = array('publish_date',$_POST['start_date'],$_POST['end_date']);
		$search_conditions = array(
								'code'		=> $_POST['search_term'],
								'title_ind' => $_POST['search_term'],
								'title_eng' => $_POST['search_term']
							  );

		$where_conditions  = $this->search_input($search_dateranges,$search_conditions);

		/* ==== */

		/* get data from defined function for table view */

		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);

		/* ==== */

		/* preparing data for display */

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];

		$data['category'] 		 = $this->getCategories();
		$data['subcategory'] 	 = $this->getSubcategories();
		$data['subcontent_type'] = $this->getSubcontentTypes();
		$data['status_ref'] 	 = $this->getStatusCondition();

		$data['view'] = "bidding/browse";

		$this->load->view('layout/template',$data);

		/* ==== */

	}

	function update_direct(){
		$data = array('status' => $_POST['participation_status']);

		$where = "id_announce = ".$_POST['announcement_id']." AND id_vendor = ".$_POST['vendor_id'];

	 	$this->crud->multiple_update("","announce_join_participant",$where,$data);

	 	redirect('bidding/detail/'.$_POST['announcement_id']);
	}

	function update_status(){

		$data 	   = array('status' => $_POST['edit_invitation_status']);
		$loi_field = $_FILES['loifile']["error"] != 4?$_FILES['loifile']:false;

		$where = "id_announce = ".$this->uri->segment(3)." AND id_vendor = ".$_POST['edit_vendor_id'];			

	 	$this->crud->multiple_update("","announce_join_participant",$where,$data);

	 	if ($loi_field) {
	 		$announce = $this->crud->browse("","announce","id",$this->uri->segment(3),"false","file");
	 		$vendor   = $this->crud->browse("","vendor","id",$_POST['edit_vendor_id'],"false","register_num");

	 		$upload_file = $files['name'];
			$curr_date	 = date('Y-m-d H:i:s');
			$regNum 	 = $vendor->register_num;
				
	 		$filename1 = $announce->file;
	 		$filename2 = $regNum.'.pdf';

	 		$path = 'uploads' ;
	 		if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }
		    
		    $path .= '/LOI' ; 
		    if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }

		    $path .= '/'.$filename1 ; 
		    if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }

	 		$path .= '/'.$filename2 ; 
			$tipe = $loi_field["type"];			

			if($tipe=='application/pdf')
				$upload_file = move_uploaded_file($loi_field['tmp_name'], $path) ;
	 	}

	 	redirect('bidding/detail/'.$this->uri->segment(3));
	}

	private function upload_pdf_file($fileresource = '',$path = '',$filename = '',$extension = ''){
 		$_FILES['userfile'] = $fileresource;
 		$config = $this->m_announce->set_config($filename.".".$extension,$path,'pdf');
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config);
 		 		 		
 		$msg['error_'.$extension] = "";  $msg['success_'.$extension] = array();		
		if ( ! $this->upload->do_upload()){
			$msg['error_'.$extension] = $this->upload->display_errors();
		} else {
			$msg['success_'.$extension] = $this->upload->data(); }
	}

	function detail(){

		$id = $this->uri->segment(3);

		$data['user_id']    = $this->tank_auth->get_user_id();
		$data['username']   = $this->tank_auth->get_username(); // get currently login username
		$data['deptId']     = $deptId = afterloginGetDeptId();
		$data['controller'] = $this->getControllerBySubcontentId();

		$def = $this->getDataTablesDetailHeader($id);
			
		if($def->file){
			$ctpath_id  = "./uploads/contents/".$def->file."_ind.txt";
			$ctpath_eng = "./uploads/contents/".$def->file."_eng.txt";
			$aapath_id  = "./uploads/aanwijzing/".$def->file."_ind.txt";
			$aapath_eng = "./uploads/aanwijzing/".$def->file."_eng.txt";
			$rfppath  	= "./uploads/rfp/".$def->file.".pdf";

			$document_reqs = $this->getDocumentReq($def->file);
			
			if (count($document_reqs)>0) {
				$data['documents'] = $document_reqs;
			}

	 		$data['content_ind'] 	= file_exists($ctpath_id) ? file_get_contents($ctpath_id) : "-";
	 		$data['content_eng'] 	= file_exists($ctpath_eng) ? file_get_contents($ctpath_eng) : "-";
	 		$data['aanwidzing_ind'] = file_exists($aapath_id) ? file_get_contents($aapath_id) : "-";
	 		$data['aanwidzing_eng'] = file_exists($aapath_eng) ? file_get_contents($aapath_eng) : "-";
	 		$data['rfpfile']	    = file_exists($rfppath) ? true : false;
	 	}

		$data['def'] = $def;

		$subcontent = $this->crud->browse("","subcontent_type","id",$def->subcontent_type,"false","id");

		if ($subcontent->id == '5') {
			$data['vendor_type'] 		= $this->getVendorType();
			$data['view'] 		 		= "bidding/detail_direct";
			$data['def_detail'] 		= $this->getDataTablesDirectInitial($id);
			$data['status_reference']   = $this->getStatusParticipantDirect();
			$data['status_select']   	= $this->getStatusParticipantDirectSelect();
				
		}else{
			$data['vendor_type'] 		= $this->getVendorType();
			$data['def_detail'] 		= $this->getDataTablesDetail($id);
			$data['view'] 				= "bidding/detail";
			$data['def_detail_initial'] = $this->getDataTablesDetailInitial($id);
			$data['status_reference']   = $this->getStatusParticipant();
			$data['status_select']	    = $this->getStatusParticipantSelect();
		}

		if ( $deptId->id_dept == $def->id_departement ) {
	 		$data['paramAproval']   = true;
	 		$this->session->set_userdata('file_ticket',uniqid());
	 	}	 		

	 	$categories = $this->crud->browse("","subcontent_type","id",$def->subcontent_type,"true","id,subtype_name,document_req_id,need_vendor");
		if(!$categories) $categories = array();
		foreach($categories as $val){ 
			$category 	  = $val->subtype_name;
		}

		$data['form'] = $category;

	 	$data['def_winner_list'] 	= $this->getDataTablesWinner($id,$def->subcontent_type);

		$data['loi_path']			= 'LOI/'.$def->file.'/';
		$data['permit']				= $this->permit;
			
		$data['status_ref'] 	    = $this->getStatusCondition();
		$data['offices'] 		    = $this->getOffices();
		$data['document_req'] 	    = $this->getDocumentReq($def->subcontent_type);

		$this->load->view('layout/template',$data);
	}

	function open_file(){

		$param_offset = 2;
		$params 	  = array_slice($this->uri->rsegment_array(), $param_offset);

		if ($params[0] == 'LOI') {
			$filename  = $params[2];
			$extension = 'pdf';
			$path 	   = 'uploads/'.$params[0].'/'.$params[1];
			$file 	   = $path.'/'.$filename.'.'.$extension;
		}
		
		redirect(base_url($file));
	}

	function confirm_status(){
		$value 	  = implode(',', $_POST['value']);
			
		if ($_POST['status'] != '99') {
			$data  = array(
						'status'	   => $_POST['status'],
						'last_updated' => date('Y-m-d H:i:s')
					);
			$where = "id_announce = ".$_POST['id_announce']." AND id_vendor IN (".$value.")";
			$this->crud->multiple_update("","announce_join_participant",$where,$data);
		}else{				
			foreach ($_POST['value'] as $row) {
				$this->delete_participant($_POST['id_announce'],$row);
			}
		}

		$this->sendNotifications($_POST['status'],$_POST['id_announce'],$_POST['value']);
	}

	private function sendNotifications($flag = "", $id_announce = "", $value = ""){	
		foreach ($value as $row) {
			emailSendNotificationsVendorForOpenBidding($flag,$id_announce,$row);
		}
	}

	function decide_winner(){
		
		$curr_date = date('Y-m-d H:i:s') ;

		if ($_POST['status_winner'] == 'select-winner') {
			
			$value = implode(',', $_POST['value']);

			$data = array('status' => '2' , 'last_updated' => $curr_date );

			$where = "id_announce = ".$_POST['id_announce']." AND id_vendor IN (".$value.")";

			$this->crud->multiple_update("","announce_join_participant",$where,$data);

			emailSendNotificationsForWinnerBiddingORSourcing($id_announce = $_POST['id_announce'], $listvendor = $value);

		}elseif ($_POST['status_winner'] == 'submit-winner') {
			$status_winner = '3';

			sort($_POST['value']);

			$clength = count($_POST['value']);
			$flag1 = '';
			$flag2 = '';
			$winner = '';
			$reject = '';
			for($x = 0; $x < $clength; $x++) {
			   
			    $k = explode('-', $_POST['value'][$x]);

				if( $k[0] == '3' ){
					if($flag1=='')
					{
						$winner .= $k[1];
						$flag1='1';
					}else{
						$winner .= ','.$k[1];
					}


				}else{
					if($flag2=='')
					{
						$reject .= $k[1];
						$flag2='1';
					}else{
						$reject .= ','.$k[1];
					}
				}

			}

			$this->moveFixedAttachment($_POST['id_announce']);

			if($reject!='')
			{
				$data2 = array('status' => '4' , 'last_updated' => $curr_date );

				$where2 = "id_announce = ".$_POST['id_announce']." AND id_vendor IN (".$reject.")";

				$this->crud->multiple_update("","announce_join_participant",$where2,$data2);
			}

			if($winner!='')
			{
				$data = array('status' => '3' , 'last_updated' => $curr_date );

				$where = "id_announce = ".$_POST['id_announce']." AND id_vendor IN (".$winner.")";

				$this->crud->multiple_update("","announce_join_participant",$where,$data);


				$data_announce = array( 'status' => '7' );

				$this->crud->update("","announce","id",$_POST['id_announce'],$data_announce);

				emailSendNotifWinnerBiddingORSourcingToVendor($id_announce = $_POST['id_announce'], $listvendor = $winner);

				emailSendNotifthankjoinBiddingORSourcingToVendorToVendor($id_announce = $_POST['id_announce'], $listvendor = $winner);
			}

			emailSendNotifToAdminAboutWinnerBiddingORSourcing($id_announce = $_POST['id_announce'], $listvendorwinner = $winner, $listvendorreject = $reject);
			
		}

	}

	function approve_status($id,$vendor){
		
		$data = array(
					'status' 	   => '1',
					'last_updated' => date('Y-m-d H:i:s')
				);

		$where = "id_announce = ".$id." AND id_vendor = ".$vendor;

		$this->crud->multiple_update("","announce_join_participant",$where,$data);
		emailSendNotificationsVendorForOpenBidding('1',$id,$vendor);

		// sendEmailToVendorAboutApproveRejectJoinBidding($id,$vendor,$flag='1');

		redirect('bidding/detail/'.$id);
	}

	function reject_status($id,$vendor){
		$data = array('status'			=> 	'0',
			'last_updated'	=> 	date('Y-m-d H:i:s')
			);

		$where = "id_announce = ".$id." AND id_vendor = ".$vendor;

		$this->crud->multiple_update("","announce_join_participant",$where,$data);
		emailSendNotificationsVendorForOpenBidding('0',$id,$vendor);

		// sendEmailToVendorAboutApproveRejectJoinBidding($id,$vendor,$flag='0');

		redirect('bidding/detail/'.$id);
	}

	function delete_participant(){
		$id_announcex = $_POST['idannounce'];
		$idvenx = $_POST['idven'];
		$where = array('id_announce'=>$id_announcex,'id_vendor'=>$idvenx);
		$this->crud->multiple_delete("","announce_join_participant",$where);
		if (!$this->input->is_ajax_request()) {
			redirect('bidding/detail/'.$id); }
	}

	function openFileRFP(){

		$param_offset = 2;
		$params 	  = array_slice($this->uri->rsegment_array(), $param_offset);

		$filename  = $params[0];
			
		$path 	   = EPROC_RFP_PATH;
		$extension = 'pdf';
		$file 	   = $path.$filename.'.'.$extension;
		
		redirect(base_url($file));
	}

	private function getCategories()
	{
		/* get categories data from database */

		$select = array(''=>'All');

		$iterations = $this->crud->browse("","category","","","true","id,category");

		if(!$iterations) $iterations = array();
		foreach($iterations as $val){ $iteration[$val->id] = $val->category; } 
		$getData = $select+$iteration;

		/* ===== */

		return $getData;
	}

	private function getSubcategories()
	{
		/* get subcategories data from database */

		$select = array(''=>'All');

		$iterations = $this->crud->browse("","subcategory","","","true","id,subcategory");

		if(!$iterations) $iterations = array();
		foreach($iterations as $val){ $iteration[$val->id] = $val->subcategory; } 
		$getData = $select+$iteration;

		/* ===== */

		return $getData;
	}

	private function getContentTypes($flag = '')
	{
		/* get categories data from database */

		$getData = array();
		if ($this->session->userdata('content_allowed')) {
			$select  = array(''=>'All');

			$allowed_key = implode(',', array_keys($this->session->userdata('content_allowed')));
			$allowed_val = implode(',', $this->session->userdata('content_allowed'));				

			$where   = $this->session->userdata('content_allowed')?'id IN ('.$allowed_key.')':'';

			$iterations = $this->crud->browse("","content_type","","","true","id,type_name",$where);

			if(!$iterations) $iterations = array();
			foreach($iterations as $val){ $iteration[$val->id] = $val->type_name; } 
			$getData = $select+$iteration;
			$result  = $getData;

			if ($flag != '') {
				$result  = array();
				if ($flag == 'bidding') {
					$temps  = $this->crud->browse("","content_type","","","false","id,type_name",'id IN ("2","3")');
				}elseif ($flag == 'sourcing') {
					$temps  = $this->crud->browse("","content_type","","","false","id,type_name",'id IN ("2")');
				}
				if(!$temps) $temps = array();
				foreach($temps as $val){ $result[$val->id] = $val->type_name; }
				$getData = $select+$result;
				$result  = $getData;
			}
		}

		/* ===== */

		return $result;
	}

	private function getSubcontentTypes($flag = '')
	{
		/* get categories data from database */

		$getData = array();

		// if ($this->session->userdata('subcontent_allowed')) {
			$select = array(''=>'All');

			$allowed_key = implode(',', array_keys($this->session->userdata('subcontent_allowed')));
			$allowed_val = implode(',', $this->session->userdata('subcontent_allowed'));

			$where  = $this->session->userdata('subcontent_allowed')?'id IN ('.$allowed_key.')':'';

			$iterations = $this->crud->browse("","subcontent_type","","","true","id,subtype_name",$where);

			if(!$iterations) $iterations = array();
			foreach($iterations as $val){ $iteration[$val->id] = $val->subtype_name; } 
			$getData = $select+$iteration;
			$result  = $getData;

			if ($flag != '') {
				$result  = array();
				if ($flag == 'bidding') {
					$temps  = $this->crud->browse("","subcontent_type","","","true","id,subtype_name","id IN ('3','5','6')");
				}elseif ($flag == 'sourcing') {
					$temps  = $this->crud->browse("","subcontent_type","","","true","id,subtype_name","id IN ('4')");
				}
				if(!$temps) $temps = array();
				foreach($temps as $val){ $result[$val->id] = $val->subtype_name; }
				$getData = $select+$result;
				$result  = $getData;
			}
		// }			

		/* ===== */

		return $result;
	}

	private function getDataTables($limit = NULL)
	{
		$sess_cat = $this->session->userdata('sess_cat');
		$sessID   = $this->session->flashdata('anID');
			
		$field    = $sess_cat ? "id_cat" : "";
		$id_cat   = $sess_cat ? $sess_cat  : "";

		if($sess_cat){	
			$where = "t1.subcontent_type IN ('3', '5', '6') AND t1.status IN ('3','7')";
		} else { 
			$where = !$this->session->userdata('is_admin') ? "subcontent_type IN ('3', '5', '6') AND t1.status IN ('3','7')":"";
		}

		/* get data for table view from database with pagination */

		$table 	     = "announce t1";
		$select 	 = "t1.id,code,content_type,subcontent_type,title_ind,title_eng,first_valdate,end_valdate,t1.status,t1.created_date,publish_date,t1.id_cat,t1.id_subcat";
		$order 		 = array('field'=>'t1.created_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);

		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);

		// if($sessID)	$getData = $this->crud->browse("",$table,"id",$sessID,"true",$select,$where);
		// else  {
		$joins[0][0] = 'announcement_level_approval t2';
		$joins[0][1] = 't2.id_announcement = t1.id';
		$joins[0][2] = 'left';
		$joins[1][0] = 'level t3';
		$joins[1][1] = 't2.id_level = t3.id';
		$joins[1][2] = 'left';

		// $where		 = !$this->session->userdata('is_admin') ? "t1.subcontent_type IN ('4','5') AND t1.status = '3'":"";

		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,$order,$per_page,$offset,"t1.id");
		// }

		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'bidding/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);

		/** setup for pagination **/

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();

		/** ===== **/
		/* ===== */

		/* setup variable to used in another functions */

		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;

		/* ===== */

		return $data;

	}

	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
		$subcontent_allowed = globalNeedAanwidzing();

		$sess_cat = $this->session->userdata('sess_cat');
		$sessID   = $this->session->flashdata('anID');
			
		$field    = $sess_cat ? "id_cat" : "";
		$id_cat   = $sess_cat ? $sess_cat  : "";			

		if($sess_cat){	
			$where = "t1.subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'";
		} else { 
			$where = !$this->session->userdata('is_admin') ? "subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'":"";
		}

		/* get data for table view from database with pagination */

		$table 	     = "announce t1";
		$select 	 = "t1.id,code,content_type,subcontent_type,title_ind,title_eng,first_valdate,end_valdate,publish_date,t1.status,t1.created_date,t1.id_cat,t1.id_subcat";
		$order 		 = array('field'=>'t1.created_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);

		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);

		$joins[0][0] = 'announcement_level_approval t2';
		$joins[0][1] = 't2.id_announcement = t1.id';
		$joins[0][2] = 'left';
		$joins[1][0] = 'level t3';
		$joins[1][1] = 't2.id_level = t3.id';
		$joins[1][2] = 'left';

		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,$where,$dateranges,$wherearray,$order,$per_page,$offset,"t1.id");

		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'bidding/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);

		/** setup for pagination **/

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();

		/** ===== **/
		/* ===== */

		/* setup variable to used in another functions */

		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;

		/* ===== */

		return $data;

	}

	private function getDataTablesDetailHeader($arg1 = null){

		$table = "announce a";

		$joins = array();
		$joins[0][0] = 'content_type ct';
		$joins[0][1] = 'ct.id = a.content_type';
		$joins[0][2] = 'left';
		$joins[1][0] = 'subcontent_type st';
		$joins[1][1] = 'st.id = a.subcontent_type';
		$joins[1][2] = 'left';
		$joins[2][0] = 'category c';
		$joins[2][1] = 'c.id = a.id_cat';
		$joins[2][2] = 'left';
		$joins[3][0] = 'subcategory s';
		$joins[3][1] = 's.id = a.id_subcat';
		$joins[3][2] = 'left';

		$where = '';

		$getData =  $this->crud->browse_join("",$table,"a.id",$arg1,"false","a.status,code,type_name,subtype_name,file,subcontent_type,first_valdate,end_valdate,id_office,title_ind,title_eng,category,subcategory,id_departement",$joins);

		return $getData;
	}

	private function getDataTablesDetail($arg1 = null){

		$table  = "announce_join_participant a";
		$select = "IF(a.is_redlist = '1',('redlist'),('')) AS status_vendor,a.id_announce,v.id AS vendor_id,register_num,vendor_name,vendor_type,a.created_date,a.last_updated,a.status,a.suratkep";

		$joins = array();
		$joins[0][0] = 'vendor v';
		$joins[0][1] = 'v.id = a.id_vendor';
		$joins[0][2] = 'left';

		$where = "a.status NOT IN ('6','80','81')";

		$getData =  $this->crud->browse_join("",$table,"a.id_announce",$arg1,"true",$select,$joins,$where);

		return $getData;
	}

	private function getDataTablesWinner($arg1 = null , $arg2 = null){

		$table  = "announce_join_participant a";
		$select = "a.id_announce,v.id AS vendor_id,register_num,vendor_name,a.created_date,a.last_updated,a.status";

		$joins = array();
		$joins[0][0] = 'vendor v';
		$joins[0][1] = 'v.id = a.id_vendor';
		$joins[0][2] = 'left';

		if($this->permit->sw) {
			if ($arg2 == '6' OR $arg2 == '3') {
				$where = "a.status = '1'";
			}else{
				$where = "a.status = '80'";
			}
		}elseif($this->permit->cw) {
			$where = "a.status = '2'";
		}

		$getData =  $this->crud->browse_join("",$table,"a.id_announce",$arg1,"true",$select,$joins,$where);

		return $getData;
	}

	private function getDataTablesDetailInitial($arg1 = null){

		$table  = "announce_join_participant a";
		$select = "IF(a.is_redlist = '1',('redlist'),('')) AS status_vendor,a.id_announce,v.id AS vendor_id,register_num,vendor_name,vendor_type,a.created_date,a.last_updated,a.status";

		$joins = array();
		$joins[0][0] = 'vendor v';
		$joins[0][1] = 'v.id = a.id_vendor';
		$joins[0][2] = 'left';

		$where = "a.status IN ('6','80','81')";
		// $where = "a.status IS NULL";

		$getData =  $this->crud->browse_join("",$table,"a.id_announce",$arg1,"true",$select,$joins,$where);

		return $getData;
	}

	private function getDataTablesDirectInitial($arg1 = null){

		$table  = "announce_join_participant a";
		
		$select = "IF(a.is_redlist = '1',('redlist'),('')) AS status_vendor,a.id_announce,v.id AS vendor_id,register_num,vendor_name,vendor_type,a.created_date,a.last_updated,a.status AS stt,a.suratkep";

		$joins = array();
		$joins[0][0] = 'vendor v';
		$joins[0][1] = 'v.id = a.id_vendor';
		$joins[0][2] = 'left';

		// $sess_status = $this->session->userdata('sess_status');
		// $sessID   = $this->session->flashdata('anID');
			
		// $field    = $sess_status ? "a.status" : "";
		// $id_status   = $sess_status ? $sess_status  : "";			

		// if($sess_cat){	
			//$where = "a.id_announce = ".$arg1." AND a.status IN ('6','80','81','82','83')";
		// } else { 
		// 	$where = !$this->session->userdata('is_admin') ? "a.id_announce = ".$arg1." AND a.status IN ('6','80','81','82','83')":"";
		// }

		//$where = "a.status IN ('80','81','82','83')";

		$getData =  $this->crud->browse_join("",$table,"a.id_announce",$arg1,"true",$select,$joins,$where);

		return $getData;
	}

	function set_sess_category(){
 	
	 	$id_cat = $_POST['id_cat'];

	 	if($id_cat!=0) 	{ $this->session->set_userdata('sess_cat',$id_cat);
	 	} else { $this->session->unset_userdata('sess_cat'); }
	 	
	}

	function set_sess_status(){
 	
	 	$id_status = $_POST['id_status'];

	 	if($id_status!=0) 	{ $this->session->set_userdata('sess_status',$id_status);
	 	} else { $this->session->unset_userdata('sess_status'); }
	 	
	}

	private function getStatusCondition(){
		$data = array();

		$data = array(
					0  => 'New',
					1  => 'Published',
					2  => 'Open for Joining',
					3  => 'Ongoing',
					4  => 'Closed',
					99 => 'Canceled'
/*					0  => '',
					1  => 'Create',
					2  => 'New',
					3  => 'Approve',
					4  => 'Published',
					5  => 'Draft',
					6  => 'Rejected',
					7  => 'Closed',
					8  => 'Canceled',
					9  => 'Expired',
					10 => 'English Completion',
					11 => 'Update Need Resend Email',
					99 => 'Unpublish'*/
				);

		return $data;
	}

	private function getStatusParticipantSelect(){
		$data = array();

		if ($this->permit->sw) {
			$data = array(
						1  => '<span class="label label-primary">Allowed</span>',
						0  => '<span class="label label-danger">Rejected</span>',
						90 => '<span class="label label-warning">Not Yet Responded</span>'
					);
		}

		return $data;
	}

	private function getStatusParticipant(){
		$data = array();

		$data = array(
						0  => '<span class="label label-danger">Rejected</span>',
						1  => '<span class="label label-primary">Allowed</span>',
						2  => '<span class="label label-success">Winner Unconfirmed</span>',
						3  => '<span class="label label-default">Winner</span>',
						4  => '<span class="label label-warning">Reject Unconfirmed Winner</span>',
						6  => '<span class="label label-info">New</span>',
						80 => '<span class="label label-primary">Joined</span>',
						81 => '<span class="label label-warning">Not Join</span>',
						90 => '<span class="label label-warning">Not Yet Responded</span>',
						91 => '<span class="label label-primary">Vendor Registered/Joined</span>',
						'redlist' => '<span class="label label-warning">Redlist</span>'
					);

		return $data;
	}

	private function getStatusParticipantDirectSelect(){
		$data = array();

		if ($this->permit->sw) {
			$data = array(
						83 => '<span class="label label-warning">No Response</span>',
						84 => '<span class="label label-warning">No Bid</span>',
						90 => '<span class="label label-warning">Not Yet Responded</span>'
					);
		}

		return $data;
	}

	private function getStatusParticipantDirect(){
		$data = array();

		$data = array(
					2  => '<span class="label label-success">Winner Unconfirmed</span>',
					3  => '<span class="label label-default">Winner</span>',
					4  => '<span class="label label-warning">Reject Unconfirmed Winner</span>',
					6  => '<span class="label label-info">New</span>',
					80 => '<span class="label label-primary">Joined</span>',
					81 => '<span class="label label-warning">Not Join</span>',
					82 => '<span class="label label-warning">Waiting</span>',
					83 => '<span class="label label-warning">No Response</span>',
					84 => '<span class="label label-warning">No Bid</span>',
					90 => '<span class="label label-warning">Not Yet Responded</span>',
					91 => '<span class="label label-primary">Vendor Registered/Joined</span>',
					'redlist' => '<span class="label label-warning">Redlist</span>'
				);

		return $data;
	}

	private function getVendorType(){
		$data = array();

		$data = array(
					'avl'  		=> 'AVL',
					'new'  		=> 'New',
					'shortlist' => 'Shortlist',
					'reject' 	=> 'Reject',
				);

		return $data;
	}

	private function getOffices() {

	 	$datas = $this->crud->browse("","offices","","","false","id,office_name");
	 	foreach ($datas as $data_temp) {$data[$data_temp->id] = $data_temp->office_name;}

	  	return $data;
	}

	private function getDocumentReq($filename) {

	 	$where = 'filename = '.$filename;

		$getData = $this->crud->browse("","document_requirement","filename",$filename,"true","id,filename,counter,description,remarks,created_date",$where);

	  	return $getData;
	}

	function deleteDocumentReq(){
		$id 	 = $_POST['arg'];
		$explode = explode('_', $id);
		$where 	 = array(
					'filename' => $explode[0],
					'counter'  => $explode[1]
				   );

		$path = "./uploads/document_requirements/".$id.".pdf";

		$this->crud->multiple_delete("","document_requirement",$where);
			
		if (file_exists($path)) unlink($path);
	}


	//////  Added By Dedi Rudiyanto  //////

	function upload_attachment($idven){

		$attachment_field = $_FILES['pdffile']["error"] != 4?$_FILES['pdffile']:false;
		$uniqid 		  = !$this->session->userdata('file_ticket') ? $this->session->set_userdata('file_ticket',uniqid()) : $this->session->userdata('file_ticket');
		$where 			  = "id = '".$uniqid."'";
		$count 			  = $this->crud->browse('','document_requirement','id',$uniqid,'false','MAX(counter) AS max');
			
		$data = array(
	 			'id'		   => $uniqid,
				'counter' 	   => ($count->max==null?0:$count->max)+1,
				'created_date' => date('Y-m-d H:i:s'),
				'updated_date' => date('Y-m-d H:i:s'),
				'file'		   => $idven
				);

		/* upload attachment file */
	 	if($attachment_field){
	 		$path 	   = './uploads/tmp_attach/';
	 		$extension = 'pdf';

	 		if ($this->crud->is_valid_to_upload($attachment_field,array($extension),5000000,'text')) {
		 		$this->upload_attachment_file($attachment_field,$path,$uniqid.'_'.(($count->max==null?0:$count->max)+1),$extension);
		 		$this->crud->insert('','document_requirement',$data);
		 	}else{
			    redirect('bidding/detail/'.$id,'refresh');
 			}
	 	}

	 	/* ---------------- */
	}

	function winningTab_click(){
		$this->session->unset_userdata('file_ticket');
	 	$this->session->set_userdata('file_ticket',uniqid());
	 	return false;
	}

	function deleteAttachment(){
		$raw = $_POST['file_attachment'];
		$path1= './uploads/tmp_attach/';
		$path2= './uploads/attach/';
		if ($raw == 'max') {
			$uniqid = !$this->session->userdata('file_ticket') ? $this->session->set_userdata('file_ticket',uniqid()) : $this->session->userdata('file_ticket');
			$count  = $this->crud->browse('','document_requirement','id',$uniqid,'false','MAX(counter) AS max');
			$raw 	= $uniqid.'_'.$count->max;
			$id 	= explode('_',$uniqid.'_'.$count->max);
		}else{
			$id = explode('_', $raw);
		}			

		$check 	   	= $this->crud->get_total_record('','document_requirement','filename ="'.$id[0].'"');
		$path 	   	= $check>0 ? $path2 : $path1;
		$field 		= $check>0 ? 'filename':'id';
		$extension 	= 'pdf';
			
		if (count($id)>0) {
			for ($i = 1; $i <= $count->max; $i++)
			{	$raw = $uniqid.'_'.$i;
				unlink($path.$raw.'.'.$extension);
			}
			$this->crud->multiple_delete('','document_requirement',$field,"(id='$uniqid')");
		}
			
	 	$this->session->unset_userdata('file_ticket');
	 	$this->session->set_userdata('file_ticket',uniqid());
	 	return false;
	}

	private function upload_attachment_file($fileresource = '',$path = '',$filename = '',$extension = ''){
 		$_FILES['userfile'] = $fileresource;
 		$config = $this->m_announce->set_config($filename.".".$extension,$path,'pdf');
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config);
 		 		 		
 		$msg['error_'.$extension] = "";  $msg['success_'.$extension] = array();		
		if ( ! $this->upload->do_upload()){
			$msg['error_'.$extension] = $this->upload->display_errors();
		} else {
			$msg['success_'.$extension] = $this->upload->data(); }
	}

	private function getControllerBySubcontentId(){
	 	$controller_lists = array(
			 			'1'=>'news_public',
			 			'2'=>'news_blast',
			 			'3'=>'open_bidding',
			 			'4'=>'open_sourcing',
			 			'5'=>'direct_selection',
			 			'6'=>'limited_bidding'
			 			);

	 	return $controller_lists;
 	}

	private function moveFixedAttachment($id_announce){
		$path_temp 	= './uploads/tmp_attach/' ;
		$path_new 	= './uploads/attach/' ;
		$uniqid 	= $this->session->userdata('file_ticket');
		$order 		= array('field'=>'file','order'=>'ASC');
		$datafile  	= $this->crud->browse('','document_requirement',"id",$uniqid,"true","counter,file","",$order);
		$samefile 	= '';
		$i 			= 0 ;

		foreach ($datafile as $value) {
			$name_old  = $uniqid."_".$value->counter;
			$name_new  = $name_old;
	 		rename ( $path_temp.$name_old.'.pdf', $path_new.$name_new.'.pdf' );

	 		if($value->file != $samefile) $i++ ;
	 		$a[$i][]	= $name_old.'.pdf';
	 		$b[$i]		= $value->file;
	 		$samefile	= $value->file ;
		}

		for ($n = 1; $n <= $i; $n++)
		{
			$filename 	= implode(",",$a[$n]);
			$idven 	  	= $b[$n] ;
			$where    	= "id_announce = ".$id_announce." AND id_vendor = ".$idven ;
		 	$data_attach= array('suratkep' => $filename);
	 		$this->crud->multiple_update('','announce_join_participant',$where,$data_attach);
		}
		$wherex = array('id'=>$uniqid);
		$this->crud->multiple_delete("","document_requirement",$wherex);
	}

	function download($filename='')
	{ 	$this->load->helper('download');
		$path 	= './uploads/attach/' ;
		if($filename=='') redirect(base_url(),'refresh') ;
		$data = file_get_contents($path.$filename);  
		force_download($filename, $data); 
	}

	function cancelBidding(){
		$id = $this->uri->segment(3);

		$where = "id = ".$id;
	 	$data  = array('status' => '8');
 		$this->crud->multiple_update('','announce',$where,$data);

 		sendNotifToVendorCancelledBidding($id);

 		redirect('bidding');
	}

}

?>