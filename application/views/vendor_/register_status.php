<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   
	   $('#btn_change').click(function(){ 	
	   			   		
	   		var baseUrl = "<?php echo base_url(); ?>";
		 	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
			var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
			var opt     = $("input:radio[name=status]:checked").val();						
			$.ajax({
					url  : baseUrl + 'vendor/change_register_status',
					type : "POST",              		               
					data : csrf + '=' + token +'&opt=' + opt,
					success: function(resp){
						$("#info_status").load(baseUrl+'vendor/load_status_info');
					}
			});		
	   		
	   });
	
   });
    
</script>

<div id="debug_temp"></div>

<div class = "panel panel-info">
   <div class = "panel-heading">
      <h3 class = "panel-title"><?php echo ucwords($vendor->reg_status); ?></h3>
   </div>
   
   <div class = "panel-body">      
      <?php     
      	foreach($status as $val){
      		 $checked = ($val->id == $vendor->reg_sts) ? true : false;
			 if(!$checked){ echo form_radio('status', $val->id, $checked); 
			 echo "&nbsp;".ucwords($val->status); 
			 echo "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;"; }
		}      
      ?>
      <input type="button" id="btn_change" value="Submit" class="btn btn-warning btn-sm"/>
   </div>
</div>

