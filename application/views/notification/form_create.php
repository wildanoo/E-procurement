<div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
                          
              <div class="box-header with-border">
                <h3 class="box-title">Create Notification</h3>
              </div>
              
<?php echo form_open("notification/create",array('class'=>'form-horizontal')); ?>
<table width="50%">
<tr>
	<td><label>Notifications</label></td><td>:</td>
	<td><!-- input type="text" name="notifications" id="notifications" placeholder="Notifications" class="form-control"/> -->
	<textarea class="form-control"rows="3" cols="" name="notifications" id="notifications" placeholder="Notifications"></textarea>
	</td>	
	<div id="msg1" style="color: red;font-style: oblique;"></div>		
</tr>
<tr>
	<td><label>Path</label></td><td>:</td>
	<td><input type="text" name="path" id="path" placeholder="Path" class="form-control"/></td>	
	<div id="msg2" style="color: red;font-style: oblique;"></div>		
</tr>
<tr>
	<td><label>Editable</label></td><td>:</td>
	  <td>
		  <input type="radio" name="editable" id="editable" value="true"> True &nbsp;&nbsp;
		  <input type="radio" name="editable" id="editable" value="false"> False<br>
	  <div id="msg3" style="color: red;font-style: oblique;"></div>	
</tr>
</table>

</div><!-- col-md-6 --> 
</div><!-- /.box-body -->
<div class="box-footer" style="padding-left: 30px;">
					<input type="submit" value="Submit" class="btn btn-primary btn-sm"/>
                    <?php  $prop = array('class'=>'btn btn-sm btn-default','title'=>'Cancel'); ?>					 
					<?php  echo anchor('notification/','Cancel',$prop); ?>
                </div>
<?php echo form_close(); ?>            
			</div><!-- /.box -->
          </div>
        </div>