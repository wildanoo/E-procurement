<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Region extends Controller {

	function __construct()
	{
		parent::__construct();
		islogged_in();
	}	
		
	function index() {
		if ($this->tank_auth->is_logged_in ()) {
				
			$data ['user_id'] = $this->tank_auth->get_user_id ();
			$data ['username'] = $this->tank_auth->get_username ();

			//$where =  array('status'=>'1');
			
			//$sess_like  = $this->session->flashdata('like');
               
            $sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
			
				
			$field      = $sess_field != '' ? $sess_field : "region_name";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
				
			if($sess_cat != ''){	$where = array('region_name'=>$sess_cat);
			} else { $where = "";	}   
            
			$table = "region";
			$page = $this->uri->segment ( 3 );
			$per_page = 5;
			$offset = $this->crud->set_offset ( $page, $per_page );
			//$total_rows = $this->crud->get_total_record ("",$table,$where);
			$total_rows = $this->crud->get_total_record ("",$table);
			$set_config = array (
					'base_url' => base_url () . '/region/index',
					'total_rows' => $total_rows,
					'per_page' => 5,
					'uri_segment' => 3
			);
			$config = $this->crud->set_config ( $set_config );
				
			$this->load->library ( 'pagination' );
			$this->pagination->initialize ( $config );
			$paging = $this->pagination->create_links ();
				
			$order = array (
					'field' => 'created_date',
					'order' => 'DESC'
			);
			$data ['pagination'] = $paging;
			$data ['num'] = $offset;
			$select = "id,(SELECT country_name FROM country WHERE id=id_country) as country_name,region_name,remark,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
				
			//if($sess_like){
			//$like   = array('col'=>'region_name','field'=>$sess_like);
			//$browse = $this->crud->browse("",$table." l","","","true",$select,"","","",$like);	} else {
			//$browse = $this->crud->browse_with_paging("",$table." l","","","true",$select,"",$order,$config['per_page'],$offset); }			
					
			//$data['browse'] = $browse;
            $sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
				
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			$data['browse'] = $browse;

			$region = $this->crud->browse("","region","","","true","id,region_name");
			if(!$region) $region = array();
			$select = array(''=>'All');
			foreach($region as $val){ $options[$val->id] = $val->region_name; }
			$data['region'] = $select + $options;
            
	 		$data['view']	= "region/browse"; 		
 			$this->load->view('layout/template',$data);
 			
		} else {
			$this->session->set_flashdata('message','user not authorized');
			redirect ( '/auth/login/' );
		}
	}
	
	function get_region_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'region_name','sort'=>'ASC'); $select = "region_name as name";
		$result = $this->crud->autocomplete("","region",$select,"region_name",$term,"",$order);
		echo json_encode($result);
	}
	
	function set_sess_search(){
		$like = $_POST['search'];
		$this->session->set_flashdata('like',$like);
	}
    
    function set_sess_region(){
	
		$type = $_POST['region_name'];
		if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
		} else { $this->session->unset_userdata('sess_cat'); }
	
		$type = $_POST['field'];
		if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
		} else { $this->session->unset_userdata('sess_field'); }
	
	}
	
	function is_exist(){
		$country	  = $_POST['id_country'];
		$region       = $_POST['region_name'];
		$is_exist['1']= !$country ? "false" : "true";
		$is_exist['2']= !$region ? "false" : "true";		
		$msg['1']	  = !$country ? "country name required" : "";
		$msg['2']	  = !$region ? "region name required" : "";
		
		if ($is_exist['1']=='true' && $is_exist['2']=='true'){
			$where   = array('region_name'=>$region, 'id_country'=>$country,'status'=>'1');
			$checked = $this->crud->is_exist("","region","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$is_exist['2']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate country name" : "";
			$msg['2']	= $checked ? "duplicate region name" : "";
				
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2']);
	
		echo json_encode($result);
	}
	
	function form_create(){	
		$country = $this->crud->browse("","country","","","true","id,country_name");
		if(!$country) $country = array();
		$select = array(''=>'-- Select --');
		foreach($country as $val){ $options[$val->id] = $val->country_name; }
		$data['country'] = $select + $options;
	
		$this->load->view('region/form_create',$data);
	
	}
	
	function create(){
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array(
				'id' => null,
				'id_country' => $_POST ['id_country'],
				'region_name' => $_POST['region_name'],
				//'remark' => $_POST['remark'],
				'created_id' => $userID,
				'created_date' => $curr_date,
				'last_updated' => $curr_date
		);
// 		$select		= array('status'=>'0','region_name'=>$_POST['region_name']);
// 		$data1		= $this->crud->browse("","region","region_name",$_POST['region_name'],$select);
// 		if($data1 == null || $data == ''){
// 			$id = $this->crud->insert("","region",$data);
// 		} else {
// 			$select2  = array('status'=>'1','id_country'=>$_POST['id_country'],'region_name'=>$_POST['region_name'],
// 							  'remark'=>$_POST['remark'],'created_id'=>$userID,'created_date'=>$curr_date,'last_updated'=>$curr_date);
// 			$update = $this->crud->update("","region","id",$data1->id,$select2);
// 		}
		$id = $this->crud->insert("","region",$data);
		$this->session->set_flashdata('message','1 data success insert');
	}
	
	function form_update(){
		$id = $this->uri->segment(3);
		$select 			 = "id,(SELECT id FROM country WHERE id=id_country) as id_country,region_name,remark,created_id,created_date,last_updated";
		$def = $this->crud->browse("","region l","id",$id,"false",$select);
		$data['def'] = $def ;
		$country = $this->crud->browse("","country","","","true","id,country_name");
		//print_r($data['def']);
		if(!$country) $country = array();
		$select = array(''=>'-- Select --');
		foreach($country as $val){ $options[$val->id] = $val->country_name; }
		$data['country'] = $select + $options;
		$this->load->view('region/form_update',$data);
			
	}
	
	function update_(){		
		$curr_date 	= date('Y-m-d H:i:s');
		$id   = $this->uri->segment(3);
		$data = array('id_country'  =>$_POST['id_country'],
					  'region_name' =>$_POST['region_name'],
				 	  'remark' 		=>$_POST['remark'],
					  'last_updated'=> $curr_date);
		$select = "(SELECT id FROM country WHERE id=id_country) as id_country,region_name,remark";
		$check  = $this->crud->browse("","region","id",$id,"false",$select);
		//print_r($check);
		if (($check->id_country==$_POST['id_country']) && ($check->region==$_POST['region']) && ($check->remark==$_POST['remark'])){
			//echo "berhasil";exit;
			$this->crud->update("","region","id",$_POST['id'],$data);
			$this->session->set_flashdata('message','1 data success update');
		}

	}
	
	function update(){
		$curr_date 	= date('Y-m-d H:i:s');
		$id   = $this->uri->segment(3);
		$data = array('id_country'  =>$_POST['id_country'],
				'region_name' =>$_POST['region_name'],
				//'remark' 		=>$_POST['remark'],
				'last_updated'=> $curr_date);
		$this->crud->update("","region","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
	}
	
	function delete_(){
	
		$id = $this->uri->segment(3);
		$this->crud->delete("","region","id",$id);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('region/','refresh');
	}
	
	function delete(){
	
		$id = $this->uri->segment(3);
		
		$where = array('id_region'=>$id);
		$checked1 = $this->crud->is_exist("","offices","id",$where);
		$checked2 = $this->crud->is_exist("","vendor","id",$where);
		
		if ($checked1 || $checked2){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		} else {
			$this->crud->delete("","region","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
		
// 		$data = array('status'=>'0');
// 		$this->crud->update("","region","id",$id,$data);
		redirect('region/','refresh');
	}
	
}
?>