<script>
	$(function(){
		
	});
	function delete_ven(){
		var cek 	= confirm('Anda yakin ingin menghapus vendor ini?');
		if(cek)
		{
			$('#del-but').hide();
			var i;

			for (i=1; i < 10; i++) 
			{
				setTimeout(progbar, i*500, "bar"+i, "cek"+i);
			}
			var id    	= "<?php echo $this->uri->segment(3); ?>";
			var v_url 	= baseUrl+'vendordelete/delete_all/';
			$.post(v_url, {'id' : id}, function(data){
				if(data=="success"){
					setTimeout(progbar, 10*500, "bar10", "cek10");
					setTimeout(progbar, 11*500, "bar11", "cek11");
					setTimeout(function(){
						$('#complete-but').html("<span style='color: #0c0;font-size: 22px;font-weight: bold;'>Delete Complete</span>");
					},9500);
				}
			});
		}
		
	}
	function progbar(element,ceklist){
		var width 	= 1;
		var id 		= setInterval(frame, 30);
		// delete_ven();
		function frame() {
			if (width >= 100) {
				clearInterval(id);
			} else {
				width++;
				$('#'+element).css('width', width+'%');
				$('#'+element).html(width + '%');
				if(width==100){
					$('#'+ceklist).html("<i class='glyphicon glyphicon-ok' style='color: #2f2;font-size: 19px;'></i>");
					
				}
			}
		}

		

	}


</script>	
<div id="main_content">
	<div class="page-header">
		<h3>Delete Vendor Process</h3>
	</div>

	<table width="80%">
		<tbody>
			<tr>
				<td>Supplier Name</td>
				<td>:</td>
				<td><h1><?php echo $browse->vendor_name ?></h1></td>
			</tr>
			<tr>
				<td>Registration Code</td>
				<td>:</td>
				<td><h1><?php echo $browse->register_num ?></h1></td>
			</tr>
			<tr>
				<td>Supplier Number</td>
				<td>:</td>
				<td><h1><?php if(!empty($browse->vendor_num)){echo $browse->vendor_num;}else{echo "-";} ?></h1></td>
			</tr>
		</tbody>
	</table>
	<div class="col-md-8">
		<div class="pull-right" id="complete-but">
			
		</div>
	</div>
	
	<div class="col-md-3 pull-right">
		<a href="<?php echo base_url('vendorlist'); ?>" class="btn btn-warning pull-right">Back</a>
		<button id="del-but" type="button" class="btn btn-sm btn-primary pull-right" onclick="delete_ven()">Delete Now</button>
	</div>

	<table class="table table-bordered">
		<tbody>
			<tr>
				<td width="25%"><strong>1. Afiliasi</strong></td>
				<td width="70%">
					<div class="progress">
						<div id="bar1" class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
							0%
						</div>
					</div>
				</td>
				<td width="5%">
					<div align="center" id="cek1"></div>
				</td>
			</tr>
			<tr>
				<td width="25%"><strong>2. Referensi</strong></td>
				<td width="70%">
					<div class="progress">
						<div id="bar2" class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
							0%
						</div>
					</div>
				</td>
				<td width="5%">
					<div align="center" id="cek2"></div>
				</td>
			</tr>
			<tr>
				<td width="25%"><strong>3. Surat Legalitas</strong></td>
				<td width="70%">
					<div class="progress">
						<div id="bar3" class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
							0%
						</div>
					</div>
				</td>
				<td width="5%"><div align="center" id="cek3"></div></td>
			</tr>
			<tr>
				<td width="25%"><strong>4. Pengurus</strong></td>
				<td width="70%">
					<div class="progress">
						<div id="bar4" class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
							0%
						</div>
					</div>
				</td>
				<td width="5%"><div align="center" id="cek4"></div></td>
			</tr>
			<tr>
				<td width="25%"><strong>5. Owner</strong></td>
				<td width="70%">
					<div class="progress">
						<div id="bar5" class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
							0%
						</div>
					</div>
				</td>
				<td width="5%"><div align="center" id="cek5"></div></td>
			</tr>
			<tr>
				<td width="25%"><strong>6. Akta Pendirian</strong></td>
				<td width="70%">
					<div class="progress">
						<div id="bar6" class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
							0%
						</div>
					</div>
				</td>
				<td width="5%"><div align="center" id="cek6"></div></td>
			</tr>
			<tr>
				<td width="25%"><strong>7. Bank Account</strong></td>
				<td width="70%">
					<div class="progress">
						<div id="bar7" class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
							0%
						</div>
					</div>
				</td>
				<td width="5%">
					<div align="center" id="cek7"></div>
				</td>
			</tr>
			<tr>
				<td width="25%"><strong>8. Contact Person</strong></td>
				<td width="70%">
					<div class="progress">
						<div id="bar8" class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
							0%
						</div>
					</div>
				</td>
				<td width="5%">
					<div align="center" id="cek8"></div>
				</td>
			</tr>
			<tr>
				<td width="25%"><strong>9. Category</strong></td>
				<td width="70%">
					<div class="progress">
						<div id="bar9" class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
							0%
						</div>
					</div>
				</td>
				<td width="5%">
					<div align="center" id="cek9"></div>
				</td>
			</tr>
			<tr>
				<td width="25%"><strong>10. Transaction</strong></td>
				<td width="70%">
					<div class="progress">
						<div id="bar10" class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
							0%
						</div>
					</div>
				</td>
				<td width="5%">
					<div align="center" id="cek10"></div>
				</td>
			</tr>
			<tr>
				<td width="25%"><strong>11. General Information</strong></td>
				<td width="70%">
					<div class="progress">
						<div id="bar11" class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
							0%
						</div>
					</div>
				</td>
				<td width="5%">
					<div align="center" id="cek11"></div>
				</td>
			</tr>
		</tbody>
	</table>


</div>