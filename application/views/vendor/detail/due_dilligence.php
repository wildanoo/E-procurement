<div id="container15">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	
   
   $('#btn_create15').click(function(){  
   		$("#container15").load(baseUrl+'vendor/form_document');   		
   });  
   
   $('#btn_recmd').click(function(){  
   		$("#container15").load(baseUrl+'vendor/form_recommend');   		
   });  	
   
 });  
  
 function update_docdd(id){
 	
 	$("#container15").load(baseUrl+'vendor/form_update_duedill/'+id); 
 }
 
 function delete_docdd(id){
 	
 	var baseUrl  = "<?php echo base_url(); ?>";
 	var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';
    if (confirm('Are you sure you want to delete this?')) {
	 	$.post( baseUrl+ "vendor/delete_duedill", { csrf: token, id: id })
		  .done(function( resp ) {
		    	$("#container15").load(baseUrl+'vendor/load_due_dilligence');
		});
	}	
 }
  
 </script>
<?php if($this->permit->uvd){ ?> 

<button type="button" id="btn_create15" class="btn btn-info btn-primary">Add New</button> 
<button type="button" id="btn_recmd" class="btn btn-info btn-primary">Add Recommendation</button>

<?php } ?>
<div class = "panel panel-warning">
   <div class = "panel-heading">
      <h3 class = "panel-title">Recommendation</h3>
   </div>
   
   <div class = "panel-body">
     Recommend to <b><?php  echo strtoupper($recmd->type); ?></b> Vendor type . 
   </div>
</div>


<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Document Name</th>		
		<th>Remark</th>
		<th>Created Date</th>
		<?php if($this->permit->uvd){ ?><th>Action</th><?php } ?>
	</tr>
	</thead>
	<tbody>

 <?php   //$due_dill = array();
 if($due_dill){ $i=1;
 	foreach($due_dill as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->doc_name; ?></td>
			<td><?php echo $row->remark ?></td>
			<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
			<?php if($this->permit->uvd){ ?>
			<td style="text-align: center;">			
			<?php 			
					$icon = '<i class="glyphicon glyphicon-download"></i>';			
					$prop = array('title'=>'Download'); 				 
				  	echo anchor('vendor/download_duedill/'.$row->id,$icon,$prop);
		  	 
					echo '&nbsp;&nbsp;&nbsp;';	
					echo '<i class="fa fa-pencil" onclick="update_docdd('.$row->id.')"></i>'; echo '&nbsp;&nbsp;&nbsp;';					
					echo '<i class="fa fa-trash"  onclick="delete_docdd('.$row->id.')"></i>';	
			?></td>
			<?php } ?>											
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="8">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>