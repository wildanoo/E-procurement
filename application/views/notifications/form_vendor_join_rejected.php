<?php $this->load->view('notifications/header');?>

<br>Dengan hormat,<br>
<?php echo $data->vendor_name;?><br>
<br>
Pertama-tama kami mengucapkan terima kasih atas minat Bapak/Ibu telah mendaftar sebagai rekanan PT Garuda Indonesia (Persero) Tbk untuk kategori <?php echo $data->category;?><br>
<br> 
<br>
Berdasarkan evaluasi yang telah kami lakukan, bersama ini kami sampaikan bahwa Perusahaan Bapak/Ibu belum dapat kami evaluasi ke tahapan lebih lanjut.<br>
<br>
<br>
Sekali lagi kami sampaikan terima kasih atas partisipasi Perusahaan Bapak/Ibu, semoga kita dapat bekerja sama untuk  kesempatan mendatang.<br>
<br>

<?php $this->load->view('notifications/footer');?>