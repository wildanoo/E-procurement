<div id="container1">

<script type="text/javascript">
 $(function () {
   	var baseUrl  = "<?php echo base_url(); ?>";
   	$('#update1').hide();
  	
   	$('#edit1').click(function(){	
   	
   		$("#vendor_name").css("background","#ffffff");
	    $("#vendor_name").css("color","#827e85");             
	    $("#vendor_name").prop('readonly',false);
	    
	    $("#vendor_address").css("background","#ffffff");
	    $("#vendor_address").css("color","#827e85");             
	    $("#vendor_address").prop('readonly',false);
	    
	    $("#vendor_postcode").css("background","#ffffff");
	    $("#vendor_postcode").css("color","#827e85");             
	    $("#vendor_postcode").prop('readonly',false);
	    
	    $("#npwp").css("background","#ffffff");
	    $("#npwp").css("color","#827e85");             
	    $("#npwp").prop('readonly',false);
	    
	    $("#npwp_address").css("background","#ffffff");
	    $("#npwp_address").css("color","#827e85");             
	    $("#npwp_address").prop('readonly',false);
	    
	    $("#npwp_postcode").css("background","#ffffff");
	    $("#npwp_postcode").css("color","#827e85");             
	    $("#npwp_postcode").prop('readonly',false);
	    
	    $("#phone").css("background","#ffffff");
	    $("#phone").css("color","#827e85");             
	    $("#phone").prop('readonly',false);
	    
	    $("#fax").css("background","#ffffff");
	    $("#fax").css("color","#827e85");             
	    $("#fax").prop('readonly',false);
	    
	    $("#email").css("background","#ffffff");
	    $("#email").css("color","#827e85");             
	    $("#email").prop('readonly',false);
	    
	    $("#web").css("background","#ffffff");
	    $("#web").css("color","#827e85");             
	    $("#web").prop('readonly',false);   		
   	
   		$('#update1').show();
   		$('#edit1').hide();
   		
   	});
   	
   	$('#update1').click(function(){	  	
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
   	    var values = $("#form").serializeArray();
		values.push({name: csrf,value: token});
		values = jQuery.param(values); 
			$.ajax({
               url  : baseUrl + 'vendor/general_update',
               type : "POST",              		               
               data : values,
               success: function(resp){
               	  $("#container1").load(baseUrl+'vendor/load_general');
               }
            });
			
   		$('#update1').hide();
   		$('#edit1').show();
   	});   		
		 	   
 });   

 </script>

<div id="debug"></div>
<?php echo form_open('#',array('id'=>'form')); ?>
<table width="100%">
<?php if($this->session->userdata("feature")!="register") {  ?>
<tr>
<td><label>Vendor type</label></td><td>:</td>
<td><?php echo "<b>".ucfirst($vendor->type)."</b>"; ?></td>
<?php } ?>
</tr>
<tr>
<td><label>Name </label></td><td>:</td>
<td><?php $vendor_name = array( 'name' => "vendor_name" ,
								'id'   => "vendor_name", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> ucfirst($vendor->name)); 
	 echo form_input($vendor_name);	?>
</td>
</tr>
<tr>
<td><label>Address</label></td><td>:</td>
<td><?php $vendor_address = array( 'name' => "vendor_address" ,
							'id'   => "vendor_address", 
							'class'=> "form-control", 
							'readonly'=> "true", 
							'style'=> "width: 400px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> ucwords($vendor->address)); 
	 echo form_input($vendor_address);	?>
</td>
</tr>
<tr>
<td><label>Post Code</label></td><td>:</td>
<td><?php $vendor_postcode = array( 'name' => "vendor_postcode" ,
							'id'   => "vendor_postcode", 
							'class'=> "form-control",
							'readonly'=> "true",  
							'style'=> "width: 100px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> $vendor->postcode); 
	 echo form_input($vendor_postcode);	?>	
</td>
</tr>
<tr>
<td><label>NPWP</label></td><td>:</td>
<td>	<?php $npwp = array( 'name' => "npwp" ,
							 'id'   => "npwp", 
							 'class'=> "form-control", 'readonly'=> "true", 
							 'style'=> "width: 200px; height: 27px; background-color: #e9e9e9 !important;", 
							 'value'=> strtoupper($vendor->npwp)); 
	 echo form_input($npwp);	?>	
</td>
</tr>
<tr>
<td><label>NPWP  Address</label></td><td>:</td>
<td><?php $npwp_address = array( 'name' => "npwp_address" ,
							'id'   => "npwp_address", 
							'class'=> "form-control", 'readonly'=> "true", 
							'style'=> "width: 400px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> ucwords($vendor->npwp_address)); 
	 echo form_input($npwp_address);	?>	
</td>
</tr>
<tr>
<td><label>NPWP  Post Code</label></td><td>:</td>
<td><?php $npwp_postcode = array( 'name' => "npwp_postcode" ,
							'id'   => "npwp_postcode", 
							'class'=> "form-control", 'readonly'=> "true", 
							'style'=> "width: 100px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> $vendor->npwp_postcode); 
	 echo form_input($npwp_postcode);	?>	
</td>
</tr>
<tr>
<td><label>Phone</label></td><td>:</td>
<td><?php $phone = array( 'name' => "phone" ,
							'id'   => "phone", 
							'class'=> "form-control", 'readonly'=> "true", 
							'style'=> "width: 150px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> $vendor->phone); 
	 echo form_input($phone);	?>	
</td>
</tr>
<tr>
<td><label>Fax</label></td><td>:</td>
<td><?php $fax  = array( 'name' => "fax" ,
						  'id'   => "fax", 
						  'class'=> "form-control", 'readonly'=> "true", 
						  'style'=> "width: 150px; height: 27px; background-color: #e9e9e9 !important;", 
						  'value'=> $vendor->fax); 
	 echo form_input($fax);	?>
</td>
</tr>
<tr>
<td><label>Email</label></td><td>:</td>
<td><?php $email  = array('name' => "email" ,
						  'id'   => "email", 
						  'class'=> "form-control", 'readonly'=> "true", 
						  'style'=> "width: 200px; height: 27px; background-color: #e9e9e9 !important;", 
						  'value'=> $vendor->email); 
	 echo form_input($email);	?>	
</td>
</tr>
<tr>
<td><label>Web</label></td><td>:</td>
<td><?php $web    = array('name' => "web" ,
						  'id'   => "web", 
						  'class'=> "form-control", 'readonly'=> "true", 
						  'style'=> "width: 200px; height: 27px; background-color: #e9e9e9 !important;", 
						  'value'=> $vendor->web); 
	 echo form_input($web);	?>		
</td>
</tr>
</table>
<div style="text-align: right;">
<?php  if($this->global->active){ ?>	
<button type="button" id="edit1" class="btn btn-info btn-primary">Edit</button> 
<button type="button" id="update1" class="btn btn-info btn-primary">Update</button>    
<?php } ?>
</div>
<?php echo  form_close(); ?>

<br/>
<?php $this->load->view('vendor/register/properties'); ?>
</div>

