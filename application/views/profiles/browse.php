<div class="row">
  <div class = "col-md-12">
    <div class="box box-info">
      <div class="box-header with-border">
          <h3 class="box-title">Account Information</h3>
      </div><!-- /.box-header -->

<!-- form start -->
<!--form class="form-horizontal"-->
<div id="debug"></div>

					<?php echo form_open_multipart('profiles/change_picture',array('id'=>'form1','class'=>'form-horizontal'));  ?> 
                      <div class="box-body">
                        <br>
                        <div class = "col-md-4">
                          <div class = "form-group">
                            <div class="col-md-12 text-center">
                            <?php
                            	$userID  = $this->tank_auth->get_user_id();
                            	$images    = array('jpg','gif','png'); $imgfile = "false";
						 		foreach($images as $imgext){
						 			$imgpath  = "./uploads/images/".$userID.".".$imgext;
									if(file_exists($imgpath)){	
									$imgfile = $userID.".".$imgext; 	break;	}
								}                            
                                if($imgfile=="false") $path = base_url()."assets/images/avatar.png"; 
                            	else $path = base_url()."uploads/images/".$imgfile;               	
                            
                            ?>
                              <img class = "img-thumbnail imgs" src = "<?php echo $path; ?>"/>
                            </div>
                            <div class = "col-md-12 text-center">
                              <br>
                              <?php	
								$style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";
								$data  = array('type'=>'file','name'=> 'userfile','id' => 'file_upload','value'=>'true','content'=>'Import','style'=>$style); 
								echo form_input($data); ?>
                              <button type="submit" class="btn btn-info btn-danger">Change Picture</button>
                            </div>
                          </div>
                        </div>
                    <?php echo form_close(); ?> 

<?php echo form_open('#',array('id'=>'form_profiles','class'=>'form-horizontal'));  ?> 
<?php echo form_hidden('id',$profiles->id); ?>
<table width="100%">
<tr>
	<td><label class="col-md-3 control-label">Username</label></td><td>:</td>
	<td><input type="text" value="<?php  echo $def->username; ?>" class="form-control" style="width: 250px; height: 27px; background-color: #e9e9e9 !important;" disabled/></td>
</tr>
<tr>
	<td><label class="col-md-3 control-label">Employee Number</label></td><td>:</td>
	<td><input type="text" value="<?php  echo $profiles->nopeg; ?>" class="form-control" style="width: 250px; height: 27px; background-color: #e9e9e9 !important;" disabled/></td>
</tr>  
<tr>
	<td><label class="col-md-3 control-label">Employee Name</label></td><td>:</td>
	<td><input type="text" value="<?php  echo $profiles->fullname; ?>" class="form-control" style="width: 250px; height: 27px; background-color: #e9e9e9 !important;" disabled/></td>
</tr>    
<tr>
	<td><label class="col-md-3 control-label">Department </label></td><td>:</td>
	<td><input type="text" value="<?php  echo $profiles->id_dept; ?>" class="form-control" style="width: 250px; height: 27px; background-color: #e9e9e9 !important;" disabled/></td>
</tr>
<tr>
	<td><label class="col-md-3 control-label">Position </label></td><td>:</td>
	<td><input type="text" value="<?php  echo $profiles->id_position; ?>" class="form-control" style="width: 250px; height: 27px; background-color: #e9e9e9 !important;" disabled/></td>
</tr>
<tr>
	<td><label class="col-md-3 control-label">Group </label></td><td>:</td>
	<td><input type="text" value="<?php  echo $group->group_id; ?>" class="form-control" style="width: 250px; height: 27px; background-color: #e9e9e9 !important;" disabled/></td>
</tr>
<tr>
	<td><label class="col-md-3 control-label">Email </label></td><td>:</td>
	<td><input type="text" value="<?php  echo $def->email; ?>" class="form-control" style="width: 250px; height: 27px; background-color: #e9e9e9 !important;" disabled/></td>
</tr> 
<tr>
	<td><label class="col-md-3 control-label">Last IP </label></td><td>:</td>
	<td><input type="text" value="<?php  echo $def->last_ip; ?>" class="form-control" style="width: 250px; height: 27px; background-color: #e9e9e9 !important;" disabled/></td>
</tr>  
<br>
</table>
<?php echo form_close(); ?>
 <br>
</div>       
</div>
</div>
</div>  