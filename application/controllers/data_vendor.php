<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_vendor extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('m_datavendor','m_vendor','m_user','m_change_request'));
	$this->load->library('Ajax_pagination');
    islogged_in();
    $this->permit = new stdClass();
    $this->permit = $this->crud->get_permissions("10");

  }

  public function index()
  {
  	
	if ($this->m_datavendor->authenticate())
	{
		$user_id = $this->tank_auth->get_user_id();
		$is_vendor = $this->m_datavendor->is_vendor($user_id);
		if($is_vendor)
		{
			$this->session->set_userdata('is_avl', true);
			redirect('data_avl');
			break;
		}
		$this->kill_sess_sort();
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$table 			= "vendor t1";
		$select 		= "t1.id,register_num,vendor_num,vendor_name,t1.created_date,(SELECT category FROM category WHERE id=t2.id_category) as category,(SELECT subcategory FROM subcategory WHERE id=t2.id_subcat) as subcategory,vendor_type,t1.`status`,t2.id_subcat";
		$order 			= array('field'=>'t1.last_updated','order'=>'DESC');
		$where 			= "vendor_type = 'avl' ";
		$groupby 	 	= "t1.id";

		$joins[0][0]	= 'vendor_category t2';
		$joins[0][1]	= 't1.id=t2.id_vendor';
		$joins[0][2]	= 'left';
		$joins[1][0]	= 'category t3';
		$joins[1][1]	= 't2.id_category=t3.id';
		$joins[1][2]	= 'left';
		$joins[2][0]	= 'subcategory t4';
		$joins[2][1]	= 't4.id=t2.id_subcat';
		$joins[2][2]	= 'left';

		$page 			= $this->uri->segment(3);
		$per_page	 	= 10;
		$offset 		= $this->crud->set_offset($page,$per_page);
		$total_rows		= count($this->crud->browse_join("",$table,"","","true",$select,$joins,$where,"",$groupby));
		$set_config 	= array('base_url'=>base_url().'data_vendor/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>'3');
		$config     	= $this->crud->set_config($set_config);

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();

		$data['pagination'] = $paging;
		$data['num']			  = $offset;

		$selectsub 			  	= 'a.id as idsub,id_cat,subcategory,a.`status`,b.id,category';
		$tablecat			    	= 'subcategory a';
		$joinsub[0][0]			= 'category b';
		$joinsub[0][1]			= 'a.id_cat=b.id';
		$joinsub[0][2]			= 'left';

		$data['subcat']			= $this->crud->browse_join("",$tablecat,"","","",$selectsub,$joinsub);

		$browse 				= $this->crud->browse_join_with_paging("",$table,"","","true",$select,$joins,$where,$order,$per_page,$offset,$groupby);

		if(!empty($browse))
		{
			
			foreach($browse as $row){$id2 = $row->id;}
			$where2 				= "vendor_type != 'new' AND t1.id = $id2";
			$browse2 				= $this->crud->browse_join("",$table,"","","true",$select,$joins,$where2);
		}

		// foreach($browse as $row){$id2 = $row->id;}
		// $where2 				= "vendor_type != 'new' AND t1.id = $id2";
		// $browse2 				= $this->crud->browse_join("",$table,"","","true",$select,$joins,$where2);

		$detail = array();
		foreach($browse as $row)
    	{
			$category = $this->get_category($row->id);
			$detail[$row->id] = $category;
		}

		$data['browsecat']		= $browse2;
		$data['browse']			= $browse;
		$data['detail']			= $detail;
		$data['view']			= "data_vendor/home";

		if($this->input->is_ajax_request())
    	{
			$this->load->view('data_vendor/list_data',$data);
		}else
    	{
			$this->load->view('layout/template',$data);
		}
	}
	else
	{
		$this->session->set_flashdata('message','user not authorized');
		redirect('/auth/login/');
	}
		

  }

  function kill_sess_sort()
  {
		$search_val = array(
			'id'=>'',
			'search_term'=>''
			);
		$this->session->set_userdata($search_val);
	}

  function sorting(){

		$subcat_select 		= ($this->input->post('subcat')) ? $this->input->post('subcat') : $this->session->userdata('id');
		$search_term 		= ($this->input->post('search_term')) ? $this->input->post('search_term') : $this->session->userdata('search_term');
		$status_select 		= ($this->input->post('status')) ? $this->input->post('status') : $this->session->userdata('status_select');
		$search_val 		= array('id'=>$subcat_select,'search_term'=>$search_term,'status_select'=>$status_select);

		$this->session->set_userdata($search_val);

		$table 			= "vendor t1";
		$select 		= "t1.id,register_num,vendor_num,vendor_name,t1.created_date,(SELECT category FROM category WHERE id=t2.id_category) as category,(SELECT subcategory FROM subcategory WHERE id=t2.id_subcat) as subcategory,vendor_type,t1.`status`,t2.id_subcat";
		$order 			= array('field'=>'t1.last_updated','order'=>'DESC');

		$where 			= "vendor_type = 'avl' ";

		if($subcat_select=='all' &&  !empty($search_term))
		{
			$where .= " AND t1.vendor_name LIKE '%$search_term%'";
		}
    if($subcat_select!='all' &&  !empty($search_term))
		{
			$where .= " AND t2.id_subcat = $subcat_select AND t1.vendor_name LIKE '%$search_term%'";
		}
    if($subcat_select!='all' && empty($search_term))
		{
			$where .= " AND t2.id_subcat = $subcat_select";
		}

		$groupby 	 	= "t1.id";

		$joins[0][0]	= "vendor_category t2";
		$joins[0][1]	= "t1.id=t2.id_vendor";
		$joins[0][2]	= "left";
		$joins[1][0]	= "category t3";
		$joins[1][1]	= "t2.id_category=t3.id";
		$joins[1][2]	= "left";
		$joins[2][0]	= "subcategory t4";
		$joins[2][1]	= "t4.id=t2.id_subcat";
		$joins[2][2]	= "left";

		$page 			= $this->uri->segment(3);
		$per_page	 	= 10;
		$offset 		= $this->crud->set_offset($page,$per_page);
		$total_rows		= count($this->crud->browse_join("",$table,"","","true",$select,$joins,$where,"",$groupby));

		$set_config 	= array('base_url'=>base_url().'vendor3/sorting','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>'3');
		$config     	= $this->crud->set_config($set_config);

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();

		$selectsub 				  = 'a.id as idsub,id_cat,subcategory,a.`status`,b.id,category';
		$tablecat				    = 'subcategory a';
		$joinsub[0][0]			= 'category b';
		$joinsub[0][1]			= 'a.id_cat=b.id';
		$joinsub[0][2]			= 'left';

		$browse 		= $this->crud->browse_join_with_paging("",$table,"","","true",$select,$joins,$where,$order,$per_page,$offset,$groupby);

		$data['subcat']			= $this->crud->browse_join("",$tablecat,"","","",$selectsub,$joinsub);

		$detail = array();
		foreach($browse as $row)
    	{
			$category_hover = $this->get_category($row->id);
			$detail[$row->id] = $category_hover;
		}

		$data['detail']			= $detail;
		$data['pagination'] 	= $paging;
		$data['num']			= $offset;
		$data['browse']			= $browse;
		$data['view']			= "data_vendor/home";

		if($this->input->is_ajax_request())
    	{
			$this->load->view('data_vendor/list_data',$data);
		}
	    else
	    {
			$this->load->view('layout/template',$data);
		}

	}

  function export_excel(){
    $id 			= $this->uri->segment(3);
    $check 			= !$id ? $this->input->post('ck_list') : $id ;

    $idsubcat		= $this->input->post('idsubcat');
    $filter			= !$id ? implode(',',$check) : $id;
    $filtersubcat 	= implode(',',$idsubcat);

    $table 			= "vendor t1";
    $select 		= "t1.id,register_num,vendor_num,vendor_name,t1.created_date,t1.last_updated,(SELECT category FROM category WHERE id=t2.id_category) as category,(SELECT subcategory FROM subcategory WHERE id=t2.id_subcat) as subcategory,vendor_type,t1.`status`,t2.id_subcat";
    $order 			= array('field'=>'t1.last_updated','order'=>'DESC');

    $joins[0][0]	= "vendor_category t2";
    $joins[0][1]	= "t1.id=t2.id_vendor";
    $joins[0][2]	= "left";
    $joins[1][0]	= "category t3";
    $joins[1][1]	= "t2.id_category=t3.id";
    $joins[1][2]	= "left";
    $joins[2][0]	= "subcategory t4";
    $joins[2][1]	= "t4.id=t2.id_subcat";
    $joins[2][2]	= "left";

    $where = "vendor_type = 'avl' AND t1.id IN($filter)";

    if(!empty($check))
    {
      $data['vendorlist'] = $this->crud->browse_join("",$table,"","","true",$select,$joins,$where,$order);
      $this->load->view('data_vendor/excel',$data);
    }
    else
    {
      redirect('data_vendor');
    }
  }

  function get_category($id)
  {
		$table 			= "vendor t1";
		$select 		= "t1.id,register_num,vendor_num,vendor_name,t1.created_date,(SELECT category FROM category WHERE id=t2.id_category) as category,(SELECT subcategory FROM subcategory WHERE id=t2.id_subcat) as subcategory,vendor_type,t1.`status`,t2.id_subcat";
		$joins[0][0]	= 'vendor_category t2';
		$joins[0][1]	= 't1.id=t2.id_vendor';
		$joins[0][2]	= 'left';
		$joins[1][0]	= 'category t3';
		$joins[1][1]	= 't2.id_category=t3.id';
		$joins[1][2]	= 'left';
		$joins[2][0]	= 'subcategory t4';
		$joins[2][1]	= 't4.id=t2.id_subcat';
		$joins[2][2]	= 'left';
		$where 		= "vendor_type != 'new' AND t2.id_vendor= $id";

		$browse 	= $this->crud->browse_join("",$table,"","","true",$select,$joins,$where);
		$num = 1;
		foreach($browse as $row)
    {
			$cat .= $num.'.'.$row->category.' - '.$row->subcategory.'<br>';
			$num++;
		}
		return $cat;
	}

  function getList()
  {
  	if($this->m_datavendor->authenticate())
  	{
	    $venID = $this->uri->segment(3);
	    $this->session->set_userdata("venID",$venID);
	    $data['user_id']  = $this->tank_auth->get_user_id();
	    $data['username'] = $this->tank_auth->get_username();
	    //data changed_data
	    $data['dc_vendor'] = $this->m_datavendor->get_changed_data($venID, 'vendor');
	    $data['dc_bank'] = $this->m_datavendor->get_changed_data($venID, 'bank_account');
	 	$data['dc_akta'] = $this->m_datavendor->get_changed_data($venID, 'akta');
	    
	    //main data
	    $data['bank']   = $this->m_datavendor->get_data_bank($venID);
	    $data['akta']   = $this->m_datavendor->get_data_akta($venID);
	    $data['owner']  = $this->crud->browse("","owner","id_vendor",$venID,"true");
	    $data['correspondence'] = $this->crud->browse("","correspondence","id_vendor",$venID,"true");
	    $data['org']   = $this->crud->browse("","organization","id_vendor",$venID,"true");
	    $data['ref']   = $this->crud->browse("","references","id_vendor",$venID,"true");
	    $data['aff']   = $this->crud->browse("","affiliates","id_vendor",$venID,"true");
	    $data['due_dill']   = $this->crud->browse("","due_dilligence","id_vendor",$venID,"true");
	    $data['categories'] = $this->m_datavendor->get_data_category($venID);
	    $data['vendor'] = $this->m_datavendor->get_data_vendor($venID);
	    $data['browse'] = $this->crud->browse("","contact_person","id_vendor",$venID,"true");

	    $reg_status = $this->crud->browse("","register_status","","","true","id,status");
	    foreach($reg_status as $val){	$status[$val->id] = ucwords($val->status);}
	    $data['status'] = $status;

	    $default_visit	= array('status'=>"0",'note'=>"none",'files'=> "none");
	    // $db_visit = $this->crud->browse("","vendor_visit_status","id_vendor",$venID,"false","status,note,files");
	    // $visit    = !$db_visit ? (object) $default_visit  : $db_visit;
	    // $data['visit'] = $visit;

	    $data['vendor_visit'] = $this->crud->browse("","vendor_visit","id_vendor",$venID,"true");

	    $select2 = "id,(SELECT remark FROM approval_level WHERE level=approval) as note_from,notes,status,created_date";
	    $data['notes'] = $this->crud->browse("","vendor_rejected","id_vendor",$venID,"true",$select2);

	    $data['view']	= "data_vendor/generate_tab";
	    $this->load->view('layout/template',$data);
  	}
  	else
  	{
  		redirect('home','refresh');
  	}

  }

  function general_update()
  {
    $user_id = $this->tank_auth->get_user_id();
  	$venID = $this->session->userdata("venID");
    $old = $this->m_datavendor->get_data_vendor2($venID);
  	$data_new = array('vendor_name'		=> $_POST['vendor_name'],
					'vendor_address'	        => $_POST['vendor_address'],
					'postcode'			  => $_POST['vendor_postcode'],
					'npwp'				    => $_POST['npwp'],
					'npwp_address'		=> $_POST['npwp_address'],
					'npwp_postcode'		=> $_POST['npwp_postcode'],
					'phone'				    => $_POST['phone'],
					'fax'				      => $_POST['fax'],
					'email'				    => $_POST['email'],
					'web'			        => $_POST['web'],
					'last_updated'	  => date('Y-m-d H:i:s'));
    $data_old = json_decode(json_encode($old), true);
    $remove = array('id', 'num','rejected','completed', 'reg_num','type','reg_sts','approval','id_stsreg');
    foreach ($remove as $value)
    {
      unset($data_old[$value]);
    }
    unset($data_new['last_updated']);

    $result=$this->comparison($data_new,$data_old);
    foreach ($result as $key => $value)
    {
      $this->crud->insert("",'vendor_changedata', array('id' =>null ,'id_vendor'=>$venID,'table'=>'vendor','fieldname'=>$key, 'before'=> $data_old[$key],'after'=> $data_new[$key],'status'=>'request','created_id'=>$user_id,'created_date'=>date('Y-m-d H:i:s'),'last_updated'=>date('Y-m-d H:i:s') ) );
    }


    if(!empty($result))
    {
    	$this->send_notification();
    }

  }

  function bank_update(){
    $user_id = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
    $old = $this->m_datavendor->get_data_bank($venID);
 	$data_old = json_decode(json_encode($old), true);
 	$curr_date 	= date('Y-m-d H:i:s'); 
	$data_new 	= array('id_vendor'		=> $venID,
				'acc_number'	=> $_POST['acc_number'],
			 	'acc_name'		=> $_POST['acc_name'],
			 	'acc_address'	=> $_POST['acc_address'],
			 	'bank_name'		=> $_POST['bank_name'],
			 	'branch_name'	=> $_POST['branch_name'],
			 	'status'		=> '0',					 	
			 	'created_id'	=> $user_id,		
			 	'created_date'  => $curr_date,	
			 	'last_updated'	=> $curr_date);	
 	$remove = array('id_vendor', 'status','created_id','created_date', 'last_updated');
    foreach ($remove as $value)
    {
      unset($data_new[$value]);
    }

    $result=$this->comparison($data_new,$data_old);
    foreach ($result as $key => $value)
    {
      $this->crud->insert("",'vendor_changedata', array('id' =>null ,'id_vendor'=>$venID,'table'=>'bank_account','fieldname'=>$key, 'before'=> $data_old[$key],'after'=> $data_new[$key],'status'=>'request','created_id'=>$user_id,'created_date'=>date('Y-m-d H:i:s'),'last_updated'=>date('Y-m-d H:i:s') ) );
    }

    if(!empty($result))
    {
    	$this->send_notification();
    }		 	
		 	
 	
 }

 function akta_update(){
 	
 	$venID      = $this->session->userdata("venID");
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$user_id     = $this->tank_auth->get_user_id();
 	$old = $this->m_datavendor->get_data_akta($venID);
 	$data_old = json_decode(json_encode($old), true);
 	$data_new 		= array('id_vendor'		    => $venID,
 						'notaris_name'		=> $_POST['notaris_name'],
					 	'notaris_address'	=> $_POST['notaris_address'],
					 	'notaris_phone'		=> $_POST['notaris_phone'],
					 	'notaris_number'	=> $_POST['notaris_number'],
					 	'notaris_date'		=> $_POST['notaris_date'],
					 	'remark'			=> $_POST['remark'],					 	
					 	'created_id'		=> $user_id,		
					 	'created_date'  	=> $curr_date,							 					 	
					 	'last_updated'		=> $curr_date);	
	$remove = array('id_vendor', 'status','created_id','created_date', 'last_updated');
    foreach ($remove as $value)
    {
      unset($data_new[$value]);
    }	

    $result=$this->comparison($data_new,$data_old);
    foreach ($result as $key => $value)
    {
      $this->crud->insert("",'vendor_changedata', array('id' =>null ,'id_vendor'=>$venID,'table'=>'akta','fieldname'=>$key, 'before'=> $data_old[$key],'after'=> $data_new[$key],'status'=>'request','created_id'=>$user_id,'created_date'=>date('Y-m-d H:i:s'),'last_updated'=>date('Y-m-d H:i:s') ) );
    }

    if(!empty($result))
    {
    	$this->send_notification();
    }					 	
 	
 }

  function load_general()
  {
    $venID = $this->session->userdata("venID");
    $data['vendor'] = $this->m_datavendor->get_data_vendor($venID);
    $data['dc_vendor'] = $this->m_datavendor->get_changed_data($venID, 'vendor');
    $this->load->view('data_vendor/tab/general',$data);
  }

   function load_bank_account()
   { 	
 		$venID   = $this->session->userdata("venID");	
		$data['bank'] = $this->m_datavendor->get_data_bank($venID);	
	    $data['dc_bank'] = $this->m_datavendor->get_changed_data($venID, 'bank_account');

		$this->load->view('data_vendor/tab/bank_account',$data); 
  }

   function load_akta(){ 	
 		$venID   = $this->session->userdata("venID"); 
 		$data['akta'] = $this->m_datavendor->get_data_akta($venID);		
 		$data['dc_akta'] = $this->m_datavendor->get_changed_data($venID, 'akta');
	
		$this->load->view('data_vendor/tab/akta',$data); 
 }

  function comparison($new, $old)
  {
    function clearing($n)
    {
        return strtolower(preg_replace('/[^0-9a-zA-Z_]/',"",$n));
    }

    $a = array_map("clearing", $new);
    $b = array_map("clearing", $old);

    return $result = array_diff_assoc($a,$b);
  }

 

  function view_logs2()
  {
  	
  	$venID = $this->session->userdata('venID');
  	$tb_name = $this->input->post('tb_name')?$this->input->post('tb_name'):$this->session->userdata('tb_name');
  	
  	$this->session->set_userdata(array('tb_name'=>$tb_name));

  	if($tb_name == 'vendor')
  	{
  		$tab = 'home';
  	}elseif($tb_name == 'bank_account')
  	{
  		$tab = 'menu1';
  	}elseif($tb_name == 'akta')
  	{
  		$tab = 'menu2';
  	}
  	$this->session->set_userdata(array('tab'=>$tab));
  	

 	$page = $this->input->post('page');
	if(!$page){
	    $offset = 0;
	}else{
	    $offset = $page;
	}

	
	$totalRec = count($this->m_datavendor->getLogs2('',$venID, $tb_name));

	$config['target']      = '#logs-data';
	$config['base_url']    = site_url().'data_vendor/view_logs2';
	$config['total_rows']  = $totalRec;
	$config['per_page']    = 5;

	$this->ajax_pagination->initialize($config);

	$data['logs'] = $this->m_datavendor->getLogs2(array('start'=>$offset,'limit'=>5), $venID, $tb_name);
	$data['offset'] = $offset;
	$data['tb_name'] = $tb_name;
	$this->load->view('data_vendor/tab/logs', $data);

  }

  function view_log_cats()
  {
  	$venID = $this->session->userdata('venID');
  	$tb_name = 'vendor_category';
  	
  	$this->session->set_userdata(array('tb_name'=>$tb_name));

 	$page = $this->input->post('page');
	if(!$page){
	    $offset = 0;
	}else{
	    $offset = $page;
	}

	
	$totalRec = count($this->m_datavendor->getLogs2('',$venID, $tb_name));

	$config['target']      = '#logs-data';
	$config['base_url']    = site_url().'data_vendor/view_log_cats';
	$config['total_rows']  = $totalRec;
	$config['per_page']    = 10;

	$this->ajax_pagination->initialize($config);

	$data['logs'] = $this->m_datavendor->getLogs2(array('start'=>$offset,'limit'=>10), $venID, $tb_name);
	$data['offset'] = $offset;
	$data['tb_name'] = $tb_name;

	$data['logs_temp'] = $this->db->get_where('temp_vendor_category', array('id_vendor'=>$venID))->result();
	$this->load->view('data_vendor/tab/logs_cats', $data);
  }

   function view_log_cp()
  {
  	$venID = $this->session->userdata('venID');
  	$tb_name = 'contact_person';
  	
  	$this->session->set_userdata(array('tb_name'=>$tb_name));

 	$page = $this->input->post('page');
	if(!$page){
	    $offset = 0;
	}else{
	    $offset = $page;
	}

	
	$totalRec = count($this->m_datavendor->getLogs2('',$venID, $tb_name));

	$config['target']      = '#logs-data';
	$config['base_url']    = site_url().'data_vendor/view_logs_cp';
	$config['total_rows']  = $totalRec;
	$config['per_page']    = 10;

	$this->ajax_pagination->initialize($config);

	$data['logs'] = $this->m_datavendor->getLogs2(array('start'=>$offset,'limit'=>10), $venID, $tb_name);
	$data['offset'] = $offset;
	$data['tb_name'] = $tb_name;

	$data['logs_temp'] = $this->db->get_where('temp_contact_person', array('id_vendor'=>$venID))->result();
	$this->load->view('data_vendor/tab/logs_cp', $data);
  }

   function view_log_owner()
  {
  	$venID = $this->session->userdata('venID');
  	$tb_name = 'owner';
  	
  	$this->session->set_userdata(array('tb_name'=>$tb_name));

 	$page = $this->input->post('page');
	if(!$page){
	    $offset = 0;
	}else{
	    $offset = $page;
	}

	
	$totalRec = count($this->m_datavendor->getLogs2('',$venID, $tb_name));

	$config['target']      = '#logs-data';
	$config['base_url']    = site_url().'data_vendor/view_logs_owner';
	$config['total_rows']  = $totalRec;
	$config['per_page']    = 10;

	$this->ajax_pagination->initialize($config);

	$data['logs'] = $this->m_datavendor->getLogs2(array('start'=>$offset,'limit'=>10), $venID, $tb_name);
	$data['offset'] = $offset;
	$data['tb_name'] = $tb_name;

	$data['logs_temp'] = $this->db->get_where('temp_owner', array('id_vendor'=>$venID))->result();
	$this->load->view('data_vendor/tab/logs_owner', $data);
  }

   function view_log_org()
  {
  	$venID = $this->session->userdata('venID');
  	$tb_name = 'organization';
  	
  	$this->session->set_userdata(array('tb_name'=>$tb_name));

 	$page = $this->input->post('page');
	if(!$page){
	    $offset = 0;
	}else{
	    $offset = $page;
	}

	
	$totalRec = count($this->m_datavendor->getLogs2('',$venID, $tb_name));

	$config['target']      = '#logs-data';
	$config['base_url']    = site_url().'data_vendor/view_logs_org';
	$config['total_rows']  = $totalRec;
	$config['per_page']    = 10;

	$this->ajax_pagination->initialize($config);

	$data['logs'] = $this->m_datavendor->getLogs2(array('start'=>$offset,'limit'=>10), $venID, $tb_name);
	$data['offset'] = $offset;
	$data['tb_name'] = $tb_name;

	$data['logs_temp'] = $this->db->get_where('temp_organization', array('id_vendor'=>$venID))->result();
	$this->load->view('data_vendor/tab/logs_org', $data);
  }

    function view_log_corresp()
  {
  	$venID = $this->session->userdata('venID');
  	$tb_name = 'correspondence';
  	
  	$this->session->set_userdata(array('tb_name'=>$tb_name));

 	$page = $this->input->post('page');
	if(!$page){
	    $offset = 0;
	}else{
	    $offset = $page;
	}

	
	$totalRec = count($this->m_datavendor->getLogs2('',$venID, $tb_name));

	$config['target']      = '#logs-data';
	$config['base_url']    = site_url().'data_vendor/view_logs_corresp';
	$config['total_rows']  = $totalRec;
	$config['per_page']    = 10;

	$this->ajax_pagination->initialize($config);

	$data['logs'] = $this->m_datavendor->getLogs2(array('start'=>$offset,'limit'=>10), $venID, $tb_name);
	$data['offset'] = $offset;
	$data['tb_name'] = $tb_name;

	$data['logs_temp'] = $this->db->get_where('temp_correspondence', array('id_vendor'=>$venID))->result();
	$this->load->view('data_vendor/tab/logs_corresp', $data);
  }

   function view_log_ref()
  {
  	$venID = $this->session->userdata('venID');
  	$tb_name = 'references';
  	$this->session->set_userdata(array('tb_name'=>$tb_name));

 	$page = $this->input->post('page');
	if(!$page){
	    $offset = 0;
	}else{
	    $offset = $page;
	}

	
	$totalRec = count($this->m_datavendor->getLogs2('',$venID, $tb_name));

	$config['target']      = '#logs-data';
	$config['base_url']    = site_url().'data_vendor/view_logs_ref';
	$config['total_rows']  = $totalRec;
	$config['per_page']    = 10;

	$this->ajax_pagination->initialize($config);

	$data['logs'] = $this->m_datavendor->getLogs2(array('start'=>$offset,'limit'=>10), $venID, $tb_name);
	$data['offset'] = $offset;
	$data['tb_name'] = $tb_name;

	$data['logs_temp'] = $this->db->get_where('temp_references', array('id_vendor'=>$venID))->result();
	$this->load->view('data_vendor/tab/logs_ref', $data);
  }

   function view_log_aff()
  {
  	$venID = $this->session->userdata('venID');
  	$tb_name = 'affiliates';
  	$this->session->set_userdata(array('tb_name'=>$tb_name));

 	$page = $this->input->post('page');
	if(!$page){
	    $offset = 0;
	}else{
	    $offset = $page;
	}

	
	$totalRec = count($this->m_datavendor->getLogs2('',$venID, $tb_name));

	$config['target']      = '#logs-data';
	$config['base_url']    = site_url().'data_vendor/view_logs_aff';
	$config['total_rows']  = $totalRec;
	$config['per_page']    = 10;

	$this->ajax_pagination->initialize($config);

	$data['logs'] = $this->m_datavendor->getLogs2(array('start'=>$offset,'limit'=>10), $venID, $tb_name);
	$data['offset'] = $offset;
	$data['tb_name'] = $tb_name;

	$data['logs_temp'] = $this->db->get_where('temp_affiliates', array('id_vendor'=>$venID))->result();
	$this->load->view('data_vendor/tab/logs_aff', $data);
  }

	function form_update_category()
	{

	$id      = $this->uri->segment(3); 
	$select  = "id,id_category as id_cat,id_subcat,(SELECT subcategory FROM subcategory r WHERE r.id=l.id_subcat) as subcategory";	
	$default = $this->crud->browse("","vendor_category l","id",$id,"false",$select);

	$subcat  = array($default->id_subcat => $default->subcategory);

	$dbcat = $this->crud->browse("","category","","","true","id,category");
	foreach($dbcat as $val){ $category[$val->id] = $val->category; }

	$data['default']     =  $default;
	$data['category']    =  $category;
	$data['subcategory'] =  $subcat;

	$this->load->view('data_vendor/tab/categories/form_update',$data);
	//send notif
    $this->send_notification();

	}

	function load_categories(){

	$venID   = $this->session->userdata("venID");	
	$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
	(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";					
	$data['categories'] = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2);	

	$this->load->view('data_vendor/tab/categories',$data);

	}

	function form_categories($stat=null){

	$default  = array(''=>'--Select Category--');
	$dbcat = $this->crud->browse("","category","","","true","id,category");
	foreach($dbcat as $val){ $category[$val->id] = $val->category; }
	$data['category'] = $default + $category;
	$data['page'] = ($stat == 1) ? 'general' : 'categories';
	$data['container'] = ($stat == 1) ? 'container' : 'container14';
	$this->load->view('data_vendor/tab/categories/form_create',$data);

	}

	function is_exist_category(){

	$venID      = $this->session->userdata("venID");
	$id_cat     = $_POST['id_category'];
	$id_subcat  = $_POST['id_subcat'];

	$where    = array('id_vendor'=>$venID,'id_category'=>$id_cat,'id_subcat'=>$id_subcat);
	$is_exist = $this->crud->is_exist("","vendor_category","id",$where);
	$result   = (!$is_exist  && $id_subcat) ? "true" : "false"; 

	$array['status']  =  $result;	
	echo json_encode($array);

	}

	function get_subcategory(){
	$id_cat  = $_POST['id_cat']; $select = "id,subcategory as subcat"; 	
	$subcat  = $this->crud->browse("","subcategory","id_cat",$id_cat,"true",$select); 	
	if($subcat) { 	
	echo "<option value=''>-- Select Sub Category --</option>";	
	foreach ($subcat as $row){	echo "<option value='$row->id'>$row->subcat</option>"; }	
	} else { echo "<option value=''>-- Empty --</option>";	}
	}

	function create_category(){

	$curr_date 	= date('Y-m-d H:i:s'); 
	$userID     = $this->tank_auth->get_user_id();
	$venID      = $this->session->userdata("venID");
	$data 		= array('id'			=> null, 						
	'id_vendor'		=> $venID,
	'id_category'	=> $_POST['id_category'],
	'id_subcat'		=> $_POST['id_subcat'],					 	
	'status'		=> '0',
	'created_id'	=> $userID,
	'created_date'	=> $curr_date,
	'last_updated'	=> $curr_date,
	'approval'		=> 'add'
	);

	$this->crud->insert("","temp_vendor_category",$data); 
	//send notif
    $this->send_notification();

	}


	function delete_subcategory()
	{
	$id = $this->input->post('id');
	$q = "insert into temp_vendor_category (id_vendor,id_category,id_subcat,status,created_id,created_date,last_updated,approval)
	select id_vendor, id_category, id_subcat,'0',created_id,created_date,last_updated,'delete' from
	vendor_category where id = $id ";
	$this->db->query($q);
	print $this->db->last_query();

	//send notif
    $this->send_notification();
	

	}

	function update_category()
	{
		$id = $this->input->post('id');
		$venID = $this->session->userdata("venID");
		$data_old = $this->db->get_where('vendor_category', array('id'=>$id))->result_array();
		$user_id = $this->tank_auth->get_user_id();
		$curr_date = date('Y-m-d H:i:s'); 
		$userID = $this->tank_auth->get_user_id();
		$data_new  = array(	'id_category'	=> $_POST['id_category'],
							'id_subcat'  	=> $_POST['id_subcat'],					 						 						 	
							'last_updated' 	=> $curr_date);
		$remove = array('id', 'id_vendor', 'status','created_id', 'created_date' , 'last_updated');
		foreach ($remove as $value)
		{
		unset($data_old[0][$value]);
		}	
		
		unset($data_new['last_updated']);
		
		$result = $this->comparison($data_new,$data_old);
		foreach ($result as $key => $value)
		{
		$this->crud->insert("",'vendor_changedata', array('id' =>null ,'id_vendor'=>$venID,'table'=>'vendor_category', 'index'=>$id, 'fieldname'=>$key, 'before'=> $data_old[0][$key],'after'=> $data_new[$key],'status'=>'request','created_id'=>$user_id,'created_date'=>date('Y-m-d H:i:s'),'last_updated'=>date('Y-m-d H:i:s') ) );
		}

		//send notif
		if(!empty($result))
		{
    		$this->send_notification();
		}



	}  

	//end of function category

	function load_contact(){ 	
	 	$venID   = $this->session->userdata("venID");	
		$data['browse'] = $this->crud->browse("","contact_person","id_vendor",$venID,"true");	
		$this->load->view('data_vendor/tab/contact',$data); 
	 }

	function form_createContact()
	{
		$venID  = $this->session->userdata("venID");
		$data['vendor'] = $this->crud->browse("","vendor","id",$venID,"false","vendor_name as name");		
		$this->load->view('data_vendor/tab/contact/form_create',$data);
	}


	function form_updateContact(){
		$contactID       = $this->uri->segment(3);
		$venID           = $this->session->userdata("venID");
		
		$data['default'] = $this->crud->browse("","contact_person","id",$contactID,"false"); 	
		$data['vendor']  = $this->crud->browse("","vendor","id",$venID,"false","vendor_name as name");		
		$this->load->view('data_vendor/tab/contact/form_update',$data);
	}

	function add_contact(){	

	$curr_date  = date('Y-m-d H:i:s'); 
	$userID     = $this->tank_auth->get_user_id();
	$venID      = $this->session->userdata("venID");
	$data 		= array('id'		=> null, 						
				 	'id_vendor'		=> $venID,
				 	'fullname'		=> $_POST['name'],
				 	'position'		=> $_POST['position'],
				 	'mobile'		=> $_POST['mobile'],
				 	'email'			=> $_POST['email'],
				 	'status'		=> '0',
				 	'created_id'	=> $userID,
				 	'created_date'	=> $curr_date,
				 	'last_updated'	=> $curr_date,
				 	'approval'		=> 'add'
				 	);

	$this->crud->insert("","temp_contact_person",$data); 
	//send notif

	$this->send_notification();


				 	
	}

	function update_contact()
	{	
		$id = $this->input->post('id');
		$old = $this->db->get_where('contact_person', array('id'=>$id))->result_array();
		// $data_old = json_decode(json_encode($old), true);
		$data_old = $old[0];
		// print_r($data_old);
		// exit();
		$user_id = $this->tank_auth->get_user_id();
		$curr_date = date('Y-m-d H:i:s');
	
		$venID      = $this->session->userdata("venID");
		$data_new 	= array('fullname' => $_POST['name'],
				 	'position'		=> $_POST['position'],
				 	'mobile'		=> $_POST['mobile'],
				 	'email'			=> $_POST['email'],					 					 	
				 	'last_updated'	=> $curr_date);	

		$remove = array('id', 'id_vendor', 'status','created_id', 'created_date' , 'last_updated');
		foreach ($remove as $value)
		{
		unset($data_old[$value]);
		}	
		
		unset($data_new['last_updated']);
				 				
		$result = $this->comparison($data_new,$data_old);
		foreach ($result as $key => $value)
		{
			$this->crud->insert("",'vendor_changedata', array('id' =>null ,'id_vendor'=>$venID,'table'=>'contact_person', 'index'=>$id, 'fieldname'=>$key, 'before'=> $data_old[$key],'after'=> $data_new[$key],'status'=>'request','created_id'=>$user_id,'created_date'=>date('Y-m-d H:i:s'),'last_updated'=>date('Y-m-d H:i:s') ) );
		}

		//send notif
		if(!empty($result))
		{
    		$this->send_notification();
		}

				 	
	}

	function delete_contact()
	{ 
		$id = $this->input->post('id');
		$q = "insert into temp_contact_person (id_tbl,id_vendor,fullname,position,mobile,email,status,created_id,created_date,last_updated,approval)
		select id, id_vendor, fullname, position, mobile, email,'0',created_id,created_date,last_updated,'delete' from
		contact_person where id = $id ";
		$this->db->query($q);
		print $this->db->last_query();
	
		//send notif
		$this->send_notification();
		


	}

	//end of function contact


 function form_owner(){
 	
 	$this->load->view('data_vendor/tab/owner/form_create'); 
 }
 
 function load_owner(){
 	
 	$venID   = $this->session->userdata("venID"); 
	$data['owner'] = $this->crud->browse("","owner","id_vendor",$venID,"true");		
	$this->load->view('data_vendor/tab/owner',$data); 	
 	
 }
 
 function add_owner(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('id'			=> null, 						
					 	'id_vendor'		=> $venID,
					 	'owner_name'	=> $_POST['owner_name'],
					 	'owner_address'	=> $_POST['owner_address'],
					 	'owner_phone'	=> $_POST['owner_phone'],
					 	'owner_position'=> $_POST['owner_position'],
					 	'owner_shared'	=> $_POST['owner_shared'],
					 	'remark'		=> $_POST['remark'],
					 	'status'		=> '0',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date,
					 	'approval'		=> 'add'
					 	);

	$this->crud->insert("","temp_owner",$data); 
					 	
	//send notif
	
	$this->send_notification();
		
		
 	
 }

 function form_update_owner(){
 	
 	$prsnID  = $this->uri->segment(3);
 	$venID   = $this->session->userdata("venID"); 	
 	$data['default'] = $this->crud->browse("","owner","id",$prsnID,"false"); 	 	

 	$this->load->view('data_vendor/tab/owner/form_update',$data);
 	
 }
 
 function update_owner(){
 	
	$id = $this->input->post('id');
	$old = $this->db->get_where('owner', array('id'=>$id))->result_array();
	$data_old = $old[0];
	$user_id = $this->tank_auth->get_user_id();
	$curr_date = date('Y-m-d H:i:s');
	
	$venID      = $this->session->userdata("venID");
	$data_new 		= array('owner_name'	=> $_POST['owner_name'],
	'owner_address'	=> $_POST['owner_address'],
	'owner_phone'	=> $_POST['owner_phone'],
	'owner_position'=> $_POST['owner_position'],
	'owner_shared'	=> $_POST['owner_shared'],
	'remark'		=> $_POST['remark'],						 							 					 	
	'last_updated'	=> $curr_date);

		

	$remove = array('id', 'id_vendor', 'status','created_id', 'created_date' , 'last_updated');
	foreach ($remove as $value)
	{
	unset($data_old[$value]);
	}	

	unset($data_new['last_updated']);
				 				
	$result = $this->comparison($data_new,$data_old);

	foreach ($result as $key => $value)
	{
		$this->crud->insert("",'vendor_changedata', array('id' =>null ,'id_vendor'=>$venID,'table'=>'owner', 'index'=>$id, 'fieldname'=>$key, 'before'=> $data_old[$key],'after'=> $data_new[$key],'status'=>'request','created_id'=>$user_id,'created_date'=>date('Y-m-d H:i:s'),'last_updated'=>date('Y-m-d H:i:s') ) );
	}

		//send notif
	if(!empty($result))
	{
		$this->send_notification();
	}

			
 	
 }

 function delete_owner(){

	$id = $this->input->post('id');
	$q = "insert into temp_owner (id_tbl,id_vendor,owner_name,owner_address,owner_phone,owner_position,owner_shared,remark,status,created_id,created_date,last_updated,approval)
	select id, id_vendor,owner_name,owner_address,owner_phone,owner_position,owner_shared,remark,'0',created_id,created_date,last_updated,'delete' from
	owner where id = $id ";
	$this->db->query($q);
	print $this->db->last_query();

	//send notif
	$this->send_notification();
		

 	
 }

 //end of function owner


 function form_org(){
 	
 	$this->load->view('data_vendor/tab/org/form_create'); 
 }
 
 function load_org(){ 
 	
 	$venID   = $this->session->userdata("venID");	
	$data['org'] = $this->crud->browse("","organization","id_vendor",$venID,"true");		
	$this->load->view('data_vendor/tab/org',$data);  	
 }
 
 function add_person_org(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('id'			=> null, 						
					 	'id_vendor'		=> $venID,
					 	'name'			=> $_POST['prsn_name'],
					 	'address'		=> $_POST['prsn_addrss'],
					 	'phone'			=> $_POST['prsn_phone'],
					 	'position'		=> $_POST['prsn_post'],
					 	'remark'		=> $_POST['remark3'],
					 	'status'		=> '0',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date,
					 	'approval'		=>'add');
				 	
	$this->crud->insert("","temp_organization",$data);

	//send notif
	$this->send_notification();
		
 		
 }

 function form_update_org(){
 	
 	$prsnID  = $this->uri->segment(3);
 	$venID      = $this->session->userdata("venID");
 	
 	$data['default'] = $this->crud->browse("","organization","id",$prsnID,"false"); 	 	
 	$data['vendor'] = $this->crud->browse("","vendor","id",$venID,"false","vendor_name as name");		
 	$this->load->view('data_vendor/tab/org/form_update',$data);
 	
 }
 
 function update_person_org(){
 	
	$id = $this->input->post('id');
	$old = $this->db->get_where('organization', array('id'=>$id))->result_array();
	$data_old = $old[0];
	$user_id = $this->tank_auth->get_user_id();
	$curr_date = date('Y-m-d H:i:s');
	
	$venID      = $this->session->userdata("venID");
	$data_new 		= array('name'		    => $_POST['prsn_name'],
 						'address'		=> $_POST['prsn_addrs'],
 						'phone'		    => $_POST['prsn_phone'],
					 	'position'		=> $_POST['prsn_post'],
					 	'remark'		=> $_POST['remark'],				 	
					 	'last_updated'	=> $curr_date);

		

	$remove = array('id', 'id_vendor', 'status','created_id', 'created_date' , 'last_updated');
	foreach ($remove as $value)
	{
	unset($data_old[$value]);
	}	

	unset($data_new['last_updated']);
		
	// print_r($data_old);
	// print_r($data_new);

	$result = $this->comparison($data_new,$data_old);
	// print_r($result);
	foreach ($result as $key => $value)
	{
		$this->crud->insert("",'vendor_changedata', array('id' =>null ,'id_vendor'=>$venID,'table'=>'organization', 'index'=>$id, 'fieldname'=>$key, 'before'=> $data_old[$key],'after'=> $data_new[$key],'status'=>'request','created_id'=>$user_id,'created_date'=>date('Y-m-d H:i:s'),'last_updated'=>date('Y-m-d H:i:s') ) );
	}

		//send notif
	if(!empty($result))
	{
		$this->send_notification();
	}

	
 }

 function delete_person(){ 
 	$id = $this->input->post('id');
	$q = "insert into temp_organization (id_tbl,id_vendor,name,address,phone,position,remark,status,created_id,created_date,last_updated,approval)
	select id, id_vendor,name,address,phone,position,remark,'0',created_id,created_date,last_updated,'delete' from
	organization where id = $id ";
	$this->db->query($q);
	print $this->db->last_query();

	//send notif
	$this->send_notification();
	
 }

//end of function org

  function form_ref(){
 	
 	$this->load->view('data_vendor/tab/ref/form_create');
 	 
 }
 
 function load_ref(){ 
 	
 	$venID   = $this->session->userdata("venID");	
	$data['ref'] = $this->crud->browse("","references","id_vendor",$venID,"true");
	$this->load->view('data_vendor/tab/reference',$data);  	
	
 }

 function add_reference(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('id'			=> null, 						
					 	'id_vendor'		=> $venID,
					 	'cust_name'		=> $_POST['cust_name'],
					 	'project'		=> $_POST['project'],
					 	'point'			=> $_POST['point'],
					 	'date'			=> $_POST['pjdate'],
					 	'remark'		=> $_POST['remark'],
					 	'status'		=> '0',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date,
					 	'approval'		=> 'add');
				 	
	$this->crud->insert("","temp_references",$data); 

	//send notif
	$this->send_notification();
		

	
 }

 function form_update_ref(){
 	
 	$refID     = $this->uri->segment(3); 	
 	$data['default'] = $this->crud->browse("","references","id",$refID,"false"); 
 	$this->load->view('data_vendor/tab/ref/form_update',$data);
 }
 
 function update_reference(){
 	
	$id = $this->input->post('id');
	$old = $this->db->get_where('references', array('id'=>$id))->result_array();
	$data_old = $old[0];
	$user_id = $this->tank_auth->get_user_id();
	$curr_date = date('Y-m-d H:i:s');
	
	$venID      = $this->session->userdata("venID");
	$data_new 		= array('cust_name'		=> $_POST['cust_name'],
					 	'project'		=> $_POST['project'],
					 	'point'			=> $_POST['point'],
					 	'date'			=> $_POST['pjdate'],
					 	'remark'		=> $_POST['remark'],					 	
					 	'last_updated'	=> $curr_date);

		

	$remove = array('id', 'id_vendor', 'status','created_id', 'created_date' , 'last_updated');
	foreach ($remove as $value)
	{
	unset($data_old[$value]);
	}	

	unset($data_new['last_updated']);
		
	// print_r($data_old);
	// print_r($data_new);

	$result = $this->comparison($data_new,$data_old);
	// print_r($result);
	foreach ($result as $key => $value)
	{
		$this->crud->insert("",'vendor_changedata', array('id' =>null ,'id_vendor'=>$venID,'table'=>'references', 'index'=>$id, 'fieldname'=>$key, 'before'=> $data_old[$key],'after'=> $data_new[$key],'status'=>'request','created_id'=>$user_id,'created_date'=>date('Y-m-d H:i:s'),'last_updated'=>date('Y-m-d H:i:s') ) );
	}

		//send notif
	if(!empty($result))
	{
		$this->send_notification();
	}

 }
 
 function delete_ref(){ 
 	$id = $this->input->post('id');
	$q = "insert into temp_references (id_tbl,id_vendor,cust_name,project,point,date,remark,status,created_id,created_date,last_updated,approval)
	select id, id_vendor,cust_name,project,point,date,remark,'0',created_id,created_date,last_updated,'delete' from
	`references` where id = $id ";
	$this->db->query($q);
	print $this->db->last_query();

	//send notif
	$this->send_notification();
	
 }

 //end of function reference

 function form_aff(){
 	
 	$this->load->view('data_vendor/tab/aff/form_create');
 	 
 }
 
 function load_aff(){  	
 	$venID   = $this->session->userdata("venID");	
	$data['aff'] = $this->crud->browse("","affiliates","id_vendor",$venID,"true");		
	$this->load->view('data_vendor/tab/affiliate',$data);  	
 }
 
 function add_affiliate(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('id'			=> null, 						
					 	'id_vendor'		=> $venID,
					 	'company_name'	=> $_POST['company_name'],
					 	'competency'	=> $_POST['competency'],
					 	'contact'		=> $_POST['contact'],					 	
					 	'remark'		=> $_POST['remark'],
					 	'status'		=> '0',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date,
					 	'approval'		=> 'add');

	$this->crud->insert("","temp_affiliates",$data); 
	
	//send notif
	$this->send_notification();
		

 	
 }

 function form_update_aff(){
 	
 	$affID  = $this->uri->segment(3); 	
 	$data['default'] = $this->crud->browse("","affiliates","id",$affID,"false"); 
 	$this->load->view('data_vendor/tab/aff/form_update',$data); 	
 }
 
 function update_affiliate(){

	$id = $this->input->post('id');
	$old = $this->db->get_where('affiliates', array('id'=>$id))->result_array();
	$data_old = $old[0];
	$user_id = $this->tank_auth->get_user_id();
	$curr_date = date('Y-m-d H:i:s');
	
	$venID      = $this->session->userdata("venID");
	$data_new 		= array('company_name'	=> $_POST['company_name'],
					 	'competency'	=> $_POST['competency'],
					 	'contact'		=> $_POST['contact'],					 	
					 	'remark'		=> $_POST['remark'],				 	
					 	'last_updated'	=> $curr_date);

	$remove = array('id', 'id_vendor', 'status','created_id', 'created_date' , 'last_updated');
	foreach ($remove as $value)
	{
	unset($data_old[$value]);
	}	

	unset($data_new['last_updated']);
		
	// print_r($data_old);
	// print_r($data_new);

	$result = $this->comparison($data_new,$data_old);
	// print_r($result);
	foreach ($result as $key => $value)
	{
		$this->crud->insert("",'vendor_changedata', array('id' =>null ,'id_vendor'=>$venID,'table'=>'affiliates', 'index'=>$id, 'fieldname'=>$key, 'before'=> $data_old[$key],'after'=> $data_new[$key],'status'=>'request','created_id'=>$user_id,'created_date'=>date('Y-m-d H:i:s'),'last_updated'=>date('Y-m-d H:i:s') ) );
	}

		//send notif
	if(!empty($result))
	{
		$this->send_notification();
	}

 	
 }
 
 function delete_aff(){
 	$id = $this->input->post('id');
	$q = "insert into temp_affiliates (id_tbl,id_vendor,company_name,competency,contact,remark,status,created_id,created_date,last_updated,approval)
	select id, id_vendor,company_name,competency,contact,remark,'0',created_id,created_date,last_updated,'delete' from
	`affiliates` where id = $id ";
	$this->db->query($q);
	print $this->db->last_query();
	
	//send notif
	$this->send_notification();
	
 	
 }

//end of function affiliate

 function form_correspondence(){  	
 	$this->load->view('data_vendor/tab/correspondence/form_create'); 	
 } 
 
 function form_update_correspondence(){ 
  	
  	$id = $this->uri->segment(3);
  	$data['default'] = $this->crud->browse("","correspondence","id",$id,"false");	  	
  	
 	$this->load->view('data_vendor/tab/correspondence/form_update',$data); 	
 } 
 
 function add_correspondence(){ 	
 	
 	$venID      = $this->session->userdata("venID");
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$file_type  = "-";
 	
 	if($_FILES){
	$file_type = $this->m_datavendor->get_file_type($_FILES); }
    
	$insert = array( 	'id_vendor' 	=> $venID,
						'doc_name'  	=> $_POST['doc_name'],
						'doc_num'   	=> $_POST['doc_num'],
						'desc'     	 	=> $_POST['desc'],
						'eff_date'  	=> $_POST['eff_date'],
						'exp_date'  	=> $_POST['exp_date'],
						'remark'    	=> $_POST['remark'],
						'filetype'  	=> $file_type,
						'status'    	=> "0",
						'created_id'   	=> $userID,
						'created_date' 	=> $curr_date,
						'last_updated' 	=> $curr_date,
						'approval'		=> 'add');
						
	 $where1    = array('doc_num'=>$_POST['doc_num'],'id_vendor' => $venID);
	 $where2    = array('doc_num'=>$_POST['doc_name'],'id_vendor' => $venID);
	 	 
 	 $is_exist1 = $this->crud->is_exist("","correspondence","id",$where1);	 
 	 $is_exist2 = $this->crud->is_exist("","correspondence","id",$where2);  
     $docID     = $this->crud->insert("","temp_correspondence",$insert);	
 	
 	if($_FILES && $docID && !$is_exist1 && !$is_exist2){ 	
 	     	    
 	    $where3    = array('type'=>$file_type);
 	    $is_exist3 = $this->crud->is_exist("","file_type","id",$where3); 	    
 	    if(!$is_exist3) $this->crud->insert("","file_type",array('type'=>$file_type)); 	    
 	    $ftype = $this->crud->browse("","file_type","","","true","type"); 	    
 	    foreach($ftype as $val){ $type[] =  $val->type;	}
 	    $type = implode("|",$type);
 	 	 		
 		$filename =	$docID.".".$file_type;			
 		$path 	  = './uploads/documents/_temp'; 		
 		$config   = $this->m_datavendor->set_config($filename,$path,"*");
 		
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config); 
 		 		 		
 		$msg['error']     = array();
 		$msg['success']   = array();		
		if ( ! $this->upload->do_upload()){
		print $type;
		die($this->upload->display_errors());
		} else {
		$msg['success'] = $this->upload->data(); }			
 	}

 	//send notif
	$this->send_notification();
	
 	
 	$this->session->set_flashdata('tab','collapse7');
 	$this->session->set_flashdata('msg_Cor','Data akan ditampilkan setelah proses approval disetuui');
 	redirect("data_vendor/getList/{$venID}");
 }
 
 function load_correspondence(){ 	
 	$venID   = $this->session->userdata("venID");	
	$data['correspondence'] = $this->crud->browse("","correspondence","id_vendor",$venID,"true");		
	$this->load->view('data_vendor/tab/correspondence',$data); 
 }

 function update_correspondence(){
	$venID 		= $this->session->userdata("venID");
	$user_id 	= $this->tank_auth->get_user_id();
	$id 		= $this->input->post('docID');
	$old 		= $this->db->get_where('correspondence', array('id'=>$id))->result_array();
	$data_old = $old[0];

 	$docID      = $_POST['docID'];
 	$doc_name   = $_POST['doc_name'];
	$doc_num    = $_POST['doc_num']; 
	$attcfile   = $_POST['attcfile'];
	$file_type  = $this->input->post('doc_type');

	$upload_file = $_FILES['userfile']['name'];
 	

	if($upload_file)
 	{ 	
 		$file_type = $this->m_datavendor->get_file_type($_FILES); 	
	 	$path 	   = "./uploads/documents/".$attcfile; 		
		if (file_exists($path)) unlink($path); 

 	    $where3    = array('type'=>$file_type);
 	    $is_exist3 = $this->crud->is_exist("","file_type","id",$where3); 	    
 	    if(!$is_exist3) $this->crud->insert("","file_type",array('type'=>$file_type)); 	    
 	    $ftype = $this->crud->browse("","file_type","","","true","type"); 	    
 	    foreach($ftype as $val){ $type[] =  $val->type;	}
 	    $type = implode("|",$type);
 	 	 		
 		$filename =	$docID.".".$file_type;			
 		$path 	  = './uploads/documents/_temp'; 		
 		$config   = $this->m_datavendor->set_config($filename,$path,"*");
 		
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config); 

 		$msg['error']     = array();
 		$msg['success']   = array();

		if(!$this->upload->do_upload())
		{
			die($this->upload->display_errors());
		}else
		{
			$msg['success'] = $this->upload->data(); 
		}	

		print_r($data_old);

		$this->crud->insert("",'vendor_changedata', array('id' =>null ,'id_vendor'=>$venID,'table'=>'correspondence', 'index'=>$id, 'fieldname'=>'doc_file', 'before'=> $id.'.'.$data_old['filetype'],'after'=> $id.'.'.$file_type,'status'=>'request','created_id'=>$user_id,'created_date'=>date('Y-m-d H:i:s'),'last_updated'=>date('Y-m-d H:i:s') ) );
 	} 
	
	$update = array( 	'doc_name'  => $_POST['doc_name'],
						'doc_num'   => $_POST['doc_num'],
						'desc'      => $_POST['desc'],
						'eff_date'  => $_POST['eff_date'],
						'exp_date'  => $_POST['exp_date'],
						'remark'    => $_POST['remark'],
						'filetype'  => $file_type,
						'status'    => "1",						
						'last_updated' => date('Y-m-d H:i:s'));	
						
	 if($file_type=="-") unset($update['filetype']);				 
 	 
 	 $is_exist  = $this->m_datavendor->validate_upd_vendor($_POST); 	 
     
     if(!$is_exist)
     {
     	
		$curr_date = date('Y-m-d H:i:s');
		
		$data_new = $update;

		$remove = array('id', 'id_vendor', 'status','created_id', 'created_date' , 'last_updated','filetype');
		foreach ($remove as $value)
		{
		unset($data_old[$value]);
		}	

		unset($data_new['last_updated']);
		unset($data_new['status']);
		unset($data_new['filetype']);
		
		$result = $this->comparison($data_new,$data_old);

		//send notif
		if(!empty($result))
		{
			$this->send_notification();
		}
			
		foreach ($result as $key => $value)
		{
			$this->crud->insert("",'vendor_changedata', array('id' =>null ,'id_vendor'=>$venID,'table'=>'correspondence', 'index'=>$id, 'fieldname'=>$key, 'before'=> $data_old[$key],'after'=> $data_new[$key],'status'=>'request','created_id'=>$user_id,'created_date'=>date('Y-m-d H:i:s'),'last_updated'=>date('Y-m-d H:i:s') ) );
		}

     }	
 	 	
 	$this->session->set_flashdata('tab','collapse7');
 	$this->session->set_flashdata('msg_Cor','Data akan ditampilkan setelah proses approval disetuui');
 	
 	redirect("data_vendor/getList/{$venID}");

 }

 function delete_correspondence(){
 	$id = $this->input->post('id');
	$q = "insert into `temp_correspondence` (id_tbl,id_vendor,doc_name,doc_num,`desc`,eff_date,exp_date,remark,filetype,status,created_id,created_date,last_updated,approval)
	select id, id_vendor,doc_name,doc_num,`desc`,eff_date,exp_date,remark,filetype,'0',created_id,created_date,last_updated,'delete' from
	`correspondence` where id = $id ";
	$this->db->query($q);
	print $this->db->last_query();

	//send notif
	$this->send_notification();
	
 
 }

 function send_notification()
 {
 	$venID = $this->session->userdata('venID');

 	$select  = "register_num as num,vendor_name as name,email";
    $vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);

    $subject =  "Notification Change Data Approval | E-procurement Garuda";

    $recepient = $this->m_user->get_userdata_group('sm_ibs');
   print_r($recepient);
    foreach($recepient as $value)
    {
      if(filter_var($value->email, FILTER_VALIDATE_EMAIL))
      {
      	$user    = $this->users->get_user_by_email($value->email);
        if($user)
        { 
        	$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'change_request','param'=>$venID); 
            
          	$link    = $this->create_session_link((object) $require);
	        $to      =  $value->email;
	        $param   =  array('reg_num'     => $vendor->num,
	                          'vendor_name' => $vendor->name,
	                          'link'		=> $link,
	                          'to'          => $value->username);
	        $param['view']  = NOTIF_PATH."EPCGA056";    
         	$message    = $this->load->view(NOTIF_TMPL,$param,true); 
	        $this->crud->sendMail($to,$subject,$message);
    	}
      }
    }
 
 }

 function create_session_link($require){
  
  $expired = date('Y-m-d H:i:s', strtotime("+3 days")); $session = md5(uniqid());
  $insert  = array( 'token'    => $session,
            'email'      => $require->email,
            'username'     => $require->username,            
            'request'      => $require->request,
            'parameter'    => $require->param,            
            'expired_date' => $expired,
            'status'     => "1",
            'created_id'   => $this->tank_auth->get_user_id(),
            'created_date' => date('Y-m-d H:i:s'),
            'last_updated' => date('Y-m-d H:i:s'));
            
  $this->crud->insert("","session_notification",$insert);       
  $result   = base_url()."authenticate/session/".$session;  
  
  return $result;               
 } 


 





}
