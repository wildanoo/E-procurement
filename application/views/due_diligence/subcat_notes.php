<div id="container11">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	
      
   $('#btn_cancel11').click(function(){ 
	 	var id_visit  = $("#id_visit").val(); 
	 	var id_subcat = $("#id_subcat").val();
	 	$("#container11").load(baseUrl+'due_diligence/view_category/'+id_visit);    		
   }); 
   
   $('#btn_create11').click(function(){ 
   		var id_visit  = $("#id_visit").val(); 
	 	var id_subcat = $("#id_subcat").val();
   		$("#container11").load(baseUrl+'due_diligence/form_subcat_notes/'+id_visit+'/'+id_subcat);    		
   }); 
   
 }); 
 
 function del_subcat_notes(id){ 
 		var baseUrl  = "<?php echo base_url(); ?>";	 	
 		var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';  	
  		if (confirm('Are you sure you want to delete this?')) {  			
  			$.post( baseUrl+ "due_diligence/delete_subcat_note", { csrf: token , id: id } )
			  .done(function( resp ) {
			    	var id_visit  = $("#id_visit").val(); 
			 		var id_subcat = $("#id_subcat").val();
		   			$("#container11").load(baseUrl+'due_diligence/show_subcat_notes/'+id_visit+'/'+id_subcat);    	
			    	//$("#debug11").html(resp);
			});
  		}	  	
  } 

 </script>
  
<div id="debug11"></div>
<h2>Sub Category Notes</h2>
<div style="text-align: right;">
<button type="button" id="btn_cancel11" class="btn btn-default"><i class="fa fa-backward"></i>&nbsp;&nbsp;Previous</button>
<button type="button" id="btn_create11" class="btn btn-primary">Create New</button>
</div>
<input type="hidden" name="id_visit" id="id_visit" value="<?php echo $id_visit;  ?>"/>
<input type="hidden" name="id_subcat" id="id_subcat" value="<?php echo $id_subcat;  ?>"/>

<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Notes Sub Category</th>
		<th>Created Date</th>
		<?php if($this->permit->md3){ ?><th>Action</th><?php } ?>
	</tr>
	</thead>
	<tbody>
 <?php 
 
 if($notes){ $i=1;
 	foreach($notes as $row){ ?>
 		<tr>
 			<td style="text-align: center"><?php echo $i; ?></td>
			<td><?php echo $row->notes; ?></td>
			<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
			<?php if($this->permit->md3){ ?>
			<td><button type="button" id="btn_delete11" onclick="<?php echo "del_subcat_notes($row->id)";?>" class="btn btn-warning">Delete</button></td>
			<?php } ?>
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="3">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>