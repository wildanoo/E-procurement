<script type="text/javascript"> 
	$(document).ready(function(){	
		var baseUrl   = "<?php echo base_url(); ?>";	
		$(function(){	});	
		
		$('#type').change(function(){ 
			var type = $('#type').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'policy_agreement/set_sess_category',			   
               data : 'type='+type,
               success: function(data){ 				 	
				 	document.location.href= baseUrl + 'policy_agreement/';
			   }		   
            });	
		
		});

		$("#srch-term").autocomplete({
			source: baseUrl + 'policy_agreement/get_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'policy_agreement/set_browse_session',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){                		            
				    document.location.href= baseUrl + 'policy_agreement/';
               }
            });
        });  

	});			
</script>

<div id="debug"></div>

<?php 	$msg = $this->session->flashdata('message');
		if(strpos($msg,'failed')) $color = "#bc0505";
		else $color = "#487952"; 		
	 	 ?>	
<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>

<div class="page-header">
  <h3>Policy Agreement</h3>
</div>
		<div class="col-sm-3 col-md-3 pull-right">
        <!--form class="navbar-form" role="search"-->
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" id="btn_search" ><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        <!--/form-->
        </div>

<?php
	$sess_cat = $this->session->userdata('sess_cat');
	$id_cat   = $sess_cat == '' ? '' : $sess_cat;	            
	$prop_cat = 'id=type  style="width: 120px; height: 25px; font-size: 13px"'; ?>
	<?php echo form_dropdown('type', $category, $id_cat,$prop_cat); ?>

<table id="browse" class="table table-striped table-bordered" style="font-size: 13px">
	<thead>
        <tr>
            <th>No</th>
            <th>Category</th>
            <th>Detail</th>
            <th>Content Status</th>
            <th>Created date</th>            
            <th>Action</th>
        </tr>
	</thead>
    <tbody>
<?php			
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td><?php  echo $num = $num+1; ?></td>
				<td><?php echo $category[$row->type]; ?></td>
				<td><?php   echo $row->detail; ?></td>
				<td style="text-align: center"><?php   echo $status[$row->status]; ?></td>
				<td><?php   echo date('M d, Y',strtotime($row->created_date)); ?></td>						
				<td>								
				<?php				
					$eye_open  = '<i class="glyphicon glyphicon-eye-open"></i>';
					echo anchor('policy_agreement/reviewed/'.$row->id, $eye_open,array('class'=>'default_link','title'=>'Review Content')); ?>&nbsp;
				 <?php	  
					$pencil = '<i class="fa fa-pencil"></i>';
					echo anchor('policy_agreement/form_update/'.$row->id, $pencil,array('class'=>'default_link','title'=>'Select Group')); ?>&nbsp;

				</td>								
			</tr>
<?php	$i++;  } ?>	
<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?><tr><td colspan="9">-</td></tr><?php } ?>
    </tbody>
</table>
