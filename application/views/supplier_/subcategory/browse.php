<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";	 
	 $(function(){  });
	 
	  $("#srch-term").autocomplete({
			source: baseUrl + 'category/get_subcat_tags',
			minLength:1
	   });
	   
	  $("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'category/set_sess_search',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){               		            
				   document.location.href= baseUrl + 'category/subcategory';
               }
            });
        });  
	 
	 $('#btn_create').click(function(){ 
	 	  form_create();	 	  
	 });	
});

	function form_create(){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
			BootstrapDialog.show({
			title: 'Create Sub Category',
            message: 'Category : <input type="text" id="subcategory" class="form-control">',            
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){
		                	var subcategory  = $('#subcategory').val();		                		                    
		                    $.ajax({
				               url  : baseUrl + 'category/create_subcat',
				               type : "POST",              		               
				               data : csrf +'='+ token +'&subcategory='+subcategory,
				               success: function(resp){
				               		document.location.href= baseUrl + 'category/subcategory';				               		  
						    	}
				            });            
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'category/subcategory';
				                    dialogRef.close(); 
				                }
			        }]
        });
	}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Sub Category',
            message: $('<div></div>').load(baseUrl + "category/form_update_subcategory/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){
		                	var subcategory  = $('#subcategory').val();		                		                    
		                    $.ajax({
				               url  : baseUrl + 'category/update_subcat',
				               type : "POST",              		               
				               data : csrf +'='+ token +'&id='+ id +'&subcategory='+subcategory,
				               success: function(resp){
				               		document.location.href= baseUrl + 'category/subcategory';				               		  
						    	}
				            });            
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'category/subcategory';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>
</head>
<body>
<div id="debug"></div>

<div id="debug"></div>
<div class="page-header">
  <h3>Sub Category</h3>
</div>
<?php  //$prop = array('class'=>'btn btn-primary btn-sm','title'=>'New Category','style'=>'width:80px;'); ?>					 
<?php  //echo anchor('category/form_create/','Create',$prop); ?>

<input type="button" id="btn_create" class="btn btn-primary btn-sm" value="Create" style="width:80px;"/>


<?php $msg = $this->session->flashdata('message');
	if(strpos($msg,'failed')) $color="#bc0505";
	else $color = "#487952"; ?>	
<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>

<div class="col-sm-3 col-md-3 pull-right">
        <!--form class="navbar-form" role="search"-->
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" id="btn_search"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        <!--/form-->
        </div>
	
<table border="1" id="browse" class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px">
	<thead>
		<tr>
            <th style="width:4%;text-align:center;">No</th>
            <th>Code</th>
            <th>Sub Category</th>
            <th>Created Date</th>
            <th style="text-align: center;">Action</th>
        </tr>
     </thead>
     <tbody>
 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td style="text-align:center;"><?php echo $num = $num+1; ?></td>
			<td><?php echo $row->init_code; ?></td>	
			<td><?php echo $row->subcategory; ?></td>
 			<td align="center"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>						
								
				<td style="text-align: center;">
				<?php
					echo  '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/>&nbsp;&nbsp;';
					$trash  = '<i class="fa fa-trash"></i>';
					$js = "if(confirm('Are you sure to delete record ?')){ return true; } else {return false; }";
					echo anchor('category/delete_subcat/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete','onclick'=>$js));			
				?>				
				</td>	
				
										
		</tr>
<?php	$i++;  } ?>	
<tr><td colspan="8" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?>
	<tr><td colspan="8">-</td></tr>
<?php } ?>
     </tbody>
</table>
</body>
</html>
