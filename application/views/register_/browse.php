<script type="text/javascript"> 
	var baseUrl   = "<?php echo base_url(); ?>";	
	$(function(){		
	
		$("#srch-term").autocomplete({
			source: baseUrl + 'register/get_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'register/set_browse_session',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){                		            
				    document.location.href= baseUrl + 'register/';
               }
            });
        });  
	
	
	});	
</script>

<div id="debug"></div>


<div class="page-header">
  <h3>Registration</h3></div>
  
		<div class="col-sm-3 col-md-3 pull-right">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" id="btn_search" ><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        </div>


<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
        <tr>
            <th style="text-align: center;" >No</th>
            <th>Reg. Num</th>            
            <th>Vendor Name</th>  
            <th>Category</th>  
            <th>Sub Category</th>        
            <th>Status</th>
            <th>Created Date</th>     
            <th>Action</th>  
        </tr>       
	</thead>
    <tbody>
<?php	

if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td style="text-align: center;"><?php  echo $num = $num+1;; ?></td>
				<td><?php 	echo $row->reg_num ?></td>		
				<td><?php 	echo $row->vendor_name ?></td>
				<td><?php echo $row->category; ?></td>	
				<td><?php echo $row->subcat; ?></td>			
				<td align="center"><?php   echo $status[$row->reg_sts]; ?></td>						
				<td style="text-align: center;"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
				<td><?php										
					$pencil   = '<i class="fa fa-pencil"></i>';
					echo anchor('register/form_status/'.$row->id, $pencil,array('class'=>'default_link','title'=>'Change Status')); ?>
				</td>	
				
			</tr>
<?php	$i++;  } ?>	
<tr><td colspan="8" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?>
	<tr><td colspan="8" align="center">-</td></tr>
<?php } ?>
    </tbody>
</table>

