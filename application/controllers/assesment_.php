<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Assesment extends Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_user');
		islogged_in();
 		//$this->m_user->authenticate(array(41));
	}	

	function index() {
		
		$extract = $this->getDataTables(5);
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
			
			$data ['user_id'] = $this->tank_auth->get_user_id ();
			$data ['username'] = $this->tank_auth->get_username ();
			
			$sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
			
				
			$field      = $sess_field != '' ? $sess_field : "description";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
				
			if($sess_cat != ''){	$where = array('description'=>$sess_cat);
			} else { $where = "";	}
			
			$table = "dd_assesment";
			$page = $this->uri->segment ( 3 );
			$per_page = 5;
			$offset = $this->crud->set_offset ( $page, $per_page );
			$total_rows = $this->crud->get_total_record ("",$table);
			$set_config = array (
					'base_url' => base_url() . '/assesment/index',
					'total_rows' => $total_rows,
					'per_page' => 5,
					'uri_segment' => 3 
			);
			$config = $this->crud->set_config ( $set_config );
			
			$this->load->library ( 'pagination' );
			$this->pagination->initialize ( $config );
			$paging = $this->pagination->create_links ();
			
			$order = array (
					'field' => 'created_date',
					'order' => 'DESC' 
			);
			$data ['pagination'] = $paging;
			$data ['num'] = $offset;
			$select = "id,status,description,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated,id_subcategory";

			$sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
				
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			$data['browse'] = $browse;

	 		$data['view']	= "assesment/browse"; 		
 			$this->load->view('layout/template',$data);
	}
	
	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
	
		//$search_dateranges = array('publish_date',$_POST['start_date'],$_POST['end_date']);
		$search_conditions = array(
				'description'		=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		$data['view'] = "assesment/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{
	
		/* get data for table view from database with pagination */
	
		$table 	     = "dd_assesment t1";
		$select 	 = "t1.id,status,description,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'assesment/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
	
		/* get data for table view from database with pagination */
	
		$table 	     = "dd_assesment t1";
		$select 	 = "t1.id,status,description,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,$where,$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'assesment/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	function is_exist(){
		$assesment      = $_POST['description'];
		$is_exist['1']= !$assesment ? "false" : "true";
		$msg['1']	  = !$assesment ? "assesment name required" : "";
				
		if ($is_exist['1']=='true'){
				$where    = array('description'=>$assesment);
				$checked = $this->crud->is_exist("","dd_assesment","id",$where);
				$is_exist['2']    = !$checked ? "true" : "false";
				$msg['2']	= $checked ? "duplicate assesment name" : "";
					
		}
		
		$msg = implode(" ", $msg);
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg' =>$msg);
		
		echo json_encode($result);
	}
	
	function create_(){
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array('id'=>null,'description'=>$_POST['description'],'status'=>'1',
					  'created_id'=>$userID,'created_date'=>$curr_date,'last_updated'=>$curr_date);
		$id = $this->crud->insert("","dd_assesment",$data);
		$this->session->set_flashdata('message','1 data success insert');
	}
	
	function form_create() {
		$data ['user_id'] = $this->tank_auth->get_user_id ();
		$data ['username'] = $this->tank_auth->get_username ();
		$data['subcategory']  = $this->crud->browse("","subcategory","","","true","id,subcategory");
		$this->load->view('assesment/form_create',$data);
	}
	
	function create(){
		$ttl   		  = $_POST['ttl'];
		$subcat  	  = $_POST['subcategory'];
		$subcategory = "";
		//$where 	      = array('subcategory'=>$subcat);
		//$is_exist     = $this->crud->is_exist("","subcategory","id",$where);
		
		for ($i = 1; $i <= $ttl; $i++) {
			$value = $this->input->post('check'.$i);
			if($value) $doc[] = $value;
		}
		if($doc){ $subcategory = implode(",",$doc); }
		$data = array(
				'description'	=> $_POST['description'],
				'id_subcategory'	=> $subcategory,
				'status'		=> '1',
				'created_id'	=> $this->tank_auth->get_user_id(),
				'created_date'  => date('Y-m-d H:i:s'),
				'last_updated'  => date('Y-m-d H:i:s'));
		//print_r($ttl);exit;
// 		if(!$is_exist){
			$this->crud->insert("","dd_assesment",$data); 
// 		}
				
// 			$msg1 = "succesfully add new document";
// 			$msg2 = "duplicate group document";
// 			$msg  = !$is_exist ? $msg1 : $msg2;
// 			//print_r($data);exit;
		
 			$this->session->set_flashdata('message','1 data success insert');
	}
	
	function form_update(){
		$id   = $this->uri->segment(3);
		$select = "id,id_subcategory,description,created_id,created_date,last_updated";
		$data ['def'] = $this->crud->browse ( "", "dd_assesment", "id", $id, "false", $select);
	
		$order  = array('field'=>'subcategory','sort'=>'ASC');
		$data['subcategory']  = $this->crud->browse("","subcategory","","","true","id,subcategory","",$order);
	
		$this->load->view ( 'assesment/form_update', $data );
	}
	
	function update_(){
		$curr_date 	= date('Y-m-d H:i:s');
		$userID = $this->tank_auth->get_user_id();
		$id   = $this->uri->segment(3);
		$data = array(	'description'	=> $_POST['description'],
						'last_updated'	=> $curr_date,
						'status'		=> '1',
						'created_id'	=> $userID
		);
		$this->crud->update("","dd_assesment","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
	}
	
	function update(){
		$ttl   		 = $_POST['ttl'];
		$id  		 = $_POST['id'];
		$curr_date 	= date('Y-m-d H:i:s');
		$this->crud->delete("","dd_assesment","id",$id);
		$userID = $this->tank_auth->get_user_id();
		$doc		 = "";
		$subcategory	 = "";
		for ($i = 1; $i <= $ttl; $i++) {
			$value = $this->input->post('check'.$i);
			if($value) $doc[] = $value; }
	
			if($doc) $subcategory = implode(",",$doc);
			//$userID = $this->tank_auth->get_user_id();
			$data = array(//'document_req_id'  => !$document ? "" : $document,
					'id'			=> $id,
					'description'	=> $_POST['description'],
					'id_subcategory'	=> $subcategory,
					'status'		=> '1',
					'created_id'	=> $this->tank_auth->get_user_id(),
					'created_date'  => date('Y-m-d H:i:s'),
					'last_updated'  => date('Y-m-d H:i:s'));
			//$this->crud->update("","subcontent_type","id",$id,$update);
			$this->crud->insert("","dd_assesment",$data);
			//print_r($update);
	
			$this->session->set_flashdata('message','1 data success update');
			redirect('assesment','refresh');
	}
	
	function delete(){
	
		$id = $this->uri->segment(3);
		$status = array('status'=>'0');
		$this->crud->update("","dd_assesment","id",$id,$status);
		redirect('assesment/','refresh');
	}
    
    	function delete_(){
		$data ['username'] = $this->tank_auth->get_username ();
		$id = $this->uri->segment(3);
		
		$where1   = array('id_country'=>$id);
		$checked1 = $this->crud->is_exist("","region","id",$where1);
		
		//$checked2 = $this->crud->is_exist("","vendor","id",$where1);
		//if ($checked1 || $checked2){
		if ($checked1){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		}
		else {
			$this->crud->delete("","country","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
		redirect('country/','refresh');
		}
}