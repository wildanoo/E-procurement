<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('m_document')); islogged_in();

	} 

	function index()
	{
		if($this->m_document->authenticate()){
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			$session 		= $this->session->userdata('sess_document');
			$sess_id 		= $this->session->userdata('search');

			$table 			= "vendor as t1";
			$page			= $this->uri->segment(3);
			$per_page		= 5;
			$offset 		= $this->crud->set_offset($page,$per_page);

			$total_rows		= $this->m_document->get_total_record_search("",$table,$where,$groupby,$select,$joins,$like); 

			$set_config 	= array('base_url'=>base_url().'document/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
			$config 		= $this->crud->set_config($set_config);

			$this->load->library('pagination');
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();

			$data['pagination']	= $paging;
			$data['num']		= $offset;


			$order				= array('field'=>'t1.last_updated','order'=>'DESC');

			$select			 	= "t1.id,register_num as reg_num,vendor_type,vendor_num,vendor_name,t1.created_date,t1.last_updated,SUM(filesize) as filesize";

			$joins[0][0] 		= 'vendor_docs as t2';
			$joins[0][1] 		= 't2.id_vendor = t1.id';
			$joins[0][2] 		= 'left';

			$groupby 			= 't1.id';

			$browse			= $this->m_document->browse_search_join_with_paging("",$table,"","","",$select,$joins,"",$order,$per_page,$offset,$groupby,$like);


			$data['browse']	= $browse;
			$data['view']	= 'document/browse';
			$this->load->view('layout/template',$data);
		}else{
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/');
		}
	}

	function search(){
		$term 	= ($this->input->post('search_term'))? $this->input->post('search_term') : $this->session->userdata('term');
		$search_vals = array('term'=>$term);
		$this->session->set_userdata($search_vals);

		$table 				= "vendor as t1";
		$select				= "t1.id,register_num as reg_num,vendor_type,vendor_num,vendor_name,t1.created_date,t1.last_updated,SUM(filesize) as filesize";
		$order				= array('field'=>'t1.last_updated','order'=>'DESC');
		$joins[0][0] 		= 'vendor_docs as t2';
		$joins[0][1] 		= 't2.id_vendor = t1.id';
		$joins[0][2] 		= 'left';
		$groupby 			= 't1.id';
		

		$where 			= "t1.vendor_name like '%$term%' or t1.vendor_num like '%$term%' ";

		$page 			= $this->uri->segment(3);
		$per_page 		= 5;
		$offset 		= $this->crud->set_offset($page,$per_page);
		$total_rows		= count($this->crud->browse_join("",$table,"","","true",$select,$joins,$where,$order,"",""));
		/*$this->m_document->get_total_record_search("",$table,$where,$groupby,$select,$joins,$like);*/
		$set_config 	= array('base_url'=>base_url().'document/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config 		= $this->crud->set_config($set_config);

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();

		$order				= array('field'=>'t1.last_updated','order'=>'DESC');
		$data['pagination']	= $paging;
		$data['num']		= $offset;

		$browse				= $this->m_document->browse_search_join_with_paging("",$table,"","","",$select,$joins,$where,$order,$per_page,$offset,$groupby,$like);
		$data['browse']		= $browse;

		$data['view']		= 'document/browse';

		$this->load->view('layout/template',$data);

	}
	function set_sess_document(){
		$id = $this->uri->segment(3);
		$this->session->set_userdata("docID",$id);
		redirect("document/detail");
	}

	function detail(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$docID 				= $this->session->userdata("docID");
		
		$table  			= "vendor_docs as t1";
		$where 				= array('id_vendor' => $docID);
		$joins[0][0] 		= 'table_docs as t2';
		$joins[0][1] 		= 't2.id = t1.id_table';
		$joins[0][2] 		= '';
		$order				= array('field'=>'t1.last_updated','order'=>'DESC');

		$page				= $this->uri->segment(3);
		$per_page			= 5;
		$offset 			= $this->crud->set_offset($page,$per_page);
		$total_rows 		= $this->crud->get_total_record("",$table,$where);

		$set_config 		= array('base_url'=>base_url().'document/detail','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);

		$config 		= $this->crud->set_config($set_config);

		$select  		= "*,t1.id as venDoc_id";
		

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();

		$data['pagination']	= $paging;
		$data['num']		= $offset;

		$browse 			= $this->crud->browse_join_with_paging("",$table,"","","",$select,$joins,$where,$order,$per_page,$offset,"");

		$data['browse']		= $browse;
		$data['view']		= "document/detail";
		$this->load->view('layout/template',$data);
	}

	function delete_document(){
		$ID 		= $_POST['id'];
		$docID 		= $_POST['doc'];
		$tabID		= $_POST['tab'];
		$type 		= $this->crud->browse("","vendor_docs","id",$ID,"false","filetype as type")->type;
		$filename 	= $docID.".".$type;


		if($tabID == '1'){
			$path 		= "./uploads/documents/".$filename;
			$this->crud->delete("","correspondence","id",$docID);
		}elseif($tabID == '2'){
			$path 		= "./uploads/due_dilligence/".$filename;
			$this->crud->delete("","dd_docs","id",$docID);
		}

		if(file_exists($path)) unlink($path);

		$this->crud->delete("","vendor_docs","id",$ID);
		
	}

	function load_document(){
		$docID 				= $this->session->userdata("docID");
		
		$table  			= "vendor_docs as t1";
		$where 				= array('id_vendor' => $docID);
		$joins[0][0] 		= 'table_docs as t2';
		$joins[0][1] 		= 't2.id = t1.id_table';
		$joins[0][2] 		= '';
		$order				= array('field'=>'t1.last_updated','order'=>'DESC');

		$page				= $this->uri->segment(3);
		$per_page			= 5;
		$offset 			= $this->crud->set_offset($page,$per_page);
		$total_rows 		= $this->crud->get_total_record("",$table,$where);

		$set_config 		= array('base_url'=>base_url().'document/detail','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);

		$config 			= $this->crud->set_config($set_config);

		$select  			= "*,t1.id as venDoc_id";
		

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();

		$data['pagination']	= $paging;
		$data['num']		= $offset;

		$browse 			= $this->crud->browse_join_with_paging("",$table,"","","",$select,$joins,$where,$order,$per_page,$offset,"");

		$data['browse']		= $browse;
		$this->load->view("document/detail",$data);
	}

}

/* End of file document.php */
/* Location: ./application/controllers/document.php */