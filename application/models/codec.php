<?php 
class codec extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->model('crud');
    }
    
 function general($table,$chr,$where="",$group=""){ 
    
    $Mdate    = date("m-d"); 
    $year     = date('Y'); 
    $yearCode = substr($year,2,4);
    $dateNow  = $yearCode.$Mdate;
    $dateNow  = str_replace("-","", $dateNow);  
    $counter  = $this->total_record("",$table,$where="",$group=""); 
    $counter  = $counter==0 ? 1 : $counter+1;   
    
    if(strlen($counter)==1){ $pre='00'; 
    } elseif(strlen($counter)==2){ $pre='0'; } else { $pre=''; }
        
    $seq     = $pre.$counter;
    $gen_num = $dateNow.'.'.$chr.$seq;
    
    return $gen_num;
    
 } 
 
 function announce($id_type){
    
    $TYPE      = array('1'=>'NW','2'=>'NW','4'=>'SO','3'=>'BD','5'=>'DS','6'=>'LB');    
    $user_id   = $this->tank_auth->get_user_id();
    $select    = "(SELECT dept_code FROM departemen r WHERE r.id=l.id_dept) as code";
    $DEPT_CODE = $this->crud->browse("","ga_employee l","userID",$user_id,"false",$select)->code;
    // $DEPT_CODE = "DEFAULT";
    if ($id_type == '1' || $id_type == '2') {
        $addition = 'content_type = "1"';
    }else{
        $addition = 'subcontent_type = "'.$id_type.'"';
    }    
    
    $DATE       = date('Y/m'); $where=$addition.' AND (code IS NOT NULL AND code != "")';
    // $counter   = $this->crud->get_total_record("","announce",$where);
    $counters   = $this->crud->browse('','announce','','','false','(COALESCE (max(counter), (0))) AS max',$where);
    foreach ($counters as $counter) {}

    $counter = $counter->max==0 ? 1 : ($counter->max+1);
    
    if(strlen($counter)==1){ $pre='000';
    } elseif(strlen($counter)==2){ $pre='00';
    } elseif(strlen($counter)==3){ $pre='0';
    } else { $pre=''; } 
    
    $RUN_NUM = $pre.$counter;
    $result['runningcode']  = "EPROC-".$DEPT_CODE."/".$DATE."/".$TYPE[$id_type]."/".$RUN_NUM;
    $result['increment']    = $counter;
    
    return $result;
    
 }
 
 function register($id_src){
    
    $CODE      = array('1'=>'SR','2'=>'BD','3'=>'SO','4'=>'DS','5'=>'LB','6'=>'AR');
    $DATE      = substr(date('Y'),2,2);
    $counter   = $this->crud->get_total_record("","vendor");
    $counter   = $counter==0 ? 1 : $counter+1;
    if(strlen($counter)==1){ $pre='00000';
    } elseif(strlen($counter)==2){ $pre='0000';
    } elseif(strlen($counter)==3){ $pre='000';
    } elseif(strlen($counter)==4){ $pre='00';
    } elseif(strlen($counter)==5){ $pre='0';
    } else { $pre=''; } 
    
    $RUN_NUM = $pre.$counter;   
    $result  = $CODE[$id_src].$DATE.$RUN_NUM;
    
    return $result;
    
 }

 function getRealIpAddress()
{
    if (isset($_SERVER["HTTP_CLIENT_IP"])) return $_SERVER["HTTP_CLIENT_IP"];
    elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) return $_SERVER["HTTP_X_FORWARDED_FOR"];
    elseif (isset($_SERVER["HTTP_X_FORWARDED"])) return $_SERVER["HTTP_X_FORWARDED"];
    elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) return $_SERVER["HTTP_FORWARDED_FOR"];
    elseif (isset($_SERVER["HTTP_FORWARDED"])) return $_SERVER["HTTP_FORWARDED"];
    else return $_SERVER["REMOTE_ADDR"];
}

 function request_encryption($arraykey=array()){
    # --- ENCRYPTION ---
    # the key should be random binary, use scrypt, bcrypt or PBKDF2 to
    # convert a string into a key
    # key is specified using hexadecimal
    $key = $this->request_hash("md5",'gotoHellMan!!');

    $hashed_val = implode(';;;', $arraykey);
    
    # show key size use either 16, 24 or 32 byte keys for AES-128, 192
    # and 256 respectively
    $key_size =  strlen($key);
    //echo "Key size: " . $key_size . "\n";
    
    $plaintext = $hashed_val;

    # create a random IV to use with CBC encoding
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    
    # creates a cipher text compatible with AES (Rijndael block size = 128)
    # to keep the text confidential 
    # only suitable for encoded input that never ends with value 00h
    # (because of default zero padding)
    $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key,
                                 $plaintext, MCRYPT_MODE_CBC, $iv);

    # prepend the IV for it to be available for decryption
    $ciphertext = $iv . $ciphertext;
    
    # encode the resulting cipher text so it can be represented by a string
    $ciphertext_base64 = base64_encode($ciphertext);

    return $ciphertext_base64;
 }

 function request_decryption($ciphertext){

    $key = $this->request_hash("sha256",'gotoHellMan!!');

    # === WARNING ===

    # Resulting cipher text has no integrity or authenticity added
    # and is not protected against padding oracle attacks.
    
    # --- DECRYPTION ---
    
    $ciphertext_dec = base64_decode($ciphertext);
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    
    # retrieves the IV, iv_size should be created using mcrypt_get_iv_size()
    $iv_dec = substr($ciphertext_dec, 0, $iv_size);
    
    # retrieves the cipher text (everything except the $iv_size in the front)
    $ciphertext_dec = substr($ciphertext_dec, $iv_size);

    # may remove 00h valued characters from end of plain text
    $plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key,
                                    $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);
    
    return  $plaintext_dec;
 }

 function request_hash($algo,$public_key){
    $rnd = hash($algo, $public_key);
    $key = pack('H*', $rnd);

    return $key;
 }
 
 function index_notif(){
 
 	$counter  = $this->crud->get_total_record("","notifications");
 	$counter  = $counter==0 ? 1 : $counter+1;
 
 	if(strlen($counter)==1){ $pre='00';
 	} elseif(strlen($counter)==2){ $pre='0'; } else { $pre=''; }
 
 	$seq      = $pre.$counter;
 	$result    = "EPCGA".$seq;
 
 	return $result;
 
 }
    
}   
?>