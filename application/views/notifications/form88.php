<?php $this->load->view('notifications/header');?>

<b>Kepada Yth.</b><br/>
<?php echo $vname;?>.<br/><br/>
Pertama-tama kami mengucapkan terima kasih atas minat Bapak/Ibu telah mendaftar sebagai rekanan PT Garuda Indonesia (Persero) Tbk ("Garuda”) untuk kategori <?php echo $category ?>.<br/>
<br>
Namun berdasarkan beberapa pertimbangan, bidding kami batalkan.<br>
<br>
<br>
Terimakasih <br>
<br>
    
<?php $this->load->view('notifications/footer');?>