<?php
///                                       from sendEmailToVendor - blacklist.php
$this->load->view('notifications/header');?>

<br/>
<b>Kepada Yth.</b><br/>
<b><?php echo $vendor_name;?></b><br/>
Di <?php echo $address;?><br/><br/><br/>

Dengan hormat,<br/><br/>

Bersama ini kami sampaikan teguran terkait dengan kelalaian <b><?php echo $vendor_name;?></b> dengan rincian sebagai berikut : <br/><br/>

<b><?php echo $reason;?></b><br/><br/>

Maka menindaklanjuti hal tersebut, mengacu pada kebijakan vendor yang berlaku di PT. Garuda Indonesia (Persero) Tbk (Refer <a href="http://eproc.garuda-indonesia.com/">http://eproc.garuda-indonesia.com/</a> pada bagian vendor policy poin II), bahwa <b><?php echo $vendor_name;?></b> telah melakukan pelanggaran dan dikenakan sanksi berupa Garuda berhak memberikan surat pernyataan dilarang mengikuti kegiatan pengadaan dilingkungan Garuda Indonesia dan Garuda berhak mengumumkan status Blacklist bagi perusahaan tersebut di web E-Procurement Garuda Indonesia.<br/><br/>

Kami harapkan hal ini tidak terulang dan dapat menjadi perbaikan di kemudian hari.<br/><br/>

<?php
if($file!=''){ echo '<a href="'.$file.'">(link download surat keputusan)</a><br/><br/>';}
?>

Demikian disampaikan, atas perhatiannya diucapkan terima kasih <br/><br/>

    
<?php $this->load->view('notifications/footer');?>