<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_vendorlist extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('crud');
	}

	function authenticate(){
		$allowed  	= array(71);
		$permit  	= $this->session->userdata('permissions');
		$permit 	= explode(",",$permit);
		foreach($permit as $str){
			$permissions[] = preg_replace("/[^0-9]/","",$str);
		}
		$result 	= array_intersect($allowed, $permissions);
		if($result){ return true;} return false;
	}

	function is_permit($allowed){
		$permit 	= $this->session->userdata('permissions');
		$permit 	= explode(",",$permit);
		foreach($permit as $str){
			$permissions[] = preg_replace("/[^0-9]/","",$str);
		}
		$result 	= array_intersect($allowed, $permissions);

		if($result){ return true;} return false;
	}

	function is_approved(){
		$user_group 		= $this->session->userdata('user_group');
		$approval_level 	= $this->session->approval_level();

		if(in_array($user_group,$approval_level)){ return true;
		}else{ return false;}
	}

	function approval_level(){

		$approved = $this->crud->browse("","approval_level","","","true","id_group,level");
		foreach($approved as $val){ $level[$val->level] = $val->id_group;	}

		return $level;
	}

	function status_def(){
		$status = array(
			'avl' 		=> 'AVL',
			'shortlist' => 'Shortlist',
			'reject' 	=> 'Reject'
			);

		return $status;
	}
	
	function search_cond($A,$B,$C,$D,$E,$F){
		if($A != 'all' && empty($B) && $C =='all' && empty($D) && empty($E) && $F == 'all'){
			$where = " AND t2.id_subcat = $A";
		}elseif($A != 'all' && !empty($B) && $C =='all' && empty($D) && empty($E) && $F == 'all'){
			$where = " AND t2.id_subcat = $A AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%')";
		}elseif($A != 'all' && !empty($B) && $C !='all' && empty($D) && empty($E) && $F == 'all'){
			$where = " AND t2.id_subcat = $A AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%') AND t1.vendor_type = '$C'";
		}elseif($A != 'all' && !empty($B) && $C !='all' && !empty($D) && !empty($E) && $F == 'all'){
			$where = " AND t2.id_subcat = $A AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%') AND t1.vendor_type = '$C' AND (t1.approved_date between '$D' AND '$E')";
		}elseif($A != 'all' && !empty($B) && $C !='all' && !empty($D) && !empty($E) && $F != 'all'){
			$where = " AND t2.id_subcat = $A AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%') AND t1.vendor_type = '$C' AND (t1.approved_date between '$D' AND '$E') AND t1.status = '$F'";
		}elseif(!empty($B) && $C =='all' && empty($D) && empty($E) && $F =='all' && $A == 'all'){
			$where = " AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%')";
		}elseif(!empty($B) && $C !='all' && empty($D) && empty($E) && $F =='all' && $A == 'all'){
			$where = " AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%') AND t1.vendor_type = '$C'";
		}elseif(!empty($B) && $C !='all' && !empty($D) && !empty($E) && $F =='all' && $A == 'all'){
			$where = " AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%') AND t1.vendor_type = '$C' AND (t1.approved_date between '$D' AND '$E')";
		}elseif(!empty($B) && $C !='all' && !empty($D) && !empty($E) && $F !='all' && $A == 'all'){
			$where = " AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%') AND t1.vendor_type = '$C' AND (t1.approved_date between '$D' AND '$E') AND t1.status = '$F' "; 
		}elseif($C !='all' && empty($D) && empty($E) && $F =='all' && $A == 'all' && empty($B)){
			$where = " AND t1.vendor_type = '$C' ";
		}elseif($C !='all' && !empty($D) && !empty($E) && $F =='all' && $A == 'all' && empty($B)){
			$where = " AND t1.vendor_type = '$C' AND (t1.approved_date between '$D' AND '$E')";
		}elseif($C !='all' && !empty($D) && !empty($E) && $F !='all' && $A == 'all' && empty($B)){
			$where = " AND t1.vendor_type = '$C' AND (t1.approved_date between '$D' AND '$E') AND t1.status = '$F'";
		}elseif($C !='all' && !empty($D) && !empty($E) && $F !='all' && $A != 'all' && empty($B)){
			$where = " AND t1.vendor_type = '$C' AND (t1.approved_date between '$D' AND '$E') AND t1.status = '$F' AND t2.id_subcat = $A";
		}elseif(!empty($D) && !empty($E) && $F =='all' && $A == 'all' && empty($B) && $C =='all'){
			$where = " AND t1.approved_date between '$D' AND '$E'";
		}elseif(!empty($D) && !empty($E) && $F !='all' && $A == 'all' && empty($B) && $C =='all'){
			$where = " AND (t1.approved_date between '$D' AND '$E') AND t1.status = '$F'";
		}elseif(!empty($D) && !empty($E) && $F !='all' && $A != 'all' && empty($B) && $C =='all'){
			$where = " AND (t1.approved_date between '$D' AND '$E') AND t1.status = '$F' AND t2.id_subcat = $A";
		}elseif(!empty($D) && !empty($E) && $F !='all' && $A != 'all' && !empty($B) && $C =='all'){
			$where = " AND (t1.approved_date between '$D' AND '$E') AND t1.status = '$F' AND t2.id_subcat = $A AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%')";
		}elseif($F !='all' && $A == 'all' && empty($B) && $C =='all' && empty($D) && empty($E)){
			$where = " AND t1.status = '$F'";
		}elseif($F !='all' && $A != 'all' && empty($B) && $C =='all' && empty($D) && empty($E)){
			$where = " AND t1.status = '$F' AND t2.id_subcat = $A";
		}elseif($F !='all' && $A != 'all' && !empty($B) && $C =='all' && empty($D) && empty($E)){
			$where = " AND t1.status = '$F' AND t2.id_subcat = $A AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%')";
		}elseif($F !='all' && $A != 'all' && !empty($B) && $C !='all' && empty($D) && empty($E)){
			$where = " AND t1.status = '$F' AND t2.id_subcat = $A AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%') AND t1.vendor_type = '$C'";
		}elseif($A != 'all' && $C != 'all' && empty($B) && empty($D) && empty($E) && $F =='all'){
			$where = " AND t2.id_subcat = $A AND t1.vendor_type = '$C'";
		}elseif($A != 'all' && !empty($D) && !empty($E) && $F =='all' && $C == 'all' && empty($B)){
			$where = " AND t2.id_subcat = $A AND (t1.approved_date between '$D' AND '$E')";
		}elseif(!empty($B) && !empty($D) && !empty($E) && $A == 'all' && $C =='all' && $F =='all'){
			$where = " AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%') AND (t1.approved_date between '$D' AND '$E')";
		}elseif(!empty($B) && $F !='all' && empty($D) && empty($E) && $A == 'all' && $C =='all'){
			$where = " AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%') AND t1.status = '$F'";
		}elseif($C !='all' && $F !='all' && empty($D) && empty($E)  && $A == 'all' && empty($B)){
			$where = " AND t1.vendor_type = '$C' AND t1.status = '$F'";
		}elseif($A != 'all' && !empty($B) && !empty($D) && !empty($E) && $C =='all' && $F == 'all'){
			$where = " AND t2.id_subcat = $A AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%') AND (t1.approved_date between '$D' AND '$E')";
		}elseif($A != 'all' && $C !='all'  && !empty($D) && !empty($E) && empty($B) && $F == 'all'){
			$where = " AND t2.id_subcat = $A AND t1.vendor_type = '$C' AND (t1.approved_date between '$D' AND '$E')";
		}elseif($A != 'all' && $C !='all'  && $F != 'all' && empty($D) && empty($E) && empty($B)){
			$where = " AND t2.id_subcat = $A AND t1.vendor_type = '$C' AND t1.status = '$F'";
		}elseif(!empty($B) && $C !='all' && $F !='all' && empty($D) && empty($E)  && $A == 'all'){
			$where = " AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%') AND t1.vendor_type = '$C' AND t1.status = '$F' "; 
		}elseif(!empty($B) && $F !='all' && !empty($D) && !empty($E)  && $C =='all'  && $A == 'all'){
			$where = " AND (t1.vendor_name LIKE '%$B%' OR t1.vendor_num LIKE '%$B%') AND (t1.approved_date between '$D' AND '$E') AND t1.status = '$F' "; 
		}else{}

		return $where;
	}

	function vend_process($id){
		$query = "SELECT *
		FROM (SELECT 'redlist' as tbl,`id_vendor`,`status`,`created_date`
		FROM vendor_redlist WHERE status = 'wait'
		UNION ALL
		SELECT 'blacklist' as tbl,`id_vendor`,`status`,`created_date`
		FROM vendor_blacklist WHERE status = 'wait'
		UNION ALL
		SELECT 'inactive' as tbl,`id_vendor`,`status`,`created_date`
		FROM vendor_inactive WHERE status = 'request' AND process = 'inactive'
		UNION ALL
		SELECT 'active' as tbl,`id_vendor`,`status`,`created_date`
		FROM vendor_inactive WHERE status = 'request' AND process = 'active'
		UNION ALL
		SELECT 'delete' as tbl,`id_vendor`,`status`,`created_date`
		FROM vendor_delete WHERE status = 'request' 
		UNION ALL
		SELECT 'deletenow' as tbl,`id_vendor`,`status`,`created_date`
		FROM vendor_delete WHERE status = 'approved' OR status = 'start' 
		) as sub WHERE id_vendor = $id ORDER BY created_date DESC LIMIT 1";
		$row = $this->db->query($query)->row();
		$result = $row->tbl;
		if(empty($result)){ $remark = "Normal";}else{ $remark = $result;}
		return $remark;

	}


}

/* End of file m_vendor3.php */
/* Location: ./application/models/m_vendor3.php */