<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bank_account_robot extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('m_user','m_vendor'));	
    }
    
 function index(){
 	$select = "id,id_vendor,expired_date,(SELECT email FROM vendor r WHERE r.id=l.id_vendor) as email,
 			  (SELECT register_num FROM vendor r WHERE r.id=l.id_vendor) as num,
 			  (SELECT vendor_name FROM vendor r WHERE r.id=l.id_vendor) as name";
 	$current = strtotime("now");  //echo $current;  exit;
 	$where   = "UNIX_TIMESTAMP(`expired_date`) <= '$current' AND `status` = '1' ";		
 	$data    = $this->crud->browse("","bank_account_mail l","","","true",$select,$where); //print_r($data); exit;
 	$subject = "Notification E-procurement Garuda";
 	$adm_src = $this->m_user->get_userdata_group("as");
	foreach($adm_src as $value){
		$user    = $this->users->get_user_by_email($value->email);	
	 	if($user){			 	
		 	foreach($data as $val){	
		 		$execute['id'] = $val->id;
		 		$require   = array('email'=>$user->email,'username'=>$user->username,'request'=>'bank_account','param'=>$val->id_vendor);	 								 
				$link      = $this->m_vendor->create_session_link((object) $require);								
				$param     = array('to' 			=> $value->username,
								 'reg_num'		=> $val->num,
								 'vendor_name'	=> $val->name,
								 'link'			=> $link);	
								 		  
				$param['view']  = NOTIF_PATH."EPCGA009"; 		
				$message 		= $this->load->view(NOTIF_TMPL,$param,true);	//echo $message."<br/>";	
				$this->crud->sendMail($value->email,$subject,$message);			
			} 
		}
	} 	
	
	if($execute){
		$update = array('status'=>'0','last_updated'=>date('Y-m-d H:i:s'));	
		foreach($execute as $key=>$value2){ $params[]  = "`$key` = '$value2'";	}	
		if(count($params)>1){	$OR  = implode(" OR ",$params);   unset($params);
		} else { $OR = $params[0]; }
	    $this->crud->multiple_update("","bank_account_mail",$OR,$update); 
	}
 	
 } 

}

