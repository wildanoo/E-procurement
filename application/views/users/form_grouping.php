 
 
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">

              <div class="box-header with-border">
                <h3 class="box-title">Create Group</h3>
              </div>
              <!--form class = "form-horizontal"-->
              <?php echo form_open("user/create_grouping",array('class'=>'form-horizontal'));               
              $group_id = $this->session->userdata("group_id");
              $user_id  = $this->session->userdata("user_id");
              echo form_hidden("user_id",$user_id);
              ?>
              <table width="100%">
              	<tr>
              		<td colspan="2"><b>Groups Name</b></td>
              		<td><b>Permissions</b></td>
              	</tr>              	
              	<?php $i = 1;               
                      foreach($groups as $row){ ?>
                      <tr>
                        <?php $checked = ($row->id == $group_id) ? true : false; ?>
                        <td><?php echo form_radio('group_id',$row->id, $checked); ?></td>
					  	<td><?php echo $row->name; ?> </td>	
					  	<td><?php echo "{".$row->permissions."}"; ?> </td>					         	            	
              		  </tr>
              	<?php  $i++; }  ?> 
              </table>

                  </div><!-- col-md-6 -->                
                </div><!-- /.box-body -->
                <div class="box-footer" style="padding-left: 30px;">
                    <?php  $prop = array('class'=>'btn btn-sm btn-default','title'=>'Cancel'); ?>					 
					<?php  echo anchor('user/','Cancel',$prop); ?>
                    <?php  echo form_submit('submit', 'Submit','class="btn btn-sm btn-primary"'); ?>
                </div><!-- /.box-footer -->
              <?php echo form_close(); ?>
            </div><!-- /.box -->
          </div>
        </div>
      