<meta name="csrf-token" content="<?php echo $this->security->get_csrf_token_name(); ?>">
<meta name="csrf-hash" content="<?php echo $this->security->get_csrf_hash(); ?>">
<meta name="form-decider" content="form-update">
<script src="<?php echo base_url('assets_/js/limited_bidding.js?'.date('His')) ?>"></script>
<script src="<?php echo base_url() ?>assets/plugins/tagsinput/bootstrap-tagsinput.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/tagsinput/bootstrap-tagsinput.css" />

<style type="text/css">
  .valign-top td{
    vertical-align: top !important;
  }
</style>

<div class="page-header" style="vertical-align: middle">
  <h3 style="display:inline-block">Update Content - Limited Bidding</h3>
</div> 
<?php
if($this->session->flashdata('error_file_upload')) {
echo '<div class="alert alert-warning">';
  echo '<strong><i class="fa fa-exclamation-triangle fa-1x" aria-hidden="true"></i> </strong>'.$this->session->flashdata('error_file_upload');
echo '</div>';
echo '<br>';
}
?>
<div id="debug"></div>
<?php echo form_open_multipart('limited_bidding/update/resendEmail',array('id'=>'form'));$current_date = date('Y/m/d')?>
<?php echo form_hidden('id',$def->id);?>
<table width="100%" border="0">
<tr id="aanwidzing_option" class="valign-top">
  <td><label>Aanwijzing Agenda</label></td><td>:</td>
  <td class="form_tab_content" width="75%;">
    <div style="margin-top: -5px;">
      <ul class="nav nav-tabs test2">
          <li class="active"><a data-toggle="tab" href="#aanwidzing_eng" aria-expanded="true">Limited Bidding Aanwijzing Agenda</a></li>
      </ul>
      <div class="tab-content">
          <!-- <div id="aanwidzing_idn" class="tab-pane fade active in" style="padding:15px;">
            <?php //$data['content'] = $aanwijzing[0]; $data['idaanwidzing']='idaanwidzing_ind'; $this->load->view('announce/aanwidzing',$data); ?>
          </div> -->
          <div id="aanwidzing_eng" class="tab-pane fade active in" style="padding:15px;">
            <?php $data['content'] = $aanwijzing[0]; $data['idaanwidzing']='idaanwidzing_eng'; $this->load->view('announce/aanwidzing',$data); ?>
          </div>
      </div>
    </div>
  </td>
</tr>
<tr>
  <td><label>RFP</label></td><td>:</td>
  <td><?php 
    $datarfp  = array('type'=>'file','name'=> 'rfpfile','id' => 'file_upload_pdf','value'=>'true','content'=>'Import','style'=>$style); 
    echo form_input($datarfp); ?><?php echo $rfpfile; ?>&nbsp;<i>*.pdf</i>
  </td>
</tr>
<tr><td colspan="3"><hr/></td></tr>
</table>
<table width="100%" style="height:80px;">
  <tr>
    <td colspan="3" style="text-align: right;">
      <button style="vertical-align: baseline;" type="submit" value="submit" name="button-process" class="btn btn-primary btn-sm"/>Submit</button>
      <?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>"Cancel", 'style'=>"vertical-align: baseline;"); ?>
      <?php  echo anchor('announce/','Cancel',$prop); ?>
    </td>
  </tr>
</table>
<?php echo form_close(); ?>
<?php echo form_open_multipart('',array('id'=>'attachment-upload'));?>
<div id="dialog-attachment" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Attachment</h4>
      </div>
      <div class="modal-body form-horizontal">
        <div class="form-group">
          <label style="line-height: 35px;" class="col-sm-2" for="attachment-description">Description:</label>
          <div class="col-sm-10">
            <input type="text" name="attachment-description" id="attachment-description" class="form-control"></input>
          </div>
        </div>
        <div class="form-group">
          <label style="line-height: 35px;" class="col-sm-2" for="attachment-remarks">Remarks:</label>
          <div class="col-sm-10">
            <input type="text" name="attachment-remarks" id="attachment-remarks" class="form-control"></input>
          </div>
        </div>
        <div class="form-group">
          <label style="line-height: 35px;" class="col-sm-2" for="attachment-file">Attachment:</label>
          <div class="col-sm-10">
            <div id="attachment-uploader">
            </div>
            <i>*.pdf</i>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary process-winner" id="submit-winner" style="margin-bottom:0;">Submit</button>
        <button type="button" class="btn btn-warning" style="margin-bottom:0;" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<?php echo form_close();?>