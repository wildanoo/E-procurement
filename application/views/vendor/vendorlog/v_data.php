<script>
	$(function(){
		$('#checkall').click(function(){
			$(this).closest('table').find('.ck_ex').prop('checked', this.checked);
		});
	});
</script>
<div id="logs-data">
	<table id="v_tab" class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
		<thead>
			<tr>
				<th width="40px">No</th>
				<th width="30px"><input type="checkbox" id="checkall" value="1"></th>
				<th>Created Date</th>
				<th>Username</th>
				<th>Vendor Name</th>
				<th>Status</th>
				<th>Description</th>
				<th>Reason</th>
			</tr>
		</thead>

		<tbody>
			<?php 
			$i = 1;
			foreach($browse as $row){

				?>
				<tr>
					<td><?php echo $num = $num+1; ?></td>
					<td><input type="checkbox" class="ck_ex" name="ck_list[]" id="ck<?=$i?>" value="<?php echo $row->id ?>"></td>
					<td><?php echo $row->created_date; ?></td>
					<td><?php echo ucfirst($row->username) ?></td>
					<td><?php echo $row->vendor_name ?></td>
					<td><?php echo ucfirst($row->action) ?></td>
					<td><?php echo ucfirst($row->description) ?></td>
					<td><?php echo ucfirst($row->reason) ?></td>
				</tr>
				<?php  
				$i++;
			}
			?>
			<tr>
				<td colspan="8" style="text-align: center;"><?php echo $this->ajax_pagination->create_links(); ?></td>
			</tr>
		</tbody>
	</table>
</div>
