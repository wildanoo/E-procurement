<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";   
	   
   $('#btn_create13').click(function(){
    	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		var values  = $("#form_aff").serializeArray();
				values.push({name: csrf,value: token});
				values = jQuery.param(values); 
						$.ajax({
						    url  : baseUrl + 'vendor/add_affiliate',
						    type : "POST",              		               
						    data : values,
						    success: function(resp){
						        //$('#debug13').html(resp);
						        $("#container13").load(baseUrl+'vendor/load_aff');
						    	
						    }
						});							     	
     }); 
     
   $('#btn_cancel13').click(function(){ 
   		 $("#container13").load(baseUrl+'vendor/load_aff');   		 
     });     
	   
   });

</script>
<div id="debug13"></div>
<?php echo form_open('#',array('id'=>'form_aff')); $current_date = date('Y-m-d'); ?>
<table width="50%">
<tr>
	<td><label>Company Name</label></td><td>:</td>
	<td><input type="text" name="company_name" id="company_name" placeholder="Company Name" class="form-control" required/></td>
	<td id="msg1">&nbsp;</td>	
</tr>
<tr>
	<td><label>Competency</label></td><td>:</td>
	<td><input type="text" name="competency" id="competency" placeholder="Competency" class="form-control" required/></td>
	<td id="msg2">&nbsp;</td>	
</tr>
<tr>
	<td><label>Contact</label></td><td>:</td>
	<td><input type="text" name="contact" id="contact" placeholder="Contact" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Remark</label></td><td>:</td>
	<td><input type="text" name="remark" id="remark" placeholder="Remark" class="form-control" required/></td>
</tr>
<tr>
	<td colspan="3">
		<input type="button" id="btn_cancel13" value="Cancel" class="btn btn-default btn-sm"/>
		<input type="button" id="btn_create13" value="Submit" class="btn btn-primary btn-sm"/>		
	</td>	
</tr>
</table><br/><br/>
<?php echo  form_close(); ?>


