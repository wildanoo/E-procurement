<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){	 
	 $(function(){  });
		var baseUrl = "<?php echo base_url(); ?>";
		
		$('#status').change(function(){ 
			var reg_status = $('#status').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'reg_status/set_sess_status',			   
               data : 'field=id&status='+reg_status,
               success: function(data){ 
                   //alert(category);	
					 //$('#debug').html(resp);
					 //dialogRef.close();				 	
				 	document.location.href= baseUrl + 'reg_status/';
			   }		   
            });	
		
		});
		$("#srch-term").autocomplete({
			source: baseUrl + 'reg_status/get_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
	        var search = $("#srch-term").val(); // alert(search);
	        var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
	        $.ajax({
	           url  : baseUrl + 'reg_status/set_sess_search',
	           type : "POST",              		               
	           data : csrf +'='+ token +'&search='+search,
	           success: function(resp){    $("#debug").html(resp);            		            
				    document.location.href= baseUrl + 'reg_status/';
	           }
	        });
	    });
	    	 
	 $('#btn_create').click(function(){ 
	 	  form_create();
	 	  //alert('test');
	 });	
});

	function form_create(){
		var baseUrl = "<?php echo base_url(); ?>";		 
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		BootstrapDialog.show({
		title: 'Create Register Status',
        message: $('<div></div>').load(baseUrl + "reg_status/form_create/"),
        buttons: [{ label: 'Submit',
	                cssClass: 'btn btn-primary btn-sm',		               
	                action: function(dialogRef){
	                	 var status  = $("#status").val(); 
	                	 
	                	 $.post( baseUrl + 'reg_status/is_exist', { csrf: token , status : status })
						 .done(function( resp ) {
//								 $('#debug').html(resp);
//								 dialogRef.close(); 
								var json = $.parseJSON(resp);
								if (json.status=='true'){
				                	var values = $("#form").serializeArray();
				                	values.push({ name: csrf,value: token });
									values = jQuery.param(values);
									 $.ajax({
							               url  : baseUrl + 'reg_status/create',
							               type : "POST",              		               
							               data : values,
							               success: function(resp){
							               		document.location.href= baseUrl + 'reg_status/';	               				               		  
									       }
							            });   
								} else { 	
									 $("#msg1").html(json.msg1); 
							}
	               		 });  
	                }
	            }, {
	            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
			                action: function(dialogRef) {
			                	document.location.href= baseUrl + 'reg_status/';
			                    dialogRef.close(); 
			                }
		        }]
    });
}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Register Status',
            message: $('<div></div>').load(baseUrl + "reg_status/form_update/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef)
		                {
		                	 var status  = $("#status").val(); 
		                	 
		                	 $.post( baseUrl + 'reg_status/is_exist', { csrf: token , status : status })
							 .done(function( resp ) {
//									 $('#debug').html(resp);
//									 dialogRef.close(); 
									var json = $.parseJSON(resp);
									if (json.status=='true'){
					                	var values = $("#form").serializeArray();
					                	values.push({ name: csrf,value: token });
										values = jQuery.param(values);
										 $.ajax({
								               url  : baseUrl + 'reg_status/update',
								               type : "POST",              		               
								               data : values,
								               success: function(resp){
								               		document.location.href= baseUrl + 'reg_status/';	               				               		  
										       }
								            });   
									} else { 	
									 $("#msg1").html(json.msg1); 
								}
		               		 });  
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'reg_status/';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>
<script src="<?php echo base_url()?>assets/js/sorttable.js"></script>
</head>
<body>
<div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Register Status</h3>
              </div><!-- /.box-header -->
                <br>
                <!-- div class="col-md-2 pull-left">
                    <input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Reg Status" value="New Reg Status" style="width:150px;" title="Create New Reg Status"/>
					<?php //$msg = $this->session->flashdata('message');
						//if(strpos($msg,'failed')) $color="#bc0505";
						//else $color = "#487952"; ?>	
				<font color='<?php //echo $color; ?>'><i><? //echo $msg; ?></i></font>
                </div> -->
                <?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
				<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>

                <div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('reg_status/search',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>reg_status" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		        </div>
		        </div>
		        
        <div class="box-body">
<table border="1" id="example2" class="table table-bordered table-hover sortable" cellspacing="0" width="80%" style="font-size: 13px">
	<thead>
		<tr>
            <th class="table-sorted-harvest">No</th>
            <th class="">Status</th>
            <th class="">Created Date</th>
            <th class="">Updated Date</th>
            <th class="">Updated By</th>
            <th>Action</th>
        </tr>
     </thead>
     <tbody>
 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td><?php echo $num = $num+1; ?></td>
			<td><?php echo $row->status; ?></td>
 			<td align="center"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
 			<td align="center"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>	
 			<td align="center"><?php echo $row->created_id; ?></td>					
				<td><?php				
					echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete status ?')){ return true; } else {return false; }";
					//echo anchor('reg_status/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Status','onclick'=>$js));?>
				</td>					
		</tr>
<?php	$i++;  } ?>	
</table>
<tr><div colspan="9" style="text-align: center;"><?php echo $pagination;?></div></tr>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>  
	</table>  
                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->
     
