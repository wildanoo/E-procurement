<?php
class crud extends CI_Model{	
 function __construct(){
     parent::__construct();
     $this->load->database();	
 }  
 
 function insert($db="",$table,$data){ 
		if(!$db)$db="db";
		$this->$db->db_select();		
		$this->$db->insert($table,$data);
		$insert_id = $this->$db->insert_id();
		//$result = $this->$db->affected_rows();
		return $insert_id;
	}

 function update($db,$table="",$field="",$id,$data){
		if(!$db)$db="db";
		$this->$db->db_select();		
		$this->$db->where($field, $id);
		$this->$db->update($table, $data);
		//echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>'; 
 } 

 function delete($db,$table="",$field="",$id){
		    if(!$db)$db="db";
			$this->$db->db_select();
			$this->$db->delete($table, array($field => $id));
 }
 
 function multiple_insert($db,$table="",$data){	
		if(!$db)$db="db";
		$this->$db->db_select();
		$this->$db->insert_batch($table,$data);
		//echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>';
 }

 function multiple_update($db,$table,$where,$data){
		    if(!$db)$db="db";
		    $this->$db->db_select();	
			$this->$db->where($where);
			$this->$db->update($table, $data);
			//echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>'; 
 } 

 function multiple_delete($db,$table="",$where1="",$where2=""){
		    if(!$db)$db="db";
		    $this->$db->db_select();	
			if($where1) foreach($where1 as $key=>$val){ $this->$db->where($key, $val); }
			if($where2) $this->$db->where($where2);
			$this->$db->delete($table);	
 }
 
 function browse($db="",$table,$field="",$id="",$multi="",$select="",$where="",$order="",$group_by="",$like=""){
		if(!$db)$db="db";
		$this->$db->db_select();
		if($where)$this->$db->where($where);	
		if(!$select){ $select ="*"; } $this->$db->select($select);
		if($like) $this->$db->like($like['col'],$like['field']);	
		if($order)$this->$db->order_by($order['field'], $order['sort']);	
		if($group_by)$this->$db->group_by($group_by);	
		if($id || $id!="") {
			if($multi=="true"){ $result = $this->$db->get_where($table, array($field => $id))->result(); 
			} else { $result = $this->$db->get_where($table, array($field => $id))->row();}
		} else 	{ $result = $this->$db->get($table)->result();	}
		//echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>';
		return $result;	
	}	

 function browse_with($db="",$table,$where1="",$where2="",$select="",$multi="",$group_by="",$order=""){
		if(!$db)$db="db";
		$this->$db->db_select();
		if(!$select){ $select ="*"; } $this->$db->select($select);		
		if($order)$this->$db->order_by($order['field'], $order['sort']);
		if($group_by)$this->$db->group_by($group_by);		
		if($where1){  foreach($where1 as $key=>$val){ $this->$db->where($key, $val); } }
		if($where2){  $this->$db->where($where2); }
		if($multi=="true"){  $result = $this->$db->get($table)->result();			
			} else { $result = $this->$db->get($table)->row();	}
		//if($this->session->userdata('id')==1){ echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>'; }
		return $result;
	}
 
 function browse_range($db="",$table,$where,$select="",$multi="",$group_by="",$order=""){  //echo $db."/".$table."/".$where;
		if(!$db)$db="db";
		$this->$db->db_select();
		if(!$select){ $select ="*"; } $this->$db->select($select);
		if($order)$this->$db->order_by($order['field'], $order['sort']);
		if($group_by)$this->$db->group_by($group_by);			
		$this->$db->where($where);		
		if($multi=="true"){  $result = $this->$db->get($table)->result();			
		} else { $result = $this->$db->get($table)->row();	} 
		//if($this->session->userdata('id')==1){ echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>'; }
		return $result;		
	}

 function browse_distinct($db,$table,$field="",$where=""){ 
 	
 	    if(!$db)$db="db";    	
		$this->$db->db_select();		     	
    	$this->$db->distinct();
    	$this->$db->select($field);	
	    	    	    
	    foreach($where as $key=>$val){ $this->$db->where($key, $val); } 	    
	    $result = $this->$db->get($table)->result();
	    //echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>';
	    return $result;    
 }

 function autocomplete($db,$table,$select,$field,$like,$where="",$order="",$limit=5){
 	
 	if(!$db)$db="db";
	$this->$db->db_select();
	$this->$db->select($select);	
	$this->$db->like($field, $like); 
	if($where) $this->$db->where($where);
	if($order) $this->$db->order_by($order['field'], $order['sort']);
	$this->$db->limit($limit);
	$result = $this->$db->get($table)->result();
	//echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>';
	//return $result; 	//print_r($result);
	foreach($result as $key=>$value){ $return[] = $value->name;	}
	
	return $return;		
 } 

 function summ_field($db,$table,$field,$and_where="",$or_where=""){ 
    	if(!$db)$db="db";   	
		$this->$db->db_select();		     	
    	$this->$db->select_sum($field);
	    $this->$db->from($table);	    	    
	    if($and_where) foreach($and_where as $key=>$val){ $this->$db->where($key, $val); }
	    if($or_where) $this->$db->where($or_where);
	    /*if($or_where) {
			foreach($or_where as $value){ 
	    	foreach($value as $key=>$val){ $this->$db->or_where($key, $val);  } }
		} */	    
	    $query = $this->$db->get();
	    $total_part = $query->row()->$field; 
		/*if ($this->session->userdata('id')==1) { 
			echo '<pre>'; print_r($this->$db->last_query()); echo '</pre><br/>';
		}*/
	    if ($total_part > 0){ return $total_part; }
	    return NULL;  
		
 }

 function dummy($filename,$data){ //print_r($data); exit;
	    
		header("Content-Disposition: attachment; filename=\"$filename\"");
	  	header("Content-Type: application/vnd.ms-excel");	
	  	if($data){
			foreach($data as $rows) { 
		  	 	foreach($rows as $key=>$val){ $row[$key] = $val; } 
		  	 	$result[] = $row;	  	 			  	 		 
			 } 
		} else { $result = array(); }
  	 
	  $flag = false;
	  foreach($result as $rowss) {   	
		    if(!$flag) { 
		      echo implode("\t", array_keys($rowss)) . "\r\n";
		      $flag = true;
			} 			
		echo implode("\t", array_values($rowss)) . "\r\n"; 
	  }
	  exit;
  }   

 function send_message($id, $message, $progress=""){
    $d = array('message' => $message , 'progress' => $progress);
      
    echo "id: $id" . PHP_EOL;
    echo "data: " . json_encode($d) . PHP_EOL;
    echo PHP_EOL;
      
    ob_flush();
    flush();
 }  

 function grouping_objdata($data,$key){		
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$grouped = array();
		foreach ($data as $item) {  
			$grouped[$item->$key][] = $item;	
		}
		
		return $grouped;		
	} 

 function is_exist($db,$table,$field,$where){
 	if(!$db)$db="db";
	$this->$db->db_select();
	
	foreach($where as $key=>$value){	
		$param[]  = "`$key`='$value'";									
	}
	
	if(count($param)>1){
		$AND  = implode(" AND ",$param);   unset($param);
	} else { $AND = $param[0]; }

	$state  = "WHERE $AND ";
	$query  = "SELECT EXISTS(SELECT $field FROM $table $state) as jum ";  //echo $query; exit;
	$result =  $this->$db->query($query); 
	$hasil  =  $result->row()->jum;	
	//echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>';
	if ($hasil > 0){ return true;
    } else {  return false;  }	
	
 }  

 function get_field($db,$table,$filter_field=""){
		if(!$db)$db="db";
		$this->$db->db_select();
		$result = $this->$db->list_fields($table);
	
		if(!$filter_field){ $filter_field = array(); }
		foreach($result as $field){
			if(!in_array($field, $filter_field))$data[] = $field; 	
		}
	return $data;
   }

 function month_to_num($txt_month){
 	
	 	$num_month = array( 'Jan'	=>	'01',
							'Feb'	=>	'02',
							'Mar'	=>	'03',
							'Apr'	=>	'04',
							'May'	=>	'05',
							'Jun'	=>	'06',
							'Jul'	=>	'07',
							'Aug'	=>	'08',
							'Sep'	=>	'09',
							'Oct'	=>	'10',
							'Nov'	=>	'11',
							'Dec'	=>	'12');
					
	return $num_month[$txt_month];

 } 
 
 function array_sort_by_column($arr, $col, $dir = SORT_ASC) {
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$sort_col = array();
		foreach ($arr as $key=> $row) {
			$sort_col[$key] = $row[$col];
		}
		array_multisort($sort_col, $dir, $arr);
		
		return $arr;
	 }

 function set_offset($page,$per_page){
	if(!$page){ $offset=0; 
	} else { 				 
		 $dumm1=$page*$per_page;
		 $dumm2=($per_page);
		 $offset=$dumm1-$dumm2;}
		 
	return $offset;
 }
 
 function get_total_record($db,$table,$where="",$group_by=""){ 
	if(!$db)$db="db";
	$this->$db->db_select();
	if($where){ $this->$db->where($where); } 
	if($group_by){ $this->$db->group_by($group_by); }
	
	return $this->$db->get($table)->num_rows();
 }

 function set_config($set_config){
	foreach($set_config as $key=>$val){			
		$config[$key] = $val ; 
	}
	return $config;
 }
	
 function set_paging($config,$page,$per_page){
	 if(!$page)$page=1; $link= array();
	 $mod = $config["total_rows"] % $per_page; 
	 if($mod==0){ $num_rows = $config["total_rows"]; $add=0; } else { $num_rows = $config["total_rows"]-$mod; $add=1; }
	 $set_page 	 = $num_rows / $config["per_page"] + $add;
	 $info 		 = "<td> Displayed $page from ".$set_page."</td>";
	 $return	 = array('info'=>$info) ;
	 return $return;
 }
	
 function pagination($db,$table,$per_page,$base_url,$uri="",$where="",$group="",$total=""){
	if(!$db)$db="db";
	if(!$uri)$uri = "3";
	$page       = $this->uri->segment($uri);
	$offset     = $this->set_offset($page,$per_page);
	$total_rows = !$total ? $this->get_total_record($db,$table,$where,$group) : $total; 
	$set_config = array('base_url'=> base_url().'hpp/'.$base_url.'/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri);
	$config     = $this->set_config($set_config);	
	$set_paging = $this->set_paging($config,$page,$per_page);
	$this->pagination->initialize($config);
	$paging 	= $this->pagination->create_links();
	$halaman    = $paging.$set_paging['info']; 	 
	
	$result  	= array('page'=>$page,'halaman'=>$halaman,'per_page'=>$config['per_page'],'offset'=>$offset); 
	return $result;
 }
 
 function browse_with_paging($db="",$table,$field="",$id="",$multi="",$select="",$where="",$order_by="",$num="",$offset="",$group_by=""){  
	if(!$db)$db="db";
	$this->$db->db_select();	
	if($where)$this->$db->where($where);
	if($order_by)$this->$db->order_by($order_by['field'], $order_by['order']);
	if($group_by){ $this->$db->group_by($group_by); }
	if(!$select){ $select ="*"; } $this->$db->select($select);			
	if($id || $id!="") {
		if($multi=="true"){ $result = $this->$db->get_where($table, array($field => $id))->result(); 
		} else { $result = $this->$db->get_where($table, array($field => $id))->row();}
		} else 	{ $result = $this->$db->get($table,$num,$offset)->result();	}
	//if($sts){ 
	//echo '<pre>'; print_r($this->$db->last_query()); echo ';</pre>'; //} //exit;
	return $result;	
 }

 function check_exist_table($db,$tableName){
 		if(!$db)$db="db";   	
        $result = $this->$db->list_tables();	

        foreach( $result as $row ) {
            if( $row == $tableName )    return true;
        }
        return false;     
  }

 function sendMail($to,$subject,$message){
  $config = Array('protocol'  => 'smtp',
				  'smtp_host' => 'ssl://smtp.gmail.com',
				  'smtp_port' => 465,
				  'smtp_user' => '', // change it to yours
				  'smtp_pass' => '', // change it to yours
				  'mailtype'  => 'html',
				  'charset'   => 'iso-8859-1',
				  'wordwrap'  => TRUE );

      $this->load->library('email', $config);
      $this->email->set_newline("\r\n");
      $this->email->from('admin@eproc.com'); 
      $this->email->to($to);
      $this->email->subject($subject);
      $this->email->message($message);
      if($this->email->send()){ $result = 'Email sent.';
      }  else  {  $return = show_error($this->email->print_debugger()); }
      
	return $result;
}

 function register_number($table,$chr="REG"){
 	
 	$Mdate = date("m-d"); $year =  date('Y'); $yearCode =substr($year,2,4);
 	$dateNow = $yearCode.$Mdate;
 	$dateNow = str_replace("-","", $dateNow);
 	$counter = $this->get_total_record("",$table); 
 	if($counter==0){ $counter=1; } else { $counter= $counter+1;	} 
 	if(strlen($counter)==1){ $pre='00';	} elseif(strlen($counter)==2){ $pre='0'; } else { $pre='';	 }	
 	$seq = $pre.$counter;
 	$reg_num = $dateNow.'-'.$chr.$seq;
 	
 	return $reg_num; 	
 } 

 function supplier_number($id_cat){
 	
 	$init_code = $this->browse("","supplier_category","id",$id_cat,"false","id,init_code")->init_code;
 	$where     = array('id_category'=>$id_cat);
 	$counter   = $this->get_total_record("","supplier",$where); 	 	
 	$counter   = $counter==0 ? 1 : $counter+1;
 	if(strlen($counter)==1){ $pre='00';	} elseif(strlen($counter)==2){ $pre='0'; } else { $pre='';	 }
 	$reg_num   = $init_code.'.'.$pre.$counter;
 	
 	return $reg_num; 	
 } 
 
 function auto_complete($db,$table,$select="",$field="",$like="",$where=""){
		if(!$db)$db="db";
		$this->$db->db_select();		
		$query  ="SELECT $select FROM `$table` WHERE $field LIKE '%".$like."%' $where GROUP BY $field LIMIT 5";	
		$result = $this->$db->query($query); 
		return $result->result();	
	}

 function sendemail($from,$to,$subject,$msg){
  $config = Array("protocol"  => "smtp",
				  "smtp_host" => "ssl://smtp.googlemail.com",
				  "smtp_port" =>  465,
				  "smtp_user" => "",
				  "smtp_pass" => "" );
	
  	  $this->load->library("email", $config);
	  $this->email->set_newline("\r\n");
	  $this->email->from($from['email'], $form['name']);
	  $this->email->to($to);
	  $this->email->subject($subject);
	  $this->email->message($msg);
	  if (!$this->email->send()) {   $result = show_error($this->email->print_debugger()); }
	  else {   $result = "Your e-mail has been sent!";  }
	  
	  return $result;
}

 function hash_password($str){
 	$passwd = sha1($str);	return $passwd;
 }


}
?>