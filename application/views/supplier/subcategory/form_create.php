<script type="text/javascript"> 	
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";		 
	  $(function(){  });
	  
	  $('#id_cat').change(function(){  
   		var id_cat = $('#id_cat').val(); 
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
           $.ajax({
               url  : baseUrl + 'category/get_subcategory',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_cat='+id_cat,
               success: function(resp){
               	  $('#id_subcat').html(resp);
               }
            });
     });
});	  
</script>
<?php echo form_open('#',array('id'=>'form')); ?>
<table cellpadding="2" cellspacing="2">
<tr>
	<td><label>Category</label></td><td>:</td>
	<td><?php 
		$prop_category = 'class="form-control" id="id_cat" style="width:220px"';
		echo form_dropdown('id_cat',$category,'default',$prop_category); ?>			
	</td>
	<td id="msg1"></td>	
</tr>
<tr>
	<td><label>Subcategory</label></td><td>:</td>
	<td><input type="text" name="subcategory" id="subcategory" placeholder="Subcategory" class="form-control" required/></td>	
	<td id="msg2"></td>		
</tr>
<tr>
	<td><label>Description</label></td><td>:</td>
	<td><input type="text" name="remark" id="remark" placeholder="Description" class="form-control" required/></td>	
	<td id="msg3"></td>		
</tr>
</table>