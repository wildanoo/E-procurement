<div id="container11">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   
    $('#btn_cancel11').click(function(){ 
   		var id_visit = $("#id_visit").val();
   		$("#container11").load(baseUrl+'due_diligence/view_category/'+id_visit);   		
   }); 	
   
    $('#btn_save11').click(function(){ 
	 	var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	    
	    var values   = $("#form_assest").serializeArray();
		values.push({name: csrf,value: token});
		values = jQuery.param(values);
		 	$.post( baseUrl+ "due_diligence/save_assesment", values )
			  .done(function( resp ) {
			    	var id_visit = $("#id_visit").val();
   					$("#container11").load(baseUrl+'due_diligence/view_category/'+id_visit); 
			});
     }); 
     
    $('#btn_add11').click(function(){     	
     	 $("#container11").load(baseUrl+'due_dilligence/form_category');     	
    }); 
     
    $('#btn_conc11').click(function(){     	
     	$("#container11").load(baseUrl+'due_dilligence/form_conclutions');     	
    });    	
   
 });  
  
 </script>
<div id="debug11"></div>
<div style="text-align: right;">
<button type="button" id="btn_cancel11" class="btn btn-default"><i class="fa fa-backward"></i>&nbsp;&nbsp;Previous</button>
<?php if($this->permit->md3){ ?>
	<button type="button" id="btn_save11" class="btn btn-info btn-primary">Submit</button></div>
<?php } ?>

<?php echo form_open('#',array('id'=>'form_assest')); ?> 
<input type="hidden" name="id_visit" id="id_visit" value="<?php echo $id_visit;  ?>"/>
<input type="hidden" name="id_subcat" id="id_subcat" value="<?php echo $id_subcat;  ?>"/>
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Description</th>
		<th>Result</th>		
		<th>Remark</th>		
	</tr>
	</thead>
	<tbody>
 <?php  
 $venID   = $this->session->userdata("venID");  
 if($assesment){ $i=1;
 	foreach($assesment as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->description; ?></td>
			<td><?php 			
				$where    = array('id_vendor'=>$venID,'id_assesment'=>$row->id,'id_visit'=>$id_visit,'id_subcat'=>$id_subcat);
				$db       = $this->crud->browse("","dd_vdr_assesment","id_vendor",$venID,"false","result,remark",$where);
				$none     = array('result'=>'0','remark'=>'none');
				$default  = !$db ? (object) $none : $db; 
				$checked1 = $default->result=="1" ? "checked" : "";
				$checked2 = $default->result=="0" ? "checked" : "";		?>
				<input type="radio" name="<?php echo "asst_".$row->id; ?>" value="1" <?php echo $checked1; ?>> Yes &nbsp;&nbsp;
				<input type="radio" name="<?php echo "asst_".$row->id; ?>" value="0" <?php echo $checked2; ?>> No
			</td>
			<td>
				<input type="text"  name="<?php echo "rmk_".$row->id; ?>" class="form-control" placeholder="fill remark here"  value="<?php echo $default->remark; ?>"/>
			</td>
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="8">-</td></tr>
<?php } ?>
     </tbody>
</table>
<?php echo form_close(); ?>

</div>