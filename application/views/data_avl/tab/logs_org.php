
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/bootstrap.min.css" />

<script src="<?php echo base_url() ?>assets_/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/datatables.min.js"></script>
<script>
	$(function() {
		$('#tabel').dataTable({"info":false, "searching":false,"lengthChange":false});
	});
</script>	
<div class="panel panel-info">
	<div class="panel-heading">
		Vendor Category Logs
	</div>
	<div class="panel-body">
		<div id="logs-data">
		
		<table class="table table-hover table-bordered">
		<thead>
		<tr>
		<th colspan="7">Logs Data Changes [Update Data] </th>
		</tr>
		<tr>
		<th>No</th>
		<th>Data</th>
		<th>Before</th>
		<th>After</th>
		<th>Created By</th>
		<th>Created Date</th>
		<th>Status</th>
		</tr>
		</thead>
		<tbody>
		<?php $no=$offset+1; foreach ($logs as $key => $value) { ?>
		<tr>
		<td><?=$no?></td>
		<td><?=$value->fieldname?></td>
		<td><?=$value->before?></td>
		<td><?=$value->after?></td>
		<td><?=$value->username?></td>
		<td><?=$value->created_date?></td>
		<td><?=ucfirst($value->status)?></td>
		</tr>
		<?php $no++; } ?>
		
		</tbody>
		</table>
		<?php echo $this->ajax_pagination->create_links(); ?>
		

		<br>
		<br>
		<br>
		<hr>

		<table id="tabel" class="table table-hover table-bordered">
			<thead>
				<tr>
					<th colspan="8">Logs Data Changes [Add and Delete Data] </th>
				</tr>
				<tr>
					<th>No</th>
					<th>Name</th>
					<th>Address</th>
					<th>Phone</th>
					<th>Position</th>
					<th>Remark</th>
					<th>Action</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$no=1;
					foreach ($logs_temp as $logs) 
					{
						switch ($logs->status) {
							case '0':
								$result = 'Request';
								break;
							case '1':
								$result = 'Approved 1';
								break;
							case '2':
								$result = 'Approved 2';
								break;
							default:
								$result = ucfirst($logs->status);
								break;
						}
						echo '<tr>';
						echo '<td>'.$no.'</td>';
						echo '<td>'.$logs->name.'</td>';
						echo '<td>'.$logs->address.'</td>';
						echo '<td>'.$logs->phone.'</td>';
						echo '<td>'.$logs->position.'</td>';
						echo '<td>'.$logs->remark.'</td>';
						echo '<td>'.ucfirst($logs->approval).'</td>';
						echo '<td>'.$result.'</td>';
						echo '</tr>';
						$no++;
					}


				 ?>
			</tbody>
		</table>
		</div>
	</div>
	<div class="panel-footer">
		
	</div>
</div>


