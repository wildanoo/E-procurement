<!DOCTYPE HTML>
<html lang="en-US">
<?php $this->load->view('layout/header_default'); ?>
</head>
<body>
<div id="debug"></div>

	<!-- sidebar english -->
	<div id="wrapper1" class="st-container">
		<nav class="st-menu st-effect-1" style="height:100%;background-color:#005291;position:fixed;">
			<?php $this->load->view('auth/login_form'); ?>
		</nav>
	</div>
	<div id="wrapper2" class="st-container">
		<nav class="st-menu st-effect-2" style="height:100%;background-color:#ededea;position:fixed;overflow-y: scroll;overflow-x: hidden;">	  
			<?php $this->load->view('vendor/register'); ?>
		</nav>
	</div>
	<!-- sidebar english -->

	<header id="header" class="container-fluid main">
	  <div class="header-bg">
		<div class="container">
			<div class="decoration dec-left visible-lg"></div>
			<div class="decoration dec-right visible-lg"></div>
			<div class="row">
				<div id="logo" class="col-md-5 col-md-offset-1"><a href="login" class="logo" alt="E-Procurement Garuda Indonesia"></a></div>
				<div class="col-md-4 col-md-offset-1">
					<ul class="social-icons">
					<?php if ($this->session->userdata('lang') == 'eng') { ?>
						<li class="lang eng" data="lang-eng" title="English"><a style="top:0 !important;" href="#">English</a></li>
						<li class="lang idn" data="lang-ind" title="Indonesian"><a class="inactive" href="#" id="lang_ind">Indonesian</a></li>
					<?php }elseif ($this->session->userdata('lang') == 'ind') { ?>
						<li class="lang eng" data="lang-eng" title="English"><a class="inactive" href="#" id="lang_eng">English</a></li>
						<li class="lang idn" data="lang-ind" title="Indonesian"><a style="top:0 !important;" href="#">Indonesian</a></li>
					<?php }?>
					</ul>	
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<ul id="menu" class="visible-desktop" style="font-size: 13px;">
						<li><?php echo anchor('auth/login', 'HOME')?></li>
						<!-- li><?php  echo anchor('main/news','News'); ?></li> -->
						<li><?php echo anchor('main/about_us','ABOUT US'); ?></li>
						<li><?php echo anchor('main/faq','FAQ'); ?></li>
						<li id="st-trigger-effects2"><a data-effect="st-effect-2">REGISTRATION</a></li>
						<li id="st-trigger-effects1"><a data-effect="st-effect-1">LOG IN</a></li>
						<li><?php  echo anchor('main/contact_us','CONTACT US'); ?></li>
						<!-- Search Form -->
						<li class="search-form visible-desktop" style="float: right;top: 10px;"\>
							<form method="get" action="#">
								<input type="text" value="Search" class="search-text-box"/>
								<input type="submit" value="" class="search-text-submit"/>
							</form>
						</li>
					</ul>
				</div>
			</div>
			
<div class="row"> 
<div class="col-sm-12 main-slider" style="margin-top:-3px;">	
<div class="row"> 
<div class="col-sm-12 main-slider"> 

<div class=""> 
<div class="ls-layer ls-active" style="width: 1140px; height: 380px; visibility: visible; display: none; left: 0px; right: auto; top: 0px; bottom: auto;"> 
<div class="ls-s1 hidden-xs" style="margin-left: 0px; margin-top: 0px; display: block;"><img alt="" src="<?php echo base_url(); ?>assets/images/gallery/32.png" style="margin-top:4px;"></div> 
<img alt="" src="<?php echo base_url(); ?>assets/images/gallery/32.png" class="ls-s6 visible-xs" style="margin-right: -15px;margin-top: 4px;"><a href="#" class="ls-s3" style="z-index: 4; top: 230px; left: 190px; margin-left: 0px; margin-top: 0px; display: block;"></a> 
<div class="ls-s4 hidden-xs" style="z-index: 5; top: 160px; left: 150px; margin-left: 0px; margin-top: 0px; display: block;"></div> 
<div class="ls-s5" style="z-index: 6; top: 100px; left: 120px; margin-left: 0px; margin-top: 0px; display: block;"></div> 
<div class="col-sm-2 ls-s2" style="z-index: 3; top: 80px; left: 40px; margin-left: 0px; margin-top: 0px; display: block; width:100%;"></div> 
<div class="col-sm-10 ls-bg bg1" style="z-index: 1; right: 0px; width: 910px; height: 380px; margin-left: -451px; margin-top: -190px; left:585px;"></div> 
</div> 
</div> 
<div id="layerslider-container">	
<div id="layerslider" style="width:100%; height:380px;"> 
<!-- <div class="ls-layer ls-active" style="width: 1140px; height: 380px; visibility: visible; display: none; left: 0px; right: auto; top: 0px; bottom: auto;"> 
<div class="ls-s1 hidden-xs" style="margin-left: 0px; margin-top: 0px; display: block;"><img alt="" src="http://localhost/eprocga/assets/images/gallery/32.png"></div> 
<img alt="" src="http://localhost/eprocga/assets/images/gallery/32.png" class="ls-s6 visible-xs" style="top: 0px; left: 912px; margin-left: 0px; margin-top: 0px; display: block;"><a href="#" class="ls-s3" style="z-index: 4; top: 230px; left: 190px; margin-left: 0px; margin-top: 0px; display: block;">READ DETAILS</a> 
<div class="ls-s4 hidden-xs" style="z-index: 5; top: 160px; left: 150px; margin-left: 0px; margin-top: 0px; display: block;">No gifts and gratuities on Idul Fitri</div> 
<div class="ls-s5" style="z-index: 6; top: 100px; left: 120px; margin-left: 0px; margin-top: 0px; display: block;">Prohibition to Accept</div> 
<div class="col-sm-2 ls-s2" style="z-index: 3; top: 80px; left: 35px; margin-left: 0px; margin-top: 0px; display: block;"></div> 
<div class="col-sm-10 ls-bg bg1" style="z-index: 1; right: 0px; width: 910px; height: 380px; margin-left: -451px; margin-top: -190px;"></div> 
</div> --> 
<div class="ls-layer ls-active" style="slidedelay:4000;"> 
<!-- <div class="col-sm-10 ls-bg bg1" style="z-index:1;right:0px;"></span> --> 
<!-- <div class="ls-s1 hidden-xs"><img alt="" src="<?php //echo base_url(); ?>assets/images/gallery/32.png"/></div> --> 
<img alt="" src="<?php echo base_url(); ?>assets/images/gallery/32.png" class="ls-s6 visible-xs" style="top:0px; left:80%; "/> 
<!-- <div class="col-sm-2 ls-s2" style="z-index:3;top:80px; left:35px; "/></span> --> 
<a href="#" class="ls-s3" style="z-index:4;top:230px; left:190px; ">READ DETAILS</a> 
<div class="ls-s4 hidden-xs" style="z-index:5;top:160px; left:150px; ">Of Accepting and Giving Gifts/Presents of Ied Fitri 1437H</div> 
<div class="ls-s5" style="z-index:6;top:100px; left:110px; ">Prohibition</div> 
</div> 
<div class="ls-layer" style="slidedelay:4000;"> 
<!-- <div class="col-sm-10 ls-bg bg1" style="z-index:1;right:0px;"></span> --> 
<!-- <div class="ls-s1 hidden-xs"><img alt="" src="<?php echo base_url(); ?>assets/images/gallery/32.png"/></div> --> 
<img alt="" src="<?php echo base_url(); ?>assets/images/gallery/32.png" class="ls-s6 visible-xs" style="top:0px; left:80%; "/> 
<!-- <div class="col-sm-2 ls-s2" style="z-index:3;top:80px; left:35px; "/></span> --> 
<a href="#" class="ls-s3" style="z-index:4;top:230px; left:190px; ">READ DETAILS</a> 
<div class="ls-s4 hidden-xs" style="z-index:5;top:160px; left:150px; ">Requires iPad Mounting Provider</div> 
<div class="ls-s5" style="z-index:6;top:100px; left:110px; ">Implementation of e-Auction</div> 
</div> 
<div class="ls-layer" style="slidedelay:4000;"> 
<!-- <div class="col-sm-10 ls-bg bg1" style="z-index:1;right:0px;"></span> --> 
<!-- <div class="ls-s1 hidden-xs"><img alt="" src="<?php echo base_url(); ?>assets/images/gallery/32.png"/></div> --> 
<img alt="" src="<?php echo base_url(); ?>assets/images/gallery/32.png" class="ls-s6 visible-xs" style="top:0px; left:80%; "/> 
<!-- <div class="col-sm-2 ls-s2" style="z-index:3;top:80px; left:35px; "/></span> --> 
<a href="#" class="ls-s3" style="z-index:4;top:230px; left:190px; ">READ DETAILS</a> 
<div class="ls-s4 hidden-xs" style="z-index:5;top:160px; left:150px; ">Procurement Company Merchandise</div> 
<div class="ls-s5" style="z-index:6;top:100px; left:110px; ">Garuda Indonesia</div> 
</div> 
</div>	
</div>	
</div> 
</div> 
</div> 
</div>
    </div>
	</header>
	<section id="content" class="container-fluid">
	<div class="container">
		<div class="col-md-12">
			<!-- <div class="col-md-2"></div> -->
			<div class="col-md-10 col-md-offset-1">
				<br>
				<h1 style="margin-top: 20px;text-align: justify;">Welcome to Garuda Indonesia Procurement Online</h1><br>
				<span class="purchase" style="text-align:justify;font-size: 24px;line-height: 34px;">
					<?php $root = $_SERVER['DOCUMENT_ROOT'];
					$path = base_url()."uploads/cms/home_eng.txt";
					if( file_exists($path)) { echo file_get_contents($path);
					} else { echo file_get_contents($path); }	?>
				</span>

				<!-- <span class="purchase" style="text-align:justify;font-size: 24px;line-height: 34px;"><?php  $root = $_SERVER['DOCUMENT_ROOT']; $path = $root."/eproc/uploads/cms/home_ind.txt";
						//if( file_exists($path)) { echo file_get_contents($path);			
						//} else { echo ""; }	?></span> -->

			</div>
			<!-- <div class="col-md-2"></div> -->
		</div>
		</div>
	</div>
	</section>
</div>
<footer class="main-footer footer-line container-fluid text-center">
	Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
</footer>
<?php $this->load->view('layout/footer_default'); ?>
</body>
</html>