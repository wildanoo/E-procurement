<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendorpromote extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_vendorpromote');
	}

	public function index()
	{
		if ($this->m_vendorpromote->authenticate()) {
			$this->remove_sess_sort();
			$this->session->set_userdata('feature', 'vendorpromote');
			$table 			= 'vendor_promote t1';
			$select 		= 't1.id as promID, t1.id_vendor as venID, t1.status as stat_prom, t2.register_num, t2.vendor_num,t2.vendor_name, t2.vendor_type, t2.status';
			$joins[0][0]	= 'vendor t2';
			$joins[0][1]	= 't1.id_vendor = t2.id';
			$joins[0][2]	= 'left';
			$order 			= array('field'=>'t1.last_updated','order'=>'DESC');

			$page 			= $this->uri->segment(3);
			$per_page	 	= 10;
			$offset 		= $this->crud->set_offset($page,$per_page);
			$total_rows		= count($this->crud->browse_join("",$table,"","","true",$select,$joins));
			$set_config 	= array('base_url'=>base_url().'vendorpromote/index/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>'3');
			$config     	= $this->crud->set_config($set_config);

			$this->load->library('pagination');
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();

			$data['pagination'] 	= $paging;
			$data['num']			= $offset;

			$browse 		= $this->crud->browse_join_with_paging("",$table,"","","true",$select,$joins,"",$order,$per_page,$offset,$groupby);

		//cat & subcat
			$detail = array();
			foreach($browse as $row){

				$category_hover = $this->category_hover($row->venID);
				$detail[$row->venID] = $category_hover;

			}

			$data['detail']			= $detail;

			

			$data['browse']	= $browse;
			$data['view'] 	= 'vendor/vendorpromote/browse';

			$this->load->view('layout/template',$data);
		}else{
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/');
		}


	}

	function remove_sess_sort(){
		$search_val = array(
			'search_term'=>'');
		$this->session->set_userdata($search_val);
	}

	function searchprom(){
		if ($this->m_vendorpromote->authenticate()) {
			$search 			= $this->input->post('search_term');
			$search_term 		= (!empty($search)) ? $search : $this->session->userdata('search_term');
			$search_val 		= array('search_term'=>$search_term);

			$this->session->set_userdata($search_val);

			$table 			= 'vendor_promote t1';
			$select 		= 't1.id as promID, t1.id_vendor as venID, t1.status as stat_prom, t2.register_num, t2.vendor_num,t2.vendor_name, t2.vendor_type, t2.status';
			$joins[0][0]	= 'vendor t2';
			$joins[0][1]	= 't1.id_vendor = t2.id';
			$joins[0][2]	= 'left';
			$order 			= array('field'=>'t1.last_updated','order'=>'DESC');
			$where 			= "t2.vendor_name LIKE '%$search_term%'";

			$page 			= $this->uri->segment(3);
			$per_page	 	= 10;
			$offset 		= $this->crud->set_offset($page,$per_page);
			$total_rows		= count($this->crud->browse_join("",$table,"","","true",$select,$joins,$where));
			$set_config 	= array('base_url'=>base_url().'vendorpromote/searchprom/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>'3');
			$config     	= $this->crud->set_config($set_config);

			$this->load->library('pagination');
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();

			$data['pagination'] 	= $paging;
			$data['num']			= $offset;

			$browse 		= $this->crud->browse_join_with_paging("",$table,"","","true",$select,$joins,$where,$order,$per_page,$offset,$groupby);

		//cat & subcat
			$detail = array();
			foreach($browse as $row){

				$category_hover = $this->category_hover($row->venID);
				$detail[$row->venID] = $category_hover;

			}

			$data['detail']			= $detail;

			$data['browse']	= $browse;
			$data['view'] 	= 'vendor/vendorpromote/browse';

			if($this->input->is_ajax_request()){
				$this->load->view('vendor/vendorpromote/v_data',$data);

			}else{
				$this->load->view('layout/template',$data);
			}
		}else{
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/');
		}
	}

	private function category_hover($id,$mode=""){

		$table 			= "vendor t1";
		$select 		= "t1.id,register_num,vendor_num,vendor_name,t1.created_date,(SELECT category FROM category WHERE id=t2.id_category) as category,(SELECT subcategory FROM subcategory WHERE id=t2.id_subcat) as subcategory,vendor_type,t1.`status`,t2.id_subcat";
		$joins[0][0]	= 'vendor_category t2';
		$joins[0][1]	= 't1.id=t2.id_vendor';
		$joins[0][2]	= 'left';
		$joins[1][0]	= 'category t3';
		$joins[1][1]	= 't2.id_category=t3.id';
		$joins[1][2]	= 'left';
		$joins[2][0]	= 'subcategory t4';
		$joins[2][1]	= 't4.id=t2.id_subcat';
		$joins[2][2]	= 'left';

		$where 		= "t2.id_vendor = $id";

		$browse 	= $this->crud->browse_join("",$table,"","","true",$select,$joins,$where);

		$num = 1;
		if(!empty($mode)){
			foreach($browse as $row){
				$cat .= $num.'.'.$row->category.' - '.$row->subcategory.' ';
				$num++;
			}
		}else{
			foreach($browse as $row){
				$cat .= $num.'.'.$row->category.' - '.$row->subcategory.'<br>';
				$num++;
			}
		}
		return $cat;
	}

	function export_excel(){
		$id 			= $this->uri->segment(3);
		$check 			= !$id ? $this->input->post('ck_list') : $id ;

		$filter			= !$id ? implode(',',$check) : $id;

		$table 			= 'vendor_promote t1';
		$select 		= 't1.id as promID, t1.id_vendor as venID, t1.status as stat_prom, t2.register_num, t2.vendor_num,t2.vendor_name, t2.vendor_type, t2.status';
		$joins[0][0]	= 'vendor t2';
		$joins[0][1]	= 't1.id_vendor = t2.id';
		$joins[0][2]	= 'left';
		$order 			= array('field'=>'t1.last_updated','order'=>'DESC');

		$where = "t1.id_vendor IN($filter)";

		if(!empty($check)){
			$browse 			= $this->crud->browse_join("",$table,"","","true",$select,$joins,$where,$order);
			$data['vendorlist'] = $browse;

			$detail = array();
			foreach($browse as $row){
				$category 				= $this->category_hover($row->venID,1);
				$detail[$row->venID]	= $category;
			}

			$data['detail']				= $detail;
			$this->load->view('vendor/vendorpromote/v_excel',$data);

		}else{
			$this->index();
		}

	}

}

/* End of file vendorpromote.php */
/* Location: ./application/controllers/vendorpromote.php */