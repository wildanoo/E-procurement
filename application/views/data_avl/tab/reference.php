<div id="container9">
<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	
   
   $('#btn_create9').click(function(){  
   		$("#container9").load(baseUrl+'data_avl/form_ref');   		
   });             	
   
 }); 
 
 function update_ref(id){
 	var baseUrl = "<?php echo base_url(); ?>";
 	$("#container9").load(baseUrl+'data_avl/form_update_ref/'+id); 
 }
 
 function delete_ref(id){
 	var baseUrl = "<?php echo base_url(); ?>";
 	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
 	
 	if (confirm('Are you sure you want to delete reference?')) {	
		$.post( baseUrl + "data_avl/delete_ref", { csrf: token, id: id } );
		$("#container9").load( baseUrl + "data_avl/load_ref", function(){$('#ref_msg').html('Data akan berubah setelah proses approval disetujui');});	
	}
	
 } 
  
 </script>
<div id="ref_msg" class="bg-warning text-center text-danger"></div>

<?php if($this->permit->upven){ ?>
<div style="text-align: right;">
<button type="button" id="btn_create9" class="btn btn-info btn-primary">Add Ref.</button> </div> <?php } ?>
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Customer Name</th>
		<th>Project</th>
		<th>Point</th>
		<th>Date</th>
		<th>Remark</th>
		<?php if($this->permit->upven){ ?><th>Action</th><?php } ?>
	</tr>
	</thead>
	<tbody>

 <?php  //$browse = array();
 if($ref){ $i=1;
 	foreach($ref as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->cust_name; ?></td>	
			<td><?php echo $row->project; ?></td>
			<td><?php echo $row->point; ?></td>
			<td><?php echo $row->date; ?></td>
			<td><?php echo $row->remark; ?></td>
			<?php if($this->permit->upven){ ?>
			<td style="text-align: center;">
			<?php
					echo '<a href="javascript:void(0)"><i class="fa fa-pencil" onclick="update_ref('.$row->id.')"></i></a> | '; 					
					echo '<a href="javascript:void(0)"><i class="fa fa-trash"  onclick="delete_ref('.$row->id.')"></i></a>';	
			?></td>	
			<?php } ?>										
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>
     </tbody>
</table>
</div>

