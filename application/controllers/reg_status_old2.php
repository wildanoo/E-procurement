<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Reg_status extends Controller {

	function __construct()
	{
		parent::__construct();			
	}	
		
	function index(){
		if ($this->tank_auth->is_logged_in()) {

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			
			//$sess_like  = $this->session->flashdata('like');
			
			$sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
			
				
			$field      = $sess_field != '' ? $sess_field : "status";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
				
			if($sess_cat != ''){	$where = array('status'=>$sess_cat);
			} else { $where = "";	}
			
	 		$table 	    = "register_status";		
			$page       = $this->uri->segment(3);
			$per_page   = 5;		
			$offset     = $this->crud->set_offset($page,$per_page);
			$total_rows = $this->crud->get_total_record("",$table);
			$set_config = array('base_url'=> base_url().'/reg_status/index','total_rows'=>$total_rows,'per_page'=>5,'uri_segment'=>3);
			$config     = $this->crud->set_config($set_config);		
				 
			$this->load->library('pagination');			 
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();			
				
			$order 			     = array('field'=>'created_date','order'=>'DESC');
			$data['pagination']  = $paging;
			$data['num']         = $offset; 
			$select 			 = "id,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
			
// 			if($sess_like){
// 			$like   = array('col'=>'status','field'=>$sess_like);
// 			$browse = $this->crud->browse("",$table." l","","","true",$select,"","","",$like);	} else {
// 			$browse = $this->crud->browse_with_paging("",$table." l","","","true",$select,"",$order,$config['per_page'],$offset); }
			
// 			$data['browse'] = $browse;

			$sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
				
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			$data['browse'] = $browse;
				
			$reg_status = $this->crud->browse("","register_status","","","true","id,status");
			if(!$reg_status) $reg_status = array();
			$select = array(''=>'All');
			foreach($reg_status as $val){ $options[$val->id] = $val->status; }
			$data['reg_status'] = $select + $options;
			
	 		$data['view']	= "register_status/browse"; 		
 			$this->load->view('layout/template',$data);
 		
	 		
		} else {  
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/'); 	 }
	}	
	
	function get_tags(){ 	
	 	$term   = $_GET['term'];  	
	 	$order  = array('field'=>'status','sort'=>'ASC'); $select = "status as name";
	 	$result = $this->crud->autocomplete("","register_status",$select,"status",$term,"",$order);  	
	 	echo json_encode($result);   	 		
	}
	
	function set_sess_search(){
		$like = $_POST['search']; 	
	 	$this->session->set_flashdata('like',$like); 
	}
	
	function set_sess_status(){
	
		$type = $_POST['status'];
		if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
		} else { $this->session->unset_userdata('sess_cat'); }
	
		$type = $_POST['field'];
		if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
		} else { $this->session->unset_userdata('sess_field'); }
	
	}
	
	function is_exist(){
		$reg_status	  = $_POST['status'];
		$is_exist['1']= !$reg_status ? "false" : "true";
		$msg['1']	  = !$reg_status ? "register status required" : "";
		
		if ($is_exist['1']=='true'){
			$where   = array('status'=>$reg_status);
			$checked = $this->crud->is_exist("","register_status","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate register status" : "";
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1']);
	
		echo json_encode($result);
	}
	
	function create(){
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array('id'=>null,'status'=>$_POST['status'],
					  'created_id'=>$userID,'created_date'=>$curr_date,'last_updated'=>$curr_date);
		$id = $this->crud->insert("","register_status",$data);
		$this->session->set_flashdata('message','1 data success insert');
	}
	
	function form_create(){
		$data['data'] = $this->crud->browse("","register_status","","","true","id,status");
 		$this->load->view('register_status/form_create',$data);
	}
	
	function update(){
		$curr_date =  date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data = array('status'=>$_POST['status'],'last_updated'=>$curr_date);
		$this->crud->update("","register_status","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
		redirect('reg_status/','refresh');
	}
	
	function form_update(){
		$id = $this->uri->segment(3);
		$data['data'] = $this->crud->browse("","register_status","id",$id,"false");
 		$this->load->view('register_status/form_update',$data);
 		
	}
	
	function delete(){
	
		$id = $this->uri->segment(3);
		$this->crud->delete("","register_status","id",$id);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('reg_status/','refresh');
	
	}
}