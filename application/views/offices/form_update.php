
<script type="text/javascript"> 	
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";		 
	  $(function(){  });
	  
	  $('#id_country').change(function(){  
   		var id_country = $('#id_country').val(); 
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
           $.ajax({
               url  : baseUrl + 'offices/get_region',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_country='+id_country,
               success: function(resp){
               	  $('#id_region').html(resp);
               }
            });
     });
});	  
</script>

<?php echo form_open('#',array('id'=>'form')); ?>
<table cellpadding="2" cellspacing="2">
<?php echo form_input(array('type'=>'hidden', 'name'=>'id', 'id'=>'id_office','value'=>$def->id)); ?>
<tr>
	<td><label>Office Code</label></td><td>:</td>
	<td><input type="text" name="office_code" id="office_code" value="<?php  echo $def->office_code; ?>" class="form-control" required/></td>
	<td id="msg1"></td>	
</tr>
<tr>
	<td><label>Office Name</label></td><td>:</td>
	<td><input type="text" name="office_name" id="office_name" value="<?php  echo $def->office_name; ?>" class="form-control" required/></td>
	<td id="msg2"></td>	
</tr>
<tr>
	<td><label>Country</label></td><td>:</td>
	<td><?php 
		$prop_country = 'class="form-control" id="id_country" style="width:220px"';
		echo form_dropdown('id_country',$country,$def->id_country,$prop_country); ?>			
	</td>
	<td id="msg3"></td>	
</tr>
<tr>
	<td><label>Region</label></td><td>:</td>
	<td>
		<?php 
		$prop_region = 'class="form-control" id="id_region" style="width:220px"';
		echo form_dropdown('id_region',$region,$def->id_region,$prop_region); ?>				
	</td>
	<td id="msg4"></td>		
</tr>
<tr>
	<td><label>Email</label></td><td>:</td>
	<td><input type="text" name="email" id="email" value="<?php  echo $def->email; ?>" class="form-control" required/></td>
	<td id="msg5"></td>		
</tr>
<tr>
	<td><label>Phone</label></td><td>:</td>
	<td><input type="text" name="phone" id="phone" value="<?php  echo $def->phone; ?>" class="form-control" required/></td>
	<td id="msg6"></td>		
</tr>
<tr>
	<td><label>Fax</label></td><td>:</td>
	<td><input type="text" name="fax" id="fax" value="<?php  echo $def->fax; ?>" class="form-control" required/></td>
	<td id="msg7"></td>		
</tr>
</table>