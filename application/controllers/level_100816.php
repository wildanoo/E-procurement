<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Level extends Controller {

	function __construct()
	{
		parent::__construct();			
	}	
		
	function index(){
		if ($this->tank_auth->is_logged_in()) {

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			
			$sess_like  = $this->session->flashdata('like');
			
	 		$table 	    = "level";		
			$page       = $this->uri->segment(3);
			$per_page   = 10;		
			$offset     = $this->crud->set_offset($page,$per_page);
			$total_rows = $this->crud->get_total_record("",$table);
			$set_config = array('base_url'=> base_url().'/level/index','total_rows'=>$total_rows,'per_page'=>10,'uri_segment'=>3);
			$config     = $this->crud->set_config($set_config);		
				 
			$this->load->library('pagination');			 
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();			
				
			$order 			     = array('field'=>'created_date','order'=>'DESC');
			$data['pagination']  = $paging;
			$data['num']         = $offset; 
			$select 			 = "id,(SELECT name FROM groups WHERE id=id_group) as id_group,(SELECT subtype_name FROM subcontent_type WHERE id=id_subcontent) as id_subcontent,
									(SELECT username FROM users WHERE id=created_id)as created_id,created_date,is_last,last_updated";
			
			if($sess_like){
			$like   = array('col'=>'created_id','field'=>$sess_like);
			$browse = $this->crud->browse("",$table." l","","","true",$select,"","","",$like);	} else {
			$browse = $this->crud->browse_with_paging("",$table." l","","","true",$select,"",$order,$config['per_page'],$offset); }
			
			$data['browse'] = $browse;
	 		$data['view']	= "level/browse"; 		
 			$this->load->view('layout/template',$data);
 		
	 		
		} else {  
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/'); 	 }
	}	
	
	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
	
		//$search_dateranges = array('publish_date',$_POST['start_date'],$_POST['end_date']);
		$search_conditions = array(
				'name'			=> $_POST['search_term'],
				'subtype_name'	=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		// 		$data['category'] 		 = $this->getCategories();
		// 		$data['subcategory'] 	 = $this->getSubcategories();
		// 		$data['subcontent_type'] = $this->getSubcontentTypes();
		// 		$data['status_ref'] 	 = $this->getStatusCondition();
	
	
		$data['view'] = "level/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{
	
		/* get data for table view from database with pagination */
	
		$table 	     = "level t1";
		$select      = "t1.id,(SELECT name FROM groups WHERE id=id_group) as id_group,(SELECT subtype_name FROM subcontent_type WHERE id=id_subcontent) as id_subcontent,
						(SELECT username FROM users WHERE id=t1.created_id)as created_id,t1.created_date,is_last,t1.last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		$joins[0][0] = 'groups t2';
		$joins[0][1] = 't2.id = t1.id_group';
		$joins[0][2] = 'left';
		$joins[1][0] = 'subcontent_type t3';
		$joins[1][1] = 't3.id = t1.id_subcontent';
		$joins[1][2] = 'left';
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'level/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
		$joins[0][0] = 'groups t2';
		$joins[0][1] = 't2.id = t1.id_group';
		$joins[0][2] = 'left';
		$joins[1][0] = 'subcontent_type t3';
		$joins[1][1] = 't3.id = t1.id_subcontent';
		$joins[1][2] = 'left';
		/* get data for table view from database with pagination */
	
		$table 	     = "level t1";
		$select      = "t1.id,(SELECT name FROM groups WHERE id=id_group) as id_group,(SELECT subtype_name FROM subcontent_type WHERE id=id_subcontent) as id_subcontent,
						(SELECT username FROM users WHERE id=t1.created_id)as created_id,t1.created_date,is_last,t1.last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse_join('',$table,"COUNT(t1.id) AS COUNT",$joins,$where,$dateranges,$wherearray);	
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,$where,$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'level/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
		return $data;
	
	}
	
	function get_tags(){ 	
	 	$term   = $_GET['term'];  	
	 	$order  = array('field'=>'level','sort'=>'ASC'); $select = "level as name";
	 	$result = $this->crud->autocomplete("","level",$select,"level",$term,"",$order);  	
	 	echo json_encode($result);   	 		
	}
	
	function set_sess_search(){
		$like = $_POST['search']; 	
	 	$this->session->set_flashdata('like',$like); 
	}
	
	function is_exist(){
		$level_code	  = $_POST['level_code'];
		$level    = $_POST['level'];
		$is_exist['1']= !$level_code ? "false" : "true";
		$is_exist['2']= !$level ? "false" : "true";		
		$msg['1']	  = !$level_code ? "level code required" : "";
		$msg['2']	  = !$level ? "level required" : "";
		
		if ($is_exist['1']=='true' && $is_exist['2']=='true'){
			$where   = array('level_code'=>$level_code, 'level'=>$level);
			$checked = $this->crud->is_exist("","level","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$is_exist['2']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate level code" : "";
			$msg['2']	= $checked ? "duplicate level" : "";
				
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2']);
	
		echo json_encode($result);
	}
	
	function create(){
		$ttl = $_POST['ttl'];
		$group = "";
		$curr_date 	= date('Y-m-d H:i:s');
		$userID = $this->tank_auth->get_user_id();
		for ($i = 1; $i <= $ttl; $i++) {
			$value = $this->input->post('check'.$i);
			if($value) $sub[] = $value;
		} 
		foreach ($sub as $val){
			$group_id = $_POST['id_group'];
			if ($group_id == '4' || $group_id == '5' || $group_id == '10')
			{ $is_last = '1'; } else { $is_last = '0'; }
			$data 		= array('id'			=>null,
								'id_group'		=>$_POST['id_group'],
								'id_subcontent'	=>$val,
								'created_id'	=>$this->tank_auth->get_user_id(),
								'created_date'	=>$curr_date,
								'last_updated'	=>$curr_date,
								'is_last'		=>$is_last,
			);
			$id = $this->crud->insert("","level",$data);
		}
		$this->session->set_flashdata('message','1 data success insert');
		redirect('level/','refresh');
	}
	
	function form_create(){
		$data ['user_id'] = $this->tank_auth->get_user_id();
		$data ['username'] = $this->tank_auth->get_username();
		$select = 'id,(SELECT name FROM groups WHERE id=id_group) as id_group,(SELECT subtype_name FROM subcontent_type WHERE id=id_subcontent) as id_subcontent';
		
		$group = $this->crud->browse("","groups","","","true","id,name");
		if(!$group) $group = array();
		$select_ = array('0'=>'-- Select --');
		foreach($group as $val){ $options_group[$val->id] = $val->name; }
		$data['group'] = $select_ + $options_group;
		
		$data['sub']  = $this->crud->browse("","subcontent_type","","","true","id,subtype_name");
		
		$data['data'] = $this->crud->browse("","level","","","true",$select);
		$data['view'] = 'level/form_create';
 		$this->load->view('layout/template',$data);
	}
	
	function update_(){
		$ttl   		 = $_POST['ttl'];
		
		$curr_date =  date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		
		for ($i = 1; $i <= $ttl; $i++) {
			$value = $this->input->post('check'.$i);
			if($value) $sub[] = $value;
		}
		foreach ($sub as $val){
			$group_id = $_POST['id_group'];
			
			if ($group_id == '4' || $group_id == '5' || $group_id == '10')
			{ $is_last = '1'; } else { $is_last = '0'; }
		$data 		= array('id'			=>null,
							'id_group'		=>$_POST['id_group'],
							'id_subcontent'	=>$val,
							'created_id'	=>$this->tank_auth->get_user_id(),
							'created_date'	=>$curr_date,
							'last_updated'	=>$curr_date,
							'is_last'		=>$is_last,
		);
		}
		$id = $this->crud->insert("","level",$data);
		$this->session->set_flashdata('message','1 data success update');
		redirect('level/','refresh');
	}

	function update(){
		$ttl = $_POST['ttl'];
		$group = "";
		$curr_date 	= date('Y-m-d H:i:s');
		$id  		 = $_POST['id'];
		$this->crud->delete("","level","id",$id);
		$userID = $this->tank_auth->get_user_id();
		for ($i = 1; $i <= $ttl; $i++) {
			$value = $this->input->post('check'.$i);
			if($value) $sub[] = $value;
		}
		foreach ($sub as $val){
			$group_id = $_POST['id_group'];
			if ($group_id == '4' || $group_id == '5' || $group_id == '10')
			{ $is_last = '1'; } else { $is_last = '0'; }
			$data 		= array('id'			=>null,
					'id_group'		=>$_POST['id_group'],
					'id_subcontent'	=>$val,
					'created_id'	=>$this->tank_auth->get_user_id(),
					'created_date'	=>$curr_date,
					'last_updated'	=>$curr_date,
					'is_last'		=>$is_last,
			);
			$id = $this->crud->insert("","level",$data);
		}
		$this->session->set_flashdata('message','1 data success insert');
		redirect('level/','refresh');
}
	
	function form_update(){
		$id = $this->uri->segment(3);
		$data ['user_id'] = $this->tank_auth->get_user_id();
		$data ['username'] = $this->tank_auth->get_username();
		
		$select = 'id,(SELECT id FROM groups WHERE id=id_group) as id_group,(SELECT id FROM subcontent_type WHERE id=id_subcontent) as id_subcontent';
		$def    = $this->crud->browse("","level","id",$id,"false",$select);
		$data['def'] = $def ;
		
		$group = $this->crud->browse("","groups","","","true","id,name");
		if(!$group) $group = array();
		$select_ = array('0'=>'-- Select --');
		foreach($group as $val){ $options_group[$val->id] = $val->name; }
		$data['group'] = $select_ + $options_group;
		$data['sub']  = $this->crud->browse("","subcontent_type","","","true","id,subtype_name");
		$data['data'] = $this->crud->browse("","level","id",$id,"false");
		$data['view'] = 'level/form_update';
 		$this->load->view('layout/template',$data);
 		
	}
	
	function delete(){
	
		$id = $this->uri->segment(3);
	
		$where1 = array('id_group'=>$id);
		$where2 = array('id_subcontent'=>$id);
		$checked1 = $this->crud->is_exist("","groups","id",$where);
		$checked2 = $this->crud->is_exist("","subcontent_type","id",$where);
	
		if ($checked1 || $checked2){
		//if ($checked1){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		} else {
			$this->crud->delete("","level","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
	
		// 		$data = array('status'=>'0');
		// 		$this->crud->update("","region","id",$id,$data);
		redirect('region/','refresh');
	}
}