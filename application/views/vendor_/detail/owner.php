<div id="container4">

<script type="text/javascript">
 $(function () {
   	var baseUrl  = "<?php echo base_url(); ?>";
   	$('#update4').hide();   	
  	
   	$('#change4').click(function(){	
   		
   		$("#owner_name").focus();
   		$("#owner_name").css("background","#ffffff");
	    $("#owner_name").css("color","#827e85");             
	    $("#owner_name").prop('readonly',false);
	    
	    $("#owner_address").css("background","#ffffff");
	    $("#owner_address").css("color","#827e85");             
	    $("#owner_address").prop('readonly',false);
	    
	    $("#owner_phone").css("background","#ffffff");
	    $("#owner_phone").css("color","#827e85");             
	    $("#owner_phone").prop('readonly',false);
	    
	    $("#owner_position").css("background","#ffffff");
	    $("#owner_position").css("color","#827e85");             
	    $("#owner_position").prop('readonly',false);	    
	    
	    $("#owner_shared").css("background","#ffffff");
	    $("#owner_shared").css("color","#827e85");             
	    $("#owner_shared").prop('readonly', false);
	    
	    $("#remark4").css("background","#ffffff");
	    $("#remark4").css("color","#827e85");             
	    $("#remark4").prop('readonly',false);	    
   	
   		$('#update4').show();
   		$('#change4').hide();
   		
   	});
   	
   	$('#update4').click(function(){	  	
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
   	    var values = $("#form_owner").serializeArray();
		values.push({name: csrf,value: token});
		values = jQuery.param(values); 
			$.ajax({
               url  : baseUrl + 'vendor/owner_update',
               type : "POST",              		               
               data : values,
               success: function(resp){
               	  //$('#debug4').html(resp);
               	  $("#container4").load(baseUrl+'vendor/load_owner');
               }
            });
			
   		$('#update4').hide();
   		$('#change4').show();
   	});
	 	   
 });
 
 </script>
 
<div id="debug4"></div>
<?php echo form_open('#',array('id'=>'form_owner')); ?>
<table width="100%">
<tr>
<td><label>Owner Name</label></td><td>:</td>
<td><?php 
	$owner_name = array( 'name'     => "owner_name" ,
						 'id'       => "owner_name", 
						 'class'	=> "form-control", 
						 'readonly' => "true", 
						 'style'    => "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
						 'value'    => ucfirst($owner->owner_name)); 
	 echo form_input($owner_name);	?>
</td>
</tr>
<tr>
<td><label>Owner Address</label></td><td>:</td>
<td><?php 
	$owner_address = array( 'name'  => "owner_address" ,
						 'id'       => "owner_address", 
						 'class'	=> "form-control", 
						 'readonly' => "true", 
						 'style'    => "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
						 'value'    => ucfirst($owner->owner_address)); 
	 echo form_input($owner_address);	?>
</td>
</tr>
<tr>
<td><label>Owner Phone</label></td><td>:</td>
<td><?php 
	$owner_phone = array( 'name'    => "owner_phone" ,
						 'id'       => "owner_phone", 
						 'class'	=> "form-control", 
						 'readonly' => "true", 
						 'style'    => "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
						 'value'    => ucfirst($owner->owner_phone)); 
	 echo form_input($owner_phone);	?>
</td>
</tr>
<tr>
<td><label>Owner Position</label></td><td>:</td>
<td><?php 
	$owner_position = array( 'name'    => "owner_position" ,
						 'id'       => "owner_position", 
						 'class'	=> "form-control", 
						 'readonly' => "true", 
						 'style'    => "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
						 'value'    => ucfirst($owner->owner_position)); 
	 echo form_input($owner_position);	?>
</td>
</tr>
<tr>
<td><label>Owner Shared</label></td><td>:</td>
<td><?php 
	$owner_shared = array('name'     => "owner_shared" ,
						  'id'       => "owner_shared", 
						  'class'	 => "form-control", 
						  'readonly' => "true", 
						  'style'    => "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
						  'value'    => ucfirst($owner->owner_shared)); 
	 echo form_input($owner_shared); ?>
</td>
</tr>
<tr>
<td><label>Remark</label></td><td>:</td>
<td><?php $remark = array( 'name' 		=> "remark4" ,
						   'id'   		=> "remark4", 
						   'class'		=> "form-control", 
						   'readonly'	=> "true", 
						   'style'		=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
						   'value'		=> ucfirst($owner->remark)); 
	 echo form_input($remark);	?></td>
</tr>
<tr><td colspan="3">
	<button type="button" id="change4" class="btn btn-info btn-primary">Change</button> 
    <button type="button" id="update4" class="btn btn-info btn-primary">Update</button>
</td></tr>
</table>
<?php echo  form_close(); ?>
<br/>
</div>





