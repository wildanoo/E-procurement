<div class="page-header">
  <h3>Terms and Conditions</h3>
</div>

<?php echo form_open('cms/change_content',array('id'=>'form')); ?> 

		<td class="form_tab_content" width="75%;">
		<td colspan="2">&nbsp;</td>
			<div>
				<ul class="nav nav-tabs test2">
				    <li class="active"><a data-toggle="tab" href="#idn" aria-expanded="true">Indonesia</a></li>
				    <li class=""><a data-toggle="tab" href="#eng" aria-expanded="false">English</a></li>
				</ul>

				<div class="tab-content">
				    <div id="idn" class="tab-pane fade active in" style="padding:15px;">
				      <?php $data['content'] = $contents[0]; $this->load->view('announce/note',$data); ?>
				    </div>
				    <div id="eng" class="tab-pane fade" style="padding:15px;">
				      <?php $data['content'] = $contents[1]; $this->load->view('announce/note',$data); ?>
				    </div>
				</div>
			</div>
		</td>

<input type="hidden" name="filename" value="terms"/>
<input type="submit" value="Submit" class="btn btn-primary btn-sm"/>

<?php echo form_close(); ?>


	