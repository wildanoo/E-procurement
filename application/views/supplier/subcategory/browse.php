<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){	 
	 $(function(){  });

		var baseUrl = "<?php echo base_url(); ?>";

		$('#init_code').change(function(){ 
			var init_code = $('#init_code').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'category/set_sess_subcategory',			   
               data : 'field=init_code&subcategory='+init_code,
               success: function(data){ 
                   //alert(init_code);	
					 //$('#debug').html(resp);
					 //dialogRef.close();				 	
				 	document.location.href= baseUrl + 'category/subcategory/';
			   }		   
            });	
		
		});

		$('#subcategory2').change(function(){ 
			var subcategory = $('#subcategory2').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'category/set_sess_subcategory',			   
               data : 'field=id&subcategory='+subcategory,
               success: function(data){ 
                   //alert(category);	
					 //$('#debug').html(resp);
					 //dialogRef.close();				 	
				 	document.location.href= baseUrl + 'category/subcategory/';
			   }		   
            });	
		
		});

		$("#srch-term").autocomplete({
			source: baseUrl + 'category/get_subcat_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
	        var search = $("#srch-term").val(); // alert(search);
	        var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
	        $.ajax({
	           url  : baseUrl + 'category/set_sess_search_subcat',
	           type : "POST",              		               
	           data : csrf +'='+ token +'&search='+search,
	           success: function(resp){    $("#debug").html(resp);            		            
				    document.location.href= baseUrl + 'category/subcategory';
	           }
	        });
	    });
	 
	 $('#btn_create').click(function(){ 
	 	  form_create();	 	  
	 });
	 
});

	function form_create(){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
			BootstrapDialog.show({
			title: 'Create Sub Category',
            message: $('<div></div>').load(baseUrl + "category/form_create_subcat/"),       
            buttons: [{			            	
            	label: 'Submit',
                cssClass: 'btn btn-primary btn-sm',		               
                action: function(dialogRef){
                	 var id_cat  = $("#id_cat").val(); 
                	 var subcategory = $("#subcategory").val();
                	 var remark = $("#remark").val();
                	 
                	 $.post( baseUrl + 'category/is_exist_subcat1', { csrf: token , id_cat : id_cat, subcategory : subcategory , remark : remark })
					 .done(function( resp ) {
//							 $('#debug').html(resp);
//							 dialogRef.close(); 
							var json = $.parseJSON(resp);
							if (json.status=='true'){
			                	var values = $("#form").serializeArray();
			                	values.push({ name: csrf,value: token });
								values = jQuery.param(values);
								 $.ajax({
						               url  : baseUrl + 'category/create_subcat',
						               type : "POST",              		               
						               data : values,
						               success: function(resp){
						               		document.location.href= baseUrl + 'category/subcategory';	               				               		  
								       }
						            });   
							} else { 	
							 $("#msg1").html(json.msg1); 	
							 $("#msg2").html(json.msg2);	
							 $("#msg3").html(json.msg3);
						}
               		 });  
                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'category/subcategory';
				                    dialogRef.close(); 
				                }
			        }]
        });
	}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Sub Category',
            message: $('<div></div>').load(baseUrl + "category/form_update_subcat/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){
		                	 var id_cat  = $("#id_cat").val(); 
		                	 var subcategory = $("#subcategory").val();
		                	 var remark = $("#remark").val();
		                	 
		                	 $.post( baseUrl + 'category/is_exist_subcat2', { csrf: token , id_cat : id_cat, subcategory : subcategory  , remark : remark })
							 .done(function( resp ) {
//									 $('#debug').html(resp);
//									 dialogRef.close(); 
									var json = $.parseJSON(resp);
									if (json.status=='true'){
					                	var values = $("#form").serializeArray();
					                	values.push({ name: csrf,value: token });
										values = jQuery.param(values);
										 $.ajax({
								               url  : baseUrl + 'category/update_subcat',
								               type : "POST",              		               
								               data : values,
								               success: function(resp){
								               		document.location.href= baseUrl + 'category/subcategory';	               				               		  
										       }
								            });   
									} else { 	
									 $("#msg1").html(json.msg1); 	
									 $("#msg2").html(json.msg2);	
									 $("#msg3").html(json.msg3);
								}
		               		 });            
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'category/subcategory';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>
<script src="<?php echo base_url(); ?>assets/js/sorttable.js"></script>
</head>

<body>
 <div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Sub Category</h3>
              </div><!-- /.box-header -->

                <br>
                    <?php if($this->session->flashdata('msg_warning')!=null) { ?>
                	<div class="alert alert-warning">
  						<strong>Warning!</strong> <?php echo $this->session->flashdata('msg_warning'); ?>
					</div>
					<?php } ?>                
                <div class="col-md-2 pull-left">
                    <input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Sub Category" value="New Sub Category" style="width:130px;" title="Create New Sub Category"/>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
				<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
                </div>
                
                <div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('category/search2',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>category/subcategory" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		        </div>
		        </div> 
     
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover sortable">
                    <thead>
                      <tr>
                        <!--th><input type="checkbox" name="cba[]" id="cba" /></th-->
                        <th class="sorttable_sorted_reverse">No</th>
                        <th class="">Code</th>
                        <th class="">Category</th>
                        <th class="">Sub Category</th>
                        <th class="">Description</th>
                        <!--th class="">Status</th>-->
                        <th class="">Created Date</th>
                        <th class="">Updated Date</th>
                        <th class="">Updated By</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    
                    <tbody>
<?php
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td><?php echo $num = $num+1;; ?></td>
				<td><?php echo $row->init_code; ?></td>
				<td><?php echo $row->id_cat; ?></td>
				<td><?php echo $row->subcategory; ?></td>
				<td><?php echo $row->remark; ?></td>	
 				<!--td><?php echo $row->status==1 ? "Active" : "Not Active"; ?></td>-->
				<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
				<td><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>
				<td align="center"><?php echo $row->created_id; ?></td>	
				<td><?php				
					echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete subcategory ?')){ return true; } else {return false; }";
					echo anchor('category/delete_subcat/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Subcategory','onclick'=>$js));?>
				</td>
				
			</tr>
<?php	$i++;  } ?>	
</table>
<tr><div colspan="9" style="text-align: center;"><?php echo $pagination;?></div></tr>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>  
	</table>  
                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->

