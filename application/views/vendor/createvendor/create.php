<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/chosen.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sol.css">
<script src="<?php echo base_url() ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/sol.js"></script>
<script>
	$(function(){
		$('#public-vendor-select').searchableOptionList();
		$('#btn_register').click(function(){
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
			var token   = '<?php echo $this->security->get_csrf_hash(); ?>';    
			var values  = $("#form_register").serializeArray();                  
			values.push({name: csrf,value: token}); values = jQuery.param(values);  
			var cek 	= confirm("Anda yakin ingin membuat vendor baru?");
			if(cek){
				$.ajax({
					url  : baseUrl + 'register/validate',
					type : "POST",                                  
					data : values,
					success: function(resp){ 
						var json = $.parseJSON(resp); 
						if(json.status=="false"){
							$('#msg1').html(json.msg1); $('#msg6').html(json.msg6);
							$('#msg2').html(json.msg2); $('#msg7').html(json.msg7);
							$('#msg8').html(json.msg8);
							$('#msg4').html(json.msg4); $('#msg9').html(json.msg9);
							$('#msg5').html(json.msg5); $('#msg10').html(json.msg10);
							$('#msg11').html(json.msg11);                       
						} else {                    
							$.post( baseUrl + 'create_vendor/create', values ).done(function( resp2 ) {
								window.location="<?php echo base_url('register') ?>";
							});                         
						}
					}
				});
			}else{}        
			
		});
	});

</script>
<style type="text/css" media="screen">
	.cinput{width: 100%;margin-top:15px;}
</style>
<div class="box box-info">
	<?php echo form_open('',array('id'=>'form_register','class'=>'form-horizontal')); ?>
	<div class="box-header">
		<h3 class="box-title" style="padding: 5px 0 16px 0;">Create Vendor</h3>
		<div class="box-footer"></div>
		<div class="col-md-12" style="padding: 12px 0 12px 0;">
			<div class="col-md-12">				 
				<div class="panel-group">
					<div class="accordion" id="accordion">
						<div class="accordion-group">
							<div class="accordion-heading">
								<a style="color: #fff;background-color: #368ccc;border-radius: 0px;" aria-expanded="true" class="accordion-toggle target" /*data-toggle="collapse"*/ /*data-parent="#accordion"*/ /*href="#collapse1"*/ ><i class="fa fa-building fa-1x" style="color: #fff;margin: 0 5px;"></i>Company</a>
							</div>
							<div style="" aria-expanded="true" id="collapse1" class="accordion-body collapse in">
								<div class="accordion-inner" style="padding: 0;">             	
									<input name="ttl" value="15" type="hidden">
									<b>
										<style type="text/css" media="screen">
											.sol-selection-container{
												/*margin-top:155px !important;*/
											}
											.sol-inner-container{
												background-color: #fff !important;
												border: 1px solid #ccc !important;
												width: 100% !important;
												left:0px !important;
												top: 0px !important;
												border-radius: 1px;
												height: 36px !important;
											}
										</style>
										<div class="box-body">
											<div class="form-group" style="margin:0px;">
												<input type="text" class="form-control cinput" name="vendor_name" value="" placeholder="Vendor name">
												<div id="msg1" style="color: red;"></div>
												<select class="appt-form-select cinput" id="public-vendor-select" multiple tabindex="6" name="id_subcat[]">
													<?php foreach($category as $group=>$row){ ?>
														<optgroup label="<?php echo $group; ?>">
															<?php foreach ($row as $val){ ?>                                
																<option value="<?php echo $val->id; ?>"><?php echo $val->subcategory; ?></option>                              
																<?php } ?>                                
															</optgroup>
															<?php } ?>
														</select>
														<div id="msg4" style="color: red;"></div>
														<input type="text" class="form-control cinput" name="vendor_address" value="" placeholder="Supplier Address">
														<div id="msg2" style="color: red;"></div>
														<input type="text" class="form-control cinput" name="phone" value="" placeholder="Phone Number">
														<div id="msg6" style="color: red;"></div>
														<input type="text" class="form-control cinput" name="fax" value="" placeholder="Fax Number">
														<div id="msg7" style="color: red;"></div>
														<input type="text" class="form-control cinput" name="email" value="" placeholder="Email Address">
														<div id="msg8" style="color: red;"></div>
														<select class="form-control cinput" name="overseas" id="overseas">
															<option value="0">Indonesia</option>
															<option value="1">Others</option>														    
														</select>
														<input type="text" class="form-control cinput" name="npwp" value="" placeholder="NPWP">
														<div id="msg9" style="color: red;"></div>
													</div>
												</div>
											</b>
										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<a style="color: #fff;background-color: #368ccc;border-radius: 0px;" aria-expanded="true" class="accordion-toggle target" /*data-toggle="collapse"*/ /*data-parent="#accordion"*/ /*href="#collapse2"*/ ><i class=" glyphicon glyphicon-phone-alt" style="color: #fff;margin: 0 5px;"></i>Contact Person</a>
									</div>
									<div style="" aria-expanded="true" id="collapse2" class="accordion-body collapse in">
										<div class="accordion-inner" style="padding: 0;">             	
											<input name="ttl" value="15" type="hidden">
											<b>
												<div class="box-body">
													<div class="form-group" style="margin:0px;">
														<input type="text" class="form-control cinput" name="cp_name" value="" placeholder="Contact Person">
														<div id="msg5" style="color: red;"></div>
														<input type="text" class="form-control cinput" name="cp_mobile" value="" placeholder="Mobile Phone">
														<div id="msg10" style="color: red;"></div>
														<input type="text" class="form-control cinput" name="cp_email" value="" placeholder="Email">
														<div id="msg11" style="color: red;"></div>
													</div>
												</div>
											</b>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- col-md-6 -->                
			</div><!-- /.box-body -->
			<div class="box-footer" style="padding-left: 30px;">
				<div class="col-md-3">
					<a href="<?php echo base_url() ?>register" class="btn btn-sm btn-default" title="Cancel" style="margin-bottom: 0px">Cancel</a>
					<input name="submit" value="Submit" class="btn btn-sm btn-primary" type="button" id="btn_register">
				</div>
			</div><!-- /.box-footer -->
			<?php echo form_close(); ?>
		</div>