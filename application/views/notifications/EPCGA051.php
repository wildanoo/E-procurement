
Dengan hormat,<br/>
<b>Bapak/Ibu <?php echo $to; ?></b><br/>

Bersama ini kami sampaikan terkait pembatalan proses redlist vendor terhadap <?=$vendor_name?> .

<br/><br/>
Maka menindaklanjuti hal tersebut, <?=$vendor_name?> telah diperbolehkan mengikuti
proses pengadaaan pada kategori dimana rekanan terdaftar.
<br/>
<br/>

Untuk detail informasi perusahaan dapat diakses :<br/>
<?php echo $link; ?><br/>

Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/><br/>

(&nbsp;<b><?php echo ucfirst( $this->tank_auth->get_username()); ?></b>&nbsp;)

