 
 
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">

              <div class="box-header with-border">
                <h3 class="box-title">Update Permission</h3>
              </div>

              <!--form class = "form-horizontal"-->
              <?php echo form_open("user/update_permit",array('class'=>'form-horizontal')); 
              		echo form_hidden("id",$def->id); ?>
              <table width="50%">
              	<tr><td>Permission Name </td><td>:</td>
              	<td><input type="text" name="name" id="name" class="form-control" value="<?php echo $def->name; ?>" 
              		placeholder="Permission Name"/></td></tr>              	
              	<tr><td>Permission Value </td><td>:</td>
              		<td><input type="text" name="value" id="value" class="form-control" value="<?php echo $def->value; ?>" 
              		placeholder="Permission Value"/></td></tr> 		           	
              	<tr><td>Permission Code </td><td>:</td>
              		<td><input type="text" name="code" id="code" class="form-control" value="<?php echo $def->code; ?>" 
              		placeholder="Permission Value" disabled/></td></tr> 
              	<tr><td>Permission Group </td><td>:</td>
              		<td><?php 
							$prop = 'class="form-control" id="id_function" style="width:220px"';
							echo form_dropdown('id_function',$perm_group,$def->id_function,$prop); ?></td></tr>  
              	<tr>
					<td style="vertical-align: top;"><label>Description</label></td><td style="vertical-align: top;">:</td>
					<td><textarea name="desc" id="desc" cols="" rows="5" placeholder="Descriptions" class="form-control">
						<?php echo $def->description; ?>
					</textarea></td>
				</tr>
              </table>

                  </div><!-- col-md-6 -->                
                </div><!-- /.box-body -->
                <div class="box-footer" style="padding-left: 30px;">
                    <?php  $prop = array('class'=>'btn btn-sm btn-default','title'=>'Cancel'); ?>					 
					<?php  echo anchor('user/permissions','Cancel',$prop); ?>
                    <?php  echo form_submit('submit', 'Update','class="btn btn-sm btn-primary"'); ?>
                </div><!-- /.box-footer -->
              <?php echo form_close(); ?>
            </div><!-- /.box -->
          </div>
        </div>
      