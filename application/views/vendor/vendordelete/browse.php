<script>
	$(function(){
		$('#idsubcat').change(function(){
			$('#form_search').submit();
		});

		$('#form_search').submit(function(){
			var sdata = $(this).serialize();
			var v_url = baseUrl+'vendordelete/sorting';
			$.post(v_url, sdata, function(data){
				$('#v_tab').html(data);
			});
		});

		$('#start_date, #end_date').datepicker({
			changeMonth: true, changeYear: true, dateFormat:"yy-mm-dd"
		});

		$('#idstat').change(function(){
			var sdata = $('#idstat').val();
			var v_url = baseUrl+'vendordelete/sorting';
			$.post(v_url, {"statID": sdata}, function(data){
				$('#v_tab').html(data);

			});
		});
	});

	function cancel(){
		$('#main_browse').load(baseUrl+'vendordelete/browse');
	}
	function del_vendor(id,reason){
		var csrf	= '<?php echo $this->security->get_csrf_token_name(); ?>';
		var token 	= '<?php echo $this->security->get_csrf_hash(); ?>';
		var cek 	= confirm('Anda yakin ingin menyetujui permintaan delete vendor?');
		if(cek){
			var v_url = baseUrl+'vendordelete/approve_delete/';
			$.post(v_url , { id : id, reason : reason, csrf : token } , function(data){
				cancel();
			});
		}
	}
	function del_ven_ibs(id,reason,delID){
		var csrf	= '<?php echo $this->security->get_csrf_token_name(); ?>';
		var token 	= '<?php echo $this->security->get_csrf_hash(); ?>';
		var cek 	= confirm('Anda yakin ingin menyetujui permintaan delete vendor?');
		if(cek){
			var v_url = baseUrl+'vendordelete/app_to_ibk';
			$.post(v_url,{ id : id, delID : delID, reason : reason, csrf : token}, function(data){
				cancel();
			});
		}
	}
</script>
<div id="main_browse">
	<div class="page-header">
		<h3>Request Delete Vendor</h3>
	</div>
	<?php echo form_open('',array('id'=>'form_search','novalidate'=>'true','onsubmit'=>'return false')); ?>
	<div class="table">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-search"></i> <strong>Search Section</strong>
			</div>
			<style type="text/css">
				.list-inline li{
					vertical-align: middle !important;
				}
				.btn-srch{
					margin-bottom: 0;
				}
			</style>

			<div class="panel-body">
				<ul class="list-inline">
					<li style="width:24%">
						<div class="input-group">
							<input type="date" value="" placeholder="date start" class="form-control date_picker" id="start_date" name="start_date">
							<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
						</div>
					</li>
					<li style="width:2%">to</li>
					<li style="width:24%">
						<div class="input-group">
							<input type="date" value="" placeholder="date end" class="form-control date_picker" id="end_date" name="end_date">
							<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
						</div>
					</li>
					<li style="width:34%"><div class="input-group" style="width:100%"><input placeholder="search by vendor name..." type="text" value="" class="form-control" id="search_term" name="search_term"></div></li>
					<li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search" id="searchbtn">Search</button></div></li>
					<li style="width:7%"><div class="input-group"><a href="<?php echo base_url() ?>vendordelete" class="btn btn-srch btn-warning btn-sm">View All</a></div></li>
				</ul>

			</div>


		</div>
	</div>
	<hr>
	<div class="col-md-3">
		<select name="statID" id="idstat" class="form-control">
			<option value="all">- All Status -</option>
			<?php 
			foreach($stat as $sk => $sv){?>
				<option  value="<?php echo $sk  ?>"<?php if($sk==$this->session->userdata('statID')){echo "selected";} ?> ><?php echo $sv; ?></option>
				<?php } ?>
			</select>
		</div>

		<?php echo form_close(); ?>
		<!-- tabelcontent -->
		<?php require_once('v_data.php'); ?>

	</div>