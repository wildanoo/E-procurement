<div id="container5">

<script type="text/javascript">
 $(function () {
   	var baseUrl  = "<?php echo base_url(); ?>";
   	$('#update5').hide();
   	
   	$("#notaris_date").prop('disabled', true);
   	
   	$('#notaris_date').datepicker({  
        	changeMonth: true, changeYear: true,
			dateFormat: "yy-mm-dd" ,
     }); 

    $('#form5 :input[type="text"]').prop('disabled',true);

  	
   	$('#change5').click(function(){	
   	
   		$('#form5 :input[type="text"]').each(function(index, el) {
		   	$(this).prop('disabled',false);
   		});    
   	
   		$('#update5').show();
   		$('#change5').hide();
   		
   	});
   	
   	$('#update5').click(function(){	  	
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
   	    var values = $("#form5").serialize()+'&'+csrf+':'+token;
			$.ajax({
               url  : baseUrl + 'data_avl/akta_update',
               type : "POST",              		               
               data : values,
               success: function(resp){
               	  $("#container5").load(baseUrl+'data_avl/load_akta');
               }
            });
			
   		$('#update5').hide();
   		$('#change5').show();
   	});
	 	   
 });
 
 </script>
 <?php
$ff = array('notaris_name','notaris_number','notaris_address','notaris_phone','notaris_date','remark');

foreach ($dc_akta as $key => $value)
{
  foreach ($ff as $ffv)
  {
    if($value->fieldname == $ffv)
    {
      $dc[$ffv] = $value->status;
      $label[$ffv] = ($value->status=='request') ? 'warning':'success';

    }
  }

}
// print_r($dc);
?>
<div id="debug3"></div>

<?php echo form_open('#',array('id'=>'form5','class'=>'form-hoizontal','role'=>'form')); ?>
  

  <div class="form-group">
   <label for="vname" class="col-sm-2 control-label">Notaris Name</label>
   <div class="col-sm-10">
   <div class="input-group" style="margin-bottom:3px;">
     <input type="text" value="<?=ucfirst($akta->notaris_name)?>" class="form-control" id="notaris_name" name="notaris_name" disabled>
     <span class="input-group-addon label-<?=$label[notaris_name]?>"><?=$dc['notaris_name']?></span>
   </div>
   </div>
 </div>
 <div class="form-group">
  <label for="vname" class="col-sm-2 control-label">Notaris Address</label>
  <div class="col-sm-10">
  <div class="input-group" style="margin-bottom:3px;">
    <input type="text" value="<?=ucwords($akta->notaris_address)?>" class="form-control" id="notaris_address" name="notaris_address" disabled>
    <span class="input-group-addon label-<?=$label[notaris_address]?>"><?=$dc['notaris_address']?></span>
  </div>
  </div>
</div>
<div class="form-group">
 <label for="vname" class="col-sm-2 control-label">Notaris Phone</label>
 <div class="col-sm-10">
 <div class="input-group" style="margin-bottom:3px;">
   <input type="text" value="<?=ucwords($akta->notaris_phone)?>" class="form-control" id="notaris_phone" name="notaris_phone" disabled>
   <span class="input-group-addon label-<?=$label[notaris_phone]?>"><?=$dc['notaris_phone']?></span>
 </div>
 </div>
</div>
<div class="form-group">
 <label for="vname" class="col-sm-2 control-label">Notaris Number</label>
 <div class="col-sm-10">
 <div class="input-group" style="margin-bottom:3px;">
   <input type="text" value="<?=ucfirst($akta->notaris_number)?>" class="form-control" id="notaris_number" name="notaris_number" disabled>
   <span class="input-group-addon label-<?=$label[notaris_number]?>"><?=$dc['notaris_number']?></span>
 </div>
 </div>
</div>
<div class="form-group">
 <label for="vname" class="col-sm-2 control-label">Notaris Date</label>
 <div class="col-sm-10">
 <div class="input-group" style="margin-bottom:3px;">
   <input type="text" value="<?=ucwords($akta->notaris_date)?>" class="form-control" id="notaris_date" name="notaris_date" disabled>
   <span class="input-group-addon label-<?=$label[notaris_date]?>"><?=$dc['notaris_date']?></span>
 </div>
 </div>
</div>
<div class="form-group">
 <label for="vname" class="col-sm-2 control-label">Remark</label>
 <div class="col-sm-10">
 <div class="input-group" style="margin-bottom:3px;">
   <input type="text" value="<?=ucwords($akta->remark)?>" class="form-control" id="remark" name="remark" disabled>
   <span class="input-group-addon label-<?=$label[remark]?>"><?=$dc['remark']?></span>
 </div>
 </div>
</div>

<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
  <?php if($this->permit->upven) { ?>
      <button type="button" id="change5" class="btn btn-info btn-primary">Change</button>
      <button type="button" id="update5" class="btn btn-info btn-primary">Update</button>
      <a href="javascript:void(0)" onclick="view_logs('akta')" type="button" id="log1" class="btn btn-info btn-info">View Log</a>
  <?php } ?>

  </div>
</div>
<?php echo form_close();  ?>


<br/>
</div>



