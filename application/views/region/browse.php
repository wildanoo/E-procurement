<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";		 
	  $(function(){  });
	 
     		$('#region').change(function(){ 
			var region = $('#region').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'region/set_sess_region',			   
               data : 'field=id&region_name='+region,
               success: function(data){ 
                   //alert(category);	
					 //$('#debug').html(resp);
					 //dialogRef.close();				 	
				 	document.location.href= baseUrl + 'region/';
			   }		   
            });	
		
		});
     
	  $("#srch-term").autocomplete({
			source: baseUrl + 'region/get_region_tags',
			minLength:1
	  });
	   
	  $("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'region/set_sess_search',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){               		            
				   document.location.href= baseUrl + 'region/';
               }
            });
        });  
	 
	  $('#btn_create').click(function(){  form_create();  });	
});

        	function form_create(){
    			var baseUrl = "<?php echo base_url(); ?>";		 
    			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
                var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
    			BootstrapDialog.show({
    			title: 'Create Region',
                message: $('<div></div>').load(baseUrl + "region/form_create/"),
                buttons: [{ label: 'Submit',
    		                cssClass: 'btn btn-primary btn-sm',		               
    		                action: function(dialogRef){
    		                	 var id_country  = $("#id_country").val(); 
    		                	 var region_name = $("#region_name").val();
    		                	 
    		                	 $.post( baseUrl + 'region/is_exist', { csrf: token , id_country : id_country, region_name : region_name })
    							 .done(function( resp ) {
//     								 $('#debug').html(resp);
//     								 dialogRef.close(); 
    									var json = $.parseJSON(resp);
    									if (json.status=='true'){
    					                	var values = $("#form").serializeArray();
    					                	values.push({ name: csrf,value: token });
    										values = jQuery.param(values);
    										 $.ajax({
    								               url  : baseUrl + 'region/create',
    								               type : "POST",              		               
    								               data : values,
    								               success: function(resp){
    								               		document.location.href= baseUrl + 'region/';	               				               		  
    										       }
    								            });   
    									} else { 	
    									 $("#msg1").html(json.msg1); 	
    									 $("#msg2").html(json.msg2);
    								}
    		               		 });  
    		                }
    		            }, {
    		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
    				                action: function(dialogRef) {
    				                	document.location.href= baseUrl + 'region/';
    				                    dialogRef.close(); 
    				                }
    			        }]
            });
    	}


        	function form_update(id){
    			var baseUrl = "<?php echo base_url(); ?>";
    			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
                var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
             
    			BootstrapDialog.show({
    			title: 'Update Region',
                message: $('<div></div>').load(baseUrl + "region/form_update/" + id),  
                buttons: [{			            	
    		                label: 'Submit',
    		                cssClass: 'btn btn-primary btn-sm',		               
    		                action: function(dialogRef){
        		                var id_country  = $("#id_country").val();
    		                	var region_name = $("#region_name").val();  
    		                	//var remark		= $("#remark").val();               	  

    		                	 $.post( baseUrl + 'region/is_exist', { csrf: token , id_country : id_country , region_name : region_name })
    							 .done(function( resp ) {

    							var json = $.parseJSON(resp);
    							if (json.status=='true'){
    			                	var values = $("#form").serializeArray();
    			                	values.push({ name: csrf,value: token });
    								values = jQuery.param(values);
    								 $.ajax({
    						               url  : baseUrl + 'region/update',
    						               type : "POST",              		               
    						               data : values,
    						               success: function(resp){
   		     								 //$('#debug').html(resp);
   		     								 //dialogRef.close(); 
    						                 document.location.href= baseUrl + 'region/';	               				               		  
    								       }
    						            });   
    							} else { 	
    								 $("#msg1").html(json.msg1);
    								 $("#msg2").html(json.msg2);
    								 //$("#msg3").html(json.msg3);
    							}

    							});   
    		                }
    		            }, {
    		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
    				                action: function(dialogRef) {
    				                	document.location.href= baseUrl + 'region/';
    				                    dialogRef.close(); 
    				                }
    			        }]
            	});
    	}


</script>
<script src="<?php echo base_url(); ?>assets/js/sorttable.js"></script>
</head>
<body>
<div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Area</h3>
              </div><!-- /.box-header -->
              <div class="box-body">
                <br>
                    <?php if($this->session->flashdata('msg_warning')!=null) { ?>
                	<div class="alert alert-warning">
  						<strong>Warning!</strong> <?php echo $this->session->flashdata('msg_warning'); ?>
					</div>
					<?php } ?>
                <div class="col-md-2 pull-left">
					<input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Area" value="New Area" style="width:100px;" title="Create New Area"/>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
					<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>	
				</div>
                    
				<div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('region/search',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>region" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		        </div>
		
		        </div>
                    
<div class="box-body">
<table id="example2" class="table table-bordered table-hover sortable">
	<thead>
		<tr>
            <th class="sorttable_sorted_reverse">No</th>
            <th class="">Area</th>
            <th class="">Country</th>
            <!--th class="">Status</th>-->
            <!-- th>Remark</th> -->
            <th class="">Created Date</th>
            <th class="">Updated Date</th>
            <th class="">Updated By</th>
            <th class="">Action</th>
        </tr>
     </thead>
     <tbody>
 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td><?php echo $num = $num+1; ?></td>
			<td><?php echo $row->region_name; ?></td>	
			<td><?php echo $row->country_name; ?></td>
			<!--td><?php echo $row->status==1 ? "Active" : "Not Active"; ?></td>-->
			<!-- td><?php //echo $row->remark; ?></td> -->
			<td align="center"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>	
 			<td align="center"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>
 			<td align="center"><?php echo $row->created_id; ?></td>							
				
				<td style="text-align: center;">
				<?php
					echo  '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/>&nbsp;&nbsp;';
					$trash  = '<i class="fa fa-trash"></i>';
					$js = "if(confirm('Are you sure to delete record ?')){ return true; } else {return false; }";
					echo anchor('region/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete','onclick'=>$js));			
				?>				
				</td>	
										
		</tr>
<?php	$i++;  } ?>	
</table>
<tr><div colspan="9" style="text-align: center;"><?php echo $pagination;?></div></tr>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>  
	</table>  
                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->
