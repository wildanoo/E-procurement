<script type="text/javascript">
  $(function () { 
   	  var baseUrl  = "<?php echo base_url(); ?>"; 
   	 $('#form_visit :input[type="text"], textarea').attr('disabled', true); 
   	 
     $("#visit_date").datetimepicker({
		format:'Y/m/d H:i',
		minDate: 0,
		onChangeDateTime: function (dp,selectedDate) {
			var time = $("#visit_date").val();
        }
	});
     
     $('#btn_cancel_visit').click(function(){ 
   			$("#container11").load(baseUrl+'due_diligence/load_visit_form');   		
   	  }); 
   	  
   	 $('#btn_visit_update').click(function(){ 
   	  
   	  		var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	    
		    var values   = $("#form_visit").serializeArray();
			values.push({name: csrf,value: token});
			values = jQuery.param(values);
			 	$.post( baseUrl+ "due_diligence/save_visit", values )
				  .done(function( resp ) {
				    	$("#container11").load(baseUrl+'due_diligence/load_visit_form'); 
				});
	     	
	     });    
     
  });

</script>

<div id="debug11"></div>
<?php  //print_r($visit);?>
<?php echo form_open('#',array('id'=>'form_visit')); $current_date = date('Y-m-d'); ?>
<input type="hidden" name="id" id="id" value="<?php echo $visit->id; ?>" />
<table width="80%">
	<tr>
		<td><label>Jenis Kantor</label></td><td>:</td>
		<td><input type="text" name="office_type" id="office_type" class="form-control" value="<?php echo $visit->office_type; ?>" placeholder="Jenis Kantor" required/></td>
	</tr>	
	<tr>
		<td style="vertical-align: top;"><label>Alamat Kantor</label></td><td style="vertical-align: top;">:</td>
		<td><textarea name="office_address" id="office_address" cols="20" rows="5" class="form-control" placeholder="Alamat Kantor" required><?php echo $visit->address; ?></textarea></td>
	</tr>
	<tr>
		<td><label>PIC</label></td><td>:</td>
		<td><input type="text" name="pic" id="pic" class="form-control" value="<?php echo $visit->pic; ?>" placeholder="PIC" required/></td>
	</tr>
	<tr>
		<td><label>No. Telp</label></td><td>:</td>
		<td><input type="text" name="office_telp" id="office_telp" class="form-control" value="<?php echo $visit->telp; ?>" placeholder="No. Telephone" required/></td>
	</tr>
	<tr>
		<td><label>No. Fax</label></td><td>:</td>
		<td><input type="text" name="office_fax" id="office_fax" class="form-control" value="<?php echo $visit->fax; ?>" placeholder="No Fax" required/></td>
	</tr>
	<tr>
		<td><label>Waktu Kedatangan</label></td><td>:</td>
		<td>
			<?php //$time = explode(":",$visit->visit_time); //print_r($time); 
				$visit_date = str_replace("-","/",$visit->visit_date)." ".$visit->visit_time;
			?>			
			<input type="text" name="visit_date" id="visit_date"  value="<?php echo $visit_date; ?>" style="width:130px;height: 25px;" />	
			<!--input type="text" name="visit_hour" id="visit_hour"  value="<?php echo $time[0]; ?>" placeholder="Jam"   style="width:80px;height: 25px;" />:
			<input type="text" name="visit_minute" id="visit_minute"  value="<?php echo $time[1]; ?>" placeholder="Menit"   style="width:80px;height: 25px;" /-->
		</td>
	</tr>
	<tr>
		<td style="vertical-align: top;"><label>Dalam Rangka</label></td><td style="vertical-align: top;">:</td>
		<td><textarea name="remark" id="remark" cols="20" rows="5" class="form-control" placeholder="Keterangan" required><?php echo $visit->remark; ?></textarea></td>
	</tr>
	<tr>
		<td align="right" colspan="3">
			<button type="button" id="btn_cancel_visit" class="btn btn-default">Back</button>
					
		</td>
	</tr>
</table>

