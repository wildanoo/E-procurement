  <script type="text/javascript"> 
	$(function(){
		var baseUrl = "<?php echo base_url(); ?>";
		
		$('#id_cat').change(function(){  
	   		var id_cat = $('#id_cat').val(); //alert(id_cat);
	   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	            $.ajax({
	               url  : baseUrl + 'vendor/get_subcategory',
	               type : "POST",              		               
	               data : csrf +'='+ token +'&id_cat='+id_cat,
	               success: function(resp){
	               	  $('#id_subcat').html(resp);
	               }
	            });
	    }); 
	    
	    $("#btn_search").click(function(){            
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';            
            var values = $("#form_search").serializeArray();
			values.push({name: csrf,value: token});			
			values = jQuery.param(values);            
            $.ajax({
               url  : baseUrl + 'vendor/set_sess_search',
               type : "POST",              		               
               data : values,
               success: function(resp){                		            
				    document.location.href= baseUrl + 'vendor/';
					//$("#debug").html(resp);
               }
            });
        });
        
        $("#btn_reset").click(function(){   
         		document.location.href= baseUrl + 'vendor/reset_filter';
        });	    	
		
}); 
</script>
  	  
 <div id="debug"></div> 	  
	  <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-search"></i>&nbsp;&nbsp;Searching</div>
      <div class="panel-body">  
       <?php echo form_open('vendor/set_sess_search',array('id'=>'form_search'));      					
      		$page    = $this->session->userdata('page');      					
      		$default = array('id_category'=>'default','id_stsreg'=>'default','id_subcat'=>'0','vendor_type'=>'default');
      		$sess    = $this->session->userdata('sess_vendor');      				  
      		if($sess) $default = $sess + $default; ?>
      		
      		<table cellpadding="5" cellspacing="5">
      			<tr>
      				<td><input type="text" name="srch-term" id="srch-term" placeholder="vendor name or code" class="form-control"/></td>
      				<?php if($page=="register" && !$this->m_vendor->is_approved()) { ?> 
      				<td>     					
      				<select name="id_stsreg" id="id_stsreg" class="form-control" style="font-size:12px; width: 170px;">	
      					<?php foreach($register as $key=>$val){
      							if($key==$default['id_stsreg']) $selected = "selected"; else 
      							$selected ="";
      						  echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>'; } ?>
      				</select>	
      				</td><?php } ?>
      				
      				
      				<?php if(!$page) { ?>  <td>
      					<?php   
      					$status = array(''=>'-- Vendor Type --','new'=>'NEW','avl'=>'AVL','shortlist'=>'SHORTLIST','fail'=>'FAIL');
	    				echo form_dropdown("vtype",$status,$default['vendor_type'],'class="form-control" id="vtype" style="font-size:12px;"'); ?>
      				</td><?php } ?>
      				<td>     					
      				<select name="id_cat" id="id_cat" class="form-control" style="font-size:12px; width: 170px;">	
      					<?php foreach($category as $key=>$val){
      							if($key==$default['id_category']) $selected = "selected"; else $selected ="";
      						  echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>'; } ?>
      				</select>	
      				</td>
      				<td>
      					<select name="id_subcat" id="id_subcat" class="form-control" style="font-size:12px; width: 170px;">							
						<?php $id_subcat = $default['id_subcat'];														
							if($id_subcat){	 
								$subcat   = $this->crud->browse("","subcategory","id",$id_subcat,"false","id,subcategory");
								echo '<option value="'.$subcat->id.'">'.$subcat->subcategory.'</option>'; 
							} else { ?>	<option value="">--Select Category--</option>  <?php } ?>	
						</select>
      				</td>
      				<td>&nbsp;</td>
      				<td><input type="button" class="btn btn-info" id="btn_search" value="Search"/></td>
	    			<td><input type="button" class="btn btn-warning" id="btn_reset" value="Reset"/></td>
      				
      			</tr>
      		</table>
	    <?php echo form_close(); ?>
      </div>
    </div>  		