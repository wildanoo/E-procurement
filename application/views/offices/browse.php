<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";		 
	  $(function(){  });
	 
      $('#office').change(function(){ 
			var office = $('#office').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'offices/set_sess_office',			   
               data : 'field=id&office_name='+office,
               success: function(data){ 
                   //alert(category);	
					 //$('#debug').html(resp);
					 //dialogRef.close();				 	
				 	document.location.href= baseUrl + 'offices/';
			   }		   
            });	
		
		});
     
	  $("#srch-term").autocomplete({
			source: baseUrl + 'offices/get_office_tags',
			minLength:1
	  });
	   
	  $("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'offices/set_sess_search',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){               		            
				   document.location.href= baseUrl + 'offices/';
               }
            });
        });  
	 
	  $('#btn_create').click(function(){  form_create();  });	
});

	

	function form_create(){
			var baseUrl = "<?php echo base_url(); ?>";		 
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
			BootstrapDialog.show({
			title: 'Create New Offices',
            message: $('<div></div>').load(baseUrl + "offices/form_create/"),
            buttons: [{ label: 'Submit',
            			hotkey: 13,
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){
			                 var office_code = $("#office_code").val();
		                	 var office_name = $("#office_name").val(); 
		                	 var id_country  = $("#id_country").val(); 
		                	 var id_region   = $("#id_region").val();
		                	 var email   	 = $("#email").val();
		                	 var phone   	 = $("#phone").val();
		                	 var fax   	 	 = $("#fax").val();		                	  
							 $.post( baseUrl + 'offices/is_exist', { csrf: token , office_code : office_code , office_name : office_name, id_country : id_country , id_region : id_region , email : email, phone : phone, fax : fax, action:'create' })
			 				 .done(function( resp ) {
							var json = $.parseJSON(resp);
							if (json.status=='true'){	                	
			                	var values = $("#form").serializeArray();
			                	values.push({ name: csrf,value: token });
								values = jQuery.param(values);	              		                    
			                    $.ajax({
					               url  : baseUrl + 'offices/create',
					               type : "POST",              		               
					               data : values,
					               success: function(resp){
					               		document.location.href= baseUrl + 'offices/';	
					               		//$('#debug').html(resp);
					               		//alert(values);               				               		  
							       }
					            });   
					        } else { 
						        	$("#msg1").html(json.msg1); 
					        		$("#msg2").html(json.msg2); 
					        		$("#msg3").html(json.msg3);
					        		$("#msg4").html(json.msg4);
					        		$("#msg5").html(json.msg5);
					        		$("#msg6").html(json.msg6);
					        		$("#msg7").html(json.msg7);
			 				 }
		                });       
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'offices/';
				                    dialogRef.close(); 
				                }
			        }]
        });
	}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Office',
            message: $('<div></div>').load(baseUrl + "offices/form_update/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){
		                	 var office_code = $("#office_code").val();
		                	 var office_name = $("#office_name").val(); 
		                	 var id_country  = $("#id_country").val(); 
		                	 var id_region   = $("#id_region").val();	
		                	 var email   	 = $("#email").val();
		                	 var phone   	 = $("#phone").val();
		                	 var fax   	 	 = $("#fax").val();
		                	 var id   	 	 = $("#id_office").val();
		                	 //var remark		 = $("#remark").val();		                	  
							 $.post( baseUrl + 'offices/is_exist', { csrf: token , office_code : office_code , office_name : office_name, id_country : id_country , id_region : id_region , email : email, phone : phone, fax : fax, id : id })
			 				 .done(function( resp ) {
							var json = $.parseJSON(resp);
							if (json.status=='true'){	                	
			                	var values = $("#form").serializeArray();
			                	values.push({ name: csrf,value: token });
								values = jQuery.param(values);	              		                    
			                    $.ajax({
					               url  : baseUrl + 'offices/update',
					               type : "POST",              		               
					               data : values,
					               success: function(resp){
					               		document.location.href= baseUrl + 'offices/';	               				               		  
							       }
					            });   
					        } else { 
						        	$("#msg1").html(json.msg1); 
					        		$("#msg2").html(json.msg2); 
					        		$("#msg3").html(json.msg3);					        		
					        		$("#msg4").html(json.msg4);
					        		$("#msg5").html(json.msg5);
					        		$("#msg6").html(json.msg6);
					        		$("#msg7").html(json.msg7);
			 				 }
		                });      
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'offices/';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>
<script src="<?php echo base_url(); ?>assets/js/sorttable.js"></script>
</head>
<body>
 <div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Offices</h3>
              </div><!-- /.box-header -->
                <br>
                    <?php if($this->session->flashdata('msg_warning')!=null) { ?>
                	<div class="alert alert-warning">
  						<strong>Warning!</strong> <?php echo $this->session->flashdata('msg_warning'); ?>
					</div>
					<?php } ?>
                <div class="col-md-2 pull-left">
                    <input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Office" value="New Office" style="width:100px;" title="Create New Region"/>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
				<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
                </div>
                    
				<div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('offices/search',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>offices" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		        </div>
		        </div>                    
                    
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover sortable">
                    <thead>
                      <tr>
                        <!--th><input type="checkbox" name="cba[]" id="cba" /></th-->
                        <th class="sorttable-sorted-reverse">No</th>
                        <th class="">Office Code</th>
                        <th class="">Office Name</th>
                        <th class="">Area</th>
                        <th class="">Country</th>
                        <th class="">Email</th>
                        <th class="">Phone</th>
                        <th class="">Fax</th>
                        <!--th class="">Status</th>-->
                        <th class="">Created Date</th>
                        <th class="">Updated Date</th>
                        <th class="">Updated By</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    
                    <tbody>
<?php
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td><?php echo $num = $num+1;; ?></td>
				<td><?php echo $row->office_code; ?></td>
				<td><?php echo $row->office_name; ?></td>
				<td><?php echo $row->region; ?></td>					
				<td><?php echo $row->country; ?></td>					
				<td><?php echo $row->email; ?></td>					
				<td><?php echo $row->phone; ?></td>					
				<td><?php echo $row->fax; ?></td>					
				<!--td><?php if($row->status==1) echo "Active";  else echo "Not Active"; ?></td>-->
				<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>	
				<td><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>
				<td align="center"><?php echo $row->created_id; ?></td>	
				<td><?php				
					echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete office ?')){ return true; } else {return false; }";
					echo anchor('offices/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Offices','onclick'=>$js));?>
				</td>
				
			</tr>
<?php	$i++;  } ?>	
</table>
<tr><div colspan="9" style="text-align: center;"><?php echo $pagination;?></div></tr>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>  
	</table>  
                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->
