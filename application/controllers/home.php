<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		islogged_in();
    }
    
 function index(){
 	/*if ($this->tank_auth->is_logged_in()) { */	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();	
		
		//$session = $this->session->all_userdata(); print_r($session);
				
		$data['view']	= "layout/dashboard"; 		
 		$this->load->view('layout/template',$data);
 		
	/*} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	}*/
 	
 } 
 
 function server_time(){
 	
 	echo "=>".date('Y-m-d H:i:s');
 	
 }

}

