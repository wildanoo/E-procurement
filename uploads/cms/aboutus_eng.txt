<p>Garuda Indonesia E-Procurement is a web-based application system that provides various facilities as well as information related to procurement process of goods / services in PT Garuda Indonesia (Persero) Tbk. The system is developed in order to improve the efficiency and effectiveness of Procurement.</p>

<p>Through Web E-Procurement, Suppliers could access some information including the Latest News about the Procurement Updates in Garuda, Procurement Information and Procurement Policy applied in Garuda. Suppliers also can sign up and register online to be our registered vendor..</p>
