$(document).ready(function(){

  function getUrlParameter(name) {
      name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
      var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
      var results = regex.exec(location.search);
      return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  };

  var csrf_token = $('meta[name="csrf-token"]').attr('content');
  $.ajaxPrefilter(function(options, originalOptions, jqXHR){
    if (options.type.toLowerCase() === "post") {
        options.data = options.data || "";
        options.data += options.data?"&":"";
        options.data += "_token=" + csrf_token;
    }
  });

  $('#login-attempt').click(function(){
    var login     = $('#login').val();
    var password  = $('#password').val();

    $.ajax({
      url  : 'email_portal/auth_attempt',
      type : "POST",
      data : 'login='+login+'&password='+password+'&token = '+getUrlParameter('token'),
      success: function(resp){
        var json = $.parseJSON(resp);
        if(json.status=="false"){
          $('.failed-attempt').show();
          $("#message").html(json.errors);
        } else {
          $('.failed-attempt').hide();
          document.location.href= 'email_portal?token='+getUrlParameter('token');
        }
      }
    });
  });

}); 