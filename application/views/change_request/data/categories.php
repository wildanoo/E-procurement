<div id="cat">

<div id="cat_msg" class="text-center text-danger bg-warning"></div>	

<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th>No</th>
			<th>Category</th>
			<th>Subcategory</th>
			<th>Approval</th>
			<th>Status</th>
			<th>#</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach ($categories as $key => $value) 
	{
		$stat = ($value->status == '0') ? 'request':'approved';

		echo
		'<tr>
		<td>'.$no++.'</td>
		<td>'.$value->category.'</td>
		<td>'.$value->subcategory.'</td>
		<td>'.$value->approval.'</td>
		<td><span class="label label-warning">'.$stat.'</span></td>
		<td><button onclick="approve_category([\''.$value->id.'\',\''.$value->approval.'\',\''.$value->id_vendor.'\',\''.$value->id_subcat.'\'])" type="button" class="btn btn-default"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
			<button onclick="reject_temp_table('.$value->id.',\'temp_vendor_category\')" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
		</tr>';
	} ?>
	</tbody>
</table>

</div>