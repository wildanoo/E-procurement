

Dengan hormat,<br/>
<b>Bapak/Ibu <?php echo $to; ?></b><br/>

Bersama ini kami sampaikan bahwa proses perubahan data vendor yang Bapak/Ibu <?php echo $to; ?> ajukan tidak disetujui. 
<br/>
Berikut ini adalah informasi detail vendor.
<br/>
<table>
	<tr>
		<td>Nama Perusahaan</td>
		<td>:</td>
		<td><?php echo $vendor_name; ?></td>
	</tr>
	<tr>
		<td>No. Registrasi</td>
		<td>:</td>
		<td><?php echo $reg_num ?></td>
	</tr>
</table>


Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/><br/>

(&nbsp;<b><?php echo ucfirst( $this->tank_auth->get_username()); ?></b>&nbsp;)

