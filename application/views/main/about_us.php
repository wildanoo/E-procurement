<!DOCTYPE HTML>
<html lang="en-US">
<?php $this->load->view('layout/header_default'); ?>
</head>
<body>
<div id="wrapper">
	
	<!-- sidebar english -->
	<div id="wrapper1" class="st-container">
		<nav class="st-menu st-effect-1" style="height:100%;background-color:#005291;position:fixed;">
			<?php $this->load->view('auth/login_form'); ?>
		</nav>
	</div>
	<div id="wrapper2" class="st-container">
		<nav class="st-menu st-effect-2" style="height:100%;background-color:#ededea;position:fixed;overflow-y: scroll;overflow-x: hidden;">	  
			<?php $this->load->view('vendor/register'); ?>
		</nav>
	</div>
	<!-- sidebar english -->

	<header id="header" class="container-fluid">
	  <div class="header-bg">
		<div class="container">	  
			<div class="row">
				<div id="logo" class="col-md-5 col-md-offset-1"><a href="login" class="logo" alt="E-Procurement Garuda Indonesia"></a></div>
				<div class="col-md-4">
					<ul class="social-icons">
					<?php if ($this->session->userdata('lang') == 'eng') { ?>
						<li class="lang eng" data="lang-eng" title="English"><a style="top:0 !important;" href="#">English</a></li>
						<li class="lang idn" data="lang-ind" title="Indonesian"><a class="inactive" href="#" id="lang_ind">Indonesian</a></li>
					<?php }elseif ($this->session->userdata('lang') == 'ind') { ?>
						<li class="lang eng" data="lang-eng" title="English"><a class="inactive" href="#" id="lang_eng">English</a></li>
						<li class="lang idn" data="lang-ind" title="Indonesian"><a style="top:0 !important;" href="#">Indonesian</a></li>
					<?php }?>
					</ul>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<ul id="menu" class="visible-lg" style="font-size: 13px;">
						<li><?php echo anchor('auth/login', 'HOME')?></li>
						<!-- li><?php  echo anchor('main/news','News'); ?></li> -->
						<li><?php echo anchor('main/about_us','ABOUT US'); ?></li>
						<li><?php echo anchor('main/faq','FAQ'); ?></li>
						<li id="st-trigger-effects2"><a data-effect="st-effect-2">REGISTRATION</a></li>
						<li id="st-trigger-effects1"><a data-effect="st-effect-1">LOG IN</a></li>
						<li><?php  echo anchor('main/contact_us','CONTACT US'); ?></li>
						<!-- Search Form -->
						<li class="search-form visible-desktop" style="float: right;top: 10px;"\>
							<form method="get" action="#">
								<input type="text" value="Search" class="search-text-box"/>
								<input type="submit" value="" class="search-text-submit"/>
							</form>
						</li>
					</ul>
				</div>
			</div>
		</div>
	  </div>
	</header>
	<section id="content" class="container-fluid">
	<div class="container" style="width: 940px;margin: 0 auto;padding:0;">
		<!-- <div id="headline-page">
			<h1 style="margin-top:12px;"><?php //echo $browse->title; ?></h1>
			<div id="crumbs"><a href="index.html">Home</a> / <a href="#" class="active">News</a></div> 
		</div> -->
		<div class="row">
			<div class="col-md-12">
				<div class="post">
					<div class="title">
						<h2 style="display:inline-block;vertical-align:baseline;">About Us<?php //echo $browse->title; ?></h2>
						<div style="display:inline-block;float:right;padding:25px 0 0 0;" id="crumbs"><a href="<?php echo base_url(); ?>auth/login">Home</a> / <a href="#" class="active">About Us</a></div> 
					</div>
					<div class="blog-line">
						<!-- a href="#" class="date item"><?php 
						//$date = str_replace('/', '-', $browse->publish_date);
						//echo date('M d / Y', strtotime($date)); 
						//echo '';?></a>
						<a href="#" class="category item">Category: <?php //echo $browse->subtype_name; ?></a> -->
						<p><?php  $root = $_SERVER['DOCUMENT_ROOT']; $path = $root."/uploads/cms/aboutus_eng.txt";
						if( file_exists($path)) {	echo file_get_contents($path);			
						} else { echo ""; }	?></p>
					</div>
					<!-- div class="thumbnail">
						<div id="myCarousel" class="carousel slide">
							<div class="carousel-inner">
								<div class="item active"> 
									<img alt="" src="<?php //echo base_url(); ?>uploads/images/<?php //echo $browse->file;?>.jpg" >
								</div>
							</div>					
						</div>
					</div> -->
					<!-- <p style="text-align:justify;"><strong>As a commitment of PT Garuda Indonesia (Persero) Tbk. (Garuda)</strong> to always maintain and carry out the principles of Good Corporate Governance (GCG) and in line with the implementation of Business Ethics and Work Ethics in the Garuda, we herewith that Garuda not levy / charge anything on a whole series of processes Registration Client and also for Procurement of Goods / Services in the Garuda. If there are parties who tried to charge with a promise / offer help on related processes, beg to be ignored and more Mr / Ms can report it through the Whistle Blowing System Garuda which can be accessed at <a href="http://www.ga-whistleblower.com.">http://www.ga-whistleblower.com.</a></p><p style="text-align:justify;">We greatly appreciate the support of Mr / Ms to jointly safeguard the implementation of the procurement of clean and transparent.</p> -->
					<p style="text-align:justify;"><?php //echo isset($contents)? $contents:'' ; ?></p>
					<hr/>
					<?php //if ($browse->content_type != '1') { ?>
					<!-- a href="<?php //echo base_url().'webcontent/registration/'.$this->uri->segment(3) ?>" id="btn_approve" class="col-md-offset-10 btn btn-primary btn-sm" style="width:80px;">Join Now</a> -->
					<!-- hr/> -->

					<?php //} ?>
				</div>			
			</div>
			<?php //$this->load->view('content_public/recent_post',$recent_post); ?>
		</div>
	</div>
	</section>
</div>
<footer class="main-footer footer-line container-fluid text-center">
	Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
</footer>
<?php $this->load->view('layout/footer_default'); ?>
</body>
</html>