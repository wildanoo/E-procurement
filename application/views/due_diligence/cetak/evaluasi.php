<?php
if(!$vendor->result)
{
	echo "<script>window.close()</script>";
	exit();
}
$tanggal_visit = indo_date($vendor_visit->visit_date);

$no=1;
//url logo
if(base_url() == '/')
{
	$base = "http://$_SERVER[HTTP_HOST]";
	$url_logo = $base.'/assets/imagesv2/logo.png';
}
else
{
	$url_logo = base_url('assets/imagesv2/logo.png');
}
//data category
$no=1;
foreach ($categories as $val_cats)
{
	$cont_cats .= "$no . $val_cats->category - $val_cats->subcategory <br>";
	$no++;
}
//data general
$cont_general = '<table>';
$cont_general .= '<tr><td>a. Nama Perusahaan</td><td>:</td><td> '.ucfirst($vendor->name).' </td></tr>';
$cont_general .= '<tr><td>b. Alamat</td><td>:</td><td> '.ucwords($vendor->address).' </td></tr>';
$cont_general .= '<tr><td>c. Kode Pos</td><td>:</td><td> '.$vendor->postcode.' </td></tr>';
$cont_general .= '<tr><td>d. NPWP</td><td>:</td><td> '.strtoupper($vendor->npwp).' </td></tr>';
$cont_general .= '<tr><td>e. Telepon</td><td>:</td><td> '.$vendor->phone.' </td></tr>';
$cont_general .= '<tr><td>f. Fax</td><td>:</td><td> '.$vendor->fax.' </td></tr>';
$cont_general .= '<tr><td>g. Email</td><td>:</td><td> '.$vendor->email.' </td></tr>';
$cont_general .= '<tr><td>h. Web</td><td>:</td><td> '.$vendor->web.' </td></tr>';
$cont_general .= '</table>';
//data cp
$no=1;
$cont_cp = '<table border="2" style="border-collapse:collapse;">';
$cont_cp .= '<tr><td>No</td><td>Name</td><td>Position</td><td>Mobile</td><td>Email</td></tr>';
if(empty($cp))
{
	$cont_cp .= "<tr><td style='text-align:center;' colspan='5'>Belum ada data</td></tr>";
}
else
{
	foreach ($cp as $conper)
	{
		$cont_cp .= "<tr><td>$no</td><td>$conper->fullname</td><td>$conper->position</td><td>$conper->mobile</td><td>$conper->email</td></tr>";
		$no++;
	}
}
$cont_cp .= '</table>';

//data akta
$cont_akta = '<table >';
$cont_akta .= '<tr><td>a. Nama Notaris</td><td>:</td><td> '.ucfirst($akta->notaris_name).' </td></tr>';
$cont_akta .= '<tr><td>b. Alamat Notaris</td><td>:</td><td> '.ucfirst($akta->notaris_address).' </td></tr>';
$cont_akta .= '<tr><td>c. Telepon Notaris</td><td>:</td><td> '.ucfirst($akta->notaris_phone).' </td></tr>';
$cont_akta .= '<tr><td>d. Nomor Notaris</td><td>:</td><td> '.ucfirst($akta->notaris_number).' </td></tr>';
$cont_akta .= '<tr><td>e. Tanggal Notaris</td><td>:</td><td> '.$akta->notaris_date.' </td></tr>';
$cont_akta .= '<tr><td>f. Keterangan</td><td>:</td><td> '.$akta->remark.' </td></tr>';
$cont_akta .= '</table>';
//data owner
$no=1;
$cont_owner = '<table border="2" style="border-collapse:collapse;">';
$cont_owner .= '<tr><td>No</td><td>Owner Name</td><td>Owner Address</td><td>Owner Phone</td><td>Owner Position</td><td>Owner Shared</td><td>Remark</td></tr>';
if(empty($owner))
{
$cont_owner .= '<tr><td style="text-align:center;" colspan="7">Belum ada data</td></tr>';
}
else
{
	foreach ($owner as $oner)
	{
		$cont_owner .= "<tr><td>$no</td><td>$oner->owner_name</td><td>$oner->owner_address</td><td>$oner->owner_phone</td><td>$oner->owner_position</td><td>$oner->owner_shared</td><td>$oner->remark</td></tr>";
		$no++;
	}
}
$cont_owner .= '</table>';
//data pengurus
$no=1;
$cont_pengurus = '<table border="2" style="border-collapse:collapse;">';
$cont_pengurus .= '<tr><td>No</td><td>Name</td><td>Address</td><td>Phone</td><td>Position</td><td>Remark</td></tr>';
if(empty($pengurus))
{
$cont_pengurus .= '<tr><td style="text-align:center;" colspan="6">Belum ada data</td></tr>';

}
else
{
	foreach ($pengurus as $pgs)
	{
		$cont_pengurus .= "<tr><td>$no</td><td>$pgs->name</td><td>$pgs->address</td><td>$pgs->phone</td><td>$pgs->position</td><td>$pgs->remark</td></tr>";
		$no++;
	}
}
$cont_pengurus .= '</table>';
//data surat legalitas
$no=1;
$cont_surleg = '<table border="2" style="border-collapse:collapse;">';
$cont_surleg .= '<tr><td>No</td><td>Document Name</td><td>Document Number</td><td>Description</td><td>Effective Date</td><td>Expired Date</td><td>Remark</td></tr>';
if(empty($surleg))
{
$cont_surleg .= '<tr><td style="text-align:center;" colspan="7">Belum ada data</td></tr>';
}
else
{
	foreach ($surleg as $srg)
	{
		$cont_surleg .= "<tr><td>$no</td><td>$srg->doc_name</td><td>$srg->doc_num</td><td>$srg->desc</td><td>$srg->eff_date</td><td>$srg->exp_date</td><td>$srg->remark</td></tr>";
		$no++;
	}
}
$cont_surleg .= '</table>';
//data reference
$no=1;
$cont_ref = '<table border="2" style="border-collapse:collapse;">';
$cont_ref .= '<tr><td>No</td><td>Customer Name</td><td>Project</td><td>Poin</td><td>Date</td><td>Remark</td></tr>';
if(empty($ref))
{
$cont_ref .= '<tr><td style="text-align:center;" colspan="6">Belum ada data</td></tr>';

}
else
{
	foreach ($ref as $ref)
	{
		$cont_ref .= "<tr><td>$no</td><td>$ref->cust_name</td><td>$ref->project</td><td>$ref->poin</td><td>$ref->date</td><td>$ref->remark</td></tr>";
		$no++;
	}
}
$cont_ref .= '</table>';
//data afiliates
$no=1;
$cont_af = '<table border="2" style="border-collapse:collapse;">';
$cont_af .= '<tr><td>No</td><td>Company Name</td><td>Competency</td><td>Contact</td><td>Remark</td></tr>';
if(empty($aff))
{
$cont_af .= '<tr><td style="text-align:center;" colspan="5">Belum ada data</td></tr>';
}
else
{
	foreach ($aff as $aff)
	{
		$cont_af .= "<tr><td>$no</td><td>$aff->company_name</td><td>$aff->competency</td><td>$aff->contact</td><td>$aff->remark</td></tr>";
		$no++;
	}
}
$cont_af .= '</table>';


//page 1
$content .= '<page>';
$content .= '<style type="text/css">
			<!--
			page {font-size:12px;}
			table { vertical-align: top; }
			tr    { vertical-align: top; }
			td    { vertical-align: top; }
			}
			-->
			</style>
			';
$content .=
'
<p style="text-align:Center;font-weight:bold;">
<img src="'.$url_logo.'" style="margin:0 auto;" >
<br><br>
BERITA ACARA <br>
EVALUASI SOURCING <br>
PT. GARUDA INDONESIA (PERSERO) Tbk <br>
</p>
<hr>
<br>
<p>
Menindaklanjuti proses sourcing terkait kebutuhan penyedia barang dan jasa PT Garuda
Indonesia (Persero) Tbk, telah dilakukan proses due diligence terhadap perusahaan calon
rekanan dengan hasil sebagai berikut : <br>
</p>';
$content .='
<table>
	<tr>
		<td colspan="4"><b>Informasi Perusahaan :</b></td>
	</tr>
	<tr>
		<td>1</td>
		<td>Nama</td>
		<td>:</td>
		<td>'.$vendor->name.'</td>
	</tr>
	<tr>
		<td>2</td>
		<td>Rekomendasi</td>
		<td>:</td>
		<td>'.$recmd = !$vendor->result ? "-" : strtoupper($vendor->result).'</td>
	</tr>
	<tr>
		<td>3</td>
		<td>Kategori</td>
		<td>:</td>
		<td>'.$cont_cats.'</td>
	</tr>
	<tr>
		<td>4</td>
		<td>Note</td>
		<td>:</td>
		<td>'.strtoupper($vendor->result_note).'</td>
	</tr>
</table>

Adapun detail informasi perusahaan adalah sebagai berikut : <br><br>
';
$content .='
<b>1. General Informasi </b><br><br>
'.$cont_general.'<br><br><b>Contact Person</b><br><br>'.$cont_cp.'<br><br>';
$content .= '<b>2. Akta Pendirian</b><br><br>';
$content .= $cont_akta.'<br>';
$content .= '</page>';
//page 2
$content .= '<page>';
$content .= '<p style="text-align:center;font-weight:bold;"><img src="'.$url_logo.'" style="margin:0 auto;" ><br></p>';
$content .= '<b>3. Owner</b><br><br>';
$content .= $cont_owner.'<br><br>';
$content .= '<b>4. Pengurus</b><br><br>';
$content .= $cont_pengurus.'<br><br>';
$content .= '<b>5. Surat Legalitas</b><br><br>';
$content .= $cont_surleg.'<br><br>';
$content .= '<b>6. Referensi</b><br><br>';
$content .= $cont_ref.'<br><br>';
$content .= '<b>7. Afiliates</b><br><br>';
$content .= $cont_af.'<br><br>';
$content .= '</page>';
//page 3
$content .= '<page>';
$content .= '<p style="text-align:center;font-weight:bold;"><img src="'.$url_logo.'" style="margin:0 auto;" ><br></p>';
$content .= '<p>Selanjutnya, mohon persetujuan Bapak/Ibu atas hasil evaluasi sourcing tersebut. Atas
persetujuan/arahan Bapak/Ibu diucapkan terima kasih.</p>';
$content .= '<table>';
$content .= '<tr><td style="width:270px;">Unit Strategic Sourcing / JKTIBS </td><td style="width:270px;"></td></tr>';
$content .= '<tr><td style="height:100px;" colspan="2"></td></tr>';
$content .= '<tr><td><u>'.$this->tank_auth->get_username().'</u><br> Sourcing Koordinator</td><td>dikeluarkan pada : 16 Agustus 2016</td></tr>';
$content .= '</table>';

$content .= '<br><br><br><b>Persetujuan Evaluasi Sourcing : </b>';
$content .= '<table border="1" style="border-collapse:collapse;">';
$content .= '<tr><td style="width:180px"><b>Nama</b></td><td><b>Jabatan</b></td><td style="width:80px;"><b>Status</b></td><td style="width:80px;"><b>Tanggal</b></td></tr>';
$content .= '<tr><td>Triesandi A.</td><td>SM Strategic Sourcing</td><td></td><td></td></tr>';
$content .= '<tr><td>Mitra Piranti</td><td>SM Business Support Quality Assurance</td><td></td><td></td></tr>';
$content .= '<tr><td>M. Iksan</td><td>VP Business Support & General Affairs</td><td></td><td></td></tr>';

$content .= '</table>';
$content .= '<p>*Persetujuan diperoleh dari sistem e-Procurment yang merupakan dokumen resmi dan sah.</p>';
$content .= '</page>';

//load library html2pdf
require_once APPPATH.'./libraries/html2pdf/html2pdf.class.php';
//config pdf
$html2pdf = new HTML2PDF('P','A4','en', true, 'UTF-8', array(30, 10, 30, 10));
$namefile = 'berita_acara_evaluasi_'.$vendor->name;
$namefile = str_replace(array(' ', ':'), '_', $namefile);
$html2pdf->pdf->SetTitle("{$namefile}");

$html2pdf->WriteHTML($content);

if(!empty($is_saved))
{
	$html2pdf->Output("due_diligence_attachment/$namefile.pdf", 'F');
}
else
{
	$html2pdf->Output("$namefile.pdf");
}


 ?>
