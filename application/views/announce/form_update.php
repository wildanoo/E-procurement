<script type="text/javascript">
$(document).ready(function(){
   var baseUrl  = "<?php echo base_url(); ?>";
   $(function () { 	$("#upload1").hide();  });
   
   $('#id_company').change(function(){  
   		var id_comp = $('#id_company').val();
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'announce/get_office',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_comp='+id_comp,
               success: function(resp){
               	  $('#id_office').html(resp);
               }
            });
   }); 
   
   $('#id_cat').change(function(){
   		var id_cat = $('#id_cat').val();
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'announce/get_subcategory',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_cat='+id_cat,
               success: function(resp){
               	  $('#id_subcat').html(resp);
               }
            });
   });

   $("#detail").keyup(function(){
		var len   = $(this).val().length;
		$("#count-char").html(250-len);
	});

   $('#id_subcat').change(function(){  
   		var id_subcat = $('#id_subcat').val();
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax({
           url  : baseUrl + 'announce/get_vendor/',
           type : "POST",
           data : csrf +'='+ token +'&id_subcat='+id_subcat,
           success: function(resp){
           	  $('#id_vendor').html(resp);
           }
        });
   });

   $("#first_valdate").datetimepicker({
		format:'Y/m/d H:i',
		minDate: 0,
		maxDate: $("#end_valdate").val(),
		step:1,
		onChangeDateTime: function (dp,selectedDate) {
			var time = $("#first_valdate").val();

            $("#end_valdate").datetimepicker({minDate: selectedDate.val()});
            $("#publish_date").datetimepicker({maxDate: selectedDate.val()});
        }
	});

   $("#end_valdate").datetimepicker({
		format:'Y/m/d H:i',
		minDate: 0,
		minTime: '08:00',
		step:1,
		onChangeDateTime: function (dp,selectedDate) {
			var time = $("#first_valdate").val();
			var init = selectedDate.val();

            if (new Date($("#end_valdate").val()).getTime() < new Date($("#first_valdate").val()).getTime()) {
        		$("#publish_date").datetimepicker({maxDate: selectedDate.val()});
        	}
            $("#first_valdate").datetimepicker({maxDate: selectedDate.val()});
        }
	});

   $('#publish_date').datetimepicker({
		format:'Y/m/d H:i',
		step:1,
		minDate: 0,
		onChangeDateTime: function (dp,selectedDate) {
			var time = $("#first_valdate").val();
			var init = selectedDate.val();

			$("#first_valdate").datetimepicker({minDate: selectedDate.val()});
			$("#end_valdate").datetimepicker({minDate: selectedDate.val()});
            if (new Date($("#end_valdate").val()).getTime() < new Date($("#publish_date").val()).getTime()) {
        		$("#end_valdate").datetimepicker({maxDate: selectedDate.val()});
        	}
        }
	});

   // $('#id_area').change(function(){
   // 	alert("meh");
   // 		var id_area = $('#id_area').val();
   // 		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
   //      var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
   //      $.ajax({
   //         url  : baseUrl + 'announce/get_office',
   //         type : "POST",              		               
   //         data : csrf +'='+ token +'&id_area='+id_area,
   //         success: function(resp){
   //         	  $('#id_office').html(resp);
   //         }
   //      });
   // });

  //  $('.date_picker').datepicker({
  //       inline: true,
  //       dateFormat: 'yy/mm/dd',
  //       onSelect: function (selectedDate) {
  //           var id = $(this).attr("id");
  //           if (id == "first_valdate") {
  //               $("#end_valdate").datepicker('option', 'minDate', selectedDate);
  //               $("#publish_date").datepicker('option', 'maxDate', selectedDate);
  //           }
  //           else {
  //           	if (new Date($("#end_valdate").val()).getTime() < new Date($("#first_valdate").val()).getTime()) {
  //           		$("#publish_date").datepicker('option', 'maxDate', selectedDate);
  //           	}
  //               $("#first_valdate").datepicker('option', 'maxDate', selectedDate);
  //           }
  //       }
  //   });
     
  //  $('#publish_date').datepicker({  
  //       changeMonth: true, changeYear: true,
		// dateFormat: "yy/mm/dd", maxDate: $("#first_valdate").val().toString()
  //    });

   $("input:radio[name=content]").click(function () {
		var opt = $("input:radio[name=content]:checked").val();
		if (opt == '2'){
			$("#upload1").show(); $("#wyswyg").hide(); 
		} else {
			$("#wyswyg").show(); $("#upload1").hide();	}
     });    
}); 

</script>
<style type="text/css">
	.valign-top td{
		vertical-align: top !important;
	}
</style>
<?php echo form_open_multipart('announce/update',array('id'=>'form'));	$current_date = date('Y/m/d')?>
<div class="page-header" style="vertical-align: middle">
  <h3 style="display:inline-block">Update <?php echo $form; ?></h3>
</div> 

<div id="debug"></div>

<table width="100%" border="0">
<tr class="valign-top">
	<td><label>Title</label></td><td>:</td>
	<td class="form_tab_content" width="75%;">
		<div style="margin-top: -5px;">
			<ul class="nav nav-tabs test2">
			    <li class="active"><a data-toggle="tab" href="#t_idn" aria-expanded="true">Indonesia</a></li>
			    <li class=""><a data-toggle="tab" href="#t_eng" aria-expanded="false">English</a></li>
			</ul>

			<div class="tab-content">
			    <div id="t_idn" class="tab-pane fade active in" style="padding:15px;">
			      <input class="form-control" value="<?php echo $def->title_ind; ?>" type="text" name="title_ind" id="title" placeholder="Judul" class="form-control" required/>
			    </div>
			    <div id="t_eng" class="tab-pane fade" style="padding:15px;">
			      <input class="form-control" value="<?php echo $def->title_eng; ?>" type="text" name="title_eng" id="title" placeholder="Title" class="form-control" required/>
			    </div>
			</div>
		</div>
	</td>
</tr>
<!-- <tr>
	<td><label>Title</label></td><td>:</td>
	<td><input type="text" name="title" id="title" value="<?php //echo $def->title; ?>" placeholder="Title" class="form-control" required/></td>	
</tr> -->
<?php echo form_hidden('id',$def->id); ?>
<?php echo form_hidden('id_type',$def->content_type); ?>
<?php echo form_hidden('id_subtype',$def->subcontent_type); ?>
<?php if ($def->content_type != 1) { ?>
<tr>
	<td><label>Category</label></td><td>:</td>
	<td><?php 
		$prop_cat = 'class="form-control" id="id_cat"';
		echo form_dropdown('id_cat',$category,$def->id_cat,$prop_cat); ?>
	</td>	
</tr>
<tr>
	<td><label>Sub Category</label></td><td>:</td>
	<td>
		<select name="id_subcat" id="id_subcat" class="form-control">
			<?php if($subcat){ ?>		
			<option value="<?php echo $subcat->id; ?>"><?php echo $subcat->subcategory; ?></option> <?php } else { ?>
			<option value="">--Select Category--</option><?php } ?> 		
		</select>		
	</td>	
</tr>
<?php }?>
<?php
	 if ($need_vendor == 1) { ?>
	<tr class="vendor-select valign-top" style="display: table-row">
		<td><label>List of Participant(s)</label></td><td>:</td>
		<td>
			<select multiple="multiple" class="multi-select" id="vendor_multiselect" name="vendor_multiselect[]">
			<?php if ($vendor_lists) {
				echo $vendor_lists;
			} ?>
			</select>
			<br>
		</td>
	</tr>
<?php } ?>
<tr id="company_area">
	<td><label>Company</label></td><td>:</td>
	<td>
		<?php
		$prop_company = 'class="form-control" id="id_company"';			
		echo form_dropdown('id_company',$company,$def->id_company,$prop_company); ?>
	</td>
</tr>
<tr id="area">
	<td><label>Area</label></td><td>:</td>
	<td><?php
		$prop_area = 'class="form-control" id="id_area"';
		echo form_dropdown('id_area',$areas,$def->id_region,$prop_area); ?>
	</td>
</tr>
<tr id="office_area">
	<td><label>Office</label></td><td>:</td>
	<td><?php
		$prop_office = 'class="form-control" id="id_office"';
		echo form_dropdown('id_office',$offices,$def->id_office,$prop_office); ?>
	</td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr class="valign-top">
	<td><label>Overview</label></td><td>:</td>
	<td class="form_tab_content" width="75%;">
		<div style="margin-top: -5px;">
			<ul class="nav nav-tabs">
			    <li class="active"><a data-toggle="tab" href="#d_idn" aria-expanded="true">Indonesia</a></li>
			    <li class=""><a data-toggle="tab" href="#d_eng" aria-expanded="false">English</a></li>
			</ul>

			<div class="tab-content">
			    <div id="d_idn" class="tab-pane fade active in" style="padding:15px;">
			      (<span id="count-char"><strong><?php echo (250-strlen($def->detail_ind)); ?></strong></span> chars left)
			      <textarea name="detail_ind" id="detail_ind" maxlength="250" cols="20" rows="5" class="form-control detail-section" required><?php echo $def->detail_ind; ?></textarea>
			    </div>
			    <div id="d_eng" class="tab-pane fade" style="padding:15px;">
			      (<span id="count-char"><strong><?php echo (250-strlen($def->detail_eng)); ?></strong></span> chars left)
			      <textarea name="detail_eng" id="detail_eng" maxlength="250" cols="20" rows="5" class="form-control detail-section" required><?php echo $def->detail_eng; ?></textarea>
			    </div>
			</div>
		</div>
	</td>
</tr>
<?php echo form_hidden("creator",$username); ?>
<tr id="option">
 <td><label>Content</label></td><td>:</td>
  <td>
	  <input type="radio" name="content" value="1" checked> Typing &nbsp;&nbsp;
	  <input type="radio" name="content" value="2"> Upload<br>
  </td>
</tr>

<tr id="wyswyg">
	<td colspan="2">&nbsp;</td>
	<td class="form_tab_content" width="75%;">
		<div>
			<ul class="nav nav-tabs test2">
			    <li class="active"><a data-toggle="tab" href="#idn" aria-expanded="true">Indonesia</a></li>
			    <li class=""><a data-toggle="tab" href="#eng" aria-expanded="false">English</a></li>
			</ul>

			<div class="tab-content">
			    <div id="idn" class="tab-pane fade active in" style="padding:15px;">
			      <?php $data['content'] = $contents[0]; $this->load->view('announce/note',$data); ?>
			    </div>
			    <div id="eng" class="tab-pane fade" style="padding:15px;">
			      <?php $data['content'] = $contents[1]; $this->load->view('announce/note',$data); ?>
			    </div>
			</div>
		</div>
	</td>
</tr>
	
	<!-- <tr><td colspan="2">&nbsp;</td>			    
		<td id="wyswyg" width="75%;"><?php //$this->load->view('announce/note'); ?></td>					
	</tr> -->
		
	<tr id="upload1">
		<td colspan="2">&nbsp;</td>				
		<td class="form_tab_content" width="75%;">
			<div>
				<ul class="nav nav-tabs test2">
				    <li class="active"><a data-toggle="tab" href="#upidn" aria-expanded="true">Indonesia</a></li>
				    <li class=""><a data-toggle="tab" href="#upeng" aria-expanded="false">English</a></li>
				</ul>
				<?php $style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";?>
				<div class="tab-content">
				    <div id="upidn" class="tab-pane fade active in" style="padding:15px;">
				      <?php	
						$data  = array('type'=>'file','name'=> 'contentfileind','id' => 'file_upload_ind','value'=>'true','content'=>'Import','style'=>$style); 
						echo form_input($data); ?> <i>*.txt</i>
				    </div>
				    <div id="upeng" class="tab-pane fade" style="padding:15px;">
				      <?php	
						$data  = array('type'=>'file','name'=> 'contentfileeng','id' => 'file_upload_eng','value'=>'true','content'=>'Import','style'=>$style); 
						echo form_input($data); ?> <i>*.txt</i>
				    </div>
				</div>
			</div>
		</td>
	</tr>
	<tr id="upload2">
		<td colspan="2">&nbsp;</td>	
		<td><?php	
			$data2  = array('type'=>'file','name'=> 'imagefile','id' => 'file_upload','value'=>'true','content'=>'Import','style'=>$style); 
			echo form_input($data2); ?><?php echo $imgfile; ?>&nbsp;<i>*.jpg,gif,png</i>
		</td>
	</tr>
	<?php if ($def->content_type != 1) { ?>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr class="valign-top">
		<td><label>Validity Period</label></td><td>:</td>	
		<td>
			<div>
			    <input type="text" value="<?php echo str_replace("-","/",$def->first_valdate); ?>"  class="form-control date_picker" id="first_valdate" name="first_valdate">
		    </div>
		    <div>
			    <input type="text" value="<?php echo str_replace("-","/",$def->end_valdate); ?>"  class="form-control date_picker" id="end_valdate" name="end_valdate">
		    </div>
		</td>
	</tr>
	<?php } ?>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr class="valign-top">
		<td><label>Publish Date</label></td><td>:</td>
		<td><input type="text" value="<?php echo str_replace("-","/",$def->publish_date); ?>"  class="form-control" id="publish_date" name="publish_date"></td>
	</tr>
<?php if (isset($needAanwidzing)) { 
	if ($needAanwidzing) {?>
	<tr><td colspan="3">&nbsp;</td></tr>
<tr id="aanwidzing_option" class="valign-top">
 	<td><label>Aanwijzing Agenda</label></td><td>:</td>
 	<td class="form_tab_content" width="75%;">
		<div style="margin-top: -5px;">
			<ul class="nav nav-tabs test2">
			    <li class="active"><a data-toggle="tab" href="#aanwidzing_idn" aria-expanded="true">Indonesia</a></li>
			    <li class=""><a data-toggle="tab" href="#aanwidzing_eng" aria-expanded="false">English</a></li>
			</ul>
			<div class="tab-content">
			    <div id="aanwidzing_idn" class="tab-pane fade active in" style="padding:15px;">
 			    	<?php $data['content'] = $needAanwidzing[0]; $this->load->view('announce/aanwidzing',$data); ?>
 			    </div>
 			    <div id="aanwidzing_eng" class="tab-pane fade" style="padding:15px;">
 			    	<?php $data['content'] = $needAanwidzing[1]; $this->load->view('announce/aanwidzing',$data); ?>
 			    </div>
			</div>
		</div>
	</td>
</tr>
<?php }} ?>
<tr id="upload3">
		<td><label>Attachment</label></td><td>:</td>
		<td><?php
			$data3  = array('type'=>'file','name'=> 'pdffile','id' => 'file_upload','value'=>'true','content'=>'Import','style'=>$style); 
			echo form_input($data3); ?><?php echo $attfile; ?>&nbsp;<i>*.pdf</i>
		</td>
</tr>
<?php if(count($documentReq) > 0) {		
	foreach ($documentReq as $key => $value){
?>
<tr id="hdocreq">
	<td><label><?php echo $value->document_req; ?></label></td><td>:</td>
	<td><?php
		$data3  = array('type'=>'file','name'=> 'docreq['.$value->id.']','id' => 'file_upload','value'=>'true','content'=>'Import','style'=>$style); 
		echo form_input($data3); ?><?php echo $docreqfile[$key]; ?>&nbsp;<i>*.pdf</i>
	</td>
</tr>
<?php }
} ?>
<tr>
	<td colspan="3" style="text-align: right;">
		<button style="vertical-align: baseline;" type="submit" value="submit" name="button-process" class="btn btn-primary btn-sm"/>Submit</button>
		<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>"Cancel", 'style'=>"vertical-align: baseline;"); ?>
		<?php  echo anchor('announce/','Cancel',$prop); ?>
	</td>
</tr>
</table>
</form>
<script type="text/javascript">
	$('#vendor_multiselect').multiSelect({
    	selectableOptgroup: true,
    	// selectableHeader: "<div class='input-group'><span class='input-group-addon' id='selectable-addon'>@</span><input type='text' class='search-input form-control' autocomplete='off' placeholder='search selectable vendor...' aria-describedby='selectable-addon'></div>",
    	selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='search selectable vendor...'>",
		selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='search selected vendor...'>",
		afterInit: function(ms){
			var that = this,
		        $selectableSearch = that.$selectableUl.prev(),
		        $selectionSearch = that.$selectionUl.prev(),
		        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
		        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

		    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
		    .on('keydown', function(e){
		      if (e.which === 40){
		        that.$selectableUl.focus();
		        return false;
		      }
		    });

		    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
		    .on('keydown', function(e){
		      if (e.which == 40){
		        that.$selectionUl.focus();
		        return false;
		      }
		    });
		  },
		  afterSelect: function(){
		    this.qs1.cache();
		    this.qs2.cache();
		  },
		  afterDeselect: function(){
		    this.qs1.cache();
		    this.qs2.cache();
		  }
	});

	$('#id_company').change(function(){  
   		var id_comp = $('#id_company').val();
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax({
           url  : baseUrl + 'announce/get_office',
           type : "POST",              		               
           data : csrf +'='+ token +'&id_comp='+id_comp,
           success: function(resp){
           	  $('#id_office').html(resp);
           }
        });
   });

	$('#id_area').change(function(){
			var id_area = $('#id_area').val();
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	     var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	     $.ajax({
	        url  : baseUrl + 'announce/get_office',
	        type : "POST",              		               
	        data : csrf +'='+ token +'&id_area='+id_area,
	        success: function(resp){
	        	  $('#id_office').html(resp);
	        }
	     });
	});

	$('#form').submit(function(e){ 
		var type = $('#id_type').val();
		if (<?php echo $need_vendor?$need_vendor:0; ?> == 1) {
	   		if ($('.cbvendor').length == 0) {
	   			alert('The Category doesn\'t have list of participant(s). Please input the vendor(s) for this category.');
	   			e.preventDefault();
	   		}else if($('.cbvendor:checked').length < 1){
	   			alert('Please select the list of participant(s) for this category.');
	   			e.preventDefault();
	   		}
	   	}
    });
</script>

<?php echo form_close(); ?>