<div id="container">

<script type="text/javascript"> 
	var baseUrl   = "<?php echo base_url(); ?>";
	var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	var token     = '<?php echo $this->security->get_csrf_hash(); ?>';	
	
	$(document).ready(function(){	
	
		$('.checkall').on('click', function(){
			$(this).closest('table').find(':checkbox').prop('checked', this.checked);
		});
		
		$("#start_date").datetimepicker({
			format:'Y/m/d',
			timepicker:false,
			maxDate: $("#end_date").val(),
			onChangeDateTime: function (dp,selectedDate) {
				var time = $("#start_date").val();

	            $("#end_date").datetimepicker({minDate: selectedDate.val()});
	        }
		});

		$("#end_date").datetimepicker({
			format:'Y/m/d',
			minDate: 0,
			timepicker:false,
			onChangeDateTime: function (dp,selectedDate) {
				var time = $("#start_date").val();
				var init = selectedDate.val();

	            $("#start_date").datetimepicker({maxDate: selectedDate.val()});
	        }
		});

		$("#search_term").autocomplete({ 
			source: baseUrl + 'verification/get_tags',
			minLength:1
		}); 

		$('#reset_session').click(function(){ 
				$.post( baseUrl + 'verification/reset_browse', function( resp ) {
				  	document.location.href= baseUrl + 'verification/';
				});
		});
						 
	});	
		
</script> 
<style type="text/css">
.list-inline li{
	vertical-align: middle !important;   }
.btn-srch{	margin-bottom: 0;    }
</style>

<div id="debug"></div>


<div class="table">
	<div class="panel panel-default">
	    <div class="panel-heading">
	        <i class="fa fa-search"></i> <strong>Search Section</strong>
	    </div>	    
	    <?php echo form_open('verification/set_session_browse',array('id'=>'form-search','novalidate' => 'true')); ?>
	    <div class="panel-body">
	    	<ul class="list-inline">
	    		<li style="width:24%">
	    			<div class="input-group">
	    			<input type="text" value="" placeholder="date start" class="form-control date_picker" id="start_date" name="start_date">
	    			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	    			</div>
	    		</li>
	    		<li style="width:2%">to</li>
	    		<li style="width:24%">
	    			<div class="input-group">
	    			<input type="text" value="" placeholder="date end" class="form-control date_picker" id="end_date" name="end_date">
	    			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	    			</div>
	    		</li>
	    		<li style="width:34%"><div class="input-group" style="width:100%">
	    		<input placeholder="search by code or title..." type="text" value="" class="form-control" id="search_term" name="search_term"></div></li>
	    		<li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search">Search</button></div></li>
	    		<li style="width:7%"><div class="input-group">
	    		<button class="btn btn-srch btn-warning btn-sm" id="reset_session" title="Show All">Show All</button>
	    		</div></li>
	    	</ul>
	    </div>
	    <?php echo form_close()?>
	</div>
</div>
<hr/>

<div class="page-header">
	<h3>Verification List
	<div style="float:right">
		<a href="javascript:;" onclick="exporttofile()" class="btn btn-srch btn-success btn-sm">
		<i class="fa fa-file-excel-o" style="color:#FFFFFF"></i> Export To Excel</a>
	</div>
	</h3>
</div>

<?php echo $this->session->flashdata("message"); ?>
<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
        <tr>
            <th style="text-align: center;" >No</th>
            <th style="text-align: center;"> &nbsp; <input id="selectAll" class="checkall" type="checkbox" /> &nbsp; </th>
            <th>Registration Num.</th> 
            <th>Vendor Num.</th>            
            <th>Vendor Name</th> 
            <th>Category - Sub Category</th> 
            <th>Register Status</th>
            <th>Register Date</th>
            <th style="text-align: center;">Status</th> 
            <th style="text-align: center;">Action</th>
        </tr>       
	</thead>
    <tbody>
<?php
	
echo form_open('#',array('id'=>'form'));
echo form_hidden('total',count($browse));
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td style="text-align: center;"><?php  echo $num = $num+1; ?></td>
				<td style="text-align: center;"> <input type="checkbox" name="selected[]" id="checkbox-<?php  echo $num; ?>" value="<?php echo $row->id ; ?>" /> </td>
				<td><?php echo $row->reg_num ?></td>
				<td><?php if($row->vendor_num) echo $row->vendor_num; else echo "-"; ?></td>	
				<td><?php echo anchor("verification/set_vendor_session/".$row->id,$row->vendor_name); ?></td>
				<td><?php
						$select   = "(SELECT category  FROM category r WHERE r.id=l.id_category) as catName,
									 (SELECT subcategory  FROM subcategory s WHERE s.id=l.id_subcat) as subcatName"; 
						$category = $this->crud->browse("","vendor_category l","id_vendor",$row->id,"true",$select); $j = 1 ; 						
						foreach($category as $val){	echo $j.". ".$val->catName." - ".$val->subcatName."<br/>"; $j++; } unset($j);					 
				?></td>
				<td><?php echo ucwords($row->sts_reg); ?></td>
				<td style="text-align: center;"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
				
				<td style="text-align: center;">
				<?php 
					  if($this->permit->recon) { $field = "recon_number"; $check = "recon_number";  }
					  elseif($this->permit->sap) { $field = "sap_number";  $check = "recon_number"; }
					  else { $field = "srm_username";  $check = "sap_number"; }
					  $is_exist   = $this->crud->browse("","vendor_account","id_vendor",$row->id,"false",$field);
					  
					  if($is_exist->$field) $result = true;
					  else $result = false;					  
					  $stslabel =  ucwords(str_replace("_"," ",$field));
		
					  $label = $result ? "Completed" : "Waiting for input ".$stslabel;
					  $class = $result ? "label label-success" : "label label-warning";	?>
					<i class="<?php echo $class; ?>"><?php echo $label; ?></i>
				</td>
				<td style="text-align: center;"><?php 				
					$filled = $this->crud->browse("","vendor_account","id_vendor",$row->id,"false",$check);	
					if($filled->$check || $field == "recon_number"){ 
						$url       = "verification/set_vendor_session/".$row->id."/true";
						$btn_class = "btn btn-info btn-sm";
					} else { $url = "verification/"; $btn_class = "btn btn-default btn-sm";	}					
					$attr = array('class' =>$btn_class,'id'=>'btn_acct'); echo anchor($url,"Create Account",$attr);
				?></td> 
				
			</tr>
<?php	$i++;  } ?>	
<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?>
	<tr><td colspan="9" align="center">-</td></tr>
<?php } ?>
<?php echo form_close();?>

    </tbody>
    
</table>

</div>

