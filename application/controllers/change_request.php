<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class change_request extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('m_datavendor', 'm_change_request','m_user'));
    //Codeigniter : Write Less Do More
  }

  function index()
  {
    if ($this->m_change_request->authenticate())
    {

    $data['user_id']  = $this->tank_auth->get_user_id();
    $data['username'] = $this->tank_auth->get_username();
    $data['view'] = "change_request/home";
    $this->load->view('layout/template',$data);
    }
    else
    {
      redirect('home');
    }
  }

  function browse()
  {
    $user_id = $this->tank_auth->get_user_id();

    $select      = "id,vendor_num,vendor_name,vendor_type,
                      (select count(*) from vendor_changedata where id_vendor=b.id and status='request') as request ";
    $table      = "vendor b";
    $order      = array('field'=>'request','order'=>'DESC');
    $where      = array('vendor_type' => 'avl');
    $page       = $this->uri->segment(3);
    $per_page   = 10;
    $offset     = $this->crud->set_offset($page,$per_page);
    $total_rows = $this->crud->get_total_record("",$table,$where);
    $set_config = array('base_url'=>base_url().'change_request/browse','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
    $config     = $this->crud->set_config($set_config);

    $this->load->library('pagination');
    $this->pagination->initialize($config);
    $paging = $this->pagination->create_links();

    $data['pagination']  = $paging;
    $data['num']         = $offset;

    $browse = $this->crud->browse_with_paging("",$table,"","","true",$select,$where,$order,$per_page,$offset,"");
	 
    $data['title']    = 'Change Request';
    $data['browse']   = $browse;

    $data['view'] = "change_request/browse";

    $join_cat[0][0] = 'subcategory b';
    $join_cat[0][1] = 'a.id=b.id_cat';
    $join_cat[0][2] = 'right';
    $data['subcat'] = $this->crud->browse_join("","category a","","","true","a.id as id_cat,b.id as id_subcat,a.category,b.subcategory",$join_cat);
   
    if($this->input->is_ajax_request())
    {
      $this->load->view($data['view'], $data);
    }
    else
    {
      $this->load->view('layout/template',$data);
    }

  }

  function detail($venID)
  {
    if ($this->m_change_request->authenticate())
    {
      $get_id = $this->input->get('id');
      
      $this->session->set_userdata( array('venID'=>$get_id) );
      $user_id = $this->tank_auth->get_user_id();
      $venID = $get_id;
      // print $venID;
      $select      = "a.*, (select username from users where id=a.created_id) as username";
      $table       = "vendor_changedata a";
      $where       = array('id_vendor' => $venID, 'status'=>'request');
      $order       = array('field'=>'last_updated','order'=>'DESC');
     
      $page = $this->uri->segment(3);

      $per_page   = 10;
      $offset     = $this->crud->set_offset($page,$per_page);
      $total_rows = $this->crud->get_total_record("",$table,$where);
      $set_config = array('base_url'=>base_url().'change_request/detail','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3, 'suffix' => '?' . http_build_query($_GET, '', "&"), 'first_url' => base_url().'change_request/detail?'.http_build_query($_GET) );
      $config     = $this->crud->set_config($set_config);

      $this->load->library('pagination');
      $this->pagination->initialize($config);
      $paging = $this->pagination->create_links();

      $data['pagination']  = $paging;
      $data['num']         = $offset;

      $browse = $this->crud->browse_with_paging("",$table,"","","true",$select,$where,$order,$per_page,$offset,"");
	//print $this->db->last_query();
	//exit();

      $data['title']    = 'Change Request Data';
      $data['browse']   = $browse;
      $data['vendor'] = $this->m_datavendor->get_data_vendor($venID);
      $data['categories'] = $this->m_change_request->get_data_category($venID);
      $data['cp'] = $this->crud->browse("","temp_contact_person","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
      $data['owner']  = $this->crud->browse("","temp_owner","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
      $data['org']   = $this->crud->browse("","temp_organization","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
       $data['ref']   = $this->crud->browse("","temp_references","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
      $data['aff']   = $this->crud->browse("","temp_affiliates","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
      $data['correspondence'] = $this->crud->browse("","temp_correspondence","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
     
      $data['view'] = "change_request/browse_detail";
      if($this->input->is_ajax_request())
      {
        $this->load->view($data['view'], $data);
      }
      else
      {
        $this->load->view('layout/template',$data);
      }
    }
    else
    {
      redirect('home');
    }

  }


  function approve_old()
  {
    $ck = $this->input->post('my_ck');
    $filter = implode(',', $ck);
    foreach ($ck as $value)
    {
      $this->crud->update("",'vendor_changedata','id',$value,array('status' => 'approved' ) );
    }
    $where = "id IN($filter)";
    $data = $this->crud->browse('','vendor_changedata','','','true','',$where);

    $curr_date  = date('Y-m-d H:i:s'); 

    foreach ($data as $key => $value)
    {
      $rule1 = array('bank_account', 'akta');
      $rule2 = array(' vendor_category', 'contact_person','owner', 'organization', 'references', 'affiliates');
      
      if($value->table == 'vendor')
      {
        $dt_upd = array($value->fieldname => $value->after, 'created_id'=>$value->created_id, 'last_updated'=>$curr_date);
        $this->crud->update('',$value->table, 'id', $value->id_vendor,$dt_upd);
        print $this->db->last_query();
        
      }
      elseif($value->table == 'bank_account')
      {
        $where     = array('id_vendor' => $value->id_vendor);
        $is_exist  = $this->crud->is_exist("",$value->table,'id',$where); 
        if(!$is_exist)
        {
          $this->crud->insert('', $value->table, array('id_vendor'=>$value->id_vendor, 'created_id'=>$value->created_id,'created_date'=>$curr_date, 'last_updated'=>$curr_date) );
        }
        $dt_upd = array($value->fieldname => $value->after, 'created_id'=>$value->created_id, 'last_updated'=>$curr_date);
        $this->crud->update('',$value->table, 'id_vendor', $value->id_vendor,$dt_upd);
        print $this->db->last_query();
      }
      elseif($value->table == 'akta')
      {
        $where     = array('id_vendor' => $value->id_vendor);
        $is_exist  = $this->crud->is_exist("",$value->table,'id',$where); 
        if(!$is_exist)
        {
          $this->crud->insert('', $value->table, array('id_vendor'=>$value->id_vendor, 'created_id'=>$value->created_id,'created_date'=>$curr_date, 'last_updated'=>$curr_date) );
        }
        $dt_upd = array($value->fieldname => $value->after, 'created_id'=>$value->created_id, 'last_updated'=>$curr_date);
        $this->crud->update('',$value->table, 'id_vendor', $value->id_vendor,$dt_upd);
        print $this->db->last_query();
      }
      elseif($value->table == 'vendor_category')
      {
        $dt_upd = array($value->fieldname => $value->after, 'created_id'=>$value->created_id, 'last_updated'=>$curr_date);
        $this->crud->update('',$value->table, 'id', $value->index, $dt_upd);
        print $this->db->last_query();
      }
      elseif($value->table == 'contact_person')
      {
        $dt_upd = array($value->fieldname => $value->after, 'created_id'=>$value->created_id, 'last_updated'=>$curr_date);
        $this->crud->update('',$value->table, 'id', $value->index, $dt_upd);
        print $this->db->last_query();
      }
       elseif($value->table == 'owner')
      {
        $dt_upd = array($value->fieldname => $value->after, 'created_id'=>$value->created_id, 'last_updated'=>$curr_date);
        $this->crud->update('',$value->table, 'id', $value->index, $dt_upd);
        print $this->db->last_query();
      }
      elseif($value->table == 'organization')
      {
        $dt_upd = array($value->fieldname => $value->after, 'created_id'=>$value->created_id, 'last_updated'=>$curr_date);
        $this->crud->update('',$value->table, 'id', $value->index, $dt_upd);
        print $this->db->last_query();
      }
       elseif($value->table == 'references')
      {
        $dt_upd = array($value->fieldname => $value->after, 'created_id'=>$value->created_id, 'last_updated'=>$curr_date);
        $this->crud->update('',$value->table, 'id', $value->index, $dt_upd);
        print $this->db->last_query();
      }
       elseif($value->table == 'affiliates')
      {
        $dt_upd = array($value->fieldname => $value->after, 'created_id'=>$value->created_id, 'last_updated'=>$curr_date);
        $this->crud->update('',$value->table, 'id', $value->index, $dt_upd);
        print $this->db->last_query();
      }
      else
      {
        print 'error';
      }

    }


  }

  function approve()
  {
    $ck = $this->input->post('my_ck');
    $filter = implode(',', $ck);
    foreach ($ck as $value)
    {
      $this->crud->update("",'vendor_changedata','id',$value,array('status' => 'approved' ) );
    }
    $where = "id IN($filter)";
    $data = $this->crud->browse('','vendor_changedata','','','true','',$where);

    $curr_date  = date('Y-m-d H:i:s'); 

    $rule1 = array('bank_account', 'akta');
    $rule2 = array('vendor_category', 'contact_person','owner', 'organization', 'references', 'affiliates','correspondence');
    print_r($data);
    foreach ($data as $key => $value)
    {
      if($value->table == 'vendor')
      {
        $dt_upd = array($value->fieldname => $value->after, 'created_id'=>$value->created_id, 'last_updated'=>$curr_date);
        $this->crud->update('',$value->table, 'id', $value->id_vendor,$dt_upd);
        print $this->db->last_query();
        
      }
      elseif(in_array($value->table, $rule1))
      {
        $where     = array('id_vendor' => $value->id_vendor);
        $is_exist  = $this->crud->is_exist("",$value->table,'id',$where); 
        if(!$is_exist)
        {
          $this->crud->insert('', $value->table, array('id_vendor'=>$value->id_vendor, 'created_id'=>$value->created_id,'created_date'=>$curr_date, 'last_updated'=>$curr_date) );
        }
        $dt_upd = array($value->fieldname => $value->after, 'created_id'=>$value->created_id, 'last_updated'=>$curr_date);
        $this->crud->update('',$value->table, 'id_vendor', $value->id_vendor,$dt_upd);
        print $this->db->last_query();
      }
     
      elseif(in_array($value->table, $rule2))
      {
        if($value->fieldname == 'doc_file')
        {
          //hapus file lama
          if(file_exists('uploads/documents/'.$value->before))
          {
            unlink('uploads/documents/'.$value->before);
          }
          //update filetype
          $newfile = explode('.', $value->after);
          $type = array_pop($newfile);
          $this->crud->update('','correspondence','id',$value->index, array('filetype'=>$type));
          //move file to original folder
         rename('uploads/documents/_temp/'.$value->after, 'uploads/documents/'.$value->after);
        }
        else
        { 

        $dt_upd = array($value->fieldname => $value->after, 'created_id'=>$value->created_id, 'last_updated'=>$curr_date);
        $this->crud->update('',$value->table, 'id', $value->index, $dt_upd);
        print $this->db->last_query();
        }
        //move file
      }
     
      else
      {
        print 'error';
      }

    }


  }

 

  function approve_category()
  {
    $data = $this->input->post('data');
    $id = $data[0];
    $action = $data[1];
    $id_vendor = $data[2];
    $id_subcat = $data[3];
    //update status jadi 1/approve
    $this->crud->update('','temp_vendor_category', 'id', $id, array('status'=>1));
    if($action == 'add')
    {
      //insert temp data to original table
      $q = "insert into vendor_category (id_vendor,id_category,id_subcat,created_id,created_date,last_updated)
      select id_vendor, id_category, id_subcat,created_id,created_date,now() from temp_vendor_category where id = $id ";
      $this->db->query($q);
    }
    elseif($action == 'delete')
    {
      $this->db->delete('vendor_category', array('id_vendor'=>$id_vendor, 'id_subcat'=>$id_subcat) );
    }

  }

  function load_category()
  {
     $venID = $this->session->userdata('venID');
     $data['categories'] = $this->m_change_request->get_data_category($venID);
     $this->load->view('change_request/data/categories', $data);
   
  }

  function approve_contact()
  {
    $data = $this->input->post('data');
    $id = $data[0];
    $action = $data[1];
    $id_table = $data[2];
    //update status jadi 1/approve
    print_r($data);
    // exit();
    $this->crud->update('','temp_contact_person', 'id', $id, array('status'=>1));
    if($action == 'add')
    {
      //insert temp data to original table
       $q = "insert into contact_person (id_vendor,fullname,position,mobile,email,created_id,created_date,last_updated)
      select id_vendor,fullname,position,mobile,email,created_id,created_date,now() from temp_contact_person where id = $id ";
     
      $this->db->query($q);
    }
    elseif($action == 'delete')
    {
      $this->db->delete('contact_person', array('id'=>$id_table) );
    }

  }

  function load_contact()
  {
     $venID = $this->session->userdata('venID');
     $data['cp'] = $this->crud->browse("","temp_contact_person","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));

     $this->load->view('change_request/data/contact', $data);
  }

  function approve_owner()
  {
    $data = $this->input->post('data');
    $id = $data[0];
    $action = $data[1];
    $id_table = $data[2];
    //update status jadi 1/approve
    print_r($data);
    // exit();
    $this->crud->update('','temp_owner', 'id', $id, array('status'=>1));
    if($action == 'add')
    {
      //insert temp data to original table

       $q = "insert into owner (id_vendor,owner_name,owner_address,owner_phone,owner_position,owner_shared,remark,status,created_id,created_date,last_updated)
      select id_vendor,owner_name,owner_address,owner_phone,owner_position,owner_shared,remark,status,created_id,created_date,now() from temp_owner where id = $id ";
      $this->db->query($q);
    }
    elseif($action == 'delete')
    {
      $this->db->delete('owner', array('id'=>$id_table) );
    }

  }

  function load_owner()
  {
     $venID = $this->session->userdata('venID');
     $data['owner']  = $this->crud->browse("","temp_owner","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));

     $this->load->view('change_request/data/owner', $data);
  }

  function approve_org()
  {
    $data = $this->input->post('data');
    $id = $data[0];
    $action = $data[1];
    $id_table = $data[2];
    //update status jadi 1/approve
    print_r($data);
    // exit();
    $this->crud->update('','temp_organization', 'id', $id, array('status'=>1));
    if($action == 'add')
    {

       $q = "insert into organization (id_vendor,name,address,phone,position,remark,status,created_id,created_date,last_updated)
      select id_vendor,name,address,phone,position,remark,status,created_id,created_date,now() from temp_organization where id = $id ";
      $this->db->query($q);
    }
    elseif($action == 'delete')
    {
      $this->db->delete('organization', array('id'=>$id_table) );
    }
    print $this->db->last_query();

  }

  function load_org()
  {
     $venID = $this->session->userdata('venID');
     $data['org']   = $this->crud->browse("","temp_organization","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
   
     $this->load->view('change_request/data/org', $data);
  }

  function approve_ref()
  {
    $data = $this->input->post('data');
    $id = $data[0];
    $action = $data[1];
    $id_table = $data[2];
    //update status jadi 1/approve
    print_r($data);
    // exit();
    $this->crud->update('','temp_references', 'id', $id, array('status'=>1));
    if($action == 'add')
    {
       $q = "insert into `references` (id_vendor,cust_name,project,point,date,remark,status,created_id,created_date,last_updated)
      select id_vendor,cust_name,project,point,date,remark,status,created_id,created_date,now() from `temp_references` where id = $id ";
      $this->db->query($q);
    }
    elseif($action == 'delete')
    {
      $this->db->delete('references', array('id'=>$id_table) );
    }
    print $this->db->last_query();

  }

  function load_ref()
  {
     $venID = $this->session->userdata('venID');
    $data['ref']   = $this->crud->browse("","temp_references","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
   
     $this->load->view('change_request/data/reference', $data);
  }

  function approve_aff()
  {
    $data = $this->input->post('data');
    $id = $data[0];
    $action = $data[1];
    $id_table = $data[2];
    //update status jadi 1/approve
    print_r($data);
    // exit();
    $this->crud->update('','temp_affiliates', 'id', $id, array('status'=>1));
    if($action == 'add')
    {
       $q = "insert into `affiliates` (id_vendor,company_name,competency,contact,remark,status,created_id,created_date,last_updated)
      select id_vendor,company_name,competency,contact,remark,status,created_id,created_date,now() from `temp_affiliates` where id = $id ";
      $this->db->query($q);
    }
    elseif($action == 'delete')
    {
      $this->db->delete('affiliates', array('id'=>$id_table) );
    }
    print $this->db->last_query();

  }

  function load_aff()
  {
     $venID = $this->session->userdata('venID');
     $data['aff']   = $this->crud->browse("","temp_affiliates","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
   
     $this->load->view('change_request/data/affiliate', $data);
  }

  function approve_corresp()
  {
    $data = $this->input->post('data');
    $id = $data[0];
    $action = $data[1];
    $id_table = $data[2];
    //update status jadi 1/approve
    print_r($data);
    $dm = $this->db->get_where('temp_correspondence', array('id'=>$id))->row();
    print_r($dm);
    // exit();
    $this->crud->update('','temp_correspondence', 'id', $id, array('status'=>1));
    if($action == 'add')
    {
       $q = "insert into `correspondence` (id_vendor,doc_name,doc_num,`desc`,eff_date,exp_date,remark,filetype,status,created_id,created_date,last_updated)
      select id_vendor,doc_name,doc_num,`desc`,eff_date,exp_date,remark,filetype,status,created_id,created_date,now() from `temp_correspondence` where id = $id ";
      $this->db->query($q);
      $name = $this->db->insert_id();
      //chcek if exist uploaded file, so move to original folder
      if(file_exists('uploads/documents/_temp/'.$dm->id.'.'.$dm->filetype)):
        rename('uploads/documents/_temp/'.$dm->id.'.'.$dm->filetype, 'uploads/documents/'.$name.'.'.$dm->filetype);
      endif;
    }
    elseif($action == 'delete')
    {
      $this->db->delete('correspondence', array('id'=>$id_table) );
      //check if file exist so delete 
       if(file_exists('uploads/documents/'.$dm->id_tbl.'.'.$dm->filetype)):
        unlink('uploads/documents/'.$dm->id_tbl.'.'.$dm->filetype);
      endif;
    }
    print $this->db->last_query();

  }

  function load_corresp()
  {
     $venID = $this->session->userdata('venID');
     $data['correspondence'] = $this->crud->browse("","temp_correspondence","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
     $this->load->view('change_request/data/correspondence', $data);
  }

   function send_approveMail()
  {
    $venID = $this->session->userdata('venID');
  
    $recepient = $this->m_user->get_userdata_group('as');
    $form = 'EPCGA057';
    
    $select  = "register_num as num,vendor_name as name,email";
    $vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);

    $subject =  "Notification Change Data Approval | E-procurement Garuda";

    foreach($recepient as $value)
    {
        if(filter_var($value->email, FILTER_VALIDATE_EMAIL)) 
        {
          $user    = $this->users->get_user_by_email($value->email);
          if($user)
          {           
            $require = array('email'=>$user->email,'username'=>$user->username,'request'=>'data_vendor','param'=>$venID); 
            
            $link    = $this->m_change_request->create_session_link((object) $require);
            
            $send_to = (empty($value->username) ) ? $value->fullname : $value->username;
            $to      =  $value->email;
            $param   =  array('reg_num'     => $vendor->num,
                              'vendor_name' => $vendor->name,
                              'link'        => $link,
                              'to'          => $send_to );
            $param['view']  = NOTIF_PATH.$form;    
            $message    = $this->load->view(NOTIF_TMPL,$param,true); 

            // $message =  $this->load->view("change_request/notification/{$form}",$param,true);
            $this->crud->sendMail($to,$subject,$message);
          }
       }
    }


  }

  function send_rejectMail()
  {
    $venID = $this->session->userdata('venID');
    
    $recepient = $this->m_user->get_userdata_group('as');
    $form = 'EPCGA058';
    
    $select  = "register_num as num,vendor_name as name,email";
    $vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);

    $subject =  "Notification Change Data Approval | E-procurement Garuda";

    foreach($recepient as $value)
    {
      if(filter_var($value->email, FILTER_VALIDATE_EMAIL)) 
      {

        $user    = $this->users->get_user_by_email($value->email);
        if($user)
        {           
          $require = array('email'=>$user->email,'username'=>$user->username,'request'=>'data_vendor','param'=>$venID); 
          $link    = $this->m_change_request->create_session_link((object) $require);
          $send_to = (empty($value->username) ) ? $value->fullname : $value->username;
          
          $to      =  $value->email;
          $param   =  array('reg_num' => $vendor->num,
          'vendor_name' => $vendor->name,
          'link'        => $link,
          'to'          => $send_to);
          $param['view']  = NOTIF_PATH.$form;    
          $message    = $this->load->view(NOTIF_TMPL,$param,true);
          $this->crud->sendMail($to,$subject,$message);
        }
      }
    }
  }

  function reject_temp_table($id=null,$table=null)
  {
    if(empty($id))
    {
      $id = $this->input->post('id');
      $table = $this->input->post('table');
    }
    $this->crud->update('',$table, 'id', $id, array('status'=>'-1'));
    // print $this->db->last_query();
    switch ($table) {
      case 'temp_vendor_category':
        $result = 'cat';
        break;
      case 'temp_contact_person':
        $result = 'contact';
        break;
      case 'temp_owner':
        $result = 'owner';
        break;
      case 'temp_organization':
        $result = 'org';
        break;
      case 'temp_references':
        $result = 'ref';
        break;
      case 'temp_affiliates':
        $result = 'aff';
        break;
      case 'temp_correspondence':
        $result = 'corresp';
        break;
      default:
        # code...
        break;
    }
    echo $result;
  }

  function reject_datachange($ck=null)
  {
      if(empty($ck))
      {
        $ck = $this->input->post('my_ck');
      }

      foreach ($ck as $value)
      {
        $this->crud->update("",'vendor_changedata','id',$value,array('status' => 'rejected' ) );
      }  
   
    print $this->db->last_query();
  }

   function load_temp_table($table)
  {
     $venID = $this->session->userdata('venID');
      switch ($table) {
        case 'cat':
           $data['categories'] = $this->m_change_request->get_data_category($venID);
           $this->load->view('change_request/data/categories', $data);
          break;
        case 'contact':
          $data['cp'] = $this->crud->browse("","temp_contact_person","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
          $this->load->view('change_request/data/contact', $data);
          break;
        case 'owner':
          $data['owner']  = $this->crud->browse("","temp_owner","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
          $this->load->view('change_request/data/owner', $data);
          break;
        case 'org':
          $data['org']   =$this->crud->browse("","temp_organization","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
          $this->load->view('change_request/data/org', $data);
          break;
        case 'ref':
          $data['ref'] = $data['ref']   = $this->crud->browse("","temp_references","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
          $this->load->view('change_request/data/reference', $data);
          break;
        case 'aff':
          $data['aff']   = $this->crud->browse("","temp_affiliates","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
          $this->load->view('change_request/data/affiliate', $data);
          break;
        case 'corresp':
          $data['correspondence'] = $this->crud->browse("","temp_correspondence","id_vendor",$venID,"true",'',array('status'=>'0'), array('field'=>'last_updated', 'sort'=>'desc'));
          $this->load->view('change_request/data/correspondence', $data);
          break;
        default:
          # code...
          break;
      }
  }



 


}
