<?php

/* author  : richo mahardika */
/* created : 10 august 2016 17:01 */

//PAGES
// $lang['home_page']		 = 'auth/login';
// $lang['navbar_top']		 = 'main/eng/navbar_top';
// $lang['sidebar_nav']	 = 'main/eng/sidebar_nav';

//PAGE STRUCTURE GLOBAL
$lang['global_registration_tag'] 	 = 'Registration';
$lang['global_vendor_tag'] 			 = 'Vendor Policy';
$lang['global_home_tag'] 			 = 'Home';
$lang['global_newsannouncement_tag'] = 'News & Announcement';
$lang['global_contactus_tag']  		 = 'Contact Us';
$lang['global_recent_post_tag'] 	 = 'Recent Post';
$lang['global_search_to_tag']		 = 'to';
$lang['global_readmore_tag']		 = 'Read More';
$lang['global_pagingprev_tag']  	 = 'Previous';
$lang['global_pagingnext_tag']  	 = 'Next';
$lang['global_category_tag']		 = 'Category';
$lang['global_subcategory_tag']		 = 'Sub Category';
$lang['global_title_tag']			 = 'Title';
$lang['global_regnum_tag']			 = 'Registration Number';
$lang['global_startdate_tag']		 = 'Start Date';
$lang['global_enddate_tag']			 = 'End Date';
$lang['global_publishdate_tag']		 = 'Publish Date';
$lang['global_technical_tag'] 	 	 = 'Technical Guidance';

//PAGE STRUCTURE HOME
$lang['section1_sub1']	 = 'WELCOME TO';
$lang['section2']		 = 'NEWS AND ANNOUNCEMENT';
$lang['section2_sub1']	 = 'NEWS';
$lang['section2_sub2']	 = 'ANNOUNCEMENT';
$lang['section2_sub3']	 = 'Continue Reading';
$lang['section2_sub4']	 = 'Details';
$lang['section2_sub5']	 = 'Show All';
$lang['section4']		 = 'CONTACT US';
$lang['section4_sub1']	 = 'Lower Ground, Garuda Management Building';
$lang['section4_sub2']	 = 'Soekarno-Hatta Airport, Jakarta';
$lang['section4_sub3']	 = 'Contact';
$lang['section5_sub1']	 = 'Member Login';
$lang['section5_sub2']	 = 'Registration and Vendor Policy';
$lang['section5_sub3']	 = 'How can I register my Company as a Vendor?';
$lang['section5_sub4']	 = 'You can register your company by fill the form on';
$lang['section5_sub5']	 = 'this link';
$lang['section5_sub6']	 = 'How can I find out which policy is being applied to Vendor?';
$lang['section5_sub7']	 = 'You can read the following Vendor Policies by clicking';
$lang['section6']		 = 'Registration';
$lang['section6_sub1']   = 'Check Registration Status';
$lang['section7']		 = 'Vendor Policy';

//PAGE STRUCTURE NEWS
$lang['news_title'] 	 = 'NEWS';
$lang['news_subtitle'] 	 = 'List of News';

//PAGE STRUCTURE ANNOUNCEMENT
$lang['announcement_title'] 	   = 'ANNOUNCEMENT';
$lang['announcement_subtitle'] 	   = 'List of Announcements';
$lang['announcement_thead_catsub'] = 'Category/Sub Category';
$lang['announcement_thead_type']   = 'Type of Invitation';

//PAGE STRUCTURE PAGE DETAIL
$lang['page_detail_prev_article']  = 'Previous Article';
$lang['page_detail_next_article']  = 'Next Article';

//FILES
$lang['about_us_file']   = 'uploads/cms/aboutus_eng.txt';
$lang['contact_us_file'] = 'uploads/cms/contact_eng.txt';
$lang['faq_file']		 = 'uploads/cms/faq_eng.txt';
$lang['general_file']	 = 'uploads/policy_agreement/general_eng';
$lang['open_sourcing']	 = 'uploads/policy_agreement/open_sourcing_eng';
$lang['open_bidding']	 = 'uploads/policy_agreement/open_bidding_eng';
$lang['direct_selection']= 'uploads/policy_agreement/direct_selection_eng';
$lang['question']		 = 'question_eng';
$lang['answer']			 = 'answer_eng';

//
$lang['header_file_technical'] = "Technical Guidance";