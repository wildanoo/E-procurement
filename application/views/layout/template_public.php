<!-- begin HEADER -->
<?php $this->load->view('layout/header_public');?>
<!-- end HEADER -->

<!-- begin BODY -->
<div class="complete-content">
<?php if($view) $this->load->view($view);?>
</div>
<!-- end BODY -->

<!-- begin FOOTER -->
<?php $this->load->view('layout/footer_public');?>
<!-- end FOOTER -->