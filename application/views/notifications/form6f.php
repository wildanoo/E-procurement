<?php $this->load->view('notifications/header');?>

<br>Dengan hormat,<br/>
<b><?php echo $vname;?></b><br>
<i><?php echo $address;?></i><br/><br/>

Dengan ini kami informasikan bahwa perusahaan Bapak/Ibu telah berhasil mendaftar untuk mengikuti bidding di PT. Garuda Indonesia (Persero) Tbk. dengan nomor registasi <b style="color:green"><?php echo $rnum;?></b><br/><br/>
			
Sebagai vendor untuk kategori <?php echo $category;?> (<?php echo $subcategory;?>)<br/><br/>

Untuk proses selanjutnya akan kami informasikan melalui email ini.<br/><br/>

Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih.<br/><br/>
<br/><br/>
<?php echo $sender;?>.
    
<?php $this->load->view('notifications/footer');?>