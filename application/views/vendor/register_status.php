<script type="text/javascript">
  $(function () { 
	   var baseUrl = "<?php echo base_url(); ?>";
	   var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	   var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
   
	   $('#btn_change').click(function(){ 
	   		var next_id = $("#next_id").val();	         		
			var values  = $("#form_approved").serializeArray();
			values.push({name: csrf ,value: token});
			values = jQuery.param(values);						
			$.ajax({
					url  : baseUrl + 'vendor/approved_process',
					type : "POST",              		               
					data : values,
					success: function(resp){						
						if(next_id==3 || next_id==4){ document.location.href= baseUrl + 'register/detail';
						} else { $("#info_status").load(baseUrl+'vendor/load_status_info'); } 
					}
			});		
	   		
	   });

	   $('#btn_reject').click(function(){ 
			$.ajax({
					url  : baseUrl + 'vendor/rejected_vendor',
					type : "POST",              		               
					data : csrf + '=' + token,
					success: function(resp){
						document.location.href= baseUrl + 'register/detail';
					}
			});		
	   		
	   });	  
	
   });
   
   function finalization(id){ 
		var baseUrl   = "<?php echo base_url(); ?>";
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	    var token     = '<?php echo $this->security->get_csrf_hash(); ?>';
		if (confirm('Are you sure you want to vendor finalization ?')) { 
		   	$.post( baseUrl + "vendor/finalization", { csrf : token ,id_vendor : id }).done(function( resp ) {
		   		var json = $.parseJSON(resp);	
		   		if(json.status=="false") { $("#message").html(json.msg);
				} else { document.location.href= baseUrl + 'register/'; }
				//$('#debug_temp').html(resp);
			});
		}			
	}	
    
</script>

<div id="debug_temp"></div>
<div id="message" style="color: red;"></div>
<?php echo form_open('#',array('id'=>'form_approved')); $next_lbl= "end"; ?>
<table width="80%">
	<tr><td>Supplier Name</td><td>:</td><td><h1><?php  echo $vendor->name;  ?></h1></td></tr>
	<tr><td>Registration Code</td><td>:</td><td><h1><?php  echo $vendor->reg_num;  ?></h1></td></tr>
	<tr><td>Supplier Number</td><td>:</td><td><h1><?php  if($vendor->num) echo $vendor->num; else echo "-";  ?></h1></td></tr>
	<?php	$sess_url = $this->session->userdata("feature");
		 	$url      = !$sess_url ? "register" : $sess_url;  ?>	
	
	<?php if($vendor->type!="avl" && $url=="register"){ ?>	
	<tr><td>Registration Status</td><td>:</td><td>      
		<?php 	$next = $vendor->id_stsreg + 1; 
				foreach($status as $key=>$val){ 
					if($key == $next)  $next_lbl = $val;				
					if($key<=$vendor->id_stsreg)  echo ucwords(strtolower($val));  
					if($key < $vendor->id_stsreg) {
					echo "&nbsp;&nbsp;&nbsp;<i class='glyphicon glyphicon-forward'></i>&nbsp;&nbsp;&nbsp;";	}
				}
		?>
	</td></tr>	
	<?php } ?>
		
</table>
	
		<div style="text-align: right;">	
		<?php if($vendor->type!="reject" && $this->permit->msr && $url=="register"){  ?>
				<input type="hidden" id="next_id" name="next_id" value="<?php echo $next; ?>" />	   
			<?php if($next_lbl!="end" && $next<=4 ){ ?>
				<input type="button" id="btn_change" value="<?php echo ucwords(strtolower($next_lbl)); ?>" class="btn btn-warning btn-sm" /><?php } ?>
		  	<?php if($vendor->id_stsreg==1){  ?>
		  		<input type="button" id="btn_reject" value="Rejected" class="btn btn-danger btn-sm"/><?php } ?>
		  	<?php if($this->m_vendor->is_finalize($vendor->result,$vendor->approval,$vendor->id_stsreg) && $this->permit->fvr ) {	?>  	
		  		<input type="button" id="btn_finalize" value="Finalization" onclick="<?php echo "finalization($vendor->id)"; ?>" 
		  		class="btn btn-warning btn-sm"/><?php }  ?>
		 <?php }  ?> 
		 	
		 
		 		
		<a href="<?php echo base_url().$url."/"; ?>" class="btn btn-srch btn-success btn-sm">
		<i class="fa fa-backward" style="color:#FFFFFF"></i> Previous Page </a>
		</div>	
	<?php //} ?>	  	
<?php echo  form_close(); ?>
<br/>



