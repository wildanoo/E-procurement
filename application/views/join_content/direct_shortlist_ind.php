<script type="text/javascript"> 
    $(function(){
        var baseUrl   = "<?php echo base_url(); ?>";

        $('#btnsubmit').click(function(event){
            event.preventDefault();
            if(document.getElementById('i_accept').checked)
            {   var s = confirm("Apakah Anda yain ?");
                if (s == true) {
                    $("#form").submit();
                }
            }else{
                alert('Silakan cek syarat dan ketentuan');
                return false;
            }
        });
        
        $('#btnreject').click(function(event){
            event.preventDefault();
            var r = confirm("Apakah Anda yain ingin menolak undangan ini ?");
            if (r == true) {
                window.location.href = baseUrl ;
            }
            
        });
    }); 
</script>

<div class="about-content-wrap pull-left" id="#">
    <div class="head" style="margin-top: 25px;">
        <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;">Verifikasi</h1>
        <a href="#" class="next-section about animate2">
            <i class="fa fa-angle-double-down"></i>
        </a>
    </div>
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 content-tabs-wrap wow fadeInUp animated" data-wow-delay="1.7s" data-wow-offset="200">
            <div class="col-xs-12 col-sm-12 col-md-12 content-tabs no-pad">            
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#" data-toggle="tab"> Direct Selection </a></li>
                  <li><a>&nbsp;</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-home">
                        <div class = "row">
                            <div class="col-md-12 tab-content">
                                <div class="box2 box-info2 tab-pane fade in active" id="registrasi-step-1">
                                    <div class="box-body">
                                        <div class="about-intro-wrap pull-left">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 faq-tabs-wrap">
                                                    <div class="tabbable tabs-left">                                                        
                                                        <?php echo form_open_multipart('joinbiddingsourcing/joinShortlist',array('id'=>'form')); ?>
                                                        <input type="hidden" name="unikform" value="<?php echo $unikform ;?>" />
                                                        <input type="hidden" name="vendor_id" value="<?php echo $vendor_id ;?>" />
                                                        <input type="hidden" name="announce_id" value="<?php echo $this->uri->segment(3) ;?>" />
                                                        <div class="tab-content col-md-12 col-sm-12 col-xs-12">
                                                            <div class="tab-pane active" id="a">
                                                                <div class="dept-title-tabs"><?php echo strtoupper($announce->title_ind); ?></div>
                                                                <div class="">
                                                                    <div style="padding:5px;">
                                                                        <br>
                                                                        <div class="col-sm-12" style="padding-bottom:10px;">
                                                                            <span><strong><?php echo $announce->code; ?></strong></span> - <span style="color:red"><strong>Limited Invitation</strong></span>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="row">
                                                                            <div class="col-sm-7 row">
                                                                                <span class="control-label col-sm-4">Kategori</span>
                                                                                <span class="control-label col-sm-8" style="color:red">: <?php echo $categoryx; ?> </span>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <span class="control-label col-sm-5">Tanggal Publish</span>
                                                                                <span class="control-label col-sm-7" style="color:red">: <?php echo date('M d, Y',strtotime($announce->publish_date)); ?> </span>
                                                                            </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12">
                                                                            <div class="row">
                                                                            <div class="col-sm-7 row">
                                                                                <span class="control-label col-sm-4">Sub Kategori</span>
                                                                                <span class="control-label col-sm-8" style="color:red">: <?php echo $subcategoryx; ?> </span>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <span class="control-label col-sm-5">Tanggal Mulai</span>
                                                                                <span class="control-label col-sm-7" style="color:red">: <?php echo date('M d, Y',strtotime($announce->first_valdate)); ?> </span>
                                                                            </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12">
                                                                            <div class="row">
                                                                            <div class="col-sm-7 row">
                                                                                <span class="control-label col-sm-4">Status</span>
                                                                                <span class="control-label col-sm-8" style="color:red">: <?php echo array_key_exists($announce->status, $status_ref)?$status_ref[$announce->status]:'-'; ?></span>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <span class="control-label col-sm-5">Tanggal Berakhir</span>
                                                                                <span class="control-label col-sm-7" style="color:red">: <?php echo date('M d, Y',strtotime($announce->end_valdate)); ?> </span>
                                                                            </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12"><hr style="border-bottom: 1px solid #e0e1e2;"/></div>
                                                                        <div class="col-sm-12" align="justify">
                                                                            <span>
                                                                                <?php if ($content_ind) {
                                                                                    echo $content_ind?$content_ind:'';
                                                                                    echo "<br><br>";
                                                                                } ?>
                                                                                <?php if ($aanwijzing_ind) { ?>
                                                                                    <h3>Agenda Aanwijzing</h3>
                                                                                    <?php echo $aanwijzing_ind; ?>
                                                                                <?php } ?>
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-sm-12"><hr style="border-bottom: 1px solid #e0e1e2;"/></div>
                                                                        <div class="col-sm-12" align="justify">
                                                                            <?php if ($rfpfile) { ?>
                                                                              <section>
                                                                                <div class="col-md-2"><strong>RFP: </strong></div>
                                                                                <div class="col-md-10 row">
                                                                                  <ul class="list-inline">
                                                                                      <li><a href="<?php echo base_url().'webcontent/openFileRFP/'.$rfpfile ?>" target="_blank"><?php echo $rfpfile.'.pdf'; ?></a></li>
                                                                                  </ul>
                                                                                </div>
                                                                              </section>
                                                                            <?php } ?>

                                                                            <?php if (count($attachment)>0) { ?>
                                                                              <section>
                                                                                <div class="col-md-2"><strong>Attachment(s): </strong></div>
                                                                                <div class="col-md-10 row">
                                                                                  <ul class="list-inline">
                                                                                    <?php foreach ($attachment as $val) { ?>
                                                                                      <li><a href="<?php echo base_url().'webcontent/open_file/'.$val ?>" target="_blank"><?php echo $val.'.pdf'; ?></a></li>
                                                                                    <?php } ?>
                                                                                  </ul>
                                                                                </div>
                                                                              </section>
                                                                            <?php } ?>
                                                                        </div>

                                                                        <br>
                                                                        <div class="col-sm-12"><hr style="border-bottom: 1px solid #e0e1e2;"/></div>
                                                                        <div class="col-sm-12">
                                                                            <h3>Syarat dan Ketentuan</h3>
                                                                            Kebijakan Privasi ini mengatur ketentuan Garuda Indonesia menampung, menggunakan, memelihara dan menangkap informasi yang dikumpulkan dari pengguna (semua pengguna) dari https://www.eproc.garuda-indonesia.com/ . Kebijakan privasi ini berlaku untuk semua produk dan Jasa layanan yang ditawarkan oleh Garuda Indonesia
                                                                            <br><br>
                                                                            <label>
                                                                            <input type="checkbox" name="i_accept" id="i_accept" style="margin-right:10px;">Saya menerima syarat dan ketentuan yang berlaku
                                                                            </label>
                                                                            <br/><br>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12"><hr style="border-bottom: 1px solid #e0e1e2;"/></div>
                                        <div class="pull-left" style="margin-right:30px;">
                                            <button type="button" class="btn button-custom" id="btnsubmit" href="#">JOIN</button>
                                            <button type="button" class="btn button-custom-back" id="btnreject" href="#">TOLAK</button>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        </div>
    </div>
</div>