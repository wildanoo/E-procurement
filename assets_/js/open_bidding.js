$(document).ready(function(){

  var path       = window.location.pathname.split( '/' );
  var identifier = path[2]=='form_create'?'form_create':'form_update';

  var csrf_token = $('meta[name="csrf-token"]').attr('content');
  var csrf_hash  = $('meta[name="csrf-hash"]').attr('content');
  $.ajaxPrefilter(function(options, originalOptions, jqXHR){
    if (options.type.toLowerCase() === "post") {
        options.data = options.data || "";
        options.data += options.data?"&":"";
        options.data += csrf_token+"=" + csrf_hash;
    }
  });

  $(function () {
    if ($('meta[name="form-decider"]').attr('content') == 'form-create') {
      $("#wyswyg").hide();$("#upload4").hide();$(".news_detail_seg").hide();
      $("#id_company").change();
    }else{
      $("#upload1").hide();
    }
  });

	$(".detail-section").keyup(function(){
		var len 	 = $(this).val().length;
		var count_el = $(this).parent().find("#count-char");
		count_el.html(250-len);
	});

  $('#id_area').change(function(){
 		var id_area = $('#id_area').val();
    $.ajax({
      url  : 'get_office',
      type : "POST",              		               
      data : 'id_area='+id_area,
      success: function(resp){
       	if ($('#id_area').val() != '') {
       		$('#id_office').removeAttr('disabled');
       		$('#id_office').html(resp);
       	}else{
       		$('#id_office').attr('disabled','disabled');
       	}
      }
    });
  });

  $('#id_cat').change(function(){  
      var id_cat = $('#id_cat').val();
      $.ajax({
         url  : 'get_subcategory',
         type : "POST",                                
         data : 'id_cat='+id_cat,
         success: function(resp){
            $('#id_subcat').html(resp);
            if (id_cat!='')
            $('#id_subcat').removeAttr('disabled');
            else
            $('#id_subcat').attr('disabled','disabled');
         }
      });
  });

  $("#first_valdate").datetimepicker({
    format:'Y/m/d H:i',
    minDate: 0,
    // maxDate: $("#end_valdate").val(),
    step:1,
    onChangeDateTime: function (dp,selectedDate) {
      var time = $("#first_valdate").val();

            $("#end_valdate").datetimepicker({minDate: selectedDate.val()});
            $("#publish_date").datetimepicker({maxDate: selectedDate.val()});
        }
  });

  $("#end_valdate").datetimepicker({
    format:'Y/m/d H:i',
    minDate: 0,
    minTime: '08:00',
    step:1,
    onChangeDateTime: function (dp,selectedDate) {
      var time = $("#first_valdate").val();
      var init = selectedDate.val();

      if (new Date($("#end_valdate").val()).getTime() < new Date($("#first_valdate").val()).getTime()) {
        $("#publish_date").datetimepicker({maxDate: selectedDate.val()});
      }

      $("#first_valdate").datetimepicker({maxDate: selectedDate.val()});
    }
  });

  $('#publish_date').datetimepicker({
		format:'Y/m/d H:i',
		step:1,
		minDate: 0
	});

  $("input:radio[name=content]").click(function () { 
		var opt = $("input:radio[name=content]:checked").val();
		if (opt == '2'){
			$("#upload1").show();	 $("#wyswyg").hide(); 
		} else {
			$("#wyswyg").show(); $("#upload1").hide();	}
	});

  $("#attachment-uploader").uploadFile({
    url: '/'+path[1]+'/upload_attachment',
    multiple:false,
    dragDrop:true,
    // maxFileCount:1,
    fileName:"pdffile[]"
  });

  $("#attachment-upload").on("submit", function(){
    var data = $(this).serialize();

    $.ajax({    
      type : 'POST',
      url  : '/'+path[1]+'/updateAttachment',
      data : data,
      success :  function(data){
        $('#dialog-attachment').modal('toggle');
        $('.ajax-file-upload-statusbar').remove();
        $(".container-attachment").load('/'+path[1]+'/load_browse');
      }
    });
    $("#attachment-upload")[0].reset();
    return false;
  });

  $("#cancel-upload-attachment").on("click", function(){
    $("#attachment-upload")[0].reset();
    if ($(".ajax-file-upload-statusbar").length){
      $.ajax({    
        type : 'POST',
        url  : '/'+path[1]+'/deleteAttachment',
        data : 'file_attachment=max',
        success :  function(data){
          $('#dialog-attachment').modal('toggle');
          $('.ajax-file-upload-statusbar').remove();
          $('#attachment-description').reset();
          $('#attachment-remarks').reset();
          $(".container-attachment").load('load_browse');
        }
      });
      return false;
    }
  });

  $(".container-attachment").on("click",".delete-file",function(){
    var file = $(this).attr('data-file');
    
    $.ajax({
      type : 'POST',
      url  : '/'+path[1]+'/deleteAttachment',
      data : 'file_attachment='+file+'&identifier='+identifier,
      success :  function(resp){
        $(".container-attachment").load('/'+path[1]+'/load_browse/');
      }
    });
    return false;
  });

  /// add by Dedi Rudiyanto //////////////////////////////////////////////////////////
  $("#form").on("submit",function(event){
    // event.preventDefault();
    
    var title2      = $("input[name='title_eng']").val();
    var detail_eng  = $("textarea[name='detail_eng']").val();

    var opt = $("input:radio[name=content]:checked").val();
    if (opt == '1'){
      var content1  = CKEDITOR.instances.editor_ind.getData();
      var content2  = CKEDITOR.instances.editor_eng.getData();
    } else {
      var content1  = document.getElementById("file_upload_ind").files.length ;
      var content2  = document.getElementById("file_upload_eng").files.length ;
    }

    var aanwidzing1 = CKEDITOR.instances.idaanwidzing_ind.getData();
    var aanwidzing2 = CKEDITOR.instances.idaanwidzing_eng.getData();

    var strconfirm = "Content that you created is incomplete, should Admin Asyst added that content?";

    if(content1=='' || content1==0)
    { 
      if(content1=='' && opt=='1'){ $("#radio1").focus(); $("#cke_editor_ind").attr("style","border:1px solid red");}
      if(content1==0 && opt=='2') { $("#radio2").focus(); $("#file_upload_ind").attr("style","border:1px solid red");}
      return false;
    }else if(title2=='' || detail_eng=='' || content2=='' || content2==0)
    {
      var answer = confirm(strconfirm);
      if(answer)return true;
      return false;
    }
    else
    { 
      if(aanwidzing1!='' && aanwidzing2==''){
        var answer = confirm(strconfirm);
        if(answer)return true;
        return false;
      }
    }
    
  });
  $("#file_upload_ind").change(function(){ cektype($(this),'txt') });
  $("#file_upload_eng").change(function(){ cektype($(this),'txt') });
  $("#file_upload").change(function(){ cektype($(this),'image') });
  $("#file_upload_pdf").change(function(){ cektype($(this),'pdf') });

  ////////////////////////////////////////////////////////////////////////////////////

}); 
  
function lostfocus()
{
  $("#cke_editor_ind").attr("style","");
  $("#file_upload_ind").attr("style","");
}

function cektype(filename,typefile)
{
  filex = filename.val().split(".");
  tipe = filex[filex.length-1].toLowerCase() ;
  var stralert = 'use '+typefile+' format file' ;
  if(typefile=='image'){
    if(tipe!='jpg' && tipe!='gif' && tipe!='png') { alert(stralert);filename.val(''); }
  }
  else
  {
    if(tipe!=typefile) { alert(stralert);filename.val(''); }
  }
}