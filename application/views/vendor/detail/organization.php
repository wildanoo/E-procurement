<div id="container5">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	
   
   $('#btn_create5').click(function(){  
   		$("#container5").load(baseUrl+'vendor/form_organization');   		
   });   	
   
 }); 
 
 function update_organization(id){
 	
 	$("#container5").load(baseUrl+'vendor/form_update_organization/'+id); 
 }
 
 function delete_organization(id){
 	
 	var baseUrl  = "<?php echo base_url(); ?>";
 	var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';
    if (confirm('Are you sure you want to delete this?')) {
	 	$.post( baseUrl+ "vendor/delete_organization", { csrf: token, id: id })
		  .done(function( resp ) {
		    	$("#container5").load(baseUrl+'vendor/load_organization');
		});
	}	
 }
  
 </script>
<button type="button" id="btn_create5" class="btn btn-info btn-primary">Add New</button> 
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Organization Name</th>
		<th>Address</th>
		<th>Phone</th>
		<th>Position</th>
		<th>Remark</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>

 <?php 
 if($organization){ $i=1;
 	foreach($organization as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->name; ?></td>	
			<td><?php echo $row->address; ?></td>
			<td><?php echo $row->phone; ?></td>
			<td><?php echo $row->position; ?></td>
			<td><?php echo $row->remark; ?></td>
			<td style="text-align: center;">
			<?php
					echo '<i class="fa fa-pencil" onclick="update_organization('.$row->id.')"></i>'; echo '&nbsp;&nbsp;&nbsp;';					
					echo '<i class="fa fa-trash"  onclick="delete_organization('.$row->id.')"></i>';	
			?></td>											
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>
     </tbody>
</table>
</div>
