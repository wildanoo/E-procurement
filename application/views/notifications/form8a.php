<?php
///                                       from sendEmailToApproval - blacklist.php
$this->load->view('notifications/header');?>

<br/>
<p style="margin-bottom: 0;">Dengan hormat,
<br/>
Bapak/Ibu <?php echo $toname;?> / (<?php echo $unit;?>)
</p>
<br/><br/>

Bersama ini kami sampaikan informasi bahwa vendor telah melakukan pelanggaran dan dikenakan sanksi memberikan surat pernyataan dilarang mengikuti kegiatan pengadaan di lingkungan Garuda Indonesia.<br/><br/><br/><br/>

Berikut adalah informasi detail vendor:<br/>
<table bolder="0">
	<tr>
		<td widh="120px">Nama Perusahaan</td>
		<td>: <b><?php echo ucwords($vendor_name);?></b></td>
	</tr>
	<tr>
		<td widh="120px">ID Vendor</td>
		<td>: <b><?php echo ucwords($venID);?></b></td>
	</tr>
	<tr>
		<td widh="120px" valign="top">Kategori yang terdaftar</td>
		<td valign="top">
		: 
		<?php 
		$i=1; $spasi='';
		foreach($category as $cate)
		{
			echo $spasi . $i . '). ' . $cate->cate_name .'<br/>';
			$i++; $spasi='&nbsp; ';
		}
		?>
		</td>
	</tr>
	<tr>
		<td widh="120px">Alasan</td>
		<td>: <b><?php echo ucwords($reason);?></b></td>
	</tr>
</table><br/>

Untuk proses persetujuan dapat diakses :<br/>

<i><a href="<?php echo $link;?>">(link web sistem)</a></i><br/><br/>

Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih <br/>
<b><i>(<?php echo $from;?>)</i></b>
<br/><br/>
    
<?php $this->load->view('notifications/footer');?>