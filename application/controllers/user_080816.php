<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_user');
    }
    
 function index(){
 	if ($this->m_user->authenticate()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$srch_term = $this->input->get('srch-term'); $user_id = "";
		if($srch_term){
		$user_id = $this->crud->browse("","users","username",$srch_term,"false","id")->id; }	

		$usrID = !$user_id ? "" : $user_id;
		$field = !$usrID ? "" : "id";
		$ID    = !$usrID ? "" : $usrID;		
				
		if($usrID) $where =  array($field=>$ID);
		else $where = "";

 		$table 	    = "users l";		
		$page       = $this->uri->segment(3);
		$per_page   = 10;		
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table,$where);
		$set_config = array('base_url'=> base_url().'user/index','total_rows'=>$total_rows,'per_page'=>10,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);		
			 
		$this->load->library('pagination');			 
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();			
			
		$order 			     = array('field'=>'username','order'=>'ASC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;		
		$select          	 = "id,username,email,is_admin,activated,created,
							   (SELECT group_id FROM users_groups r WHERE l.id=r.user_id) as group_id,
							   (SELECT name FROM groups s WHERE s.id=group_id) as group_name";

		if($usrID){ $browse  = $this->crud->browse("",$table,$field,$ID,"true",$select,$where);	} else {
		$browse = $this->crud->browse_with_paging("",$table,$field,$ID,"true",$select,$where,$order,$config['per_page'],$offset); }

		$data['browse']	= $browse; 	
		$data['view']	= "users/browse"; 		
 		$this->load->view('layout/template',$data);
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	}
 	
 }

 function get_tags(){ 	
 	$term   = $_GET['term'];  	
 	$order  = array('field'=>'username','sort'=>'ASC'); $select = "username as name";
 	$result = $this->crud->autocomplete("","users",$select,"username",$term,"",$order);  	
 	echo json_encode($result); 	
 }
 
 function get_group_tags(){ 	
 	$term   = $_GET['term'];  	
 	$order  = array('field'=>'name','sort'=>'ASC'); //$select = "username as name";
 	$result = $this->crud->autocomplete("","groups","name","name",$term,"",$order);  	
 	echo json_encode($result); 	
 }  
 
 function get_permit_tags(){ 	
 	$term   = $_GET['term'];  	
 	$order  = array('field'=>'name','sort'=>'ASC'); //$select = "username as name";
 	$result = $this->crud->autocomplete("","permissions","name","name",$term,"",$order);  	
 	echo json_encode($result); 	
 }  
 
 function activated(){
 	
 	$userID   = $this->uri->segment(3); 	
 	$users    = $this->crud->browse("","users","id",$userID,"false","activated"); 	
 	$status   = $users->activated==1 ? 0 : 1; 	
 	$update   = array('activated'=>$status,'modified'=>date('Y-m-d H:i:s')); 	
 	
 	$this->crud->update("","users","id",$userID,$update);	 
 	$this->session->set_flashdata('message','successfully updates');
 	redirect('user/','refresh'); 	
 }
 
 function as_admin(){
 	
 	$userID   = $this->uri->segment(3); 	
 	$users    = $this->crud->browse("","users","id",$userID,"false","is_admin"); 	
 	$status   = $users->is_admin==1 ? 0 : 1; 	
 	$update   = array('is_admin'=>$status,'modified'=>date('Y-m-d H:i:s')); 	
 	
 	$this->crud->update("","users","id",$userID,$update);	 
 	$this->session->set_flashdata('message','successfully updates');
 	redirect('user/','refresh'); 	
 }

 function delete(){ 	
 	$id = $this->uri->segment(3);
 	$this->crud->delete("","users","id",$id);
 	$msg = "succesfully delete user";
 	$this->session->set_flashdata('message',$msg);
 	redirect('user/','refresh');	
 	
 }

 function set_user_sess(){ 	
 	$user_id = $this->uri->segment(3);
 	$select          	 = "id,username,email,is_admin,activated,created,
							(SELECT group_id FROM users_groups r WHERE l.id=r.user_id) as group_id,
							(SELECT name FROM groups s WHERE s.id=group_id) as group_name";
							
 	$data    = $this->crud->browse("","users l","id",$user_id,"false",$select);
 	$this->session->set_userdata('user_id',$user_id);
 	$this->session->set_userdata('group_id',$data->group_id);
 	redirect('user/form_grouping');
 }

 function form_grouping(){
 	if ($this->tank_auth->is_logged_in()) { 	
 		$data['user_id']  = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
				
		$data['groups']   = $this->crud->browse("","groups","","","true","id,name,permissions");		 	
		$data['view']     = "users/form_grouping2"; 		
 		$this->load->view('layout/template',$data);
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	} 	 	
 }
 
 function create_grouping(){
 	
 	$data = array('user_id'		 => $_POST['user_id'],					 	
				  'group_id'	 => $_POST['group_id'],					  
				  'created_id'	 => $this->tank_auth->get_user_id(),
				  'created_date' => date('Y-m-d H:i:s'),
				  'last_updated' => date('Y-m-d H:i:s'));
				  
	$where 	     = array('user_id'=>$_POST['user_id']);
 	$is_exist    = $this->crud->is_exist("","users_groups","user_id",$where);	
 	
 	if(!$is_exist){ 
 		$this->crud->insert("","users_groups",$data); 
	} else {
		$where = array('user_id'=>$_POST['user_id']); 
		unset($data['user_id']); unset($data['created_id']);  unset($data['created_date']);
		$this->crud->multiple_update("","users_groups",$where,$data);
		$this->crud->update("","users_groups","user_id",$_POST['user_id'],$data);
	}
			
	$msg1 = "succesfully users groups";
	$msg2 = "duplicate users groups";
	$msg  = !$is_exist ? $msg1 : $msg2;
	
	$this->session->set_flashdata('message',$msg);
 	redirect('user/','refresh');		  
 	
 }

 function groups(){
 	if ($this->m_user->authenticate()) { 		
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$srch_term = $this->input->get('srch-term'); $group_id = "";
		if($srch_term){
		$group_id = $this->crud->browse("","groups","name",$srch_term,"false","id")->id; }	

		$grpID = !$group_id ? "" : $group_id;
		$field = !$grpID ? "" : "id";
		$ID    = !$grpID ? "" : $grpID;		
				
		if($grpID) $where =  array($field=>$ID);
		else $where = "";	
		
		$table 	    = "groups";		
		$page       = $this->uri->segment(3);
		$per_page   = 10;		
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table,$where);
		$set_config = array('base_url'=> base_url().'user/groups','total_rows'=>$total_rows,'per_page'=>10,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);		
			 
		$this->load->library('pagination');			 
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();			
			
		$order 			     = array('field'=>'name','order'=>'ASC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;		
		$select          	 = "";

		if($grpID){ $browse  = $this->crud->browse("",$table,$field,$ID,"true",$select,$where);	} else {
		$browse = $this->crud->browse_with_paging("",$table,$field,$ID,"true",$select,$where,$order,$config['per_page'],$offset); }

		$data['browse']	= $browse; 	
		$data['view']	= "users/groups/browse"; 		
 		$this->load->view('layout/template',$data);
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	} 	
 }

 function content_approval_level(){
 	if ($this->m_user->authenticate()) { 		
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$srch_term = $this->input->get('srch-term'); $group_id = "";
		if($srch_term){
		$group_id = $this->crud->browse("","groups","name",$srch_term,"false","id")->id; }	

		$grpID = !$group_id ? "" : $group_id;
		$field = !$grpID ? "" : "id";
		$ID    = !$grpID ? "" : $grpID;		
				
		if($grpID) $where =  array($field=>$ID);
		else $where = "";	
		
		$table 	    = "groups";		
		$page       = $this->uri->segment(3);
		$per_page   = 10;		
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table,$where);
		$set_config = array('base_url'=> base_url().'user/groups','total_rows'=>$total_rows,'per_page'=>10,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);		
			 
		$this->load->library('pagination');			 
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();			
			
		$order 			     = array('field'=>'name','order'=>'ASC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;		
		$select          	 = "";

		if($grpID){ $browse  = $this->crud->browse("",$table,$field,$ID,"true",$select,$where);	} else {
		$browse = $this->crud->browse_with_paging("",$table,$field,$ID,"true",$select,$where,$order,$config['per_page'],$offset); }

		$data['browse']	= $browse; 	
		$data['view']	= "users/groups/browse"; 		
 		$this->load->view('layout/template',$data);
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	} 	
 }

 function form_group(){
 	if ($this->tank_auth->is_logged_in()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$data['perm']  = $this->crud->browse("","permissions","","","true","id,name,value");		 	
		$data['view']  = "users/groups/form_group"; 		
 		$this->load->view('layout/template',$data);
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	} 	
 }

 function create_group(){
 	$ttl   		 = $_POST['ttl'];
 	$name  		 = $_POST['name'];
 	$permissions = "";
 	$where 	     = array('name'=>$name);
 	$is_exist    = $this->crud->is_exist("","groups","id",$where);
 	
 	for ($i = 1; $i <= $ttl; $i++) { 		
 		$value = $this->input->post('check'.$i);
 		if($value) $perm[] = $value; }
 	
 	if($perm){ $permissions = implode(",",$perm); } 	
 	$data = array('id'			 => null, 						
				  'name'		 => $_POST['name'],					 	
				  'permissions'	 => $permissions,
				  'created_id'	 => $this->tank_auth->get_user_id(),
				  'created_date' => date('Y-m-d H:i:s'),
				  'last_updated' => date('Y-m-d H:i:s'));
	
	if(!$is_exist){ 
		$this->crud->insert("","groups",$data); }
			
	$msg1 = "succesfully add new group";
	$msg2 = "duplicate group name";
	$msg  = !$is_exist ? $msg1 : $msg2;
	
	$this->session->set_flashdata('message',$msg);
 	redirect('user/groups','refresh');		  
				  
 }

 function form_edit_perm(){
 	
 	if ($this->tank_auth->is_logged_in()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$id = $this->uri->segment(3);
		$order  = array('field'=>'name','sort'=>'ASC');
		$data['def']   = $this->crud->browse("","groups","id",$id,"false","id,name,permissions");	 	
		
		$data['perm']  = $this->crud->browse("","permissions","","","true","id,name,value","",$order);		 	
		$data['view']  = "users/groups/form_edit_group"; 		
 		$this->load->view('layout/template',$data);
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	}
	
 }
 
 function update_group(){
 	$ttl   		 = $_POST['ttl'];
 	$id  		 = $_POST['id']; 	
 	for ($i = 1; $i <= $ttl; $i++) { 		
 		$value = $this->input->post('check'.$i);
 		if($value) $perm[] = $value; }
 	
 	if($perm) $permit = implode(",",$perm); 	
 	$update = array('permissions'  => !$permit ? "" : $permit,				  
				    'last_updated' => date('Y-m-d H:i:s'));
	
	if($perm) $this->crud->update("","groups","id",$id,$update);			
	$msg = "succesfully update group";
	$this->session->set_flashdata('message',$msg);
 	redirect('user/groups','refresh');			  
 }

 function delete_group(){ 	
 	$id = $this->uri->segment(3);
 	$this->crud->delete("","groups","id",$id);
 	$msg = "succesfully delete group";
 	$this->session->set_flashdata('message',$msg);
 	redirect('user/groups','refresh');	 	
 }

 function permissions(){
 	if ($this->m_user->authenticate()) { 		
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();		
		
		$srch_term = $this->input->get('srch-term'); $permit_id = "";
		if($srch_term){
		$permit_id = $this->crud->browse("","permissions","name",$srch_term,"false","id")->id; }	

		$prmID = !$permit_id ? "" : $permit_id;
		$field = !$prmID ? "" : "id";
		$ID    = !$prmID ? "" : $prmID;		
				
		if($prmID) $where =  array($field=>$ID);
		else $where = "";	
		
		$table 	    = "permissions";		
		$page       = $this->uri->segment(3);
		$per_page   = 10;		
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table,$where);
		$set_config = array('base_url'=> base_url().'user/permissions','total_rows'=>$total_rows,'per_page'=>10,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);		
			 
		$this->load->library('pagination');			 
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();			
			
		$order 			     = array('field'=>'name','order'=>'ASC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;		
		$select          	 = "";

		if($prmID){ $browse  = $this->crud->browse("",$table,$field,$ID,"true",$select,$where);	} else {
		$browse = $this->crud->browse_with_paging("",$table,$field,$ID,"true",$select,$where,$order,$config['per_page'],$offset); }

		$data['browse']	= $browse; 	
		$data['view']	= "users/permissions/browse"; 		
 		$this->load->view('layout/template',$data);
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	} 	
 }

 function form_permit(){
 	if ($this->tank_auth->is_logged_in()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$data['perm']  = $this->crud->browse("","permissions","","","true","id,name,value");		 	
		$data['view']  = "users/permissions/form_permit"; 		
 		$this->load->view('layout/template',$data);
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	} 	
 }

 function create_permit(){
 	
 	$data = array('id'			 => null, 						
				  'name'		 => $_POST['name'],					 	
				  'value'		 => str_replace(" ","-",strtolower($_POST['value'])),	
				  'description'	 => $_POST['desc'],	
				  'created_id'	 => $this->tank_auth->get_user_id(),
				  'created_date' => date('Y-m-d H:i:s'),
				  'last_updated' => date('Y-m-d H:i:s'));
				  
	$where 	     = array('name'=>$_POST['name']);
 	$is_exist    = $this->crud->is_exist("","permissions","id",$where);	
 	
 	if(!$is_exist){ 
		$this->crud->insert("","permissions",$data); }
			
	$msg1 = "succesfully add new permissions";
	$msg2 = "duplicate permissions name";
	$msg  = !$is_exist ? $msg1 : $msg2;
	
	$this->session->set_flashdata('message',$msg);
 	redirect('user/permissions','refresh');		  

 }

 function form_edit_permit(){
 	
 	if ($this->tank_auth->is_logged_in()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$id = $this->uri->segment(3);
		$order 		   = array('field'=>'name','sort'=>'ASC');
		$data['def']   = $this->crud->browse("","permissions","id",$id,"false","","",$order);	 	
		
		$data['view']  = "users/permissions/form_edit"; 		
 		$this->load->view('layout/template',$data);
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	}	
 }
 
 function update_permit(){
 	
 	$id     = $_POST['id']; 	 	
 	$update = array('name'  	   => $_POST['name'],	
 					'value'  	   => str_replace(" ","-",strtolower($_POST['value'])),	
 					'description'  => $_POST['desc'],				  
				    'last_updated' => date('Y-m-d H:i:s'));
	
	$this->crud->update("","permissions","id",$id,$update);			
	$msg = "succesfully update permission";
	$this->session->set_flashdata('message',$msg);
 	redirect('user/permissions','refresh');		
 	
 }
 
 function delete_permit(){ 	
 	$id = $this->uri->segment(3);
 	$this->crud->delete("","permissions","id",$id);
 	$msg = "succesfully delete permission";
 	$this->session->set_flashdata('message',$msg);
 	redirect('user/permissions','refresh');	 	
 } 

}

