<p>Garuda Indonesia E-Procurement adalah sistem aplikasi berbasis web yang menyediakan berbagai fasilitas serta informasi terkait dengan proses pengadaan barang / jasa di PT Garuda Indonesia (Persero) Tbk. Sistem ini dikembangkan dalam rangka meningkatkan efisiensi dan efektivitas Pengadaan.</p>

<p>Melalui Web E-Procurement, Pemasok bisa mengakses beberapa informasi termasuk Berita Terbaru tentang Pembaruan Pengadaan di Garuda, Pengadaan Informasi dan Kebijakan Pengadaan diterapkan di Garuda. Pemasok juga dapat mendaftar dan mendaftar secara online untuk menjadi penjual terdaftar kami.</p>
