<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_menutop');

	}

	function index(){
		redirect('/auth/login/');
	}

	function browse(){
		if ($this->m_menutop->authenticate()) {
			$id 			= $this->uri->segment(3);
			if($id){
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				
				$browse 		= $this->crud->browse("","groups","id",$id,"false","*");

				$data1 		= $this->db->query("SELECT t1.id as menu1ID,t1.menu1,t1.status as stat1, t1.url, t2.id as featureID,t2.id_menu,t2.level,t2.groups FROM menu1 as t1 LEFT JOIN menu_feature as t2 ON t1.id=t2.id_menu AND t2.level = 1 WHERE t1.status = '1'")->result();
				$data2 		= $this->db->query("SELECT t1.id as menu2ID,t1.id_up, t1.menu2,t1.status as stat1, t1.url, t2.id as featureID,t2.id_menu,t2.level,t2.groups FROM menu2 as t1 LEFT JOIN menu_feature as t2 ON t1.id=t2.id_menu AND t2.level = 2 WHERE t1.status = '1'")->result();
				$data3 		= $this->db->query("SELECT t1.id as menu3ID,t1.id_up, t1.menu3,t1.status as stat1, t1.url, t2.id as featureID,t2.id_menu,t2.level,t2.groups FROM menu3 as t1 LEFT JOIN menu_feature as t2 ON t1.id=t2.id_menu AND t2.level = 3 WHERE t1.status = '1'")->result();

				$data['browse']		= $browse;
				$data['row1']		= $data1;
				$data['row2']		= $data2;
				$data['row3']		= $data3;

				$data['view']	= "menu/browse";
				$this->load->view('layout/template',$data);
			}else{
				redirect('/auth/login/');
			}
		}else{
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/');
		}
	}

	function updatemenu(){
		$id 	= $this->input->post('id');
		$group1  = $this->input->post('ck_list1');
		$group2  = $this->input->post('ck_list2');
		$group3  = $this->input->post('ck_list3');

		$menu1  = $this->db->query("SELECT * FROM menu_feature WHERE level = '1'")->result();
		$menu2  = $this->db->query("SELECT * FROM menu_feature WHERE level = '2'")->result();
		$menu3  = $this->db->query("SELECT * FROM menu_feature WHERE level = '3'")->result();

		foreach($menu1 as $m1){
			if(in_array($m1->id,$group1)){
				$this->update_usergroup($m1->id,$id,$m1->groups,"add");
			}else{
				$this->update_usergroup($m1->id,$id,$m1->groups,"delete");
			}
		}
		foreach($menu2 as $m2){
			if(in_array($m2->id,$group2)){
				$this->update_usergroup($m2->id,$id,$m2->groups,"add");
			}else{
				$this->update_usergroup($m2->id,$id,$m2->groups,"delete");
			}
		}
		foreach($menu3 as $m3){
			if(in_array($m3->id,$group3)){
				$this->update_usergroup($m3->id,$id,$m3->groups,"add");
			}else{
				$this->update_usergroup($m3->id,$id,$m3->groups,"delete");
			}
		}

		redirect('user/groups');
	}

	function update_usergroup($idfeature,$idgroup,$existgroup,$action){
		if($action == "add"){
			$addgroup = explode(',',$existgroup);
			if(!in_array($idgroup,$addgroup)){
				$addgroup[]=$idgroup;
				$addgroup = implode(',',$addgroup);
				$addgroup = array('groups'=>$addgroup);
				$this->crud->update("","menu_feature","id",$idfeature,$addgroup);
			}else{}

		}elseif($action == "delete"){
			$existgroup = explode(',',$existgroup);
			if(in_array($idgroup,$existgroup)){
				$idgroup = array($idgroup);
				$delgroup = array_diff($existgroup,$idgroup);
				$delgroup = implode(',',$delgroup);
				$delgroup = array('groups'=>$delgroup);
				$this->crud->update("","menu_feature","id",$idfeature,$delgroup);
			}
		}
	}

}

/* End of file menu.php */
/* Location: ./application/controllers/menu.php */