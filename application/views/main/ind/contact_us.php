<!DOCTYPE HTML>
<html lang="en-US">
<?php $this->load->view('layout/header_default'); ?>
</head>
<body>
	<div id="wrapper">

		<!-- sidebar english -->
		<div id="wrapper1" class="st-container">
			<nav class="st-menu st-effect-1" style="height:100%;background-color:#005291;position:fixed;">
				<?php $this->load->view('auth/login_form'); ?>
			</nav>
		</div>
		<div id="wrapper2" class="st-container">
			<nav class="st-menu st-effect-2" style="height:100%;background-color:#ededea;position:fixed;overflow-y: scroll;overflow-x: hidden;">	  
				<?php $this->load->view('vendor/ind/register'); ?>
			</nav>
		</div>
		<!-- sidebar english -->

		<header id="header" class="container-fluid">
			<div class="header-bg">
				<div class="container">	  
					<div class="row">
						<div id="logo" class="col-md-5 col-md-offset-1"><a href="login" class="logo" alt="E-Procurement Garuda Indonesia"></a></div>
						<div class="col-md-5">
							<ul class="social-icons">
							<?php if ($this->session->userdata('lang') == 'eng') { ?>
								<li class="lang eng" data="lang-eng" title="English"><a style="top:0 !important;" href="#">English</a></li>
								<li class="lang idn" data="lang-ind" title="Indonesian"><a class="inactive" href="#" id="lang_ind">Indonesian</a></li>
							<?php }elseif ($this->session->userdata('lang') == 'ind') { ?>
								<li class="lang eng" data="lang-eng" title="English"><a class="inactive" href="#" id="lang_eng">English</a></li>
								<li class="lang idn" data="lang-ind" title="Indonesian"><a style="top:0 !important;" href="#">Indonesian</a></li>
							<?php }?>
							</ul>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<ul id="menu" class="visible-lg" style="font-size: 13px;">
								<li><?php echo anchor('auth/login', 'BERANDA')?></li>
								<!-- li><?php  echo anchor('main/news','News'); ?></li> -->
								<li><?php echo anchor('main/about_us','TENTANG KAMI'); ?></li>
								<li><?php echo anchor('main/faq','FAQ'); ?></li>
								<li id="st-trigger-effects2"><a data-effect="st-effect-2">REGISTRASI</a></li>
								<li id="st-trigger-effects1"><a data-effect="st-effect-1">LOGIN</a></li>
								<li><?php  echo anchor('main/contact_us','HUBUNGI KAMI'); ?></li>
								<!-- Search Form -->
								<li class="search-form visible-desktop" style="float: right;top: 10px;"\>
									<form method="get" action="#">
										<input type="text" value="Search" class="search-text-box"/>
										<input type="submit" value="" class="search-text-submit"/>
									</form>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</header>

		<!-- =================== Contact us container ============== -->
		<section class="contact_us_container">
			<div class="container" style="width: 940px;
			    margin: 0 auto;
			    padding: 0;
			">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;"> <!-- section title -->
						<h2 style="color:#368ccc;">Hubungi Kami</h2>
						<p>Anda dapat menghubungi representatif kami kapanpun. Silahkan isi formulir dibawah ini.</p>
					</div> <!-- End section title -->
						<div class="col-md-12 map float-left"> <!-- form holder -->
							<div class="overlay" onClick="style.pointerEvents='none'"></div>
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12021.800034061933!2d106.63748515696331!3d-6.133446872252365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x840e34d171919f67!2sGaruda+Indonesia+City+Center!5e0!3m2!1sid!2sid!4v1459331703516" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form_holder"> <!-- form holder -->
						
							<!-- form action="includes/sendemail.php" class="contact-form"> -->
							
							<h3 style="color:#368ccc;">Tinggalkan Pesan</h3>
							<form action="<?php echo base_url().'main/message' ; ?>" method="post" class="contact-form">
								<input class="form-control name required" type="text" id="name" name="name" placeholder="Name" required/>
								<input class="form-control email required" type="text" id="email" name="email" placeholder="Email" required/>
								<input class="form-control required" type="text" id="email" name="subject" placeholder="Subject"required/>
								<textarea class="required" name="message" id="message" placeholder="Message" required></textarea>
								<button type="submit" class="submit">Submit<i class="fa fa-arrow-circle-right" style="color:#fff;"></i></button>
							</form> <!-- End form holder -->
							<?php echo form_close(); ?>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pull-right address">
							<address>
								<div class="col-md-2 icon_holder float_left"><span class="fa fa-location-arrow"></span></div>
								<div class="col-md-10 address_holder float_left">Garuda Indonesia City Center <br>						
								<?php  $root = $_SERVER['DOCUMENT_ROOT'];	$path = $root."/uploads/cms/contact_ind.txt";
								if( file_exists($path)) {	echo file_get_contents($path);			
								} else { echo ""; }	?></div>
							</address>
							<address class="clear_fix" style="border-bottom:none;">
								<div class="col-md-2 icon_holder float_left"><span class="fa fa-envelope"></span></div>
								<div class="col-md-10 address_holder float_left">business-support@garuda-indonesia.com</div>
							</address>
								<!-- <address class="clear_fix">
									<div class="icon_holder float_left"><span class="icon icon-Phone2"></span></div>
									<div class="address_holder float_left">+ (1800) 456 7890 <br> + (1544) 456 7890</div>
								</address> -->
							</div>
						</div>
					</div>
				</section>

				<!-- =================== /Contact us container ============== -->

			</div>
			<footer class="main-footer footer-line container-fluid text-center">
				Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
			</footer>
			<?php $this->load->view('layout/footer_default'); ?>
		</body>
		</html>