<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   
   $('#eff_date').datepicker({  
        changeMonth: true, changeYear: true,
		dateFormat: "yy/mm/dd" ,
     });      
     
   $('#exp_date').datepicker({  
        changeMonth: true, changeYear: true,
		dateFormat: "yy/mm/dd" ,
    });      
     
   $('#btn_cancel8').click(function(){ 
   		 $("#container8").load(baseUrl+'vendor/load_correspondence');
     });     
	   
   });

</script>
<div id="debug8"></div>
<?php echo form_open_multipart('vendor/update_document',array('id'=>'form_doc')); ?>
<input type="hidden" name="docID" id="docID" value="<?php echo $default->id; ?>"/>
<table width="80%">
<tr>
	<td><label>Document Name</label></td><td>:</td>
	<td><input type="text" name="doc_name" id="doc_name" value="<?php echo $default->doc_name; ?>" placeholder="Document Name" class="form-control" required/></td>
	<td id="msg1">&nbsp;</td>	
</tr>
<tr>
	<td><label>Document Number</label></td><td>:</td>
	<td><input type="text" name="doc_num" id="doc_num" value="<?php echo $default->doc_num; ?>" placeholder="Document Number" class="form-control" required/></td>
	<td id="msg2">&nbsp;</td>	
</tr>
<tr>
	<td><label>Description</label></td><td>:</td>
	<td><input type="text" name="desc" id="desc" value="<?php echo $default->desc; ?>"placeholder="Description" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Effective Date</label></td><td>:</td>
	<td><input type="text" name="eff_date" id="eff_date" value="<?php echo $default->eff_date; ?>" class="form-control" /></td>
</tr>
<tr>
	<td><label>Expired Date</label></td><td>:</td>
	<td><input type="text" name="exp_date" id="exp_date" value="<?php echo $default->exp_date; ?>" class="form-control" /></td>
	<td><?php 
		$checked = $default->remainder==0 ? false : true ;
		echo form_checkbox('remainder', '1', $checked,array('id'=>'remainder')); ?>&nbsp;&nbsp;Notif when expired</td>
</tr>
<tr>
	<td><label>Remark</label></td><td>:</td>
	<td><input type="text" name="remark" id="remark" value="<?php echo $default->remark; ?>" placeholder="Remark" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Attachment</label></td><td>:</td>
	<td><?php	
			$style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";
			$data  = array('type'=>'file','name'=> 'userfile','id' => 'file_upload','value'=>'true','content'=>'Import','style'=>$style); 
			echo form_input($data); 
			
			if($default->filetype!="-"){
				$file =  $default->id.".".$default->filetype;
				echo $file;
			} else { echo "empty file"; $file = ""; }
			
			echo form_hidden("attcfile",$file);			
	?>
		<br><span class="label label-danger"><i>max size 2MB</i></span>	
	</td>
</tr>
</table>

<div style="text-align: right;">
	<input type="button" id="btn_cancel8" value="Cancel" class="btn btn-default btn-sm"/>
	<input type="submit"  value="Update" class="btn btn-primary btn-sm"/>	
</div>
<br/>
<?php echo  form_close(); ?>


