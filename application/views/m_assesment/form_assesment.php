 
<script type="text/javascript">
$(document).ready(function() {
  var baseUrl = "<?php echo base_url(); ?>";
  $(".js-example-basic-single").select2({placeholder: "Select Subcategory",theme: "classic"});
  $(".js-example-basic-multiple").select2({placeholder: "Select Assesment"});

  $('#form').submit(function(event) {
  	var sdata = $(this).serialize();
  	$.post(baseUrl+'m_assesment/update_assesment', sdata, function(data, textStatus, xhr) {
  		var obj = JSON.parse(data);
      if(obj.state == 'true')
      {
  		  load_data();
      }
      else
      {
        var err_msg = $('<span class="label label-warning"></span>').text(obj.msg);
        $('#fc_msg').html(err_msg);
      }

  	});
  
    return false;

  });


});
</script>

<div class="panel panel-default">
<div class="panel-heading">Manage Asessment : Subcategory</div>
<div class="panel-body" >

<div id="fc_msg" class="text-center"></div>

<?php echo form_open('#',array('id'=>'form')); ?>
  <input type="hidden" name="id" value="<?= isset($def->id) ? $def->id : '';  ?>">
  
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Subcategory</label>
    <div class="col-sm-10">
      <select required style="width:700px;margin:10px;" name="id_subcat" class="js-example-basic-single" >
        <?php foreach($category as $group=>$row){  ?>
        <optgroup label="<?php echo $group; ?>">
          <?php foreach ($row as $val){  ?>                                
          <option value="<?php echo $val->id; ?>"><?php echo $val->subcategory; ?></option>                              
          <?php } ?>                                
        </optgroup>
        <?php } ?>
      </select>
    </div>
  </div>

  <br>

  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Assesment</label>
    <div class="col-sm-10">
      <select required style="width:700px;margin:3px;" name="assesment[]" class="js-example-basic-multiple" multiple="multiple">
        <?php 
          foreach ($assesment as $value) 
          {
            echo '<option value=" '.$value->id.' ">'.$value->description.'</optioin>';
          }

         ?>
      </select>
    </div>
  </div>
<br>
<br>
<br>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">Save</button>
      <button type="button" onclick="load_data()" class="btn btn-primary">Cancel</button>
    </div>
  </div>
<?php echo form_close(); ?>


</div>
</div>
