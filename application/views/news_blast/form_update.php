<meta name="csrf-token" content="<?php echo $this->security->get_csrf_token_name(); ?>">
<meta name="csrf-hash" content="<?php echo $this->security->get_csrf_hash(); ?>">
<meta name="form-decider" content="form-update">
<script src="<?php echo base_url() ?>assets_/js/news_blast.js"></script>

<style type="text/css">
	.valign-top td{
		vertical-align: top !important;
	}
</style>

<div class="page-header" style="vertical-align: middle">
  <h3 style="display:inline-block">Update Content - News Blast</h3>
</div> 
<?php
if($this->session->flashdata('error_file_upload')) {
echo '<div class="alert alert-warning">';
  echo '<strong><i class="fa fa-exclamation-triangle fa-1x" aria-hidden="true"></i> </strong>'.$this->session->flashdata('error_file_upload');
echo '</div>';
echo '<br>';
}
?>
<div id="debug"></div>
<?php echo form_open_multipart('news_blast/update',array('id'=>'form'));$current_date = date('Y/m/d')?>

<table width="100%" border="0">
<tr class="valign-top">
	<td><label>Title</label></td><td>:</td>
	<td class="form_tab_content" width="75%;">
		<div style="margin-top: -5px;">
			<ul class="nav nav-tabs test2">
			    <li class="active"><a data-toggle="tab" href="#t_eng" aria-expanded="true">News Title</a></li>
			</ul>
			<div class="tab-content">
			    <div id="t_eng" class="tab-pane fade active in" style="padding:15px;">
			      <input class="form-control" value="<?php echo $def->title_eng; ?>" type="text" name="title_eng" id="title" placeholder="Title" class="form-control" required/>
			    </div>
			</div>
		</div>
	</td>
</tr>
<tr class="vendor-select valign-top">
	<td><label id="label-participants">List of Recipient(s)</label></td><td>:</td>
	<td>
		<select multiple="multiple" required class="multi-select" id="vendor_multiselect" name="vendor_multiselect[]">
		<?php if ($vendor_lists) {
			echo $vendor_lists;
		} ?>
		</select>
		<br>
	</td>
</tr>
<?php echo form_hidden('id',$def->id); ?>
<tr id="company_area">
	<td><label>Company</label></td><td>:</td>
	<td>
		<?php
		$prop_company = 'class="form-control" id="id_company" required';			
		echo form_dropdown('id_company',$company,$def->id_company,$prop_company); ?>
	</td>
</tr>
<tr id="area">
	<td><label>Area</label></td><td>:</td>
	<td><?php
		$prop_area = 'class="form-control" id="id_area" required';
		echo form_dropdown('id_area',$areas,$def->id_region,$prop_area); ?>
	</td>
</tr>
<tr id="office_area">
	<td><label>Office</label></td><td>:</td>
	<td><?php
		$prop_office = 'class="form-control" id="id_office" required';
		echo form_dropdown('id_office',$offices,$def->id_office,$prop_office); ?>
	</td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr class="valign-top">
	<td><label>Overview</label></td><td>:</td>
	<td class="form_tab_content" width="75%;">
		<div style="margin-top: -5px;">
			<ul class="nav nav-tabs">
			    <li class="active"><a data-toggle="tab" href="#d_eng" aria-expanded="true">News Overview</a></li>
			</ul>
			<div class="tab-content">
			    <div id="d_eng" class="tab-pane fade active in" style="padding:15px;">
			      (<span id="count-char"><strong><?php echo (250-strlen($def->detail_eng)); ?></strong></span> chars left)
			      <textarea name="detail_eng" id="detail_eng" maxlength="250" cols="20" rows="5" class="form-control detail-section" required><?php echo $def->detail_eng; ?></textarea>
			    </div>
			</div>
		</div>
	</td>
</tr>
<?php echo form_hidden("creator",$username); ?>
<tr id="option">
 <td><label>Description</label></td><td>:</td>
  <td>
	  <input type="radio" name="content" value="1" checked> Typing &nbsp;&nbsp;
	  <input type="radio" name="content" value="2"> Upload<br>
  </td>
</tr>
<tr id="wyswyg">
	<td colspan="2">&nbsp;</td>
	<td class="form_tab_content" width="75%;">
		<div>
			<ul class="nav nav-tabs test2">
			    <li class="active"><a data-toggle="tab" href="#eng" aria-expanded="true">News Description</a></li>
			</ul>
			<div class="tab-content">
			    <div id="eng" class="tab-pane fade active in" style="padding:15px;">
			      <?php $data['content'] = $contents[1]; $this->load->view('announce/note',$data); ?>
			    </div>
			</div>
		</div>
	</td>
</tr>
<tr id="upload1">
	<td colspan="2">&nbsp;</td>
	<td class="form_tab_content" width="75%;">
		<div>
			<ul class="nav nav-tabs test2">
			    <li class="active"><a data-toggle="tab" href="#upeng" aria-expanded="true">News Description</a></li>
			</ul>
			<?php $style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";?>
			<div class="tab-content">
			    <div id="upeng" class="tab-pane fade active in" style="padding:15px;">
			      <?php	
					$data  = array('type'=>'file','name'=> 'contentfileeng','id' => 'file_upload_eng','value'=>'true','content'=>'Import','style'=>$style); 
					echo form_input($data); ?> <i>*.txt</i>
			    </div>
			</div>
		</div>
	</td>
</tr>
<tr id="upload2">
	<td><label>Image</label></td><td>:</td>
	<td><?php	
		$data2  = array('type'=>'file','name'=> 'imagefile','id' => 'file_upload','value'=>'true','content'=>'Import','style'=>$style); 
		echo form_input($data2); ?><?php echo $imgfile; ?>&nbsp;<i>*.jpg,gif,png</i>
	</td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr class="valign-top">
	<td><label>Publish Date</label></td><td>:</td>
	<td><input type="text" value="<?php echo str_replace("-","/",$def->publish_date); ?>"  onkeydown="return false" class="form-control" id="publish_date" name="publish_date"></td>
</tr>
<tr id="upload3">
	<td><label>Attachment</label></td><td>:</td>
	<td><?php	
		$data3  = array('type'=>'file','name'=> 'pdffile','id' => 'file_upload','value'=>'true','content'=>'Import','style'=>$style); 
		echo form_input($data3); ?><?php echo $attfile; ?>&nbsp;<i>*.pdf</i>
	</td>
</tr>
<tr>
	<td colspan="3" style="text-align: right;">
		<button style="vertical-align: baseline;" type="submit" value="submit" name="button-process" class="btn btn-primary btn-sm"/>Submit</button>
		<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>"Cancel", 'style'=>"vertical-align: baseline;"); ?>
		<?php  echo anchor('announce/','Cancel',$prop); ?>
	</td>
</tr>
</table>
<?php echo form_close(); ?>