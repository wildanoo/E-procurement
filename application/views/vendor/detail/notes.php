
<script type="text/javascript">
 $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	        	
   
 }); 
  
 </script>
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th style="text-align: center">No</th>
		<th>Notes</th>
		<th style="text-align: center">Status</th>	
		<th style="text-align: center">Action By</th>			
		<th style="text-align: center">Action Time</th>
	</tr>
	</thead>
	<tbody>
 <?php  
 if($notes){ $i=1;
 	foreach($notes as $row){ ?>
 		<tr>
 			<td style="text-align: center"><?php echo $i; ?></td>
 			<td><?php echo $row->notes; ?></td>
 			<td style="text-align: center"><?php echo ucwords(strtolower($row->reg_sts)); ?></td>
			<td style="text-align: center"><?php 
				if($row->created_id=="0") echo "Vendor";
				else echo $this->m_user->get_user_group($row->created_id)->group_name;	
			?></td>				
			<td style="text-align: center;"><?php echo date('M d, Y H:i:s',strtotime($row->created_date)); ?></td>												
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="3">-</td></tr>
<?php } ?>
     </tbody>
</table>

