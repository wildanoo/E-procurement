<div class="page-header">
  <h3>Dashboard</h3>
</div>

<div id="page-wrapper" style="min-height: 319px;">
	<div class="row">
		<div class="col-lg-4">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                <i class="fa fa-bar-chart-o fa-fw"></i> Active Announcement
	            </div>
	            <div class="panel-body">
	            	<div class="panel-icon pull-left color-spring-green">
					<i class="fa fa-truck"></i>
					</div>
					<span class="panel-number"><strong>12</strong></span>
                </div>
                <div class="panel-footer text-right">
                    <a href="#" class="btn btn-primary" style="margin-bottom:0;">More <i class="glyphicon glyphicon-chevron-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                <i class="fa fa-bar-chart-o fa-fw"></i> New Vendor Request
	            </div>
	            <div class="panel-body">
	            	<div class="panel-icon pull-left color-fire-red">
					<i class="fa fa-group"></i>
					</div>
					<span class="panel-number"><strong>6</strong></span>
                </div>
                <div class="panel-footer text-right">
                    <a href="#" class="btn btn-primary" style="margin-bottom:0;">More <i class="glyphicon glyphicon-chevron-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                <i class="fa fa-bar-chart-o fa-fw"></i> News
	            </div>
	            <div class="panel-body">
	            	<div class="panel-icon pull-left color-aqua-blue">
					<i class="fa fa-file-text"></i>
					</div>
					<span class="panel-number"><strong>19</strong></span>
                </div>
                <div class="panel-footer text-right">
                    <a href="#" class="btn btn-primary" style="margin-bottom:0;">More <i class="glyphicon glyphicon-chevron-right"></i></a>
                </div>
            </div>
        </div>
	    <div class="col-lg-6">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                <i class="fa fa-bar-chart-o fa-fw"></i> Account Information
	            </div>
	            <div class="panel-body">
	            	<ul class="list-inline">
	            		<li class="col-md-6 text-right"><strong>Username</strong></li>
	            		<li class="col-md-6 col-sm-6"><?php echo $this->session->userdata('username'); ?></li>
	            	</ul>
	            	<ul class="list-inline">
	            		<li class="col-md-6 text-right"><strong>Last Login</strong></li>
	            		<li class="col-md-6 col-sm-6">11 May 2016 09:04:37</li>
	            	</ul>
                </div>
                <div class="panel-footer text-center">
                    <!--a href="#" class="btn btn-primary" style="margin-bottom:0;">Change Password</a-->
                    <?php echo anchor("auth/change_password/","Change Password",array('style'=>'margin-bottom:0;','class'=>'btn btn-primary'));?>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="panel panel-default">
	            <div class="panel-heading">
	                <i class="fa fa-bar-chart-o fa-fw"></i> Latest Inbox
	            </div>
	            <div class="panel-body">
	            	<ul class="col-md-12 list-unstyled">
	            		<li class="panel panel-default panel-body" style="padding:10px;">
	            			<ul class="list-inline">
			            		<li class="col-md-2"><strong>User 1</strong></li>
			            		<li class="col-md-10">20 May 2015 12:32:25</li>
			            	</ul>
			            	<ul class="list-inline">
			            		<li class="col-md-2"><strong>SUBJECT</strong></li>
			            		<li class="col-md-10">Lorem ipsum dolor sit amet</li>
			            	</ul>
	            		</li>
	            	</ul>
	            	<ul class="col-md-12 list-unstyled">
	            		<li class="panel panel-default panel-body" style="padding:10px;">
	            			<ul class="list-inline">
			            		<li class="col-md-2"><strong>User 1</strong></li>
			            		<li class="col-md-10">20 May 2015 12:32:25</li>
			            	</ul>
			            	<ul class="list-inline">
			            		<li class="col-md-2"><strong>SUBJECT</strong></li>
			            		<li class="col-md-10">Lorem ipsum dolor sit amet</li>
			            	</ul>
	            		</li>
	            	</ul>
	            	<ul class="col-md-12 list-unstyled">
	            		<li class="panel panel-default panel-body" style="padding:10px;">
	            			<ul class="list-inline">
			            		<li class="col-md-2"><strong>User 1</strong></li>
			            		<li class="col-md-10">20 May 2015 12:32:25</li>
			            	</ul>
			            	<ul class="list-inline">
			            		<li class="col-md-2"><strong>SUBJECT</strong></li>
			            		<li class="col-md-10">Lorem ipsum dolor sit amet</li>
			            	</ul>
	            		</li>
	            	</ul>
                </div>
            </div>
        </div>
    </div>
</div>
