<script type="text/javascript">
 $(function () {
   	var baseUrl  = "<?php echo base_url(); ?>";
   	
   	$('#account').click(function(){   	
   		var checked   = $('#account').is(':checked');
   		var label     = !checked ? 'Uncompleted' : 'Completed' 
   		var status    = !checked ? 'false' : 'true' ;;
   		var id_vendor = $('#id_vendor').val();
   		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token     = '<?php echo $this->security->get_csrf_hash(); ?>';
   		
   		
   		$.post( baseUrl + "vendor/checked_account", { csrf: token, id_vendor : id_vendor,status : status })
   				.done(function( resp ) {   			
   					$('#lbl_acc').html(label);		
		    		//$('#debug').html(resp);
		});
   		
   	
   	});
   	
   	$('#btn_cancel').click(function(){	  	
		document.location.href= baseUrl + 'vendor/';
   	});  
		 	   
 });

 </script>

<div id="debug"></div>

<h2>GENERAL INFORMATION</h2>

<table width="100%">
<tr>
<td><label>Vendor type</label></td><td>:</td>
<td><?php echo "<b>".ucfirst($vendor->type)."</b>";	 ?></td>
</tr>
<tr>
<td><label>Name </label></td><td>:</td>
<td><?php echo "<b>".ucwords($vendor->name)."</b>";	 ?></td>
</tr>
<tr>
<td><label>Address</label></td><td>:</td>
<td><?php echo "<b>".ucwords($vendor->address)."</b>"; ?></td>
</tr>
<tr>
<td><label>Post Code</label></td><td>:</td>
<td><?php echo "<b>".ucwords($vendor->postcode)."</b>"; ?></td>
</tr>
<tr>
<td><label>NPWP</label></td><td>:</td>
<td><?php echo "<b>".ucwords($vendor->npwp)."</b>"; ?></td>
</tr>
<tr>
<td><label>NPWP  Address</label></td><td>:</td>
<td><?php echo "<b>".ucwords($vendor->npwp_address)."</b>"; ?></td>

</tr>
<tr>
<td><label>NPWP  Post Code</label></td><td>:</td>
<td><?php echo "<b>".ucwords($vendor->npwp_postcode)."</b>"; ?></td>
</tr>
<tr>
<td><label>Phone</label></td><td>:</td>
<td><?php echo "<b>".ucwords($vendor->phone)."</b>"; ?></td>
</tr>
<tr>
<td><label>Fax</label></td><td>:</td>
<td><?php echo "<b>".ucwords($vendor->fax)."</b>"; ?></td>
</tr>
<tr>
<td><label>Email</label></td><td>:</td>
<td><?php echo "<b>".strtolower($vendor->email)."</b>"; ?></td>
</tr>
<tr>
<td><label>Web</label></td><td>:</td>
<td><?php echo "<b>".ucwords($vendor->web)."</b>"; ?></td>
</tr>

</table><br/>

<h2>BANK INFORMATION</h2>

<table width="60%">
<tr>
<td><label>Account Holder</label></td><td>:</td>
<td><?php echo "<b>".ucfirst($bank->acc_name)."</b>";	 ?></td>
</tr>
<tr>
<td><label>Account No</label></td><td>:</td>
<td><?php echo "<b>".ucwords($bank->acc_number)."</b>";	 ?></td>
</tr>
<tr>
<td><label>Bank Name</label></td><td>:</td>
<td><?php echo "<b>".ucwords($bank->bank_name)."</b>";	 ?></td>
</tr>
<tr>
<td><label>Country</label></td><td>:</td>
<td><?php echo "<b>".ucwords($bank->acc_country)."</b>";	 ?></td>
</tr>
<tr>
<td><label>Branch Name</label></td><td>:</td>
<td><?php echo "<b>".ucwords($bank->branch_name)."</b>"; ?></td>
</tr>
</table><br/>

<?php  if($this->permit->account){ ?>
<h2>ACCOUNTING INFORMATION</h2>	<?php 

$checked = $account->status=="1" ? true : false;
$label   = $account->status=="1" ? "Completed" : "Uncomplete";

$checkbox = array(
    'name'        => 'account',
    'id'          => 'account',
    'value'       => '1',
    'checked'     => $checked,
    'style'       => 'margin:10px');	
    
    
    echo form_checkbox($checkbox); ?><p id="lbl_acc"><?php echo $label; ?></p><br/><br/>
    
<?php echo form_open('vendor/create_account',array('id'=>'form')); ?>
<table width="60%">
<input type="hidden" name="id_vendor" id="id_vendor" value="<?php echo $id_vendor; ?>"/>	
<tr>
<td><label>Reconciliation Account</label></td><td>:</td>
<td>
	<?php  if($this->permit->recon){ ?>	
	<input type="text" name="recon_number" value="<?php echo $account->recon_number; ?>" placeholder="Reconciliation Account" class="form-control" required/>
	<?php  } else { echo $account->recon_number; }  ?>
</td>
</tr>	
<tr>
<td><label>Vendor Nr. SAP</label></td><td>:</td>
<td>
	<?php if($this->permit->sap){ ?>
	<input type="text" name="sap_number" value="<?php echo $account->sap_number; ?>" placeholder="Vendor Nr. SAP" class="form-control" required/>
	<?php } else { echo $account->sap_number; } ?>
</td>
</tr>
<?php if($this->permit->srm){ ?>
<tr>
<td><label>SRM Username</label></td><td>:</td>
<td><input type="text" name="srm_username" value="<?php echo $account->srm_username; ?>" placeholder="SRM Username" class="form-control" required/></td>
</tr>
<tr>
<td><label>SRM password</label></td><td>:</td>
<td><input type="password" name="srm_password" value="<?php echo $account->srm_password; ?>" placeholder="SRM Password" class="form-control" required/></td>
</tr>
<?php }  ?>


<tr><td colspan="3">&nbsp;</td
></tr>
<tr><td colspan="3">		
	<button type="submit" id="bt_submit" class="btn btn-info btn-primary">Submit</button>     
</td></tr>
</table>
<?php echo  form_close(); ?>
<?php  } ?>

<button type="button" id="btn_cancel" class="btn btn-info btn-primary">Previous</button>	
<br/><br/>

