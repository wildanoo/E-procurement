<div id="container5">

<script type="text/javascript">
 $(function () {
   	var baseUrl  = "<?php echo base_url(); ?>";
   	$('#update5').hide();
   	
   	$("#notaris_date").prop('disabled', true);
   	
   	$('#notaris_date').datepicker({  
        	changeMonth: true, changeYear: true,
			dateFormat: "yy-mm-dd" ,
     }); 
  	
   	$('#change5').click(function(){	
   	
   		$("#notaris_name").css("background","#ffffff");
	    $("#notaris_name").css("color","#827e85");             
	    $("#notaris_name").prop('readonly',false);
	    
	    $("#notaris_address").css("background","#ffffff");
	    $("#notaris_address").css("color","#827e85");             
	    $("#notaris_address").prop('readonly',false);
	    
	    $("#notaris_phone").css("background","#ffffff");
	    $("#notaris_phone").css("color","#827e85");             
	    $("#notaris_phone").prop('readonly',false);
	    
	    $("#notaris_number").css("background","#ffffff");
	    $("#notaris_number").css("color","#827e85");             
	    $("#notaris_number").prop('readonly',false);	    
	    
	    $("#notaris_date").css("background","#ffffff");
	    $("#notaris_date").css("color","#827e85");             
	    $("#notaris_date").prop('disabled', false);
	    
	    $("#remark").css("background","#ffffff");
	    $("#remark").css("color","#827e85");             
	    $("#remark").prop('readonly',false);	    
   	
   		$('#update5').show();
   		$('#change5').hide();
   		
   	});
   	
   	$('#update5').click(function(){	  	
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
   	    var values = $("#form5").serializeArray();
		values.push({name: csrf,value: token});
		values = jQuery.param(values); 
			$.ajax({
               url  : baseUrl + 'vendor/akta_update',
               type : "POST",              		               
               data : values,
               success: function(resp){
               	  $("#container5").load(baseUrl+'vendor/load_akta');
               }
            });
			
   		$('#update5').hide();
   		$('#change5').show();
   	});
	 	   
 });
 
 </script>
 
<div id="debug5"></div>
<?php echo form_open('#',array('id'=>'form5')); ?>
<table width="100%">
<tr>
<td><label>Notaris Name</label></td><td>:</td>
<td><?php 
	$notaris_name = array( 	    'name' => "notaris_name" ,
								'id'   => "notaris_name", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> ucfirst($akta->notaris_name)); 
	 echo form_input($notaris_name);	?>
</td>
</tr>
<tr>
<td><label>Notaris Address</label></td><td>:</td>
<td>
	<?php $notaris_address = array( 'name' => "notaris_address" ,
								'id'   => "notaris_address", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> ucfirst($akta->notaris_address)); 
	 echo form_input($notaris_address);	?>
</td>
</tr>
<tr>
<td><label>Notaris Phone</label></td><td>:</td>
<td>
	<?php $notaris_phone = array( 'name' => "notaris_phone" ,
								'id'   => "notaris_phone", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> ucfirst($akta->notaris_phone)); 
	 echo form_input($notaris_phone);	?>
</td>
</tr>
<tr>
<td><label>Notaris Number</label></td><td>:</td>
<td>
	<?php $notaris_number = array( 'name' => "notaris_number" ,
								'id'   => "notaris_number", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> ucfirst($akta->notaris_number)); 
	 echo form_input($notaris_number);	?>
</td>
</tr>
<tr>
<td><label>Notaris Date</label></td><td>:</td>
<td><?php $notaris_date = array( 'name' => "notaris_date" ,
								'id'   => "notaris_date", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 100px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> $akta->notaris_date); 
	 echo form_input($notaris_date);	?></td>
</tr>
<tr>
<td><label>Remark</label></td><td>:</td>
<td><?php $remark = array( 'name' => "remark" ,
								'id'   => "remark", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> ucfirst($akta->remark)); 
	 echo form_input($remark);	?></td>
</tr>
</table>

<div style="text-align: right;">
	<?php if($this->global->active){ ?>
	<button type="button" id="change5" class="btn btn-info btn-primary">Edit</button> 
    <button type="button" id="update5" class="btn btn-info btn-primary">Update</button>
    <?php } ?>
</div>

<?php echo  form_close(); ?>
<br/>
</div>



