<div id="owner">

<div id="owner_msg" class="text-center text-danger bg-warning"></div>

<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Owner Name</th>
		<th>Owner Address</th>
		<th>Owner Phone</th>
		<th>Owner Position</th>
		<th>Owner Shared</th>
		<th>Remark</th>
		<th>Approval</th>
		<th>Status</th>
		<th>#</th>
	</tr>
	</thead>
	<tbody>

 <?php $i=1;
 	foreach($owner as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->owner_name; ?></td>	
			<td><?php echo $row->owner_address; ?></td>
			<td><?php echo $row->owner_phone; ?></td>
			<td><?php echo $row->owner_position; ?></td>
			<td><?php echo $row->owner_shared; ?></td>				
			<td><?php echo $row->remark ?></td>
			<td><?php echo $row->approval ?></td>
			<td><span class="label label-warning"><?= $row->status == '0' || $row->status == '1' ? 'request':'approved'; ?></span></td>
			<td>
			<?php 
				echo '<button onclick="approve_owner([\''.$row->id.'\',\''.$row->approval.'\',\''.$row->id_tbl.'\'])" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>';
				echo '<button onclick="reject_temp_table('.$row->id.',\'temp_owner_avl\')" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>';
			?>
			</td>
		</tr>
<?php $i++; } ?>										
     </tbody>
</table>

</div>