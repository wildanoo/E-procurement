<div id="container">
<script type="text/javascript">

$(document).ready(function(){
   var baseUrl  = "<?php echo base_url(); ?>";	
   $(function () {    		
		$("#wyswyg").hide(); $("#upload4").hide();
   });

//    $('#upload_attach').click(function(){
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
// 		var values  = $("#form_attach").serializeArray();
// 				values.push({name: csrf,value: token});
// 				values = jQuery.param(values); 
// 						$.ajax({
// 						    url  : baseUrl + 'policy_agreement/add_file',
// 						    type : "POST",              		               
// 						    data : values,
// 						    success: function(resp){
// 							    //alert('tes');
// 						        //$('#debug').html(resp);
//  						    	document.location.href= baseUrl + 'policy_agreement/form_create';	
// 						    }
// 						});							     	
// 	 }); 
   
   $('#id_cat').change(function(){  
   		var id_cat = $('#id_cat').val();
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'policy_agreement/get_subcategory',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_cat='+id_cat,
               success: function(resp){
               	  $('#id_subcat').html(resp);
               }
            });
            $.ajax({
               url  : baseUrl + 'policy_agreement/get_vendor/',
               type : "POST",
               data : csrf +'='+ token +'&id_cat='+id_cat,
               success: function(resp){
               	  $('#id_vendor').html(resp);
               }
            });
   });  

   $("input:radio[name=content]").click(function () {
		var opt = $("input:radio[name=content]:checked").val();
		if (opt == '2'){
			$("#upload1").show();	 $("#wyswyg").hide(); 
		} else {
			$("#wyswyg").show(); $("#upload1").hide();	}
     });

   $('#btn_create').click(function(){
  		$("#container").load(baseUrl+'policy_agreement/form_attach');   		
   });

   //$('#btn_create').click(function(){  form_attach();  });	
   
}); 
</script>
<script>
//    function delete_file(id){
	 	//var baseUrl = "<?php //echo base_url(); ?>";
	 	//var csrf    = '<?php //echo $this->security->get_csrf_token_name(); ?>'; 
		//var token   = '<?php //echo $this->security->get_csrf_hash(); ?>';
	 	
// 	 	if (confirm('Are you sure you want to delete file?')) {	
// 			$.post( baseUrl + "policy_agreement/delete_file", { csrf: token, id: id } );
// 			$.load( baseUrl + "policy_agreement/form_create");	
// 			alert('ok');	
// 		}
		
// 	 } 
   </script>
<div class="page-header">
  <h3>New Vendor Policy</h3>
</div> 

<div id="debug"></div>
<?php echo form_open_multipart('policy_agreement/create',array('id'=>'form','novalidate' => 'true'));	$current_date = date('Y/m/d')?>

<table width="80%" border="0">
<tr>
	<td><label>Type</label></td><td>:</td>
	<td><?php
		$prop_cat = 'class="form-control" id="type" style="width:220px"';
		echo form_dropdown('type',$category,'0',$prop_cat); ?>			
	</td>	
</tr>
<tr id="option">
 <td><label>Content</label></td><td>:</td>
  <td>
	  <input type="radio" name="content" value="1"> Typing &nbsp;&nbsp;
	  <input type="radio" name="content" value="2" checked> Upload<br>
  </td>
</tr>
<tr id="wyswyg">
	<td colspan="2">&nbsp;</td>
	<td class="form_tab_content" width="75%;">
		<div>
			<ul class="nav nav-tabs test2">
			    <li class="active"><a data-toggle="tab" href="#idn" aria-expanded="true">Indonesia</a></li>
			    <li class=""><a data-toggle="tab" href="#eng" aria-expanded="false">English</a></li>
			</ul>

			<div class="tab-content">
			    <div id="idn" class="tab-pane fade active in" style="padding:15px;">
			      <?php $this->load->view('policy_agreement/note'); ?>
			    </div>
			    <div id="eng" class="tab-pane fade" style="padding:15px;">
			      <?php $this->load->view('policy_agreement/note'); ?>
			    </div>
			</div>
		</div>
	</td>
</tr>	
	
<tr id="upload1">
	<td colspan="2">&nbsp;</td>				
	<td class="form_tab_content" width="75%;">
		<div>
			<ul class="nav nav-tabs test2">
			    <li class="active"><a data-toggle="tab" href="#upidn" aria-expanded="true">Indonesia</a></li>
			    <li class=""><a data-toggle="tab" href="#upeng" aria-expanded="false">English</a></li>
			</ul>
			<?php $style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";?>
			<div class="tab-content">
			    <div id="upidn" class="tab-pane fade active in" style="padding:15px;">
			      <?php	
					$data  = array('type'=>'file','name'=> 'contentfileind','id' => 'file_upload_ind','value'=>'true','content'=>'Import','style'=>$style); 
					echo form_input($data); ?> <i>*.txt</i>
			    </div>
			    <div id="upeng" class="tab-pane fade" style="padding:15px;">
			      <?php	
					$data  = array('type'=>'file','name'=> 'contentfileeng','id' => 'file_upload_eng','value'=>'true','content'=>'Import','style'=>$style); 
					echo form_input($data); ?> <i>*.txt</i>
			    </div>
			</div>
		</div>
	</td>
</tr>
<tr>
	<td style="vertical-align: top;"><label>Title</label></td><td style="vertical-align: top;">:</td>
	<td><textarea name="title" id="title" cols="20" rows="5" class="form-control" required></textarea></td>
</tr>
<tr id="creator_area">
	<td><label>Creator</label></td><td>:</td><?php echo form_hidden("creator",$username); ?>
	<td>&nbsp;&nbsp;<?php echo ucfirst($username); ?> <!--input type="text" name="creator" id="creator" placeholder="Creator" class="form-control"/--></td>
</tr>

<tr>
<td><h1>Attachment</h1></td></br>
<!-- td><button type="button" id="btn_create" class="btn btn-info btn-primary">Add File</button></td> -->
<td><a href="<?php echo base_url('policy_agreement/form_attach'); ?>" type="button" class="btn btn-primary btn-sm">Add File</a></td>
</tr>

<table id="browse" class="table table-striped table-bordered" style="font-size: 13px">
	<thead>
        <tr>
            <th>No</th>
            <th>Deskripsi</th>
            <th>Keterangan</th>
            <th>Created Date</th>
            <th>Attachment</th>           
            <th>Action</th>
        </tr>
	</thead>
	
	<tbody>
<?php			
 if($cari){ $i=1;
 	foreach($cari as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->deskripsi; ?></td>	
			<td><?php echo $row->keterangan; ?></td>
			<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
			<td><?php echo anchor('policy_agreement/open_content_pdf/'.$row->id,"Viewed",array('class'=>'default_link','title'=>'Viewed')); ?></td>
			<td style="text-align: center;">
			<?php				
					//echo '<i class="fa fa-trash"  onclick="delete_file('.$row->id.')"></i>';	
			?>
				<?php	
				$trash  = '<i class="fa fa-trash"></i>';
				$js     = "if(confirm('Are you sure to delete record ?')){ return true; } else {return false; }";					
				echo anchor('policy_agreement/delete_file/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Record','onclick'=>$js));?>
			</td>											
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>
    </tbody>
</table>

<tr>
	<td colspan="3" style="text-align: right;">
		<input type="submit" value="Submit" class="btn btn-primary btn-sm"/>
		<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>'Cancel'); ?>					 
		<?php  echo anchor('policy_agreement/','Cancel',$prop); ?>
	</td>
</tr>
<?php echo form_close(); ?>

</table>
</div>