<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('security');
		
		if (!$this->session->userdata('lang')) {
			$this->session->set_userdata('lang','eng');
		}			
	}

	function index($param)
	{
		// if ($message = $this->session->flashdata('message')) {
		// 	$this->load->view('auth/general_message', array('message' => $message));
		// } else {
		// 	redirect('/auth/login/');
		// }

		$this->load->view('article/page');
	}

}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */