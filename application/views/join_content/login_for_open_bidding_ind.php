<script type="text/javascript"> 
    $(document).ready(function(){
        $('#create').click(function(){
            var baseUrl    = "<?php echo base_url(); ?>";
            var login      = $('#login').val();
            var password   = $('#password').val();
            var remember   = $('#remember').val();
            var gen        = $('#gen').val();
            var idx        = $('#idx').val();
            var content_id = $('#content_id').val();
            var csrf       = '<?php echo $this->security->get_csrf_token_name(); ?>';
            var token      = '<?php echo $this->security->get_csrf_hash(); ?>';

            $.ajax({
                url  : baseUrl + 'auth/validate_login',
                type : "POST",
                data : csrf +'='+ token +'&login='+login+'&password='+password+'&remember='+remember+'&content_id='+content_id,
                success: function(resp){
                    // $('#debug').html(resp);
                    var json = $.parseJSON(resp);
                    if(json.status=="false"){
                        var div = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+json.errors+'</div>';
                        $("#message").html(div);
                    } else {
                        $('#contact-form').submit();
                    }
                }
            });
        });
    });

    $(function(){

        $("input[type=file]").change(function(){
            var filename = $(this).val() ;
            filex = filename.split(".");
            tipe = filex[filex.length-1] ;

            if(tipe!='pdf') { $(this).val(""); alert('File harus berformat pdf'); }
        });

    });

</script>

<div class="about-content-wrap pull-left" id="#">
    <div class="head" style="margin-top: 25px;">
        <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;">Verifikasi</h1>
        <a href="#" class="next-section about animate2">
            <i class="fa fa-angle-double-down"></i>
            <input type="hidden" id="gen" value="<?php echo $generete ;?>" />
            <input type="hidden" id="idx" value="<?php echo $idx ;?>" />
        </a>
    </div>
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 content-tabs-wrap wow fadeInUp animated" data-wow-delay="1.7s" data-wow-offset="200">
            <div class="col-xs-12 col-sm-12 col-md-12 content-tabs no-pad">            
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#" data-toggle="tab"> Open Bidding </a></li>
                  <li><a>&nbsp;</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-home">
                        <div class = "row">
                            <div class="col-md-12 tab-content">
                                <div class="box2 box-info2 tab-pane fade in active" id="registrasi-step-1">
                                    <div class="box-body">
                                        <div class="about-intro-wrap pull-left">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 faq-tabs-wrap">
                                                    <div class="tabbable tabs-left">
                                                        <div class="tab-content col-md-12 col-sm-12 col-xs-12">
                                                            <div class="tab-pane active" id="a">
                                                                
                                                                <div class="row">
                                                                    <div class="col-md-6 wow fadeInRight animated" data-animate="fadeInLeft" style="">
                                                                        <?php
                                                                            $login = array( 'type'          => 'email',
                                                                                            'placeholder'   => 'Username',
                                                                                            'name'          => 'login',
                                                                                            'id'            => 'login',
                                                                                            'value'         => set_value('login'),
                                                                                            'maxlength'     => 80,
                                                                                            'size'          => 30,);
                                                                            $content_id = array( 'type'          => 'hidden',
                                                                                                 'name'          => 'content_id',
                                                                                                 'id'            => 'content_id',
                                                                                                 'value'         => $this->uri->segment(3));
                                                                            if ($login_by_username AND $login_by_email) {
                                                                                $login_label = 'Email or login';
                                                                            } else if ($login_by_username) {
                                                                                $login_label = 'Login';
                                                                            } else {
                                                                                $login_label = 'Email';
                                                                            }
                                                                            $password = array(  'type'          => 'password',
                                                                                                'placeholder'   => 'Password',
                                                                                                'name'          => 'password',
                                                                                                'id'            => 'password',
                                                                                                'size'          => 30,);
                                                                            $remember = array(
                                                                                'type'  => 'checkbox',
                                                                                'name'  => 'remember',
                                                                                'id'    => 'remember',
                                                                                'value' => 1,
                                                                                'checked'   => set_value('remember'),
                                                                                'class' => 'icheckbox_square-blue',
                                                                            );
                                                                            $captcha = array(
                                                                                'name'  => 'captcha',
                                                                                'id'    => 'captcha',
                                                                                'maxlength' => 8,
                                                                            );

                                                                            $regnew = $_SERVER['REQUEST_URI'] .'/true';
                                                                            $uri_ = $_SERVER['REQUEST_URI'];
                                                                            if(substr($uri_,-1,1)=='/') $regnew = $uri_.'true'; 
                                                                        ?>
                                                                        <?php echo form_open($this->uri->uri_string(),array('id'=>'contact-form')); ?>
                                                                            <h1>Silakan login terlebih dahulu untuk melanjutkan</h1>
                                                                            <a href="<?php echo $regnew; ?>" >Vendor baru?</a>
                                                                            <br><br>
                                                                            <div id="message">
                                                                            </div>
                                                                            <?php echo form_input($content_id); ?>
                                                                            <div class="text-fields">
                                                                                <div class="float-input">
                                                                                    <?php echo form_input($login); ?>
                                                                                    <span><i class="fa fa-user"></i></span>
                                                                                </div>
                                                                                <?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
                                                                                <div class="float-input">
                                                                                    <?php echo form_password($password); ?>
                                                                                    <span><i class="fa fa-lock"></i></span>
                                                                                </div>
                                                                                <?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
                                                                                <input type="button" id="create" class="main-button" value="Masuk"><br><br>
                                                                                <div align="right"><a href="#" >Lupa Password?</a></div>
                                                                            </div>
                                                                        <?php echo form_close(); ?>
                                                                    </div>

                                                                    <div class="col-md-6 wow fadeInRight animated" data-animate="fadeInLeft" style="">
                                                                        <form id="contact-form">
                                                                            <h1><?php echo $this->lang->line('section5_sub2');?></h1>
                                                                            <div class="text-fields">
                                                                                <p class="first-line"><?php echo $this->lang->line('section5_sub3');?></p>
                                                                                <p><?php echo $this->lang->line('section5_sub4');?> <a href="#"><?php echo $this->lang->line('section5_sub5');?></a></p>
                                                                                <br>
                                                                                <p class="first-line"><?php echo $this->lang->line('section5_sub6');?></p>
                                                                                <p><?php echo $this->lang->line('section5_sub7');?> <a href="#"><?php echo $this->lang->line('section5_sub5');?></a></p>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        </div>
    </div>
</div>