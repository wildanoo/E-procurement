 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Announcement</h3>
              </div><!-- /.box-header -->

                <form class = "form-horizontal">
                <br>
                <div class="col-md-2 pull-left">
                    <a href="#"><button class="btn btn-primary" id = "btn-pd">Add</button></a>
                </div>
                <div class = "col-md-10 pull-right">
                  <div class="input-group pull-right" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search" style="height: 28px;">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                </div>

                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover text-center">
                    <thead>
                      <tr>
                        <th><input type="checkbox" name="cba[]" id="cba" /></th>
                        <th>No</th>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Detail</th>
                        <th>Validity Period</th>
                        <th>Status</th>
                        <th>Created Date</th>
                        <th>Action</th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td><input type="checkbox" name="ch[]" value="" class="ch"></td>
                        <td>1</td>
                        <td>News</td>
                        <td>Coba1</td>
                        <td>Nananana</td>
                        <td>222</td>
                        <td>222</td>
                        <td>222</td>
                        <td>
                            <a href="#" class="small-box-footer" title = "Edit"><i class="fa fa-pencil"></i></a>
                            <a href="#" class="small-box-footer" title = "Delete"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>

                      <tr>
                        <td><input type="checkbox" name="ch[]" value="" class="ch"></td>
                        <td>2</td>
                        <td>News</td>
                        <td>Coba2</td>
                        <td>Nininini</td>
                        <td>222</td>
                        <td>222</td>
                        <td>222</td>
                        <td>
                            <a href="#" class="small-box-footer" title = "Edit"><i class="fa fa-pencil"></i></a>
                            <a href="#" class="small-box-footer" title = "Delete"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>

                      <tr>
                        <td><input type="checkbox" name="ch[]" value="" class="ch"></td>
                        <td>3</td>
                        <td>Open Sourcing</td>
                        <td>Coba3</td>
                        <td>Nunununu</td>
                        <td>222</td>
                        <td>222</td>
                        <td>222</td>
                        <td>
                            <a href="#" class="small-box-footer" title = "Edit"><i class="fa fa-pencil"></i></a>
                            <a href="#" class="small-box-footer" title = "Delete"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                    </tbody>

                  </table>
                </div><!-- /.box-body -->
              </form>
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->