<div id="container2">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	
   
   $('#btn_create2').click(function(){ 
   		$("#container2").load(baseUrl+'data_vendor/form_categories');   		
   });             	
   
 }); 
 
 function update_cat(id){
 	
 	$("#container2").load(baseUrl+'data_vendor/form_update_category/'+id); 
 }
 
 function delete_cat(id){
 	var baseUrl = "<?php echo base_url(); ?>";
 	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
 	
 	if (confirm('Are you sure you want to delete vendor category?')) {	
		$.post( baseUrl + "data_vendor/delete_subcategory", { csrf: token, id: id } );
		$("#container2").load(baseUrl+'data_vendor/load_categories', function(){$('#cmsg').html('Data akan berubah setelah proses approval disetujui');});		
	}
	
 } 
  
 </script>
 <div id="cmsg" class="bg-warning text-center text-danger"></div>
 <br>
 <?php if($this->permit->upven){ ?>
<button type="button" id="btn_create2" class="btn btn-info btn-primary">Add New</button> <?php } ?>
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Category</th>
		<th>Sub Category</th>		
		<?php if($this->permit->upven){ ?><th>Action</th><?php } ?>
	</tr>
	</thead>
	<tbody>
 <?php  //$categories = array();
 if($categories){ $i=1;
 	foreach($categories as $row){ ?>
 		<tr>
 			<td style="text-align: center"><?php echo $i; ?></td>
			<td><?php echo $row->category; ?></td>	
			<td><?php echo $row->subcategory; ?></td>
			<?php if($this->permit->upven){ ?>			
			<td style="text-align: center;"><?php
					echo '<a href="javascript:void(0)"><i class="fa fa-pencil" onclick="update_cat('.$row->id.')"></i></a> | '; 				
					echo '<a href="javascript:void(0)"><i class="fa fa-trash"  onclick="delete_cat('.$row->id.')"></i></a>';	
			?></td>	
			<?php } ?>										
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="3">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>