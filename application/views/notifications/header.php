<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this tag, the sky will fall on your head -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>e-Procurement Garuda Indonesia</title>
	
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/css/email.css" />

</head>
<style type="text/css">
.header-panel {
    /* position: fixed; */
    background: url("<?php echo base_url('assets_/images/subheading-bg.jpg') ?>");
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    background-color: #199aac;
    /*background-color: #1A1D52;*/
    width: 100%;
    height: 10px;
    display: block;
    z-index: 999;
    top: 85px;
    -moz-box-shadow: 0px 2px 10px rgba(0,0,0,0.4);
    -webkit-box-shadow: 0px 2px 10px rgba(0,0,0,0.4);
    box-shadow: 0px 2px 10px rgba(0,0,0,0.4);
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    left: 0;
}
</style>
<body bgcolor="#ebf5f8">
<table class="head-wrap" bgcolor="#fff">
	<tr>
		<td></td>
		<td class="header container">
			<div class="content">
				<table bgcolor="#fff">
					<tr>
						<td><img src="<?php echo base_url('assets_public/images/garuda-logo.png') ?>" /></td>
						<td align="right"><h6 class="collapse" style="text-transform: none;"><span style="color: #103b7a">e-Procurement</span> <span style="color: #5fc6d5"> Garuda Indonesia</span></h6></td>
					</tr>
				</table>
			</div>		
		</td>
		<td></td>
	</tr>
</table>
<div class="header-panel"></div>
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">
            <div class="content">