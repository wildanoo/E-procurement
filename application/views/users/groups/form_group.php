 <script>
 	$(function(){
 		$('.checkall').click(function(){
 			$(this).closest('div').find(':checkbox').prop('checked', this.checked);
 		});
 		$('.target').click(function(){
			var datastyle = $(this).attr('style');
			if(datastyle){
				$(this).removeAttr('style');
			}else{
				$(this).attr('style','color: #fff;background-color: #368ccc;border-radius: 0px;');
			}
		});
 	});

 </script>
 
 <div class="row">
 	<div class="col-md-12">
 		<div class="widget widget-nopad">
 			<div class="col-md-12">
 				<!--form class = "form-horizontal"-->
 				<?php echo form_open("user/create_group",array('class'=>'form-horizontal')); ?>
 				<div class="box box-info">
 					<div class="box-header">
 						<h3 class="box-title" style="padding: 5px 0 16px 0;">Create Group</h3>
 						<div class="box-footer"></div>
 						<div class="col-md-12" style="padding: 12px 0 12px 0;">
 							<div class="col-md-2">Group Name </div>
 							<div class="col-md-1 text-center">:</div>
 							<div class="col-md-9"><input type="text" name="name" id="name" class="form-control" placeholder="Group Name"/></div>
 						</div>             	
 						<div class="col-md-12" style="padding: 12px 0 12px 0;">
 							<div class="col-md-2">Permissions </div>
 							<div class="col-md-1 text-center">:</div>
 							<div class="col-md-9">				 
 								<!-- <div class="container"> -->
 								<div class="panel-group">
				    <!-- <div class="panel panel-primary" style="">
				      <div class="panel-heading">Registration Vendor</div>
				      <div class="panel-body">              	<?php 
              			 echo form_hidden("ttl",count($perm));
              			 $i = 1;
                         foreach($perm as $row){
						 	$value = $row->value.":".$row->id;
						 	$function = $row->id_function;
						 	if ($function == 1){
						 	echo form_checkbox('check'.$i,$value, false); echo $row->name."<br/>";}
						$i++; }  ?></div>
				    </div>
				
				    <div class="panel panel-primary">
				      <div class="panel-heading">Approval Vendor</div>
				      <div class="panel-body">              	<?php 
              			 echo form_hidden("ttl",count($perm));
              			 $i = 1;
                         foreach($perm as $row){
						 	$value = $row->value.":".$row->id;
						 	$function = $row->id_function;
						 	if ($function == 2){
						 	echo form_checkbox('check'.$i,$value, false); echo $row->name."<br/>";}
						$i++; }  ?></div>
				    </div>
				
				    <div class="panel panel-primary">
				      <div class="panel-heading">Content Management</div>
				      <div class="panel-body">              	<?php 
              			 echo form_hidden("ttl",count($perm));
              			 $i = 1;
                         foreach($perm as $row){
						 	$value = $row->value.":".$row->id;
						 	$function = $row->id_function;
						 	if ($function == 3){
						 	echo form_checkbox('check'.$i,$value, false); echo $row->name."<br/>";}
						$i++; }  ?></div>
				    </div>
				
				    <div class="panel panel-primary">
				      <div class="panel-heading">Bidding Management</div>
				      <div class="panel-body">              	<?php 
              			 echo form_hidden("ttl",count($perm));
              			 $i = 1;
                         foreach($perm as $row){
						 	$value = $row->value.":".$row->id;
						 	$function = $row->id_function;
						 	if ($function == 4){
						 	echo form_checkbox('check'.$i,$value, false); echo $row->name."<br/>";}
						$i++; }  ?></div>
				    </div>
				    
				        <div class="panel panel-primary">
				      <div class="panel-heading">Sourcing Management</div>
				      <div class="panel-body">              	<?php 
              			 echo form_hidden("ttl",count($perm));
              			 $i = 1;
                         foreach($perm as $row){
						 	$value = $row->value.":".$row->id;
						 	$function = $row->id_function;
						 	if ($function == 5){
						 	echo form_checkbox('check'.$i,$value, false); echo $row->name."<br/>";}
						$i++; }  ?></div>
					</div>-->
					<div class="accordion" id="accordion">
						<?php 
						$n=1;
						foreach($perm as $rs=>$key) { 
							foreach($key as $name=>$val) { ?>
								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle target" data-toggle="collapse" data-parent="#accordion"  href="#collapse<?php echo $n ?>"><?php echo $name; ?></a>
									</div>
									<?php //echo $name; ?>
									<div id="collapse<?php echo $n ?>" class="accordion-body collapse">
										<div class="accordion-inner" style="padding: 0;">             	
											<?php 
											echo form_hidden("ttl",count($perm));
											$idc = "class='checkall'";?>
											<b>
												<div class="box-body" style="padding: 0;">
													<table id="example2" class="table table-bordered2 table-hover text-center">
														<thead>
															<tr>
																<th><?php echo form_checkbox('checkall','accept',false,$idc);?></th>
																<th>Check all</th>
															</tr>
														</thead>
														<tbody>
															<?php
															foreach($val as $row){
																$value = $row['value'].":".$row['id'];?>
																<tr>
																	<td style="border-top: none;text-align: center;"><?php echo form_checkbox('check[]',$value, false);?></td>
																	<td style="border-top: none;text-align: center;"><?php echo $row['name'];}?></td>
																</tr>
																<?php }  //} ?>
															</tbody>
														</table>
													</div>
												</b>
											</div>
										</div>
									</div>
									<?php $n++; } ?>
								</div>
							</div>
							<!-- </div>   -->
						</td></tr>
					</div>
				</div><!-- col-md-6 -->                
			</div><!-- /.box-body -->
			<div class="box-footer" style="padding-left: 30px;">
				<div class="col-md-3">
				<?php  $prop = array('class'=>'btn btn-sm btn-default','title'=>'Cancel', 'style'=>'margin-bottom: 0px'); ?>					 
					<?php  echo anchor('user/groups','Cancel',$prop); ?>
					<?php  echo form_submit('submit', 'Submit','class="btn btn-sm btn-primary"'); ?>
				</div>
			</div><!-- /.box-footer -->
			<?php echo form_close(); ?>
		</div><!-- /.box -->
	</div>
</div>
