<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   
   $('#btn_create6').click(function(){
    	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		var values  = $("#form_owner").serializeArray();
				values.push({name: csrf,value: token});
				values = jQuery.param(values); 
						$.ajax({
						    url  : baseUrl + 'data_avl/update_owner',
						    type : "POST",              		               
						    data : values,
						    success: function(resp){
						        $("#container6").load(baseUrl+'data_avl/load_owner', function(){$('#omsg').html('Data akan ditampilkan setelah proses approval disetujui');});						    	
						    }
						});							     	
     });     
     
   $('#btn_cancel6').click(function(){ 
   		 $("#container6").load(baseUrl+'data_avl/load_owner');
    });     
	   
   });

</script>
<div id="debug6"></div>
<?php echo form_open_multipart('#',array('id'=>'form_owner')); //$current_date = date('Y-m-d'); ?>
<input type="hidden" name="id" id="id" value="<?php echo $default->id; ?>"/>
<table width="50%">
<tr>
	<td><label>Name</label></td><td>:</td>
	<td><input type="text" name="owner_name" id="owner_name" value="<?php echo $default->owner_name; ?>" placeholder="Document Name" class="form-control" required/></td>
	<td id="msg1">&nbsp;</td>	
</tr>
<tr>
	<td><label>Address</label></td><td>:</td>
	<td><input type="text" name="owner_address" id="owner_address" value="<?php echo $default->owner_address; ?>" placeholder="Document Number" class="form-control" required/></td>
	<td id="msg2">&nbsp;</td>	
</tr>
<tr>
	<td><label>Phone</label></td><td>:</td>
	<td><input type="text" name="owner_phone" id="owner_phone" value="<?php echo $default->owner_phone; ?>"placeholder="Description" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Position</label></td><td>:</td>
	<td><input type="text" name="owner_position" id="owner_position" value="<?php echo $default->owner_position; ?>"placeholder="Description" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Position</label></td><td>:</td>
	<td><input type="text" name="owner_shared" id="owner_shared" value="<?php echo $default->owner_shared; ?>"placeholder="Description" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Remark</label></td><td>:</td>
	<td><input type="text" name="remark" id="remark" value="<?php echo $default->remark; ?>" placeholder="Remark" class="form-control" required/></td>
</tr>
</table>

<div style="text-align: right;">
	<input type="button" id="btn_cancel6" value="Cancel" class="btn btn-default btn-sm"/>
	<input type="button" id="btn_create6" value="Submit" class="btn btn-primary btn-sm"/>
</div>
<br/>
<?php echo  form_close(); ?>


