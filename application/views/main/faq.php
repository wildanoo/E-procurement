<div class="about-intro-wrap pull-left">
    <a name="faq"><span></a>
    <div class="head" style="margin-top: 85px;padding-left:0;padding-right:0;">
        <h1>FAQS</h1>
        <a href="#faq" class="next-section about animate2">
            <i class="fa fa-angle-double-down"></i>
        </a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 faq-tabs-wrap wow fadeInUp animated" data-wow-delay="0.5s" data-wow-offset="200">
            <?php if (isset($faq) && count($faq) > 0) { ?>
             
            
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs col-md-4 col-sm-4 col-xs-5">
                    <?php $i = 0; foreach ($faq as $content) { $i++; ?><?php //print_r (count($faq)); exit;?>
                        <li <?php if($i == 1) echo 'class="active"'; ?>><a data-toggle="tab" href="#a<?php echo $content->id; ?>" ><span class="faq-ques"><?php $question = $this->lang->line('question');  echo $content->{"$question"}; ?></span><i class="right-arr"></i></a></li>                    
					<?php } ?>
                    </ul>

                    <div class="tab-content col-md-8 col-sm-8 col-xs-7 pull-right">
                    <?php $i = 0; foreach ($faq as $content) { $i++; ?>
                        <div class="tab-pane  <?php if($i == 1) echo 'active'; ?>" id="a<?php echo $content->id; ?>">
                            <div class="dept-title-tabs"><?php $question = $this->lang->line('question'); echo $content->{"$question"}; ?></div>
                            <!-- div class="dept-title-tabs">Bagaimana Rekanan dapat mengikuti proses pengadaan Garuda?</div> -->
                            <!-- p>Setiap rekanan yang ingin mengikuti proses pengadaan di PT Garuda Indonesia harus melakukan registrasi terlebih dahulu dengan mengirimkan data profile perusahaan dan kelengkapan bukti dokumen sesuai dengan bidang usahanya.</p> -->
                        	<p><?php $answer = $this->lang->line('answer');  echo $content->{"$answer"}; ?></p>
                        </div><?php } ?>
                    </div>
                </div>
                
             <?php } ?>
             </br></br>
			<table>
                                            	<?php                                             	
                                            	$dwld_ico = '<img src="'.base_url().'assets/images/download.png" width="30" height="25"/>'; 
                                            	$prop    = array('class'=>'default_link','title'=>'FAQ','style'=>' text-decoration: none;'); ?>
                                            	<tr>
                                            		<td><?php echo anchor('faq/download_faq/',$dwld_ico."FAQ",$prop);  ?></td>
                                            	</tr>
                                            </table>
            </div>
        </div>
    </div>
</div>
                                            