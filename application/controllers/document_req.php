<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Document_req extends Controller {

	function __construct()
	{
		parent::__construct();
		islogged_in();
	}	
		
	function index(){
		if ($this->tank_auth->is_logged_in()) {

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			
			$sess_like  = $this->session->flashdata('like');
			
	 		$table 	    = "document_req";		
			$page       = $this->uri->segment(3);
			$per_page   = 5;		
			$offset     = $this->crud->set_offset($page,$per_page);
			$total_rows = $this->crud->get_total_record("",$table);
			$set_config = array('base_url'=> base_url().'/document_req/index','total_rows'=>$total_rows,'per_page'=>5,'uri_segment'=>3);
			$config     = $this->crud->set_config($set_config);		
				 
			$this->load->library('pagination');			 
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();			
				
			$order 			     = array('field'=>'created_date','order'=>'DESC');
			$data['pagination']  = $paging;
			$data['num']         = $offset; 
			$select 			 = "id,document_req,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
			
			if($sess_like){
			$like   = array('col'=>'document_req','field'=>$sess_like);
			$browse = $this->crud->browse("",$table." l","","","true",$select,"","","",$like);	} else {
			$browse = $this->crud->browse_with_paging("",$table." l","","","true",$select,"",$order,$config['per_page'],$offset); }
			
			$data['browse'] = $browse;
	 		$data['view']	= "document_req/browse"; 		
 			$this->load->view('layout/template',$data);
 		
	 		
		} else {  
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/'); 	 }
	}	
	
	
	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
	
		//$search_dateranges = array('publish_date',$_POST['start_date'],$_POST['end_date']);
		$search_conditions = array(
				'document_req'		=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		$data['view'] = "document_req/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{
	
		/* get data for table view from database with pagination */
	
		$table 	     = "document_req t1";
		$select 	 = "t1.id,document_req,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'document_req/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
	
		/* get data for table view from database with pagination */
	
		$table 	     = "document_req t1";
		$select 	 = "t1.id,document_req,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,$where,$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'document_req/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	function get_tags(){ 	
	 	$term   = $_GET['term'];  	
	 	$order  = array('field'=>'document_req','sort'=>'ASC'); $select = "document_req as name";
	 	$result = $this->crud->autocomplete("","document_req",$select,"document_req",$term,"",$order);  	
	 	echo json_encode($result);   	 		
	}
	
	function set_sess_search(){
		$like = $_POST['search']; 	
	 	$this->session->set_flashdata('like',$like); 
	}
	
	function is_exist(){
		$document_req	  = $_POST['document_req'];
		$is_exist['1']= !$document_req ? "false" : "true";	
		$msg['1']	  = !$document_req ? "document name required" : "";
		
		if ($is_exist['1']=='true'){
			$where   = array('document_req'=>$document_req);
			$checked = $this->crud->is_exist("","document_req","id",$where);
			$is_exist['2']    = !$checked ? "true" : "false";
			$msg['2']	= $checked ? "duplicate document name" : "";
				
		}
		
		$msg = implode(" ", $msg);
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg' =>$msg);
	
		echo json_encode($result);
	}
	
	function create(){
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array('id'=>null,'document_req'=>$_POST['document_req'],
					  'created_id'=>$userID,'created_date'=>$curr_date,'last_updated'=>$curr_date);
		$id = $this->crud->insert("","document_req",$data);
		$this->session->set_flashdata('message','1 data success insert');
	}
	
	function form_create(){
		$data['data'] = $this->crud->browse("","document_req","","","true","id,document_req");
 		$this->load->view('document_req/form_create',$data);
	}
	
	function update(){
		$curr_date =  date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data = array('document_req'=>$_POST['document_req'],'created_id'=>$userID,'last_updated'=>$curr_date,'status'=>'1');
		$this->crud->update("","document_req","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
		redirect('document_req/','refresh');
	}
	
	function form_update(){
		$id 		 = $this->uri->segment(3);
		$select 	 = "id,document_req";
		$data['def'] = $this->crud->browse("","document_req","id",$id,"false",$select);
 		$this->load->view('document_req/form_update',$data);
 		
	}
	
	function delete(){
		$id = $this->uri->segment(3);
		$status = array('status'=>'0');
		$this->crud->update('','document_req','id',$id,$status);
		redirect('document_req/','refresh');
	}
	
	function delete_(){
	
		$id = $this->uri->segment(3);
		$doc_req = $this->crud->browse("","document_req","id",$id,"false","document_req");
		$like   = array('col'=>'document_req_id','field'=>$doc_req->document_req);
				
		$select = $this->crud->browse("","subcontent_type","","","true","","","","",$like);
		//print_r($select);exit;
		
		if(sizeof($select) > 0){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		} else {
			$this->crud->delete("","document_req","id",$id); 
			$this->session->set_flashdata('message','1 data success deleted');
		}
		
		redirect('document_req/','refresh');
	
	}
	
	function delete__(){
		
		$id = $this->uri->segment(3);
		$doc_req = $this->crud->browse("","document_req","id",$id,"false","id,document_req");
		$like   = array('col'=>'document_req_id','field'=>$doc_req->id);
		
		$select = $this->crud->browse("","subcontent_type","","","true","","","","",$like);
		//print_r($like);exit;
		
		if(sizeof($select) > 0){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		} else {
			$this->crud->delete("","document_req","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
		
		redirect('document_req/','refresh');
		
		}
}