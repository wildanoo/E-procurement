<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Notification extends Controller {

	function __construct()
	{
		parent::__construct();			
	}

	function index(){
		
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
			
		$table 	    = "notification";
		$page       = $this->uri->segment(3);
		$per_page   = 10;
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table);
		$set_config = array('base_url'=> base_url().'/notification/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);
			
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();
	
		$order 			     = array('field'=>'created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;
		$select 			 = "id,desc_eng,desc_ind,status,
								(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$sessID  = $this->session->flashdata('anID');
		$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
	
		if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
		else  $browse  = $this->crud->browse_with_paging("",$table." l",'status','1',"true",$select,"",$order,$config['per_page'],$offset);
		$data['browse'] = $browse;
			
		$data['view']	= "notification/browse";
		$this->load->view('layout/template',$data);
	}
	
	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
	
		$search_conditions = array(
				'desc_ind'		=> $_POST['search_term'],
				'desc_eng'		=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		$data['view'] = "notification/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{
		/* get data for table view from database with pagination */
	
		$table 	     = "notification t1";
		$select 	 = "id,desc_ind,desc_eng,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'notification/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
	
		/* get data for table view from database with pagination */
	
		$table 	     = "notification t1";
		$select 	 = "id,desc_ind,desc_eng,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,'t1.status = "1"',$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'notification/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}

	function form_create(){
		
		$data ['user_id'] = $this->tank_auth->get_user_id();
		$data ['username'] = $this->tank_auth->get_username();
		$data['view'] = 'notification/form_create';
		$this->load->view('layout/template',$data);
		
	}
	
	function create(){
		
		$curr_date 	= date('Y-m-d H:i:s'); 
		$userID = $this->tank_auth->get_user_id();
		$data = array(	'desc_ind'		=>$_POST['desc_ind'],
						'desc_eng'		=>$_POST['desc_eng'],
						'status'		=>'1',
						'created_id'	=>$userID,
						'created_date'	=>$curr_date,
						'last_updated'	=>$curr_date
		);
		$where 	     = array('desc_ind'=>$_POST['desc_ind']);
	 	$is_exist    = $this->crud->is_exist("","notification","id",$where);	
	 	
	 	if(!$is_exist){ 
			$this->crud->insert("","notication",$data); }
				
		$msg1 = "succesfully add new question";
		$msg2 = "duplicate duestion";
		$msg  = !$is_exist ? $msg1 : $msg2;
		
		$this->session->set_flashdata('message',$msg);
		redirect('notification/','refresh');	
	}
	
	function form_update(){
		
		$id = $this->uri->segment(3);
		$select = 'id,desc_eng,desc_ind,status,created_id,created_date,last_updated';
		$def = $this->crud->browse('','notification','id',$id,$select);
		$data['def'] = $def;
		$data['view'] = 'notification/form_update';
		$this->load->view('layout/template',$data);
	}
	
	function update(){

		$id = $_POST['id'];
		$curr_date 	= date('Y-m-d H:i:s');
		$userID = $this->tank_auth->get_user_id();
		$desc_ind = $_POST['desc_ind'];
		$desc_eng = $_POST['desc_eng'];
		$select = array('desc_ind'=>$desc_ind,'desc_eng'=>$desc_eng,'status'=>'1','created_id'=>$userID,'last_updated'=>$curr_date);
		$this->crud->update("","notification","id",$id,$select);
		$this->session->set_flashdata('message','1 data success updated');
		redirect('notification/','refresh');
	}
	
	function delete(){
		$id = $this->uri->segment(3);
		$status = array('status'=>'0');
		$this->crud->update("","notification","id",$id,$status);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('notification/','refresh');
	}
}
