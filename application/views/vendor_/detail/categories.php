<?php  //print_r($categories); exit; ?>

<div id="container14">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	
   
   $('#btn_create14').click(function(){ 
        //alert('test'); 
   		$("#container14").load(baseUrl+'vendor/form_categories');   		
   });             	
   
 }); 
 
 function update_cat(id){
 	
 	$("#container14").load(baseUrl+'vendor/form_update_category/'+id); 
 }
 
 function delete_cat(id){
 	var baseUrl = "<?php echo base_url(); ?>";
 	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
 	
 	if (confirm('Are you sure you want to delete vendor category?')) {	
		$.post( baseUrl + "vendor/delete_subcategory", { csrf: token, id: id } );
		$("#container14").load(baseUrl+'vendor/load_categories');		
	}
	
 } 
  
 </script>
 
<button type="button" id="btn_create14" class="btn btn-info btn-primary">Add New</button> 
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Category</th>
		<th>Sub Category</th>		
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
 <?php  //$categories = array();
 if($categories){ $i=1;
 	foreach($categories as $row){ ?>
 		<tr>
 			<td style="text-align: center"><?php echo $i; ?></td>
			<td><?php echo $row->category; ?></td>	
			<td><?php echo $row->subcategory; ?></td>			
			<td style="text-align: center;"><?php
					echo '<i class="fa fa-pencil" onclick="update_cat('.$row->id.')"></i>'; echo '&nbsp;&nbsp;&nbsp;';					
					echo '<i class="fa fa-trash"  onclick="delete_cat('.$row->id.')"></i>';	
			?></td>											
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="3">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>