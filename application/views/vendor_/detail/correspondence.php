<div id="container10">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	
   
   $('#btn_create10').click(function(){  
   		$("#container10").load(baseUrl+'vendor/form_correspondence');   		
   });   	
   
 }); 
 
 function download(id){
 	
 	var baseUrl  = "<?php echo base_url(); ?>";
 	var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';
	
	$.post( baseUrl+ "vendor/download_file", { csrf: token, id: id } );
	
	alert('ok');
 	
 }
 
 function update_corresp(id){
 	
 	$("#container10").load(baseUrl+'vendor/form_update_correspondence/'+id); 
 }
 
 function delete_contact(id){
 	
 	var baseUrl  = "<?php echo base_url(); ?>";
 	var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';
    if (confirm('Are you sure you want to delete this?')) {
	 	$.post( baseUrl+ "vendor/delete_document", { csrf: token, id: id })
		  .done(function( resp ) {
		    	$("#container10").load(baseUrl+'vendor/load_correspondence');
		});
	}	
 }
  
 </script>
 
<button type="button" id="btn_create10" class="btn btn-info btn-primary">Add New</button> 
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Document Name</th>
		<th>Document Number</th>
		<th>Description</th>
		<th>Effective Date</th>
		<th>Expired Date</th>
		<th>Remark</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>

 <?php  
 if($correspondence){ $i=1;
 	foreach($correspondence as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->doc_name; ?></td>	
			<td><?php echo $row->doc_num; ?></td>
			<td><?php echo $row->desc; ?></td>
			<td><?php echo $row->eff_date; ?></td>
			<td><?php echo $row->exp_date; ?></td>
			<td><?php echo $row->remark ?></td>
			<td style="text-align: center;">
			
			<?php  
			
			$icon = '<i class="glyphicon glyphicon-download"></i>';			
			$prop = array('title'=>'Download'); 				 
		  	echo anchor('vendor/download_file/'.$row->id,$icon,$prop);
		  	 
					echo '&nbsp;&nbsp;&nbsp;';	
					echo '<i class="fa fa-pencil" onclick="update_corresp('.$row->id.')"></i>'; echo '&nbsp;&nbsp;&nbsp;';					
					echo '<i class="fa fa-trash"  onclick="delete_contact('.$row->id.')"></i>';	
			?></td>											
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="8">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>