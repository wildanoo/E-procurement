<div id="org">
<div id="org_msg" class="text-center text-danger bg-warning"></div>	
	
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Name</th>
		<th>Address</th>
		<th>Phone</th>
		<th>Position</th>
		<th>Remark</th>
		<th>Approval</th>
		<th>Status</th>
		<th>#</th>
	</tr>
	</thead>
	<tbody>
 <?php  //$org = array();
 if($org){ $i=1;
 	foreach($org as $row){ $stat = ($row->status == '0') ? 'request':'approved'; ?>
 		<tr>
 			<td><?= $i; ?></td>
			<td><?= $row->name; ?></td>	
			<td><?= $row->address; ?></td>
			<td><?= $row->phone; ?></td>
			<td><?= $row->position; ?></td>			
			<td><?= $row->remark ?></td>
			<td><?= $row->approval ?></td>
			<td><span class="label label-warning"><?= $stat ?></span></td>
			<td style="text-align: center;">
				<?php 
					echo '<button onclick="approve_org([\''.$row->id.'\',\''.$row->approval.'\',\''.$row->id_tbl.'\'])" type="button" class="btn btn-default"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>';
					echo '<button onclick="reject_temp_table('.$row->id.',\'temp_organization\')" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>';
				?>
			</td>	
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="9">-</td></tr>
<?php } ?>
     </tbody>
</table>
</div>
