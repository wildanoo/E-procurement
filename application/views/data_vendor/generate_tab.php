<?php
	$ses_tab = $this->session->flashdata('tab');
	$clps = !$ses_tab ? "collapse1" : $ses_tab ;
?>

<script type="text/javascript">
   $(document).ready(function(){
      var clps  = "<?php echo $clps; ?>";
   if(clps!="collapse1"){
    $('#collapse1').collapse('hide');
    $('#'+ clps ).collapse('show');
   }

   });


  function view_logs(id)
  {
    var purl = baseUrl+'data_vendor/view_logs2';
    $.post(purl, {'tb_name': id}, function(data, textStatus, xhr) {
      console.log(data);
      $('#logs_'+id).html(data);
    });

  }

  function hide_logs(id)
  {
    $('#logs_'+id).html('');
  }

  function view_log_temp(id)
  {
    url = baseUrl+'data_vendor/view_log_'+id;
    window.open(url, "", "width="+$(window).width()+",height="+$(window).height()+",scrollbars=yes");
  }


</script>
<?php //print_r($this->permit) ?>
<div class="panel panel-default">
<div class="panel-heading"></div>
  <div class="panel-body">
     <div class="form-group">
      <label for="inputEmail3" class="col-xs-6 control-label">Supplier Name</label>
      <div class="col-xs-6">
        <label for="">: <?=$vendor->name?></label>
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail3" class="col-xs-6 control-label">Registration Code</label>
      <div class="col-xs-6">
        <label for="">: <?=$vendor->reg_num?></label>
      </div>
    </div>
    <hr>
    <?php  $this->load->view('data_vendor/tab/general'); ?>

    <div id="logs_vendor"></div>
  </div>
</div>


<div class="bs-example">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">1. Category</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p><?php  $this->load->view('data_vendor/tab/categories'); ?></p>
                    <button onclick="view_log_temp('cats')" type="button" class="btn btn-warning">view logs</button>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">2. Contact Person</a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('data_vendor/tab/contact'); ?></p>
                    <button onclick="view_log_temp('cp')" type="button" class="btn btn-warning">view logs</button>

                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">3. Bank Account</a>
                </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('data_vendor/tab/bank_account'); ?></p>
                    <div id="logs_bank_account"></div>
                </div>
            </div>
        </div>
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">4. Akta Pendirian</a>
                </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('data_vendor/tab/akta'); ?></p>
                    <div id="logs_akta"></div>
                </div>
            </div>
        </div>
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">5. Owner</a>
                </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('data_vendor/tab/owner'); ?></p>
                    <button onclick="view_log_temp('owner')" type="button" class="btn btn-warning">view logs</button>

                </div>
            </div>
        </div>
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">6. Pengurus</a>
                </h4>
            </div>
            <div id="collapse6" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('data_vendor/tab/org'); ?></p>
                    <button onclick="view_log_temp('org')" type="button" class="btn btn-warning">view logs</button>

                </div>
            </div>
        </div>
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">7. Surat Legalitas</a>
                </h4>
            </div>
            <div id="collapse7" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('data_vendor/tab/correspondence'); ?></p>
                    <button onclick="view_log_temp('corresp')" type="button" class="btn btn-warning">view logs</button>
                    
                </div>
            </div>
        </div>
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">8. Referensi</a>
                </h4>
            </div>
            <div id="collapse8" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('data_vendor/tab/reference'); ?></p>
                    <button onclick="view_log_temp('ref')" type="button" class="btn btn-warning">view logs</button>
                    
                </div>
            </div>
        </div>
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">9. Afiliasi</a>
                </h4>
            </div>
            <div id="collapse9" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('data_vendor/tab/affiliate'); ?></p>
                    <button onclick="view_log_temp('aff')" type="button" class="btn btn-warning">view logs</button>
                    
                </div>
            </div>
        </div>
    </div>
 
</div>


