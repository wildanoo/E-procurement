<script type="text/javascript">
$(document).ready(function(){
   var baseUrl  = "<?php echo base_url(); ?>";	

	  $('#id_office').change(function(){  
	   		var id_office = $('#id_office').val(); 
	   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	           $.ajax({
	               url  : baseUrl + 'employee/get_division',
	               type : "POST",              		               
	               data : csrf +'='+ token +'&id_office='+id_office,
	               success: function(resp){
	               	  $('#id_div').html(resp);
	               }
	            });
	     });

	  $('#id_div').change(function(){  
	   		var id_div = $('#id_div').val(); 
	   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	           $.ajax({
	               url  : baseUrl + 'employee/get_department',
	               type : "POST",              		               
	               data : csrf +'='+ token +'&id_div='+id_div,
	               success: function(resp){
	               	  $('#id_dept').html(resp);
	               }
	            });
	     });
}); 

</script>

<div class="page-header">
  <h3>New Employee</h3>
</div>
<?php echo form_open('employee/create',array('id'=>'form')); ?>
<table cellpadding="2" cellspacing="2">
<tr>
	<td><label>Nomor Pegawai</label></td><td>:</td>
	<td><input type="text" name="nopeg" id="nopeg" placeholder="Nomor Pegawai" class="form-control" required/>		
	</td>	
</tr>
<tr>
	<td><label>Nama Lengkap</label></td><td>:</td>
	<td><input type="text" name="fullname" id="fullname" placeholder="Nama Lengkap" class="form-control" required/>		
	</td>	
</tr>
<tr>
	<td><label>Office</label></td><td>:</td>
	<td><?php $prop_office = 'id="id_office" class="form-control"';
          echo form_dropdown("id_office",$office,'default',$prop_office);?>
	</td>	
</tr>
<tr>
	<td><label>Division</label></td><td>:</td>
	<td><select name="id_div" id="id_div" class="form-control">
		<option value="">--Select Office--</option>		
		</select>
	</td>	
</tr>
<tr>
	<td><label>Department</label></td><td>:</td>
	<td><select name="id_dept" id="id_dept" class="form-control">
		<option value="">--Select Division--</option>		
		</select>
	</td>	
</tr>
<tr>
<tr>
	<td><label>Position</label></td><td>:</td>
	<td><?php $prop_position = 'id="id_position" class="form-control"';
        echo form_dropdown("id_position",$position,'default',$prop_position);?>
	</td>	
</tr>
	</td>	
</tr>
<tr>	
	<td colspan="3" style="text-align: right;">
		<input type="submit" value="Submit" class="btn btn-primary btn-sm"/>
		<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>'Cancel'); ?>					 
		<?php  echo anchor('employee/','Cancel',$prop); ?>
	</td>
</tr>
</table>
<?php echo form_close(); ?>