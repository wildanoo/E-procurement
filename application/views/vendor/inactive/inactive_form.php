<script>
	function cancel(){
		$('#main_container').load(baseUrl+'inactive/browse');
	}

	function submit_ina(){
		var csrf	= '<?php echo $this->security->get_csrf_token_name(); ?>';
		var token 	= '<?php echo $this->security->get_csrf_hash(); ?>';
		var vdata 	= $('#formin').serialize()+'&'+csrf+":"+token;
		var cek 	= confirm('Anda yakin ingin menjadikan inactive?');
		if(cek){
			var v_url = baseUrl+'inactive/ina_approve_act/';
			$.post(v_url , vdata , function(data){
				cancel();
			});
		}
	}

	function reject_ina(){
		var csrf	= '<?php echo $this->security->get_csrf_token_name(); ?>';
		var token 	= '<?php echo $this->security->get_csrf_hash(); ?>';
		var vdata 	= $('#formin').serialize()+'&'+csrf+":"+token;
		var cek 	= confirm('Anda yakin ingin me-reject?');
		if(cek){
			var v_url = baseUrl+'inactive/ina_reject_act/';
			$.post(v_url , vdata , function(data){
				cancel();
			});
		}
	}
</script>
<div id="main_container">
	<div class="page-header">
		<h3>Vendor List</h3>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<strong>Change Vendor Status To Inactive</strong>
		</div>
		<div class="panel-body">
			<?php echo form_open('inactive/inactive_act',array('id'=>'formin', 'class'=>'form-horizontal', 'role'=>'form', 'onsubmit'=>'return false')); ?>
			<input type="hidden" name="id_ina" value="<?= $browse->inaID ?>">
			<input type="hidden" name="id_ven" value="<?= $browse->venID ?>">
			<table class="content-announcement" border="0" width="60%">
				<tbody>
					<tr>
						<td width="40%"><h3><?= $browse->vendor_name ?></h3></td>
						<input type="hidden" name="ven_name" value="<?= $browse->vendor_name ?>">
					</tr>
					<tr>
						<td width="30%"><label>Registration Code</label></td>
						<td width="5%">:</td>
						<td width="65%">
							<input type="text" name="reg_num" readonly value="<?= $browse->register_num ?>" class="form-control" style="border:none;">
						</td>
					</tr>
					<tr>
						<td width="30%"><label>Supplier Nr.</label></td>
						<td width="5%">:</td>
						<td width="65%">
							<input type="text" name="sup_num" readonly value="<?= $browse->vendor_num ?>" class="form-control" style="border:none;">
						</td>
					</tr>
					<tr>
						<td width="30%"><label>Vendor Type</label></td>
						<td width="5%">:</td>
						<td width="65%">
							<input type="text" name="ven_type" readonly value="<?= $browse->vendor_type ?>" class="form-control" style="border:none;">
						</td>
					</tr>
					<tr>
						<td width="30%"><label>Reason</label></td>
						<td width="5%">:</td>
						<td width="65%">
							<textarea name="reason" class="form-control" rows="5" readonly><?php echo $browse->reason ?></textarea>

						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="form-group">
								<div class="col-sm-12">
									<div class="pull-right">
										<a type="submit" href="javascript:void(0)" onclick="submit_ina()" class="btn btn-primary">Approve</a>
										<a href="javascript:void(0)" onclick="reject_ina()" class="btn btn-danger">Reject</a>
										<a type="button" href="javascript:void(0)" onclick="cancel()" class="btn btn-warning">Cancel</a>
									</div>
									
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<?php echo form_close(); ?>
		</div>
		
	</div>
</div>