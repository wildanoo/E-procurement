<style type="text/css" media="screen">
	.left-0{
		padding-left: 0px !important;
	}
	.ck-pad{
		padding: 8px 10px;
	}
	.point{
		cursor: pointer;
	}
</style>
<script>
	$(function(){
		$('.checkall').click(function(){
			var dat = $(this).attr('data');
			if($(this).is(":checked")){
				$('.ck2-'+dat).prop('checked',true);
			}else{
				$('.ck2-'+dat).prop('checked',false);
			}
		});
		$('.checkall2').click(function(){
			var dat = $(this).attr('data');
			if($(this).is(":checked")){
				$('.ck3-'+dat).prop('checked',true);
			}else{
				$('.ck3-'+dat).prop('checked',false);
			}
		});

	});
</script>
<div class="row">
	<div class="col-md-12">
		<div class="widget widget-nopad">
			<div class="col-md-12">
				<?php echo form_open('menu/updatemenu','id="menuform"'); ?>
				<input type="hidden" name="id" value="<?php echo $browse->id ;?>">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title" style="padding: 5px 0 16px 0;">Top Menu</h3>
						<div class="box-footer"></div>
						<div class="col-md-12" style="padding: 12px 0 12px 0;">
							<div class="col-md-2">Group Name </div>
							<div class="col-md-1 text-center">:</div>
							<div class="col-md-9"><b><?php echo $browse->name; ?></b></div>
						</div>              	
						<div class="col-md-12" style="padding: 12px 0 12px 0;">
							<div class="col-md-2">Permissions </div>
							<div class="col-md-1 text-center">:</div>
							<div class="col-md-9">
								<div class="box-body">
									<div class="accordion" id="accordion">
										<?php 	
										$i = 1;
										foreach($row1 as $r1){
											$group = explode(',',$r1->groups);
											if(in_array($browse->id,$group)){ $checked = 'checked';}else{$checked = '';}
											?>
											<div class="accordion-group ">
												<div class="accordion-heading clearfix">
													<div class="col-sm-1 form-group ck-pad">
														<?php if($r1->url != "#"){ ?><input type="checkbox"  name="ck_list1[]" value="<?php echo $r1->featureID ?>" <?php echo $checked ?> ><?php }else{ echo '<i class="fa fa-chevron-right"></i>';} ?>
													</div>
													<div class="col-md-10 left-0">
														<a class="accordion-toggle target left-0 topcol" data-toggle="collapse" data-parent="#accordion" href=".collapse<?php echo $i; ?>" >
															<?php echo $r1->menu1; ?>
														</a>
													</div>
												</div>
												<?php if($this->m_menutop->checkshow($r1->menu1ID,'menu2') == 'false'){ ?>
												<div class="accordion-body collapse collapse<?php echo $i ?>">
													<div class="accordion-inner" style="padding: 0;">
														<b>
															<div class="box-body" style="padding: 0;">
																<table id="example2" class="table table-bordered2 table-hover text-center">
																	<tbody>
																		<tr>
																			<td width="100px" style="border-top: none;text-align: center;"><?php echo form_checkbox('ck_list2[]',"accept",false,'class="checkall" data="'.$i.'"');?></td>
																			<td class="accordion-toggle target left-0 point" data-toggle="collapse" data-parent="#accordion" style="border-top: none;text-align: left;">
																				Check All
																			</td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</b>
													</div>
												</div>
												<?php };
												$n = 1;
												foreach($row2 as $r2){
													$group2 = explode(',',$r2->groups);
													if($r1->menu1ID == $r2->id_up){
														if(in_array($browse->id,$group2)){$checked2 = 'true';$val2 = $r2->menu2ID;}else{$checked2 = '';}
														?>
														<div class="accordion-body collapse collapse<?php echo $i ?>">
															<div class="accordion-inner" style="padding: 0;">
																<b>
																	<div class="box-body" style="padding: 0;">
																		<table id="example2" class="table table-bordered2 table-hover text-center">
																			<tbody>
																				<tr>
																					<td width="100px" style="border-top: none;text-align: center;"><?php if($r2->url != "#"){ echo form_checkbox('ck_list2[]',$r2->featureID, $checked2, 'class=ck2-'.$i);}else{ echo '<i class="fa fa-chevron-right"></i>';}?></td>
																					<td class="accordion-toggle target left-0 point" data-toggle="collapse" data-parent="#accordion" href=".subcollapse<?php echo $n; ?>" style="border-top: none;text-align: left;">
																						<?php  echo $submenu = $r2->menu2 ;?>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</b>
															</div>
															<?php if($this->m_menutop->checkshow($r2->menu2ID,'menu3') == 'false'){ ?>
															<div class="accordion-body collapse subcollapse<?php echo $n; ?>">
																<div class="accordion-inner" style="padding: 0;">
																	<b>
																		<div class="box-body" style="padding: 0;">
																			<table id="example2" class="table table-bordered2 table-hover text-center">
																				<tbody>
																					<tr>
																						<td width="150px" style="border-top: none;text-align: center;"><?php echo form_checkbox('ck_list2[]',"accept",false,'class="checkall2" data="'.$n.'"');?></td>
																						<td class="accordion-toggle target left-0 point" data-toggle="collapse" data-parent="#accordion" style="border-top: none;text-align: left;">
																							Check All
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</div>
																	</b>
																</div>
															</div>
															<?php } ;
															foreach($row3 as $r3){
																$group3 = explode(',',$r3->groups);
																if($r2->menu2ID == $r3->id_up){ 
																	if(in_array($browse->id,$group3)){$checked3 = 'true';$val3 = $r3->menu3ID;}else{$checked3 = '';}
																	?>
																	<div class="accordion-body collapse subcollapse<?php echo $n; ?>">
																		<div class="accordion-inner" style="padding: 0;">
																			<b>
																				<div class="box-body" style="padding: 0;">
																					<table id="example3" class="table table-bordered2 table-hover text-center">
																						<tbody>
																							<tr>
																								<td width="150px" style="border-top: none;text-align: center;"><?php echo form_checkbox('ck_list3[]',$r3->featureID,$checked3,'class=ck3-'.$n); ?></td>
																								<td class="accordion-toggle target left-0 point" data-toggle="collapse" data-parent="#accordion"  style="border-top: none;text-align: left;">
																									<?php echo $submenu2 = $r3->menu3 ?>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																				</div>
																			</b>
																		</div>
																	</div>
																	<?php } } ?>
																</div>
																<?php } $n++;}?>
															</div>
															<?php $i++ ;} ?>
														</div>
														
													</div>
												</div>
											</div>
										</div><!-- /.box-header -->
										<div class="box-footer" style="padding-left: 30px;">
											<div class="col-md-3">
												<a href="<?php echo base_url(); ?>user/groups" class="btn btn-sm btn-default" title="Cancel" >Cancel</a>	
												<input name="submit" value="Update" class="btn btn-sm btn-primary" type="submit">
											</div>
										</div>
									</div>
									<?php echo form_close(); ?>
								</div>
							</div>
						</div>
					</div>
