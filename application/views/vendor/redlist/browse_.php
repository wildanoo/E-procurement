<script>
	$(function() {
		$('#start_date, #end_date').datepicker({
	          changeMonth: true, changeYear: true,
	   		  dateFormat: "yy-mm-dd"
	     });

	    $('#ck_all').on('click', function(){
			$(this).closest('table').find(':checkbox').prop('checked', this.checked);
		});

		$('#subcat').change(function(event) {
			$('#form-search').submit();
		});

	});

	function load_data()
	{
		$('#main_container').load(baseUrl+'redlist/browse');
	}

	function form_create()
	{
		$("#main_container").load(baseUrl+'redlist/form_create');
	}

	function form_update(id)
	{
		$("#main_container").load(baseUrl+'redlist/form_create/'+id);
	}

	function form_approve(id)
	{
		$("#main_container").load(baseUrl+'redlist/form_approve/'+id);
	}

	function form_reject(id)
	{
		$("#main_container").load(baseUrl+'redlist/form_reject/'+id);
	}

	function view_log(red_id,ven_id)
	{
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>';
	    var token     = '<?php echo $this->security->get_csrf_hash(); ?>';

			var p_url = baseUrl+'redlist/browse_logs/';
			$.post(p_url, { csrf: token , red_id: red_id , ven_id: ven_id }, function(data, textStatus, xhr) {
					$("#main_container").html(data);
			});
	}

	function cancel_redlist(red_id,ven_id)
	{
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>';
	    var token     = '<?php echo $this->security->get_csrf_hash(); ?>';

		var cek = confirm('Anda akan membatalkan proses redlist vendor ? ');
		if(cek)
		{
			var p_url = baseUrl+'redlist/cancel_redlist/';
			$.post(p_url, { csrf: token , red_id: red_id , ven_id: ven_id }, function(data, textStatus, xhr) {
				load_data();
			});
		}
	}


</script>

<div id="main_container">

<div id="debug"></div>
<div class="page-header"><h3>Vendor <?php echo $title; ?></h3></div>
<!-- Search Term -->
<div class="table">
	<div class="panel panel-default">
	    <div class="panel-heading">
	        <i class="fa fa-search"></i> <strong>Search Section</strong>
	    </div>
	    <style type="text/css">
	    .list-inline li{
	    	vertical-align: middle !important;
	    }
	    .btn-srch{
	    	margin-bottom: 0;
	    }
	    </style>
	    <?php echo form_open('redlist/search',array('id'=>'form-search'));	//$current_date = date('Y/m/d')?>
	    <div class="panel-body">
	    	<ul class="list-inline">
	    		<li style="width:24%">
	    			<div class="input-group">
	    			<input type="date" value="" placeholder="date start" class="form-control date_picker" id="start_date" name="start_date">
	    			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	    			</div>
	    		</li>
	    		<li style="width:2%">to</li>
	    		<li style="width:24%">
	    			<div class="input-group">
	    			<input type="date" value="" placeholder="date end" class="form-control date_picker" id="end_date" name="end_date">
	    			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	    			</div>
	    		</li>
	    		<li style="width:34%"><div class="input-group" style="width:100%"><input placeholder="search by code or title..." type="text" value="" class="form-control" id="search_term" name="search_term"></div></li>
	    		<li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search">Search</button></div></li>
	    		<li style="width:7%"><div class="input-group"><a href="<?php echo base_url()?>redlist" class="btn btn-srch btn-warning btn-sm">Show All</a></div></li>
	    	</ul>
	    </div>
	    <div class="panel-footer">
				<select name="subcat" id="subcat" class="form-control" style="max-width:250px;">
					<option value="all">All Categories</option>
					<?php
			    $idcategory = '';
			    foreach ($category as $cate)
			    {
			    	if($idcategory != $cate->idcat)
			    		echo '<optgroup label="'.$cate->category.'">';
			    	if($id_subcat==($cate->idsub)) {$selected='selected';} else {$selected='';}
			    		echo '<option value="'.$cate->idsub.'" '.$selected.'>'.$cate->subcategory.'</option>';
			    		$idcategory = $cate->idcat;
			    }
			    ?>
				</select>
	    <?php echo form_close()?>
	    </div>
	</div>
<hr/>

</div>

<!-- Search Term -->

<?php echo form_open('redlist/export_excel',array('id'=>'myform')); ?>
<?php if($this->permit->avr) {?>
<a class="btn btn-primary" href="javascript:void(0)" onclick="form_create()" title="New Redlist">Create</a>
<?php } ?>
<button class="btn btn-primary pull-right" type="submit"  title="Export Redlist">Generate Excel</button>

<?php echo $this->session->flashdata("message"); ?>
<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
        <tr>
        	<th><input type="checkbox" id="ck_all" value="1"></th>
            <th>No</th>
            <th>Registration Num.</th>
            <th>Vendor Num.</th>
            <th>Vendor Name</th>
            <th>Start Date</th>
            <th>Counter</th>
            <th>Reason</th>
            <th>Redlist Status</th>
            <th>#</th>
        </tr>
	</thead>
    <tbody>
<?php
// echo form_hidden('total',count($browse));
if($browse){ $i=1;
	foreach($browse as $row){
		if($row->red_state == 'wait')
		{
			$label = 'warning';
		}
		elseif($row->red_state == 'approved')
		{
			$label = 'danger';
		}
		elseif($row->red_state == 'rejected')
		{
			$label = 'primary';
		}
		else
		{
			$label = 'default';
		}
		?>
			<tr>
        		<td> <input type="checkbox" class="ck_ex" name="my_ck[]" id="ck<?=$i?>" value="<?=$row->id_redlist?>"></td>
				<td style="text-align: center;"><?php  echo $num = $num+1; ?></td>
				<td><?php echo $row->register_num ?></td>
				<td><?php if($row->vendor_num) echo $row->vendor_num; else echo "-"; ?></td>
				<td><?php echo $row->vendor_name ?></td>
				<td><?php echo $row->start_date ?></td>
				<td><?php echo $row->counter ?></td>
				<td><?php echo $row->remark ?></td>
				<td> <label for="" class="label label-<?=$label?>"> <?php echo ucwords($row->red_state); ?> </label></td>

				<td><div class="btn-group">
        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
          Action <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
        <?php
        	echo "<li><a href=".base_url('vendorlog/search/'.$row->id.'/redlist')." onclick='view_log($row->id_redlist,$row->id)'>View Log</a></li>";
        	if($this->permit->evr)
        	{
        		echo "<li><a href='javascript:void(0)' onclick='form_update($row->id_redlist)'>Edit Redlist</a></li>";
        	}
        	if($this->permit->cvr && $row->red_state=='wait')
        	{
        		echo "<li><a href='javascript:void(0)' onclick='cancel_redlist($row->id_redlist,$row->id)'>Cancel Redlist</a></li>";
        	}
        	if($this->permit->vra && $row->red_state!='cancel')
        	{
        		if($row->red_state=='wait')
        		{

        			echo "<li><a href='javascript:void(0)' onclick='form_approve($row->id_redlist)'>Approve Redlist</a></li>";

        			echo "<li><a href='javascript:void(0)' onclick='form_reject($row->id_redlist)'>Reject Redlist</a></li>";
        		}	

        	}
        ?>
        </ul>
      </div></td>
			</tr>
<?php	$i++;  }  ?>
<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php  } else { ?>
	<tr><td colspan="9" align="center">-</td></tr>
<?php } ?>
    </tbody>
</table>

<?php echo form_close();?>

</div>
