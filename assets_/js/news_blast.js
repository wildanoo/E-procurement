$(document).ready(function(){

  var csrf_token = $('meta[name="csrf-token"]').attr('content');
  var csrf_hash  = $('meta[name="csrf-hash"]').attr('content');
  $.ajaxPrefilter(function(options, originalOptions, jqXHR){
    if (options.type.toLowerCase() === "post") {
        options.data = options.data || "";
        options.data += options.data?"&":"";
        options.data += csrf_token+"=" + csrf_hash;
    }
  });

  $(function () {
    if ($('meta[name="form-decider"]').attr('content') == 'form-create') {
      $("#wyswyg").hide(); $("#upload4").hide();$(".news_detail_seg").hide();
      $("#id_company").change();
    }else{
      $("#upload1").hide();
    }
  });
    
  $('#vendor_multiselect').multiSelect({
    selectableOptgroup: true,
    selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='search selectable vendor...'>",
    selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='search selected vendor...'>",
    afterInit: function(ms){
      var that = this,
      $selectableSearch = that.$selectableUl.prev(),
      $selectionSearch = that.$selectionUl.prev(),
      selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
      selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

      that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
      .on('keydown', function(e){
        if (e.which === 40){
          that.$selectableUl.focus();
          return false;
        }
      });

      that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
      .on('keydown', function(e){
        if (e.which == 40){
          that.$selectionUl.focus();
          return false;
        }
      });
    },
    afterSelect: function(){
      this.qs1.cache();
      this.qs2.cache();
    },
    afterDeselect: function(){
      this.qs1.cache();
      this.qs2.cache();
    }
  });

  $(".detail-section").keyup(function(){
    var len    = $(this).val().length;
    var count_el = $(this).parent().find("#count-char");
    count_el.html(250-len);
  });

  $('#id_area').change(function(){  
    var id_area = $('#id_area').val();
    $.ajax({
       url  : 'get_office',
       type : "POST",                                
       data : 'id_area='+id_area,
       success: function(resp){
          if ($('#id_area').val() != '') {
            $('#id_office').removeAttr('disabled');
            $('#id_office').html(resp);
          }else{
            $('#id_office').attr('disabled','disabled');
          }
       }
    });
  });

  var logic = function( currentDateTime ){
    var fullDate      = new Date();
    var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
    var currentDate   = fullDate.getFullYear() + "/" + twoDigitMonth + "/" + fullDate.getDate();
    var valYear  = parseInt(currentDate.split('/')[0]);
    var valMonth = parseInt(currentDate.split('/')[1]);
    var valDay   = parseInt(currentDate.split('/')[2]);
    if( currentDateTime.getDate() == valDay && currentDateTime.getMonth()+1 == valMonth && currentDateTime.getFullYear() == valYear){
      this.setOptions({
        minTime:0
      });
    }else{
      this.setOptions({
        minTime:'00:00'
      });
    }
  };

  $('#publish_date').datetimepicker({
    format:'Y/m/d H:i',
    step:1,
    minDate: 0,
    minTime: 0,
    showOn: "button",
    validateOnBlur: true,
    onChangeDateTime:logic
  });

  $("input:radio[name=content]").click(function () { 
    var opt = $("input:radio[name=content]:checked").val();
    if (opt == '2'){
      $("#upload1").show();  $("#wyswyg").hide(); 
    } else {
      $("#wyswyg").show(); $("#upload1").hide();  }
  });

}); 