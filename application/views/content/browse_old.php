<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){	 
	 $(function(){  });
		var baseUrl = "<?php echo base_url(); ?>";

		$("#srch-term").autocomplete({
			source: baseUrl + 'content/get_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
	        var search = $("#srch-term").val(); // alert(search);
	        var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
	        $.ajax({
	           url  : baseUrl + 'content/set_sess_search',
	           type : "POST",              		               
	           data : csrf +'='+ token +'&search='+search,
	           success: function(resp){    $("#debug").html(resp);            		            
				    document.location.href= baseUrl + 'content/';
	           }
	        });
	    });
	    	 
	 $('#btn_create').click(function(){ 
	 	  form_create();
	 	  //alert('test');
	 });	
});

	function form_create(){
		var baseUrl = "<?php echo base_url(); ?>";		 
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		BootstrapDialog.show({
		title: 'Create Content',
        message: $('<div></div>').load(baseUrl + "content/form_create/"),
        buttons: [{ label: 'Submit',
	                cssClass: 'btn btn-primary btn-sm',		               
	                action: function(dialogRef){
	                	 var type_name  = $("#type_name").val(); 
	                	 
	                	 $.post( baseUrl + 'content/is_exist', { csrf: token , type_name : type_name})
						 .done(function( resp ) {
								 //$('#debug').html(resp);
								 //dialogRef.close(); 
								var json = $.parseJSON(resp);
								if (json.status=='true'){
				                	var values = $("#form").serializeArray();
				                	values.push({ name: csrf,value: token });
									values = jQuery.param(values);
									 $.ajax({
							               url  : baseUrl + 'content/create',
							               type : "POST",              		               
							               data : values,
							               success: function(resp){
							               		document.location.href= baseUrl + 'content/';	               				               		  
									       }
							            });   
								} else { 	
								 $("#msg").html(json.msg); 
							}
	               		 });  
	                }
	            }, {
	            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
			                action: function(dialogRef) {
			                	document.location.href= baseUrl + 'content/';
			                    dialogRef.close(); 
			                }
		        }]
    });
}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Content',
            message: $('<div></div>').load(baseUrl + "content/form_update/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef)
		                {
		                	 var type_name  = $("#type_name").val(); 
		                	 
		                	 $.post( baseUrl + 'content/is_exist', { csrf: token , type_name : type_name})
							 .done(function( resp ) {
//									 $('#debug').html(resp);
//									 dialogRef.close(); 
									var json = $.parseJSON(resp);
									if (json.status=='true'){
					                	var values = $("#form").serializeArray();
					                	values.push({ name: csrf,value: token });
										values = jQuery.param(values);
										 $.ajax({
								               url  : baseUrl + 'content/update',
								               type : "POST",              		               
								               data : values,
								               success: function(resp){
								               		document.location.href= baseUrl + 'content/';	               				               		  
										       }
								            });   
									} else { 	
									 $("#msg").html(json.msg); 
								}
		               		 });  
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'content/';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>
</head>

<body>
 <div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Content</h3>
              </div><!-- /.box-header -->

                <br>
                
                <div class="col-md-2 pull-left">
                    <input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Content" value="New Content" style="width:130px;" title="Create New Content"/>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
				<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
                </div>

                <div class = "col-md-10 pull-right">
                  <div class="input-group pull-right" style="width: 200px;">
                      <input type="text" class="form-control input-sm pull-right" name="srch-term" id="srch-term" 
                      	placeholder="Search" style="height: 28px;">
                      <div class="input-group-btn">
                        <button id="btn_search" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                </div>   
     
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover text-center">
                    <thead>
                      <tr>
                        <!--th><input type="checkbox" name="cba[]" id="cba" /></th-->
                        <th>No</th>
                        <th>Content</th>
                        <!-- th>Is Active</th> -->
                        <th>Created Date</th>
                        <th>Updated Date</th>
                        <th>Updated Date</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    
                    <tbody>
<?php
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td><?php echo $num = $num+1;; ?></td>
				<td><?php echo $row->type_name; ?></td>
				<!-- td><?php   //if($row->status==1) echo "Yes";  else echo "No"; ?></td> -->	
				<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
				<td><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>
				<td><?php echo $row->created_id;?></td>
				<td><?php				
					echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete content ?')){ return true; } else {return false; }";
					echo anchor('content/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Content','onclick'=>$js));?>
				</td>
				
			</tr>
<?php	$i++;  } ?>	
<tr><td colspan="5"><?php echo $pagination;?></td></tr>
<?php } else { ?>
	<tr><td colspan="5" align="center">-</td></tr>
<?php } ?>
                      
                    </tbody>

                  </table>
                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->