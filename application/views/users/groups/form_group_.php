 
 
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">

              <div class="box-header with-border">
                <h3 class="box-title">Create Groups</h3>
              </div>

              <!--form class = "form-horizontal"-->
              <?php echo form_open("user/create_group",array('class'=>'form-horizontal')); ?>
              <table width="50%">
              	<tr><td>Group Name </td><td>:</td>
              	<td><input type="text" name="name" id="name" class="form-control" placeholder="Group Name"/></td></tr>              	
              	<tr><td>Permissions </td><td>:</td>
              	<td><?php 
              			 echo form_hidden("ttl",count($perm));
              			 $i = 1;
                         foreach($perm as $row){
						 	$value = $row->name.":".$row->id;
						 	echo form_checkbox('check'.$i,$value, false); echo $row->name."<br/>";
						$i++; }  ?>   
              	</td></tr>
              </table>

                  </div><!-- col-md-6 -->                
                </div><!-- /.box-body -->
                <div class="box-footer" style="padding-left: 30px;">
                    <?php  $prop = array('class'=>'btn btn-sm btn-default','title'=>'Cancel'); ?>					 
					<?php  echo anchor('user/groups','Cancel',$prop); ?>
                    <?php  echo form_submit('submit', 'Submit','class="btn btn-sm btn-primary"'); ?>
                </div><!-- /.box-footer -->
              <?php echo form_close(); ?>
            </div><!-- /.box -->
          </div>
        </div>
      