
Dengan hormat,<br/>
<b>Bapak/Ibu <?php echo $to; ?></b><br/>

Bersama ini kami sampaikan teguran terkait kelalaian vendor karena telah melakuakan pelanggaran dan dikenakan sanksi berupa tidak diperbolehkan mengikuti 2 kali proses pengadaan pada kategori dimana rekanan terdaftar
<br/>
Berikut ini adalah informasi detail vendor.
<br/>
Company Name :<?php echo $vendor_name; ?>	<br/>
No. Registrastion:<?php echo $reg_num ?> <br/>
<table cellpadding="2" cellspacing="2" width="50%">
	<tr>	
		<th style="text-align: left;">Category</th>	
		<th style="text-align: left;">Sub Category</th>	
	</tr>
	
	<?php if($category){ $i=1;
 		  foreach($category as $row){ ?>
 	<tr>		
		<td><?php echo $row->category; ?></td>	
		<td><?php echo $row->subcategory; ?></td>		
	</tr>
	<?php	$i++;  } ?>	
	<?php } else { ?>
		<tr><td colspan="8">-</td></tr>
	<?php } ?>	
</table>
<br/><br/>
Alasan:<?php echo $reason ?> <br/>

Untuk proses persetujuan dan detail informasi perusahaan dapat diakses :<br/>
<?php echo $link; ?><br/>


Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/><br/>

(&nbsp;<b><?php echo ucfirst( $this->tank_auth->get_username()); ?></b>&nbsp;)

