<?php 

require_once APPPATH.'./libraries/PHPExcel.php';
$objPHPExcel = new PHPExcel();
// Set properties
$objPHPExcel->getProperties()->setCreator("Garuda Indonesia");
$objPHPExcel->getProperties()->setLastModifiedBy("Garuda Indonesia");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Vendor List Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Vendor List Document");
$objPHPExcel->getProperties()->setDescription("Vendor List document for Office 2007 XLSX, generated using PHP classes.");

// Set fonts
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFont()->setBold(true);

// ADD Data
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Promote Vendor To AVL');
$objPHPExcel->getActiveSheet()->SetCellValue('A4', 'No');
$objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Registration Num.');
$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Vendor Num.');
$objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Vendor Name');
$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Category - Subcategory');
$objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Type');
$objPHPExcel->getActiveSheet()->SetCellValue('G4', 'Vendor Status');
$objPHPExcel->getActiveSheet()->SetCellValue('H4', 'Promote Status');

$no = 1;
$number = 5;
$detail_category = '';
foreach($vendorlist as $row){
	foreach($detail as $key => $value){
		if($row->venID == $key){
			$detail_category = $value;
		}
	}
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$number, $no);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$number, $row->register_num);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$number, $row->vendor_num);
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$number, $row->vendor_name);
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$number, $detail_category);
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$number, ucfirst($row->vendor_type));
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$number, ucfirst($row->status));
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$number, $this->m_vendorpromote->prom_stat($row->stat_prom));

	$number++;
	$no++;

}

$objPHPExcel->setActiveSheetIndex(0);
$filename='Vendor_list_'.date('YmdHis').'.xlsx';

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//send to browser
$objWriter->save('php://output');
//save to local folder
// $objWriter->save(str_replace('excel.php', $filename, __FILE__));

exit;

?>