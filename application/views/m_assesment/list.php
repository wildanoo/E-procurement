<div class="row">
	<div class="col-md-12">
		<div class="widget widget-nopad">
			<div class="col-md-12">
				<div class="box box-info">

					<div class="box-header with-border">
						<h3 class="box-title">Manage Assesment</h3>
					</div><!-- /.box-header -->
					<br>
					<?php if($this->session->flashdata('message')) { ?>
					<div class="label label-success">
						<?php echo $this->session->flashdata('message'); ?>
					</div>
					<?php } ?>
					<div class="col-md-2 pull-left">
						<?php if($this->permit->addar) { ?>
						<button type="button" onclick="form_create()" id="btn_create" class="btn btn-primary btn-sm" style="width:130px;" title="Create New Assesment">Create New</button>
						<?php } ?>
					</div>

					<div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
						<div class="input-group pull-right" style="width: 150px;">
							<div class="input-group-btn">
								<?php echo form_open_multipart('m_assesment/search',array('id'=>'form-search','novalidate' => 'true'));?>
								<input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="term" name="term" style="height: 28px;">
								<button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
								<a href="<?php echo base_url()?>m_assesment" class="btn btn-srch btn-warning btn-sm">Show All</a>
							</div>
						</div>
					</div>

					<div class="box-body">
						<table id="example2" class="table table-bordered table-hover sortable">
							<thead>
								<tr>
									<th>No</th>
									<th>Subtegory</th>
									<th>Assesment</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								
								if($browse){ $i=1;
									foreach($browse as $key => $value){ ?>
									<tr>
										<td><?php echo $num = $num+1; ?></td>
										<td><?php echo $value->subcategory; ?></td>
										<td><?php echo $value->description; ?></td>
										<td><?php echo $value->status == '1' ? 'Aktif':'Tidak Aktif'; ?></td>
										<td>
											<?php if($this->permit->deletear) { ?>
											<a title="Delete Data" href="javascript:void(0)" onclick="inactivate_data(<?=$value->dd_id; ?>)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
											<?php } ?>
										</td>
									</tr>
									<?php	$i++;  } ?>
								</table>
								<div align="center"><?php echo $pagination;?></div>
								<?php } else { ?>
								<tr><td colspan="4">-</td></tr>
								<?php } ?>
							</tbody>
						</table>
					</div><!-- /.box-body -->

				</div><!-- /.box-info-->
			</div><!-- /col-md-12 -->
		</div><!-- /.widget -->
	</div><!-- /.col-md-12-->
</div><!-- /row -->
