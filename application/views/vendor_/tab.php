<div id="info_status">
<?php $this->load->view('vendor/register_status2'); ?>
</div>
<?php 
	$ses_tab = $this->session->userdata('tab'); 
	$tab = !$ses_tab ? "home" : $ses_tab ;
?>

<script type="text/javascript">
   $(document).ready(function(){

		activaTab('<?php echo $tab; ?>');
   });
   
   function activaTab(tab){
    	$('.nav-tabs a[href="#' + tab + '"]').tab('show');
   };

</script>

<h1><?php  echo $vendor->name;  ?></h1>

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">General information</a></li>
  <li><a data-toggle="tab" href="#menu0">Due Diligence</a></li>
  <li><a data-toggle="tab" href="#menu1">Bank Account</a></li>
  <li><a data-toggle="tab" href="#menu2">Akta Pendirian</a></li>
  <li><a data-toggle="tab" href="#menu3">Owner</a></li>  
  <li><a data-toggle="tab" href="#menu4">Pengurus</a></li>
  <li><a data-toggle="tab" href="#menu5">Surat legalitas</a></li>
  <li><a data-toggle="tab" href="#menu6">Reference</a></li>
  <li><a data-toggle="tab" href="#menu7">Affiliates</a></li>
  <li><a data-toggle="tab" href="#menu8">Categories</a></li>
</ul>

<?php $this->session->unset_userdata('tab');  ?>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <h3>General Information</h3>
    <p><?php  $this->load->view('vendor/detail/general'); ?></p>
  </div>
  <div id="menu0" class="tab-pane fade">
    <h3>Due Diligence</h3>
    <p><?php  //$this->load->view('vendor/detail/bank_account'); ?></p>
  </div>
  <div id="menu1" class="tab-pane fade">
    <h3>Bank Account</h3>
    <p><?php  $this->load->view('vendor/detail/bank_account'); ?></p>
  </div>
  <div id="menu2" class="tab-pane fade">
    <h3>Akta Pendirian</h3>
    <p><?php  $this->load->view('vendor/detail/akta'); ?></p>
  </div>
  <div id="menu3" class="tab-pane fade">
    <h3>Owner</h3>
    <p><?php  $this->load->view('vendor/detail/owner2'); ?></p>
  </div>
  <div id="menu4" class="tab-pane fade">
    <h3>Pengurus</h3>
    <p><?php  $this->load->view('vendor/detail/org'); ?></p>
  </div>
  <div id="menu5" class="tab-pane fade">
    <h3>Correspondences</h3>
    <p><?php  $this->load->view('vendor/detail/correspondence'); ?></p>
  </div>
  <div id="menu6" class="tab-pane fade">
    <h3>Reference</h3>
     <p><?php  $this->load->view('vendor/detail/reference'); ?></p>
  </div>
  <div id="menu7" class="tab-pane fade">
    <h3>Affiliate</h3>
    <p><?php  $this->load->view('vendor/detail/affiliate'); ?></p>
  </div>
  <div id="menu8" class="tab-pane fade">
    <h3>Categories</h3>
    <p><?php  $this->load->view('vendor/detail/categories'); ?></p>
  </div>
</div>