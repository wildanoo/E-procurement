<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Policy_agreement extends Controller {

	function __construct()
	{
		parent::__construct();		
		$this->load->helper('file');
		$this->load->model('m_policy_agreement');	
		$this->load->model('m_announce');				
	}

 function index(){
 	if ($this->m_policy_agreement->authenticate()) {
 			
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$sess_cat   = $this->session->userdata('sess_cat');			
		$sess_field   = $this->session->userdata('sess_field');
		
			
		$field      = $sess_field != '' ? $sess_field : "type";
		$id_cat     = $sess_cat != '' ? $sess_cat  : "";			
		
		if($sess_cat != ''){	$where = array('type'=>$sess_cat);
		} else { $where = "";	}

 		$table 	    = "policy_agreement";		
		$page       = $this->uri->segment(3);
		$per_page   = 5;		
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table,$where);
		$set_config = array('base_url'=> base_url().'policy_agreement/index','total_rows'=>$total_rows,'per_page'=>5,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);
			 
		$this->load->library('pagination');			 
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();			
			
		$order 			     = array('field'=>'type','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset; 
		$select 			 = "id,status,type,created_date,title,last_updated,(SELECT username FROM users WHERE id=created_id)as created_id,activated";
		
		$sessID  = $this->session->flashdata('anID');
		$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
		
		if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
		else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
		$data['browse'] = $browse;
		//print_r($browse);
 		
 		$data['status']   = array('1'=>'Create','2'=>'Review','3'=>'Approve','4'=>'Publish','5'=>'Edit');
 		//$data['category'] = array('0' => 'All','1'=>'General','2'=>'Open Sourcing','3'=>'Open Bidding','4'=>'Direct Selection');
 		$data['category'] = array('' => 'All','1'=>'General','2'=>'Open Sourcing','3'=>'Open Bidding','4'=>'Direct Selection');
		$title = $this->crud->browse("","policy_agreement","","","true","id,title");
		if(!$title) $title = array();
		$select = array(''=>'All');
		$options = array();
		foreach($title as $val){ $options[$val->title] = $val->title; }
		$data['title'] = $select + $options;
 		
 		$data['view']	  = "policy_agreement/browse"; 		
 		$this->load->view('layout/template',$data);
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	} 	
 } 
 
 function set_sess_category(){
 
 	$type = $_POST['type'];
 	if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
 	} else { $this->session->unset_userdata('sess_cat'); }
 
 	$type = $_POST['field'];
 	if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
 	} else { $this->session->unset_userdata('sess_field'); }
 
 }
 
 function activated(){
 
 	$userID   = $this->uri->segment(3);
 	$users    = $this->crud->browse("","policy_agreement","id",$userID,"false","activated");
 	$status   = $users->activated==1 ? 0 : 1;
 	$update   = array('activated'=>$status,'last_updated'=>date('Y-m-d H:i:s'));
 
 	$this->crud->update("","policy_agreement","id",$userID,$update);
 	$this->session->set_flashdata('message','successfully updates');
 	redirect('policy_agreement/','refresh');
 }
 
 function form_create(){ 	
 	$data['user_id'] = $this->tank_auth->get_user_id();
	$data['username']= $this->tank_auth->get_username();
	$select = "id";
 	
 	//$data['category'] = array('0' => 'All','1'=>'General','2'=>'Open Sourcing','3'=>'Open Bidding','4'=>'Direct Selection');
 	$category = array('1'=>'General','2'=>'Open Sourcing','3'=>'Open Bidding','4'=>'Direct Selection');
 	$browse = $this->crud->browse("","policy_agreement","id","false");
 	$data['category'] = $category;
 	$data['browse'] = $browse;
	$data['cari'] = $this->crud->browse("","policy_agreement_temp");
 	$data['content']= ""; 
 	$data['view']	= "policy_agreement/form_create"; 		
 	$this->load->view('layout/template',$data);
 		
 } 
 
 function form_attach(){
 	$data['user_id'] = $this->tank_auth->get_user_id();
 	$data['username']= $this->tank_auth->get_username();
 	$data['view'] = "policy_agreement/form_attach";
 	$this->load->view('layout/template',$data);
 }

 function form_update(){ 	
 	$id  = $this->uri->segment(3);
 	$def =  $this->crud->browse("","policy_agreement","id",$id,"false");
 	$data['def'] = $def;
 	
 	$data['user_id'] = $this->tank_auth->get_user_id();
	$data['username']= $this->tank_auth->get_username();
	
	$select   = "id";
	
	if($def->file){

		$ctpath_id  = "./uploads/policy_agreement/".$def->file."_ind.txt";		
		$ctpath_eng  = "./uploads/policy_agreement/".$def->file."_eng.txt";		
		
 		$contents[0] = file_exists($ctpath_id) ? file_get_contents($ctpath_id) : "-";
 		$contents[1] = file_exists($ctpath_eng) ? file_get_contents($ctpath_eng) : "-";
 	}

 	$category = $this->crud->browse("","category","","","true","id,category"); 
 	if(!$category) $category = array(); 	
 	$select = array('0'=>'-- Select --');
 	foreach($category as $val){ $options[$val->id] = $val->category; } 
 	$data['category'] = $select + $options; 	
 	// $data['subcat']  = $this->crud->browse("","subcategory","id",$def->id_subcat,"false","id,subcategory");
 	$data['contents'] = !$contents ? "-" : $contents; 
 	$data['view']	 = "policy_agreement/form_update";
 	$this->load->view('layout/template',$data);
 		
 } 
 
 function get_office(){
 	$id_comp = $_POST['id_comp']; //echo "=>".$id_comp;
 	$select  = " id,office_name,(SELECT region_name FROM region r WHERE r.id=l.id_region) as region";				
 	$company = $this->crud->browse("","offices l","id_company",$id_comp,"true",$select); 	
	echo "<option>-- Select Office --</option>";
	foreach ($company as $row){
		echo "<option value='$row->id'>$row->office_name / $row->region</option>";	
	}
 }
 
 function get_subcategory(){
 	$id_cat  = $_POST['id_cat']; 
 	$select  = "(SELECT id FROM subcategory r WHERE r.id=l.id_subcat) as id_subcat,
 				(SELECT subcategory FROM subcategory r WHERE r.id=l.id_subcat) as subcat";
 	$subcat  = $this->crud->browse("","supplier_category l","id_cat",$id_cat,"true",$select); //print_r($subcat);	
	echo "<option>-- Select Sub Category --</option>";
	foreach ($subcat as $row){
		echo "<option value='$row->id_subcat'>$row->subcat</option>";	
	}
 }

  function get_vendor(){

 	$id_cat  = $_POST['id_cat'];
   	
   	$select  = "vendor.id,vendor_name";
   	$where = "vendor_detail.id_category = ".$id_cat." AND vendor_type='avl'";  		

   	$joins[0][0] = "vendor_detail";
   	$joins[0][1] = "vendor_detail.id_vendor = vendor.id";
   	$joins[0][2] = "left";
   	$joins[1][0] = "category";
   	$joins[1][1] = "vendor_detail.id_category = category.id";
   	$joins[1][2] = "left";

   	$vendor  = $this->crud->browse_join("","vendor","","","true",$select,$joins,$where);
   	$element = '';
  	foreach ($vendor as $row){
  		$element .= "<li>
 				<input id='$row->id' name='cbvendor[".$row->id."]' type='checkbox' value='".$row->vendor_name."'>
 				<label for='$row->id'>
 					<span>$row->vendor_name</span>
 				</label>
 			</li>";
  	}

  	echo $element;
   }

   function get_vendor_with_return($flag = null, $policy_agreement_id){

 	$id_cat  = $flag;
   	
   	$select  = "vendor.id,vendor_name";
   	$where = "vendor_detail.id_category = ".$id_cat." AND vendor_type='avl'";  		

   	$joins[0][0] = "vendor_detail";
   	$joins[0][1] = "vendor_detail.id_vendor = vendor.id";
   	$joins[0][2] = "left";
   	$joins[1][0] = "category";
   	$joins[1][1] = "vendor_detail.id_category = category.id";
   	$joins[1][2] = "left";

   	$direct_biddings  = $this->crud->browse("","direct_bidding","","","true","id_vendor","id_policy_agreement = ".$policy_agreement_id);
   	$direct_bidding = array();
   	foreach ($direct_biddings as $val){array_push($direct_bidding, $val->id_vendor);}	

   	$vendor  = $this->crud->browse_join("","vendor","","","true",$select,$joins,$where);
   	$element = '';
  	foreach ($vendor as $row){
  		if (in_array($row->id, $direct_bidding)) {
  			$element .= "<li>
 				<input id='$row->id' checked name='cbvendor[".$row->id."]' type='checkbox' value='".$row->vendor_name."'>
 				<label for='$row->id'>
 					<span>$row->vendor_name</span>
 				</label>
 			</li>";
  		}else{
  			$element .= "<li>
 				<input id='$row->id' name='cbvendor[".$row->id."]' type='checkbox' value='".$row->vendor_name."'>
 				<label for='$row->id'>
 					<span>$row->vendor_name</span>
 				</label>
 			</li>";
  		}
  	}

  	return $element;
   }
 
 function file_name(){
   	$type = $_POST['type'];
   	if($type == 1){
   		$name = 'general';
   	} if ($type == 2){
   		$name = 'open_sourcing';
   	} if ($type == 3){
   		$name = 'open_bidding';
   	} if ($type == 4){
   		$name = 'direct_selection';
   	}	   		
   	return $name;
   }
   
 function create(){
 	$filename = $this->file_name();
 	$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
 	$data 		= array('id'			=> null,
					 	'type'			=> $_POST['type'],
					 	'title'			=> $_POST['title'],
					 	'creator'		=> $_POST['creator'],
 						'activated'		=> '1',
					 	'file'			=> $filename,
					 	'status'		=> '1',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date);
 				  		
 	if($_POST['content'] == "1"){ 		//echo "from typing";
	 	$editor = $_POST['editor'];	 		
	 	$root   = $_SERVER['DOCUMENT_ROOT'];
	 	$path   = $root."/eproc/uploads/policy_agreement/";
	 	for ($i = 0;$i<2;$i++) {
	 		if ($i==0) {
	 			$myfile = fopen($path.$filename."_ind.txt", "w");
	 		}else{
	 			$myfile = fopen($path.$filename."_eng.txt", "w");
	 		}
	 		$txt    = fwrite($myfile,$editor[$i]);	
	 	}
 	} else if ($_POST['content'] == '2' && $_FILES['contentfileind'] && $_FILES['contentfileeng']) { 		//echo "from upload"; 
 		$ctnfile[0]  = $_FILES['contentfileind'];
 		$ctnfile[1]  = $_FILES['contentfileeng'];
 		for ($i=0; $i < 2; $i++) { 
 			$_FILES['userfile']  = $ctnfile[$i];
	 		$path 	  = './uploads/policy_agreement/';
	 		if ($i==0) {
	 			$config   = $this->m_policy_agreement->set_config($filename."_ind.txt",$path,'txt');
	 		}else{
	 			$config   = $this->m_policy_agreement->set_config($filename."_eng.txt",$path,'txt');
	 		}
	 		$this->load->library('upload', $config);
	 		$this->upload->initialize($config);
	 		
	 		$msg['error_content'] = "";  $msg['success_content']   = array();
			if ( ! $this->upload->do_upload()){
				$msg['error_content']   = $this->upload->display_errors();
			} else {
				$msg['success_content'] = $this->upload->data(); }
 		}
 	}
 	
 	$root    = $_SERVER['DOCUMENT_ROOT'];
 	$path 	 = $root.'/eproc/uploads/attach_temp/';
 	$path1   = $root.'/eproc/uploads/attach/'; 
 	$datefile = $this->file_pdf();
  	$default  = $this->crud->browse("","policy_agreement_temp","id","","false","deskripsi");
  	//print_r(sizeof($default));
  	foreach ($default as $val){
  	$ctpath   = $datefile.$val->deskripsi.".pdf";
  	//print_r($ctpath);
  	//$cek = $path.$ctpath;
  	//print_r($default);exit;
  	//foreach($)
 	rename($path.$ctpath, $path1.$ctpath);
  	}
  	//exit;
 	$this->crud->delete_all("","policy_agreement_temp");
 	$this->crud->insert("","policy_agreement",$data);

 	$this->session->set_flashdata('message','1 data success insert');
 	redirect('policy_agreement/','refresh');
 }  
 
 function update(){ 	
 	$def        = $this->crud->browse("","policy_agreement","id",$_POST['id'],"false");
 	$filename 	= !$def->file ? $this->m_policy_agreement->file_name("policy_agreement") : $def->file;
 	$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();

 	$data 		= array('type'			=> !$_POST['type'] ? $def->type : $_POST['type'],
					 	'title' 		=> !$_POST['title'] ? $def->title : $_POST['title'],
					 	'creator'		=> !$_POST['creator'] ? $def->creator : $_POST['creator'],
					 	'file'			=> $filename,
					 	'status'		=> '1',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date);

 				  		
 	if($_POST['content'] == "1"){ 		//echo "from typing";
	 	$editor = $_POST['editor'];
	 	$root   = $_SERVER['DOCUMENT_ROOT'];
	 	$path   = $root."/eproc/uploads/policy_agreement/";
	 	for ($i = 0;$i<2;$i++) {
	 		if ($i==0) {
	 			$myfile = fopen($path.$filename."_ind.txt", "w");
	 		}else{
	 			$myfile = fopen($path.$filename."_eng.txt", "w");
	 		}
	 		$txt    = fwrite($myfile,$editor[$i]);	
	 	}
 	} else if ($_POST['content'] == '2' && $_FILES['contentfileind'] && $_FILES['contentfileeng']) { 		//echo "from upload"; 
 		$ctnfile[0]  = $_FILES['contentfileind'];
 		$ctnfile[1]  = $_FILES['contentfileeng'];
 		for ($i=0; $i < 2; $i++) { 
 			$_FILES['userfile']  = $ctnfile[$i];
	 		$path 	  = './uploads/policy_agreement/';
	 		if ($i==0) {
	 			$config   = $this->m_policy_agreement->set_config($filename."_ind.txt",$path,'txt');
	 		}else{
	 			$config   = $this->m_policy_agreement->set_config($filename."_eng.txt",$path,'txt');
	 		}
	 		$this->load->library('upload', $config);
	 		$this->upload->initialize($config);
	 		
	 		$msg['error_content'] = "";  $msg['success_content']   = array();
			if ( ! $this->upload->do_upload()){
				$msg['error_content']   = $this->upload->display_errors();
			} else {
				$msg['success_content'] = $this->upload->data(); }
 		}
 	}

 	$this->crud->update("","policy_agreement","id",$_POST['id'],$data); 	
 	$this->session->set_flashdata('message','1 data success insert');
 	redirect('policy_agreement/','refresh');
 }
 
 function file_pdf(){
 	$date = date('Ymd');
 	$namefile = $date.'_';
 	return $namefile;
 }
 
 function add_file(){	
 	$curr_date = date('Y-m-d'); 
 	$datefile = $this->file_pdf();
 	$namefile = $_POST['deskripsi'];
 	$filename = $datefile.$namefile;
	$pdffile = $_FILES['pdffile'];
 	$root    = $_SERVER['DOCUMENT_ROOT'];
 	$data  = array('deskripsi'=>$_POST['deskripsi'],'keterangan'=>$_POST['keterangan'],'created_date'=>$curr_date);
 	for ($i=0; $i < 2; $i++){
	 	$_FILES['userfile']  = $pdffile;
	 	$path 	 = '/uploads/attach_temp/';
	 	$config  = $this->m_announce->set_config($filename.".pdf",$path,'pdf');
	 	$this->load->library('upload', $config);
	 	$this->upload->initialize($config);
	 		
	 	$msg['error_pdf'] = "";  $msg['success_pdf']   = array();
	 	if ( ! $this->upload->do_upload()){
	 		$msg['error_pdf']   = $this->upload->display_errors();
	 	} else {
	 		$msg['success_pdf'] = $this->upload->data(); }
 	
	 		
 	}	
 	
 	$this->crud->insert("","policy_agreement_temp",$data);
 	$this->session->set_flashdata('message','1 data success insert');
 	redirect('policy_agreement/form_create','refresh');		
 }

 function get_tags(){ 	
 	$term   = $_GET['term'];  	
 	$order  = array('field'=>'code','sort'=>'ASC'); $select = "code as name";
 	$result = $this->crud->autocomplete("","policy_agreement",$select,"code",$term,"",$order);  	
 	echo json_encode($result);   	 		
 }

 function reviewed(){
 	if ($this->m_policy_agreement->authenticate()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
	 	$id        = $this->uri->segment(3);
	 	$select	   = "id,type,title,creator,file,
					  status,created_id,created_date,last_updated";						
	 	$default   = $this->crud->browse("","policy_agreement l","id",$id,"false",$select); 

	 	if($default->file){

			$ctpath_id  = "./uploads/policy_agreement/".$default->file."_ind.txt";		
			$ctpath_eng  = "./uploads/policy_agreement/".$default->file."_eng.txt";		
			
	 		$contents[0] = file_exists($ctpath_id) ? file_get_contents($ctpath_id) : "-";
	 		$contents[1] = file_exists($ctpath_eng) ? file_get_contents($ctpath_eng) : "-";
	 	}

	 	$data['content'] = !$contents ? "-" : $contents; 		 	
	 	
	 	$data['type']    = array('0'=>'General','1'=>'News','2'=>'Open Sourcing','3'=>'Open Bidding','4'=>'Direct Selection');
	 	$data['default'] = $default;
	 	$data['view'] 	 = "policy_agreement/form"; 		
	 	$this->load->view('layout/template',$data);
	 	
	 } else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	} 	
 	
 }
 
 function open_content_ind(){ 	
 	$id       = $this->uri->segment(3);
 	$default  = $this->crud->browse("","policy_agreement","id",$id,"false","file"); 	
	$ctpath   = "./uploads/policy_agreement/".$default->file."_ind.txt";
	echo file_get_contents($ctpath);
 }

  function open_content_eng(){ 	
  	$id       = $this->uri->segment(3);
  	$default  = $this->crud->browse("","policy_agreement","id",$id,"false","file"); 	
 	$ctpath   = "./uploads/policy_agreement/".$default->file."_eng.txt";
 	echo file_get_contents($ctpath);
  }
  
  function open_content_pdf(){
  	$id       = $this->uri->segment(3);
  	$datefile = $this->file_pdf();
  	$default  = $this->crud->browse("","policy_agreement_temp","id",$id,"false","deskripsi");
  	$ctpath   = "./uploads/attach_temp/".$datefile.$default->deskripsi.".pdf";
  	$filename = $datefile.$default->deskripsi.".pdf";
  	
  	//print_r($filename);exit;
  	//echo file_get_contents($ctpath);
  	
  	header('Content-type: application/pdf');
  	header('Content-Disposition: inline; filename="' . $filename . '"');
  	header('Content-Transfer-Encoding: binary');
  	header('Content-Length: ' . filesize($ctpath));
  	header('Accept-Ranges: bytes');
  	@readfile($ctpath);
  }
 
 function open_file(){
 	
 	$index    = $this->uri->segment(3);
 	$id       = $this->uri->segment(4);
 	$default  = $this->crud->browse("","policy_agreement","id",$id,"false","file"); 
	$folder   = $index=="1" ? "attach" : "rfp";
	$file     = "./uploads/".$folder."/".$default->file.".pdf";
	$filename = $default->file.".pdf";

	header('Content-type: application/pdf');
	header('Content-Disposition: inline; filename="' . $filename . '"');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($file));
	header('Accept-Ranges: bytes');
	@readfile($file);

 }

 function update_status(){
 	
 	$date   = date('Y-m-d H:i:s');
 	$update = array('status'=>$_POST['id_status'],'last_updated'=>$date);

 	$this->crud->update("","policy_agreement","id",$_POST['id'],$update); 	
 	$this->session->set_flashdata('message','1 data success update');
 	redirect('policy_agreement/','refresh');
 	
 }
 
 function set_browse_session(){
 	$code     = $_POST['search'];	
 	$policy_agreement = $this->crud->browse("","policy_agreement","code",$code,"false","id");
 		
 	if($policy_agreement) $this->session->set_flashdata('anID',$policy_agreement->id); 
 	
 }
 
 function set_sess_category_(){
 	
 	$type = $_POST['type'];
 	if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
 	} else { $this->session->unset_userdata('sess_cat'); }
 	
 }

 function delete(){
 
 	$id = $this->uri->segment(3);
 	$this->crud->delete("","policy_agreement","id",$id);
 	$this->session->set_flashdata('message','1 data success deleted');
 	redirect('policy_agreement/','refresh');
 
 }
 
 function delete_file(){
 	$id   = $this->uri->segment(3);
 	$this->crud->delete("","policy_agreement_temp","id",$id);
 	redirect('policy_agreement/form_create/','refresh');
 }
 
}
?>