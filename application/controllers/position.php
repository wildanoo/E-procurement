<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Position extends Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_user');
		islogged_in();
		$this->m_user->authenticate(array(41));
	}	

	function index() {
		//if ($this->permit->mc) {
		
		$extract = $this->getDataTables(5);
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
			
			$data ['user_id'] = $this->tank_auth->get_user_id ();
			$data ['username'] = $this->tank_auth->get_username ();
			
			$sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
				
			
			$field      = $sess_field != '' ? $sess_field : "position_name";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
			
			if($sess_cat != ''){	$where = array('position_name'=>$sess_cat,'status'=>'1');
			} else { $where = array('status'=>'1');	}
			
			$table = "position";
			$page = $this->uri->segment ( 3 );
			$per_page = 10;
			$offset = $this->crud->set_offset ( $page, $per_page, $where );
			$total_rows = $this->crud->get_total_record ("",$table,$where);
			$set_config = array (
					'base_url' => base_url() . '/position/index',
					'total_rows' => $total_rows,
					'per_page' => $per_page,
					'uri_segment' => 3 
			);
			$config = $this->crud->set_config ( $set_config );
			
			$this->load->library ( 'pagination' );
			$this->pagination->initialize ( $config );
			$paging = $this->pagination->create_links ();
			
			$order = array (
					'field' => 'created_date',
					'order' => 'DESC' 
			);
			$data ['pagination'] = $paging;
			$data ['num'] = $offset;
			$select = "id,post_code,position_name,remark,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";

			$sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
				
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
			$data['browse'] = $browse;

	 		$data['view']	= "position/browse"; 		
 			$this->load->view('layout/template',$data);
			
// 		} else {
// 			$this->session->set_flashdata('message','user not authorized');
// 			redirect ( '/auth/login/' );
// 		}
	}
	
	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
	
		//$search_dateranges = array('publish_date',$_POST['start_date'],$_POST['end_date']);
		$search_conditions = array(
				'post_code'		=> $_POST['search_term'],
				'position_name' => $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
// 		$data['category'] 		 = $this->getCategories();
// 		$data['subcategory'] 	 = $this->getSubcategories();
// 		$data['subcontent_type'] = $this->getSubcontentTypes();
// 		$data['status_ref'] 	 = $this->getStatusCondition();
	
		$data['view'] = "position/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{
// 		$subcontent_allowed = globalNeedAanwidzing();
	
// 		$sess_cat = $this->session->userdata('sess_cat');
// 		$sessID   = $this->session->flashdata('anID');
			
// 		$field    = $sess_cat ? "id_cat" : "";
// 		$id_cat   = $sess_cat ? $sess_cat  : "";
	
// 		if($sess_cat){
// 			$where = "t1.subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'";
// 		} else {
// 			$where = !$this->session->userdata('is_admin') ? "subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'":"";
// 		}
	
		/* get data for table view from database with pagination */
	
		$table 	     = "position t1";
		$select 	 = "t1.id,post_code,position_name,remark,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		//$order 		 = array('field'=>'t1.publish_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		// if($sessID)	$getData = $this->crud->browse("",$table,"id",$sessID,"true",$select,$where);
		// else  {
// 		$joins[0][0] = 'announcement_level_approval t2';
// 		$joins[0][1] = 't2.id_announcement = t1.id';
// 		$joins[0][2] = 'left';
// 		$joins[1][0] = 'level t3';
// 		$joins[1][1] = 't2.id_level = t3.id';
// 		$joins[1][2] = 'left';
	
		// $where		 = !$this->session->userdata('is_admin') ? "t1.subcontent_type IN ('4','5') AND t1.status = '3'":"";
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'position/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
	
		/* get data for table view from database with pagination */
	
		$table 	     = "position t1";
		$select 	 = "t1.id,post_code,position_name,remark,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,'t1.status = "1"',$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'position/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	function is_exist(){
		$code      = $_POST['post_code'];
		$position  = $_POST['position_name'];
		$is_exist['1']= !$code ? "false" : "true";
		$is_exist['2']= !$position ? "false" : "true";
		$msg['1']	  = !$code ? "position code required" : "";
		$msg['2']	  = !$position ? "position name required" : "";
				
		if ($is_exist['1']=='true' &&  $is_exist['2']=='true'){
				$where    = array('post_code'=>$code, 'position_name'=>$position,'status'=>'1');
				$checked = $this->crud->is_exist("","position","id",$where);
				$is_exist['1']    = !$checked ? "true" : "false";
				$is_exist['2']    = !$checked ? "true" : "false";
				$msg['1']	= $checked ? "duplicate position code" : "";
				$msg['2']	= $checked ? "duplicate position name" : "";
					
		}
		
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2']);
		
		echo json_encode($result);
	}
	
	function create(){
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array('id'=>null,'post_code'=>$_POST['post_code'],'position_name'=>$_POST['position_name'],
					  'created_id'=>$userID,'created_date'=>$curr_date,'last_updated'=>$curr_date);
		$where   = array('position_name'=>$_POST['position_name'],'status'=>'0');
		$checked = $this->crud->is_exist("","position","id",$where);
	
		if ($checked){ 
			$id = $this->crud->browse("","position","position_name",$_POST['position_name'],"false","id")->id;
			$update = array('status'=>'1','last_updated'=>$curr_date);
			$this->crud->update("","position","id",$id,$update);
		} else {	$id = $this->crud->insert("","position",$data);	}
		$this->session->set_flashdata('message','1 data success insert');
	}
	
	function form_create() {
		$data ['user_id'] = $this->tank_auth->get_user_id ();
		$data ['username'] = $this->tank_auth->get_username ();		
 		$this->load->view('position/form_create',$data);
	}
	
	function update(){
		$curr_date 	= date('Y-m-d H:i:s');
		$userID = $this->tank_auth->get_user_id();
		$id   = $this->uri->segment(3);
		$data = array('post_code'=>$_POST['post_code'],'position_name'=>$_POST['position_name'],
					  'status'=>'1','last_updated'=> $curr_date,'created_id'=>$userID);
		$this->crud->update("","position","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
	}
	
	function form_update(){
		$id   = $this->uri->segment(3);
		$select = "id,post_code,position_name,remark,created_id,created_date,last_updated";
		$data ['def'] = $this->crud->browse ( "", "position", "id", $id, "false", $select);
		$this->load->view ( 'position/form_update', $data );
	}
	
	function delete(){
	
		$id = $this->uri->segment(3);
		$status = array('status'=>'0');
		$this->crud->update("","position","id",$id,$status);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('position/','refresh');
	}
    
    	function delete_(){
		$data ['username'] = $this->tank_auth->get_username ();
		$id = $this->uri->segment(3);
		
		$where1   = array('id_country'=>$id);
		$checked1 = $this->crud->is_exist("","region","id",$where1);
		
		//$checked2 = $this->crud->is_exist("","vendor","id",$where1);
		//if ($checked1 || $checked2){
		if ($checked1){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		}
		else {
			$this->crud->delete("","country","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
		redirect('country/','refresh');
		}
}