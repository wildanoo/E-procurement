<div id="container11">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   
   $('#btn_cancel').click(function(){ 
   		$("#container11").load(baseUrl+'due_diligence/load_visit_form');   		
   }); 	
   
   $('#btn_attach').click(function(){  
   		var id_visit = $('#id_visit').val();
   		$("#container11").load(baseUrl+'due_diligence/form_attachment/' + id_visit);   		
   });  
   
   $('#btn_recmd').click(function(){  
   		$("#container11").load(baseUrl+'vendor/form_recommend');   		
   });  	
   
 });  
  
 function update_docdd(id){
 	
 	$("#container11").load(baseUrl+'due_diligence/form_update_duedill/'+id); 
 }
 
 function delete_docdd(id){
 	
 	var baseUrl  = "<?php echo base_url(); ?>";
 	var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';
    if (confirm('Are you sure you want to delete this?')) {
	 	$.post( baseUrl+ "due_diligence/delete_duedill", { csrf: token, id: id })
		  .done(function( resp ) {
		  		var json = $.parseJSON(resp);						
		    	$("#container11").load(baseUrl+'due_diligence/visit_docs/'+ json.id_visit);
		});
	}	
 }
  
 </script>
<?php if($this->permit->uvd){ ?> 

<div style="text-align: right;">
<button type="button" id="btn_cancel" class="btn btn-default">Cancel</button>
<?php if($this->permit->md3){ ?>
<button type="button" id="btn_attach" class="btn btn-info btn-primary">Add Attachment</button>
<?php } ?>
</div>

<?php  } ?>

<input type="hidden" name="id_visit" id="id_visit" value="<?php echo $id_visit; ?>" />
<?php echo form_open('#',array('id'=>'form_visit_docs')); ?>
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Document Name</th>		
		<th>Remark</th>
		<th>Created Date</th>
		<?php if($this->permit->md3){ ?><th>Action</th><?php  } ?>
	</tr>
	</thead>
	<tbody>

 <?php   
 if($visit_docs){ $i=1;
 	foreach($visit_docs as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->doc_name; ?></td>
			<td><?php echo $row->remark ?></td>
			<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
			<?php if($this->permit->md3){ ?>
			<td style="text-align: center;">			
			<?php 			
					$icon = '<i class="glyphicon glyphicon-download"></i>';			
					$prop = array('title'=>'Download'); 				 
				  	echo anchor('due_diligence/download_duedill/'.$row->id,$icon,$prop);
						echo '&nbsp;&nbsp;&nbsp;';	
						echo '<i class="fa fa-pencil" onclick="update_docdd('.$row->id.')"></i>'; echo '&nbsp;&nbsp;&nbsp;';					
						echo '<i class="fa fa-trash"  onclick="delete_docdd('.$row->id.')"></i>'; 	
			?></td>
			<?php } ?>											
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="8">-</td></tr>
<?php } ?>
     </tbody>
</table>
<?php echo  form_close(); ?>

</div>