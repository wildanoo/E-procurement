<html>
<head>
	<title></title>
	<meta name="" content="">
</head>
<body>

	Dengan hormat,<br>
	<b>Bapak/Ibu <?php echo $recip ?></b><br><br>

	Bersama informasi ini kami ingatkan bahwa dokumen surat legalitas vendor dibawah ini untuk segera di perbaharui :<br><br>
	<b>Nama Perusahaan :</b><?php echo $ven_name ?><br>
	<b>Nomor Registrasi :</b><?php echo $reg_num ?><br><br>
	<table border="1">
		<tr>
			<th>No</th>
			<th>Nama Dokumen</th>
			<th>Nomor Dokumen</th>
			<th>Tanggal Expired</th>
			<th>Deskripsi</th>
		</tr>
		<?php 
		$no=1;
		foreach($doc as $d){ ?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $d['doc_name'] ?></td>
				<td><?php echo $d['doc_num'] ?></td>
				<td><?php echo $d['exp_date'] ?></td>
				<td><?php echo $d['desc'] ?></td>
			</tr>

			<?php $no++; } ?>
		</table>
		<br><br>

		Demikian disampaikan, atas perhatian dan kerjasamanya kami ucapkan terima kasih.<br><br>

		<br>
		<b>PT Garuda Indonesia (Persero) Tbk</b><br>
		<i>Business Support / JKTIBGA <br>
			Gedung Management Garuda, Lantai Dasar <br>
			Garuda City,  Bandara Soekarno Hatta <br>
			Cengkareng 19120, PO BOX 1004 TNG BUSH</i>


		</body>
		</html>