<header class="clearfix header5">
    <div class="container">
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img alt="" src="<?php echo base_url()?>assets_public/images/garuda-logo.png">
                    </a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right2">
                        <li class="active visible-xs"><a href="<?php echo base_url()?>auth/login#top">HOME</a></li>
                        <li class="visible-xs visible-sm visible-md visible-lg"><a href="<?php echo base_url()?>auth/login#login">LOGIN</a></li>
                        <li class="visible-xs"><a href="<?php echo base_url()?>auth/registration">REGISTRATION</a></li>
                        <li class="visible-xs"><a href="<?php echo base_url()?>auth/vendor">VENDOR POLICY</a></li>
                        <li class="visible-xs"><a href="<?php echo base_url()?>auth/login#contact">CONTACT US</a></li>
                    </ul>
                    <ul class="social-icons">
    					<?php if ($this->session->userdata('lang') == 'eng') { ?>
                        <li class="lang eng img-lang" data="lang-eng" title="English"><a style="" href="#"></a></li>
                        <li class="lang ind img-lang" data="lang-ind" title="Indonesian"><a class="inactive" href="#" id="lang_ind"></a></li>
                        <?php }elseif ($this->session->userdata('lang') == 'ind') { ?>
                        <li class="lang eng img-lang" data="lang-eng" title="English"><a class="inactive" href="#" id="lang_eng"></a></li>
                        <li class="lang ind img-lang" data="lang-ind" title="Indonesian"><a style="" href="#"></a></li>
                        <?php }?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>