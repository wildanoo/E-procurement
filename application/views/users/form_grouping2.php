 
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Users Groups</h3>
              </div><!-- /.box-header -->
              <div class="col-md-2 pull-left">
               
              </div>

                <!--div class = "col-md-10 pull-right">
                  <div class="input-group pull-right" style="width: 150px;">
                      <input type="text"  name="srch-term" id="srch-term"  class="form-control input-sm pull-right" placeholder="Search" style="height: 28px;">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                </div-->
<?php echo form_open("user/create_grouping",array('class'=>'form-horizontal'));               
              $group_id = $this->session->userdata("group_id");
              $user_id  = $this->session->userdata("user_id");
              echo form_hidden("user_id",$user_id);
              ?>
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover text-center">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>&nbsp;</th>
                        <th>Group Name</th>
                        <th>permissions</th>                    
                      </tr>
                    </thead>
                    <tbody>
<?php
if($groups){ $i=1;
	foreach($groups as $row){ ?>	
			<tr>
				<td style="vertical-align: text-top;"><?php echo $i; ?></td>
				<?php $checked = ($row->id == $group_id) ? true : false; ?>
                <td style="vertical-align: text-top;"><?php echo form_radio('group_id',$row->id, $checked); ?></td>
				<td style="vertical-align: text-top;text-align: left;"><?php echo ucwords($row->name); ?></td>
				<td style="text-align: left;"><?php 
				
					if($row->permissions){
						$permit = explode(",",$row->permissions); $j=1;					
						foreach($permit as $val){ echo $j.". ".$val."<br/>";	$j++; } unset($j);
					} else { echo "<b><i>No Defined</i></b>";	}
					
					
				?></td>
			</tr>
<?php	$i++;  } ?>
<?php } else { ?>
	<tr><td colspan="5" align="center">-</td></tr>
<?php } ?>
                    </tbody>
                  </table>
                  <div class="box-footer" style="padding-left: 30px;">
                    <?php  $prop = array('class'=>'btn btn-sm btn-default','title'=>'Cancel'); ?>					 
					<?php  echo anchor('user/','Cancel',$prop); ?>
                    <?php  echo form_submit('submit', 'Submit','class="btn btn-sm btn-primary"'); ?>
                </div><!-- /.box-footer -->
              <?php echo form_close(); ?>                  
                </div><!-- /.box-body -->
              <!--/form-->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->