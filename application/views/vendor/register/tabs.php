<?php $ses_tab = $this->session->userdata('tab'); 
	  $tab 	   = !$ses_tab ? "home" : $ses_tab ; ?>
<?php  /*$sess_log = $this->session->userdata('logs');
     $log   = $sess_log ? $sess_log : ''*/;?>
<script type="text/javascript">
   $(document).ready(function(){

		activaTab('<?php echo $tab; ?>');
   });
   
   function activaTab(tab){
    	$('.nav-tabs a[href="#' + tab + '"]').tab('show');
   };

</script>

<div id="info_status">
<?php $this->load->view('vendor/register_status'); ?>
</div>
<div id="tabs_container">

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">General information</a></li>
  <?php if($vendor->id_stsreg>=4) { ?><li><a data-toggle="tab" href="#menu1">Due Diligence</a></li>
  <li><a data-toggle="tab" href="#menu2">Recomendation</a></li><?php } ?>
  <li><a data-toggle="tab" href="#menu3">Tracking Registration</a></li>
  <li><a data-toggle="tab" href="#menu4">Status Vendor Activity</a></li>
</ul>

<?php $this->session->unset_userdata('tab'); ?>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <!--h3>General Information</h3-->
    <p><?php  $this->load->view('vendor/detail/general'); ?></p>
  </div>
  <?php if($vendor->id_stsreg>=4) { ?>
	  <div id="menu1" class="tab-pane fade">
	    <!--h3>Due Diligence</h3-->
	    <p><?php $this->load->view('due_diligence/site_visit',$data); ?></p>
	  </div>  
  <div id="menu2" class="tab-pane fade">
    <!--h3>Recomendation</h3-->
    <p><?php  $this->load->view('due_diligence/form_recmd'); ?></p>
  </div>
   <?php } ?>
  <div id="menu3" class="tab-pane fade">
    <!--h3>" This feature Under Development "</h3-->
    <p><?php  $this->load->view('vendor/detail/notes',$data); ?></p>
  </div> 
  <div id="menu4" class="tab-pane fade">
    <p><?php $this->load->view('vendor/vendorlog/home'); ?></p>
  </div>
</div>

</div>