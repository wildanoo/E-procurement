<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<title>E-Procurement Garuda</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/skins/red-blue/blue.css" id="colors" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/datetimepicker/jquery.datetimepicker.css"/>
	<script src="<?php echo base_url() ?>assets_/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url() ?>assets_/datetimepicker/build/jquery.datetimepicker.full.js"></script>
<!-- 	<link rel="stylesheet" type="text/css" href="css/component.css" /> -->
	<link rel="icon" href="favicon-grd.png" sizes="16x16">
</head>

<script type="text/javascript"> 
	$(document).ready(function(){

        $("#start_date").datetimepicker({
			format:'Y/m/d',
			timepicker:false,
			maxDate: $("#end_date").val(),
			onChangeDateTime: function (dp,selectedDate) {
				var time = $("#start_date").val();

	            $("#end_date").datetimepicker({minDate: selectedDate.val()});
	        }
		});

		$("#end_date").datetimepicker({
			format:'Y/m/d',
			minDate: 0,
			timepicker:false,
			onChangeDateTime: function (dp,selectedDate) {
				var time = $("#start_date").val();
				var init = selectedDate.val();

	            $("#start_date").datetimepicker({maxDate: selectedDate.val()});
	        }
		});

	});			
</script>
<body>
<div id="wrapper">
	<header id="header" class="container-fluid">
	  <div class="header-bg">
		<div class="container">	  
			<div class="row">
				<div id="logo" class="col-md-5 col-md-offset-1"><a href="login" class="logo" alt="E-Procurement Garuda Indonesia"></a></div>
				<div class="col-md-4 col-md-offset-1">
					<ul class="social-icons">
					<?php if ($this->session->userdata('lang') == 'eng') { ?>
						<li class="lang eng" data="lang-eng" title="English"><a style="top:0 !important;" href="#">English</a></li>
						<li class="lang idn" data="lang-ind" title="Indonesian"><a class="inactive" href="#" id="lang_ind">Indonesian</a></li>
					<?php }elseif ($this->session->userdata('lang') == 'ind') { ?>
						<li class="lang eng" data="lang-eng" title="English"><a class="inactive" href="#" id="lang_eng">English</a></li>
						<li class="lang idn" data="lang-ind" title="Indonesian"><a style="top:0 !important;" href="#">Indonesian</a></li>
					<?php }?>
					</ul>	
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="select-menu hidden-lg">
						<select id="selectMenu">
							<option selected value="<?php echo base_url(); ?>">Home</option>			
							<option value="<?php echo base_url(); ?>webcontent/archive/news">News</option>
							<option value="#">FAQs</option>
							<option value="#">Supplier Registration</option>
							<option value="login.html">Log In</option>
							<option value="<?php echo base_url(); ?>main/contact_us">Contact us</option>
						</select>
					</div>
					<ul id="menu" class="visible-lg" style="font-size: 13px;">
						<li>
							<a href="<?php echo base_url(); ?>">HOME</a>
						</li>
						<li><?php  echo anchor('webcontent/archive/news','News'); ?></li>
						<li><a href="#">FAQs</a></li>
						<li id="st-trigger-effects2">
							<a data-effect="st-effect-2">SUPPLIER REGISTRATION</a>
						</li>
						<li id="st-trigger-effects1">
							<a data-effect="st-effect-1">LOG IN</a>
						</li>
						<li><?php  echo anchor('main/contact_us','CONTACT US'); ?></li>
						<!-- Search Form -->
						<li class="search-form visible-desktop" style="float: right;top: 10px;"\>
							<form method="get" action="#">
								<input type="text" value="Search" class="search-text-box"/>
								<input type="submit" value="" class="search-text-submit"/>
							</form>
						</li>
					</ul>
				</div>
			</div>
		</div>
	  </div>
	</header>
	<section id="content" class="container-fluid">
	<div class="container" style="width: 940px;margin: 0 auto;padding:0;">
		<!-- Search Term -->
		<div class="table">
			<div class="panel panel-default">
			    <div class="panel-heading">
			        <i class="fa fa-search"></i> <strong>Search Section</strong>
			    </div>
			    <style type="text/css">
			    .list-inline li{
			    	vertical-align: middle !important;
			    }
			    .btn-srch{
			    	margin-bottom: 0;
			    }
			    </style>
			    <?php echo form_open_multipart('announce/search',array('id'=>'form-search','novalidate' => 'true'));	//$current_date = date('Y/m/d')?>
			    <div class="panel-body" style="padding-bottom: 0;">
			    	<ul class="list-inline">
			    		<li style="width:24%">
			    			<div class="input-group">
			    			<input type="text" value="" placeholder="date start" class="form-control date_picker" id="start_date" name="start_date">
			    			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
			    			</div>
			    		</li>
			    		<li style="width:2%">to</li>
			    		<li style="width:24%">
			    			<div class="input-group">
			    			<input type="text" value="" placeholder="date end" class="form-control date_picker" id="end_date" name="end_date">
			    			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
			    			</div>
			    		</li>
			    		<li style="width:33%"><div class="input-group" style="width:100%"><input placeholder="search by code or title..." type="text" value="" class="form-control" id="search_term" name="search_term"></div></li>
			    		<li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search">Search</button></div></li>
			    		<li style="width:7%"><div class="input-group"><a href="<?php echo base_url()?>announce" class="btn btn-srch btn-warning btn-sm">Show All</a></div></li>
			    	</ul>
			    </div>
			    <?php echo form_close()?>
			    <!-- <div class="panel-footer text-center">
			        <a href="#" class="btn btn-primary" style="margin-bottom:0;">Change Password</a>
			    </div> -->
			</div>
		</div>
		<!-- Search Term -->
		<div class="row">
			<div class="col-md-8" style="position: relative;">
			<div class="headline"><h4>Content of <?php echo $title?></h4></div>
			<hr>
			<?php if (count($browse) > 0) {
			foreach ($browse as $content) { ?>
				<div class="accordion-group">
					<div>
						<small><strong>
					  	<?php 
						$date = str_replace('/', '-', $content->publish_date);
						echo date('M d, Y h:i:s A', strtotime($date));
						?>
						</strong></small>
						<a href="<?php echo base_url();?>webcontent/page/<?php echo $content->id;?>">
							<h3 style="margin:5px 0 3px;"><?php echo $content->title; ?></h3>
						</a>
					</div>
					<div id="news<?php echo $content->id; ?>" style="margin-bottom:15px;">
						<p style="font-size: 14px;text-align:justify;">
							<?php echo $content->detail; ?>
						</p>
					</div>
			  	</div>
			  	<hr/>
			<?php }}else{ echo "<p class='col-md-12 row'>No Event(s) yet.</p>"; }?>
			<span><?php echo $pagination;?></span>
			</div>
			<?php $this->load->view('content_public/recent_post',$recent_post); ?>
		</div>
	</div>
	</section>
</div>
<footer class="main-footer footer-line container-fluid text-center">
	Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
</footer>
<!-- <script src="<?php //echo base_url(); ?>assets/js/jquery.min.js"></script> -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.color.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/SmoothScroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>
</html>