<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class News_public extends Controller {

	function __construct()
	{
		parent::__construct();		
		$this->load->helper('file');
		$this->load->model(array('m_announce','codec'));
		$this->permit = new stdClass();

		/* get function islogged_in() from helpers - to check if user already logged in */
		islogged_in();
		/* ---------------- */

		$this->permit = $this->crud->get_permissions('9');
		define(EPROC_IMAGE_PATH, "./uploads/images/");
		define(EPROC_ATTACHMENT_PATH, "./uploads/attach/");
		define(EPROC_CONTENT_DESC_PATH, "./uploads/contents/");
		define(EPROC_ATTACHMENT_TEMP_PATH, "./uploads/tmp_attach/");

	}


	////// * Main Methods * //////

	function form_create(){

	 	$data['user_id']  = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();

	 	$data['company'] = $this->getCompany();
	 	$data['areas'] 	 = $this->getAreas();
	 	$data['offices'] = $this->getOffices();

	 	// set view
	 	$data['view']	= "news_public/form_create";
	 	$this->clear_cache();
	 	$this->load->view('layout/template',$data);

	}

	function create(){

		/* initial variable */
	 	$filename 		   	  = $this->m_announce->file_name("announce");
	 	$curr_date 		   	  = date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
	 	$dept	 		   	  = afterloginGetDeptId();
	 	$description_field 	  = isset($_POST['editor'])?$_POST['editor']:false;
	 	$image_field	   	  = $_FILES['imagefile']["error"] != 4?$_FILES['imagefile']:false;
	 	$contentfileind_field = $_FILES['contentfileind']["error"] != 4?$_FILES['contentfileind']:false;
	 	$contentfileeng_field = $_FILES['contentfileeng']["error"] != 4?$_FILES['contentfileeng']:false;
	 	$attachment_field	  = $_FILES['pdffile']["error"] != 4?$_FILES['pdffile']:false;
	 	$content_exist 		  = 0;
	 	$content_eng 		  = 0;
	 	$contentfile_eng	  = 0;
	 	$extension 	 		  = 'txt';
	 	/* ---------------- */	 		

	 	/* initial data to insert/update DB */
	 	$data 	= array('id'			  => null,
					 	'content_type'	  => '1',
					 	'subcontent_type' => '1',
					 	'id_company'	  => $_POST['id_company'],
					 	'id_region'	  	  => $_POST['id_area'],
					 	'id_office'		  => $_POST['id_office'],
					 	'title_ind'		  => $_POST['title_ind'],
					 	'title_eng'		  => $_POST['title_eng'],
					 	'detail_eng'	  => $_POST['detail_eng'],
					 	'detail_ind'	  => $_POST['detail_ind'],
					 	'publish_date'	  => $_POST['publish_date'],
					 	'creator'		  => $_POST['creator'],
					 	'file'			  => $filename,
					 	'status'		  => '2',
					 	'created_id'	  => $userID,
					 	'created_date'	  => $curr_date,
					 	'id_departement'  => $dept?$dept->id_dept:'',
					 	'last_updated'	  => $curr_date);
	 	/* ---------------- */

	 	/* initial data attachment for history */
	 	$data_attach = array('filename' => $filename);
		/* ---------------- */
	 	
	 	/* making a file and uploaded if content description inserted by WYSWYG */
	 	if($description_field){
	 		$path 	 = "/uploads/contents/";

	 		for ($i = 0;$i<2;$i++) {
		 		if ($i==0) {
		 			$this->upload_text_into_file($description_field[$i],'',$path,$filename."_ind",'txt');
		 		}else{
		 			$this->upload_text_into_file($description_field[$i],'',$path,$filename."_eng",'txt');
		 			// added by dedi rudiyanto //
		 			if($description_field[$i]!='' && !empty($description_field[$i]))
		 			{
		 				$content_eng = 1;
		 			}
		 			//////////////////////////
		 		}
		 	}		 		
	 	}
	 	/* ---------------- */
	 	///////////// modifed by dedi rudiyanto /////////////
	 	/* upload content description file */
	 	if ($contentfileind_field) {
 			if ($this->crud->is_valid_to_upload($contentfileind_field,array($extension),5000000,'text')) {
 				$this->upload_text_file($contentfileind_field,EPROC_CONTENT_DESC_PATH,$filename."_ind",$extension);
		 	}else{
			    redirect('news_public/form_create','refresh');
 			}	 				
	 	}

	 	if ($contentfileeng_field) {
 			if ($this->crud->is_valid_to_upload($contentfileeng_field,array($extension),5000000,'text')) {
 				$this->upload_text_file($contentfileeng_field,EPROC_CONTENT_DESC_PATH,$filename."_eng",$extension);
	 			$contentfile_eng = 1;
		 	}else{
			    redirect('news_public/form_create','refresh');
 			}
	 	}
	 	//////////////////////////////////////

	 	/* upload content description file */
	 	/*if ($contentfileind_field || $contentfileeng_field) {
	 		$ctnfile[0]  = $contentfileind_field;
	 		$ctnfile[1]  = $contentfileeng_field;
	 		$path 	  	 = './uploads/contents/';
	 		$extension 	 = 'txt';
	 		$checker 	 = 0;

	 		for ($i = 0;$i<2;$i++) {
	 			if ($this->crud->is_valid_to_upload($ctnfile[$i],array($extension),5000000,'text')) {
	 				$checker++;
			 	}else{
				    redirect('news_public/form_create','refresh');
	 			}	 				
		 	}

		 	if ($checker == 2) {
		 		for ($i=0; $i < 2; $i++) {
			 		if ($i==0) {
			 			$this->upload_text_file($ctnfile[$i],$path,$filename."_ind",$extension);
			 		}else{
			 			$this->upload_text_file($ctnfile[$i],$path,$filename."_eng",$extension);
			 		}
		 		}
		 	}
	 	}*/
	 	/* ---------------- */
	 	
	 	/* upload image file */
	 	if($image_field){
	 		unset($_FILES['imagefile']);unset($_FILES['contentfileind']);unset($_FILES['contentfileeng']);
	 		$path 	   = './uploads/images/';
	 		$extension = array('jpg','png','gif');

	 		if ($this->crud->is_valid_to_upload($image_field,$extension,5000000,'picture')) {
		 		$this->upload_image_file($image_field,$path,$filename,$extension);
	 		}else{
			    redirect('news_public/form_create','refresh');
 			}
	 	}
	 	/* ---------------- */
	 	
	 	/* upload attachment file */
	 	if($attachment_field){
	 		unset($_FILES['imagefile']);unset($_FILES['contentfileind']);unset($_FILES['contentfileeng']);unset($_FILES['pdffile']);
	 		$path 	   = './uploads/attach/';
	 		$extension = 'pdf';

	 		if ($this->crud->is_valid_to_upload($attachment_field,array($extension),5000000,'text')) {
		 		$this->upload_attachment_file($attachment_field,$path,$filename,$extension);
		 	}else{
			    redirect('news_public/form_create','refresh');
 			}
	 	}
	 	/* ---------------- */


	 	/* ---- if english completion ---- */
		///// added by Dedi Rudiyanto ///////
		if( ($content_eng==0 && $contentfile_eng==1) || ($content_eng==1 && $contentfile_eng==0) )
		{$content_exist = 1;}
		$getdata = $this->insertAsEnglishCompletion($data, $_POST['title_eng'], $_POST['detail_eng'], $content_exist);
		//////////////////////////////////

	 	/* inserting/updating data into DB */
	 	$id = $this->crud->insert("","announce",$getdata['data']);
	 	/* ---------------- */

	 	/* inserting content history */
		$level_where = "id_group = ".$this->session->userdata('user_group')." AND id_subcontent = '1'";
		$level_id = $this->crud->browse("",'level',"id_group",$this->session->userdata('user_group'),"false","id",$level_where);
		
	 	$data_history = array('id'	 			=> null, 						
						 	  'id_announcement' => $id,
						 	  'id_level'		=> $level_id->id,
						 	  'status' 			=> '2',
						 	  'created_date'	=> $curr_date);
	 	$this->crud->insert("","announcement_level_approval",$data_history);
	 	/* ---------------- */

	 	////////////// added by dedi rudiyanto /////////////////
	 	if ($getdata['email'] == 'adminasyst') {Send_ContentUpdateToAsyst($id,'update');}
 		else{emailSendContentApproval($id);}
	 	////////////////////////////////////////////////////////
		
	 	/* finalize */
	 	$this->session->set_flashdata('message','1 data successfully inserted');
	 	redirect('announce/','refresh');
	 	/* ---------------- */
	}

	function form_update(){

	 	$id  = $this->uri->segment(3);
	 	$def =  $this->crud->browse("","announce","id",$id,"false");
	 	$data['def'] = $def;
	 	
	 	$data['user_id']  = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
		
		$data['company'] = $this->getCompany();
		$data['areas'] 	 = $this->getAreas();	 	
	 	$data['offices'] = $this->getOffices();
	 	
	 	if($def->file){
	 		$images    = array('jpg','gif','png'); $imgfile = "false";
	 		foreach($images as $imgext){
	 			$imgpath  = "./uploads/images/".$def->file.".".$imgext;
				if(file_exists($imgpath)){	
				$imgfile = $def->file.".".$imgext; 	break;	}
			}
			
			$attpath = "./uploads/attach/".$def->file.".pdf";
			$arrdocreq = explode(',', $document_req);
			if (isset($document_req)) {
				foreach ($arrdocreq as $key => $value) {
					$docreqpath[$key]  = "./uploads/document_requirements/".$def->file."_".$value.".pdf";
					$docreqfile[$key]  = file_exists($docreqpath[$key]) ? "true" : "false";
				}
			}

			$ctpath_id     = "./uploads/contents/".$def->file."_ind.txt";
			$ctpath_eng    = "./uploads/contents/".$def->file."_eng.txt";
			
	 		$attfile 	   = file_exists($attpath) ? "true" : "false";
	 		$contents[0]   = file_exists($ctpath_id) ? file_get_contents($ctpath_id) : "-";
	 		$contents[1]   = file_exists($ctpath_eng) ? file_get_contents($ctpath_eng) : "-";
	 	}

		$data['attfile'] 	= $attfile=="false" ? "-"  : $def->file.".pdf";

		$data['imgfile'] 		= $imgfile=="false" ? "-"  : $imgfile;	 
	 	$data['contents'] 		= !$contents ? "-" : $contents;
	 		
	 	$data['view']	 		= "news_public/form_update";

	 	$this->clear_cache();
	 	$this->load->view('layout/template',$data);
	}
 
	function update(){

		/* initial variable */
		$id 				  = $_POST['id'];
	 	$def   				  = $this->crud->browse("","announce","id",$id,"false");
	 	$filename 			  = !$def->file ? $this->m_announce->file_name("announce") : $def->file;
	 	$curr_date 		   	  = date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
	 	$dept	 		   	  = afterloginGetDeptId();
	 	$description_field 	  = isset($_POST['editor'])?$_POST['editor']:false;
	 	$image_field	   	  = $_FILES['imagefile']["error"] != 4?$_FILES['imagefile']:false;
	 	$contentfileind_field = $_FILES['contentfileind']["error"] != 4?$_FILES['contentfileind']:false;
	 	$contentfileeng_field = $_FILES['contentfileeng']["error"] != 4?$_FILES['contentfileeng']:false;
	 	$attachment_field	  = $_FILES['pdffile']["error"] != 4?$_FILES['pdffile']:false;
	 	$content_exist 		  = 0;
	 	$content_eng 		  = 0;
	 	$contentfile_eng	  = 0;
	 	$extension 	 		  = 'txt';
	 	/* ---------------- */

	 	/* initial data to insert/update DB */
	 	$data 		= array('id_company'	 => !$_POST['id_company'] 	? $def->id_company : $_POST['id_company'],
	 						'id_region'	  	 => !$_POST['id_area'] 		? $def->id_region : $_POST['id_area'],
						 	'id_office'		 => !$_POST['id_office']  	? $def->id_office : $_POST['id_office'],
						 	'title_ind'		 => !$_POST['title_ind']    ? $def->title_ind : $_POST['title_ind'],
						 	'title_eng'		 => !$_POST['title_eng']    ? $def->title_eng : $_POST['title_eng'],
						 	'detail_eng'	 => !$_POST['detail_eng']   ? $def->detail_eng : $_POST['detail_eng'],
						 	'detail_ind'	 => !$_POST['detail_ind']   ? $def->detail_ind : $_POST['detail_ind'],
						 	'publish_date'	 => !$_POST['publish_date'] ? $def->publish_date : $_POST['publish_date'],
						 	'creator'		 => !$_POST['creator']      ? $def->creator : $_POST['creator'],
						 	'file'			 => $filename,
						 	'code'			 => null,
						 	'status'		 => '2',
						 	'created_id'	 => $userID,
						 	'created_date'	 => $curr_date,
						 	'code'			 => $reg_num,
						 	'last_updated'	 => $curr_date);
		/* ---------------- */

		/* initial data attachment history */
	 	$data_attach = array('filename' => $filename);
		/* ---------------- */
		
		/* making a file and uploaded if content description inserted by WYSWYG */
		if($description_field){
	 		$path 	 = "/uploads/contents/";

	 		for ($i = 0;$i<2;$i++) {
		 		if ($i==0) {
		 			$this->upload_text_into_file($description_field[$i],'',$path,$filename."_ind",'txt');
		 		}else{
		 			$this->upload_text_into_file($description_field[$i],'',$path,$filename."_eng",'txt');
		 			// add by dedi rudiyanto //
		 			if($description_field[$i]!='' && !empty($description_field[$i]))
		 			{
		 				$content_eng = 1;
		 			}
		 			//////////////////////////
		 		}
		 	}
	 	}
	 	/* ---------------- */

	 	///////////// modifed by dedi rudiyanto /////////////
	 	/* upload content description file */

	 	if ($contentfileind_field) {
 			if ($this->crud->is_valid_to_upload($contentfileind_field,array($extension),5000000,'text')) {
 				$this->upload_text_file($contentfileind_field,EPROC_CONTENT_DESC_PATH,$filename."_ind",$extension);
		 	}else{
			    redirect('news_public/form_update'.$id,'refresh');
 			}	 				
	 	}

	 	if ($contentfileeng_field) {
 			if ($this->crud->is_valid_to_upload($contentfileeng_field,array($extension),5000000,'text')) {
 				$this->upload_text_file($contentfileeng_field,EPROC_CONTENT_DESC_PATH,$filename."_eng",$extension);
	 			$contentfile_eng = 1;
		 	}else{
			    redirect('news_public/form_update'.$id,'refresh');
 			}
	 	}
	 	//////////////////////////

	 	/* upload content description file */
	 	/*if ($contentfileind_field || $contentfileeng_field) {
	 		$ctnfile[0]  = $contentfileind_field;
	 		$ctnfile[1]  = $contentfileeng_field;
	 		$path 	  	 = './uploads/contents/';
	 		$extension 	 = 'txt';
	 		$checker 	 = 0;

	 		for ($i = 0;$i<2;$i++) {
	 			if ($this->crud->is_valid_to_upload($ctnfile[$i],array($extension),5000000,'text')) {
	 				$checker++;
			 	}else{
				    redirect('news_public/form_update/'.$id,'refresh');
	 			}	 				
		 	}

		 	if ($checker == 2) {
		 		for ($i=0; $i < 2; $i++) {
			 		if ($i==0) {
			 			$this->upload_text_file($ctnfile[$i],$path,$filename."_ind",$extension);
			 		}else{
			 			$this->upload_text_file($ctnfile[$i],$path,$filename."_eng",$extension);
			 		}
		 		}
		 	}
	 	}*/
	 	/* ---------------- */
	 	
	 	/* upload image file */
	 	if($image_field){
	 		unset($_FILES['imagefile']);unset($_FILES['contentfileind']);unset($_FILES['contentfileeng']);
	 		$path 	   = './uploads/images/';
	 		$extension = array('jpg','png','gif');

	 		if ($this->crud->is_valid_to_upload($image_field,$extension,5000000,'picture')) {
		 		$this->upload_image_file($image_field,$path,$filename,$extension);
	 		}else{
			    redirect('news_public/form_update/'.$id,'refresh');
 			}
	 	}
	 	/* ---------------- */
	 	
	 	/* upload attachment file */
	 	if($attachment_field){
	 		unset($_FILES['imagefile']);unset($_FILES['contentfileind']);unset($_FILES['contentfileeng']);unset($_FILES['pdffile']);
	 		$path 	   = './uploads/attach/';
	 		$extension = 'pdf';

	 		if ($this->crud->is_valid_to_upload($attachment_field,array($extension),5000000,'text')) {
		 		$this->upload_attachment_file($attachment_field,$path,$filename,$extension);
		 	}else{
			    redirect('news_public/form_update/'.$id,'refresh');
 			}
	 	}
	 	/* ---------------- */

	 	/* ---- if english completion ---- */
		///// added by Dedi Rudiyanto ///////
		if( ($content_eng==0 && $contentfile_eng==1) || ($content_eng==1 && $contentfile_eng==0) )
		{$content_exist = 1;}
		$getdata = $this->insertAsEnglishCompletion($data, $_POST['title_eng'], $_POST['detail_eng'], $content_exist);
		//////////////////////////////////

		/* inserting/updating data into DB */
	 	$this->crud->update("","announce","id",$_POST['id'],$getdata['data']);
	 	/* ---------------- */

	 	/* Send email to vendor */
		////////////// added by dedi rudiyanto /////////////////
	 	if ($getdata['email'] == 'adminasyst') {Send_ContentUpdateToAsyst($_POST['id'],'update');}
 		else{emailSendContentApproval($_POST['id']);}
	 	/* ---------------- */

	 	/* finalize */
	 	$this->session->set_flashdata('message','1 data successfully updated');
	 	redirect('announce/','refresh');
	 	/* ---------------- */
	}

	/////////////////////////////////

	private function getAllSubcontentTypes()
	{
		/* get categories data from database */

		$getData = array();

		$iterations = $this->crud->browse("","subcontent_type","","","true","id,subtype_name");

		if(!$iterations) $iterations = array();
		foreach($iterations as $val){ $iteration[$val->id] = $val->subtype_name; } 
		$getData = $iteration;

		/* ===== */

		return $getData;
	}

	////// * Required Methods * //////

	function get_office(){
	 	$id_area = $_POST['id_area']; //echo "=>".$id_comp;
	 	$select  = " id,office_name";
	 	$getDatas = $this->crud->browse("","offices l","id_region",$id_area,"true",$select); 	
		echo "<option selected='selected' value=''>-- Select Office --</option>";
		foreach ($getDatas as $row){
			echo "<option value='$row->id'>$row->office_name</option>";	
		}
	}

	function get_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'code','sort'=>'ASC'); $select = "code as name";
		$result = $this->crud->autocomplete("","announce",$select,"code",$term,"",$order);
		echo json_encode($result);
	}

	function getCompany(){
	 	
	 	$select2 	 = "id,code,company_name as name";

	 	$getDatas = $this->crud->browse("","company l","","","true",$select2); 
	 	if(!$getDatas) $getDatas = array();
	 	foreach($getDatas as $val){ $options[$val->id] = $val->name; } 

	 	$getData = $options;

	 	return $getData;
	}
 
	function set_browse_session(){
		$code     = $_POST['search'];
		$announce = $this->crud->browse("","announce","code",$code,"false","id");
		if($announce) $this->session->set_flashdata('anID',$announce->id);
	}

	/////////////////////////////////


	////// * Utility Methods * //////

	private function getOffices(){
	 	$options[""] = "-- Select Office --";
	 	$select2 	 = "id,office_name,(SELECT region_name FROM region r WHERE r.id=l.id_region) as region";

	 	$offices = $this->crud->browse("","offices l","id_region","","true",$select2); 
	 	if(!$offices) $offices = array();
	 	foreach($offices as $val){ $options[$val->id] = $val->office_name." / ".$val->region; } 

	 	$getData = $options;

	 	return $getData;
	}

	private function getAreas(){
	 	$options[""] = "-- Select Area --";
	 	$select2 	 = "id,region_name as name";

	 	$getDatas = $this->crud->browse("","region l","","","true",$select2); 
	 	if(!$getDatas) $getDatas = array();
	 	foreach($getDatas as $val){ $options[$val->id] = $val->name; } 

	 	$getData = $options;	 		

	 	return $getData;
	}

	private function upload_text_into_file($textfield = '',$root = '',$path = '',$filename = '',$extension = ''){
	 	if ($root == '') {
	 		$root = $_SERVER['DOCUMENT_ROOT'];
	 	}
	 	$pathfixed = $root.SRC_PATH.$path;

	 	$myfile = fopen($pathfixed.$filename.".".$extension, "w");
 		fwrite($myfile,$textfield);
	}

	private function upload_image_file($fileresource = '',$path = '',$filename = '',$extension = ''){
 		$_FILES['userfile'] = $fileresource;
 		$array = explode('.', $fileresource['name']);
		$extension = end($array);
 		$config = $this->m_announce->set_config($filename.".".$extension,$path,'jpg|gif|png');
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config); 			
 		 		 		
 		$msg['error_'.$extension] = "";  $msg['success_'.$extension] = array();
		if ( ! $this->upload->do_upload()){
			$msg['error_'.$extension] = $this->upload->display_errors();
		} else {
			$msg['success_'.$extension] = $this->upload->data(); }
	}

	private function upload_attachment_file($fileresource = '',$path = '',$filename = '',$extension = ''){
 		$_FILES['userfile'] = $fileresource;
 		$config = $this->m_announce->set_config($filename.".".$extension,$path,'pdf');
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config);
 		 		 		
 		$msg['error_'.$extension] = "";  $msg['success_'.$extension] = array();		
		if ( ! $this->upload->do_upload()){
			$msg['error_'.$extension] = $this->upload->display_errors();
		} else {
			$msg['success_'.$extension] = $this->upload->data(); }
	}

	private function upload_text_file($fileresource = '',$path = '',$filename = '',$extension = ''){
 		$_FILES['userfile'] = $fileresource;
 		$config = $this->m_announce->set_config($filename.".".$extension,$path,'txt');
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config);
 		 		 		
 		$msg['error_'.$extension] = "";  $msg['success_'.$extension] = array();		
		if ( ! $this->upload->do_upload()){
			$msg['error_'.$extension] = $this->upload->display_errors();
		} else {
			$msg['success_'.$extension] = $this->upload->data(); }
	}

	private function insertAsEnglishCompletion($data = array(),$title_eng = '',$detail_eng = '',$content_exist = ''){
		if( $title_eng=='' || $detail_eng=='' || $content_exist == 0 )
		{
			$data['status']   = '10';
			$getdata['email'] = 'adminasyst';
		}else{
			$getdata['email'] = 'sm';
		}

		$getdata['data'] = $data;

		return $getdata;
	}

	private function clear_cache(){
		header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
	}

	/////////////////////////////////
 
}
?>