<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    
    <title>e-Procurement Garuda Indonesia</title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets_public/css/bootstrap.css" media="screen"> 
    <link href="<?php echo base_url()?>assets_public/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets_public/css/blue.css" id="style-switch" />
    <link href="<?php echo base_url()?>assets_public/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url()?>assets_public/css/layout-rtl.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>assets_public/css/blue-rtl.min.css" rel="stylesheet" type="text/css" id="style_color" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets_public/plugins/rs-plugin/css/settings.min.css" media="screen" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets_public/css/inline.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets_/css/sol.css"/>
    <link rel="icon" type="image/png" href="<?php echo base_url()?>assets_public/images/favicon-grd.png">

    <script type="text/javascript" src="<?php echo base_url()?>assets_public/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_/js/sol.js"></script>
</head>

<body class="page-header-fixed2 page-container-bg-solid2">
    <!-- <div id="loader-overlay"><img src="<?php echo base_url()?>assets/images/plane.gif" alt="Loading" /></div> // PESAWAT BOLAK BALIK GIF-->
    <div class="page-header2 navbar navbar-fixed-top2">
        <?php $this->load->view('main/navbar_top');?>
    </div>
    <div class="clearfix"> </div>
    <div class="page-container">
        <div class="page-sidebar-wrapper">
        <?php $this->load->view('main/sidebar_nav');?>
        </div>
        <div class="page-content-wrapper">
            <div class="page-content">
                <header>
                    <div class="header-bg">
                        <div id="search-overlay">
                            <div class="container2">
                              <div id="close">X</div>
                              <input id="hidden-search" type="text" placeholder="Start Typing..." autofocus autocomplete="off"  />
                              <input id="display-search" type="text" placeholder="Start Typing..." autofocus autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </header>