<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Assesment extends Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_user');
		islogged_in();
 		//$this->m_user->authenticate(array(41));
	}

	function index() 
	{

		$data ['user_id'] = $this->tank_auth->get_user_id ();
		$data ['username'] = $this->tank_auth->get_username ();


		$table = "assesment";
		$page = $this->uri->segment (3);
		$per_page = 10;
		$offset = $this->crud->set_offset($page, $per_page);
		$total_rows = $this->crud->get_total_record ("",$table);

		$set_config = array (
				'base_url' => base_url() . '/assesment/index',
				'total_rows' => $total_rows,
				'per_page' => $per_page,
				'uri_segment' => 3
		);

		$config = $this->crud->set_config($set_config);

		$this->load->library ( 'pagination' );
		$this->pagination->initialize ( $config );
		$paging = $this->pagination->create_links ();

		$order = array (
				'field' => 'created_date',
				'order' => 'DESC'
		);

		$data ['pagination'] = $paging;
		$data ['num'] = $offset;
		$select = "id,status,description,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";

		$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
		$data['browse'] = $browse;
		if($this->input->is_ajax_request())
		{
				$this->load->view('assesment/list',$data);
		}
		else
		{

 		$data['view']	= "assesment/browse";
			$this->load->view('layout/template',$data);
		}
	}

	private function search_input($search_dateranges = array(),$search_conditions = array()){

		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');

			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}

		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');

			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}

		$getData = array($search_dateranges,$search_conditions);

		return $getData;
	}


	function search(){

		/* initiate search inputs */

		//$search_dateranges = array('publish_date',$_POST['start_date'],$_POST['end_date']);
		$search_conditions = array(
				'description'		=> $_POST['search_term'],
		);

		$where_conditions  = $this->search_input("",$search_conditions);

		/* ==== */

		/* get data from defined function for table view */

		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);

		/* ==== */

		/* preparing data for display */

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];

		$data['view'] = "assesment/browse";

		$this->load->view('layout/template',$data);

		/* ==== */

	}

	private function getDataTables($limit = NULL)
	{

		/* get data for table view from database with pagination */

		$table 	     = "dd_assesment t1";
		$select 	 = "t1.id,status,description,(SELECT username FROM users WHERE id=created_id)as created_id,(SELECT u.subcategory FROM subcategory u WHERE u.id=id_subcat)as subcat,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);

		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);


		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'assesment/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);

		/** setup for pagination **/

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();

		/** ===== **/
		/* ===== */

		/* setup variable to used in another functions */

		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;

		/* ===== */

		return $data;

	}

	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{

		/* get data for table view from database with pagination */

		$table 	     = "assesment t1";
		$select 	 = "t1.id,status,description,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);

		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);

		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,$where,$dateranges,$wherearray,"",$per_page,$offset,"t1.id");

		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'assesment/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);

		/** setup for pagination **/

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();

		/** ===== **/
		/* ===== */

		/* setup variable to used in another functions */

		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;

		/* ===== */

		return $data;

	}

	

	
	function form_create() 
	{
		$data ['user_id'] = $this->tank_auth->get_user_id ();
		$data ['username'] = $this->tank_auth->get_username ();
		$this->load->view('assesment/form_create',$data);
	}

	function create()
	{
		$data = array(
				'description'		=> $_POST['description'],
				'status'			=> '1',
				'created_id'		=> $this->tank_auth->get_user_id(),
				'created_date'  	=> date('Y-m-d H:i:s'),
				'last_updated'  	=> date('Y-m-d H:i:s'));

		
			$msg = array('state'=>'true', 'msg'=>'data saved');
			$this->crud->insert("","assesment",$data);
 			$this->session->set_flashdata('message','1 data success insert');
		

		echo json_encode($msg);
		
	}

	function form_update()
	{
		$id   = $this->input->post('id');
		$select = "id,description,created_id,created_date,last_updated";
		$data['def'] = $this->crud->browse ( "", "assesment", "id", $id, "false", $select);
		$this->load->view ( 'assesment/form_update', $data );
	}

	function update()
	{
		$id  		 = $_POST['id'];
		$curr_date 	= date('Y-m-d H:i:s');
		$data = array(
					'description'	=> $_POST['description'],
					'status'		=> '1',
					'created_id'	=> $this->tank_auth->get_user_id(),
					'created_date'  => date('Y-m-d H:i:s'),
					'last_updated'  => date('Y-m-d H:i:s'));

		
		$msg = array('state'=>'true', 'msg'=>'data saved');
		$this->crud->update("","assesment",'id', $id, $data);
		$this->session->set_flashdata('message','1 data success update');

		echo json_encode($msg);

	}

	function delete()
	{
		$id = $this->uri->segment(3);
		$status = array('status'=>'0');
		$this->crud->update("","assesment","id",$id,$status);
		redirect('assesment/','refresh');
	}

    

	
}
