<html lang="en-US">
<head>
  <title>E-Procurement Garuda</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/style.css">
  <link rel="icon" href="<?php echo base_url(); ?>assets/favicon-grd.png" sizes="16x16">
</head>
<style type="text/css">
  .avl-landing{padding: 100px 0;}
  #green-header{border-top: #00acac solid 8px;}
  #yellow-header{border-top: #B64C2E solid 8px;}
  .nounderline{text-decoration: none}
  .success-logo{
    height: 70px;
    width: 70px;
    position: absolute;
    border-radius: 50%;
    background: #00acac;
    z-index: 1;
    top: -32px;
    margin: 0 auto;
    left: 0;
    right: 0;
  }
  .warning-logo{
    height: 70px;
    width: 70px;
    position: absolute;
    border-radius: 50%;
    background: #B64C2E;
    z-index: 1;
    top: -32px;
    margin: 0 auto;
    left: 0;
    right: 0;
  }
  .borderless{
    border: 0 !important;
  }
  .terms-text ul{
    list-style: none;
    text-align: left;
  }
</style>
<body class="hold-transition login-bgcolor" style="height:100%;">
  <div class="avl-landing" style="height:90%;">
    <div class="col-md-12">
      <div class="col-md-12 text-center">
        <span class="<?php echo $class;?>"><i class="<?php echo $glyphicon;?>" style="font-size:40px;color:#fff;top:22%;vertical-align: middle;"></i></span>
      </div>
      <div style="padding-bottom:20px;margin-bottom:80px;" class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 box box-info text-center" id = "<?php echo $border;?>">
          <div class="fade in active" id="tab-home">
            <div class = "row">
              <div class="col-md-12 tab-content" style="margin-bottom:0;">
                <div class="box2 box-info2 tab-pane fade in active borderless" id="registrasi-step-1" style="padding: 28px 15px 0;">
                  <div class="box-header with-border">
                    <h1 class="box-title" style="margin: 40px 0 0;"><strong style="color: #368ccc;"><?php echo $title;?></strong></h1>
                    <h2><?php echo $subtitle;?></h2>
                  </div>
                  <hr/>
                  <p style="margin-bottom: 10px;">Please go to dashboard to track your current bidding</p>
                  <span style="display:inline-block">
                    <a href="<?php echo base_url();?>avl/dashboard" class="btn btn-primary nounderline">Go to Dashboard</a>
                    <span style="margin: 0px 10px 0 5px">or</span>
                    <a href="<?php echo base_url();?>auth/logout" class="btn btn-danger nounderline">Log Out</a>
                  </span>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
    <footer class="main-footer footer-line container-fluid text-center" style="height:10%;bottom:0;"> 
      Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
    </footer>
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  </body>
  </html>