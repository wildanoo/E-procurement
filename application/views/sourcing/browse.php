<script type="text/javascript"> 
	$(document).ready(function(){	
		var baseUrl   = "<?php echo base_url(); ?>";	
		$(function(){	});	
		
		$('#id_cat').change(function(){ 
			var id_cat = $('#id_cat').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'sourcing/set_sess_category',			   
               data : 'id_cat='+id_cat,
               success: function(data){ 				 	
				 	document.location.href= baseUrl + 'sourcing/';
			   }		   
            });	
		
		});

		$("#srch-term").autocomplete({
			source: baseUrl + 'sourcing/get_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'sourcing/set_browse_session',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){                		            
				    document.location.href= baseUrl + 'sourcing/';
               }
            });
        });  

	});			
</script>

<div id="debug"></div>

<?php 	$msg = $this->session->flashdata('message');
		if(strpos($msg,'failed')) $color = "#bc0505";
		else $color = "#487952"; 		
	 	 ?>	
<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>

<div class="page-header">
  <h3>Sourcing Management</h3>
</div>
<!-- Search Term -->
<div class="table">
	<div class="panel panel-default">
	    <div class="panel-heading">
	        <i class="fa fa-search"></i> <strong>Search Section</strong>
	    </div>
	    <style type="text/css">
	    .list-inline li{
	    	vertical-align: middle !important;
	    }
	    .btn-srch{
	    	margin-bottom: 0;
	    }
	    </style>
	    <?php echo form_open_multipart('sourcing/search',array('id'=>'form-search','novalidate' => 'true'));	//$current_date = date('Y/m/d')?>
	    <div class="panel-body">
	    	<ul class="list-inline">
	    		<li style="width:24%">
	    			<div class="input-group">
	    			<input type="text" value="" placeholder="date start" class="form-control date_picker" id="start_date" name="start_date">
	    			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	    			</div>
	    		</li>
	    		<li style="width:2%">to</li>
	    		<li style="width:24%">
	    			<div class="input-group">
	    			<input type="text" value="" placeholder="date end" class="form-control date_picker" id="end_date" name="end_date">
	    			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	    			</div>
	    		</li>
	    		<li style="width:34%"><div class="input-group" style="width:100%"><input placeholder="search by code or title..." type="text" value="" class="form-control" id="search_term" name="search_term"></div></li>
	    		<li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search">Search</button></div></li>
	    		<li style="width:7%"><div class="input-group"><a href="<?php echo base_url()?>sourcing" class="btn btn-srch btn-warning btn-sm">Show All</a></div></li>
	    	</ul>
	    </div>
	    <?php echo form_close()?>
	    <!-- <div class="panel-footer text-center">
	        <a href="#" class="btn btn-primary" style="margin-bottom:0;">Change Password</a>
	    </div> -->
	</div>
</div>
<hr/>
<!-- Search Term -->

<?php if ($this->permit->cc) { ?>
<div class="pull-right">
	<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>'Create'); ?>
	<?php  echo anchor('open_sourcing/form_create/','Create',$prop); ?>
</div>
<?php }?>

<?php
	$sess_cat = $this->session->userdata('sess_cat');
	$id_cat   = !$sess_cat ? 0 : $sess_cat;	            
	$prop_cat = 'id=id_cat  style="width: 120px; height: 25px; font-size: 13px"'; ?>
	<?php echo form_dropdown('id_cat', $category, $id_cat,$prop_cat); ?>

<table id="browse" class="table table-striped table-bordered" style="font-size: 13px">
	<thead>
        <tr>
            <th>No</th>
            <th>Code</th>
            <th>Title</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Published Date</th>
            <th>Category</th>
            <th>Subcategory</th>
            <th>Type of Invitation</th>
            <th>Status</th>
        </tr>
	</thead>
    <tbody>
<?php			
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
		<tr>
			<td><?php echo $num = $num+1; ?></td>
			<td>
				<?php
				$pencil = '<i class="fa fa-pencil"></i>';
				echo anchor('sourcing/detail/'.$row->id, $row->code,array('class'=>'default_link','title'=>'Edit Announcement')); 
				?>
			</td>
			<td>
				<?php echo $row->title_ind ? $row->title_ind.' <strong>(Indonesia)</strong>':'- <strong>(Indonesia)</strong>'; ?>
				<br>
				<?php echo $row->title_eng ? $row->title_eng.' <strong>(English)</strong>':'- <strong>(English)</strong>'; ?>
			</td>
			<td><?php echo date('M d, Y',strtotime($row->first_valdate)); ?></td>
			<td><?php echo date('M d, Y',strtotime($row->end_valdate)); ?></td>
			<td><?php echo date('M d, Y',strtotime($row->publish_date)); ?></td>
			<td><?php echo array_key_exists($row->id_cat, $category)?$category[$row->id_cat]:'-'; ?></td>
			<td><?php echo array_key_exists($row->id_subcat, $subcategory)?$category[$row->id_subcat]:'-'; ?></td>
			<td><?php echo array_key_exists($row->subcontent_type, $subcontent_type)?$subcontent_type[$row->subcontent_type]:'-'; ?></td>
			<?php
			if (date('Y-m-d H:i:s') < $row->publish_date){$status = 0;}
			elseif (date('Y-m-d H:i:s') >= $row->publish_date && date('Y-m-d H:i:s') < $row->first_valdate){$status = 1;}
			elseif (date('Y-m-d H:i:s') >= $row->first_valdate && date('Y-m-d H:i:s') <= $row->end_valdate){$status = 2;}
			elseif ($row->status == 7){$status = 4;}
			elseif ($row->status == 8){$status = 99;}
			else {$status = 3;}
			?>
			<td><?php echo array_key_exists($status, $status_ref)?$status_ref[$status]:'-'; ?></td>
		</tr>
<?php	$i++;  } ?>	
<tr><td colspan="10" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?><tr><td colspan="10">-</td></tr><?php } ?>
    </tbody>
</table>
