<?php

$tanggal_visit = indo_date($vendor_visit->visit_date);

$no=1;
//url logo
if(base_url() == '/')
{
	$base = "http://$_SERVER[HTTP_HOST]";
	$url_logo = $base.'/assets/imagesv2/logo.png';
}
else
{
	$url_logo = base_url('assets/imagesv2/logo.png');
}

//data notes
if(!empty($notes))
{
	foreach ($notes as $val_note)
	{
		$cont_notes .= "<tr><td>$no</td><td>$val_note->notes</td></tr>";
		$no++;
	}
}
else
{
	$cont_notes ='<tr><td style="text-align:center" colspan="2"><b>Belum ada catatan</b></td></tr>';
}
//data participant
$no=1;
if(!empty($participant))
{
	foreach ($participant as $val_part)
	{
		$cont_part .= "<tr><td>$no</td><td>$val_part->visitor_name</td><td>$val_part->unit</td><td></td></tr>";
		$no++;
	}
}
else
{
	$cont_part ='<tr><td style="text-align:center" colspan="4"><b>Belum ada participant</b></td></tr>';

}
//data docs filter cuma gambar
$no=1;
foreach ($visit_docs as $val_docs)
{
	$cont_docs .= '<tr>';
	if($val_docs->filetype == 'jpg' || $val_docs->filetype == 'jpeg' || $val_docs->filetype == 'png')
	{
		if(file_exists('uploads/due_dilligence/'.$val_docs->id.'.'.$val_docs->filetype))
		{

		$cont_docs .= "<td><img style='height:300px;' src=' ".base_url('uploads/due_dilligence').'/'.$val_docs->id.'.'.$val_docs->filetype." '> <br><br> $val_docs->doc_name  </td>";
		}
	}

	$cont_docs .= '</tr>';
	$no++;

}
//data category
$no=1;
foreach ($categories as $val_cats)
{
	$cont_cats .= "$no $val_cats->category - $val_cats->subcategory <br>";
	$no++;
}
//data assessment
$limit = $limit1; //jumlah data asesment
$no=1;
$nem=1;
$limit2 = $limit2;//jumlah subcat
foreach ($assessment as $value)
{
	$cont_assess .= '<h5>Kategori : '.$value['category'].' - '.$value['subcategory'].'</h5><br>';
	$cont_assess .= '<table border="2" style="border-collapse:collapse;border:2px solid #000;"><tr><td>No</td><td>Deskripsi</td><td>Keterangan</td><td>Catatan</td></tr>';

	foreach ($assessment['data1'] as $dt)
	{

		foreach ($dt as $val)
		{

			if($val['id_subcat'] == $value['id_subcat'])
			{
				$result = ($val['result'] == '1') ? 'Ada' : 'Tidak Ada';
				$cont_assess .= '<tr><td>'.$no.'</td><td style="width:300px;">'.$val['description'].'</td><td>'.$result.'</td><td style="width:200px;">'.$val['remark'].'</td></tr>';
				$no++;
			}


		}



		$no=1;

	}


	$cont_assess .= '</table>';
	$numb=1;
	$cont_assess .= '<h5>Catatan : Kategori '.$value['category'].' - '.$value['subcategory'].'</h5><br>';
	$cont_assess .= '<table border="2" style="border-collapse:collapse;border:2px solid #000;"><tr><td>No</td><td>Catatan</td></tr>';


	foreach ($assessment['data2'] as $dt2)
	{
		foreach ($dt2 as $val2)
		{


			if($val2['id_subcat'] == $value['id_subcat'])
			{

				$cont_assess .= '<tr><td>'.$numb.'</td><td style="width:580px;">'.$val2['notes'].'</td></tr>';
				$numb++;

			}

		}

	}


	$cont_assess .= '</table>';

	$cont_assess .= '<h5>Kesimpulan : Kategori '.$value['category'].' - '.$value['subcategory'].'</h5><br>';

	foreach ($assessment['data3'] as $dt3)
	{
		foreach ($dt3 as $val3)
		{
			if($val3['id_subcat'] == $value['id_subcat'])
			{
				$cont_assess .= '<p>'.$val3['resume'].'</p>';

			}

		}
	}

if ($nem >= $limit2) 
{
	break;
}


$nem++;

}

//page 1
$content .= '<page>';
$content .= '<style type="text/css">
			<!--
			page {font-size:12px;}
			table { vertical-align: top; }
			tr    { vertical-align: top; }
			td    { vertical-align: top; }
			}
			-->
			</style>
			';
$content .= '';
$content .=
'


<p style="text-align:Center;font-weight:bold;">

<img src="'.$url_logo.'" style="margin:0 auto;" >
<br><br>
BERITA ACARA <br/>
PELAKSANAAN DUE DILIGENCE <br/>
PT. GARUDA INDONESIA (PERSERO) Tbk <br/>
</p>
<hr>
<br>
<p>
Sehubungan dengan kebutuhan penyedia barang dan jasa, dimana calon rekanan yang belum
terdaftar sebagai Approved Vendor List (AVL) PT Garuda Indonesia (Persero) Tbk.
("Garuda") telah menyampaikan dokumen administrasi, Garuda telah melaksanakan due diligence
terhadap perusahaan calon rekanan pada tanggal '.$tanggal_visit.' <br>
Due diligence dilakukan dengan cara kunjungan (site visit), wawancara dan konfirmasi
sebagai pendukung/pelengkap dokumen persyaratan yang disampaikan/dikirim oleh perusahaan calon AVL tersebut. <br>
Adapun informasi perusahaan adalah sebagai berikut: <br>
</p>
<table>
	<tr>
		<td colspan="4"><b>Informasi Perusahaan :</b></td>
	</tr>
	<tr>
		<td>1</td>
		<td>Nama</td>
		<td>:</td>
		<td>'.$vendor->name.'</td>
	</tr>
	<tr>
		<td>2</td>
		<td>PIC</td>
		<td>:</td>
		<td>'.$vendor_visit->pic.'</td>
	</tr>
	<tr>
		<td>3</td>
		<td>Jenis</td>
		<td>:</td>
		<td>'.$vendor_visit->office_type.'</td>
	</tr>
	<tr>
		<td>4</td>
		<td>Alamat Perusahaan</td>
		<td>:</td>
		<td>'.$vendor_visit->address.'</td>
	</tr>
	<tr>
		<td>5</td>
		<td>No. Telp</td>
		<td>:</td>
		<td>'.$vendor_visit->telp.'</td>
	</tr>
	<tr>
		<td>6</td>
		<td>No.Fax</td>
		<td>:</td>
		<td>'.$vendor_visit->fax.'</td>
	</tr>
	<tr>
		<td>7</td>
		<td>Waktu Kedatangan</td>
		<td>:</td>
		<td>'.$tanggal_visit.' Jam '.$vendor_visit->visit_time.'</td>
	</tr>
	<tr>
		<td>8</td>
		<td>Dalam Rangka</td>
		<td>:</td>
		<td>'.$vendor_visit->remark.'</td>
	</tr>
</table>
<br>
Kategori yang dilakukan kunjungan meliputi: <br><br>
'.$cont_cats.' ';
$content .= '</page>';
//page 2
$content .= '<page backtop="30mm" backbottom="10mm">';

$content .= '<page_header><p style="text-align:Center;font-weight:bold;"><img src="'.$url_logo.'" style="margin:0 auto;" ></p></page_header>';
$content .= '<p style="text-align:Center;font-weight:bold;">RINCIAN DUE DILIGENCE</p>';
$content .= $cont_assess;
$content .= '</page>';
//page 3
$content .= '<page>';
$content .= '<p style="text-align:Center;font-weight:bold;"><img src="'.$url_logo.'" style="margin:0 auto;" ><br><br></p>
<b>Catatan Site Visit:</b> <br><br>
<table border="1" style="border-collapse:collapse;border:2px solid #000;">
	<tr>
		<td>No</td>
		<td style="width:550px;">Catatan</td>
	</tr>
	'.$cont_notes.'
</table>
<br>
<b>Keputusan Site Visit:</b>
<p>
'.$summary->summary.'
</p>
<br>
<b>Tim Due Diligence:</b> <br><br>
<table border="1" style="border-collapse:collapse;border:2px solid #000;">
	<tr>
		<td>No</td>
		<td style="width:230px;">Nama</td>
		<td style="width:200px;">Unit</td>
		<td style="width:100px;">Tanda Tangan</td>
	</tr>
	'.$cont_part.'
</table>
';
$content .= '</page>';
//page 4
$content .= '<page>';
$content .= '<p style="text-align:Center;font-weight:bold;"><img src="'.$url_logo.'" style="margin:0 auto;" ><br><br>
<b>Lampiran Due Diligence</b></p>
<b>'.$vendor->name.'</b><br><br>
<table border="2" style="border-collapse:collapse;border:2px solid #000;">
'.$cont_docs.'
</table>';
$content .= '</page>';

//load library html2pdf
require_once APPPATH.'./libraries/html2pdf/html2pdf.class.php';
//config pdf
$html2pdf = new HTML2PDF('P','A4','en', true, 'UTF-8', array(30, 10, 30, 10));
$namefile = 'lampiran_berita_acara_'.$vendor->name.'_'.$vendor_visit->visit_date.$vendor_visit->visit_time;
$namefile = str_replace(array(' ', ':'), '_', $namefile);
$html2pdf->pdf->SetTitle("{$namefile}");

$html2pdf->WriteHTML($content);

if(!empty($is_saved))
{
	$html2pdf->Output("due_diligence_attachment/$namefile.pdf", 'F');
}
else
{
	$html2pdf->Output("$namefile.pdf");
}





 ?>
