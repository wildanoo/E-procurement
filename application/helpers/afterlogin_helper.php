<?php
if(!function_exists('afterloginGetDeptId')) {
	function afterloginGetDeptId() {

		$CI = get_instance();
		$CI->load->model('crud');

		$id_dept = $CI->crud->browse("","ga_employee","userID",$CI->tank_auth->get_user_id(),"false","id_dept");

		return $id_dept;

	}
}
?>