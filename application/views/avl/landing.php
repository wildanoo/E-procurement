<html lang="en-US">
<head>
  <title>E-Procurement Garuda</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/style.css">
  <link rel="icon" href="<?php echo base_url(); ?>assets/favicon-grd.png" sizes="16x16">
</head>
<style type="text/css">
  .avl-landing{padding: 100px 0;}
  #green-header{border-top: #00acac solid 8px;}
  #yellow-header{border-top: #B64C2E solid 8px;}
  .nounderline{text-decoration: none}
  .success-logo{
    height: 70px;
    width: 70px;
    position: absolute;
    border-radius: 50%;
    background: #00acac;
    z-index: 1;
    top: -32px;
    margin: 0 auto;
    left: 0;
    right: 0;
  }
  .warning-logo{
    height: 70px;
    width: 70px;
    position: absolute;
    border-radius: 50%;
    background: #B64C2E;
    z-index: 1;
    top: -32px;
    margin: 0 auto;
    left: 0;
    right: 0;
  }
  .borderless{
    border: 0 !important;
  }
  .terms-text ul{
    list-style: none;
    text-align: left;
  }
</style>
<body class="hold-transition login-bgcolor" style="height:100%;">
  <div class="avl-landing" style="height:90%;">
    <div class="col-md-12">
      <div class="col-md-12 text-center">
        <span class="<?php echo $class;?>"><i class="<?php echo $glyphicon;?>" style="font-size:40px;color:#fff;top:22%;vertical-align: middle;"></i></span>
      </div>
      <div style="padding-bottom:20px;margin-bottom:80px;" class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 box box-info text-center" id = "<?php echo $border;?>">
        <?php if ($checker) { ?>
          <div class="fade in active" id="tab-home">
            <div class = "row">
              <div class="col-md-12 tab-content" style="margin-bottom:0;">
                <div class="box2 box-info2 tab-pane fade in active borderless" id="registrasi-step-1" style="padding: 28px 15px 0;">
                    <div class="tab-pane borderless" style="padding-bottom: 0;padding: 28px 15px 0;">
                      <h1><strong>OPEN BIDDING REGISTRATION</strong></h1>
                      <h2 style="font-weight: 300;">- Vendor Policy -</h2>
                      <hr>
                      <h4><strong>Open Bidding</strong></h4>
                      <?php
                      $root = $_SERVER['DOCUMENT_ROOT']; $path = $root."/uploads/policy_agreement/open_bidding_eng.txt";
                      if( file_exists($path)) {   echo file_get_contents($path);          
                      } else { echo ""; }
                      ?>
                      <div class="col-md-12 text-center">
                        <?php                                               
                        $dwld_ico = '<img src="'.base_url().'assets/images/download.png" width="30" height="25"/>'; 
                        $prop1    = array('class'=>'default_link','title'=>'Vendor Policy','style'=>' text-decoration: none;');
                        $prop2    = array('class'=>'default_link','title'=>'Petunjuk Teknis','style'=>' text-decoration: none;'); ?>
                        <tr>
                          <td><?php echo anchor('register/download_vendor_policy/',$dwld_ico."Vendor Policy",$prop1);  ?></td>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td><?php echo anchor('register/download_petunujuk_teknis/',$dwld_ico."Petunjuk Teknis",$prop2);  ?></td>
                        </tr>
                      </div>
                      <div class="pull-right" style="width:100%">
                          <hr />
                          <button style="float:right" type="button" class="btn btn-primary" data-toggle="tab" id="btn_next2" href="#registrasi-step-2">Next</button>
                      </div>
                    </div>
                </div>
                <div class="box2 box-info2 tab-pane borderless fade in" id="registrasi-step-2" style="padding: 28px 15px 0;">
                  <div class="tab-pane borderless" style="padding-bottom: 0;padding: 28px 15px 0;">
                    <h1><strong>OPEN BIDDING REGISTRATION</strong></h1>
                    <h2 style="font-weight: 300;">- Terms & Conditions -</h2>
                    <hr>
                    <div class="form-group terms-text">
                      <?php $root = $_SERVER['DOCUMENT_ROOT']; $scnd_path = $root."/uploads/cms/terms_eng.txt";
                      if( file_exists($scnd_path)) {  echo file_get_contents($scnd_path); }   ?>
                      <hr>
                      <input type="checkbox" name="agree" id="agree" value="1" style="margin-right:10px; float: left;"><label style="text-align:left;padding-bottom:0;">I accept the Terms and Conditions</label>
                    </div>
                    <hr>
                  </div>
                  <div class="pull-right" style="width:100%">
                    <button style="float:right" type="button" class="btn btn-primary" data-toggle="tab" href="#registrasi-step-3" id="btn_term_next" disabled>Next</button>
                    <button style="float:right" type="button" class="btn btn-danger" id="btn_return" data-toggle="tab" href="#registrasi-step-1">Back</button>
                  </div>
                </div>
                <div class="box2 box-info2 tab-pane borderless fade in" id="registrasi-step-3" style="padding: 28px 15px 0;">
                  <div class="tab-pane borderless" style="padding-bottom: 0;padding: 28px 15px 0;">
                    <h1><strong>OPEN BIDDING REGISTRATION</strong></h1>
                    <h2 style="font-weight: 300;">- LOI Attachment -</h2>
                    <hr>
                    <?php echo form_open_multipart('avl/submitLOI',array('id'=>'form')); ?>
                    <div class="form-inline">
                      <label style="display: inline;" for="loi-upload">Upload LOI : </label>
                      <input style="display: inline;margin-right:10px;" type="file" onblur="lostfocus()" name="file_loi" id="file_loi" required />
                    </div>
                    <hr>
                  </div>
                  <div class="pull-right" style="width:100%">
                    <button style="float:right" type="submit" class="btn btn-primary" data-toggle="tab" id="btn_submit">Submit</button>
                    <button style="float:right" type="button" class="btn btn-danger" id="btn_return" data-toggle="tab" href="#registrasi-step-2">Back</button>
                  </div>
                  <?php echo form_close();?>
                </div>
              </div>
            </div>
          </div>
        <?php }else{ ?>
        <div class="box-header with-border">
          <h1 class="box-title" style="margin: 40px 0 0;"><strong style="color: #368ccc;"><?php echo $title;?></strong></h1>
          <h2><?php echo $subtitle;?></h2>
        </div>
        <hr/>
        <p style="margin-bottom: 10px;">Please go to dashboard to track your current bidding</p>
        <span style="display:inline-block">
          <a href="<?php echo base_url();?>avl/dashboard" class="btn btn-primary nounderline">Go to Dashboard</a>
          <span style="margin: 0px 10px 0 5px">or</span>
          <a href="<?php echo base_url();?>auth/logout" class="btn btn-danger nounderline">Log Out</a>
        </span>
        <?php } ?>
      </div>
    </div>
    </div>
    <footer class="main-footer footer-line container-fluid text-center" style="height:10%;bottom:0;"> 
      Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
    </footer>
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    var baseUrl   = "<?php echo base_url(); ?>";
    $('#agree').click(function(){
      $('#btn_term_next').prop('disabled', function(i, v) { return !v; });
    });
    $('#btn_submit').click(function(e){
      var file_loi  = document.getElementById("file_loi").files.length ;
      if(file_loi==''){
        $("#file_loi").focus();
        $("#file_loi").attr("style","border:1px solid red;display: inline;margin-right:10px;");
      }else{
        $('#form').submit();
      }
    });
    function lostfocus()
    {
      $("#file_loi").attr("style","border:none;display: inline;margin-right:10px;");
    };
    </script>
  </body>
  </html>