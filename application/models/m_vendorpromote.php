<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_vendorpromote extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('crud');
	}

	function authenticate(){
		$allowed  	= array(144);
		$permit  	= $this->session->userdata('permissions');
		$permit 	= explode(",",$permit);
		foreach($permit as $str){
			$permissions[] = preg_replace("/[^0-9]/","",$str);
		}
		$result 	= array_intersect($allowed, $permissions);
		if($result){ return true;} return false;
	}

	function prom_stat($param){
		if($param == "1"){
			$res = "Process";
		}else{
			$res = "Finished";
		}

		return $res;
	}

}

/* End of file m_vendorpromote.php */
/* Location: ./application/models/m_vendorpromote.php */