<p style="margin-bottom: 0;">Dengan hormat,
<br/>
Bapak/Ibu <b><?php echo $toname;?></b>
</p>
<br/><br/>

Dengan ini kami informasikan, bahwa vendor di bawah ini telah disetujui untuk di blacklist :<br/>
<table bolder="0">
	<tr>
		<td widh="120px">Nama Perusahaan</td>
		<td>: <b><?php echo ucwords($vendor_name);?></b></td>
	</tr>
	<tr>
		<td widh="120px">ID Vendor</td>
		<td>: <b><?php echo ucwords($venID);?></b></td>
	</tr>
	<tr>
		<td widh="120px" valign="top">Kategori yang terdaftar</td>
		<td valign="top">
		: <?php 
		$i=1; $spasi='';
		foreach($category as $cate)
		{
			echo $spasi . $i . '). ' . $cate->cate_name .'<br/>';
			$i++; $spasi='&nbsp; ';
		}
		?>
		</td>
	</tr>
	<tr>
		<td widh="120px">Alasan</td>
		<td>: <b><?php echo $reason;?></b></td>
	</tr>
</table><br/>
<?php echo $strmsg;?>
<br/><br/>
<b>Link Vendor :</b><br>
<?= $link; ?>
<br><br/>

Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih.<br/>
<br/><br/>
