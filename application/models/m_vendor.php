<?php 
class m_vendor extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('crud');		
	}
	
 function authenticate(){      	
 	$allowed = array(92);	
	$permit  = $this->session->userdata('permissions');
	$permit  = explode(",",$permit);
	foreach($permit as $str){
	$permissions[] = preg_replace("/[^0-9]/","",$str);}
 	$result        = array_intersect($allowed, $permissions);
 	
	if( $result ){ return true;
	} return false;
 }
 
 function is_permit($allowed){  
 	     	
	$permit  = $this->session->userdata('permissions');
	$permit  = explode(",",$permit);
	foreach($permit as $str){
	$permissions[] = preg_replace("/[^0-9]/","",$str);}
 	$result        = array_intersect($allowed, $permissions);
 	
	if( $result ){ return true;
	} return false;
 }

 function approval_level(){
 	
 	$approved = $this->crud->browse("","approval_level","","","true","id_group,level");
	foreach($approved as $val){ $level[$val->level] = $val->id_group;	}
	
	return $level;
 } 
 
 function get_file_type($files){
 	
 	$fname = $files['userfile']['name'];	
	$fname = explode(".",$fname);
	$jum   = (count($fname)-1); 	
	$file_type  = $fname[$jum];
	
	return $file_type;
 }	
 
 function validate_upd_vendor($post){ 	
 	$docID      = $post['docID'];
 	$doc_name   = $post['doc_name'];
	$doc_num    = $post['doc_num'];
	$db="db"; $this->$db->db_select();	
	$state  = "WHERE (doc_num='$doc_num' AND doc_name='$doc_name') AND id!='$docID' ";
	$query  = "SELECT EXISTS(SELECT id FROM correspondence $state) as jum ";  //echo $query; exit;
	$result =  $this->$db->query($query); 
	$hasil  =  $result->row()->jum;	
	//echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>';
	if ($hasil > 0){ return true;
    } else {  return false;  }	
 } 
 
 function validate_upd_duedill($post){ 	
 	$docID      = $post['docID'];
 	$doc_name   = $post['doc_name'];	
	$db="db"; $this->$db->db_select();	
	$state  = "WHERE doc_name='$doc_name' AND id!='$docID' ";
	$query  = "SELECT EXISTS(SELECT id FROM dd_docs $state) as jum ";  //echo $query; exit;
	$result =  $this->$db->query($query); 
	$hasil  =  $result->row()->jum;	
	//echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>';
	if ($hasil > 0){ return true;
    } else {  return false;  }	
 } 

 function get_category(){
 	
 	$dbcategory  = $this->crud->browse("","category","","","true","id,category");
	$option      = array(''=>'-- Select Category --');
	foreach($dbcategory as $val){ $category[$val->id] = ucwords($val->category);	}
	$result =  $option + $category;	
	
	return $result; 	
 }
 
 function get_sts_register(){
 	
 	$dbregister  = $this->crud->browse("","register_status","","","true","id,status");
	$option      = array(''=>'-- Status Registration --');
	foreach($dbregister as $val){ $register[$val->id] = ucwords($val->status);	}
	$result =  $option + $register;	
	
	return $result; 	
 }

 function get_user_level($user_group=""){

	$result = 0;
	for ($i = 1; $i <= 3; $i++) {	$var = "apvr".$i;  //echo $i."=".$this->permit->$var."<br/>";
	    if($this->permit->$var) { $result =  $i ; break; }
	}
	
	return $result;
 	
 }
 
 function is_approved(){
 	$user_group     = $this->session->userdata('user_group');
 	$approval_level = $this->approval_level();  
	
	if(in_array($user_group,$approval_level)) {	 return true;
	} else {  return false;	}	
 }

 function register_status(){
 	
 	$dbstatus = $this->crud->browse("","register_status","","","false","id,status");
	foreach($dbstatus as $val){ $status[$val->id] = ucwords($val->status); }
	
	return $status;
 	
 }

 function set_session_search($field,$index,$post){
 	
 	$session  = array($field        => $index,
 					  'vendor_type' => $post['vtype'],
 					  'id_stsreg'   => $post['id_stsreg'],
 					  'id_category' => $post['id_cat'],
 					  'id_subcat'   => $post['id_subcat']);
 					  
 					  
 	$page = $this->session->userdata('page');
 	if($page=="register"){
		$session['vendor_type'] = "new";
	} else if($page=="verify"){ $session['id_stsreg'] = "6"; }			  
 	
 	foreach($session as $key=>$val){
		if(!$session[$key]) unset($session[$key]);	}
		
	return $session; 	
 }

 function search_browse($session,$sess_id){ 	
	 	
	$user_group  = $this->session->userdata('user_group');
	$user_level  = $this->get_user_level($user_group); 	
	 	
 	$like  = ""; $where = array(); $str_where = ""; $str1 = "";
 	if($session) {
		if (array_key_exists("vendor_name",$session)) {				
			$like = array('col'=>'vendor_name','field'=>$session['vendor_name']); 
			unset($session['vendor_name']);	}		
		$where = $session ;			
	}
	
	if($where['vendor_type']=="all"){
		unset($where['vendor_type']);
		$str_where[] = "(`vendor_type` != 'new')";
	}	
		
	//echo "=>".$sess_id;	
	unset($where['id_category']); unset($where['id_subcat']);
	if($sess_id){	//print_r($where);		
		$add   = array('session'=>$sess_id);	$where = $where + $add ;	
	}	
	
	if($this->is_approved()) { 
		$where['approval']  = $user_level;	$where['id_stsreg'] = "5";		 
	}
	
	$page = $this->session->userdata('page');   
	foreach($where as $field=>$val){
		if($field=='approval') $str = "(`$field` >= '$val' AND `$field` < '8')";
		//if(($field=='id_stsreg' || $field=='approval')) $str = "(`$field` >= '$val' AND `$field` < '8')";
		else $str =  "`$field` = '$val'";
		$str_where[] =  $str;
	}
	
	
		
	if($str_where) { $where = implode(" AND ",$str_where); }
	//echo $str_where."<br/>";	
	$result = array('where'=>$where,'like'=>$like);
	return (object) $result;
 	
 }

 function set_config($filename,$path,$type,$overwrite=true){	
		
		$config = array();
		$config['upload_path']   = $path;
		$config['allowed_types'] = $type;
		$config['file_name']	 = $filename;
		$config['overwrite']	 = $overwrite;
		
		return $config;
 }  
 
 function arrange_files($FILES){
 	
 	foreach($FILES['userfile'] as $key1=>$rows){
		foreach($rows as $key2=>$val){			
				$data[$key2][$key1] = $val;			
		}				
	}	
	return $data;
 	
 }

 function get_title(){
 	
 	$page = $this->session->userdata('page');
 	switch ($page) {
	    case "register":
	        $title = "Registration";
	        break;
	    case "verify":
	        $title = "Verification";
	        break;    
	    default:
	        $title = "Management";
	}
	
	return $title;
 	
 }

 function define_form($vendor_type){
 	
 	switch ($vendor_type) {
	    case "shortlist":
	        $form = "EPCGA015";
	        break;
	    case "reject":
	        $form = "EPCGA016";
	        break;    
	    default:  //avl
	        $form = "EPCGA014";
	}
	
	return $form;
 }

 function create_captcha(){
 	
 	$options = array('img_path'		=> './captcha/',
 					 'font_path' => FCPATH. 'captcha/fonts/verdana.ttf',
 					 'img_url'		=> base_url().'captcha/',
 					 'img_width'	=> '200',
 					 'img_height'	=> '60',
 					 'expiration'	=> 7200);
 					 
 	$cap   = create_captcha($options);
 	$image = $this->data = $cap['image']; 	
 	$this->session->set_userdata('captchaword',$cap['word']);
 	
 	return $image;				 
 					 
 }

 function vendor_action($id_stsreg,$id_vdr,$rejected,$completed,$recomend){  	
 	
 	switch ($id_stsreg) {
	    case "5":   
	        $result['class'] = "btn btn-warning btn-sm";
	        $result['title'] = "Verification";
	        $result['fc']    = "verification($id_vdr)";
	        break;
	    case "6": 
	        $result['class'] = ($completed && !$this->permit->account) ? "btn btn-success btn-sm" : "btn btn-info btn-sm";
	        $result['title'] = ($completed && !$this->permit->account) ? "Finalization" : "Review";
	        $result['fc']    = ($completed && !$this->permit->account) ? "finalization($id_vdr)" : "review($id_vdr)";     
	        break;
	    case "7":
	        $result['class'] = "btn btn-success btn-sm";
	        $result['title'] = "Finalization";
	        $result['fc']    = "finalization($id_vdr)";
	        break;    
	    default:
	        $result['class'] = "btn btn-default btn-sm";
	        $result['title'] = "Finished";
	        $result['fc']    = "";
	}
	
	if($recomend=="reject" || $recomend=="shortlist"){
		$result['class']  = "btn btn-danger btn-sm";
	    $result['title'] = "Finished";
	    $result['fc']    = "";	}
	
	return (object) $result;
 	
 	
 }

 function random_username($string) {
	$pattern = " ";
	$firstPart = strstr(strtolower($string), $pattern, true);
	$secondPart = substr(strstr(strtolower($string), $pattern, false), 0,3);
	$nrRand = rand(0, 100);

	$username = trim($firstPart).trim($secondPart).trim($nrRand);
	return $username;
}

 function get_vendor_number(){
 	
 	$start     = "10060";
 	$where     = "`vendor_type` = 'avl' OR `vendor_type` = 'shortlist' "; 
 	$counter   = $this->crud->get_total_record("","vendor",$where);		
 	$result    = $start + $counter;
 	
 	return $result;
 	
 }

 function is_finalize($result,$approval,$id_stsreg){
 	
 	switch ($result) {
		case "avl":   
			$result  = ($approval=="4" && $id_stsreg=="7") ? true : false;
			break;
		case "shortlist": 
			$result  = $approval=="5" ? true : false;  
			break;
		case "reject":
			$result  = ($approval=="0" && $id_stsreg=="4") ? true : false;
			break;    
		default:
			$result  = false;
	}	
	return $result; 	
 }
 
 function get_level_approval($approval){
 	
 	switch ($approval) {
		    case "1":
		        $level = "2";     break;
		    case "2":
		        $level = "3";     break;		     
		    default:
		       $level = "4";      break;
	}
	
	return $level;
	
 } 

 function register_tracking($venID,$notes){ 	
 	
 	$vendor   = $this->crud->browse("","vendor","id",$venID,"false","id_stsreg"); 
 	$insert   = array( 	'id_vendor'    => $venID,
						'id_stsreg'    => $vendor->id_stsreg,						
						'notes'        => $notes,						
						'status'       => "1",
						'created_id'   => $this->tank_auth->get_user_id(),
						'created_date' => date('Y-m-d H:i:s'),
						'last_updated' => date('Y-m-d H:i:s'));		//print_r($insert);			
	$this->crud->insert("","vendor_register_tracking",$insert);		
 	
 }

 function create_session_link($require){
 	
 	$expired = date('Y-m-d H:i:s', strtotime("+3 days")); $session = md5(uniqid());
 	$insert  = array( 'token'		 => $session,
					  'email'    	 => $require->email,
					  'username'     => $require->username,						 
					  'request'      => $require->request,
					  'parameter'    => $require->param,					  
					  'expired_date' => $expired,
					  'status' 		 => "1",
					  'created_id'   => $this->tank_auth->get_user_id(),
					  'created_date' => date('Y-m-d H:i:s'),
					  'last_updated' => date('Y-m-d H:i:s'));
					  
	$this->crud->insert("","session_notification",$insert);				
	$result   = base_url()."authenticate/session/".$session;	
	
	return $result;							  
 }

 function is_send_mail($venID){ 	
 	$exept  = array('SR','AR');
	$slct   = " SUBSTR(register_num, CAST(1 AS UNSIGNED ) , CAST(2 AS UNSIGNED )) as code";
	$vend   = $this->crud->browse("","vendor","id",$venID,"false",$slct);	
	if(in_array($vend->code,$exept)) return true;
	else return false;	
 }
 	
}	
?>