<html>
<head>
<title></title>
<meta name="" content="">
</head>
<body>

Dengan hormat,<br/>
<b><?php echo $vendor_name; ?></b><br/><br/>

Pertama tama kami mengucapkan terima kasih atas minat Bapak/Ibu untuk mendaftar menjadi Rekanan PT Garuda Indonesia (Persero) Tbk ("Garuda”) .<br/> 
Berdasarkan evaluasi yang telah kami lakukan, bersama ini kami sampaikan bahwa Perusahaan telah terdaftar menjadi Approved Vendor List (AVL). <br/>

Selanjutnya mohon dapat menyampaikan informasi bank sebagai referensi data vendor master  dengan mengakses :<br/>
(<?php echo anchor($link); ?>)<br/><br/>

Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/>
<b>PT Garuda Indonesia (Persero) Tbk</b> 


</body>
</html>