<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Robot extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();		
		islogged_in();
		$this->load->library('Curl');
    }
    
 function index(){
 	$data['user_id']	= $this->tank_auth->get_user_id();
	$data['username']	= $this->tank_auth->get_username();
	$data['view']	    = "testing/vendor_robot/form"; 		
 	$this->load->view('layout/template',$data);  	
 } 
 
 function execute(){
 	$uri  = $this->input->post('url');
 	$url  = base_url().$uri;  //echo $url;
 	$result = $this->curl->simple_get($url);
 	echo "success running"; 
 }
 


}

