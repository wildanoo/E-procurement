<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Profiles extends Controller {

	function __construct()
	{
		parent::__construct();			
	}	
		
	function index(){
		if ($this->tank_auth->is_logged_in()) {

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			$id = $this->tank_auth->get_user_id();
			$select = "id,userID,name,nopeg,id_office,id_dept,
				  phone,mobile,status,created_id,created_date,last_updated";
			$data['def'] =$this->crud->browse("","profiles","id",$id,"false",$select);
			
			$office = $this->crud->browse("","offices","","","true","id,office_name");
			if(!$office) $office = array();
			foreach($office as $val){ $option_office[$val->id] = $val->office_name; }
			$data['offices'] = $option_office;
			
			$browse = $this->crud->browse("","profiles","","","true","userID");
			foreach ($browse as $browses){};
			$data['browse']=$browses;
			
			$department = $this->crud->browse("","departemen","","","true","id,dept_name");
			if(!$department) $department = array();
			foreach($department as $val){ $option_dept[$val->id] = $val->dept_name; }
			$data['department'] = $option_dept;
			//$data['profiles'] = $this->crud->browse("","profiles","id",$id,"false",$select);
			//print_r($data);
			//$where = "userID = '".$data['user_id']."'";
			$profiles = $this->crud->browse("","profiles","","","false",$select);
			foreach ($profiles as $profile){};
			$data['profiles']=$profile;
			//function browse($db="",$table,$field="",$id="",$multi="",$select="",$where="",$order="",$group_by="",$like="")
			$none_profile = array('id'			=> "none",
					'name'		=> "none",
					'nopeg'		=> "none",
					'id_office'	=> "none",
					'id_dept'	=> "none",
					'phone'		=> "none",
					'mobile'  	=> "none",
			);
			$profile           = $this->crud->browse("","profiles","userID",$id,"false");
			$data['profiles']   = !$profile ? (object) $none_profile : $profile;
			$data['view']	= "profiles/form_update";
			$this->load->view('layout/template',$data);
 		
	 		
		} else {  
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/'); 	 }
	}
	
	function load_profiles(){
		$id = $this->tank_auth->get_user_id();
		$select	 = "id,name,nopeg,phone,mobile,
 					(SELECT id_office FROM offices WHERE id=id_office) as id_office,
					(SELECT id_dept FROM departemen WHERE id=id_dept) as id_dept,
					status,created_id,created_date,last_updated";
		
		$office = $this->crud->browse("","offices","","","true","id,office_name");
		if(!$office) $office = array();
		foreach($office as $val){ $option_office[$val->id] = $val->office_name; }
		$data['offices'] = $option_office;
		
		$department = $this->crud->browse("","departemen","","","true","id,dept_name");
		if(!$department) $department = array();
		foreach($department as $val){ $option_dept[$val->id] = $val->dept_name; }
		$data['department'] = $option_dept;
		
		$data['profiles'] = $this->crud->browse("","profiles","userID",$id,"false",$select);
		
		$this->load->view('profiles/form_update',$data);
	}
	
	function profiles_update(){
		$curr_date =  date('Y-m-d H:i:s');		
		$where      = array('id'=>$venID);
		$is_exist   = $this->crud->is_exist("","profiles","id",$where);
		$userID     = $this->tank_auth->get_user_id();
		$browse     = $this->crud->browse("","profiles","userID",$userID,"");
		$data = array('id'			=>$_POST['id'],
					  'name'		=>$_POST['name'],
					  'nopeg'		=>$_POST['nopeg'],
				      'id_office'	=>$_POST['id_office'],
					  'id_dept'		=>$_POST['id_dept'],
				      'phone'		=>$_POST['phone'],
					  'mobile'		=>$_POST['mobile'],
					  'userID'		=>$userID,
					  'created_id'  =>$userID,
					  'created_date'=>$curr_date,
					  'last_updated'=>$curr_date);
			
		if($browse){
			$this->crud->update("","profiles","userID",$userID,$data);
		} else { $this->crud->insert("","profiles",$data); }
	}
	
}