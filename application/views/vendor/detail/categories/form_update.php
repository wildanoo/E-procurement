<div id="container2">

<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   
   $('#id_category').change(function(){  
	   		var id_cat = $('#id_category').val(); //alert(id_cat);
	   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	            $.ajax({
	               url  : baseUrl + 'vendor/get_subcategory',
	               type : "POST",              		               
	               data : csrf +'='+ token +'&id_cat='+id_cat,
	               success: function(resp){
	               	  $('#id_subcat').html(resp);
	               }
	            });
	});	      
   
   $('#btn_create2').click(function(){
    	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token   = '<?php echo $this->security->get_csrf_hash(); ?>';	
		var values  = $("#form_category").serializeArray();
		values.push({name: csrf,value: token});
		values = jQuery.param(values);
		
		$.post( baseUrl + "vendor/is_exist_category", values ).done(function( resp ) {
			var json = $.parseJSON(resp);
			if(json.status == "false")	{
				$('#msg').html('Sub category has been exist or empty sub category !');
			} else {					
				$.post( baseUrl + "vendor/update_category", values ).done(function( resp2 ) {
					$("#container2").load(baseUrl+'vendor/load_categories');					
				});									
			}		    	
		});		
									     	
     }); 
     
   $('#btn_cancel2').click(function(){ 
   		 $("#container2").load(baseUrl+'vendor/load_categories');   		 
     });     
	   
   });

</script>
 
<div id="debug14"></div>
<?php echo form_open('#',array('id'=>'form_category'));?>
<input type="hidden" name="id" id="id" value="<?php echo $default->id; ?>"/>
<table width="50%">
<tr>
<td><label>Category</label></td><td>:</td>
<td>
	<?php echo form_dropdown("id_category",$category,$default->id_cat,'class="form-control" id="id_category" style="font-size:12px;"'); ?>
</td>
</tr>
<tr>
<td><label>Sub Category</label></td><td>:</td>
<td>	 
	<?php echo form_dropdown("id_subcat",$subcategory,"",'class="form-control" id="id_subcat" style="font-size:12px;"'); ?>
</td>
</tr>
<tr>
	<td colspan="3" id="msg">&nbsp;</td>
</tr>
</table>

<div style="text-align: left">
	<input type="button" id="btn_cancel2" value="Cancel" class="btn btn-default btn-sm"/>
	<input type="button" id="btn_create2" value="Submit" class="btn btn-primary btn-sm"/>	
</div>


<?php echo  form_close(); ?>
<br/>
</div>





