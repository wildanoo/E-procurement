<?php
class LanguageLoader
{
    function initialize() {
        $CI =& get_instance();
        $CI->load->helper('language');
        $siteLang = $CI->session->userdata('lang')?$CI->session->userdata('lang'):$CI->session->set_userdata('lang','eng');
        if ($siteLang == 'ind') {
            $CI->lang->load('page_define','indonesia');
        } else {
            $CI->lang->load('page_define','english');
        }
    }
}