<div id="container13">
<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	
   
   $('#btn_create13').click(function(){  
   		$("#container13").load(baseUrl+'vendor/form_aff');   		
   });             	
   
 }); 
 
 function update_aff(id){
 	var baseUrl = "<?php echo base_url(); ?>";
 	$("#container13").load(baseUrl+'vendor/form_update_aff/'+id); 
 }
 
 function delete_aff(id){
 	var baseUrl = "<?php echo base_url(); ?>";
 	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
 	
 	if (confirm('Are you sure you want to delete affiliate?')) {	
		$.post( baseUrl + "vendor/delete_aff", { csrf: token, id: id } );
		$("#container13").load( baseUrl + "vendor/load_aff");	
		//alert('ok');	
	}
	
 } 
  
 </script>

<button type="button" id="btn_create13" class="btn btn-info btn-primary">Add Affiliate</button> 
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Company Name</th>
		<th>Competency</th>
		<th>Contact</th>
		<th>Remark</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
 <?php  //$aff = array();
 if($aff){ $i=1;
 	foreach($aff as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->company_name; ?></td>	
			<td><?php echo $row->competency; ?></td>
			<td><?php echo $row->contact; ?></td>
			<td><?php echo $row->remark; ?></td>
			<td style="text-align: center;">
			<?php
				echo '<i class="fa fa-pencil" onclick="update_aff('.$row->id.')"></i>'; echo '&nbsp;&nbsp;&nbsp;';					
				echo '<i class="fa fa-trash"  onclick="delete_aff('.$row->id.')"></i>';	
			?></td>											
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="6">-</td></tr>
<?php } ?>
     </tbody>
</table>
</div>

