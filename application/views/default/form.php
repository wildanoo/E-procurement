 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">

              <div class="box-header with-border">
                <h3 class="box-title">New Announce</h3>
              </div>

              <form class = "form-horizontal">
                <div class="box-body">

                  <div class = "col-md-6">
                    <div class="form-group">
                      <label for="title" class="col-md-3 control-label inputbox-size">Title</label>
                      <div class="col-md-9 row">
                        <input type="text" class="form-control form-margin" id="title" placeholder="Title">
                      </div> 
                    </div>

                  <div class="form-group clear-pad-top">
                    <label for="category" class="col-sm-3 control-label inputbox-size">Type</label>
                    <div class="col-sm-6 control-label clear-pad-top row">
                        <select class = "form-control form-margin" name="category" id="category">
                          <option value = "">--- Category ---</option>
                          <option value = "">News</option>
                          <option value = "">Open Sourcing</option>
                          <option value = "">Open Bidding</option>
                          <option value = "">Invitation</option>
                        </select>
                    </div>
                  </div>

                  <div class="form-group clear-pad-top">
                    <label for="category" class="col-sm-3 control-label inputbox-size">Company Name</label>
                    <div class="col-sm-6 control-label clear-pad-top row">
                        <select class = "form-control form-margin" name="companyname" id="category">
                          <option value = "">--- Company Name ---</option>
                          <option value = "">PT. Aerofood ACS (001)</option>
                          <option value = "">PT. Aerotrans (002)</option>
                        </select>
                    </div>
                  </div>

                  <div class="form-group clear-pad-top">
                    <label for="category" class="col-sm-3 control-label inputbox-size">Office Region</label>
                    <div class="col-sm-6 control-label clear-pad-top row">
                        <select class = "form-control form-margin" name="officeregion" id="category">
                          <option value = "">--- Office Region ---</option>
                          <option value = "">PT. Aerofood ACS (001)</option>
                          <option value = "">PT. Aerotrans (002)</option>
                        </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="namedesc" class="col-md-3 control-label inputbox-size">Creator</label>
                    <div class="col-md-9 row">
                      <input type="text" class="form-control" id="namedesc" placeholder="Creator">
                    </div>
                  </div>


                  <div class = "form-group">
                    <label for="serviceclass" class="col-sm-3 control-label inputbox-size" >Content</label>
                      <div class="col-lg-9 col-sm-6 col-12 row inputbox-size">
                        <input type="radio" name="content" value="1" > <span style="vertical-align: sub;margin-left: 5px;">Typing</span> &nbsp;&nbsp;&nbsp;
                        <input type="radio" name="content" value="2" checked=""> <span style="vertical-align: sub;margin-left: 5px;">Upload</span><br>
                      </div>
                  </div>

                  <div class = "form-group">
                    <label for="serviceclass" class="col-sm-3 control-label inputbox-size" ></label>
                      <div class="col-lg-9 col-sm-6 col-12 row" >
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="No file chosen (.jpg, .png, .gif)" readonly>
                          <span class="input-group-btn">
                              <span class="btn btn-crt btn-file">Browse<input type="file" multiple></span>
                          </span>
                        </div>
                      </div>
                  </div>


                  <div class = "form-group" id="upload41">
                    <label for="serviceclass" class="col-sm-3 control-label inputbox-size" ></label>
                    <div class="col-lg-9 col-sm-6 col-12 row" >
                      <div class="input-group">
                        <?php 
                        $style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";
                        $style = "";
                          $data3  = array('type'=>'text','name'=> 'loifile','id' => 'file_upload','placeholder'=>'No file chosen (.pdf)','content'=>'Import','class' => 'form-control','style'=>$style); 
                          echo form_input($data3); ?> 
                        <span class="input-group-btn">
                          <span class="btn btn-crt btn-file">Browse<input type="file" multiple></span>
                        </span>
                      </div>
                    </div>
                  </div>
                  </div><!-- col-md-6 -->

                  <div class = "col-md-6">
                    
                    <div class="form-group" style="margin-bottom: -10px;">
                      <label for="effectivedate" class="col-md-3 inputbox-size-right">Validity Period</label>
                      <div class="col-md-9" style="margin-top: -10px; margin-bottom: -10px;">
                        <div class="col-md-5">
                          <div class="form-group clear-mar-bottom">
                              <div class="input-group date row" id="datetimepicker7">
                                  <input type="text" class="form-control" />
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                          </div>
                        </div>
                        <div class = "col-md-2 text-center">
                          <label class = "control-label" style="margin-top: 13px;">to</label>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group clear-mar-bottom">
                                <div class="input-group date row" id="datetimepicker8">
                                    <input type="text" class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div><!-- form-group -->

                    <div class="form-group" style="margin-bottom: -10px;">
                      <label for="effectivedate" class="col-md-3 inputbox-size" style="margin-top:20px;">Publish Date</label>
                      <div class="col-md-9">
                        <div class="col-md-5">
                            <div class="form-group clear-mar-bottom">
                                <div class="input-group date row" id="datetimepicker9">
                                    <input type="text" class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div><!-- form-group -->

                    <div class="form-group">
                      <label for="authority" class="col-md-3 control-label inputbox-size">Detail</label>
                      <div class="col-md-9 row">
                        <textarea class="form-control" rows="6" placeholder="Detail"></textarea>
                        <!-- <textarea class="ckeditor" cols="80" id="detail" name="editor1" rows="10"></textarea> -->
                      </div>
                    </div>

                    <div class = "form-group">
                      <label for="serviceclass" class="col-sm-3 control-label inputbox-size">Attachment</label>
                        <div class="col-lg-9 col-sm-6 col-12 row">
                          <div class="input-group">
                            <input type="text" class="form-control" placeholder="No file chosen (.pdf)" readonly>
                            <span class="input-group-btn">
                                <span class="btn btn-crt btn-file">Browse<input type="file" multiple></span>
                            </span>
                          </div>
                        </div>
                    </div><!-- form-group -->

                  </div><!-- col-md-6 -->
                </div><!-- /.box-body -->

                <table class="table" border="0">       
                  <tr id="wyswyg">
                    <td><textarea class="ckeditor" cols="80" id="detail" name="editor1" rows="10"></textarea><?php //$this->load->view('announce/note'); ?></td>
                  </tr>
                </table>

                <div class="box-footer" style="padding-left: 30px;">
                    <button type="submit" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info btn-danger">Submit</button>
                </div><!-- /.box-footer -->
              </form>
            </div><!-- /.box -->
          </div>
        </div>
      </div>
    </div><!-- /row --> 