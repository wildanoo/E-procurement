<script type="text/javascript">
$(document).ready(function() {
  var baseUrl = "<?php echo base_url(); ?>";

  $('#form').submit(function(event) {
  	var sdata = $(this).serialize();
  	$.post(baseUrl+'assesment/update', sdata, function(data, textStatus, xhr) {
  		var obj = JSON.parse(data);
      if(obj.state == 'true')
      {
        load_data();
      }
      else
      {
        var err_msg = $('<span class="label label-warning"></span>').text(obj.msg);
        $('#fc_msg').html(err_msg);
      }
  	});
  });


});
</script>

<?php //print_r($data); ?>
<div class="panel panel-default">
<div class="panel-heading">Manage Asessment</div>
<div class="panel-body">

<div id="fc_msg" class="text-center"></div>

<?php echo form_open('#',array('id'=>'form','onsubmit'=>'return false')); ?>
	<input type="hidden" name="id" value="<?=$def->id?>">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Assesment</label>
    <div class="col-sm-10">
      <input type="text" name="description" id="description" placeholder="Assesment" class="form-control" value="<?=$def->description; ?>">
    </div>
  </div>
   
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">Save</button>
      <button onclick="load_data()" type="button" class="btn btn-primary">Cancel</button>
    </div>
  </div>
<?php echo form_close(); ?>


</div>
</div>
