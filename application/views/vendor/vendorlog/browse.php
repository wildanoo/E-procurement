<script>
	$(function(){
		$('#idstat').change(function(){
			var sdata = $('#idstat').val();
			var v_url = baseUrl+'vendorlog/browse';
			$.post(v_url, {"data": sdata}, function(data){
				$('#main_browse').html(data);

			});
		});

		$('#excelbtn').click(function(){
			var fields = $("input[name='ck_list[]']").serializeArray(); 
			if (fields.length === 0){ 
				alert('Select one or more data!'); 
				return false;
			}else{ 
				$('#myform').submit();
			}
		});
	});
</script>	

<div id="main_browse">
	<div class="page-header">
		<h3><?php echo $title ?>  Vendor Logs</h3>
	</div>
	<div class="col-md-3">
	<?php if($this->session->userdata('feature') == "vendorlist" || $this->session->userdata('feature') == "register"){ ?>
		<select name="statID" id="idstat" class="form-control">
			<option value="all">- All Status -</option>
			<?php 
			foreach($stat as $sk => $sv){?>
				<option  value="<?php echo $sk  ?>"<?php if($sk==$this->session->userdata('statID')){echo "selected";} ?> ><?php echo $sv; ?></option>
				<?php } ?>
			</select>
			<?php } ?>
		</div>

		<?php echo form_open('vendorlog/export_excel',array('id'=>'myform')); ?>
		<button class="btn btn-success pull-right" type="button" id="excelbtn"  title="Export Vendor Logs"><i class="fa fa-file-excel-o" style="color:#FFFFFF"></i>Export Excel</button>

		<!-- tabelcontent -->
		<?php require_once('v_data.php'); ?>

		<?php echo form_close(); ?>


	</div>

</div>