<script type="text/javascript">
   $(document).ready(function(){

		activaTab('<?php echo $tab; ?>');
   });
   
   function activaTab(tab){
    	$('.nav-tabs a[href="#' + tab + '"]').tab('show');
   };

</script>

<h1>DUE DILLIGENCE</h1>


<?php $this->load->view('due_dilligence/info_vendor'); ?>

<div id="tabs_container">

<?php 
	$ses_tab = $this->session->userdata('tab'); 
	$tab = !$ses_tab ? "home" : $ses_tab ;
?>
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Site Visit</a></li>
  <li><a data-toggle="tab" href="#menu0">Assesment</a></li>
  <li><a data-toggle="tab" href="#menu1">Attachment</a></li>  
  <li><a data-toggle="tab" href="#menu2">Recommendation</a></li> 
</ul>

<?php $this->session->unset_userdata('tab');  ?>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <!--h3>Site Visit</h3-->
    <p><?php  $this->load->view('due_dilligence/site_visit'); ?></p>
  </div>
  <div id="menu0" class="tab-pane fade">
    <!--h3>Assesment</h3-->
    <p><?php $this->load->view('due_dilligence/due_dilligence'); ?></p>
  </div>
  <div id="menu1" class="tab-pane fade">
    <!--h3>Attachment</h3-->
    <p><?php  $this->load->view('due_dilligence/documents'); ?></p>
  </div>  
  <div id="menu2" class="tab-pane fade">
    <!--h3>Recommendation</h3-->
    <p><?php  $this->load->view('due_dilligence/form_recmd'); ?></p>
  </div>  
</div>

</div>