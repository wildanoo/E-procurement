<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){	 
	 $(function(){  });
		var baseUrl = "<?php echo base_url(); ?>";

		$("#srch-term").autocomplete({
			source: baseUrl + 'level/get_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
	        var search = $("#srch-term").val(); // alert(search);
	        var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
	        $.ajax({
	           url  : baseUrl + 'level/set_sess_search',
	           type : "POST",              		               
	           data : csrf +'='+ token +'&search='+search,
	           success: function(resp){    $("#debug").html(resp);            		            
				    document.location.href= baseUrl + 'level/';
	           }
	        });
	    });
	    	 
	 $('#btn_create').click(function(){ 
	 	  form_create();
	 	  //alert('test');
	 });	
});

	function form_create(){
		var baseUrl = "<?php echo base_url(); ?>";		 
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		BootstrapDialog.show({
		title: 'Create Level',
        message: $('<div></div>').load(baseUrl + "level/form_create/"),
        buttons: [{ label: 'Submit',
	                cssClass: 'btn btn-primary btn-sm',		               
	                action: function(dialogRef){
	                	 var level_code  = $("#level_code").val();
	                	 var level  = $("#level").val(); 
	                	 
	                	 $.post( baseUrl + 'level/is_exist', { csrf: token , level_code : level_code , level : level })
						 .done(function( resp ) {
//								 $('#debug').html(resp);
//								 dialogRef.close(); 
								var json = $.parseJSON(resp);
								if (json.status=='true'){
				                	var values = $("#form").serializeArray();
				                	values.push({ name: csrf,value: token });
									values = jQuery.param(values);
									 $.ajax({
							               url  : baseUrl + 'level/create',
							               type : "POST",              		               
							               data : values,
							               success: function(resp){
							               		document.location.href= baseUrl + 'level/';	               				               		  
									       }
							            });   
								} else { 	
									 $("#msg1").html(json.msg1); 
									 $("#msg2").html(json.msg2); 
							}
	               		 });  
	                }
	            }, {
	            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
			                action: function(dialogRef) {
			                	document.location.href= baseUrl + 'level/';
			                    dialogRef.close(); 
			                }
		        }]
    });
}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Level',
            message: $('<div></div>').load(baseUrl + "level/form_update/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef)
		                {
		                	 var level_code  = $("#level_code").val(); 
		                	 var level  = $("#level").val(); 
		                	 
		                	 $.post( baseUrl + 'level/is_exist', { csrf: token , level_code : level_code , level : level })
							 .done(function( resp ) {
//									 $('#debug').html(resp);
//									 dialogRef.close(); 
									var json = $.parseJSON(resp);
									if (json.status=='true'){
					                	var values = $("#form").serializeArray();
					                	values.push({ name: csrf,value: token });
										values = jQuery.param(values);
										 $.ajax({
								               url  : baseUrl + 'level/update',
								               type : "POST",              		               
								               data : values,
								               success: function(resp){
								               		document.location.href= baseUrl + 'level/';	               				               		  
										       }
								            });   
									} else { 	
									 $("#msg1").html(json.msg1); 
									 $("#msg2").html(json.msg2); 
								}
		               		 });  
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'level/';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>
<script src="<?php echo base_url(); ?>assets/js/sorttable.js"></script>
</head>
<body>
<div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Level</h3>
              </div><!-- /.box-header -->
                <br>
<td><a href="<?php echo base_url('level/form_create'); ?>" type="button" class="btn btn-primary btn-sm">New Policy</a></td>
<!-- input type="button" id="btn_create" class="btn btn-primary btn-sm" value="Create" style="width:80px;"/> -->

<?php $msg = $this->session->flashdata('message');
	if(strpos($msg,'failed')) $color="#bc0505";
	else $color = "#487952"; ?>	
<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>	

                <div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('level/search',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>level" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		        </div>
		        </div>
		        
<div class="box-body">
<table id="example2" class="table table-bordered table-hover text-center sortable">
	<thead>
		<tr>
            <th class="sorttable_sorted_reverse">No</th>
            <th class="">Group</th>
            <th class="">Subcontent</th>
            <th class="">Permission</th>
            <th class="">Created Date</th>
            <th class="">Updated Date</th>
            <th class="">Updated By</th>
            <th>Action</th>
        </tr>
     </thead>
     <tbody>
 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td style="text-align:center;"><?php echo $num = $num+1; ?></td>
			<td><?php echo $row->id_group; ?></td>	
			<td><?php echo $row->id_subcontent; ?></td>
			<td><?php if ($row->order == '2') {echo 'Create';} else {echo 'Approve';}  ?></td>
 			<td align="center"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>		
 			<td align="center"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>	
 			<td align="center"><?php echo $row->created_id; ?></td>					
				<?php	$del_ico   = '<img src="'.base_url().'assets/images/delete.png" width="20" height="20"/>'; 
						$edit_ico  = '<img src="'.base_url().'assets/images/edit.png" width="20" height="20"/>'; ?>				
				<td style="text-align: center;">			
					<?php
					$pencil = '<i class="fa fa-pencil"></i>';
					echo anchor('level/form_update/'.$row->id, $pencil,array('class'=>'default_link','title'=>'Edit Level')); ?>&nbsp;
					<?php				
					//echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete level ?')){ return true; } else {return false; }";
					echo anchor('level/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Level','onclick'=>$js));?>
				</td>						
		</tr>
<?php	$i++;  } ?>	
</table>
<tr><div colspan="9" style="text-align: center;"><?php echo $pagination;?></div></tr>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>  
	</table>  
               </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->
