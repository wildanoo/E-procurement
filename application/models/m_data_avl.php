<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_data_avl extends CI_Model {

	public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->model('crud');
    $this->load->model('m_user');
  }

  function authenticate()
  {        
    $allowed = array(18); 
    $permit  = $this->session->userdata('permissions');
    $permit  = explode(",",$permit);
    foreach($permit as $str){
    $permissions[] = preg_replace("/[^0-9]/","",$str);}
    $result        = array_intersect($allowed, $permissions);
    
    if( $this->tank_auth->is_logged_in() 
      && $result ){ return true;
    } return false;
 }

  function get_changed_data($venID, $table)
  {
    $where = "id_vendor = $venID and `table` = '$table' ";
    $order = array('field' => 'last_updated', 'sort'=>'desc');
    return $this->crud->browse("",'avl_changedata','','','true','',$where);
  }

  function get_data_category($venID)
  {
    $select = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
          (SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";
    return $data = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select);
  }

  function get_data_vendor($venID)
  {
    $select	 = "id,vendor_name as name,register_num as reg_num,vendor_type as type,vendor_num as num,vendor_address as address,
           (SELECT id FROM vendor_rejected s WHERE s.id_vendor=l.id AND status=1 LIMIT 1) as rejected,
           (SELECT id FROM vendor_account  t WHERE t.id_vendor=l.id AND status=1) as completed,
           (SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as reg_sts,approval,
           id_stsreg,web,phone,fax,email,postcode,npwp,npwp_address,npwp_postcode";
    return $data= $this->crud->browse("","vendor l","id",$venID,"false",$select);
  }

  function get_data_vendor2($venID)
  {
    $select  = "id,vendor_name,register_num as reg_num,vendor_type as type,vendor_num as num,vendor_address,
           (SELECT id FROM vendor_rejected s WHERE s.id_vendor=l.id AND status=1 LIMIT 1) as rejected,
           (SELECT id FROM vendor_account  t WHERE t.id_vendor=l.id AND status=1) as completed,
           (SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as reg_sts,approval,
           id_stsreg,web,phone,fax,email,postcode,npwp,npwp_address,npwp_postcode";
    return $data= $this->crud->browse("","vendor l","id",$venID,"false",$select);
  }

  function get_data_bank($venID)
  {
    $none_bank = array('acc_number'	=> "none",
                  'acc_name'		=> "none",
                  'acc_address'	=> "none",
                  'bank_name'		=> "none",
                  'branch_name'	=> "none");
    $bank = $this->crud->browse("","bank_account","id_vendor",$venID,"false");
    return $result = !$bank ? (object) $none_bank : $bank;
  }

  function get_data_akta($venID)
  {
    $none_akta = array('notaris_name'		=> "none",
                 'notaris_address'	=> "none",
                 'notaris_phone'		=> "none",
                 'notaris_number'	=> "none",
                 'notaris_date'		=> date('Y-m-d'),
                 'remark'			=> "none");
    $akta = $this->crud->browse("","akta","id_vendor",$venID,"false");
    return $result = !$akta ? (object) $none_akta : $akta;
  }

  function getLogs($venID, $tb_name)
  {
    $select = 'a.*,b.username';
    $joins[0][0] = 'users b';
    $joins[0][1] = 'a.created_id = b.id';
    $joins[0][2] = 'left';
    $where = "a.id_vendor = $venID and a.table = '$tb_name' ";
    $order = array('field'=>'a.last_updated', 'sort'=>'desc');
    return $data = $this->crud->browse_join("",'avl_changedata a',"","","true",$select,$joins,$where,$order);

  }

  function getLogs2($params = array(),$venID, $tb_name)
  {
    $q = "select a.*,b.username from avl_changedata a left join users b on a.created_id=b.id where a.id_vendor=$venID and a.`table`='$tb_name' order by a.last_updated desc";
    if(array_key_exists("start",$params) && array_key_exists("limit",$params))
    {
    $q .= " LIMIT $params[start],$params[limit] ";
    }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
      {
    $q .= " LIMIT $params[limit] ";
    }
   
    $r = $this->db->query($q);
    return $r->result();

  }

  function validate_upd_vendor($post){  
  $docID      = $post['docID'];
  $doc_name   = $post['doc_name'];
  $doc_num    = $post['doc_num'];
  $db="db"; $this->$db->db_select();  
  $state  = "WHERE (doc_num='$doc_num' AND doc_name='$doc_name') AND id!='$docID' ";
  $query  = "SELECT EXISTS(SELECT id FROM correspondence $state) as jum ";  //echo $query; exit;
  $result =  $this->$db->query($query); 
  $hasil  =  $result->row()->jum; 
  //echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>';
  if ($hasil > 0){ return true;
    } else {  return false;  }  
 }

  function set_config($filename,$path,$type,$overwrite=true){   
    
    $config = array();
    $config['upload_path']   = $path;
    $config['allowed_types'] = $type;
    $config['file_name']   = $filename;
    $config['overwrite']   = $overwrite;
    
    return $config;
  }

   function get_file_type($files){
    
      $fname = $files['userfile']['name'];  
      $fname = explode(".",$fname);
      $jum   = (count($fname)-1);   
      $file_type  = $fname[$jum];

      return $file_type;
    }  

  function is_vendor($id)
  {
    $id_vendor = $this->db->get_where('users', array('id'=>$id) )->row()->id_vendor;
    if($id_vendor == 0)
    {
      return false;
    }
    else
    {
      $this->session->set_userdata(array('venID'=>$id_vendor) );
      return true;
    }
  }

  function get_sess_change()
  {
    $q =  " SELECT MAX(sess_name) as MaxVal
            FROM
            (
                SELECT sess_name
                FROM avl_changedata
                UNION ALL
                SELECT sess_name
                FROM temp_references_avl
                UNION ALL
                SELECT sess_name
                FROM temp_owner_avl
                UNION ALL
                SELECT sess_name
                FROM temp_affiliates_avl
                UNION ALL
                SELECT sess_name
                FROM temp_vendor_category_avl
                UNION ALL
                SELECT sess_name
                FROM temp_organization_avl
                UNION ALL
                SELECT sess_name
                FROM temp_correspondence_avl
                UNION ALL
                SELECT sess_name
                FROM temp_contact_person_avl
              
            ) as subQuery ";
    $dt = $this->db->query($q)->row()->MaxVal;
  	// $this->db->order_by("last_updated", "desc");
  	// $dt = $this->db->get('avl_changedata')->row()->sess_name; 
  	if(empty($dt) || $dt == 0)
  	{
  		$dt = 1;
  	}
  	else
  	{
  		$dt += 1;
  	}
  	return $dt;
  }

  function sending_mail($group_id, $venID)
  {
    $select  = "register_num as num,vendor_name as name,email";
    $vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);

    $subject =  "Notification Change Data Approval | E-procurement Garuda";

    $recepient = $this->m_user->get_userdata_group($group_id);
    
    foreach($recepient as $value)
    {
      if(filter_var($value->email, FILTER_VALIDATE_EMAIL))
      { 
        $user    = $this->users->get_user_by_email($value->email);
        if($user)
        {           
          $require = array('email'=>$user->email,'username'=>$user->username,'request'=>'change_request_avl','param'=>$venID); 
            
          $link    = $this->create_session_link((object) $require);
          

          $to      =  $value->email;
          $param   =  array('reg_num'     => $vendor->num,
                            'vendor_name' => $vendor->name,
                            'link'        => $link,
                            'to'          => $value->username);

          $param['view']  = NOTIF_PATH."EPCGA052";    
          $message    = $this->load->view(NOTIF_TMPL,$param,true); 
          $this->crud->sendMail($to,$subject,$message);
        }
      }
    }

  }

  function create_session_link($require){
  
  $expired = date('Y-m-d H:i:s', strtotime("+3 days")); $session = md5(uniqid());
  $insert  = array( 'token'    => $session,
            'email'      => $require->email,
            'username'     => $require->username,            
            'request'      => $require->request,
            'parameter'    => $require->param,            
            'expired_date' => $expired,
            'status'     => "1",
            'created_id'   => $this->tank_auth->get_user_id(),
            'created_date' => date('Y-m-d H:i:s'),
            'last_updated' => date('Y-m-d H:i:s'));
            
  $this->crud->insert("","session_notification",$insert);       
  $result   = base_url()."authenticate/session/".$session;  
  
  return $result;               
 } 

}

/* End of file data_avl.php */
/* Location: ./application/models/data_avl.php */