<?php

$checkreg   = 'Check Registration Status';
$word1      = 'Sorry, You can\'t join this bidding in Garuda Indonesia (Persero)';
$word2      = 'Would you try to register ?';
$word11     = 'You are successfully joined this event.';
$word22     = 'Please check your email.';
$yes        = 'Yes';
$back       = 'Back';
$home       = 'Back to Home';
$tracking   = 'Tracking Registration Status';
$wordplease = 'Please Enter Registration Number Then Click "Search"';
$placeholder= 'Registration Number';
$search     = 'Search';

if ($this->session->userdata('lang') == 'ind') {
    $checkreg   = 'Cek Status Registrasi';
    $word1      = 'Maaf, Anda tidak dapat mengikuti pengadaan di Garuda Indonesia (Persero)';
    $word2      = 'Apakah Anda mau mencoba registrasi ?';
    $word11     = 'Anda berhasil mendaftar pada event ini';
    $word22     = 'Mohon untuk cek email Anda.';
    $yes        = 'Ya';
    $back       = 'Kembali';
    $home       = 'Kembali ke Beranda';
    $tracking   = 'Pencarian Status Pendaftaran';
    $wordplease = 'Silakan masukan nomor registrasi kemudian klik "Cari"';
    $placeholder= 'Nomor Registrasi';
    $search     = 'Cari';
}

?>
<div class="about-content-wrap pull-left" id="#">
    <div style="margin-top:60px;">
    </div>
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 content-tabs-wrap wow fadeInUp animated" data-wow-delay="1.7s" data-wow-offset="200">
            <div class="col-xs-12 col-sm-12 col-md-12 content-tabs no-pad">            
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-home" data-toggle="tab"> &nbsp;  &nbsp; <?php echo $notiv; ?> &nbsp;  &nbsp; </a></li>
                    <?php 
                    if($notiv=='Success')
                    {
                        echo '<li><a href="#tab-profile" data-toggle="tab">'.$checkreg.'</a></li>';
                        $ico = 'check-circle-o';
                        $color='green';
                    }else{
                        echo '<li><a>&nbsp;</a></li>';
                        $ico = 'exclamation-circle';
                        $color='red';
                    }
                    ?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-home">
                        <div class = "row">
                            <div class="col-md-12 tab-content">
                                <div class="box2 box-info2 tab-pane fade in active" id="registrasi-step-1">
                                    <form class="form-horizontal">
                                        <div class="box-body">
                                            <div class="about-intro-wrap pull-left">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 faq-tabs-wrap">
                                                        <div class="tabbable tabs-left">
                                                            <div class="tab-content col-md-12 col-sm-12 col-xs-12">
                                                                <div class="row">
                                                                    <div class="col-sm-1">
                                                                        <i class="fa fa-<?php echo $ico;?>" style="color:<?php echo $color;?>;font-size:80px;margin-top:20px"></i>
                                                                    </div>
                                                                    <div class="col-sm-11">
                                                                        <div class="tab-pane active" id="a">
                                                                            
                                                                            <?php 
                                                                            if($notiv=='Attention')
                                                                            {
                                                                                echo '<h2>'.$word1.'</h2>
                                                                                      <h3>'.$word2.'</h3>';
                                                                            }else{
                                                                                echo '<h2>'.$word11.'</h2>
                                                                                      <h3>'.$word22.'</h3>';
                                                                            }
                                                                            ?>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div align="left" style="padding:0 0 0 85px">
                                                                    <?php 
                                                                    if($notiv=='Attention')
                                                                    {
                                                                        echo '<a type="button" class="btn button-custom" href="'.base_url('auth/registration').'">'.$yes.'</a> &nbsp;  &nbsp; 
                                                                              <a type="button" class="btn button-custom" href="'.base_url().'">'.$back.'</a><br /><br /> <br />';
                                                                    }else
                                                                    {
                                                                        echo '<a type="button" class="btn button-custom" href="'.base_url().'">'.$home.'</a><br /><br /> <br />';
                                                                    }
                                                                    ?>

                                                                </div>
                                                                <br><br><br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade no-pad" id="tab-profile">
                        <div class="row background-terms">
                            <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i><?php echo $tracking;?></div>
                            <div class="col-md-12 col-sm-12 terms-custom">
                                <div class="form-group terms-text">
                                    <?php echo $wordplease;?>
                                    <br>
                                    <br>
                                    <input type="text" class="appt-form-txt" placeholder="<?php echo $placeholder;?>" style="width:40%;" /><button type="button" class="btn button-search" data-toggle="tab" href="#registrasi-step-2"><?php echo $search;?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        </div>
    </div>
</div>