<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
        <li class="nav-item start active open">
            <a href="#top" class="nav-link nav-toggle">
                <i class="fa fa-home"></i>
                <span class="title">Home</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="#news" class="nav-link nav-toggle">
                <i class="fa fa-bullhorn"></i>
                <span class="title">News</span>
                <span class="title">and</span>
                <span class="title">Announ</span>
                <span class="title">cement</span>
                <span class="arrow"></span>
            </a>
        </li>  
        <li class="nav-item  ">
            <a href="#faq" class="nav-link nav-toggle">
                <i class="fa fa-question-circle"></i>
                <span class="title">FAQs</span>
                <span class="arrow"></span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="#contact" class="nav-link nav-toggle">
                <i class="fa fa-map-marker"></i>
                <span class="title">Contact Us</span>
                <span class="arrow"></span>
            </a>
        </li>
    </ul>
</div>