<script type="text/javascript"> 
	$(function(){
		var baseUrl   = "<?php echo base_url(); ?>";
		
		$('#id_category').change(function(){  
	   		var id_cat = $('#id_category').val(); //alert(id_cat);
	   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	            $.ajax({
	               url  : baseUrl + 'register/get_subcategory',
	               type : "POST",              		               
	               data : csrf +'='+ token +'&id_cat='+id_cat,
	               success: function(resp){
	               	  $('#id_subcat').html(resp);
	               }
	            });
	    }); 
	    
	    $('#btn_register').click(function(){
	    	var values  = $("#form").serializeArray();	    	
	    	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	        values.push({name: csrf,value: token});	values = jQuery.param(values);	        
	            $.ajax({
	               url  : baseUrl + 'register/validate',
	               type : "POST",              		               
	               data : values,
	               success: function(resp){
	               		var json = $.parseJSON(resp); 
	               		 if(json.status=="false"){
						 	$('#msg1').html(json.msg1);	$('#msg6').html(json.msg6);
						 	$('#msg2').html(json.msg2);	$('#msg7').html(json.msg7);
						 	$('#msg3').html(json.msg3); $('#msg8').html(json.msg8);
						 	$('#msg4').html(json.msg4); $('#msg9').html(json.msg9);
						 	$('#msg5').html(json.msg5);						 	
						 } else {	$("#form").submit(); }
	               }
	            });
	    });      	
		
}); 
</script>


<div id="debug"></div>
				<div class="module form-module" style="overflow-y: scroll;">
					<div id="toggle-close2" class="toggle-close">
					<i class="icon icon-times fa-pencil">Close [x]</i>
				</div>
				<div class="form form-logs">
				    <h2 class="pad-log">SUPPLIER REGISTRATION</h2>
				   
				    <?php echo form_open('register/create',array('id'=>'form')); ?>
				   			      	
						<input type="text" placeholder="Supplier Name" name="vendor_name" id="vendor_name"> 
						<div id="msg1" style="color: red; font-style: italic; "></div>						
				      	<input type="text" placeholder="Supplier Address" name="vendor_address" id="vendor_address">
				      	<div id="msg2" style="color: red; font-style: italic; "></div>	 	
				      	
				      	<?php echo form_dropdown("id_category",$category,"",'class="form-control" id="id_category" style="font-size:12px;"'); ?>
				      	<div id="msg3" style="color: red; font-style: italic; "></div>	
				      	<select name="id_subcat" id="id_subcat" class="form-control" style="font-size:12px;">
							<option value="">--Select Category--</option> 		
						</select><div id="msg4" style="color: red; font-style: italic; "></div>	
						
					    <input type="contact" placeholder="Contact Person" name="cp" id="cp"><div id="msg5" style="color: red; font-style: italic; "></div>			
					    <input type="text" placeholder="Phone Number" name="phone" id="phone"><div id="msg6" style="color: red; font-style: italic; "></div>			
					    <input type="tel" placeholder="Fax Number" name="fax" id="fax"><div id="msg7" style="color: red; font-style: italic; "></div>					
					    <input type="email" placeholder="Email Address" name="email" id="email">
					    <div id="msg8" style="color: red; font-style: italic; "></div>		
					    <input type="tel" placeholder="NPWP" name="npwp" id="npwp"><div id="msg9" style="color: red; font-style: italic; "></div>										    
					    <?php 
					    	$blank_post = array('id_lang','id_country','id_region','district','postcode','city',
												'street','house_num','sprefix','ssuffix','building','floor',
												'room','bank_name','branch_detail','bckey','acc_holder','acc_num','bank_region');						
							foreach($blank_post as $field){	echo form_hidden($field,""); }
					    ?>		    
					    <button type="button" id="btn_register">Register</button>
						    
				    <?php echo form_close(); ?>
				  </div>
				  
				  
	
<!--reg_num
vendor_name
vendor_address
category
cp
email
phone
fax
npwp
status
created_id
created_date
last_updated-->
			  