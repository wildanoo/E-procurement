<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   
	$('#pjdate').datepicker({  
        changeMonth: true, changeYear: true,
		dateFormat: "yy/mm/dd" ,
    });     
   
   $('#btn_create12').click(function(){
    	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		var values  = $("#form_ref").serializeArray();
				values.push({name: csrf,value: token});
				values = jQuery.param(values); 
						$.ajax({
						    url  : baseUrl + 'vendor/add_reference',
						    type : "POST",              		               
						    data : values,
						    success: function(resp){
						        //$('#debug12').html(resp);
						        $("#container12").load(baseUrl+'vendor/load_ref');
						    	
						    }
						});							     	
     }); 
     
   $('#btn_cancel12').click(function(){ 
   		 $("#container12").load(baseUrl+'vendor/load_ref');   		 
     });     
	   
   });

</script>
<div id="debug12"></div>
<?php echo form_open('#',array('id'=>'form_ref')); $current_date = date('Y-m-d'); ?>
<table width="50%">
<tr>
	<td><label>Customer Name</label></td><td>:</td>
	<td><input type="text" name="cust_name" id="cust_name" placeholder="Customer Name" class="form-control" required/></td>
	<td id="msg1">&nbsp;</td>	
</tr>
<tr>
	<td><label>Project Name</label></td><td>:</td>
	<td><input type="text" name="project" id="project" placeholder="Project Name" class="form-control" required/></td>
	<td id="msg2">&nbsp;</td>	
</tr>
<tr>
	<td><label>Point</label></td><td>:</td>
	<td><input type="text" name="point" id="point" placeholder="Point" class="form-control" required/></td>
</tr>

<tr>
	<td><label>Project Date</label></td><td>:</td>
	<td><input type="text" name="pjdate" id="pjdate" value="<?php echo $current_date; ?>" class="form-control" /></td>
</tr>
<tr>
	<td><label>Remark</label></td><td>:</td>
	<td><input type="text" name="remark" id="remark" placeholder="Remark" class="form-control" required/></td>
</tr>
<tr>
	<td colspan="3">
		<input type="button" id="btn_cancel12" value="Cancel" class="btn btn-default btn-sm"/>
		<input type="button" id="btn_create12" value="Submit" class="btn btn-primary btn-sm"/>		
	</td>	
</tr>
</table><br/><br/>
<?php echo  form_close(); ?>


