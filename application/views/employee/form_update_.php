<div class="page-header">
  <h3>Edit Employee</h3>
</div>
<?php echo form_open('employee/update',array('id'=>'form')); ?>
<?php echo form_hidden('id',$def->id); ?>
<table cellpadding="2" cellspacing="2">
<tr>
	<td><label>Nomor Pegawai</label></td><td>:</td>
	<td><input type="text" name="nopeg" id="nopeg" placeholder="Nomor Pegawai" value="<?php  echo $def->nopeg; ?>" class="form-control" required/>		
	</td>	
</tr>
<tr>
	<td><label>Nama Lengkap</label></td><td>:</td>
	<td><input type="text" name="fullname" id="fullname" placeholder="Nama Lengkap" value="<?php  echo $def->fullname; ?>" class="form-control" required/>		
	</td>	
</tr>
<tr>
	<td><label>Office</label></td><td>:</td>
	<td><?php $prop_office = 'id="id_office" class="form-control"';
          echo form_dropdown("id_office",$office,$def_office->id_office,$prop_office);?>
	</td>	
</tr>
<tr>
	<td><label>Division</label></td><td>:</td>
	<td><?php $prop_div = 'id="id_div" class="form-control"';
          echo form_dropdown("id_div",$division,$def_div->id_div,$prop_div);?>
	</td>	
</tr>
<tr>
	<td><label>Department</label></td><td>:</td>
	<td><?php $prop_dept = 'id="id_office" class="form-control"';
          echo form_dropdown("id_dept",$department,$def_dept->id_dept,$prop_dept);?>
	</td>	
</tr>
<tr>
<tr>
	<td><label>Position</label></td><td>:</td>
	<td><?php $prop_position = 'id="id_position" class="form-control"';
        echo form_dropdown("id_position",$position,$def_position->id_position,$prop_position);?>
	</td>	
</tr>
	</td>	
</tr>
<tr>	
	<td colspan="3" style="text-align: right;">
		<input type="submit" value="Submit" class="btn btn-primary btn-sm"/>
		<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>'Cancel'); ?>					 
		<?php  echo anchor('employee/','Cancel',$prop); ?>
	</td>
</tr>
</table>
<?php echo form_close(); ?>