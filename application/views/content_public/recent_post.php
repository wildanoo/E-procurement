<div class="col-md-4">
	<div class="headline"><h4>Recent Posts</h4></div>
	<hr>
	<ul class="post-list no-bullet">
	<?php foreach ($recent_post as $post) { ?>
		<li style="text-align:justify;">
			<a href="#" class="headline"><?php echo $post->title;?></a>
			<time datetime="2012-09-01"><?php echo date( 'd/m/Y', strtotime( $post->publish_date ));?></time>
		</li>
	<?php	} ?>
	</ul>			
</div>