<h3>Supplier Notification Letter</h3>
<p>
Dear Supplier, 

	The intent of this letter is to inform you of recent regulations impacting the defense and shipbuilding industries and other industries to which you may supply products and materials, including Huntington Ingalls Incorporated (�HII�), and to request your cooperation in addressing this important matter. 
</p>