<?php echo form_open('#',array('id'=>'form')); ?>
<table cellpadding="2" cellspacing="2">
<?php echo form_hidden('id',$data->id); ?>
<tr>
	<td><label>Department</label></td><td>:</td>
	<td><?php 
		$prop_department = 'class="form-control" id="id_dept" style="width:220px"';
		echo form_dropdown('id_dept',$departemen,$def->id_dept,$prop_department); ?>			
	</td>
	<td id="msg1"></td>	
</tr>
<tr>
	<td><label>Section Code</label></td><td>:</td>
	<td><input type="text" name="section_code" id="section_code" value="<?php  echo $data->section_code; ?>" class="form-control" required/></td>
	<td id="msg2"></td>	
</tr>
<tr>
	<td><label>Section Name</label></td><td>:</td>
	<td><input type="text" name="section_name" id="section_name" value="<?php  echo $data->section_name; ?>" class="form-control" required/></td>
	<td id="msg3"></td>	
</tr>
</table>