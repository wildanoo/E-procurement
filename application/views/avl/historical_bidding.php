<script>
$(document).ready(function(){   
    var baseUrl   = "<?php echo base_url(); ?>";    
    var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
    var token     = '<?php echo $this->security->get_csrf_hash(); ?>';


    $("#start_date").datetimepicker({
        format:'Y/m/d',
        timepicker:false,
        maxDate: $("#end_date").val(),
        onChangeDateTime: function (dp,selectedDate) {
            var time = $("#start_date").val();

            $("#end_date").datetimepicker({minDate: selectedDate.val()});
        }
    });

    $("#end_date").datetimepicker({
        format:'Y/m/d',
        minDate: 0,
        timepicker:false,
        onChangeDateTime: function (dp,selectedDate) {
            var time = $("#start_date").val();
            var init = selectedDate.val();

            $("#start_date").datetimepicker({maxDate: selectedDate.val()});
        }
    });

    $('#id_cat').change(function(){
        $('#form-select-category').submit(); 
    });

}); 
</script>
<div class="page-header">
  <h3>Historical Bidding</h3>
</div>
<!-- Search Term -->
<div class="table">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-search"></i> <strong>Search Section</strong>
        </div>
        <style type="text/css">
        .list-inline li{
            vertical-align: middle !important;
        }
        .btn-srch{
            margin-bottom: 0;
        }
        </style>
        <?php echo form_open_multipart('avl/search_history_bidding',array('id'=>'form-search'));    //$current_date = date('Y/m/d')?>
        <div class="panel-body">
            <ul class="list-inline">
                <?php $dateranges = $this->session->userdata('search_dateranges');?>
                <li style="width:24%">
                    <div class="input-group">
                    <input type="text" value="<?php echo $dateranges[1]?>" placeholder="date start" class="form-control date_picker" id="start_date" name="start_date" autocomplete="off">
                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                    </div>
                </li>
                <li style="width:2%">to</li>
                <li style="width:24%">
                    <div class="input-group">
                    <input type="text" value="<?php echo $dateranges[2]?>" placeholder="date end" class="form-control date_picker" id="end_date" name="end_date" autocomplete="off">
                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                    </div>
                </li>
                <li style="width:34%"><div class="input-group" style="width:100%"><input placeholder="search by title..." value="<?php echo $this->session->userdata('search_term')?$this->session->userdata('search_term'):'';?>" type="text" value="" class="form-control" id="search_term" name="search_term" autocomplete="off"></div></li>
                <li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search">Search</button></div></li>
                <li style="width:7%"><div class="input-group"><a href="<?php echo base_url('avl/history_bidding')?>" class="btn btn-srch btn-warning btn-sm">Show All</a></div></li>
            </ul>
        </div>
        <?php echo form_close()?>
        <!-- <div class="panel-footer text-center">
            <a href="#" class="btn btn-primary" style="margin-bottom:0;">Change Password</a>
        </div> -->
    </div>
</div>

<?php echo form_open('avl/subcategory/history_bidding',array('id'=>'form-select-category')); ?>
<select name="id_cat" id="id_cat" class="form-control" style="font-size:12px; width: 200px;">   
    <option value="all">-- All Category --</option>
    <?php
    $idcategory = '';
    foreach ($selectcategory as $cate) 
    {
        if($idcategory != $cate->idcat)
            echo '<optgroup label="'.$cate->category.'">';
        if($id_subcat==($cate->idsub)) {$selected='selected';} else {$selected='';}
            echo '<option value="'.$cate->idsub.'" '.$selected.'>'.$cate->subcategory.'</option>';
            $idcategory = $cate->idcat;
    }
    ?>
</select>
<?php echo form_close()?>

<!-- Search Term -->
<table id="browse" class="table table-striped table-bordered" style="font-size: 13px">
	<thead>
        <tr>
            <th>No</th>
            <th>Invitation Number</th>
            <th>Title</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Category</th>
            <th>Sub Category</th>
            <th>Type of Invitation</th>
            <th>Participation</th>
        </tr>
	</thead>
    <tbody>
<?php			
if($browse){ $i=0;
	foreach($browse as $row){
        $xcode = md5($row->id.$username);
        $unicode = substr($xcode,5,1) . $xcode . substr($xcode,-15,10);
        ?>	
		<tr>
			<td><?php echo $num = $num+1; ?></td>
            <td>
                <?php
                echo anchor('avl/bidding_detail/'.$row->id.'/'.$unicode, $row->code.' ',array('class'=>'default_link','title'=>'Detail Bidding')); 
                ?>
            </td>
			<td><?php echo $row->title?$row->title:'-'; ?></td>
			<td><?php echo $row->publish_date?$row->first_valdate:'-'; ?></td>
            <td><?php echo $row->publish_date?$row->end_valdate:'-'; ?></td>
			<td><?php echo array_key_exists($row->id_cat, $category)?$category[$row->id_cat]:'-'; ?></td>
			<td><?php echo array_key_exists($row->id_subcat, $subcategory)?$subcategory[$row->id_subcat]:'-'; ?></td>
            <td><?php echo array_key_exists($row->subcontent_type, $type)?$type[$row->subcontent_type]:'-'; ?></td>
            <td><?php echo $row->participant_status != '3'?'Participant':'Winner'; ?></td>
		</tr>
<?php } ?>	
<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?><tr><td colspan="9">-</td></tr><?php } ?>
    </tbody>
</table>
