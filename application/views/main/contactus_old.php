<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<title>E-Procurement Garuda</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/contact_style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/skins/red-blue/blue.css" id="colors" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/layerslider.css"/> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/component.css" />
	<!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/iCheck/square/blues.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reset.css">
	<link rel="icon" href="<?php echo base_url(); ?>assets/favicon-grd.png" sizes="16x16">
</head>
<body>
<div id="wrapper">
	<header id="header" class="container-fluid">
	  <div class="header-bg">
		<div class="container">
			<div class="decoration dec-left visible-desktop"></div>
			<div class="decoration dec-right visible-desktop"></div>	  
			<div class="row">
				<div id="logo" class="span5"><a href="index.html" class="logo" alt="E-Procurement Garuda Indonesia"></a></div>
				<div class="span7">
					
					<ul class="social-icons">
						<li class="eng" title="English"><a href="#">English</a></li>
						<li class="idn" title="Indonesian"><a href="#">Indonesian</a></li>
					</ul>				
				</div>	
			</div>

			<div class="row">
				<div class="span12" style="font-size: 13px;">
					<div class="select-menu hidden-desktop">
						<select id="selectMenu">
							<option selected value="<?php echo base_url(); ?>auth/login">Home</option>			
							<option value="<?php echo base_url(); ?>main/news">News</option>						
							<option value="#">FAQs</option>
							<option value="<?php echo base_url(); ?>main/contact_us">Contact us</option>
						</select>
					</div>
					<ul id="menu" class="visible-desktop">
						<li>							
							<?php  echo anchor('auth/login','HOME'); ?>
						</li>
						<li><?php  echo anchor('main/news','NEWS'); ?></li>
						<li><a href="#">FAQs</a></li>
						<li><?php  echo anchor('main/contact_us','CONTACT US'); ?></li>
						<!-- Search Form -->
						<li class="search-form visible-dekstop" style="float: right;top: 10px;">
							<form method="get" action="#">
								<input type="text" value="Search" class="search-text-box"/>
								<input type="submit" value="" class="search-text-submit"/>
							</form>
						</li>
					</ul>
				</div>
			</div>
			<div class="row visible-desktop">
				<div class="span12 main-slider"><!-- SLIDER --></div>
			</div>
		</div>
	  </div>
	</header>

	<section class="contact_us_container">
		<div class="container">
			<div class="row">
				<div class="span12" style="text-align:center;"> <!-- section title -->
					<h2>Get In Touch With Us</h2>
					<p>Feel free to contact Garuda Indonesia Business Support if you have any questions or inquiries about us and our services. Our staff will always be ready to respond as soon as possible to ensure that all of your need and concern are being addressed immediately.</p>
				</div> <!-- End section title -->
				<div class="span8 map float-left"> <!-- form holder -->
					<!-- <div class="map"> -->
						<style type="text/css">
						.overlay {
						   background:transparent; 
						   position:relative; 
						   width:100%;
						   height:450px; /* your iframe height */
						   top:450px;  /* your iframe height */
						   margin-top:-430px;  /* your iframe height */
						   margin-right: 25px;
						}
						</style>
						<div class="overlay" onClick="style.pointerEvents='none'"></div>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12021.800034061933!2d106.63748515696331!3d-6.133446872252365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x840e34d171919f67!2sGaruda+Indonesia+City+Center!5e0!3m2!1sid!2sid!4v1459331703516" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				<div class="span4 pull-right address">
					<address>
						<div class="icon_holder float_left"><span class="icon icon-Pointer"></span></div>
						<div class="address_holder float_left">
						<strong>Address</strong><br>
						<?php	echo file_get_contents($path); ?>
						</div>
					<!--/address>
					<address class="clear_fix">
						<div class="icon_holder float_left"><span class="icon icon-Plaine"></span></div>
						<div class="address_holder float_left"><strong>Email</strong><br>business-support@garuda-indonesia.com</div>
					</address-->
				</div>
			</div>
		</div>
	</section>

</div>
<footer class="main-footer footer-line container-fluid text-center">
	Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
</footer>
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.color.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/SmoothScroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<script src="<?php echo base_url(); ?>assets/js/classie.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sidebarEffects.js"></script>
<script src="<?php echo base_url(); ?>assets/js/index.js"></script>

</body>
</html>