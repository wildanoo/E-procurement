<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Category extends Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_category', 'm_category');
		$this->load->model('crud', 'crud');
		$this->load->library('session');
		
		$this->limit = 5;
		islogged_in();
		$this->load->model('m_user');
		islogged_in();
		$this->m_user->authenticate(array(43));
	}	
		
	function index(){
		//if ($this->tank_auth->is_logged_in()) {

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			
			//$where =  array('status'=>'1');
			
			$sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');

			
			$field      = $sess_field != '' ? $sess_field : "init_code";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
			
			if($sess_cat != ''){	$where = array('init_code'=>$sess_cat,'status'=>'1');
			} else { $where = array('status'=>'1');	}
			
			//$sess_like  = $this->session->flashdata('like');

	 		$table 	    = "category";		
			$page       = $this->uri->segment(3);
			$per_page   = 10;		
			$offset     = $this->crud->set_offset($page,$per_page, $where);
			$total_rows = $this->crud->get_total_record("",$table,$where);
			$set_config = array('base_url'=> base_url().'/category/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
			$config     = $this->crud->set_config($set_config);		
				 
			$this->load->library('pagination');			 
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();			
				
			$order 			     = array('field'=>'created_date','order'=>'DESC');
			$data['pagination']  = $paging;
			$data['num']         = $offset; 
			$select 			 = "id,init_code,category,status,remark,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
			
			$sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
			$data['browse'] = $browse;
			
			$category = $this->crud->browse("","category","","","true","id,init_code");
			if(!$category) $category = array();
			$select = array(''=>'All'); $options = array();
			foreach($category as $val){ $options[$val->init_code] = $val->init_code; }
			$data['init_code'] = $select + $options;
			
			$category_ = $this->crud->browse("","category","","","true","id,category,init_code");
			if(!$category_) $category_ = array();
			$select_ = array(''=>'All'); $options_ = array();
			foreach($category_ as $val_){ $options_[$val_->id] = $val_->category; }
			$data['category'] = $select_ + $options_;
						
			$data['view']	= "supplier/category/browse";
			$this->load->view('layout/template',$data);

	 		
// 		} else {  
// 			$this->session->set_flashdata('message','user not authorized');
// 			redirect('/auth/login/');	 
// 		}
	}	
	
	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
	
		$search_conditions = array(
				'category'		=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		$data['view'] = "supplier/category/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{
		/* get data for table view from database with pagination */
	
		$table 	     = "category t1";
		$select 	 = "id,init_code,category,remark,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'country/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
	
		/* get data for table view from database with pagination */
	
		$table 	     = "category t1";
		$select 	 = "id,init_code,category,remark,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,'t1.status = "1"',$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'category/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
    
    function get_cat_tags(){ 	
	 	$term   = $_GET['term'];  	
	 	$order  = array('field'=>'category','sort'=>'ASC'); $select = "category as name";
	 	$result = $this->crud->autocomplete("","category",$select,"category",$term,"",$order);  	
	 	echo json_encode($result);   	 		
	}
	
	function get_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'category','sort'=>'ASC'); $select = "category as name";
		$result = $this->crud->autocomplete("","category",$select,"category",$term,"",$order);
		echo json_encode($result);
	}
	
	function set_sess_search(){
		$like = $_POST['search']; 	
	 	$this->session->set_flashdata('like',$like); 
	}
	
	function is_exist1(){
		$category	  = $_POST['category'];
		$remark		  = $_POST['remark'];
		$is_exist['1']= !$category ? "false" : "true";
		$is_exist['2']= !$remark ? "false" : "true";
		$msg['1']	  = !$category ? "category name required" : "";
		$msg['2']	  = !$remark ? "description required" : "";
	
		if ($is_exist['1']=='true'){
			$where   = array('category'=>$category,'status'=>'1');
			$checked = $this->crud->is_exist("","category","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate category name" : "";
	
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status, 'msg1'=>$msg['1'], 'msg2'=>$msg['2']);
	
		echo json_encode($result);
	}
	
	function is_exist2(){
		$category	  = $_POST['category'];
		$remark   	  = $_POST['remark'];
		$is_exist['1']= !$category ? "false" : "true";
		$is_exist['2']= !$remark ? "false" : "true";
		$msg['1']	  = !$category ? "category name required" : "";
		$msg['2']	  = !$remark ? "description required" : "";
	
		if ($is_exist['1']=='true'){
			$where   = array('category'=>$category,'remark'=>$remark,'status'=>'1');
			$checked = $this->crud->is_exist("","category","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate category name" : "";
	
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status, 'msg1'=>$msg['1'], 'msg2'=>$msg['2']);
	
		echo json_encode($result);
	}
	
	function create(){
		$init_code  = $this->m_category->category_code("category");
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array('id'=>null,
							'init_code'=>$init_code,
							'category'=>$_POST['category'],
							'remark'=> $_POST['remark'],
							'created_id'=>$userID,
							'created_date'=>$curr_date,
							'last_updated'=>$curr_date);
		$where   = array('category'=>$_POST['category'],'status'=>'0');
		$checked = $this->crud->is_exist("","category","id",$where);
	
		if ($checked){ 
			$id = $this->crud->browse("","category","category",$_POST['category'],"false","id")->id;
			$update = array('status'=>'1','remark'=>$_POST['remark']);
			$this->crud->update("","category","id",$id,$update);
		} else {	$id = $this->crud->insert("","category",$data);	}
		
		$this->session->set_flashdata('message','1 data success insert');
	}
	
	function form_update(){
		$id 				   = $this->uri->segment(3);
		$select 			   = "id,remark,init_code,(SELECT id FROM category r WHERE r.id=l.id_cat) as id_cat, subcategory,created_date,last_updated";
		$def				   = $this->crud->browse("","subcategory l","id",$id,"false",$select);
		$data['def']  		   = $def ;
		$category 			   = $this->crud->browse("","category","","","true","id,category");
		
		$id   = $this->uri->segment(3);
		$select = "id,remark,init_code,category,created_id,last_updated";
		$data ['def'] = $this->crud->browse ( "", "category", "id", $id, "false", $select);
		$this->load->view ( 'supplier/category/form_update', $data );
	}	
	
	function form_create(){
		$data ['user_id'] = $this->tank_auth->get_user_id ();
		$data ['username'] = $this->tank_auth->get_username ();		
		$this->load->view('supplier/category/form_create', $data);
	}
	
	function update(){
		$curr_date =  date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data = array('category'=>$_POST['category'],'remark'=>$_POST['remark'],'status'=>'1','last_updated'=>$curr_date);
		$this->crud->update("","category","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
	}
	
	function delete(){
		$id = $this->uri->segment(3);
		$status = array('status'=>'0');
		$this->crud->update("","category","id",$id,$status);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('category/','refresh');
	}
	
	function delete_(){
	
		$id = $this->uri->segment(3);
	
		$where   = array('id_cat'=>$id);
		$checked1 = $this->crud->is_exist("","subcategory","id",$where);
		
		//$where1  = array('id'=>$id);
		$checked2 = $this->crud->is_exist("","announce","id",$where);
		$where1	  = array('id_category'=>$id);
		//$checked3 = $this->crud->is_exist("","vendor","id",$where1);
	
		//if ($checked1 || $checked2 || $checked3){
		if ($checked1 || $checked2){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		}
		else {
			$this->crud->delete("","category","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
		
		redirect('category/','refresh');
	}
	
	function set_sess_category(){
	
		$type = $_POST['category'];
		if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
		} else { $this->session->unset_userdata('sess_cat'); }
		
		$type = $_POST['field'];
		if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
		} else { $this->session->unset_userdata('sess_field'); }
	
	}
	
	function subcategory(){
		if ($this->tank_auth->is_logged_in()) {
	
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			//$where      = array('status'=>'1');
			
			//$sess_like  = $this->session->flashdata('like');
			
			$sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
			
				
			$field      = $sess_field != '' ? $sess_field : "init_code";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
				
			if($sess_cat != ''){	$where = array('init_code'=>$sess_cat,'status'=>'1');
			} else { $where = array('status'=>'1');	}
	
			$table 	    = "subcategory";
			$page       = $this->uri->segment(3);
			$per_page   = 10;
			$offset     = $this->crud->set_offset($page,$per_page,$where);
			$total_rows = $this->crud->get_total_record("",$table,$where);
			$set_config = array('base_url'=> base_url().'/category/subcategory','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
			$config     = $this->crud->set_config($set_config);
				
			$this->load->library('pagination');
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();
	
			$order 			     = array('field'=>'created_date','order'=>'DESC');
			$data['pagination']  = $paging;
			$data['num']         = $offset;
			$select 			 = "id,init_code,status,(SELECT category FROM category r WHERE r.id=l.id_cat) as id_cat, subcategory,(SELECT username FROM users WHERE id=created_id)as created_id,remark,created_date,last_updated";
	
			$sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
			
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
			$data['browse'] = $browse;
			
			$subcategory = $this->crud->browse("","subcategory","","","true","id,init_code");
			if(!$subcategory) $subcategory = array();
			$select = array(''=>'All'); $options = array();
			foreach($subcategory as $val){ $options[$val->init_code] = $val->init_code; }
			$data['init_code'] = $select + $options;
			
			$subcategory_ = $this->crud->browse("","subcategory","","","true","id,subcategory");
			if(!$subcategory_) $subcategory_ = array();
			$select_ = array(''=>'All'); $options_ = array();
			foreach($subcategory_ as $val_){ $options_[$val_->id] = $val_->subcategory; }
			$data['subcategory'] = $select_ + $options_;

			//$data['browse']      = $browse;	
		 	$data['view']	= "supplier/subcategory/browse"; 		
 			$this->load->view('layout/template',$data);
	
		} else {  
			$this->session->set_flashdata('message','user not authorized');
			redirect ( '/auth/login/' ); 
		}
	}
	
	function search2(){
	
		/* initiate search inputs */
	
		//$search_dateranges = array('publish_date',$_POST['start_date'],$_POST['end_date']);
		$search_conditions = array(
				'category'			=> $_POST['search_term'],
				'subcategory'		=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch2(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];	
	
		$data['view'] = "supplier/subcategory/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables2($limit = NULL)
	{
		/* get data for table view from database with pagination */
	
		$table 	     = "subcategory t1";
		$select 	 = "t1.id,init_code,status,(SELECT category FROM category r WHERE r.id=t1.id_cat) as id_cat, subcategory,(SELECT username FROM users WHERE id=t1.created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		// if($sessID)	$getData = $this->crud->browse("",$table,"id",$sessID,"true",$select,$where);
		// else  {
		$joins[0][0] = 'category t2';
		$joins[0][1] = 't2.id = t1.id_cat';
		$joins[0][2] = 'left';
	
		//SELECT t1.id_country, t2.country_name FROM region t1 left join country t2 on t1.id_country = t2.id
	
		// $where		 = !$this->session->userdata('is_admin') ? "t1.subcontent_type IN ('4','5') AND t1.status = '3'":"";
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'/category/subcategory','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch2($limit = NULL,$dateranges = array(),$wherearray = array())
	{
		$joins[0][0] = 'category t2';
		$joins[0][1] = 't2.id = t1.id_cat';
		$joins[0][2] = 'left';
		/* get data for table view from database with pagination */
	
		$table 	     = "subcategory t1";
		$select 	 = "t1.id,t1.init_code,status,(SELECT category FROM category r WHERE r.id=t1.id_cat) as id_cat, subcategory,(SELECT username FROM users WHERE id=t1.created_id)as created_id,t1.created_date,t1.last_updated";

		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse_join('',$table,"COUNT(t1.id) AS COUNT",$joins,$where,$dateranges,$wherearray);
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,'t1.status = "1"',$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'category/search2/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
		return $data;
	
	}
	
	function set_sess_subcategory(){
	
		$type = $_POST['subcategory'];
		if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
		} else { $this->session->unset_userdata('sess_cat'); }
	
		$type = $_POST['field'];
		if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
		} else { $this->session->unset_userdata('sess_field'); }
	
	}
	
	function get_subcat_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'subcategory','sort'=>'ASC'); $select = "subcategory as name";
		$result = $this->crud->autocomplete("","subcategory",$select,"subcategory",$term,"",$order);
		echo json_encode($result);
	}
	
	function set_sess_search_subcat(){
		$like = $_POST['search'];
		$this->session->set_flashdata('like',$like);
	}
	
	function is_exist_subcat1(){
		$category	  = $_POST['id_cat'];
		$subcategory  = $_POST['subcategory'];
		$remark		  = $_POST['remark'];
		$is_exist['1']= !$category ? "false" : "true";
		$is_exist['2']= !$subcategory ? "false" : "true";
		$is_exist['3']= !$remark ? "false" : "true";
		$msg['1']	  = !$category ? "category name required" : "";
		$msg['2']	  = !$subcategory ? "subcategory name required" : "";
		$msg['3']	  = !$remark ? "description required" : "";
	
		if ($is_exist['1']=='true' && $is_exist['2']=='true'){
			$where   = array('id_cat'=>$category,'subcategory'=>$subcategory);
			$checked = $this->crud->is_exist("","subcategory","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$is_exist['2']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate category name" : "";
			$msg['2']	= $checked ? "duplicate subcategory name" : "";
	
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2'], 'msg3' =>$msg['3']);
	
		echo json_encode($result);
	}
	
	function is_exist_subcat2(){
		$category	  = $_POST['id_cat'];
		$subcategory  = $_POST['subcategory'];
		$remark		  = $_POST['remark'];
		$is_exist['1']= !$category ? "false" : "true";
		$is_exist['2']= !$subcategory ? "false" : "true";
		$is_exist['3']= !$remark ? "false" : "true";
		$msg['1']	  = !$category ? "category name required" : "";
		$msg['2']	  = !$subcategory ? "subcategory name required" : "";
		$msg['3']	  = !$remark ? "description required" : "";
	
		if ($is_exist['1']=='true' && $is_exist['2']=='true'){
			$where   = array('id_cat'=>$category,'subcategory'=>$subcategory,'remark'=>$remark,'status'=>'1');
			$checked = $this->crud->is_exist("","subcategory","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$is_exist['2']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate category name" : "";
			$msg['2']	= $checked ? "duplicate subcategory name" : "";
	
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2'], 'msg3' =>$msg['3']);
	
		echo json_encode($result);
	}
	
	function form_create_subcat(){
		$category = $this->crud->browse("","category","","","true","id,category");
		if(!$category) $category = array();
		$select = array(''=>'-- Select --');
		foreach($category as $val){ $options[$val->id] = $val->category; }
		$data['category'] = $select + $options;
	
		$this->load->view('supplier/subcategory/form_create',$data);
	}
	
	function create_subcat(){
		$init_code  = $this->m_category->category_code("subcategory");
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array('id'=>null,
							'init_code'   =>$init_code,
							'id_cat'	  =>$_POST['id_cat'],
							'subcategory' =>$_POST['subcategory'],
							'remark'	  =>$_POST['remark'],
							'created_id'  =>$userID,
							'created_date'=>$curr_date,	
							'last_updated'=>$curr_date);
		$where   = array('id_cat'=>$_POST['id_cat'],'subcategory'=>$_POST['subcategory'],'status'=>'0');
		$checked = $this->crud->is_exist("","subcategory","id",$where);
	
		if ($checked){ 
			$id = $this->crud->browse("","subategory","subcategory",$_POST['subcategory'],"false","id")->id;
			$update = array('status'=>'1','remark'=>$_POST['remark']);
			$this->crud->update("","subcategory","id",$id,$update);
		} else {	$id = $this->crud->insert("","subcategory",$data);	}
		
		$this->session->set_flashdata('message','1 data success insert');
	}
	
	function update_subcat(){
		$curr_date 	= date('Y-m-d H:i:s');
		$id   = $this->uri->segment(3);
		$data = array('id_cat'  =>$_POST['id_cat'],
					  'subcategory' =>$_POST['subcategory'],
					  'status'=> '1',
					  'remark'=>$_POST['remark'],
					  'last_updated'=> $curr_date);
		$this->crud->update("","subcategory","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
	}
	
	function form_update_subcat(){
		$id 				   = $this->uri->segment(3);
		$select 			   = "id,init_code,remark,(SELECT id FROM category r WHERE r.id=l.id_cat) as id_cat, subcategory,created_date,last_updated";
		$def				   = $this->crud->browse("","subcategory l","id",$id,"false",$select);
		$data['def']  		   = $def ;
		$category 			   = $this->crud->browse("","category","","","true","id,category");
		if(!$category) $category = array();
		$select				   = array(''=>'-- Select --');
		foreach($category as $val){ $options[$val->id] = $val->category; }
		$data['category'] 	  = $select + $options;
		$this->load->view('supplier/subcategory/form_update',$data);
	}
	
	function delete_subcat(){
	
		$id = $this->uri->segment(3);
		$status = array('status'=>'0');
		$this->crud->update("","subcategory","id",$id,$status);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('category/subcategory/','refresh');
	}

	function delete_subcat_(){
	
		$id = $this->uri->segment(3);
		
		$where1   = array('id_subcat'=>$id);
		$checked1 = $this->crud->is_exist("","announce","id",$where1);
		
		//$where2   = array('id_subcat'=>$id);
		//$checked2 = $this->crud->is_exist("","vendor","id",$id,$where2);
		
		if ($checked1){ //|| $checked2){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		}
		else {
			$this->crud->delete("","subcategory","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
		
		//$data = array('status'=>'0');
		//$this->crud->update("","subcategory","id",$id,$data);
		redirect('category/subcategory/','refresh');
	}
}	
?>