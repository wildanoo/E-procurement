<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Main extends Controller {

	function __construct()
	{
		parent::__construct();
	}	
 

 public function index(){ 	
 	$id  = $this->uri->segment(3);
 	$data['read']     = $this->crud->browse("","announce","id",$id,"false");
 	$data['archived'] = $this->crud->browse("","announce","status","4","true"); 	
 	$data['category'] = array('1'=>'News','2'=>'Open Sourcing','3'=>'Open Bidding'); 	
 	$data['imgpath']  = "./uploads/images/";
 	$data['ctpath']   = "./uploads/contents/";
 	$data['srcpath']  = SRC_PATH."/uploads/images/";	
 	
 	$this->load->view('main/news',$data); 	 	 		
 } 
 
 public function news(){	
 	 	
 	$read 			  = $this->crud->browse("","announce","","","false");	
 	$data['read']     = !$read ? "" : $read[0];
 	$data['archived'] = $this->crud->browse("","announce","","","true");
 	$data['category'] = array('1'=>'News','2'=>'Open Sourcing','3'=>'Open Bidding'); 	
 	$data['imgpath']  = "./uploads/images/";
 	$data['ctpath']   = "./uploads/contents/";
 	$data['srcpath']  = SRC_PATH."/uploads/images/";
 	$this->load->view('main/news',$data);
 	
 }
 
 public function about_us(){

	$select     = "id,id_cat,(SELECT category FROM category WHERE id=id_cat) as category,subcategory";
	$order      = array('field'=>'id_cat','sort'=>'ASC');
	$db_subcat  = $this->crud->browse("","subcategory","","","true",$select,$where="",$order);
	$db_subcat  = $this->crud->grouping_objdata($db_subcat,"category");
	
	$data['category'] = $db_subcat;
	$data['path'] 	  = $this->lang->line('about_us_file');

    $this->load->view($this->lang->line('about_us_page'),$data);
 }
 
 
  public function contact_us(){

	$select     = "id,id_cat,(SELECT category FROM category WHERE id=id_cat) as category,subcategory";
	$order      = array('field'=>'id_cat','sort'=>'ASC');
	$db_subcat  = $this->crud->browse("","subcategory","","","true",$select,$where="",$order);
	$db_subcat  = $this->crud->grouping_objdata($db_subcat,"category");
	
	$data['category'] = $db_subcat;
	$data['path'] 	  = $this->lang->line('contact_us_file');

    $this->load->view($this->lang->line('contact_us_page'),$data);
 	
 }
 
 function message(){
 	$select = array('id'=>null,'name'=>$_POST['name'],'email'=>$_POST['email'],'subject'=>$_POST['subject'],'message'=>$_POST['message']);
 	$data = $this->crud->insert("","message",$select);
 	redirect('main/contactus/','refresh');
 }
 
 public function contactus_ind(){
 
 	$data['path'] = "./uploads/cms/contact_ind.txt";
 	$this->load->view('main/contactus_ind',$data);
 
 }
 
 public function faq(){

	$select     = "id,id_cat,(SELECT category FROM category WHERE id=id_cat) as category,subcategory";
	$order      = array('field'=>'id_cat','sort'=>'ASC');
	$db_subcat  = $this->crud->browse("","subcategory","","","true",$select,$where="",$order);
	$db_subcat  = $this->crud->grouping_objdata($db_subcat,"category");
	
	$data['category'] = $db_subcat;
	$data['path'] 	  = $this->lang->line('faq_file');

 	$this->load->view($this->lang->line('faq_page'),$data);
 
 }
 
 public function faq_ind(){
 	
 	$attpath = "./uploads/attach/faq.pdf";
 	$attfile = file_exists($attpath) ? "true" : "false";
 	$data['attfile'] = $attfile=="false" ? "-"  : "faq.pdf";
 
 	$data['path'] = "./uploads/cms/faq_ind.txt";
 	$this->load->view('main/faq_ind',$data);
 
 }
 
 function open_content_pdf(){
//  	$id       = $this->uri->segment(3);
//  	$datefile = $this->file_pdf();
//  	$default  = $this->crud->browse("","policy_agreement_temp","id",$id,"false","deskripsi");
 	$ctpath   = "./uploads/attach/faq.pdf";
 	$filename = "faq.pdf";
 	 
 	//print_r($filename);exit;
 	//echo file_get_contents($ctpath);
 	 
 	header('Content-type: application/pdf');
 	header('Content-Disposition: inline; filename="' . $filename . '"');
 	header('Content-Transfer-Encoding: binary');
 	header('Content-Length: ' . filesize($ctpath));
 	header('Accept-Ranges: bytes');
 	@readfile($ctpath);
 }
 
}
?>