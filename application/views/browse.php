<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){	 
	 $(function(){  });
		var baseUrl = "<?php echo base_url(); ?>";

		$('#subcontent').change(function(){ 
			var subcontent = $('#subcontent').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'content/set_sess_subcontent',			   
               data : 'field=id&subtype_name='+subcontent,
               success: function(data){ 
                   //alert(status);	
					//$('#debug').html(resp);
					 //dialogRef.close();				 	
				 	document.location.href= baseUrl + 'content/subcontent/';
			   }		   
            });	
		
		});
		
		$("#srch-term").autocomplete({
			source: baseUrl + 'content/get_subcontent_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
	        var search = $("#srch-term").val(); // alert(search);
	        var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
	        $.ajax({
	           url  : baseUrl + 'content/set_sess_search_subcontent',
	           type : "POST",              		               
	           data : csrf +'='+ token +'&search='+search,
	           success: function(resp){    $("#debug").html(resp);            		            
				    document.location.href= baseUrl + 'content/subcontent';
	           }
	        });
	    });
	    	 
	 $('#btn_create').click(function(){ 
	 	  form_create();
	 	  //alert('test');
	 });	
});

	function form_create(){
		var baseUrl = "<?php echo base_url(); ?>";		 
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		BootstrapDialog.show({
		title: 'Create Sub Content',
        message: $('<div></div>').load(baseUrl + "content/form_create_subcontent/"),
        buttons: [{ label: 'Submit',
	                cssClass: 'btn btn-primary btn-sm',		               
	                action: function(dialogRef){
		                 var content_id	   = $("#content_id").val();
	                	 var subtype_name  = $("#subtype_name").val(); 
	                	 var document_req  = $("#document_req").val;
	                	 var behavior_id   = $("#behavior_id").val;
	                	 
	                	 $.post( baseUrl + 'content/is_exist_subcontent', { csrf: token , content_id : content_id , subtype_name : subtype_name , document_req : document_req , behavior_id : behavior_id })
						 .done(function( resp ) {
								 //$('#debug').html(resp);
								 //dialogRef.close(); 
								var json = $.parseJSON(resp);
								if (json.status=='true'){
				                	var values = $("#form").serializeArray();
				                	values.push({ name: csrf,value: token });
									values = jQuery.param(values);
									 $.ajax({
							               url  : baseUrl + 'content/create_subcontent',
							               type : "POST",              		               
							               data : values,
							               success: function(resp){
							            	   //$('#debug').html(resp);
							            	   //dialogRef.close(); 
								                //alert(values);
							               		document.location.href= baseUrl + 'content/subcontent';	               				               		  
									       }
							            });   
								} else { 	
								$("#msg1").html(json.msg1); 	
								$("#msg2").html(json.msg2); 
							}
	               		 });  
	                }
	            }, {
	            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
			                action: function(dialogRef) {
			                	document.location.href= baseUrl + 'content/subcontent';
			                    dialogRef.close(); 
			                }
		        }]
    });
}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Sub Content',
            message: $('<div></div>').load(baseUrl + "content/form_update_subcontent/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef)
		                {
							 var content_id   = $("#content_id").val();
		                	 var subtype_name = $("#subtype_name").val(); 
		                	 var document_req = $("#document_req").val();
		                	 
		                	 $.post( baseUrl + 'content/is_exist_subcontent', { csrf: token ,content_id : content_id, subtype_name : subtype_name , document_req : document_req })
							 .done(function( resp ) {
								 //alert(id);
//									 $('#debug').html(resp);
//									 dialogRef.close(); 
									var json = $.parseJSON(resp);
									if (json.status=='true'){
					                	var values = $("#form").serializeArray();
					                	values.push({ name: csrf,value: token });
										values = jQuery.param(values);
										 $.ajax({
								               url  : baseUrl + 'content/update_subcontent', 
								               type : "POST",              		               
								               data : values,
								               success: function(resp){
									               //alert(values);
									                //$('#debug').html(resp);
							            	   //dialogRef.close(); 
								               		document.location.href= baseUrl + 'content/subcontent';	               				               		  
										       }
								            });   
									} else { 	
										 $("#msg1").html(json.msg1); 	
										 $("#msg2").html(json.msg2);
								}
		               		 });  
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'content/subcontent';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>
<script src="<?php echo base_url()?>assets/js/sorttable.js"></script>
</head>

<body>
 <div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Sub Content</h3>
              </div><!-- /.box-header -->

                <br>
                
                <div class="col-md-2 pull-left">
                    <input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Sub Content" value="New Sub Content" style="width:130px;" title="Create New Sub Content"/>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
				<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
                </div>

                  <div class = "col-md-10 pull-right">
                  <div class="input-group pull-right" style="width: 200px;">
                      <!-- input type="text" class="form-control input-sm pull-right" name="srch-term" id="srch-term" 
                      	placeholder="Search" style="height: 28px;">
                      <div class="input-group-btn">
                        <button id="btn_search" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div> -->
                        <?php
						$sess_cat = $this->session->userdata('sess_cat');
						$id_cat   = $sess_cat == '' ? '' : $sess_cat;	            
						$prop_cat = 'id=subcontent  style="width: 120px; height: 25px; font-size: 13px"'; ?>
						<?php echo form_dropdown('subcontent', $subcontent, $id_cat,$prop_cat); ?>
                    </div>
                    </div> 
     
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover text-center sortable">
                    <thead>
                      <tr>
                        <!--th><input type="checkbox" name="cba[]" id="cba" /></th-->
                        <th class="table-sorted-reverse">No</th>
                        <th class="">Content</th>
                        <th class="">Sub Content</th>
                        <th class="">Document Requirement</th>
                        <!-- th>Is Active</th> -->
                        <th class="">Created Date</th>
                        <th class="">Updated Date</th>
                        <th class="">Updated Date</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    
                    <tbody>
<?php
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td><?php echo $num = $num+1;; ?></td>
				<td><?php echo $row->content_id; ?></td>
				<td><?php echo $row->subtype_name; ?></td>
				<td style="text-align: left;"><?php echo "{".$row->document_req_id."}"; ?></td>	
				<!-- td><?php   //if($row->status==1) echo "Yes";  else echo "No"; ?></td> -->	
				<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
				<td><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>
				<td><?php echo $row->created_id;?></td>
				<td><?php				
					echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete subcontent ?')){ return true; } else {return false; }";
					echo anchor('content/delete_subcontent/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Sub Content','onclick'=>$js));?>
				</td>
				
			</tr>
<?php	$i++;  } ?>	
</table><div align="center"><?php echo $pagination;?></div>
<?php } else { ?>
	<tr><td colspan="8" align="center">-</td></tr>
<?php } ?>
                      
                    </tbody>

                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->