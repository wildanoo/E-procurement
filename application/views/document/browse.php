<div id="main_container">
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Vendor Document</h3>
		</div>
		<div class="box-body">
			<div style="margin-right: 120px;" class="col-sm-3 col-md-3 pull-right">
				<div style="width: 150px;" class="input-group pull-right">
					<div class="input-group-btn">
						<form enctype="multipart/form-data" novalidate="true" id="form-search" accept-charset="utf-8" method="post" action="document/search">		            <input type="text" style="height: 28px;" name="search_term" id="search_term" value="" placeholder="Search by vendor" class="form-control">
							<button title="Search" type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
							<a class="btn btn-srch btn-warning btn-sm" href="<?php echo base_url() ?>document">Show All</a>
						</form>
					</div>
				</div>
			</div>
			<table class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
				<thead>
					<tr>
						<th width="40">No</th>
						<th width="150">Registration Num.</th> 
						<th width="150">Vendor Num.</th>            
						<th>Vendor Name</th>
						<th width="100">Total File Size</th>
						<th width="50">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach($browse as $row){?>
						<tr>
							<td><?php echo $num = $num+1 ?></td>
							<td><?php echo $row->reg_num ?></td>
							<td><?php if ($row->vendor_num) echo $row->vendor_num; else echo "-"; ?></td>
							<td><?php echo anchor("document/set_sess_document/".$row->id,$row->vendor_name); ?></td>
							<td><?php if ($row->filesize) echo $this->m_document->convert_mb($row->filesize); else echo "-"; ?></td>
							<td></td>
						</tr>

						<?php 
					}?>
					<tr><td colspan="5" style="text-align: center;"><?php echo $pagination;?></td></tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

