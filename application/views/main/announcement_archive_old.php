<div class="about-content-wrap pull-left" id="#">
  <div class="head" style="margin-top: 25px;">
    <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;"><?php echo $this->lang->line('announcement_title');?></h1>
    <a href="#news" class="next-section about animate2">
      <i class="fa fa-angle-double-down"></i>
    </a>
  </div>
  <div class="container" style="margin-top:30px;">
    <div class="dept-title-tabs" style="margin-bottom: 30px;"><?php echo $this->lang->line('announcement_subtitle');?></div>
    <div class="col-xs-12 col-sm-12 col-md-12 no-pad wow fadeInRight animated" data-wow-delay="1s" data-wow-offset="200">
      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 test-box calendar-box">
        <div class="form-group">
          <div class="col-md-12">
            <div class="col-md-5">
              <div class="form-group">
                <div class="input-group date" id="datetimepicker1">
                  <input type="text" class="form-control">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-2 text-center">
              <label class="control-label"><?php echo $this->lang->line('global_search_to_tag');?></label>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <div class="input-group date" id="datetimepicker2">
                  <input type="text" class="form-control">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 test-box" style="padding-bottom: 19px;background-color:#d3d3d3;">
        <div class="form-group">
          <div class="col-md-12">
            <div class="input-group">
              <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
              <div class="input-group-btn">
                <button class="btn btn-sm btn-default btn-src"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 no-pad wow fadeInRight animated" data-wow-delay="1s" data-wow-offset="200">
      <div class="col-md-12 no-pad col-xs-12 col-sm-12 pull-left">
        <div class="table-elements table-custom">
          <table class="table table-bordered table-striped table-responsive">
            <thead>
              <tr>
                <th class="table-custom"><?php echo $this->lang->line('global_regnum_tag');?></th>
                <th class="table-custom"><?php echo $this->lang->line('global_title_tag');?></th>
                <th class="table-custom"><?php echo $this->lang->line('global_startdate_tag');?></th>
                <th class="table-custom"><?php echo $this->lang->line('global_enddate_tag');?></th>
                <th class="table-custom"><?php echo $this->lang->line('announcement_thead_catsub');?></th>
                <th class="table-custom"><?php echo $this->lang->line('announcement_thead_type');?></th>
                <th class="table-custom">Status</th>
              </tr>
            </thead>
            <tbody>
              <?php if (count($browse) > 0) {
              foreach($browse as $row){ ?>
                <tr>
                  <td><?php echo $row->code; ?></td>
                  <td><a href="<?php echo base_url();?>webcontent/page/<?php echo $row->id;?>"> <?php echo $row->title ? $row->title:'<strong>-</strong>'; ?> </a></td>
                  <td><?php echo str_replace("-","/",$row->first_valdate);?></td>
                  <td><?php echo str_replace("-","/",$row->end_valdate);?></td>
                  <td><?php echo $category[$row->id_cat].' - '.$subcategory[$row->id_subcat]; ?></td>
                  <td><?php echo $subcontent[$row->subcontent_type]; ?></td>
                  <td><?php echo array_key_exists($row->status, $status)?$status[$row->status]:'-'; ?></td>
                </tr>
              <?php } ?>
                <!-- <tr><td colspan="9" style="text-align: center;"><?php //echo $pagination;?></td></tr> -->
              <?php } else { ?><tr><td colspan="9">No Event(s) yet.</td></tr><?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div> 
</div>
<div class="row">
  <div class="col-md-12 col-sm-12">
    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate" style="text-align: center;">
      <ul class="pagination">
        <li class="paginate_button previous disabled" id="example2_previous">
          <a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0"><?php echo $this->lang->line('global_pagingprev_tag');?></a>
        </li>
        <li class="paginate_button active">
          <a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0">1</a>
        </li>
        <li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0">2</a>
        </li>
        <li class="paginate_button next" id="example2_next"><a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0"><?php echo $this->lang->line('global_pagingnext_tag');?></a>
        </li>
      </ul>
    </div>
  </div>
</div>

</div>