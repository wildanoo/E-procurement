<?php 
class m_policy_agreement extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function authenticate(){      	
	 	$allowed = array(1);	
		$permit  = $this->session->userdata('permissions');
		$permit  = explode(",",$permit);
		foreach($permit as $str){
		$permissions[] = preg_replace("/[^0-9]/","",$str);}
	 	$result        = array_intersect($allowed, $permissions);
	 	
		if( $this->tank_auth->is_logged_in() 
			&& $result ){ return true;
		} return false;
	 }
	
	function runn_number(){
		
		$counter = $this->crud->get_total_record("","policy_agreement");
		if($counter==0){ $counter=1; } else { $counter= $counter+1;	}
		if(strlen($counter)==1){ $pre='00';	} elseif(strlen($counter)==2){ $pre='0'; } else { $pre='';	 }
		$result = $pre.$counter;
		return $result;
	}

	function browse2($db="",$table,$field="",$id="",$multi="",$select="",$where="",$order="",$group_by="",$like=""){
		if(!$db)$db="db";
		$this->$db->db_select();
		if($where)$this->$db->where($where);
		if(!$select){ $select ="file_name"; } $this->$db->select($select);
		if($like) $this->$db->like($like['col'],$like['field']);
		if($order)$this->$db->order_by($order['field'], $order['sort']);
		if($group_by)$this->$db->group_by($group_by);
		if($id || $id!="") {
			if($multi=="true"){ $result = $this->$db->get_where($table, array($field => $id))->result();
			} else { $result = $this->$db->get_where($table, array($field => $id))->row();}
		} else 	{ $result = $this->$db->get($table)->result();	}
		//echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>';
		return $result;
	}
	
	function get_total_record($db,$table,$where="",$group_by=""){
		if(!$db)$db="db";
		$this->$db->db_select();
		if($where){ $this->$db->where($where); }
		if($group_by){ $this->$db->group_by($group_by); }
	
		return $this->$db->get($table)->num_rows();
	}
	
	function file_name($table){
		$Mdate = date("dm"); $year =  date('Y'); $yearCode =substr($year,2,4);
		$dateNow = $Mdate.$yearCode;
		$counter = $this->get_total_record("",$table);
		if($counter==0){ $counter=1; } else { $counter= $counter+1;	}
		if(strlen($counter)==1){ $pre='00';	} elseif(strlen($counter)==2){ $pre='0'; } else { $pre='';	 }
		$seq = $pre.$counter;
		$namefile = $dateNow.$seq;
		return $namefile;
	}
	
	function file_name_update($table){	
		$Mdate = date("dm"); $year =  date('Y'); $yearCode =substr($year,2,4);
		$dateNow = $Mdate.$yearCode;
		$counter = $this->get_total_record("",$table);
		if($counter==0){ $counter=1; }
		if(strlen($counter)==1){ $pre='00';	} elseif(strlen($counter)==2){ $pre='0'; } else { $pre='';	 }
		$seq = $pre.$counter;
		$namefile = $dateNow.$seq;
		//var_dump($namefile);die;
		return $namefile;
	}	
	
	function set_config($filename,$path,$type,$overwrite=true){		
		
		$config = array();
		$config['upload_path']   = $path;
		$config['allowed_types'] = $type;
		$config['file_name']	 = $filename;
		$config['overwrite']	 = $overwrite;
		
		return $config;
	}
	
	function getMultiple_FILES() {
		$_FILE = array();
		foreach($_FILES as $name => $file) {
			foreach($file as $property => $keys) {
				foreach($keys as $key => $value) {
					$_FILE[$name][$key][$property] = $value;
				}
			}
		}
		return $_FILE;
	}
	
}	
?>