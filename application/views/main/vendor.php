<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<title>e-Procurement Garuda Indonesia</title>

<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/simple-line-icons/simple-line-icons.min.css">

<link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/bootstrap.css" media="screen">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/blue.css" id="style-switch">
<link rel="stylesheet" href="css/bootstrap-rtl.min.css">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets_/css/layout-rtl.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets_/css/blue-rtl.min.css" id="style_color">

<link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.min.css" media="screen">

<link rel="icon" type="image/png" href="images/favicon-grd.png">

<link rel="stylesheet" type="text/css" href="css/slides.css">
<link rel="stylesheet" type="text/css" href="css/inline.min.css">
</head>
<div class="about-content-wrap pull-left" id="#">
    <div class="head" style="margin-top: 25px;">
        <h1><?php echo $this->lang->line('section7');?></h1>
        <a href="#" class="next-section about animate2">
            <i class="fa fa-angle-double-down"></i>
        </a>
    </div>
    
            <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-12 content-tabs-wrap wow fadeInUp animated" data-wow-delay="1.7s" data-wow-offset="200">
                <div class="col-xs-12 col-sm-12 col-md-12 content-tabs no-pad">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab-home">
                            <div class = "row">
                                <div class="col-md-12 tab-content">
                                    <div class="box2 box-info2 tab-pane fade in active" id="registrasi-step-1">
                                        <div class="vendor-type">
                                        <?php  $root = $_SERVER['DOCUMENT_ROOT']; $path = $root."/eproc/uploads/policy_agreement/general_ind.txt";
												if( file_exists($path)) { echo file_get_contents($path);			
												} else { echo ""; }	?>
                                            <p>
                                                <span class="clear">Category : Vendor Policy , 21 Apr 2016</span>&nbsp;&nbsp;
                                            </p>
                                            <p align="center"><strong>VENDOR POLICY</strong></p>
                                            <p style="text-align: center;"><strong class="text-center !important">e-Procurement
                                                GARUDA INDONESIA</strong></p>
                                                <p>&nbsp;</p>
                                                <p><strong>I.&nbsp;&nbsp;&nbsp; </strong><strong><span class="textdecor">SUPPLIER GENERAL REQUIREMENTS</span></strong></p>
                                                <ol>
                                                    <p>1. Supplier is a legal business
                                                        entity with valid and reliable documents evidence. </p>
                                                    <p>2. Supplier is a company that
                                                        passed the supplier evaluation determined by Strategic Sourcing Garuda
                                                        Indonesia.</p>
                                                    <p>3. Supplier proposal admission submitted
                                                        via e-Procurement website of Garuda Indonesia.</p>
                                                    <p>4. Supplier will be legally
                                                        registered as the Approved Supplier of Garuda Indonesia after receiving email confirmation
                                                        from Business Support &amp; General Affairs Unit contained the approval of Supplier
                                                        status.</p>
                                                    <p>5. Supplier must update the
                                                        company information when the informaton is obsolete. If there is any inconsistent
                                                        between information stored in the application with the facts, Garuda has the
                                                        right to cancel the engagement of Supplier in procurement processes.</p>
                                                    <p>6. Supplier must obey Indonesia�s
                                                        government regulation and policy also with Garuda Indonesia regulations applied
                                                        in accordance with goods and/or services procurement.</p>
                                                    <p>7. Supplier status will expire
                                                        if:</p>
                                                    <p>a. Supplier quit the
                                                        participation by sending a formal notification letter addressed to the Garuda
                                                        Indonesia Business Support &amp; General Affairs, and has received the reply
                                                        from Garuda Indonesia to null the supplier status void.</p>
                                                    <p>b. Violate the regulation has
                                                        been set in the Garuda Indonesia regulation.</p>
                                                    <p>c. Failed to meet minimum
                                                        scoring in the periodic performance evaluation conducted by the Strategic
                                                        Sourcing Unit, where the Unit Strategic Sourcing has the right to not inform Supplier
                                                        about the aspects used in the evaluation process.</p>
                                                        <p>d. Upon Garuda Indonesia Policy.</p>
                                                    </ol>
                                                    <p>&nbsp;</p>
                                                    <p><strong>I.&nbsp;&nbsp;
                                                    </strong><strong><span class="textdecor">DOCUMENT REQUIREMENTS OF SUPPLIER
                                                    REGISTRATION</span></strong></p>

                                                    <p><strong><span class="textdecor">Administrative Document Requirement</span></strong>:</p>
                                                    <ol>
                                                        <table border="1" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                            <td width="38" valign="top">
                                                <p align="center"><strong>No.</strong></p>
                                            </td>
                                            <td width="296" valign="top">
                                                <p align="center"><strong>Administrative</strong></p>
                                            </td>
                                            <td width="104" valign="top">
                                                <p align="center"><strong>Indonesian
                                                    Bidders</strong></p>
                                                </td>
                                                <td width="91" valign="top">
                                                    <p align="center"><strong>Foreign
                                                        Bidders</strong></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="38" valign="top">
                                                        <p>1</p>
                                                    </td>
                                                    <td width="296" valign="top">
                                                        <p>Introduction
                                                            &amp; Company Profile</p>
                                                        </td>
                                                        <td width="104" valign="top">
                                                            <p align="center">Yes</p>
                                                        </td>
                                                        <td width="91" valign="top">
                                                            <p align="center">Yes</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="38" valign="top">
                                                            <p>2</p>
                                                        </td>
                                                        <td width="296" valign="top">
                                                        <p>Copy
                                                            of Articles of Association/ Company Registration Number (or equivalent
                                                            documents)</p>
                                                        </td>
                                                        <td width="104" valign="top">
                                                            <p align="center">No</p>
                                                        </td>
                                                        <td width="91" valign="top">
                                                            <p align="center">Yes</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="38" valign="top">
                                                            <p>3</p>
                                                        </td>
                                                        <td width="296" valign="top">
                                                        <p>Copy
                                                            of Certificate of Domicile</p>
                                                        </td>
                                                        <td width="104" valign="top">
                                                            <p align="center">Yes</p>
                                                        </td>
                                                        <td width="91" valign="top">
                                                            <p align="center">Yes</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="38" valign="top">
                                                            <p>4</p>
                                                        </td>
                                                        <td width="296" valign="top">
                                                        <p>Copy
                                                            of Akta Pendirian Perusahaan</p>
                                                        </td>
                                                        <td width="104" valign="top">
                                                            <p align="center">Yes</p>
                                                        </td>
                                                        <td width="91" valign="top">
                                                            <p align="center">No</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="38" valign="top">
                                                            <p>5</p>
                                                        </td>
                                                        <td width="296" valign="top">
                                                        <p>Copy
                                                            of SIUp</p>
                                                        </td>
                                                        <td width="104" valign="top">
                                                            <p align="center">Yes</p>
                                                        </td>
                                                        <td width="91" valign="top">
                                                            <p align="center">No</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="38" valign="top">
                                                            <p>6</p>
                                                        </td>
                                                        <td width="296" valign="top">
                                                        <p>Copy
                                                            of NPWP</p>
                                                        </td>
                                                        <td width="104" valign="top">
                                                            <p align="center">Yes</p>
                                                        </td>
                                                        <td width="91" valign="top">
                                                            <p align="center">No</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="38" valign="top">
                                                            <p>7</p>
                                                        </td>
                                                        <td width="296" valign="top">
                                                        <p>Copy
                                                            of nominated letter as PKP</p>
                                                        </td>
                                                        <td width="104" valign="top">
                                                            <p align="center">Yes</p>
                                                        </td>
                                                        <td width="91" valign="top">
                                                            <p align="center">No</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="38" valign="top">
                                                            <p>8</p>
                                                        </td>
                                                        <td width="296" valign="top">
                                                        <p>Copy
                                                            of recent tax payment (PPh Badan)</p>
                                                        </td>
                                                        <td width="104" valign="top">
                                                            <p align="center">Yes</p>
                                                        </td>
                                                        <td width="91" valign="top">
                                                            <p align="center">No</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="38" valign="top">
                                                            <p>9</p>
                                                        </td>
                                                        <td width="296" valign="top">
                                                        <p>Copy
                                                            of TDP (Tanda Pendirian Perusahaan)</p>
                                                        </td>
                                                        <td width="104" valign="top">
                                                            <p align="center">Yes</p>
                                                        </td>
                                                        <td width="91" valign="top">
                                                            <p align="center">No</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="38" valign="top">
                                                            <p>10</p>
                                                        </td>
                                                        <td width="296" valign="top">
                                                        <p>Copy
                                                            of Company Certification which related to its business and achievement</p>
                                                        </td>
                                                        <td width="104" valign="top">
                                                            <p align="center">Yes</p>
                                                        </td>
                                                        <td width="91" valign="top">
                                                            <p align="center">yes</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="38" valign="top">
                                                            <p>11</p>
                                                        </td>
                                                        <td width="296" valign="top">
                                                            <p>Copy
                                                                of last 2 years corporate annual reports or financial statements. (Audited by
                                                                the public author is preferred)</p>
                                                            </td>
                                                            <td width="104" valign="top">
                                                                <p align="center">Yes</p>
                                                            </td>
                                                            <td width="91" valign="top">
                                                                <p align="center">yes</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="38" valign="top">
                                                                <p>12</p>
                                                            </td>
                                                            <td width="296" valign="top">
                                                            <p>Conflict
                                                                of Interest form</p>
                                                            </td>
                                                            <td width="104" valign="top">
                                                                <p align="center">Yes</p>
                                                            </td>
                                                            <td width="91" valign="top">
                                                                <p align="center">yes</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="38" valign="top">
                                                                <p>13</p>
                                                            </td>
                                                            <td width="296" valign="top">
                                                            <p>Declaration
                                                                of Acceptance form</p>
                                                            </td>
                                                            <td width="104" valign="top">
                                                                <p align="center">Yes</p>
                                                            </td>
                                                            <td width="91" valign="top">
                                                                <p align="center">yes</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="38" valign="top">
                                                                <p>14</p>
                                                            </td>
                                                            <td width="296" valign="top">
                                                                <p>Bidders
                                                                    Representative</p>
                                                                <p>Please
                                                                    provide bidders� representative for all communication with regard to the RFP
                                                                    including </p>
                                                                <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    Company
                                                                    Name</p>
                                                                <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    Person�s
                                                                    Name</p>
                                                                <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    Title</p>
                                                                <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    Mailing
                                                                    Address</p>
                                                                <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    Telephone
                                                                    Number</p>
                                                                <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    Facsimile
                                                                    Number</p>
                                                                <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    Email
                                                                    Address</p>
                                                            </td>
                                                            <td width="104" valign="top">
                                                                <p align="center">Yes</p>
                                                            </td>
                                                            <td width="91" valign="top">
                                                                <p align="center">yes</p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </ol>
                                            <p>&nbsp;</p>
                                            <p><strong>Technical Documents Requirement:</strong></p>
                                            <p>Company is
                                                obliged to enclose credential that describes following:</p>
                                                <ol>
                                                    <p>1. Company Organization
                                                        Structure</p>
                                                    <p>2. Total employees and their qualification
                                                        (for company with experts employee).</p>
                                                    <p>3. Portofolio of
                                                        products/services offered by the company.</p>
                                                    <p>4. Production installed capacity
                                                        and&nbsp; utilized capacity (for manufacture).</p>
                                                    <p>5. Workshop general condition and
                                                        production equipments.</p>
                                                    <p>6. Main equipments and tools
                                                        that are owned by the company utilized to support its operational activities.</p>
                                                    <p>7. Customers list for the last 2
                                                        years.</p>
                                                    <p>8. Pictures that describe the
                                                        situation of the office, workshop, warehouse, business activities, etc.</p>
                                                    </ol>
                                                    <p>&nbsp;</p>
                                                    <p><strong>The Policy of Supplier Registration: </strong></p>
                                                    <p>All submission
                                                        requirements mentioned above are sent in a form of document with requirements
                                                        as follows:</p>
                                                <ol>
                                                    <p>1. Bound/ Booked, with white
                                                        laminated hard cover. The front cover must be entitled with the Company Name
                                                        (Thesis Bound) and business line (Company category).</p>
                                                    <p>2. Paper size HVS A4.</p>
                                                    <p>3. Every part (administration
                                                        and technical) is separated by divider.</p>
                                                    <p>4. Table of content in the first
                                                        page.</p>
                                                    </ol>
                                                    <p>&nbsp;</p>
                                                    <p><strong>Submission Condition:</strong></p>
                                                    <ol>
                                                        <p>1. Addressed to:</p>
                                                    </ol>
                                                    <p><strong><em>&nbsp;</em></strong></p>
                                                    <ol>
                                                        <p><strong><em>PT Garuda
                                                            Indonesia (Persero) Tbk</em></strong></p>
                                                        <p><em>�Supplier Registration � Category (Category of the
                                                            Company suitable with expertise)</em></p>
                                                            <p><em>Unit Business Support &amp; General Affairs/ JKTIBGA</em></p>
                                                            <p><em>Garuda Management Building, Ground Floor</em></p>
                                                            <p><em>Garuda City, Soekarno-Hatta International Airport</em></p>
                                                            <p><em>Cengkareng 19120, Indonesia</em></p>
                                                            <p><em>P.O BOX 1004 TNG BUSH</em></p>
                                                            <p>&nbsp;</p>
                                                        </ol>
                                                        <ol>
                                                            <p>2. Information of selection
                                                                result will be sent to each company by mail or email.</p>
                                                            </ol>
                                                            <p>&nbsp;</p>
                                                            <p><strong>Others:</strong></p>
                                                            <ol>
                                                                <p>1. For every document accepted,
                                                                    Garuda has the right to perform due dilligence included but not restricted by
                                                                    only location visit, confirming, and also checking documents originality;</p>
                                                                <p>2. Evaluation and assessment is sole
                                                                    discretion of Garuda Indonesia based on the suitability level or Garuda needs
                                                                    of goods and/or services fulfillment, quality, and any other technical aspects;</p>
                                                                    <p>3. If it is necessary, Garuda
                                                                        has the right to request additional supporting documents.</p>
                                                            </ol>
                                                        <p>&nbsp;</p>
                                                        <p><strong>II.&nbsp;&nbsp; SUPPLIER PERFORMANCE
                                                            ASSESSMENT</strong>&nbsp;
                                                        </p>
                                                        <ol>
                                                            <p>1. If the result of Supplier�s
                                                                periodic performance evaluation is not satisfying, Garuda has the right to send
                                                                a warning letter to Supplier to improve their performance in 3 months. If there
                                                                is no increase in Supplier�s performance after the given time, Garuda has the
                                                                right to end all agreement and perform document update by conducting re-due
                                                                dilligence process.</p>
                                                            <p>2. Garuda has the right to take Supplier
                                                                status from company that failed the performance evaluation process.</p>
                                                            <p>3. If in the end of the
                                                                agreement period, the perfomance of Supplier is below Garuda�s standard
                                                                (dissatisfying), Garuda has the right to not engage Supplier for 1 time in the
                                                                next procurement process.</p>
                                                        </ol>
                                                        <p>1. Supplier performance
                                                            assessment will be done using 2 method as follows:&nbsp;
                                                            <strong><span class="textdecor">&nbsp;</span></strong></p>
                                                        <ol>
                                                            <p>a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Active rate evaluation, with
                                                                several criteria such as:</p>
                                                            <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                Response rate on procurement invitation</p>
                                                            <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                Active rate on submit proposal </p>
                                                            <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                The compatibility of the proposal submit with the requirement</p>
                                                            <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                Winning rate upon the procurement process</p>
                                                            <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                Disqualification rate or resigning rate from any procurement
                                                                process </p>
                                                            <p>b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Periodic Performance
                                                                Evaluation</p>
                                                            <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                Work Quality</p>
                                                            <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                Services</p>
                                                            <p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                Innovation</p>
                                                        </ol>
                                                        <p>&nbsp;</p>
                                                        <p><strong>III.&nbsp;&nbsp;
                                                        </strong><strong><span class="textdecor">VIOLATIONS</span></strong></p>
                                                        <ol>
                                                        </ol>
                                                        <p>For all suppliers
                                                            of goods and/or services that perform violation, a sanction will be given as the
                                                            category of the violation performed.</p>
                                                            <p>1. Red List</p>
                                                            <p><span class="textdecor">Violation
                                                                types</span>:</p>
                                                <ol>
                                                    <p>a)&nbsp;Submit a writ to the
                                                        procurement unit/committee for the procurement process performed but it cannot
                                                        be legally proven.</p>
                                                    <p>b)&nbsp;Provide goods or perform
                                                        works that are not in accordance with the specification.</p>
                                                    <p>c)&nbsp;Refuse to perform the work
                                                        after being announced as the winner.</p>
                                                    <p>d)&nbsp;Divert the work to other
                                                        parties without acknowledgement of Garuda Indonesia.</p>
                                                    <p>e)&nbsp;Fail to provide
                                                        goods/services as what has been agreed in the contract and cause serious impact
                                                        for the operational of Garuda Indonesia.</p>
                                                    <p>f)&nbsp;Fail to finish the work and
                                                        fail to pay the fine until the end of the time given.</p>
                                                    </ol>
                                                    <p><span class="textdecor">Sanctions</span>:</p>
                                                    <p>Garuda has the
                                                    right to send a warning letter stating that the company is not allowed to
                                                    involved in 2 procurement processes where Supplier is listed.</p>
                                                    <p>2. Black List</p>
                                                    <p><span class="textdecor">Violation
                                                        types</span>:</p>
                                                <ol>
                                                    <p>a)&nbsp;Fake/change documents or
                                                        manipulate data.</p>
                                                    <p>b)&nbsp;Provide fake goods proven by
                                                        statements by related institution/factory/agent.</p>
                                                    <p>c)&nbsp;Govern Corruption, Colusion,
                                                        and Nepotism acts or conspiracy to play the prices among participants or with
                                                        employees of Garauda Indonesia.</p>
                                                    <p>d)&nbsp;Is proven of performing an
                                                        act of giving gratification ro Garuda Indonesia employees related to the
                                                        procurement process.</p>
                                                    <p>e)&nbsp;Is involved in any law
                                                        violation conducts as stated by the law enforcement.</p>
                                                    <p>f)&nbsp;Conduct abuse of document for
                                                        purpose that has no relation with the procurement process and/or has no
                                                        relation with the work process, without any acknowledgement by Garuda.</p>
                                                </ol>
                                                <p><span class="textdecor">Sanctions</span>:</p>
                                                <ol>
                                                    <p>a)&nbsp;Garuda has the right to send
                                                        a letter stated that the company is forbidden to join any procurement in Garuda
                                                        Indonesia environment.</p>
                                                    <p>b)&nbsp;Garuda has the right to
                                                        announce the status of Blacklist for the company mentioned in the website of
                                                        e-Procurement Garuda Indonesia.</p>
                                                </ol>
                                                <p>&nbsp;</p>
                                                <p><strong>IV.&nbsp;&nbsp;
                                                </strong><strong><span class="textdecor">POLICY CORRECTION</span></strong>
                                                </p>
                                                <ol>
                                                </ol>
                                            <p>Garuda is able to
                                                remedy, not limited by only adding or lessening this policy, with or without
                                                announcement beforehand. Supplier is considered as has understood and is
                                                willing to be bound and obey the corrected policy.</p>
                                            <p>If there is any
                                                incompatibility with the company procedure, Supplier can resign from its
                                                involvement as Garuda Supplier.</p>
                                            <p>Supplier candidate is willing
                                                to accept and obey all policies and procedures applied in Garuda. There is no
                                                charge that will be considered by Garuda related with the acceptance or
                                                rejection of a company as Garuda Supplier, included but not limited only on
                                                compensation of all costs appear on this registration process.
                                            </p>
                                        </div>   
                                    </div>              
                                </div>      
                            </div>
                        </div>
                    </div>          
                </div>
            </div>
        </div>
    
</div>