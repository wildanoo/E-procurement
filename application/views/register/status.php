<style>
.body{margin:40px;}

.stepwizard-step p {
    margin-top: 10px;    
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;     
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
    
}

.stepwizard-step {    
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
</style>

	<div class="panel panel-info">
      <div class="panel-heading"><b><?php echo strtoupper($vendor->name);  ?></b></div>
      <div class="panel-body">
      	Registration Number : <b><?php echo $vendor->num; ?></b><br/>  
      	Registration Status : <b><?php echo strtoupper($vendor->sts); ?></b>    	
      </div>
    </div>	
			       
<div class="stepwizard">
    <div class="stepwizard-row">
    	<?php $i=1;     	
    	foreach($step as $val) {     	
    		$class  = ($val->id==$vendor->id_stsreg) ? "btn btn-primary btn-circle" : "btn btn-default btn-circle";  
    		$status = ($val->id==$vendor->id_stsreg) ? "disabled=disabled" : ""; 		
    	?>
        <div class="stepwizard-step">
            <button type="button" class="<?php echo $class; ?>" <?php echo $status; ?> ><?php echo $i; ?></button>
            <p><?php 
            	$string = ucwords($val->status);
            	if (preg_match('/ /',$string)){
					$string = str_replace(" ","<br/>",$string);
					echo ucwords($string);
				} else { echo ucwords($val->status); }
            
            ?></p>
        </div>
        
        <?php $i++; } ?>     
       
    </div>
</div>
        
