<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendorlog extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_vendorlog');
		$this->load->library('Ajax_pagination');
		$this->permit = new stdClass();
		$this->permit = $this->crud->get_permissions("7");

	}

	function index()
	{
		if($this->m_vendorlog->authenticate()){
			if($this->uri->segment(3)){
				$this->kill_sess();
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$data['view'] = "vendor/vendorlog/home";
				$this->load->view('layout/template',$data);
			}else{
				redirect('/auth/login/');
			}
			
		}else{
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/');
		}
		
	}

	function browse(){

		$venID 		= ($this->input->post('venID')) ? $this->input->post('venID') : $this->session->userdata('venID');
		$statID 	= ($this->input->post('data')) ? $this->input->post('data') : $this->session->userdata('statID');
		$this->session->set_userdata(array('venID'=>$venID,'statID'=>$statID));

		if( !empty($statID) && $statID != 'all'){
			$stat = 'true';
			$ID_stat = $statID;
		}else{
			$ID_stat = NULL;
		}

		$page 			= $this->input->post('page');
		if(!$page){
			$offset = 0;
		}else{
			$offset = $page;
		}

		$total_rows 	= count($this->m_vendorlog->get_logs("", $venID, $stat,$ID_stat));

		$config['target']		= '#main_browse';
		$config['base_url']		= site_url().'vendorlog/browse';
		$config['total_rows']	= $total_rows;
		$config['per_page'] 	= 10;

		$this->ajax_pagination->initialize($config);

		$browse = $this->m_vendorlog->get_logs(array('start'=>$offset,'limit'=>10), $venID, $stat,$ID_stat);

		$data['pagination']  = $paging;
		$data['num']         = $offset;

		$data['browse']		= $browse;

		$data['stat']		= $this->m_vendorlog->sort_stat();

		$data['view'] 		= "vendor/vendorlog/browse";
		
		$param_title 		= $this->session->userdata('feature');
		if( $this->session->userdata('feature') != "vendorlist" && $this->session->userdata('feature') != "register"){
			$data['title'] 	= ucfirst($this->session->userdata('feature'))." ";
		}
		
		if($this->input->is_ajax_request())
		{
			$this->load->view($data['view'], $data);
		}
		else
		{
			$this->load->view('layout/template',$data);
		}
	}

	function search(){
		$idven   	= $this->uri->segment(3);
		$stat 		= ($this->uri->segment(4)) ? $this->uri->segment(4) : 'all';

		$this->session->set_userdata(array('venID'=>$idven,'statID'=>$stat,'tab'=>'menu4'));
		redirect('register/set_vendor_session/'.$idven);
		/*redirect('vendorlog/browse');*/
	}

	function kill_sess(){
		$this->session->set_userdata(array('venID'=>'','statID'=>'all'));
	}

	function export_excel(){
		$id 					= $this->uri->segment(3);
		$check 					= !$id ? $this->input->post('ck_list') : $id;
		$filter 				= !$id ? implode(',',$check) : $id;

		$table 			= "vendor_logs t1";
		$select 		= "t1.id,t1.id_vendor,t1.user_id,t1.created_date,t3.username,t2.vendor_name,t1.action,t1.description,t1.reason";
		$joins[0][0]	= "vendor t2";
		$joins[0][1]	= "t1.id_vendor=t2.id";
		$joins[0][2]	= "left";
		$joins[1][0]	= "users t3";
		$joins[1][1]	= "t3.id=t1.user_id";
		$joins[1][2]	= "left";

		$where  		= "t1.id IN($filter)";

		if(!empty($check)){
			$browse 		 	= $this->crud->browse_join("",$table,"","","true",$select,$joins,$where);
			$data['logexcel'] 	= $browse;

			$this->load->view('vendor/vendorlog/export_excel',$data);

		}else{
			$this->index();
		}
	}

}

/* End of file vendorlog.php 
/* Location: ./application/controllers/vendorlog.php */