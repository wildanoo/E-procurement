<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export_1 extends CI_Controller {
	
	var $permit;
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('m_vendor','m_announce','m_export_1','crud'));	islogged_in();	
    }

	public function index()
	{
		header('location: '.base_url());
	}

	function vendor($reg_num = '')
	{
		
		$regnum = $this->m_export_1->cek_reg_num($reg_num);
		if($regnum==0) exit();

		/**
		 * PHPExcel
		 *
		 * Copyright (C) 2006 - 2014 PHPExcel
		 *
		 * This library is free software; you can redistribute it and/or
		 * modify it under the terms of the GNU Lesser General Public
		 * License as published by the Free Software Foundation; either
		 * version 2.1 of the License, or (at your option) any later version.
		 *
		 * This library is distributed in the hope that it will be useful,
		 * but WITHOUT ANY WARRANTY; without even the implied warranty of
		 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
		 * Lesser General Public License for more details.
		 *
		 * You should have received a copy of the GNU Lesser General Public
		 * License along with this library; if not, write to the Free Software
		 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
		 *
		 * @category   PHPExcel
		 * @package    PHPExcel
		 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
		 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
		 * @version    1.8.0, 2014-03-02
		 */

		/** Error reporting */
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Asia/Jakarta');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		$this->load->library('PHPExcel');

		// Create new PHPExcel object
		$obj = new PHPExcel();

		




		//=====================
		//=====================
		//=====================  tabel General Information
		//=====================
		//=====================

		// Add new sheet
		$objWorkSheet = $obj->createSheet(0); //Setting index when creating

		//Mengeset Syle nya
		$headerStylenya = new PHPExcel_Style();
		$bodyStylenya   = new PHPExcel_Style();

		$headerStylenya->applyFromArray(
			array('fill' 	=> array(
				  'type'    => PHPExcel_Style_Fill::FILL_SOLID,
				  'color'   => array('argb' => 'FFEEEEEE')),
				  'borders' => array('bottom'=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'right'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'left'	    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'top'	    => array('style' => PHPExcel_Style_Border::BORDER_THIN)
				  )
			));
		$bodyStylenya->applyFromArray(
			array('fill' 	=> array(
				  'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
				  'color'	=> array('argb' => 'FFFFFFFF')),
				  'borders' => array(
								'bottom'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'right'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'left'	    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'top'	    => array('style' => PHPExcel_Style_Border::BORDER_THIN)
				  )
		    ));
		$objWorkSheet->setSharedStyle($headerStylenya, "A3:L3");
		$objWorkSheet->setSharedStyle($bodyStylenya, "A4:L4");
		$objWorkSheet->setShowGridlines(false);
		$objWorkSheet->setTitle("General Information");

		$select	 = "id as idx, vendor_name as name,vendor_type as type,vendor_address as address, 					
 					postcode,web,phone,fax,email,npwp,npwp_address,npwp_postcode";
 					
		$vendor = $this->crud->browse("","vendor","register_num",$reg_num,"false",$select);	
		

		// Set lebar kolom
		$objWorkSheet->getColumnDimension('A')->setWidth(17);
		$objWorkSheet->getColumnDimension('B')->setWidth(35);
		$objWorkSheet->getColumnDimension('C')->setWidth(12);
		$objWorkSheet->getColumnDimension('D')->setWidth(30);
		$objWorkSheet->getColumnDimension('E')->setWidth(10);
		$objWorkSheet->getColumnDimension('F')->setWidth(20);
		$objWorkSheet->getColumnDimension('G')->setWidth(30);
		$objWorkSheet->getColumnDimension('H')->setWidth(16);
		$objWorkSheet->getColumnDimension('I')->setWidth(16);
		$objWorkSheet->getColumnDimension('J')->setWidth(16);
		$objWorkSheet->getColumnDimension('K')->setWidth(30);
		$objWorkSheet->getColumnDimension('L')->setWidth(30);
		//Write cells
		$objWorkSheet	->setCellValue('A1', 'General Information')
		 				
		 				->setCellValue('A3', 'Registration Num.')
		 				->setCellValue('B3', 'Vendor Name')
		 				->setCellValue('C3', 'Vendor Type')
		 				->setCellValue('D3', 'Address')
		 				->setCellValue('E3', 'Post Code')
		 				->setCellValue('F3', 'NPWP')
		 				->setCellValue('G3', 'NPWP Address')
		 				->setCellValue('H3', 'NPWP Post Code')
		 				->setCellValue('I3', 'Phone')
		 				->setCellValue('J3', 'Fax')
		 				->setCellValue('K3', 'Email')
		 				->setCellValue('L3', 'Web')

		 				->setCellValue('A4', $reg_num)
		 				->setCellValue('B4', $vendor->name)
		 				->setCellValue('C4', $vendor->type)
		 				->setCellValue('D4', $vendor->address)
		 				->setCellValue('E4', $vendor->postcode)
		 				->setCellValue('F4', '\''.$vendor->npwp)
		 				->setCellValue('G4', $vendor->npwp_address)
		 				->setCellValue('H4', $vendor->npwp_postcode)
		 				->setCellValue('I4', '\''.$vendor->phone)
		 				->setCellValue('J4', '\''.$vendor->fax)
		 				->setCellValue('K4', $vendor->email)
		 				->setCellValue('L4', $vendor->web);




		//=====================
		//=====================
		//=====================  tabel Contact Person
		//=====================
		//=====================

		// Add new sheet
		$objWorkSheet = $obj->createSheet(1);

		// Set lebar kolom
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(25);
		$objWorkSheet->getColumnDimension('C')->setWidth(20);
		$objWorkSheet->getColumnDimension('D')->setWidth(16);
		$objWorkSheet->getColumnDimension('E')->setWidth(30);

		$browse = $this->crud->browse("","contact_person","id_vendor",$vendor->idx,"true");


		
		$objWorkSheet	->setCellValue('A1', 'Contact Person')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)
		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Name')
		 				->setCellValue('C4', 'Position')
		 				->setCellValue('D4', 'Mobile')
		 				->setCellValue('F4', 'Email');

		 if($browse){ $i=1;
 			foreach($browse as $row){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row->fullname)
		 						->setCellValue('C'.(4 + $i), $row->position)
		 						->setCellValue('D'.(4 + $i), '\''.$row->mobile)
		 						->setCellValue('E'.(4 + $i), $row->email);
 				$i++;  
 			}
 		}

		// Rename sheet
		 $objWorkSheet->setTitle("Contact Person");












		//=====================
		//=====================
		//=====================  tabel Bank Account
		//=====================
		//=====================

		// Add new sheet
		$objWorkSheet = $obj->createSheet(2);
		// Set lebar kolom
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(20);
		$objWorkSheet->getColumnDimension('C')->setWidth(25);
		$objWorkSheet->getColumnDimension('D')->setWidth(40);
		$objWorkSheet->getColumnDimension('E')->setWidth(25);
		$objWorkSheet->getColumnDimension('F')->setWidth(25);
		 
		$venID   = $vendor->idx;	
		$bank 	= $this->crud->browse("","bank_account","id_vendor",$venID,"true");	

		//Write cells
		$objWorkSheet	->setCellValue('A1', 'Bank Account')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)
		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Account Number')
		 				->setCellValue('C4', 'Account Name')
		 				->setCellValue('D4', 'Account Address')
		 				->setCellValue('E4', 'Bank Name')
		 				->setCellValue('F4', 'Branch Name');

		if($bank){ $i=1;
 			foreach($bank as $row1){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), '\''.$row1->acc_number)
		 						->setCellValue('C'.(4 + $i), $row1->acc_name)
		 						->setCellValue('D'.(4 + $i), $row1->acc_address)
		 						->setCellValue('E'.(4 + $i), $row1->bank_name)
		 						->setCellValue('F'.(4 + $i), $row1->branch_name);
 				$i++;  
 			}
 		}
		 
		// Rename sheet
		 $objWorkSheet->setTitle("Bank Account");
		 




		//=====================
		//=====================
		//=====================  tabel Akta Pendirian
		//=====================
		//=====================

		// Add new sheet
		$objWorkSheet = $obj->createSheet(3);
		// Set lebar kolom
		$objWorkSheet->getColumnDimension('A')->setWidth(5);
		$objWorkSheet->getColumnDimension('B')->setWidth(25);
		$objWorkSheet->getColumnDimension('C')->setWidth(40);
		$objWorkSheet->getColumnDimension('D')->setWidth(20);
		$objWorkSheet->getColumnDimension('E')->setWidth(20);
		$objWorkSheet->getColumnDimension('F')->setWidth(20);
		$objWorkSheet->getColumnDimension('G')->setWidth(20);

		$objWorkSheet->setSharedStyle($headerStylenya, "A4:G4");
		$objWorkSheet->setSharedStyle($bodyStylenya, "A5:G6");
		$objWorkSheet->setShowGridlines(false);

 		$none_akta 		= array('notaris_name'		=> "none",
							 	'notaris_address'	=> "none",
							 	'notaris_phone'		=> "none",
							 	'notaris_number'	=> "none",
							 	'notaris_date'		=> date('Y-m-d'),
							 	'remark'			=> "none");
 		
 		$akta  = $this->crud->browse("","akta","id_vendor",$venID,"false");	
 		$akta2   = !$akta ? (object) $none_akta : $akta;	 

		//Write cells
		$objWorkSheet	->setCellValue('A1', 'Akta Pendirian')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)
		 				
		 				->setCellValue('A4', 'No.')
		 				->setCellValue('B4', 'Notaris Name')
		 				->setCellValue('C4', 'Notaris Address')
		 				->setCellValue('D4', 'Notaris Phone')
		 				->setCellValue('E4', 'Notaris Number')
		 				->setCellValue('F4', 'Notaris Date')
		 				->setCellValue('G4', 'Remark')

		 				->setCellValue('A5', '1')
		 				->setCellValue('B5', $akta2->notaris_name)
		 				->setCellValue('C5', $akta2->notaris_address)
		 				->setCellValue('D5', '\''.$akta2->notaris_phone)
		 				->setCellValue('E5', '\''.$akta2->notaris_number)
		 				->setCellValue('F5', $akta2->notaris_date)
		 				->setCellValue('G5', $akta2->remark);
		 
		// Rename sheet
		 $objWorkSheet->setTitle("Akta Pendirian");
		 




		//=====================
		//=====================
		//=====================  tabel OWNER
		//=====================
		//=====================

		// Add new sheet
		$objWorkSheet = $obj->createSheet(4);
		// Set lebar kolom
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(25);
		$objWorkSheet->getColumnDimension('C')->setWidth(40);
		$objWorkSheet->getColumnDimension('D')->setWidth(20);
		$objWorkSheet->getColumnDimension('E')->setWidth(20);
		$objWorkSheet->getColumnDimension('F')->setWidth(20);
		$objWorkSheet->getColumnDimension('G')->setWidth(20);
		 
		$owner = $this->crud->browse("","owner","id_vendor",$venID,"true");			

		//Write cells
		 $objWorkSheet	->setCellValue('A1', 'Owner')
		 				->setCellValue('A2', 'Vendor Name : '.$vendor->name)
		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Owner Name')
		 				->setCellValue('C4', 'Owner Address')
		 				->setCellValue('D4', 'Owner Phone')
		 				->setCellValue('E4', 'Owner Position')
		 				->setCellValue('F4', 'Owner Shared')
		 				->setCellValue('G4', 'Remark');

		if($owner){ $i=1;
 			foreach($owner as $row2){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row2->owner_name)
		 						->setCellValue('C'.(4 + $i), $row2->owner_address)
		 						->setCellValue('D'.(4 + $i), '\''.$row2->owner_phone)
		 						->setCellValue('E'.(4 + $i), $row2->owner_position)
		 						->setCellValue('F'.(4 + $i), $row2->owner_shared)
		 						->setCellValue('G'.(4 + $i), $row2->remark);
 				$i++;  
 			}
 		}
		 
		// Rename sheet
		 $objWorkSheet->setTitle("Owner");




		//=====================
		//=====================
		//=====================  tabel Pengurus
		//=====================
		//=====================

		// Add new sheet
		$objWorkSheet = $obj->createSheet(5);
		// Set lebar kolom
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(25);
		$objWorkSheet->getColumnDimension('C')->setWidth(40);
		$objWorkSheet->getColumnDimension('D')->setWidth(20);
		$objWorkSheet->getColumnDimension('E')->setWidth(20);
		$objWorkSheet->getColumnDimension('F')->setWidth(20);
		 
		$org = $this->crud->browse("","organization","id_vendor",$venID,"true");		

		//Write cells
		 $objWorkSheet	->setCellValue('A1', 'Pengurus')
		 				->setCellValue('A2', 'Vendor Name : '.$vendor->name)
		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Name')
		 				->setCellValue('C4', 'Address')
		 				->setCellValue('D4', 'Phone')
		 				->setCellValue('E4', 'Position')
		 				->setCellValue('F4', 'Remark');

		if($org){ $i=1;
 			foreach($org as $row3){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row3->name)
		 						->setCellValue('C'.(4 + $i), $row3->address)
		 						->setCellValue('D'.(4 + $i), '\''.$row3->phone)
		 						->setCellValue('E'.(4 + $i), $row3->position)
		 						->setCellValue('F'.(4 + $i), $row3->remark);
 				$i++;  
 			}
 		}
		 
		// Rename sheet
		 $objWorkSheet->setTitle("Pengurus");




		//=====================
		//=====================
		//=====================  tabel Correspondences
		//=====================
		//=====================

		// Add new sheet
		$objWorkSheet = $obj->createSheet(6);
		// Set lebar kolom
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(40);
		$objWorkSheet->getColumnDimension('C')->setWidth(20);
		$objWorkSheet->getColumnDimension('D')->setWidth(40);
		$objWorkSheet->getColumnDimension('E')->setWidth(10);
		$objWorkSheet->getColumnDimension('F')->setWidth(10);
		$objWorkSheet->getColumnDimension('G')->setWidth(20);
		 
		$correspondence = $this->crud->browse("","correspondence","id_vendor",$venID,"true");		

		//Write cells
		$objWorkSheet	->setCellValue('A1', 'Correspondence')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)
		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Document Name')
		 				->setCellValue('C4', 'Document Number')
		 				->setCellValue('D4', 'Description')
		 				->setCellValue('E4', 'Effective Date')
		 				->setCellValue('F4', 'Expired Date')
		 				->setCellValue('G4', 'Remark');

		if($correspondence){ $i=1;
 			foreach($correspondence as $row4){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row4->doc_name)
		 						->setCellValue('C'.(4 + $i), '\''.$row4->doc_num)
		 						->setCellValue('D'.(4 + $i), $row4->desc)
		 						->setCellValue('E'.(4 + $i), $row4->eff_date)
		 						->setCellValue('F'.(4 + $i), $row4->exp_date)
		 						->setCellValue('G'.(4 + $i), $row4->remark);
 				$i++;  
 			}
 		}
		 
		// Rename sheet
		 $objWorkSheet->setTitle("Correspondences");



		//=====================
		//=====================
		//=====================  tabel Reference
		//=====================
		//=====================

		// Add new sheet
		$objWorkSheet = $obj->createSheet(7);// Set lebar kolom
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(25);
		$objWorkSheet->getColumnDimension('C')->setWidth(20);
		$objWorkSheet->getColumnDimension('D')->setWidth(10);
		$objWorkSheet->getColumnDimension('E')->setWidth(10);
		$objWorkSheet->getColumnDimension('F')->setWidth(20);
		 
		$ref = $this->crud->browse("","references","id_vendor",$venID,"true");		

		//Write cells
		$objWorkSheet	->setCellValue('A1', 'Reference')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)
		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Customer Name')
		 				->setCellValue('C4', 'Project')
		 				->setCellValue('D4', 'Point')
		 				->setCellValue('E4', 'Date')
		 				->setCellValue('F4', 'Remark');

		if($correspondence){ $i=1;
 			foreach($correspondence as $row5){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row5->cust_name)
		 						->setCellValue('C'.(4 + $i), $row5->project)
		 						->setCellValue('D'.(4 + $i), $row5->point)
		 						->setCellValue('E'.(4 + $i), $row5->date)
		 						->setCellValue('F'.(4 + $i), $row5->remark);
 				$i++; 
 			}
 		}
		 
		// Rename sheet
		 $objWorkSheet->setTitle("Reference");




		//=====================
		//=====================
		//=====================  tabel Affiliate
		//=====================
		//=====================

		// Add new sheet
		$objWorkSheet = $obj->createSheet(8);
		// Set lebar kolom
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(30);
		$objWorkSheet->getColumnDimension('C')->setWidth(40);
		$objWorkSheet->getColumnDimension('D')->setWidth(20);
		$objWorkSheet->getColumnDimension('E')->setWidth(20);
		 
		$aff = $this->crud->browse("","affiliates","id_vendor",$venID,"true");		

		//Write cells
		$objWorkSheet	->setCellValue('A1', 'Affiliate')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)

		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Company Name')
		 				->setCellValue('C4', 'Competency')
		 				->setCellValue('D4', 'Contact')
		 				->setCellValue('E4', 'Remark');

		if($aff){ $i=1;
 			foreach($aff as $row6){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row6->company_name)
		 						->setCellValue('C'.(4 + $i), $row6->competency)
		 						->setCellValue('D'.(4 + $i), $row6->contact)
		 						->setCellValue('E'.(4 + $i), $row6->remark);
 				$i++;  
 			}
 		}
		 
		// Rename sheet
		 $objWorkSheet->setTitle("Affiliate");




		//=====================
		//=====================
		//=====================  tabel categories
		//=====================
		//=====================

		// Add new sheet
		$objWorkSheet = $obj->createSheet(9);
		// Set lebar kolom
		$objWorkSheet->getColumnDimension('A')->setWidth(3);
		$objWorkSheet->getColumnDimension('B')->setWidth(40);
		$objWorkSheet->getColumnDimension('C')->setWidth(40);
		 
		$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
			   (SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";					
 		$categories = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2);		

		//Write cells
		$objWorkSheet	->setCellValue('A1', 'Categories')
						->setCellValue('A2', 'Vendor Name : '.$vendor->name)
		 				
		 				->setCellValue('A4', 'No')
		 				->setCellValue('B4', 'Category')
		 				->setCellValue('C4', 'Sub Category');

		if($categories){ $i=1;
 			foreach($categories as $row6){
 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
		 						->setCellValue('B'.(4 + $i), $row6->category)
		 						->setCellValue('C'.(4 + $i), $row6->subcategory);
 				$i++;  
 			}
 		}
		 
		// Rename sheet
		 $objWorkSheet->setTitle("Categories");






		$obj->setActiveSheetIndex(0);
		$filename='vendor-'.$reg_num.'.xlsx';
		// untuk excel 2007 atau yang berekstensi .xlsx
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename='.$filename);
		header('Cache-Control: max-age=0');
		 
		$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}




	function multivendor()
	{
		//print_r($_post); exit;
		if($this->input->post('mycheckbox')){
			$i = 0 ; 
			foreach($this->input->post('mycheckbox') as $cekbox){
		        
		        $id_ada = $this->m_export_1->cek_id($cekbox);
				if($id_ada==1)
				{
					//echo $cekbox . '_';
					$this->ex_excel($cekbox);
					$select	 = "register_num as nomor";
					$vendor = $this->crud->browse("","vendor","id",$cekbox,"false",$select);
					$filename='vendor-'.$vendor->nomor.'.xlsx';
					$valid_files[$i] = $filename ; 
				} 
				$i += 1 ;
				
		    }

		    $nama = uniqid();
		    $zip = new ZipArchive();
		    $dir = 'temp_excel/';
			$filename = $dir . $nama .'.zip';

			if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
			    exit("cannot open <$filename>\n");
			}

			for ($n = 0; $n < $i; $n++)
			{
            	$zip->addFile($dir . $valid_files[$n], $valid_files[$n]);
        	}
			$zip->close();

			for ($n = 0; $n < $i; $n++)
			{
				@unlink($dir.$valid_files[$n]);
			}

			echo base_url($filename);

		}

	}


	function unlinkexcel($filename = '')
	{
		if($filename=='')
		{
			exit();
		}
		
		$filename = 'temp_excel/' . $filename ;
		@unlink($filename);
	}



	function ex_excel($idx = '')
	{
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Asia/Jakarta');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		$this->load->library('PHPExcel');

			// Create new PHPExcel object
			$obj = new PHPExcel();

			




			//=====================
			//=====================
			//=====================  tabel General Information
			//=====================
			//=====================

			// Add new sheet
			$objWorkSheet = $obj->createSheet(0); //Setting index when creating

			// Set lebar kolom
			$objWorkSheet->getColumnDimension('A')->setWidth(17);
			$objWorkSheet->getColumnDimension('B')->setWidth(35);
			$objWorkSheet->getColumnDimension('C')->setWidth(12);
			$objWorkSheet->getColumnDimension('D')->setWidth(30);
			$objWorkSheet->getColumnDimension('E')->setWidth(10);
			$objWorkSheet->getColumnDimension('F')->setWidth(20);
			$objWorkSheet->getColumnDimension('G')->setWidth(30);
			$objWorkSheet->getColumnDimension('H')->setWidth(16);
			$objWorkSheet->getColumnDimension('I')->setWidth(16);
			$objWorkSheet->getColumnDimension('J')->setWidth(16);
			$objWorkSheet->getColumnDimension('K')->setWidth(30);
			$objWorkSheet->getColumnDimension('L')->setWidth(30);


			//Mengeset Syle nya
			$headerStylenya = new PHPExcel_Style();
			$bodyStylenya   = new PHPExcel_Style();

			$headerStylenya->applyFromArray(
				array('fill' 	=> array(
					  'type'    => PHPExcel_Style_Fill::FILL_SOLID,
					  'color'   => array('argb' => 'FFEEEEEE')),
					  'borders' => array('bottom'=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
									'right'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
									'left'	    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
									'top'	    => array('style' => PHPExcel_Style_Border::BORDER_THIN)
					  )
				));
			$bodyStylenya->applyFromArray(
				array('fill' 	=> array(
					  'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
					  'color'	=> array('argb' => 'FFFFFFFF')),
					  'borders' => array(
									'bottom'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
									'right'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
									'left'	    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
									'top'	    => array('style' => PHPExcel_Style_Border::BORDER_THIN)
					  )
			    ));
			$objWorkSheet->setSharedStyle($headerStylenya, "A3:L3");
			$objWorkSheet->setSharedStyle($bodyStylenya, "A4:L4");

			$select	 = "register_num as nomor, vendor_name as name,vendor_type as type,vendor_address as address, 					
	 					postcode,web,phone,fax,email,npwp,npwp_address,npwp_postcode";
	 					
			$vendor = $this->crud->browse("","vendor","id",$idx,"false",$select);	
			


			//Write cells
			 $objWorkSheet	->setCellValue('A1', 'General Information')
			 				
			 				->setCellValue('A3', 'Registration Num.')
			 				->setCellValue('B3', 'Vendor Name')
			 				->setCellValue('C3', 'Vendor Type')
			 				->setCellValue('D3', 'Address')
			 				->setCellValue('E3', 'Post Code')
			 				->setCellValue('F3', 'NPWP')
			 				->setCellValue('G3', 'NPWP Address')
			 				->setCellValue('H3', 'NPWP Post Code')
			 				->setCellValue('I3', 'Phone')
			 				->setCellValue('J3', 'Fax')
			 				->setCellValue('K3', 'Email')
			 				->setCellValue('L3', 'Web')

			 				->setCellValue('A4', $vendor->nomor)
			 				->setCellValue('B4', $vendor->name)
			 				->setCellValue('C4', $vendor->type)
			 				->setCellValue('D4', $vendor->address)
			 				->setCellValue('E4', $vendor->postcode)
			 				->setCellValue('F4', '\''.$vendor->npwp)
			 				->setCellValue('G4', $vendor->npwp_address)
			 				->setCellValue('H4', $vendor->npwp_postcode)
			 				->setCellValue('I4', '\''.$vendor->phone)
			 				->setCellValue('J4', '\''.$vendor->fax)
			 				->setCellValue('K4', $vendor->email)
			 				->setCellValue('L4', $vendor->web);

			$objWorkSheet->setShowGridlines(false);

			// Rename sheet
			 $objWorkSheet->setTitle("General Information");




			//=====================
			//=====================
			//=====================  tabel Contact Person
			//=====================
			//=====================

			// Add new sheet
			$objWorkSheet = $obj->createSheet(1);

			// Set lebar kolom
			$objWorkSheet->getColumnDimension('A')->setWidth(3);
			$objWorkSheet->getColumnDimension('B')->setWidth(25);
			$objWorkSheet->getColumnDimension('C')->setWidth(20);
			$objWorkSheet->getColumnDimension('D')->setWidth(16);
			$objWorkSheet->getColumnDimension('E')->setWidth(30);

			$browse = $this->crud->browse("","contact_person","id_vendor",$idx,"true");


			
			$objWorkSheet	->setCellValue('A1', 'Contact Person')
							->setCellValue('A2', 'Vendor Name : '.$vendor->name)
			 				
			 				->setCellValue('A4', 'No')
			 				->setCellValue('B4', 'Name')
			 				->setCellValue('C4', 'Position')
			 				->setCellValue('D4', 'Mobile')
			 				->setCellValue('F4', 'Email');

			 if($browse){ $i=1;
	 			foreach($browse as $row){
	 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
			 						->setCellValue('B'.(4 + $i), $row->fullname)
			 						->setCellValue('C'.(4 + $i), $row->position)
			 						->setCellValue('D'.(4 + $i), '\''.$row->mobile)
			 						->setCellValue('E'.(4 + $i), $row->email);
	 				$i++;  
	 			}
	 		}

			// Rename sheet
			 $objWorkSheet->setTitle("Contact Person");












			//=====================
			//=====================
			//=====================  tabel Bank Account
			//=====================
			//=====================

			// Add new sheet
			$objWorkSheet = $obj->createSheet(2);
			// Set lebar kolom
			$objWorkSheet->getColumnDimension('A')->setWidth(3);
			$objWorkSheet->getColumnDimension('B')->setWidth(20);
			$objWorkSheet->getColumnDimension('C')->setWidth(25);
			$objWorkSheet->getColumnDimension('D')->setWidth(40);
			$objWorkSheet->getColumnDimension('E')->setWidth(25);
			$objWorkSheet->getColumnDimension('F')->setWidth(25);
			 
				
			$bank 	= $this->crud->browse("","bank_account","id_vendor",$idx,"true");	

			//Write cells
			$objWorkSheet	->setCellValue('A1', 'Bank Account')
							->setCellValue('A2', 'Vendor Name : '.$vendor->name)
			 				
			 				->setCellValue('A4', 'No')
			 				->setCellValue('B4', 'Account Number')
			 				->setCellValue('C4', 'Account Name')
			 				->setCellValue('D4', 'Account Address')
			 				->setCellValue('E4', 'Bank Name')
			 				->setCellValue('F4', 'Branch Name');

			if($bank){ $i=1;
	 			foreach($bank as $row1){
	 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
			 						->setCellValue('B'.(4 + $i), '\''.$row1->acc_number)
			 						->setCellValue('C'.(4 + $i), $row1->acc_name)
			 						->setCellValue('D'.(4 + $i), $row1->acc_address)
			 						->setCellValue('E'.(4 + $i), $row1->bank_name)
			 						->setCellValue('F'.(4 + $i), $row1->branch_name);
	 				$i++;  
	 			}
	 		}
			 
			// Rename sheet
			 $objWorkSheet->setTitle("Bank Account");
			 




			//=====================
			//=====================
			//=====================  tabel Akta Pendirian
			//=====================
			//=====================

			// Add new sheet
			$objWorkSheet = $obj->createSheet(3);
			// Set lebar kolom
			$objWorkSheet->getColumnDimension('A')->setWidth(3);
			$objWorkSheet->getColumnDimension('B')->setWidth(25);
			$objWorkSheet->getColumnDimension('C')->setWidth(40);
			$objWorkSheet->getColumnDimension('D')->setWidth(20);
			$objWorkSheet->getColumnDimension('E')->setWidth(20);
			$objWorkSheet->getColumnDimension('F')->setWidth(20);
			$objWorkSheet->getColumnDimension('G')->setWidth(20);

	 		$none_akta 		= array('notaris_name'		=> "none",
								 	'notaris_address'	=> "none",
								 	'notaris_phone'		=> "none",
								 	'notaris_number'	=> "none",
								 	'notaris_date'		=> date('Y-m-d'),
								 	'remark'			=> "none");
	 		
	 		$akta  = $this->crud->browse("","akta","id_vendor",$idx,"false");	
	 		$akta2   = !$akta ? (object) $none_akta : $akta;	 

			//Write cells
			$objWorkSheet	->setCellValue('A1', 'Akta Pendirian')
							->setCellValue('A2', 'Vendor Name : '.$vendor->name)
			 				
			 				->setCellValue('A4', 'No.')
			 				->setCellValue('B4', 'Notaris Name')
			 				->setCellValue('C4', 'Notaris Address')
			 				->setCellValue('D4', 'Notaris Phone')
			 				->setCellValue('E4', 'Notaris Number')
			 				->setCellValue('F4', 'Notaris Date')
			 				->setCellValue('G4', 'Remark')

			 				->setCellValue('A5', '1')
			 				->setCellValue('B5', $akta2->notaris_name)
			 				->setCellValue('C5', $akta2->notaris_address)
			 				->setCellValue('D5', '\''.$akta2->notaris_phone)
			 				->setCellValue('E5', '\''.$akta2->notaris_number)
			 				->setCellValue('F5', $akta2->notaris_date)
			 				->setCellValue('G5', $akta2->remark);
			 
			// Rename sheet
			 $objWorkSheet->setTitle("Akta Pendirian");
			 




			//=====================
			//=====================
			//=====================  tabel OWNER
			//=====================
			//=====================

			// Add new sheet
			$objWorkSheet = $obj->createSheet(4);
			// Set lebar kolom
			$objWorkSheet->getColumnDimension('A')->setWidth(3);
			$objWorkSheet->getColumnDimension('B')->setWidth(25);
			$objWorkSheet->getColumnDimension('C')->setWidth(40);
			$objWorkSheet->getColumnDimension('D')->setWidth(20);
			$objWorkSheet->getColumnDimension('E')->setWidth(20);
			$objWorkSheet->getColumnDimension('F')->setWidth(20);
			$objWorkSheet->getColumnDimension('G')->setWidth(20);
			 
			$owner = $this->crud->browse("","owner","id_vendor",$idx,"true");			

			//Write cells
			 $objWorkSheet	->setCellValue('A1', 'Owner')
			 				->setCellValue('A2', 'Vendor Name : '.$vendor->name)
			 				
			 				->setCellValue('A4', 'No')
			 				->setCellValue('B4', 'Owner Name')
			 				->setCellValue('C4', 'Owner Address')
			 				->setCellValue('D4', 'Owner Phone')
			 				->setCellValue('E4', 'Owner Position')
			 				->setCellValue('F4', 'Owner Shared')
			 				->setCellValue('G4', 'Remark');

			if($owner){ $i=1;
	 			foreach($owner as $row2){
	 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
			 						->setCellValue('B'.(4 + $i), $row2->owner_name)
			 						->setCellValue('C'.(4 + $i), $row2->owner_address)
			 						->setCellValue('D'.(4 + $i), '\''.$row2->owner_phone)
			 						->setCellValue('E'.(4 + $i), $row2->owner_position)
			 						->setCellValue('F'.(4 + $i), $row2->owner_shared)
			 						->setCellValue('G'.(4 + $i), $row2->remark);
	 				$i++;  
	 			}
	 		}
			 
			// Rename sheet
			 $objWorkSheet->setTitle("Owner");




			//=====================
			//=====================
			//=====================  tabel Pengurus
			//=====================
			//=====================

			// Add new sheet
			$objWorkSheet = $obj->createSheet(5);
			// Set lebar kolom
			$objWorkSheet->getColumnDimension('A')->setWidth(3);
			$objWorkSheet->getColumnDimension('B')->setWidth(25);
			$objWorkSheet->getColumnDimension('C')->setWidth(40);
			$objWorkSheet->getColumnDimension('D')->setWidth(20);
			$objWorkSheet->getColumnDimension('E')->setWidth(20);
			$objWorkSheet->getColumnDimension('F')->setWidth(20);
			 
			$org = $this->crud->browse("","organization","id_vendor",$idx,"true");		

			//Write cells
			 $objWorkSheet	->setCellValue('A1', 'Pengurus')
			 				->setCellValue('A2', 'Vendor Name : '.$vendor->name)
			 				
			 				->setCellValue('A4', 'No')
			 				->setCellValue('B4', 'Name')
			 				->setCellValue('C4', 'Address')
			 				->setCellValue('D4', 'Phone')
			 				->setCellValue('E4', 'Position')
			 				->setCellValue('F4', 'Remark');

			if($org){ $i=1;
	 			foreach($org as $row3){
	 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
			 						->setCellValue('B'.(4 + $i), $row3->name)
			 						->setCellValue('C'.(4 + $i), $row3->address)
			 						->setCellValue('D'.(4 + $i), '\''.$row3->phone)
			 						->setCellValue('E'.(4 + $i), $row3->position)
			 						->setCellValue('F'.(4 + $i), $row3->remark);
	 				$i++;  
	 			}
	 		}
			 
			// Rename sheet
			 $objWorkSheet->setTitle("Pengurus");




			//=====================
			//=====================
			//=====================  tabel Correspondences
			//=====================
			//=====================

			// Add new sheet
			$objWorkSheet = $obj->createSheet(6);
			// Set lebar kolom
			$objWorkSheet->getColumnDimension('A')->setWidth(3);
			$objWorkSheet->getColumnDimension('B')->setWidth(40);
			$objWorkSheet->getColumnDimension('C')->setWidth(20);
			$objWorkSheet->getColumnDimension('D')->setWidth(40);
			$objWorkSheet->getColumnDimension('E')->setWidth(10);
			$objWorkSheet->getColumnDimension('F')->setWidth(10);
			$objWorkSheet->getColumnDimension('G')->setWidth(20);
			 
			$correspondence = $this->crud->browse("","correspondence","id_vendor",$idx,"true");		

			//Write cells
			$objWorkSheet	->setCellValue('A1', 'Correspondence')
							->setCellValue('A2', 'Vendor Name : '.$vendor->name)
			 				
			 				->setCellValue('A4', 'No')
			 				->setCellValue('B4', 'Document Name')
			 				->setCellValue('C4', 'Document Number')
			 				->setCellValue('D4', 'Description')
			 				->setCellValue('E4', 'Effective Date')
			 				->setCellValue('F4', 'Expired Date')
			 				->setCellValue('G4', 'Remark');

			if($correspondence){ $i=1;
	 			foreach($correspondence as $row4){
	 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
			 						->setCellValue('B'.(4 + $i), $row4->doc_name)
			 						->setCellValue('C'.(4 + $i), '\''.$row4->doc_num)
			 						->setCellValue('D'.(4 + $i), $row4->desc)
			 						->setCellValue('E'.(4 + $i), $row4->eff_date)
			 						->setCellValue('F'.(4 + $i), $row4->exp_date)
			 						->setCellValue('G'.(4 + $i), $row4->remark);
	 				$i++;  
	 			}
	 		}
			 
			// Rename sheet
			 $objWorkSheet->setTitle("Correspondences");



			//=====================
			//=====================
			//=====================  tabel Reference
			//=====================
			//=====================

			// Add new sheet
			$objWorkSheet = $obj->createSheet(7);// Set lebar kolom
			$objWorkSheet->getColumnDimension('A')->setWidth(3);
			$objWorkSheet->getColumnDimension('B')->setWidth(25);
			$objWorkSheet->getColumnDimension('C')->setWidth(20);
			$objWorkSheet->getColumnDimension('D')->setWidth(10);
			$objWorkSheet->getColumnDimension('E')->setWidth(10);
			$objWorkSheet->getColumnDimension('F')->setWidth(20);
			 
			$ref = $this->crud->browse("","references","id_vendor",$idx,"true");		

			//Write cells
			$objWorkSheet	->setCellValue('A1', 'Reference')
							->setCellValue('A2', 'Vendor Name : '.$vendor->name)
			 				
			 				->setCellValue('A4', 'No')
			 				->setCellValue('B4', 'Customer Name')
			 				->setCellValue('C4', 'Project')
			 				->setCellValue('D4', 'Point')
			 				->setCellValue('E4', 'Date')
			 				->setCellValue('F4', 'Remark');

			if($correspondence){ $i=1;
	 			foreach($correspondence as $row5){
	 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
			 						->setCellValue('B'.(4 + $i), $row5->cust_name)
			 						->setCellValue('C'.(4 + $i), $row5->project)
			 						->setCellValue('D'.(4 + $i), $row5->point)
			 						->setCellValue('E'.(4 + $i), $row5->date)
			 						->setCellValue('F'.(4 + $i), $row5->remark);
	 				$i++; 
	 			}
	 		}
			 
			// Rename sheet
			 $objWorkSheet->setTitle("Reference");




			//=====================
			//=====================
			//=====================  tabel Affiliate
			//=====================
			//=====================

			// Add new sheet
			$objWorkSheet = $obj->createSheet(8);
			// Set lebar kolom
			$objWorkSheet->getColumnDimension('A')->setWidth(3);
			$objWorkSheet->getColumnDimension('B')->setWidth(30);
			$objWorkSheet->getColumnDimension('C')->setWidth(40);
			$objWorkSheet->getColumnDimension('D')->setWidth(20);
			$objWorkSheet->getColumnDimension('E')->setWidth(20);
			 
			$aff = $this->crud->browse("","affiliates","id_vendor",$idx,"true");		

			//Write cells
			$objWorkSheet	->setCellValue('A1', 'Affiliate')
							->setCellValue('A2', 'Vendor Name : '.$vendor->name)

			 				->setCellValue('A4', 'No')
			 				->setCellValue('B4', 'Company Name')
			 				->setCellValue('C4', 'Competency')
			 				->setCellValue('D4', 'Contact')
			 				->setCellValue('E4', 'Remark');

			if($aff){ $i=1;
	 			foreach($aff as $row6){
	 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
			 						->setCellValue('B'.(4 + $i), $row6->company_name)
			 						->setCellValue('C'.(4 + $i), $row6->competency)
			 						->setCellValue('D'.(4 + $i), $row6->contact)
			 						->setCellValue('E'.(4 + $i), $row6->remark);
	 				$i++;  
	 			}
	 		}
			 
			// Rename sheet
			 $objWorkSheet->setTitle("Affiliate");




			//=====================
			//=====================
			//=====================  tabel categories
			//=====================
			//=====================

			// Add new sheet
			$objWorkSheet = $obj->createSheet(9);
			// Set lebar kolom
			$objWorkSheet->getColumnDimension('A')->setWidth(3);
			$objWorkSheet->getColumnDimension('B')->setWidth(40);
			$objWorkSheet->getColumnDimension('C')->setWidth(40);
			 
			$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
				   (SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";					
	 		$categories = $this->crud->browse("","vendor_category l","id_vendor",$idx,"true",$select2);		

			//Write cells
			$objWorkSheet	->setCellValue('A1', 'Categories')
							->setCellValue('A2', 'Vendor Name : '.$vendor->name)
			 				
			 				->setCellValue('A4', 'No')
			 				->setCellValue('B4', 'Category')
			 				->setCellValue('C4', 'Sub Category');

			if($categories){ $i=1;
	 			foreach($categories as $row6){
	 				$objWorkSheet	->setCellValue('A'.(4 + $i), $i)
			 						->setCellValue('B'.(4 + $i), $row6->category)
			 						->setCellValue('C'.(4 + $i), $row6->subcategory);
	 				$i++;  
	 			}
	 		}
			 
			// Rename sheet
			 $objWorkSheet->setTitle("Categories");






			$obj->setActiveSheetIndex(0);
			$filename='vendor-'.$vendor->nomor.'.xlsx';
			// untuk excel 2007 atau yang berekstensi .xlsx
			//header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			//header('Content-Disposition: attachment;filename='.$filename);
			//header('Cache-Control: max-age=0');
			 
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			$objWriter->save(str_replace(__FILE__,'temp_excel/'.$filename,__FILE__));
			
	}

}