 
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Users Groups</h3>
              </div><!-- /.box-header -->

                <form class = "form-horizontal">
                <br>
                <div class="col-md-2 pull-left">
                    <!--a href="<?php echo base_url(); ?>auth/register"><button class="btn btn-sm btn-primary" id = "btn-pd">New User</button></a-->
                    <?php  $prop = array('class'=>'btn btn-sm btn-primary','title'=>'New Group'); ?>					 
					<?php  echo anchor('user/form_group','New Group',$prop); ?>                    
                </div>

                <div class = "col-md-10 pull-right">
                  <div class="input-group pull-right" style="width: 150px;">
                      <input type="text"  name="srch-term" id="srch-term"  class="form-control input-sm pull-right" placeholder="Search" style="height: 28px;">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                </div>

                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover text-center">
                    <thead>
                      <tr>
                        <!--th><input type="checkbox" name="cba[]" id="cba" /></th-->
                        <th>No</th>
                        <th>Group Name</th>
                        <th>permissions</th>                    
                      </tr>
                    </thead>

                    <tbody>
<?php
if($perm){ $i=1;
	foreach($perm as $row){ ?>	
			<tr>
				<td><?php echo $num = $num+1;; ?></td>
				<td><?php echo ucwords($row->name); ?></td>
				<td><?php echo "{".$row->permissions."}"; ?></td>			
					
				<td><?php				
					$pencil = '<i class="fa fa-pencil"></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete user ?')){ return true; } else {return false; }";
					echo anchor('user/form_edit_perm/'.$row->id, $pencil,array('class'=>'default_link','title'=>'Edit Pemissions'));
					echo anchor('user/delete_group/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Group','onclick'=>$js));?>
				</td>
				
			</tr>
<?php	$i++;  } ?>
<?php } else { ?>
	<tr><td colspan="5" align="center">-</td></tr>
<?php } ?>
                      
                    </tbody>

                  </table>
                </div><!-- /.box-body -->
              </form>
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->