<script type="text/javascript">
  $(function () { 
   	  var baseUrl  = "<?php echo base_url(); ?>"; 
   	  
   	   $('#btn_cancel11').click(function(){ 
   	   		var id_visit  = $("#id_visit").val(); 
	 		var id_subcat = $("#id_subcat").val();
   			$("#container11").load(baseUrl+'due_diligence/view_category/'+id_visit);      	  		
   	   }); 
   	   
   	   $('#btn_create11').click(function(){   	  
   	  		var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	    
		    var values   = $("#form_resume").serializeArray();
			values.push({name: csrf,value: token});
			values = jQuery.param(values);
			 	$.post( baseUrl+ "due_diligence/save_subcat_resume", values )
				  .done(function( resp ) {
				    	var id_visit  = $("#id_visit").val(); 
	 					var id_subcat = $("#id_subcat").val();
				    	$("#container11").load(baseUrl+'due_diligence/view_category/'+id_visit);
				    	//$("#debug11").html(resp);   
				});
	     	
	     });     
  });  

</script>

<div id="debug11"></div>
<h2>Sub Category Resume</h2>
<?php echo form_open('#',array('id'=>'form_resume'));  ?>
<input type="hidden" name="id_visit" id="id_visit" value="<?php echo $id_visit;  ?>"/>
<input type="hidden" name="id_subcat" id="id_subcat" value="<?php echo $id_subcat;  ?>"/>
<table width="60%">
	<tr>
		<td style="vertical-align: top;"><label>Resume</label></td><td style="vertical-align: top;">:</td>
		<td><textarea name="resume" id="resume" cols="20" rows="5" class="form-control" placeholder="Resume" required><?php echo $resume; ?></textarea></td>
	</tr>
	<tr>
		<td align="right" colspan="3">
			<button type="button" id="btn_cancel11" class="btn btn-default">Cancel</button>
			<?php if($this->permit->md3) {  ?><button type="button" id="btn_create11" class="btn btn-info btn-primary">Create</button><?php } ?>			
		</td>
	</tr>
</table>

