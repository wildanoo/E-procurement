<meta name="csrf-token" content="<?php echo $this->security->get_csrf_hash(); ?>">
<script src="<?php echo base_url() ?>assets_/js/email_portal.js"></script>

<style type="text/css">
body{
	height: 100%;
}
.navbar,
.main-footer{
	height: 10%;
}
.main{
	height: 80%;
}
.subnavbar,
.nav-collapse .nav,
.nav-collapse .search-query{
	display: none;
}
.login-challenge{
	padding: 40px 0px;
}
.login-challenge .notes{
	font-size: 15px;
}
.form-attempt{
	padding-top: 25px;
	padding-bottom: 55px;
}
.form-attempt .input-group{
	width: 100%;
}
.form-attempt .input-group-addon{
	background: #eceeef;
	color: #55595c;
	width: 13%;
}
.failed-attempt{
	margin-top: 25px;
	margin-bottom: 0px;
	display: none;
}
</style>
<div class="login-challenge">
    <div class="page-header" style="vertical-align: middle">
	  <h3 style="display:inline-block">Welcome to e-Procurement Garuda Indonesia, </h3>
	</div>
	<div class="notes">
		<small>It looks like you don't log in yet or you are logged as another user.</small>
		<small>Please input password for <strong><?php echo $data->username?></strong> to open this content.</small>
	</div>
   	
   	<div class="form-attempt">
		<div class="input-group">
		  <span class="input-group-addon" id="username_attempt"><strong>Username / Email</strong></span>
		  <input type="text" class="form-control" readonly value="<?php echo $data->username.' / '.$data->email;?>" aria-describedby="username_attempt">
		</div>
		<?php $login = array(
              'name'        => 'login',
              'id'          => 'login',
              'value'       => $data->email,
              'type'        => 'hidden'
            );

			echo form_input($login);?>
		<br>
   		<div class="input-group">
		  <span class="input-group-addon" id="password_attempt"><strong>Password</strong></span>
		  <input type="password" class="form-control" id="password" name="password" placeholder="insert password..." aria-describedby="password_attempt">
		</div>
		<div class="alert alert-danger failed-attempt">
		  <strong>Info!</strong> <span id="message"></span>
		</div>
		<br>
		<hr/>
   		<div class="form-group">
			<a href="<?php echo base_url()?>" class="btn btn-warning pull-right">Back to Home</a>
		 	<button type="button" id="login-attempt" class="btn btn-primary pull-right">Log in</button>
		</div>
	</div>
</div>