<?php echo form_open('#',array('id'=>'form')); ?>
<table cellpadding="2" cellspacing="2">
<tr>
	<td><label>Office</label></td><td>:</td>
	<td><?php 
		$prop_office = 'class="form-control" id="id_office" style="width:220px"';
		echo form_dropdown('id_office',$office,'default',$prop_office); ?>			
	</td>
	<td id="msg1"></td>	
</tr>
<tr>
	<td><label>Department Code</label></td><td>:</td>
	<td><input type="text" name="dept_code" id="dept_code" placeholder="Department Code" class="form-control" required/></td>	
	<td id="msg2"></td>		
</tr>
<tr>
	<td><label>Department Name</label></td><td>:</td>
	<td><input type="text" name="dept_name" id="dept_name" placeholder="Department Name" class="form-control" required/></td>
	<td id="msg3"></td>	
</tr>
</table>