<script type="text/javascript">
$(document).ready(function(){
   var baseUrl  = "<?php echo base_url(); ?>";	
   $(function () { 	$("#upload1").hide();  });
   
   $('#id_company').change(function(){  
   		var id_comp = $('#id_company').val();
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'policy_agreement/get_office',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_comp='+id_comp,
               success: function(resp){
               	  $('#id_office').html(resp);
               }
            });
   }); 
   
   $('#id_cat').change(function(){  
   		var id_cat = $('#id_cat').val();
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'policy_agreement/get_subcategory',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_cat='+id_cat,
               success: function(resp){
               	  $('#id_subcat').html(resp);
               }
            });
            $.ajax({
               url  : baseUrl + 'policy_agreement/get_vendor',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_cat='+id_cat,
               success: function(resp){
               	  $('#id_vendor').html(resp);
               }
            });
   });      
     
   
   $('#first_valdate').datepicker({  
        changeMonth: true, changeYear: true,
		dateFormat: "yy/mm/dd" ,
     });      
     
   $('#end_valdate').datepicker({  
        changeMonth: true, changeYear: true,
		dateFormat: "yy/mm/dd" ,
     }); 
     
   $('#publish_date').datepicker({  
        changeMonth: true, changeYear: true,
		dateFormat: "yy/mm/dd" ,
     });    

   $("input:radio[name=content]").click(function () {
		var opt = $("input:radio[name=content]:checked").val();
		if (opt == '2'){
			$("#upload1").show();	 $("#wyswyg").hide(); 
		} else {
			$("#wyswyg").show();     $("#upload1").hide();	}
     });
    
}); 

</script>

<div class="page-header">
  <h3>Update Policy Agreement</h3>
</div> 

<div id="debug"></div>
<?php echo form_open_multipart('policy_agreement/update',array('id'=>'form'));	$current_date = date('Y/m/d')?>

<table width="80%" border="0">
<?php echo form_hidden('id',$def->id); ?>
<?php echo form_hidden('type',$def->type); ?>
	<tr id="option">
	 <td><label>Content</label></td><td>:</td>
	  <td>
		  <input type="radio" name="content" value="1" checked> Typing &nbsp;&nbsp;
		  <input type="radio" name="content" value="2"> Upload<br>
	  </td>
	</tr>

	<tr id="wyswyg">
		<td colspan="2">&nbsp;</td>
		<td class="form_tab_content" width="75%;">
			<div>
				<ul class="nav nav-tabs test2">
				    <li class="active"><a data-toggle="tab" href="#idn" aria-expanded="true">Indonesia</a></li>
				    <li class=""><a data-toggle="tab" href="#eng" aria-expanded="false">English</a></li>
				</ul>

				<div class="tab-content">
				    <div id="idn" class="tab-pane fade active in" style="padding:15px;">
				      <?php $data['content'] = $contents[0]; $this->load->view('policy_agreement/note',$data); ?>
				    </div>
				    <div id="eng" class="tab-pane fade" style="padding:15px;">
				      <?php $data['content'] = $contents[1]; $this->load->view('policy_agreement/note',$data); ?>
				    </div>
				</div>
			</div>
		</td>
	</tr>
	
	<!-- <tr><td colspan="2">&nbsp;</td>			    
		<td id="wyswyg" width="75%;"><?php //$this->load->view('policy_agreement/note'); ?></td>					
	</tr> -->
		
	<tr id="upload1">
		<td colspan="2">&nbsp;</td>				
		<td class="form_tab_content" width="75%;">
			<div>
				<ul class="nav nav-tabs test2">
				    <li class="active"><a data-toggle="tab" href="#upidn" aria-expanded="true">Indonesia</a></li>
				    <li class=""><a data-toggle="tab" href="#upeng" aria-expanded="false">English</a></li>
				</ul>
				<?php $style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";?>
				<div class="tab-content">
				    <div id="upidn" class="tab-pane fade active in" style="padding:15px;">
				      <?php	
						$data  = array('type'=>'file','name'=> 'contentfileind','id' => 'file_upload_ind','value'=>'true','content'=>'Import','style'=>$style); 
						echo form_input($data); ?> <i>*.txt</i>
				    </div>
				    <div id="upeng" class="tab-pane fade" style="padding:15px;">
				      <?php	
						$data  = array('type'=>'file','name'=> 'contentfileeng','id' => 'file_upload_eng','value'=>'true','content'=>'Import','style'=>$style); 
						echo form_input($data); ?> <i>*.txt</i>
				    </div>
				</div>
			</div>
		</td>
	</tr>
<tr>
	<td style="vertical-align: top;"><label>Detail</label></td><td style="vertical-align: top;">:</td>
	<td><textarea name="detail" id="detail" cols="20" rows="5" class="form-control" required><?php echo $def->detail; ?></textarea></td>
</tr>
<tr id="creator_area">
	<td><label>Creator</label></td><td>:</td>
	<td>&nbsp;&nbsp;<?php echo ucfirst($def->creator); ?><?php echo form_hidden("creator",$username); ?>
	<!--input type="text" name="creator" id="creator" value="<?php //echo $def->creator; ?>" placeholder="Creator" class="form-control"/--></td>	
</tr>
<tr>
	<td colspan="3" style="text-align: right;">
		<input type="submit" value="Submit" class="btn btn-primary btn-sm"/>
		<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>'Cancel'); ?>					 
		<?php  echo anchor('policy_agreement/','Cancel',$prop); ?>
	</td>
</tr>	
</table>
	

<?php echo form_close(); ?>



