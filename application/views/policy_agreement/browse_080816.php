<script type="text/javascript"> 
	$(document).ready(function(){	
		var baseUrl   = "<?php echo base_url(); ?>";	
		$(function(){	});	
		
		$('#type').change(function(){ 
			var type = $('#type').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'policy_agreement/set_sess_category',			   
               data : 'field=type&type='+type,
               success: function(data){ 				 	
				 	document.location.href= baseUrl + 'policy_agreement/';
			   }		   
            });	
		
		});

		$('#title').change(function(){ 
			var title = $('#title').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'policy_agreement/set_sess_category',			   
               data : 'field=title&type='+title,
               success: function(data){ 				 	
				 	document.location.href= baseUrl + 'policy_agreement/';
			   }		   
            });	
		
		});

		$("#srch-term").autocomplete({
			source: baseUrl + 'policy_agreement/get_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'policy_agreement/set_browse_session',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){                		            
				    document.location.href= baseUrl + 'policy_agreement/';
               }
            });
        });  

	});			
</script>
<script src="<?php echo base_url() ?>assets/js/sortable.js"></script>

<div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Vendor Policy</h3>
              </div><!-- /.box-header -->
              <br>
<?php 	$msg = $this->session->flashdata('message');
		if(strpos($msg,'failed')) $color = "#bc0505";
		else $color = "#487952"; 		
	 	 ?>	
<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
                <div class="col-md-2 pull-left">
                    <!-- input type="button" class="btn btn-primary btn-sm" title="Create New Category" value="New Category" style="width:130px;" title="Create New Category"/> -->
					<?php	  
					//$plus = '<i class="fa fa-plus"></i>';
					//echo anchor('policy_agreement/form_create/', $plus,array('class'=>'default_link','title'=>'Add Group')); ?>&nbsp; 
          			<a href="<?php echo base_url('policy_agreement/form_create'); ?>" type="button" class="btn btn-primary btn-sm">New Policy</a>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
				<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
                </div>
		<div class="col-sm-3 col-md-3 pull-right">
        <!--form class="navbar-form" role="search"-->
        <div class="input-group">
            <?php
			$sess_cat = $this->session->userdata('sess_cat');
			$id_cat   = $sess_cat == '' ? '' : $sess_cat;	            
			$prop_cat = 'id=type  style="width: 120px; height: 25px; font-size: 13px"'; ?>
			<?php echo form_dropdown('type', $category, $id_cat,$prop_cat); ?>
			
			<?php
			$sess_cat = $this->session->userdata('sess_cat');
			$id_cat   = $sess_cat == '' ? '' : $sess_cat;	            
			$prop_cat = 'id=title  style="width: 120px; height: 25px; font-size: 13px"'; ?>
			<?php echo form_dropdown('title', $title, $id_cat,$prop_cat); ?>
        </div>
        <!--/form-->
        </div>



<table id="browse" class="table table-bordered table-hover text-center sortable" style="font-size: 13px">
	<thead>
        <tr>
            <th class="table-sorted-reverse">No</th>
            <th class="">Type</th>
            <th class="">Title</th>
            <th class="">Status</th>
            <th class="">Created date</th>  
            <th class="">Updated date</th> 
            <th class="">Updated by</th>       
            <th>Action</th>
        </tr>
	</thead>
    <tbody>
<?php			
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td><?php  echo $num = $num+1; ?></td>
				<td><?php  echo $category[$row->type]; ?></td>
				<td><?php   echo $row->title; ?></td>
				<!-- td style="text-align: center"><?php   echo $status[$row->status]; ?></td> -->
				<td><?php   
					$title = $row->activated==1 ? "Active" : "Not Active";
					$qst   = $row->activated=="1" ? "Unactivated" : "Activated";
					$js    = "if(confirm('$qst Content ?')){ return true; } else {return false; }";					
					echo anchor('policy_agreement/activated/'.$row->id, $title,array('class'=>'default_link','title'=>'Change Status','onclick'=>$js));
					?>
				</td>	
				<td><?php   echo date('M d, Y',strtotime($row->created_date)); ?></td>	
				<td><?php   echo date('M d, Y',strtotime($row->last_updated)); ?></td>			
				<td><?php   echo $row->created_id; ?></td>			
				<td>								
				<?php				
					$eye_open  = '<i class="glyphicon glyphicon-eye-open"></i>';
					echo anchor('policy_agreement/reviewed/'.$row->id, $eye_open,array('class'=>'default_link','title'=>'Review Content')); ?>&nbsp;
				<?php	  
					//$pencil = '<i class="fa fa-pencil"></i>';
					//echo anchor('policy_agreement/form_update/'.$row->id, $pencil,array('class'=>'default_link','title'=>'Select Group')); ?>&nbsp;
				<?php								
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete policy ?')){ return true; } else {return false; }";
					echo anchor('policy_agreement/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Policy','onclick'=>$js));?>
				</td>								
			</tr>
<?php	$i++;  } ?>	
</table><div align="center"><?php echo $pagination;?></div>
<?php } else { ?><tr><td colspan="9">-</td></tr><?php } ?>
    </tbody>                
    			</div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->
</table>
