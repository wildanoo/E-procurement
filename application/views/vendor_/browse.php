<script type="text/javascript"> 
	var baseUrl   = "<?php echo base_url(); ?>";	
	$(function(){
	
		var baseUrl   = "<?php echo base_url(); ?>";
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	    var token     = '<?php echo $this->security->get_csrf_hash(); ?>';

		$('.checkall').on('click', function(){
			$(this).closest('table').find(':checkbox').prop('checked', this.checked);
		});						 
	
		$("#srch-term").autocomplete({
			source: baseUrl + 'vendor/get_tags',
			minLength:1
		});

		$("#id_sts").change(function(){
			
			var id_sts = $("#id_sts").val();			
			$.post( baseUrl + "vendor/set_browse_status", { csrf: token, id_sts: id_sts }).done(function( resp ) {
    			 //$("#debug").html(resp);
    			 document.location.href= baseUrl + 'vendor/';
  			});
			
			
		});
		
		$("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'vendor/set_browse_session',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){                		            
				    document.location.href= baseUrl + 'vendor/';
               }
            });
        });	  

		$("#btn_change_status").click(function(){
	            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
	            var values = $("#form").serializeArray();

				if (values.length > 1) {
					values.push({name: csrf,value: token});
					values = jQuery.param(values);

				BootstrapDialog.show({
					title: 'Update Status',
		            message: $('<div></div>').load(baseUrl + "register/form_update/" ),  
		            buttons: [{			            	
				                label: 'Confirm',
				                cssClass: 'btn btn-primary btn-sm',		               
				                action: function(dialogRef){
				                	var result = $("input:radio[name=status]:checked").val(); 
				                	var values = $("#form").serializeArray();
				                	values.push({ name: "result", value: result });
				                	values.push({ name: csrf,value: token });
				                	values = jQuery.param(values);
				                	$.ajax({
							               url  : baseUrl + 'register/update',
							               type : "POST",              		               
							               data : values,
							               success: function(resp){
							               		document.location.href= baseUrl + 'register/';     
									       }
							            });
 				                }
				            }, {
				            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
						                action: function(dialogRef) {
						                    dialogRef.close(); 
						                }
					        }]
		        	}); } else alert('Please check at least one of the options.');
		  });  
	   
	});
	
	function form_update(id){
				var baseUrl = "<?php echo base_url(); ?>";
				var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 

				BootstrapDialog.show({
				title: 'Change Status',
	            message: $('<div></div>').load(baseUrl + "register/form_update/" + id),  
	            buttons: [{			            	
			                label: 'Confirm',
			                cssClass: 'btn btn-primary btn-sm',		               
			                action: function(dialogRef){
			                	var status = $("input:radio[name=status]:checked").val();
			                	 $.post( baseUrl + 'register/update', { csrf: token , status : status , id : id })
								 .done(function( resp ){  
									 document.location.href= baseUrl + 'register/'; 
			               		 });  
			                }
			            }, {
			            	label: 'Cancel',cssClass: 'btn btn-primary btn-sm',
					                action: function(dialogRef) {
					                    dialogRef.close(); 
					                }
				        }]
	        	});
		}

	function review(id){
		var baseUrl = "<?php echo base_url(); ?>";
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 

		BootstrapDialog.show({
		title: 'Review',
        message: $('<div></div>').load(baseUrl + "register/review/" + id),  
        buttons: [{		
	            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
			                action: function(dialogRef) {
			                    dialogRef.close(); 
			                }
		        }]
    	});
}

</script>

<div id="debug"></div>
<div class="page-header"><h3>Vendor Management</h3></div>
<!--input type="button" id="btn_change_status" class="btn btn-primary btn-sm" value="Change Status" style="width:130px;"/-->
  
		<div class="col-sm-3 col-md-3 pull-right">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" id="btn_search" ><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        </div>

<?php echo $this->session->flashdata("message"); ?>
<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
        <tr>
            <th style="text-align: center;" >No</th>
            <!--th style="text-align: center;"><input type="checkbox" class="checkall">All</th-->
            <th>Registration Num.</th> 
            <th>Vendor Num.</th>            
            <th>Vendor Name</th>  
            <th>Vendor Type</th>         
            <th>Status
            <?php
            	$sess_sts = $this->session->userdata('sess_sts');
            	if(!$sess_sts) $sess_sts = 0 ; 
				$select = array('0'=>'All');	            
				$prop_sts = 'id=id_sts  style="width: 60px; height: 25px; font-size: 13px"'; 
			    echo form_dropdown('id_sts', $select+$status , $sess_sts ,$prop_sts); ?>            
            </th>
            <th>Created Date</th>     
            <!--th>Action</th-->  
        </tr>       
	</thead>
    <tbody>
<?php
	
echo form_open('#',array('id'=>'form'));
echo form_hidden('total',count($browse));
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td style="text-align: center;"><?php  echo $num = $num+1;; ?></td>
				<!--td style="text-align: center; width:10%;">
				    <?php $checked = /*$row->status=="1" ? "checked" :*/ "";  ?>
					<input type="checkbox" <?php echo $checked; ?> name="<?php echo "select".$i;?>" id="<?php echo "select".$i;?>" value="<?php echo $row->id;?> ">
					<!-- input type="hidden" name="<?php //echo "matID".$i;?>" id="<?php //echo "matID".$i;?>" value="<?php // echo $row->id; ?>"/> -->	
				<!--/td-->
				<td><?php echo $row->reg_num ?></td>
				<td><?php if($row->vendor_num) echo $row->vendor_num; else echo "-"; ?></td>		
				<td><?php  echo anchor("vendor/set_sess_vendor/".$row->id,$row->vendor_name);   ?></td>
				<td><?php echo strtoupper($row->vendor_type); ?></td>	
				<td><?php echo ucwords($row->reg_sts); ?></td>						
				<td style="text-align: center;"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>		
			</tr>
<?php	$i++;  } ?>	
<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?>
	<tr><td colspan="9" align="center">-</td></tr>
<?php } ?>
    </tbody>
    <?php echo form_close();?>
</table>

