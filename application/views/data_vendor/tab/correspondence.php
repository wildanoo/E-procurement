<div id="container8">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	
   
   $('#btn_create8').click(function(){  
   		$("#container8").load(baseUrl+'data_vendor/form_correspondence');   		
   });   	
   
 }); 
 
 function download(id){
 	
 	var baseUrl  = "<?php echo base_url(); ?>";
 	var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	
	$.post( baseUrl+ "data_vendor/download_file", { csrf: token, id: id } );
	
 }
 
 function update_corresp(id){
 	
 	$("#container8").load(baseUrl+'data_vendor/form_update_correspondence/'+id); 
 }
 
 function delete_corresp(id){
 	
 	var baseUrl  = "<?php echo base_url(); ?>";
 	var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';
    if (confirm('Are you sure you want to delete this?')) {
	 	$.post( baseUrl+ "data_vendor/delete_correspondence", { csrf: token, id: id })
		  .done(function( resp ) {
		    	$("#container8").load(baseUrl+'data_vendor/load_correspondence', function(){$('#cor_msg').html('Data akan berubah setelah proses approval disetujui')});
		});
	}	
 }
  
</script>
<div id="cor_msg" class="bg-warning text-center text-danger">
	<?= $this->session->flashdata('msg_Cor'); ?>
</div>

 <?php if($this->permit->upven){ ?>
<div style="text-align: right;">
<button type="button" id="btn_create8" class="btn btn-info btn-primary">Add New</button>
</div>
<?php } ?>
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Document Name</th>
		<th>Document Number</th>
		<th>Description</th>
		<th>Effective Date</th>
		<th>Expired Date</th>
		<th>Remark</th>
		<?php if($this->permit->upven){ ?><th>Action</th><?php } ?>
	</tr>
	</thead>
	<tbody>

 <?php  
 if($correspondence){ $i=1;
 	foreach($correspondence as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->doc_name; ?></td>	
			<td><?php echo $row->doc_num; ?></td>
			<td><?php echo $row->desc; ?></td>
			<td><?php echo $row->eff_date; ?></td>
			<td><?php echo $row->exp_date; ?></td>
			<td><?php echo $row->remark ?></td>
			<?php if($this->permit->upven){ ?>
			<td style="text-align: center;">			
			<?php  			
					echo '<a href="javascript:void(0)"><i class="fa fa-pencil" onclick="update_corresp('.$row->id.')"></i></a> | '; 				
					echo '<a href="javascript:void(0)"><i class="fa fa-trash"  onclick="delete_corresp('.$row->id.')"></i></a>';	
			?></td>	
			<?php } ?>										
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="8">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>