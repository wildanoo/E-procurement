<script type="text/javascript">
$(document).ready(function(){
   var baseUrl  = "<?php echo base_url(); ?>";	
   $(function () { 	$("#upload1").hide();  });
   
   $('#id_company').change(function(){  
   		var id_comp = $('#id_company').val();
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'policy_agreement/get_office',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_comp='+id_comp,
               success: function(resp){
               	  $('#id_office').html(resp);
               }
            });
   }); 
   
   $('#id_cat').change(function(){  
   		var id_cat = $('#id_cat').val();
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'policy_agreement/get_subcategory',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_cat='+id_cat,
               success: function(resp){
               	  $('#id_subcat').html(resp);
               }
            });
            $.ajax({
               url  : baseUrl + 'policy_agreement/get_vendor',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_cat='+id_cat,
               success: function(resp){
               	  $('#id_vendor').html(resp);
               }
            });
   });      
     
   
   $('#first_valdate').datepicker({  
        changeMonth: true, changeYear: true,
		dateFormat: "yy/mm/dd" ,
     });      
     
   $('#end_valdate').datepicker({  
        changeMonth: true, changeYear: true,
		dateFormat: "yy/mm/dd" ,
     }); 
     
   $('#publish_date').datepicker({  
        changeMonth: true, changeYear: true,
		dateFormat: "yy/mm/dd" ,
     });    

   $("input:radio[name=content]").click(function () {
		var opt = $("input:radio[name=content]:checked").val();
		if (opt == '2'){
			$("#upload1").show();	 $("#wyswyg").hide(); 
		} else {
			$("#wyswyg").show();     $("#upload1").hide();	}
     });
    
}); 

</script>

<div class="page-header">
  <h3>Update Policy Agreement</h3>
</div> 

<div id="debug"></div>
<?php echo form_open_multipart('policy_agreement/update',array('id'=>'form'));	$current_date = date('Y/m/d')?>

<table width="80%" border="0">
<?php echo form_hidden('id',$this->uri->segment(3)); ?>
<?php echo form_hidden('type',$def->type); ?>
<?php echo form_hidden('file',$def->file); ?>
	<tr id="option">
	<tr>
	<td><label>Type</label></td><td>:</td>
	<td><?php //echo $def->type; ?><?php echo array_key_exists($def->type, $subcontent)?$subcontent[$def->type]:'-'; ?><?php //$type = $this->uri->segment(3); ?><?php //if ($type==4) echo "Direct Selection"; else if ($type==3) echo "Open Bidding"; else if ($type==2) echo "Open Sourcing"; else if ($type==1) echo "General"; ?></td>			
		
	</td>	
</tr>
	 <td><label>Content</label></td><td>:</td>
	  <td>
		  <input type="radio" name="content" value="1" checked> Typing &nbsp;&nbsp;
		  <input type="radio" name="content" value="2"> Upload<br>
	  </td>
	</tr>

	<tr id="wyswyg">
		<td colspan="2">&nbsp;</td>
		<td class="form_tab_content" width="75%;">
			<div>
				<ul class="nav nav-tabs test2">
				    <li class="active"><a data-toggle="tab" href="#idn" aria-expanded="true">Indonesia</a></li>
				    <li class=""><a data-toggle="tab" href="#eng" aria-expanded="false">English</a></li>
				</ul>

				<div class="tab-content">
				    <div id="idn" class="tab-pane fade active in" style="padding:15px;">
				      <?php $data['content'] = $contents[0]; $this->load->view('policy_agreement/note',$data); ?>
				    </div>
				    <div id="eng" class="tab-pane fade" style="padding:15px;">
				      <?php $data['content'] = $contents[1]; $this->load->view('policy_agreement/note',$data); ?>
				    </div>
				</div>
			</div>
		</td>
	</tr>
	
	<!-- <tr><td colspan="2">&nbsp;</td>			    
		<td id="wyswyg" width="75%;"><?php //$this->load->view('policy_agreement/note'); ?></td>					
	</tr> -->
		
	<tr id="upload1">
		<td colspan="2">&nbsp;</td>				
		<td class="form_tab_content" width="75%;">
			<div>
				<ul class="nav nav-tabs test2">
				    <li class="active"><a data-toggle="tab" href="#upidn" aria-expanded="true">Indonesia</a></li>
				    <li class=""><a data-toggle="tab" href="#upeng" aria-expanded="false">English</a></li>
				</ul>
				<?php $style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";?>
				<div class="tab-content">
				    <div id="upidn" class="tab-pane fade active in" style="padding:15px;">
				      <?php	
						$data  = array('type'=>'file','name'=> 'contentfileind','id' => 'file_upload_ind','value'=>'true','content'=>'Import','style'=>$style); 
						echo form_input($data); ?> <i>*.txt</i>
				    </div>
				    <div id="upeng" class="tab-pane fade" style="padding:15px;">
				      <?php	
						$data  = array('type'=>'file','name'=> 'contentfileeng','id' => 'file_upload_eng','value'=>'true','content'=>'Import','style'=>$style); 
						echo form_input($data); ?> <i>*.txt</i>
				    </div>
				</div>
			</div>
		</td>
	</tr>
<tr>
	<td style="vertical-align: top;"><label>Title</label></td><td style="vertical-align: top;">:</td>
	<td><textarea name="title" id="title" cols="20" rows="5" class="form-control" required><?php echo $def->title; ?></textarea></td>
</tr>
<tr id="creator_area">
	<td><label>Creator</label></td><td>:</td>
	<td>&nbsp;&nbsp;<?php echo ucfirst($def->creator); ?><?php echo form_hidden("creator",$username); ?>
	<!--input type="text" name="creator" id="creator" value="<?php //echo $def->creator; ?>" placeholder="Creator" class="form-control"/--></td>	
</tr>
<tr> 
	<td><label>Attachment</label></td><td>:</td>
	<!-- td><?php	
		//$data  = array('type'=>'file','name'=> 'pdffile','id' => 'file_upload','value'=>'true','content'=>'Import'); 
		//echo form_input($data); ?><?php //echo $attfile; ?>&nbsp;<i>*.pdf</i> -->
<td><a href="<?php echo base_url().'policy_agreement/form_attach/'.$this->uri->segment(3); ?>" type="button" class="btn btn-primary btn-sm">Add File</a></td>
</tr>
	</td>
</tr>

<table id="browse" class="table table-striped table-bordered text-center" style="font-size: 13px">
	<thead>
        <tr>
            <th>No</th>
            <th>Deskripsi</th>
            <th>Keterangan</th>
            <th>Created Date</th>
            <th>Attachment</th>           
            <th>Action</th>
        </tr>
	</thead>
	
	<tbody>
<?php			
 if($cari){ $i=1;
 	foreach($cari as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->deskripsi; ?></td>	
			<td><?php echo $row->keterangan; ?></td>
			<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
			<td><a href="<?=base_url('uploads/attach/'.$row->name_file.'.pdf')?>" target="_blank">Viewed</a><?php //echo anchor('policy_agreement/open_content_pdf/'.$row->id,"Viewed",array('class'=>'default_link','title'=>'Viewed')); ?></td>
			<td style="text-align: center;">
			<?php				
					//echo '<i class="fa fa-trash"  onclick="delete_file('.$row->id.')"></i>';	
			?>
				<?php	  
					$pencil = '<i class="fa fa-pencil"></i>';
					echo anchor('policy_agreement/form_update_attach/'.$row->id, $pencil,array('class'=>'default_link','title'=>'Update File')); ?>&nbsp
				<?php	
				$trash  = '<i class="fa fa-trash"></i>';
				$js     = "if(confirm('Are you sure to delete record ?')){ return true; } else {return false; }";					
				echo anchor('policy_agreement/delete_file/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Record','onclick'=>$js));?>
			</td>											
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>
    </tbody>
</table>

<tr>
	<td colspan="3" style="text-align: right;">
		<input type="submit" value="Submit" class="btn btn-primary btn-sm"/>
		<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>'Cancel'); ?>					 
		<?php  echo anchor('policy_agreement/','Cancel',$prop); ?>
	</td>
</tr>
</table>
	

<?php echo form_close(); ?>



