<script>
	$(function() {
		var baseUrl   = "<?php echo base_url(); ?>";	
	});

	function load_data()
	{
		$('#main_container').load(baseUrl+'redlist/browse');
	}


</script>			

<div id="main_container">

<div id="debug"></div>
<div class="page-header"><h3>Vendor <?php echo $title; ?></h3></div>

<a href="javascript:void(0)" class="btn btn-info" onclick="load_data()">Back</a>

<?php echo form_open('redlist/export_excel',array('id'=>'myform')); ?>

<?php echo $this->session->flashdata("message"); ?>
<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
        <tr>
            <th>No</th>
            <th>Created Date</th> 
            <th>Username</th> 
            <th>User Group</th>            
            <th>Vendor Name</th> 
            <th>Description</th> 
            <th>Reason</th> 
        </tr>       
	</thead>
    <tbody>
<?php	
// echo form_hidden('total',count($browse));
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td style="text-align: center;"><?php  echo $num = $num+1; ?></td>
				<td><?php echo $row->log_date ?></td>
				<td><?php echo $row->username ?></td>
				<td><?php echo $row->group_name; ?></td>		
				<td><?php echo $row->vendor_name ?></td>
				<td><?php echo $row->description ?></td>
				<td><?php echo $row->reason ?></td>
				
			</tr>
<?php	$i++;  } ?>	
<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php  } else { ?>
	<tr><td colspan="9" align="center">-</td></tr>
<?php } ?>
    </tbody>
</table>

<?php echo form_close();?>

</div>

