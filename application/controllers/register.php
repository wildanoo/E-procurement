<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('m_register','m_vendor','codec','m_user'));
		$this->permit = new stdClass(); $this->global = new stdClass();	
		$this->permit = $this->crud->get_permissions("1"); /* 1 adalah function untuk registrasi  */			
		
		$venID   = $this->session->userdata("venID"); $stsID = 0;	
		if($venID) $stsID = $this->crud->browse("","vendor","id",$venID,"false","id_stsreg")->id_stsreg;		
		$this->global->register = $stsID;
		$this->global->active   = ($this->permit->uvd && $stsID=="3" ) ? true : false;
		$this->permit->md3      = ($this->permit->md3 && $stsID=="4" ) ? true : false;	
		
    }
    
 function index(){
 	if ($this->m_register->authenticate()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$slct   = "(SELECT id FROM register_status s WHERE s.id=l.id_stsreg) as id,
				   (SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as status";
		$status = $this->crud->browse_distinct("","vendor l",$slct,array('vendor_type'=>'new'));
		foreach($status as $row){ if($row->status) $list[$row->id] = $row->status;	}
		$data['status'] = $list;
		
		$id_stsreg   = $this->session->userdata('id_stsreg'); 
		$user_group  = $this->session->userdata('user_group'); 
		$user_level  = $this->m_vendor->get_user_level($user_group); $data['user_level'] = $user_level;		
		$where   	 = array('vendor_type'=>'new');	if($id_stsreg) $where['id_stsreg'] = $id_stsreg;	
		$session 	 = $this->session->userdata('browse'); 	
		if($session) $where = $session;


		if($user_level!=0){  $AND = "";		
			if($session) { 
				unset($session['vendor_type']);
				foreach($session as $key=>$val){ $param[]  = "`$key`='$val'"; }
				$AND  = implode(" AND ",$param); 
			}								
			$where = "`vendor_type` = 'new' AND `id_stsreg` = '5' AND `approval` >= '$user_level' $AND "; 
		}	  
		
 		$table 	    = "vendor";		
		$page       = $this->uri->segment(3);
		$per_page   = 10;		
		$offset     = $this->crud->set_offset($page,$per_page); 
		$total_rows = $this->crud->get_total_record("",$table,$where); 
		$set_config = array('base_url'=>base_url().'register/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);		
			 
		$this->load->library('pagination');			 
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();			
			
		$order 			     = array('field'=>'created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;		
		$select          	 = "id,register_num as reg_num,vendor_num,vendor_name,vendor_type,id_stsreg,approval,
								(SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as sts_reg,
							    created_date,last_updated";	

		$browse = $this->crud->browse_with_paging("",$table." l",$field="",$ID="","true",$select,$where,$order,$config['per_page'],$offset,"",$like=""); 
		
		$data['user_level'] = $user_level;	
		$data['browse']     = $browse;		
		$data['view']	    = "vendor/register/browse"; 		
 		$this->load->view('layout/template',$data); 
 	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/'); }  	
 }

 function get_tags(){
 	
 	$term     = $_GET['term']; 	
 	$order    = array('field'=>'vendor_name','sort'=>'ASC'); 
 	$order2   = array('field'=>'register_num','sort'=>'ASC'); 
 	$select   = "vendor_name as name";	 
 	$select2  = "register_num as name";	 
 	$vdr_name = $this->crud->autocomplete("","vendor",$select,"vendor_name",$term,array('vendor_type'=>'new'),$order); 	
 	$reg_num  = $this->crud->autocomplete("","vendor",$select2,"register_num",$term,array('vendor_type'=>'new'),$order2); 
	
 	if($vdr_name && $reg_num){	$result = array_merge($reg_num,$vdr_name); 	
	} elseif(!$vdr_name && $reg_num){ $result = $reg_num;
	} elseif($vdr_name && !$reg_num){ $result = $vdr_name; }
 	
 	
 	echo json_encode($result);   	 		
 }

 function set_session_browse(){
 	
 	$index      = $_POST['search_term'];  //print_r($_POST);
 	$is_exist   = $this->crud->is_exist("","vendor","id",array('register_num'=>$index));
 	$field      = $is_exist && $index ? "register_num" : "vendor_name"; 	
 	$start_date = $_POST['start_date'];
 	$end_date   = $_POST['end_date'];  	
 	$where      = array('vendor_type' => 'new');
 	
 	if($start_date && !$end_date){			
		$where['created_date >= '] = $start_date;
	} elseif(!$start_date && $end_date){		
		$where['created_date <= '] = $end_date;	
	} elseif($start_date && $end_date){
		$where['created_date >= '] = $start_date;
		$where['created_date <= '] = $end_date;	}
 	
 	if($index) $where[$field]  = $index;	
 					  
 	$this->session->set_userdata('browse',$where); 
 	redirect('register/');	
 	
 }

 function set_sess_status(){
 	
 	$id_status = $this->input->post('id_status');
 	if($id_status=="all") $this->session->unset_userdata('id_stsreg'); 
 	else $this->session->set_userdata('id_stsreg',$id_status); 	
 	
 }
 
 function reset_browse(){ 	
 	$this->session->unset_userdata('browse'); 	
 	$this->session->unset_userdata('id_stsreg'); 
 }

 function set_vendor_session(){
 	 	
 	$id = $this->uri->segment(3);
 	$this->session->set_userdata("venID",$id);
 	
 	if($this->uri->segment(4)=="register") {
	$this->session->set_userdata("feature","register"); } 	
 	
 	redirect("register/detail");
 	
 }
 
 function view_tracking(){
 	$id = $this->uri->segment(3);
 	$this->session->set_userdata("venID",$id);
 	$this->session->set_userdata('feature','register');
 	$this->session->set_userdata('tab','menu3');
 	redirect("register/detail"); 	
 }
 
 function detail(){ 	
 	if ($this->m_register->authenticate()) { 	
 		$data['user_id']  = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
		
		$venID   = $this->session->userdata("venID");	
		$select	 = "id,vendor_name as name,register_num as reg_num,vendor_type as type,vendor_num as num,vendor_address as address,
				   (SELECT id FROM vendor_rejected s WHERE s.id_vendor=l.id AND status=1 LIMIT 1) as rejected,	
				   (SELECT id FROM vendor_account  t WHERE t.id_vendor=l.id AND status=1) as completed,
				   (SELECT result FROM vendor_recomend u WHERE u.id_vendor=l.id LIMIT 1) as result,				
				   (SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as reg_sts,approval,
				   id_stsreg,web,phone,fax,email,postcode,npwp,npwp_address,npwp_postcode";					
 					
	 	  $none_bank 		= array('acc_number'	=> "none",
								 	'acc_name'		=> "none",
								 	'acc_address'	=> "none",
								 	'bank_name'		=> "none",
								 	'branch_name'	=> "none");
								 	
		 $none_akta 		= array('notaris_name'		=> "none",
								 	'notaris_address'	=> "none",
								 	'notaris_phone'		=> "none",
								 	'notaris_number'	=> "none",
								 	'notaris_date'		=> date('Y-m-d'),
								 	'remark'			=> "none");							 	
							 	
		$owner          = $this->crud->browse("","owner","id_vendor",$venID,"true");					 	
 		$bank           = $this->crud->browse("","bank_account","id_vendor",$venID,"false");
 		$akta           = $this->crud->browse("","akta","id_vendor",$venID,"false");
 		$data['bank']   = !$bank ? (object) $none_bank : $bank;		
 		$data['akta']   = !$akta ? (object) $none_akta : $akta;
 		$data['owner']  = $owner; 		
 		
 		$selectcor				= "*,(SELECT z.notif FROM reminder_doc z WHERE a.id=z.id_doc)as remindcount";
 		$data['correspondence'] = $this->crud->browse("","correspondence a","id_vendor",$venID,"true",$selectcor);
 		$whererem				= "id_vendor = $venID AND (CURDATE() BETWEEN a.exp_date - INTERVAL 90 DAY AND a.exp_date OR CURDATE() > a.exp_date)";
 		$data['remindbut']		= $this->crud->browse("","correspondence a","","","true",$selectcor,$whererem);
 		$data['remindbtn']		= $this->m_register->remindbtn($data['remindbut']);
 		
 		$data['org']   = $this->crud->browse("","organization","id_vendor",$venID,"true");
 		$data['ref']   = $this->crud->browse("","references","id_vendor",$venID,"true");
 		$data['aff']   = $this->crud->browse("","affiliates","id_vendor",$venID,"true");
 		$data['due_dill']   = $this->crud->browse("","due_dilligence","id_vendor",$venID,"true");
 		
 		$select2 = "id,id_category as id_cat,(SELECT category FROM category r WHERE r.id=l.id_category) as category,id_subcat,
					(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";					
 		$data['categories'] = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2); 		
 		$data['vendor'] = $this->crud->browse("","vendor l","id",$venID,"false",$select);		
		$data['browse'] = $this->crud->browse("","contact_person","id_vendor",$venID,"true");		
				
		$reg_status = $this->crud->browse("","register_status","","","true","id,status");
		foreach($reg_status as $val){	$status[$val->id] = ucwords($val->status);}
		$data['status'] = $status;

	 	$default_visit	   = array('note'=>"none");								
	 	$nonvisit          = $this->crud->browse("","non_visit_notes","id_vendor",$venID,"false","note"); //print_r($nonvisit);
	 	$data['nonvisit']  = !$nonvisit ? (object) $default_visit  : $nonvisit;	 		 	
	 	$data['vendor_visit'] = $this->crud->browse("","vendor_visit","id_vendor",$venID,"true");
	 	$data['files']     = $this->crud->browse("","non_visit_files","id_vendor",$venID,"true","id,filename as file");		
	 	
	 	
	 	$db_visit          = $this->crud->browse("","vendor_visit_status","id_vendor",$venID,"false","status"); 
	 	$visit             = !$db_visit ?  "0"  : $db_visit->status;	
	 	$data['visit']     = $visit;	 	
	 	

	 	$select2 = "(SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as reg_sts,
	 				id,created_id,notes,status,created_date";
	 	$data['notes'] = $this->crud->browse("","vendor_register_tracking l","id_vendor",$venID,"true",$select2);
	 	$data['recmd'] = $this->crud->browse("","vendor_recomend","id_vendor",$venID,"false","result as type,note");
	 	
	 	$user_group  = $this->session->userdata('user_group'); 
		$user_level  = $this->m_vendor->get_user_level($user_group); $data['user_level'] = $user_level;			
 				$acc_default	= array('recon_number'	=> null,
									 	'sap_number'	=> null,
									 	'srm_username'	=> null,
									 	'srm_password'	=> "",'status'=>'0');	 	
	 	$account = $this->crud->browse("","vendor_account","id_vendor",$venID,"false");
	 	$data['account'] = !$account ? (object) $acc_default : $account; 	
		$data['view']	 = "vendor/register/tabs"; 		
 		$this->load->view('layout/template',$data); 
	
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/'); 
	}	
 	
 }

 function validate_category(){
 	
 	$venID  = $this->session->userdata("venID");			   					
 	$subcat = $this->crud->browse("","vendor_category","id_vendor",$venID,"true","id_subcat as id"); 
 	
 	foreach($subcat as $val){ $id_subcat[] = $val->id ; }
 	$result = $this->m_register->category_validation($id_subcat); 	
 	$return = $result ? "true" : "false";
 	
 	echo $return;	
 }
 
 function validate(){ 		

 	/*   Modif By Dedi  */
 	
 	$vendor_name 	= $_POST['vendor_name']; 
 	$vendor_address = $_POST['vendor_address'];
 	$id_subcat		= $this->input->post('id_subcat');
 	$result     	= $this->m_register->category_validation($id_subcat);	
 	$cp_name		= $_POST['cp_name'];
 	$cp_mobile  	= $_POST['cp_mobile'];
 	$cp_email		= $_POST['cp_email'];  	
 	$phone			= $_POST['phone'];
 	$fax			= $_POST['fax']; 
 	$email			= $_POST['email']; 
 	$npwp 			= $_POST['npwp']; 
 	$text_pattern   = "/^[a-zA-Z0-9_+ .-]+$/"; 
 	$address_pattern= "/^[a-zA-Z0-9_\()+ .-]+$/"; 
 	$phone_pattern  = "/^[0-9 ]+$/"; 
 	$mobile_pattern = "/^[0-9 ]+$/";
 	$email_pattern  = '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'; 	

 	$vdr_exist      		= $this->crud->is_exist("","vendor","id",array('npwp'=>$npwp));
 	$val['vendor_name'] 	= $vendor_name && preg_match($text_pattern , $vendor_name)? "true" : "false";
// 	$val['vendor_name'] 	= !$vendor_name 	? "false" : "true"; 
 	$val['vendor_address'] 	= $vendor_address && preg_match($address_pattern , $vendor_address)? "true" : "false";
// 	$val['vendor_address']  = !$vendor_address	? "false" : "true"; 
 	$val['id_subcat'] 		= (!$id_subcat || !$result)	? "false" : "true";
 	$val['cp_name'] 		= $cp_name && preg_match($text_pattern , $cp_name)? "true" : "false";
// 	$val['cp_name'] 		= !$cp_name			? "false" : "true";
 	$val['cp_mobile'] 		= $cp_mobile && preg_match($mobile_pattern , $cp_mobile)		? "true" : "false";
 	$val['cp_email'] 		= preg_match($email_pattern , $cp_email)	? "true" : "false"; 	
 	$val['phone'] 			= preg_match($phone_pattern , $phone)	? "true" : "false";
 	$val['fax'] 			= preg_match($phone_pattern , $fax)		? "true" : "false"; 	
 	$val['email'] 			= preg_match($email_pattern , $email)	? "true" : "false"; 
 	$val['npwp']  			= !$vdr_exist && preg_match($phone_pattern , $npwp)? "true" : "false";
// 	$val['npwp']  			= !$npwp || $vdr_exist	? "false" : "true"; 

 	$msg['msg1']  = !$vendor_name 	|| !preg_match($text_pattern , $vendor_name)	? "Supplier name required or wrong" : ""; 
// 	$msg['msg1']  = !$vendor_name 		? "Supplier name required" : ""; 
 	$msg['msg2']  = !$vendor_address 	|| !preg_match($address_pattern , $vendor_address)	? "Address required or wrong" : ""; 
// 	$msg['msg2']  = !$vendor_address	? "Address required" : ""; 
 	$msg['msg4']  = (!$id_subcat || !$result) ? "Sub category required ( 2 category @4 sub category )" : "";  	
 	$msg['msg5']  = !$cp_name 	|| !preg_match($address_pattern , $cp_name)	? "Contact person name required or wrong" : ""; 
// 	$msg['msg5']  = !$cp_name			? "Contact person name required" : ""; 
 	$msg['msg11'] = !preg_match($email_pattern , $cp_email)	? "Wrong email" : "";  	
 	$msg['msg6']  = preg_match($phone_pattern , $phone)	? "" : "Phone required or wrong";
 	$msg['msg7']  = preg_match($phone_pattern , $fax)		? "" : "Fax required or wrong"; 
 	$msg['msg10'] = $cp_mobile && preg_match($mobile_pattern , $cp_mobile)		? "" : "Mobile phone required / wrong"; 
 	$msg['msg8']  = !preg_match($email_pattern , $email)	? "Wrong email" : ""; 
 	$msg['msg9']  = !$npwp || $vdr_exist || !preg_match($phone_pattern , $npwp)? "NPWP required or exist or blocked" : ""; 
 	$msg['status']= in_array("false",$val) ? "false" : "true"; 

 	echo json_encode($msg);

}

 function create(){
 	 	
 	$curr_date 	 = date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
	$footer		 = array('created_id'=>$userID,'approved_date'=>null,'created_date'=>$curr_date,'last_updated'=>$curr_date);	
	$regNum      = $this->codec->register(1);
	$arraycrypt  = array();	
	$post_subcat = $this->input->post('id_subcat');
				   foreach($post_subcat as $val){ $id_subcat[] = $val;	} 
	$all_subcat  = implode(",",$id_subcat); $sess_reg = uniqid();
	
 	$post = array(	'sess_reg'			=> $sess_reg,	
 					'register_num'		=> $regNum,	
 					'vendor_num'		=> "",
 					'vendor_type'		=> "new",
					'vendor_name'		=> !$_POST['vendor_name'] 		? "" : $_POST['vendor_name'],					
					'subcategory'		=> $all_subcat,					
					'vendor_address'	=> !$_POST['vendor_address'] 	? "" : $_POST['vendor_address'],
					'phone'				=> !$_POST['phone'] 			? "" : $_POST['phone'],
					'fax'				=> !$_POST['fax'] 				? "" : $_POST['fax'],	
					'email'				=> !$_POST['email'] 			? "" : $_POST['email'],	
					'overseas'			=> $_POST['overseas'],				
					'npwp'				=> !$_POST['npwp'] 				? "" : $_POST['npwp'],	
					'cp_name'			=> !$_POST['cp_name'] 	? "" : $_POST['cp_name'],
					'cp_mobile'			=> !$_POST['cp_mobile'] ? "" : $_POST['cp_mobile'],
					'cp_email'			=> !$_POST['cp_email'] 	? "" : $_POST['cp_email']);

	$data     = $post + $footer;
	$old_sess_reg = $this->session->userdata('sess_reg');
	if($old_sess_reg) $this->crud->delete("","vendor_temp","sess_reg",$old_sess_reg); 	
	$vdrID    = $this->crud->insert("","vendor_temp",$data);	
	$this->session->set_userdata('sess_reg',$sess_reg);
	
	/*array_push($arraycrypt, $regNum);
	array_push($arraycrypt, $curr_date);

	$url = $this->codec->request_encryption($arraycrypt);
 	redirect('register/policyagreement?regnum='.$regNum.'&sk='.$this->get_encrypt($regNum));*/
 }

 function policyagreement(){
 	$regnum = $_GET['regnum'];
 	$key = $_GET['sk'];
 	if ($this->get_encrypt($regnum) == $key) {
 		$def =  $this->crud->browse("","policy_agreement","type","1","false","file"); 		
 	
	 	if($def->file){

	 		if ($this->session->userdata('lang') == 'eng') {
	 			$ctpath  = "./uploads/policy_agreement/".$def->file."_eng.txt";
	 		}elseif ($this->session->userdata('lang') == 'ind') {
	 			$ctpath  = "./uploads/policy_agreement/".$def->file."_ind.txt";
	 		}

	 		$contents = file_exists($ctpath) ? file_get_contents($ctpath) : "-";
	 	}

	 	$data['trans_id'] = $regnum;
	 	$data['key'] = $key;
	 	$data['contents'] = !$contents ? "-" : $contents;

	 	$this->load->view('register/policyagreement',$data);
 	}

 }

 function afterregist($id){
 	$data['registration_number'] = $this->crud->browse("","vendor","id",$id,"false","register_num as num")->num;

 	$this->load->view('register/after_regist',$data);
 }

 function confirmation(){ 
 		$sess_reg = $this->session->userdata('sess_reg'); 
 		if($sess_reg){
		    $vtemp   	  = $this->crud->browse("","vendor_temp","sess_reg",$sess_reg,"false"); 		    
		    $cp_vendor	  = array('id_vendor'		=> 0,
							 	  'fullname'		=> $vtemp->cp_name,
							 	  'position'		=> "-",	
							 	  'mobile'			=> $vtemp->cp_mobile,
							 	  'email'		    => $vtemp->cp_email,			 	
							 	  'status'			=> 'active',
							 	  'created_id'		=> '0',
							 	  'created_date'	=> date('Y-m-d H:i:s'),
							 	  'last_updated'	=> date('Y-m-d H:i:s'));
		    
		    $subcategory = explode(",",$vtemp->subcategory);
		    unset($vtemp->sess_reg); unset($vtemp->subcategory); unset($vtemp->id); 
		    unset($vtemp->cp_name);  unset($vtemp->cp_mobile); unset($vtemp->cp_email);	    
		    
			$id_vendor = $this->crud->insert("","vendor",$vtemp);						
			$cp_vendor['id_vendor'] = $id_vendor;			
			$this->crud->insert("","contact_person",$cp_vendor);			
			$this->crud->delete("","vendor_temp","sess_reg",$sess_reg);
			$this->session->unset_userdata('sess_reg'); 
			
			foreach($subcategory as $val){				
				$id_cat    = $this->crud->browse("","subcategory","id",$val,"false","id_cat")->id_cat;				
				$category[]= array('id_vendor'		=> $id_vendor,
								   'id_category'	=> $id_cat,
								   'id_subcat'		=> $val,					 	
								   'status'			=> '0',
								   'created_id'		=> '0',
								   'created_date'	=> date('Y-m-d H:i:s'),
								   'last_updated'	=> date('Y-m-d H:i:s')); }	
			
			 if($category) $this->crud->multiple_insert("","vendor_category",$category);
			
			//------check vendor registration--------------
			$session   = md5(uniqid());				 	  
			$register  = array('id_vendor'		=> $id_vendor,
							   'session'		=> $session,
							   'status'			=> 1,				 	 
							   'created_date'	=> date('Y-m-d H:i:s'),
							   'last_updated'	=> date('Y-m-d H:i:s'));							 	  
			$this->crud->insert("","vendor_register",$register);			 	
			//---- finalization ---------------------------
			$vdrID   = $id_vendor;
		 	$select  = "register_num as num,vendor_name as name,email";
		 	$vendor  = $this->crud->browse("","vendor","id",$vdrID,"false",$select);		 	
		 	$to      =  $vendor->email;
			$subject =  "Notification E-procurement Garuda";			
			$link    =  base_url()."register/status/".$session;
			
			$param			= array('reg_num'=>$vendor->num,'vendor_name'=>$vendor->name,'link'=>$link);
			$param['view']  = NOTIF_PATH."EPCGA001"; 		
 			$message 		= $this->load->view(NOTIF_TMPL,$param,true); 			
			$this->crud->sendMail($to,$subject,$message);
			//---------------------------------------------	
			
			$this->m_vendor->register_tracking($vdrID,"Registration");
			
			$data['msg'] = "Thank you, registration was successfully completed, please check your email ";		
		} else { $data['msg'] = "Sorry, registration was ignored, please contact administration"; 	}	
		
		$this->load->view('register/msg',$data);

 }
 
 function delete_session(){
 	$sess_reg = $this->session->userdata('sess_reg'); 
 	$this->crud->delete("","vendor_temp","sess_reg",$sess_reg);
 	$this->session->unset_userdata('sess_reg'); 
 }

 function get_encrypt($regnum){
 	$key = hash("md5", $this->codec->getRealIpAddress().$regnum);

 	return $key;
 }

 function set_default_vendor(){
 	
 	$post   = $this->input->post('reg_num');
 	$get    = $this->uri->segment(3);
 	$regNum = !$post ? $get : $post;

 	$vendor  = $this->crud->browse("","vendor","register_num",$regNum,"false","id"); 	
 	if($vendor)	$this->session->set_userdata('venID',$vendor->id); 	
	
 	redirect('register/show_status');	
 	
 }
 
 function register_validation(){
 	
 	$reg_num  = $this->input->post('reg_num');
 	$is_exist = $this->crud->is_exist("","vendor","id",array('register_num'=>$reg_num)); 	
 	$result   = $is_exist ? "true" : "false";
 	$msg      = $is_exist ? "" : "Invalid Registration Number";
 	
 	$array['status']  =  $result;	
	$array['msg']     =  $msg;	
	echo json_encode($array);	
 }

 function destroy($id){
	$this->crud->delete("","vendor_temp","id",$id);

	redirect('');
 }

 function show_status(){
 	
 	$venID   = $this->session->userdata('venID');
 	$select = "id,register_num as num,vendor_name as name,id_stsreg,(SELECT status FROM register_status WHERE id=id_stsreg) as sts";
	$data['vendor']  = $this->crud->browse("","vendor","id",$venID,"false",$select); 	
	$data['step']    = $this->crud->browse("","register_status","","","true","id,status,remark");
 	
 	$this->load->view('register/new_status',$data);
 	
 }

 function status(){
 	
 	$status  = $this->uri->segment(3);
 	$result  = is_numeric($status) ? "true" : "false";
 	$venID   = $this->session->userdata('venID');
 	
 	if($result=="false" && $status!="false" && !$venID){ 
 		if($this->crud->is_exist("","vendor_register","id",array('session'=>$status,'status'=>'1'))){ 
		$venID   = $this->crud->browse("","vendor_register","session",$status,"false","id_vendor")->id_vendor;			
		$this->crud->update("","vendor_register","session",$status,array('status'=>"0"));	}
	} 
		
 	if($status=="false" || !$venID){
		$this->session->unset_userdata('venID');	$data  = array();
		$data['content'] = 'register/form_check';
	} else {
		$select = "id,register_num as num,vendor_name as name,id_stsreg,(SELECT status FROM register_status WHERE id=id_stsreg) as sts";
	 	$data['vendor']  = $this->crud->browse("","vendor","id",$venID,"false",$select); 	
	 	$data['step']    = $this->crud->browse("","register_status","","","true","id,status,remark");
 		$data['content'] = 'register/new_status'; 		
	}
	
	$this->session->unset_userdata('venID');
	$this->load->view('register/info',$data);	
 	 
 }
 
 function download_vendor_policy(){
 	
 	$lang     = $this->session->userdata('lang');	
 	$filename = "general_".$lang.".txt"; 
 	$path 	  = './uploads/policy_agreement/'.$filename;			
 	$is_exist = file_exists($path) ? "true" : "false";
 	
 	if($is_exist){ 
 		ob_start();	 
 		$data['title'] = $this->lnag->line('global_vendor_tag');  			
 		$data['path'] = $path; 		
 		$this->load->view('notifications/templates', $data);
 		$html = ob_get_contents();
        ob_end_clean();
        
        require_once('./application/libraries/html2pdf/html2pdf.class.php');
        $pdf = new HTML2PDF('P','A4','en');
        $pdf->WriteHTML($html);
        $pdf->Output('general_policy.pdf', 'D'); 		
	} 	
 	
 }
 
 function download_petunjuk_teknis(){ 	
 	
 	$lang     = $this->session->userdata('lang');	
 	$filename = "technical_".$lang.".txt"; 
 	$path 	  = './uploads/cms/'.$filename;			
 	$is_exist = file_exists($path) ? "true" : "false";
 	
 	if($is_exist){ 
 		ob_start();	
 		$data['title'] = $this->lang->line('header_file_technical');  		
 		$data['path']  = $path; 		
 		$this->load->view('notifications/templates', $data);
 		$html = ob_get_contents();
        ob_end_clean();
        
        require_once('./application/libraries/html2pdf/html2pdf.class.php');
        $pdf = new HTML2PDF('P','A4','en');
        $pdf->WriteHTML($html);
        $pdf->Output('petunjuk_teknis.pdf', 'D'); 		
	} 	
 	
 }
	 
}