<div id="container3">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   
   $('#btn_cancel').click(function(){ 
   		var id_visit = $("#id_visit").val();
   		$("#container15").load(baseUrl+'due_diligence/view_category/'+id_visit);   		
   }); 	
   
    $('#btn_save3').click(function(){ 
	 	var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	    
	    var values   = $("#form_assest").serializeArray();
		values.push({name: csrf,value: token});
		values = jQuery.param(values); 

		 	$.post( baseUrl+ "due_diligence/save_assesment", values )
			  .done(function( resp ) {
			    	//$("#container15").load(baseUrl+'vendor/load_due_dilligence');
			    	var id_visit = $("#id_visit").val();
   					$("#container15").load(baseUrl+'due_diligence/view_category/'+id_visit); 
			    	//$("#debug3").html(resp);
			});
     }); 
     
    $('#btn_add3').click(function(){
     	
     	 $("#container3").load(baseUrl+'due_dilligence/form_category');
     	
    }); 
     
    $('#btn_conc3').click(function(){
     	
     	$("#container3").load(baseUrl+'due_dilligence/form_conclutions');
     	
    }); 
    	
   
 });  
  
 </script>
<div id="debug3"></div>
<?php //if($this->permit->uvd) {  ?>
<i class="fa fa-backward" id="btn_cancel"></i>&nbsp;&nbsp;Previous Page&nbsp;&nbsp;&nbsp;
<!--button type="button" id="btn_add3" class="btn btn-info btn-primary">New Desc.</button-->
<button type="button" id="btn_save3" class="btn btn-info btn-primary">Save</button>
<!--button type="button" id="btn_conc3" class="btn btn-info btn-primary">Conclusions</button-->
<?php //}
echo form_open('#',array('id'=>'form_assest')); ?> 
<input type="hidden" name="id_visit" id="id_visit" value="<?php echo $id_visit;  ?>"/>
<input type="hidden" name="id_subcat" id="id_subcat" value="<?php echo $id_subcat;  ?>"/>
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Description</th>
		<th>Result</th>		
		<th>Remark</th>		
		<?php //if($this->permit->uvd){ ?><!--th>Action</th--><?php //} ?>
	</tr>
	</thead>
	<tbody>

 <?php   //$assesment = array();
 
 $venID   = $this->session->userdata("venID"); 
 
 if($assesment){ $i=1;
 	foreach($assesment as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->description; ?></td>
			<td><?php 			
				//$where    = array('id_assesment'=>$row->id);
				$where    = array('id_vendor'=>$venID,'id_assesment'=>$row->id,'id_visit'=>$id_visit,'id_subcat'=>$id_subcat);
				$db       = $this->crud->browse("","dd_vdr_assesment","id_vendor",$venID,"false","result,remark",$where);
				$none     = array('result'=>'0','remark'=>'none');
				$default  = !$db ? (object) $none : $db; 
				$checked1 = $default->result=="1" ? "checked" : "";
				$checked2 = $default->result=="0" ? "checked" : "";		?>
				<input type="radio" name="<?php echo "asst_".$row->id; ?>" value="1" <?php echo $checked1; ?>> Yes &nbsp;&nbsp;
				<input type="radio" name="<?php echo "asst_".$row->id; ?>" value="0" <?php echo $checked2; ?>> No
			</td>
			<td>
				<input type="text"  name="<?php echo "rmk_".$row->id; ?>" class="form-control" placeholder="fill remark here"  value="<?php echo $default->remark; ?>"/>
			</td>
			<?php //if($this->permit->uvd){ ?>
			<!--td style="text-align: center;">			
			<?php 			
					/*$icon = '<i class="glyphicon glyphicon-download"></i>';			
					$prop = array('title'=>'Download'); 				 
				  	echo anchor('due_dilligence/download_duedill/'.$row->id,$icon,$prop);*/
		  	 
					//echo '&nbsp;&nbsp;&nbsp;';	
					//echo '<i class="fa fa-plus" onclick="update_docdd('.$row->id.')"></i>'; echo '&nbsp;&nbsp;&nbsp;';					
					//echo '<i class="fa fa-trash"  onclick="delete_docdd('.$row->id.')"></i>';
			?></td-->
			<?php// } ?>											
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="8">-</td></tr>
<?php } ?>
     </tbody>
</table>
<?php echo form_close(); ?>

</div>