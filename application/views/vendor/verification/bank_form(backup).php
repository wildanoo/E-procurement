<script type="text/javascript">
  $(function () { 
   	  var baseUrl  = "<?php echo base_url(); ?>";   	 
     
  });

</script>

<div id="debug"></div>

<h2>Add a Bank Account</h2>

<?php echo form_open('verification/bank_account',array('id'=>'form_bank'));  ?>
<input type="hidden" name="id_vendor" id="id_vendor" value="<?php echo $id_vendor;  ?>"/>
<table width="70%">
	<tr>
		<td><label>Bank Name</label></td><td>:</td>
		<td><input type="text" name="bank_name" id="bank_name" class="form-control" placeholder="Bank Name" required/></td>
	</tr>
	<tr>
		<td><label>Branch Name</label></td><td>:</td>
		<td><input type="text" name="branch_name" id="branch_name" class="form-control" placeholder="Branch Name" required/></td>
	</tr>
	<tr>
		<td><label>Account Number</label></td><td>:</td>
		<td><input type="text" name="acc_number" id="acc_number" class="form-control" placeholder="Account Number" required/></td>
	</tr>
	<tr>
		<td><label>Account Name</label></td><td>:</td>
		<td><input type="text" name="acc_name" id="acc_name" class="form-control" placeholder="Account Name" required/></td>
	</tr>
	<tr>
		<td><label>Country</label></td><td>:</td>
		<td><input type="text" name="acc_country" id="acc_country" class="form-control" placeholder="Account Country" required/></td>
	</tr>			
	<tr>
		<td style="vertical-align: top;"><label>Account Address</label></td><td style="vertical-align: top;">:</td>
		<td><textarea name="acc_address" id="acc_address" cols="20" rows="5" class="form-control" placeholder="Account Address" required></textarea></td>
	</tr>
	
	<?php if(validation_errors()) { ?>
		<ul style="color:red;">
			<?php echo validation_errors('<li>','</li>'); ?>
		</ul>
	
	<?php } ?>
	<tr>
		<td colspan="3"><?php echo $img; ?></td>
		
	</tr>
	<tr>
		<td>Type the code</td><td>:</td>
		<td><input type="text" name="captcha" placeholder="Type the code"/></td>
	</tr>
	
	
	<tr>
		<td align="left" colspan="3">			
			<button type="submit" id="btn_visit" class="btn btn-info btn-primary">Submit</button>			
		</td>
	</tr>
</table>

