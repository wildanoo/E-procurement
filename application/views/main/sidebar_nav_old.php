<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
        <li class="nav-item start active open">
            <a href="<?php echo base_url()?>auth/login#top" class="nav-link nav-toggle">
                <i class="fa fa-home"></i>
                <span class="title"><?php echo $this->lang->line('global_home_tag');?></span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="<?php echo base_url()?>auth/login#news" class="nav-link nav-toggle">
                <i class="fa fa-bullhorn"></i>
                <span style="word-wrap: break-word;" class="title"><?php echo $this->lang->line('global_newsannouncement_tag');?></span>
                <span class="arrow"></span>
            </a>
        </li>  
        <li class="nav-item  ">
            <a href="<?php echo base_url()?>auth/login#faq" class="nav-link nav-toggle">
                <i class="fa fa-question-circle"></i>
                <span class="title">FAQs</span>
                <span class="arrow"></span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="<?php echo base_url()?>auth/login#contact" class="nav-link nav-toggle">
                <i class="fa fa-map-marker"></i>
                <span class="title"><?php echo $this->lang->line('global_contactus_tag');?></span>
                <span class="arrow"></span>
            </a>
        </li>
    </ul>
</div>