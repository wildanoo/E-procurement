<!-- MODAL -->
<div class="container">
<h1>Modal Example</h1>
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">


    <form class = "form-horizontal">
      <div class="box-body">
        <div class = "col-md-12">

          <div class="form-group">
            <label for="effectivedate" class="col-md-3 inputbox-size-right">Validity Period</label>
            <div class="col-md-9" style="margin-top: -10px; margin-bottom: -10px;">
              <div class="col-md-5">
                <div class="form-group clear-mar-bottom">
                    <div class="input-group date row" id="datetimepicker7">
                        <input type="text" class="form-control" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
              </div>
              <div class = "col-md-2 text-center">
                <label class = "control-label" style="margin-top: 13px;">to</label>
              </div>
              <div class="col-md-5">
                  <div class="form-group clear-mar-bottom">
                      <div class="input-group date row" id="datetimepicker8">
                          <input type="text" class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                  </div>
              </div>
            </div>
          </div><!-- form-group -->

          <div class="form-group">
            <label for="title" class="col-md-3 control-label inputbox-size">Title</label>
            <div class="col-md-9 row">
              <input type="text" class="form-control form-margin" id="title" placeholder="Title">
            </div> 
          </div>

        <div class="form-group clear-pad-top">
          <label for="category" class="col-sm-3 control-label inputbox-size">Type</label>
          <div class="col-sm-6 control-label clear-pad-top row">
              <select class = "form-control form-margin" name="category" id="category">
                <option value = "">--- Category ---</option>
                <option value = "">News</option>
                <option value = "">Open Sourcing</option>
                <option value = "">Open Bidding</option>
                <option value = "">Invitation</option>
              </select>
          </div>
        </div>

        <div class="form-group">
          <label for="namedesc" class="col-md-3 control-label inputbox-size">Creator</label>
          <div class="col-md-9 row">
            <input type="text" class="form-control" id="namedesc" placeholder="Creator">
          </div>
        </div>


        <div class = "form-group">
          <label for="serviceclass" class="col-sm-3 control-label inputbox-size" >Content</label>
            <div class="col-lg-9 col-sm-6 col-12 row inputbox-size">
              <input type="radio" name="content" value="1" > <span style="vertical-align: sub;margin-left: 5px;">Typing</span> &nbsp;&nbsp;&nbsp;
              <input type="radio" name="content" value="2" checked=""> <span style="vertical-align: sub;margin-left: 5px;">Upload</span><br>
            </div>
        </div>

        <div class = "form-group">
          <label for="serviceclass" class="col-sm-3 control-label inputbox-size" ></label>
            <div class="col-lg-9 col-sm-6 col-12 row" >
              <div class="input-group">
                <input type="text" class="form-control" placeholder="No file chosen (.jpg, .png, .gif)" readonly>
                <span class="input-group-btn">
                    <span class="btn btn-crt btn-file">Browse<input type="file" multiple></span>
                </span>
              </div>
            </div>
        </div>


        <div class = "form-group" id="upload41">
          <label for="serviceclass" class="col-sm-3 control-label inputbox-size" ></label>
          <div class="col-lg-9 col-sm-6 col-12 row" >
            <div class="input-group">
              <?php 
              $style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";
              $style = "";
                $data3  = array('type'=>'text','name'=> 'loifile','id' => 'file_upload','placeholder'=>'No file chosen (.pdf)','content'=>'Import','class' => 'form-control','style'=>$style); 
                echo form_input($data3); ?> 
              <span class="input-group-btn">
                <span class="btn btn-crt btn-file">Browse<input type="file" multiple></span>
              </span>
            </div>
          </div>
        </div>
        </div><!-- col-md-12 -->
      </div>
    </form>


      </div>
      <div class="box-footer" style="padding-left: 30px;">
          <button type="submit" class="btn btn-default pull-right">Cancel</button>
          <button type="submit" class="btn btn-danger pull-right">Submit</button>
      </div><!-- /.box-footer -->
    </div>
    
  </div>
</div>

</div>

<!-- MODAL -->