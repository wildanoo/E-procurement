
Dengan hormat,<br/>
<b>Bapak/Ibu <?php echo $to; ?></b><br/>

Bersama ini kami sampaikan bahwa telah terjadi perubahan data vendor yang membutuhkan proses approval dari Bapak/Ibu <?php echo $to; ?>. 
<br/>
Berikut ini adalah informasi detail vendor.
<br/>
Company Name :<?php echo $vendor_name; ?>	<br/>
No. Registrastion:<?php echo $reg_num ?> <br/><br/>


Untuk proses persetujuan dan detail informasi perusahaan dapat diakses :<br/>
<?php echo $link; ?><br/>

Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/><br/>

(&nbsp;<b><?php echo ucfirst( $this->tank_auth->get_username()); ?></b>&nbsp;)

