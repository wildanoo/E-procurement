<html lang="en-US">
<head>
  <title>E-Procurement Garuda</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/style.css">
  <link rel="icon" href="<?php echo base_url(); ?>assets/favicon-grd.png" sizes="16x16">
  <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#create').click(function(){
        var baseUrl   = "<?php echo base_url(); ?>";
        var login     = $('#login').val();
        var password  = $('#password').val();
        var content_id= '<?php echo $this->uri->segment(3); ?>';
        var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>';
        var token     = '<?php echo $this->security->get_csrf_hash(); ?>';

        $.ajax({
                url  : baseUrl + 'auth/validate_login',
                type : "POST",
                data : csrf +'='+ token +'&login='+login+'&password='+password+'&content_id='+content_id,
                success: function(resp){
                    //$('#debug').html(resp);
                    var json = $.parseJSON(resp);
                    if(json.status=="false"){
                      $("#message").html(json.errors);
                    } else {
                      document.location.href= baseUrl + 'auth/login';
                    }
          }
            });
      });
    });
  </script>
</head>
<style type="text/css">
  .form-regist,
  .form-regist-signup{
    padding: 40px;
    height: 370px;
  }
  .form-regist-signup{
    background: #ededea;
    margin-bottom: 55px;
  }
  .form-module-regist .cta a {
    color: #333333;
    text-decoration: none;
    font-size: 12px;
  }
  .form-module-regist ul {
    list-style: none;
  }
  .form-module-regist{
    position: relative;
    background: #ededea;
    width: 100%;
    border-top: 5px solid #3f9add;
    box-shadow: 0 0 3px rgba(0, 0, 0, 0.1);
    margin: 0 auto;
  }
  .form-module-regist input, .form-module-regist select {
    outline: none;
    display: block;
    width: 100%;
    border: 1px solid #d9d9d9;
    margin: 0 0 10px;
    padding: 10px 15px;
    box-sizing: border-box;
    font-wieght: 400;
    -webkit-transition: 0.3s ease;
    transition: 0.3s ease;
  }
</style>
<body class="hold-transition login-bgcolor">  
  <div class="row content-afterregis">
    <div class="col-md-12">
      
      <div class="col-md-10 col-md-offset-1 box box-info" id = "white-header">
        <div class="box-header with-border">
          <h2 class="box-title col-left" style="margin-top: 10px;"><strong style="color: #368ccc;font-size:20px;">Login or Register to join <?php echo $browse->title;?></strong></h2>
        </div>
        
        <hr style="margin-top: 0;">
        <?php if ($browse->subcontent_type == '4') { ?>
        <div class="col-md-4" style="border-right:1px solid #eee;<!-- box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.1); -->">
        <div class="form-module-regist">
            <?php
            $login = array( 'type'      => 'email',
                    'class'     => 'form-control-lg',
                    'placeholder'   => 'Username',
                    'name'      => 'login',
                    'id'      => 'login',
                    'value'     => set_value('login'),
                    'maxlength'   => 80,
                    'size'      => 30,);
            if ($login_by_username AND $login_by_email) {
              $login_label = 'Email or login';
            } else if ($login_by_username) {
              $login_label = 'Login';
            } else {
              $login_label = 'Email';
            }
            $password = array(  'type'      => 'password',
                      'class'     => 'form-control-lg',
                      'placeholder'   => 'Password',
                      'name'      => 'password',
                      'id'      => 'password',
                      'size'      => 30,);
            $remember = array(
              'type'  => 'checkbox',
              'name'  => 'remember',
              'id'  => 'remember',
              'value' => 1,
              'checked' => set_value('remember'),
              'class' => 'icheckbox_square-blue',
            );
            $captcha = array(
              'name'  => 'captcha',
              'id'  => 'captcha',
              'maxlength' => 8,
            );
          ?>
        <?php echo form_open($this->uri->uri_string(),array('id'=>'form_login')); ?>
          <div class="form-regist">
            <h2>LOGIN TO YOUR ACCOUNT</h2>
            <input name="content_id" value="<?php echo $this->uri->segment(3); ?>" type="hidden">
            <?php echo form_input($login); ?>
            <?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
            <?php echo form_password($password); ?>
            <?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
            <div id="message"></div>          
            <input type="button" value="Login" id="create" class="btn-primary">
            <ul>
              <li class="text-center" style="margin-top: 8px;">
                <div class="cta">
                  <a href="<?php echo base_url()?>auth/forgot_password">Forgot Password?</a>
                </div>
              </li>
            </ul>
          </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        <?php }?>
        <?php if ($browse->subcontent_type == '4') { ?>
        <div class="col-md-8">
        <?php }else{?>
        <div class="col-md-12">
        <?php }?>
          <div class="form-module-regist">
          <?php echo form_open('register/create'); ?>
          <div class="form-regist-signup col-md-6">
          <h2>SUPPLIER REGISTRATION</h2>
            <input type="text" placeholder="Supplier Name" name="vendor_name" id="vendor_name"> 
            <div id="msg1" style="color: red; font-style: italic; "></div>            
              <input type="text" placeholder="Supplier Address" name="vendor_address" id="vendor_address">
              <div id="msg2" style="color: red; font-style: italic; "></div>    
              
              <?php echo form_dropdown("id_category",$category,"",'class="form-control" id="id_category" style="font-size:12px;"'); ?>
              <div id="msg3" style="color: red; font-style: italic; "></div>  
              <select name="id_subcat" id="id_subcat" class="form-control" style="font-size:12px;">
              <option value="">--Select Category--</option>     
              </select><div id="msg4" style="color: red; font-style: italic; "></div> 
              <input type="tel" placeholder="NPWP" name="npwp" id="npwp"><div id="msg9" style="color: red; font-style: italic; "></div>
          </div>
          <div class="form-regist-signup col-md-6">
          <h2>&nbsp;</h2>
              <input type="contact" placeholder="Contact Person" name="cp" id="cp"><div id="msg5" style="color: red; font-style: italic; "></div>     
              <input type="text" placeholder="Phone Number" name="phone" id="phone"><div id="msg6" style="color: red; font-style: italic; "></div>      
              <input type="tel" placeholder="Fax Number" name="fax" id="fax"><div id="msg7" style="color: red; font-style: italic; "></div>         
              <input type="email" placeholder="Email Address" name="email" id="email">
              <div id="msg8" style="color: red; font-style: italic; "></div>
              <?php 
                //$blank_post = array('id_lang','id_country','id_region','district','postcode','city',
                        //'street','house_num','sprefix','ssuffix','building','floor',
                        //'room','bank_name','branch_detail','bckey','acc_holder','acc_num','bank_region');           
              //foreach($blank_post as $field){ echo form_hidden($field,""); }
              ?>
              <input value="Register" id="btn_register" class="btn-primary" type="button">
          </div>
          <?php echo form_close(); ?>
        </div>
        <div class = "col-md-3"></div>

      </div><!-- /.box-body -->
    </div>
    <footer class="main-footer footer-line container-fluid text-center" style="position:fixed;bottom:0;"> 
      Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
    </footer>
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

  </body>
  </html>