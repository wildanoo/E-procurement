 
 <?php echo form_open('user/update_status',array('id'=>'form','class'=>'form-horizontal'));
 
 	echo form_hidden('id',$default->id);
 ?>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">

              <div class="box-header with-border">
                <h3 class="box-title">Employee Information</h3>
              </div>

              <!--form class = "form-horizontal"-->
                <div class="box-body">

                  <div class="form-group">
                    <label for="category" class="col-md-3 control-label inputbox-size vcenter">Employee Number</label>                   
                    <div class="col-md-9 inputbox-size vcenter"> 
                    <?php  echo $default->nopeg; ?>

                    </div>
                  </div>

                  <div class="form-group">
                    <label for="namedesc" class="col-md-3 control-label inputbox-size vcenter">Employee Name</label>
                    <div class="col-md-9 inputbox-size vcenter"> 
                      <?php  echo $default->fullname;  ?>
                    </div>
                  </div>

                  <div class = "form-group">
                    <label for="serviceclass" class="col-sm-3 control-label inputbox-size vcenter" >Office</label>
                      <div class="col-md-9 inputbox-size vcenter">                       
                        <?php echo $default->id_office; ?>
                      </div>
                  </div>

                  <div class = "form-group">
                    <label for="serviceclass" class="col-sm-3 control-label inputbox-size vcenter" >Division</label>
                      <div class="col-md-9 inputbox-size vcenter">                       
                        <?php  
                          echo $default->id_div; ?>
                      </div>
                  </div>

                  <div class="form-group">
                      <label for="authority" class="col-md-3 control-label inputbox-size vcenter">Department</label>
                      <div class="col-md-9 inputbox-size vcenter">
                       <?php echo $default->id_dept; ?>
                      </div>
                    </div>
                    
                  <div class="form-group">
                      <label for="authority" class="col-md-3 control-label inputbox-size vcenter">Position</label>
                      <div class="col-md-9 inputbox-size vcenter">
                       <?php echo $default->id_position; ?>
                      </div>
                    </div>
					
				  <div class="form-group">
                      <label for="authority" class="col-md-3 control-label inputbox-size vcenter">Functional</label>
                      <div class="col-md-9 inputbox-size vcenter">
                       <?php $functional = !$default->id_functional ? 'Others' : $default->id_functional; echo $functional; ?>
                      </div>
                    </div>
                 
                  </div><!-- col-md-6 -->
                  </div>
                </div><!-- /.box-body -->

                <div class="box-footer" style="padding-left: 30px;">
                   
                    <?php  $prop = array('class'=>'btn btn-default','title'=>'Back'); 
                           echo anchor('user/','Back',$prop);  ?>	
                    <?php echo  form_close(); ?>
                </div><!-- /.box-footer -->
              <!--/form-->
            </div><!-- /.box -->
          </div>
        </div>