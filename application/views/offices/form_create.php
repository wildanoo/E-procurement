
<script type="text/javascript"> 	
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";		 
	  $(function(){  });
	  
	  $('#id_country').change(function(){  
   		var id_country = $('#id_country').val(); 
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
           $.ajax({
               url  : baseUrl + 'offices/get_region',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_country='+id_country,
               success: function(resp){
               	  $('#id_region').html(resp);
               }
            });
     });
});	  
</script>

<?php echo form_open('#',array('id'=>'form')); ?>
<table cellpadding="2" cellspacing="2">
<tr>
	<td><label>Office Code</label></td><td>:</td>
	<td><input type="text" name="office_code" id="office_code" placeholder="Office Code" class="form-control" required/></td>
	<td id="msg1"></td>	
</tr>
<tr>
	<td><label>Office Name</label></td><td>:</td>
	<td><input type="text" name="office_name" id="office_name" placeholder="Office Name" class="form-control" required/></td>
	<td id="msg2"></td>	
</tr>
<tr>
	<td><label>Country</label></td><td>:</td>
	<td><?php 
		$prop_country = 'class="form-control" id="id_country" style="width:220px"';
		echo form_dropdown('id_country',$country,'default',$prop_country); ?>			
	</td>
	<td id="msg3"></td>	
</tr>
<tr>
	<td><label>Area</label></td><td>:</td>
	<td>
		<select name="id_region" id="id_region" class="form-control" style="width:220px">
			<option value="">--Select Area--</option> 		
		</select>		
	</td>
	<td id="msg4"></td>		
</tr>
<tr>
	<td><label>Email</label></td><td>:</td>
	<td><input type="text" name="email" id="email" placeholder="Email" class="form-control" required/></td>
	<td id="msg5"></td>	
</tr>
<tr>
	<td><label>Phone</label></td><td>:</td>
	<td><input type="text" name="phone" id="phone" placeholder="Phone" class="form-control" required/></td>
	<td id="msg6"></td>	
</tr>
<tr>
	<td><label>Fax</label></td><td>:</td>
	<td><input type="text" name="fax" id="fax" placeholder="Fax" class="form-control" required/></td>
	<td id="msg7"></td>	
</tr>
</table>