<script>
	$(function(){
		$('#checkall').click(function(){
			$(this).closest('table').find('.ck_ex').prop('checked', this.checked);
		});
	});
</script>
<table id="v_tab" class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
		<tr>
			<th>No</th>
			<th><input type="checkbox" id="checkall" value="1"></th>
			<th>Registration Num.</th>
			<th>Vendor Num.</th>
			<th width="200">Vendor Name</th>
			<th width="300">Category - Subcategory</th>
			<th>Last Updated</th>
			<th>Reason</th>
			<th>Active Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$i = 1;
		foreach($browse as $row){
			foreach($detail as $key => $value){
				if($row->vID == $key){
					$detail_category = $value;
				}
			}
			?>
			<tr>
				<td><?= $num  = $num+1 ;?></td>
				<td><input type="checkbox" class="ck_ex" name="ck_list[]" id="ck<?=$i?>" value="<?=$row->inaID ?>">
				</td>
				<td><?= $row->register_num ?></td>
				<td><?= $row->vendor_num?></td>
				<td><?= $row->vendor_name?></td>
				<td><?php echo $detail_category ?></td>
				<td><?= date('M d, Y',strtotime($row->last_updated))?></td>
				<td><?= $row->reason?></td>
				<td><?php echo $this->m_active->ina_label($row->status,$row->approval);?></td>
				<td>
					<div class="btn-group dropup">
						<button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?php echo base_url('register/set_vendor_session/'.$row->vID); ?>">View Detail Vendor</a></li>
							<li><a href="<?php echo base_url('vendorlog/search/'.$row->vID.'/active') ?>">View Log</a></li>
							<?php if(!$this->permit->ias){ if($this->m_active->ina_status($row->status,$row->approval) == 'Request'){ ?><li><a href="javascript:void(0)" onclick="form_ina_app(<?php echo $row->inaID ?>)">Process</a></li><?php } }?>
							</ul>
						</div>
					</td>
				</tr>
				<?php  $i++; } ?>
				<tr>
					<td colspan="9" style="text-align: center;"><?php echo $pagination ; ?></td>
				</tr>
			</tbody>


		</table>
