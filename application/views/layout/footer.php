<!-- /container --> 
</div>
<!-- /main-inner --> 
</div>
</div>



<footer class="main-footer footer-line container-fluid text-center">
Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets_/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
</footer>

<!-- /footer --> 
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<!--script src="<?php echo base_url() ?>assets_/js/jquery-1.9.1.min.js"></script> 
<script src="<?php echo base_url() ?>assets_/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url() ?>assets_/js/excanvas.min.js"></script> 
<script src="<?php echo base_url() ?>assets_/js/chart.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url() ?>assets_/js/bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets_/js/full-calendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url() ?>assets_/js/base.js"></script--> 

<script>
      	 $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
          // Avoid following the href location when clicking
          event.preventDefault(); 
          // Avoid having the menu to close when clicking
          event.stopPropagation(); 
          // If a menu is already open we close it
          $('ul.dropdown-menu [data-toggle=dropdown]').parent().removeClass('open');
          // opening the one you clicked on
          $(this).parent().addClass('open');

          var menu = $(this).parent().find("ul");
          var menupos = menu.offset();
        
          if ((menupos.left + menu.width()) + 30 > $(window).width()) {
              var newpos = - menu.width();      
          } else {
              var newpos = $(this).parent().width();
          }
          menu.css({ left:newpos });
      });
</script><!-- /Calendar -->  
  
<script type="text/javascript">

 var baseUrl  = "<?php echo base_url(); ?>";  
 $(function () { 
 
 	$('#datetimepicker7, #datetimepicker8, #datetimepicker9').datetimepicker({
      format: 'YYYY-MM-DD'
  });

 	$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  }); 
  
}); 

</script>
</body>
</html>