<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_inactive extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('crud');
	}

	function authenticate(){
		$allowed  	= array(59);
		$permit  	= $this->session->userdata('permissions');
		$permit 	= explode(",",$permit);
		foreach($permit as $str){
			$permissions[] = preg_replace("/[^0-9]/","",$str);
		}
		$result 	= array_intersect($allowed, $permissions);
		if($result){ return true;} return false;
	}

	function is_permit($allowed){
		$permit 	= $this->session->userdata('permissions');
		$permit 	= explode(",",$permit);
		foreach($permit as $str){
			$permissions[] = preg_replace("/[^0-9]/","",$str);
		}
		$result 	= array_intersect($allowed, $permissions);

		if($result){ return true;} return false;
	}

	function ina_status($stats,$approv){

		if($this->permit->iibs && $stats == 'request' && $approv == '1'){$stat = "Request";}
		elseif($this->permit->iibs && $stats == 'request' && $approv == '2'){$stat = "Approved";}
		elseif($this->permit->iibs && $stats == 'request' && $approv == '3'){$stat =  "Approved";}
		elseif($this->permit->iibs && $stats == 'approved' && $approv == '3'){$stat =  "Approved";}
		elseif($this->permit->iibk && $stats == 'request' && $approv == '2'){$stat =  "Request";}
		elseif($this->permit->iibk && $stats == 'request' && $approv == '3'){$stat =  "Approved";}
		elseif($this->permit->iibk && $stats == 'approved' && $approv == '3'){$stat =  "Approved";}
		elseif($this->permit->ivpib && $stats == 'request' && $approv == '3'){$stat =  "Request";}
		elseif($this->permit->ivpib && $stats == 'approved' && $approv == '3'){$stat =  "Approved";}
		elseif($stats == 'rejected'){$stat =  "Rejected";}

		return $stat;

	}

	function ina_label($stats,$approv){
		if(($this->permit->iibs || $this->permit->ias) && $stats == 'request' && $approv == '1'){$stat = "<span class='label label-warning'>Waiting</span>";}
		elseif(($this->permit->iibs || $this->permit->ias) && $stats == 'request' && $approv == '2'){$stat = "<i class='fa fa-check-circle' style='color:#00a65a'></i>";}
		elseif(($this->permit->iibs || $this->permit->ias) && $stats == 'request' && $approv == '3'){$stat =  "<i class='fa fa-check-circle' style='color:#00a65a'></i><i class='fa fa-check-circle' style='color:#00a65a'></i>";}
		elseif(($this->permit->iibs || $this->permit->ias) && $stats == 'approved' && $approv == '3'){$stat =  "<span class='label label-success'>Approved</span>";}
		elseif($this->permit->iibk && $stats == 'request' && $approv == '2'){$stat =  "<i class='fa fa-check-circle' style='color:#00a65a'></i><span class='label label-warning'>Waiting</span>";}
		elseif($this->permit->iibk && $stats == 'request' && $approv == '3'){$stat =  "<i class='fa fa-check-circle' style='color:#00a65a'></i><i class='fa fa-check-circle' style='color:#00a65a'></i>";}
		elseif($this->permit->iibk && $stats == 'approved' && $approv == '3'){$stat =  "<span class='label label-success'>Approved</span>";}
		elseif($this->permit->ivpib && $stats == 'request' && $approv == '3'){$stat =  "<i class='fa fa-check-circle' style='color:#00a65a'></i><i class='fa fa-check-circle' style='color:#00a65a'></i><span class='label label-warning'>Waiting</span>";}
		elseif($this->permit->ivpib && $stats == 'approved' && $approv == '3'){$stat =  "<span class='label label-success' >Approved</span>";}
		elseif($stats == 'rejected'){$stat =  "<span class='label label-danger'>Rejected</span>";}

		return $stat;

	}

	function create_log($table,$data)
	{
		$this->db->insert($table,$data);
	}

	function search_condition($ss,$st,$sd,$ed){
		if($ss !='all' && empty($st) && empty($sd) && empty($ed)){
			$where = " AND t3.id_subcat = $ss";
		}elseif($ss !='all' && !empty($st) && empty($sd) && empty($ed)){
			$where = " AND t3.id_subcat = $ss AND (t2.vendor_name LIKE '%$st%' OR t2.vendor_num LIKE '%$st%')";
		}elseif($ss !='all' && !empty($st) && !empty($sd) && !empty($ed)){
			$where = " AND t3.id_subcat = $ss AND (t2.vendor_name LIKE '%$st%' OR t2.vendor_num LIKE '%$st%') AND t1.last_updated between '$sd' AND '$ed' ";
		}elseif(!empty($st) && empty($sd) && empty($ed) && $ss =='all'){
			$where = " AND (t2.vendor_name LIKE '%$st%' OR t2.vendor_num LIKE '%$st%')";
		}elseif(!empty($st) && !empty($sd) && !empty($ed) && $ss =='all'){
			$where = " AND (t2.vendor_name LIKE '%$st%' OR t2.vendor_num LIKE '%$st%') AND t1.last_updated between '$sd' AND '$ed'";
		}elseif(!empty($sd) && !empty($ed) && $ss =='all' && empty($st)){
			$where = " AND t1.last_updated between '$sd' AND '$ed'";
		}elseif(!empty($sd) && !empty($ed) && $ss !='all' && empty($st)){
			$where = " AND t1.last_updated between '$sd' AND '$ed' AND t3.id_subcat = $ss";
		}else{}

		return $where;
	}

}

/* End of file m_inactive_vendor.php */
/* Location: ./application/models/m_inactive_vendor.php */