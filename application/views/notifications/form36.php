<?php $this->load->view('notifications/header');?>

<br>Dengan hormat,<br>
<b><?php echo $data_recipients->fullname;?></b><br><br>

Dengan ini kami informasikan bahwa perusahaan Bapak/Ibu telah berhasil mendaftar untuk mengikuti pengadaaan <strong><?php echo ucfirst($data->title_eng);?></strong> di PT. Garuda Indonesia (Persero). Tbk sebagai vendor untuk kategori <?php echo $data->category;?><br><br>
Untuk proses selanjutnya akan kami informasikan melalui email ini.<br><br>
Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br><br>
<br/><br/>
<?php echo $sender;?>.

<?php $this->load->view('notifications/footer');?> 