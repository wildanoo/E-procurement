<div class="col-sm-3 col-md-3 pull-right">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
        <div class="input-group-btn">
            <button class="btn btn-default" id="btn_search" ><i class="glyphicon glyphicon-search"></i></button>
        </div>
    </div>
</div>
<div class="page-header">
  <h3>News</h3>
</div>
<table id="browse" class="table table-striped table-bordered" style="font-size: 13px">
	<thead>
        <tr>
            <th width="10px">No.</th>
            <th width="60px">Code</th>
            <th>Title</th>
            <th>Publish Date</th>
            <th>Type</th>
            <th>Category</th>
            <th>Sub Category</th>
        </tr>
	</thead>
    <tbody>
<?php			
if($browse){ $i=0;
	foreach($browse as $row){ 
        $xcode = md5($row->id.$username);
        $unicode = substr($xcode,5,1) . $xcode . substr($xcode,-15,10);
        ?>	
		<tr>
			<td><?php echo $num = $num+1; ?></td>
			<td>
				<?php echo $row->code; ?>
			</td>
            <td>
                <?php   
                    $title  = '';
                    $title .= $row->title_ind ? $row->title_ind.' <strong>(Indonesia)</strong><br>':'';
                    $title .= $row->title_eng ? $row->title_eng.' <strong>(English)</strong>':'';
                    echo anchor('avl/news_detail/'.$row->id.'/'.$unicode, $title.' ',array('class'=>'default_link')); 
                ?>
            </td>
			<td><?php echo $row->publish_date? date('M d, Y',strtotime($row->publish_date)):'-'; ?></td>
			<td><?php echo array_key_exists($row->subcontent_type,$subcontent)?$subcontent[$row->subcontent_type]:'-'; ?></td>
			<td><?php echo array_key_exists($row->id_cat,$category)?$category[$row->id_cat]:'-'; ?></td>
			<td><?php echo array_key_exists($row->id_subcat,$subcategory)?$subcategory[$row->id_subcat]:'-'; ?></td>
		</tr>
<?php } ?>	
<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?><tr><td colspan="9">-</td></tr><?php } ?>
    </tbody>
</table>
