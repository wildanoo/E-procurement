<html>
<head>
	<title></title>
	<meta name="" content="">
</head>
<body>

Dengan hormat,<br>
<b>Bapak/Ibu <?php echo $to ?></b><br><br>

Bersama ini kami sampaikan bahwa kami mengajukkan untuk Inactive vendor di bawah ini:<br>
<b>Nama Perusahaan : </b><?php echo $vendor_name ?><br>
<b>ID Vendor : </b><?php echo $reg_num ?><br>
<b>Kategori yang terdaftar :</b> <br>
<table cellpadding="2" cellspacing="2" width="100%" border="1">
		<tr>
			<th style="text-align: left;">No</th>
			<th style="text-align: left;">Category</th>	
			<th style="text-align: left;">Sub Category</th>	
		</tr>
		
		<?php if($category){ $i=1;
			foreach($category as $row){ ?>
				<tr>
					<td><?php echo $i ?></td>	
					<td><?php echo $row->category; ?></td>	
					<td><?php echo $row->subcategory; ?></td>		
				</tr>
				<?php	$i++;  } ?>	
				<?php } else { ?>
					<tr><td colspan="8">-</td></tr>
					<?php } ?>	
				</table>
				<br>
<b>Alasan : </b><br>
<?php echo $reason ?><br><br>


Untuk proses persetujuan dapat diakses :<br>
(link web sistem)<br><br>

Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br><br>
(&nbsp;<b><?php echo ucfirst( $this->tank_auth->get_username()); ?></b>&nbsp;)

<br><br>
<b>PT Garuda Indonesia (Persero) Tbk</b> <br>
<i>Business Support / JKTIBGA <br>
Gedung Management Garuda, Lantai Dasar <br>
Garuda City,  Bandara Soekarno Hatta <br>
Cengkareng 19120, PO BOX 1004 TNG BUSH</i>


</body>
</html>