<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class verification extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();		
		$this->load->model(array('m_verify','m_vendor','m_user'));		
		$this->permit = new stdClass();		
		$this->permit = $this->crud->get_permissions("2");
	}

 function index(){ 
	islogged_in();
	if ($this->m_verify->authenticate()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();	
		
		$where   	= array('id_stsreg'=>'6');	
		$session 	= $this->session->userdata('browse'); 	
		if($session) $where = $session;
		
 		$table 	    = "vendor";		
		$page       = $this->uri->segment(3);
		$per_page   = 10;		
		$offset     = $this->crud->set_offset($page,$per_page); 
		$total_rows = $this->crud->get_total_record("",$table,$where); 
		$set_config = array('base_url'=>base_url().'verification/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);		
			 
		$this->load->library('pagination');			 
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();			
			
		$order 			     = array('field'=>'created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;		
		$select          	 = "id,register_num as reg_num,vendor_num,vendor_name,vendor_type,id_stsreg,approval,
								(SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as sts_reg,
							    created_date,last_updated";	

		$browse = $this->crud->browse_with_paging("",$table." l",$field="",$ID="","true",$select,$where,$order,$config['per_page'],$offset,"",$like=""); 
		
		$data['browse']   = $browse;		
		$data['view']	  = "vendor/verification/browse"; 		
 		$this->load->view('layout/template',$data); 
 	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/'); }  		
 }

 function get_tags(){
 	islogged_in();
 	$term     = $_GET['term']; 	
 	$order    = array('field'=>'vendor_name','sort'=>'ASC'); 
 	$order2   = array('field'=>'register_num','sort'=>'ASC'); 
 	$select   = "vendor_name as name";	 
 	$select2  = "register_num as name";	 
 	$vdr_name = $this->crud->autocomplete("","vendor",$select,"vendor_name",$term,array('id_stsreg'=>'6'),$order); 	
 	$reg_num  = $this->crud->autocomplete("","vendor",$select2,"register_num",$term,array('id_stsreg'=>'6'),$order2); 
	
 	if($vdr_name && $reg_num){	$result = array_merge($reg_num,$vdr_name); 	
	} elseif(!$vdr_name && $reg_num){ $result = $reg_num;
	} elseif($vdr_name && !$reg_num){ $result = $vdr_name; }
 	
 	
 	echo json_encode($result);   	 		
 }

 function set_session_browse(){
 	islogged_in();
 	$index      = $_POST['search_term'];  //print_r($_POST);
 	$is_exist   = $this->crud->is_exist("","vendor","id",array('register_num'=>$index));
 	$field      = $is_exist && $index ? "register_num" : "vendor_name"; 	
 	$start_date = $_POST['start_date'];
 	$end_date   = $_POST['end_date'];  	
 	$where      = array('id_stsreg' => '6');
 	
 	if($start_date && !$end_date){			
		$where['created_date >= '] = $start_date;
	} elseif(!$start_date && $end_date){		
		$where['created_date <= '] = $end_date;	
	} elseif($start_date && $end_date){
		$where['created_date >= '] = $start_date;
		$where['created_date <= '] = $end_date;	}
 	
 	if($index) $where[$field]  = $index;	
 					  
 	$this->session->set_userdata('browse',$where); 
 	redirect('verification/');	
 	
 }
 
 function reset_browse(){ 
 	islogged_in();	
 	$this->session->unset_userdata('browse'); 	
 } 

 function set_vendor_session(){ 
 	islogged_in();	 	
 	$id  = $this->uri->segment(3);
 	$src = $this->uri->segment(4)=="true" ? "account_form" : "detail";
 	$this->session->set_userdata("venID",$id);
 	redirect("verification/".$src,"refresh"); 	
 }
 
 function account_form(){
 	islogged_in();
 	if ($this->m_verify->authenticate()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();	
		
 		$venID   = $this->session->userdata("venID");
 				$acc_default	= array('recon_number'	=> null,
									 	'sap_number'	=> null,
									 	'srm_username'	=> null,
									 	'srm_password'	=> "",'status'=>'0'); 
	 	
	 	$account = $this->crud->browse("","vendor_account","id_vendor",$venID,"false");
	 	$data['account'] = !$account ? (object) $acc_default : $account; 	
	 	 	
	 			$none_bank 		= array('acc_number'	=> "none",
									 	'acc_name'		=> "none",
									 	'acc_address'	=> "none",
									 	'acc_country'	=> "none",
									 	'bank_name'		=> "none",
									 	'branch_name'	=> "none"); 	
	 	
	 	$bank              = $this->crud->browse("","bank_account","id_vendor",$venID,"false");
	 	$data['bank']      = !$bank ? (object) $none_bank : $bank;	
	 	$data['venID']     = $venID;			

 		$data['view']	  = "vendor/verification/acc_form"; 		
 		$this->load->view('layout/template',$data); 
 		
 		} else { 
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/'); }  	
 }
 
 function detail(){
 	islogged_in();
 	if ($this->m_verify->authenticate()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();	
		
	 	$venID   = $this->session->userdata("venID");
		$select	 = "vendor_name as name,register_num as reg_num,vendor_type as type,vendor_num as num,vendor_address as address,						
				   (SELECT status FROM register_status WHERE id=id_stsreg) as reg_sts,approval,
				   id_stsreg,web,phone,fax,email,postcode,npwp,npwp_address,npwp_postcode";				   	
	 	$data['vendor']  = $this->crud->browse("","vendor l","id",$venID,"false",$select);
	 	
	 	$none_bank 		= array('acc_number'	=> "none",
								 	'acc_name'		=> "none",
								 	'acc_address'	=> "none",
								 	'bank_name'		=> "none",
								 	'branch_name'	=> "none");
	 	
	 	$bank           = $this->crud->browse("","bank_account","id_vendor",$venID,"false");		
 		$data['bank']   = !$bank ? (object) $none_bank : $bank;	
		$data['view']	  = "vendor/verification/detail"; 		
 		$this->load->view('layout/template',$data); 
 	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/'); }  	
 	
 }

 function create_account(){   
 	islogged_in();	
 	$venID   		= $this->input->post('id_vendor');	 
 	$recon_number   = $this->input->post('recon_number'); 
 	$sap_number     = $this->input->post('sap_number'); 
 	$srm_username   = $this->input->post('srm_username'); 
 	$srm_password   = $this->input->post('srm_password');  
 	$completed      = $this->input->post('completed'); 	
 	$status         = $this->input->post('status');
 	$is_exist       = $this->crud->browse("","vendor_account","id_vendor",$venID,"false");
 	
 	$select    = "register_num as num,vendor_name as name,email";
	$vendor    = $this->crud->browse("","vendor","id",$venID,"false",$select);
	$subject   =  "Notification E-procurement Garuda";
	$select2   = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
			     (SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory,result,note";
	$list      = $this->crud->browse("","vendor_recomend l","id_vendor",$venID,"true",$select2); 	
 	$result    = $this->m_verify->attribute_account($status); 		
 	$recipient = $this->m_user->get_userdata_group($result->to);
 	
 	foreach($recipient as $value){			
		if(is_valid_email($value->email)){	
		
			$user    = $this->users->get_user_by_email($value->email);
			if($user){			 			
				$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'account_number','param'=>$venID);							 
				$link    = $result->form != "EPCGA013" ? $this->m_vendor->create_session_link((object) $require) : "";	
					
				$param   =  array(	'reg_num'		=> $vendor->num,
									'vendor_name'	=> $vendor->name,
									'category'		=> $list,
									'link'			=> $link,
									'to'			=> $value->username);  
									
				$param['view']  = NOTIF_PATH.$result->form; 		
				$message 		= $this->load->view(NOTIF_TMPL,$param,true);				
				$this->crud->sendMail($value->email,$subject,$message); 	}			
			
		}	
	}	
 	
 		
 	$this->m_vendor->register_tracking($venID,$result->msg); 	
 	$data   =  array('id_vendor'     => $venID,
 					  'recon_number' => !$recon_number ? $is_exist->recon_number : $recon_number,
					  'sap_number'   => !$sap_number   ? $is_exist->sap_number   : $sap_number,					  
					  'srm_username' => !$srm_username ? $is_exist->srm_username : $srm_username,
					  'srm_password' => !$srm_password ? $is_exist->srm_password : $srm_password,					  
					  'created_id'   => $this->tank_auth->get_user_id(),
					  'created_date' => date('Y-m-d H:i:s'),
					  'last_updated' => date('Y-m-d H:i:s')); 	//print_r($data);				  	
	
	if($is_exist){
		unset($data['id_vendor']); unset($data['created_id']); unset($data['created_date']);
		$this->crud->update("","vendor_account","id_vendor",$venID,$data); 
	} else { $this->crud->insert("","vendor_account",$data); }	
	
	if($completed=="true"){	
		$update     = array('id_stsreg'=>"7",'last_updated'=>date('Y-m-d H:i:s'));
		$this->crud->update("","vendor","id",$venID,$update); 	}
	
	redirect('verification/');	
 	
 } 

 function check_captcha(){
		$session = $this->session->userdata('captchaword');
		if($this->input->post('captcha') == $session){		return true;
		} else {
			$this->form_validation->set_message('check_captcha','Captcha is wrong');
			return false;
		}
	} 

 function bank_account(){
	$uri3 = $this->uri->segment(3);
	$sessID   = !$uri3 ? $this->session->userdata("sessID") :  $uri3;
	$this->session->set_userdata("sessID",$sessID);
 	$session  = $this->crud->browse("","session_verification","session",$sessID,"false","id_vendor,expired_date");  //print_r($session);
 	$expired  = strtotime($session->expired_date); $now = strtotime(date("Y-m-d")); //echo $expired."|".$now."<br/>";
 	
 	if($now < $expired || $_POST){ 
 		$venID = $session->id_vendor; 	
 		$this->form_validation->set_rules('captcha','Captcha','trim|callback_check_captcha|required');
 		if($this->form_validation->run()==false){
 			$data['id_vendor'] = $venID;
 			$data['img']  = $this->m_vendor->create_captcha();
 			$data['view'] = 'vendor/verification/bank_form';
 			if($this->input->is_ajax_request())
 			{
 				$data = array('msg'=>'false', 'img'=>$data['img'] );
 				echo json_encode($data);
 			}
 			else
 			{
				$this->load->view('layout/template_public',$data);			
 			}
			// $this->load->view('layout/template_public',$data);			
 		} else {  		
 			$venID      = $_POST['id_vendor'];
 			$where      = array('id_vendor'=>$venID);
 			$where2     = array('id'=>$venID,'id_stsreg'=>'5');
 			$is_exist   = $this->crud->is_exist("","bank_account","id",$where); 
 			$insert		= array('id_vendor'		=> $venID,
				 				'acc_number'	=> $_POST['acc_number'],
				 				'acc_name'		=> $_POST['acc_name'],
				 				'acc_address'	=> $_POST['acc_address'],
				 				'bank_name'		=> $_POST['bank_name'],
				 				'branch_name'	=> $_POST['branch_name'],
				 				'acc_country'	=> $_POST['acc_country'],	
				 				'status'		=> '1',							 	
				 				'created_id'	=> $this->tank_auth->get_user_id(),		
				 				'created_date'  => date('Y-m-d H:i:s'),	 	
								'last_updated'	=> date('Y-m-d H:i:s')); //print_r($insert); 
	 		if($is_exist){
	 			unset($data['id_vendor']); unset($data['created_id']); unset($data['created_date']);
	 			$this->crud->update("","bank_account","id_vendor",$venID,$insert); 
	 		} else { 
	 			$this->crud->insert("","bank_account",$insert);	 			
	 			$update = array('status'=>'0','last_updated'=>date('Y-m-d H:i:s'));
	 			$this->crud->update("","bank_account_mail","id_vendor",$venID,$update);
	 			 				 				
	 			$select  = "vendor_num as num,vendor_name as name,email";
				$vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);	 					 				
	 			$adm_src = $this->m_user->get_userdata_group("as");
	 			foreach($adm_src as $value){			
					if(is_valid_email($value->email)){						
							$user    = $this->users->get_user_by_email($value->email);
							if($user){			 			
							$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'bank_account','param'=>$venID);							 
							$link    = $this->m_vendor->create_session_link((object) $require);	
									
							$param   =  array('vendor_num'	=> $vendor->num,
											  'vendor_name' => $vendor->name,
											  'link'		=> $link,											 
											  'to'			=> $value->username);							  
							$param['view']  = NOTIF_PATH."EPCGA008"; 		
							$message 		= $this->load->view(NOTIF_TMPL,$param,true);
							$this->crud->sendMail($value->email,$subject,$message); }
					}	
				}	
	 		}
	 		$this->session->unset_userdata("venID");
	 		echo "Thank You, Verification Success";	 				
 		}

 	} else { echo "<h1>session expired !</h1>";	}

 }
 
 function bank_update(){
 	$venID      = $this->session->userdata("venID");
 	$where      = array('id_vendor'=>$venID);
 	$is_exist   = $this->crud->is_exist("","bank_account","id",$where); 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('id_vendor'		=> $venID,
 		'acc_number'	=> $_POST['acc_number'],
 		'acc_name'		=> $_POST['acc_name'],
 		'acc_address'	=> $_POST['acc_address'],
 		'bank_name'		=> $_POST['bank_name'],
 		'branch_name'	=> $_POST['branch_name'],					 	
 		'created_id'	=> $this->tank_auth->get_user_id(),		
 		'created_date'  => $curr_date,							 					 	
 		'last_updated'	=> $curr_date);	

 	if($is_exist){
 		unset($data['id_vendor']); unset($data['created_id']); unset($data['created_date']);
 		$this->crud->update("","bank_account","id_vendor",$venID,$data); 
 	} else { $this->crud->insert("","bank_account",$data); }		 	
 	
 }

}

