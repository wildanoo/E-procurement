<script>
	$(function(){
		$('.checkall').click(function(){
			$(this).closest('div').find(':checkbox').prop('checked', $(this).prop("checked"));
		});
	});
</script> 

<div class="row">
	<div class="col-md-12">
		<div class="widget widget-nopad">
			<div class="col-md-12">
				<div class="box box-info">

					<div class="box-header with-border">
						<h3 class="box-title">Update Permissions</h3>
					</div><br/>
					<!--form class = "form-horizontal"-->
					<?php echo form_open("user/update_group",array('class'=>'form-horizontal')); ?>
					<?php form_hidden("id",$def->id); ?>
					<table width="80%">
						<tr><td>Group Name </td><td>:</td>
							<td><b><?php echo $def->name; ?></b></td></tr>              	
							<tr><td>Permissions </td><td>:</td>
								<td width="70%">				 
									<!-- <div class="container"> -->
									<div class="panel-group" id="accordion">
				    <!-- div class="panel panel-primary">
				      <div class="panel-heading">Registration Vendor</div>
				      <div class="panel-body">             	<?php 
              			 echo form_hidden("id",$def->id);
              			 echo form_hidden("ttl",count($perm));
              			 $dbchecked = explode(",",$def->permissions);
              			 $i = 1;
                         foreach($perm as $row){
						 	$value = $row->value.":".$row->id;
						 	$checked = in_array($value,$dbchecked) ? true: false ;
						 	$function = $row->id_function;
						 	if ($function == 1){
						 	echo form_checkbox('check[]'.$i,$value, $checked); echo $row->name."<br/>";}
						$i++; }  ?></div>
				    </div>
				
				    <div class="panel panel-primary">
				      <div class="panel-heading">Approval Vendor</div>
				      <div class="panel-body"><?php 
              			 echo form_hidden("id",$def->id);
              			 echo form_hidden("ttl",count($perm));
              			 $dbchecked = explode(",",$def->permissions);
              			 $i = 1;
                         foreach($perm as $row){
						 	$value = $row->value.":".$row->id;
						 	$checked = in_array($value,$dbchecked) ? true: false ;
						 	$function = $row->id_function;
						 	if ($function == 2){
						 	echo form_checkbox('check'.$i,$value, $checked); echo $row->name."<br/>";}
						$i++; }  ?></div>
				    </div>
				
				    <div class="panel panel-primary">
				      <div class="panel-heading">Content Management</div>
				      <div class="panel-body"><?php 
              			 echo form_hidden("id",$def->id);
              			 echo form_hidden("ttl",count($perm));
              			 $dbchecked = explode(",",$def->permissions);
              			 $i = 1;
                         foreach($perm as $row){
						 	$value = $row->value.":".$row->id;
						 	$checked = in_array($value,$dbchecked) ? true: false ;
						 	$function = $row->id_function;
						 	if ($function == 3){
						 	echo form_checkbox('check'.$i,$value, $checked); echo $row->name."<br/>";}
						$i++; }  ?></div>
				    </div>
				
				    <div class="panel panel-primary">
				      <div class="panel-heading">Bidding Management</div>
				      <div class="panel-body"><?php 
              			 echo form_hidden("id",$def->id);
              			 echo form_hidden("ttl",count($perm));
              			 $dbchecked = explode(",",$def->permissions);
              			 $i = 1;
                         foreach($perm as $row){
						 	$value = $row->value.":".$row->id;
						 	$checked = in_array($value,$dbchecked) ? true: false ;
						 	$function = $row->id_function;
						 	if ($function == 4){
						 	echo form_checkbox('check'.$i,$value, $checked); echo $row->name."<br/>";}
						$i++; }  ?></div>
				    </div>
				    
				        <div class="panel panel-primary">
				      <div class="panel-heading">Sourcing Management</div>
				      <div class="panel-body"><?php 
              			 echo form_hidden("id",$def->id);
              			 echo form_hidden("ttl",count($perm));
              			 $dbchecked = explode(",",$def->permissions);
              			 $i = 1;
                         foreach($perm as $row){
						 	$value = $row->value.":".$row->id;
						 	$checked = in_array($value,$dbchecked) ? true: false ;
						 	$function = $row->id_function;
						 	if ($function == 5){
						 	echo form_checkbox('check'.$i,$value, $checked); echo $row->name."<br/>";}
						$i++; }  ?></div>
					</div> -->
					<?php 
					$n=1;
					foreach($perm as $rs=>$key) { 
						foreach($key as $name=>$val) { ?>
							<div class="panel panel-primary" style="">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $n ?>"><?php echo $name; ?></a>
									</h4>
								</div>
								<?php //echo $perm; ?>
								<div id="collapse<?php echo $n ?>" class="panel-colapse collapse">
									<div class="panel-body">              	
										<?php 
										echo form_hidden("id",$def->id);
										echo form_hidden("ttl",count($perm));
										$i = 1;
										$idc = "class='checkall'";
										echo form_checkbox('checkall','accept',false,$idc); echo "Check All <br><br>";
										foreach($val as $row){
											$value = $row["value"].":".$row["id"];
						 	//$function = $row["id_function"];
						 	//if ($function == 1){
											echo form_checkbox('check[]',$value, $row["checked"], false); echo $row["name"]."<br/>";}
											$i++; }  //} ?></div>
										</div>
									</div>
									<?php $n++;} ?>

								</div>
								<!-- </div>      -->
							</td></tr>
						</table>
					</div><!-- col-md-6 -->                
				</div><!-- /.box-body -->
				<div class="box-footer" style="padding-left: 30px;">
					<?php  $prop = array('class'=>'btn btn-sm btn-default','title'=>'Cancel'); ?>					 
					<?php  echo anchor('user/groups','Cancel',$prop); ?>
					<?php  echo form_submit('submit', 'Update','class="btn btn-sm btn-primary"'); ?>
				</div><!-- /.box-footer -->
				<?php echo form_close(); ?>
			</div><!-- /.box -->
		</div>
	</div>
