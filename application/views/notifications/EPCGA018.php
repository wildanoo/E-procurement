
Dengan hormat,<br/>
<b>Bapak/Ibu <?php echo $to; ?></b><br/>


Bersama ini kami sampaikan terkait dengan kelalaian <?=$vendor_name?> dengan rincian sebagai berikut.
<br/>
(Reason)
<br/>
<?=$reason?>
<br/><br/>
Maka menindaklanjuti hal tersebut, mengacu pada kebijakan vendor yang berlaku
di PT Garuda Indonesia (persero) Tbk (Refer http://e-proc.garuda-indonesia.com
pada bagian vendor policy poin III), bahwa <?=$vendor_name?> telah melakukan
pelanggaran dan dikenakan sanksi berupa tidak diperbolehkan mengikuti 2 kali
proses pengadaaan pada kategori dimana rekanan terdaftar.
<br/>
Kami harapkan hal ini tidak terulang dan dapat menjadi perbaikan di kemudian hari.

<br/><br/>
Untuk detail informasi dapat diakses :<br/>
<?php echo $link; ?><br/>


Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/><br/>

(&nbsp;<b><?php echo ucfirst( $this->tank_auth->get_username()); ?></b>&nbsp;)

