<div id="container12">
<script type="text/javascript">
 $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>"; 
   
   $('#btn_recmd12').hide(); 
   $('input[type=checkbox]').attr('disabled', 'true');
   
   $('#type').attr('disabled', true);	
   
   $('#btn_change12').click(function(){ 
   		$('#btn_recmd12').show(); 
   		$('#btn_change12').hide();
   		
   		$("#note12").css("background","#ffffff");
	    $("#note12").css("color","#827e85");             
	    $("#note12").prop('readonly',false); 
	    
	    $('#type').attr('disabled', false);
	    $('input[type=checkbox]').attr('disabled', false);
   
   });
        
   $('#btn_cancel12').click(function(){ 
   		 $("#container12").load(baseUrl+'vendor/load_recomendation');
   });   
   
   $('#btn_approved12').click(function(){ 
   		if (confirm('Are you sure you want to approve vendor ?')) { 
   			var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
			var token     = '<?php echo $this->security->get_csrf_hash(); ?>';				
			$.post( baseUrl+ "vendor/approved", { csrf: token } )
				  .done(function( resp ) {
				    document.location.href= baseUrl + 'register/';  //$("#debug12").html(resp);
			});			  
		}
    });  
    
   $('#btn_rejected12').click(function(){ 
   		 $("#container12").load(baseUrl+'vendor/form_rejected');
     });

    $('#btn_recmd12').click(function(){ 
   		var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token  = '<?php echo $this->security->get_csrf_hash(); ?>';		
		var values = $("#form_recmd").serializeArray();
		values.push({name: csrf ,value: token });
		values = jQuery.param(values);
		
   		$.post( baseUrl+ "vendor/validate_recmd", values ).done(function( resp ) {   			
		   	if(resp=="true"){ $("#form_recmd").submit();
		   	} else { $('#msg12').html('Failed !, Decide status due diligence <br/> and Submit with complete recommendation');	}		   		
		});
    });      
	   
 });
</script>

<div id="debug12"></div>
<?php echo form_open('due_diligence/add_recommend',array('id'=>'form_recmd')); 
	  $feature  = $this->session->userdata("feature"); ?>

<table width="70%">
<tr>
	<td><label>Category / Sub Category</label></td><td>:</td>
	<td>
		<?php
			 $venID  = $this->session->userdata("venID"); 		   
			 if($categories){ $i=1;
			 	echo form_hidden('ttl',count($categories));
			 	foreach($categories as $row){				 	
				 	$where      = array('id_vendor'=>$venID,'id_subcat'=>$row->id_subcat);
				 	$is_exist   = $this->crud->is_exist("","vendor_recomend","id",$where);
			 	 	$checked    = !$is_exist ? false : true ;	 	
				 	echo form_checkbox('opt'.$i, $row->id, $checked);
				 	echo $row->category." / ".$row->subcategory."<br/>";	
		 $i++;}} ?>
	</td>
</tr>
<tr>
	<td><label>Recommendation</label></td><td>:</td>
	<td><?php 
		$options = array('0'=>'-- Select --','avl'=>'AVL','shortlist'=>'Shortlist','reject'=>'Reject');
		$prop = 'class="form-control" id="type" style="width:220px"';
		echo form_dropdown('type',$options,$recmd->type,$prop); ?></td>
	<td id="msg1">&nbsp;</td>	
</tr>
<tr>
	<td style="vertical-align: top;"><label>Note</label></td><td style="vertical-align: top;">:</td>
	<td><textarea name="note" id="note12" cols="20" rows="5" placeholder="Note"  class="form-control" required readonly><?php echo $recmd->note; ?></textarea></td>
</tr>

</table>
 <div style="text-align: left; color: red;" id="msg12"></div>
<div style="text-align: right;">
	   
		<?php if($this->permit->rvs && $vendor->approval == "0") {  ?>
		<input type="button" id="btn_cancel12" value="Cancel" class="btn btn-default btn-sm"/>
		<input type="button" id="btn_change12" value="Edit" class="btn btn-primary btn-sm" style="width: 80px;"/>
		<input type="button" id="btn_recmd12" value="Submit" class="btn btn-primary btn-sm"/><?php } ?>
		<?php if($this->permit->baes) {  ?>
		<div class="btn-group">
			<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				Generate Berita Acara <span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li>
					<a target="_blank" href="<?=base_url('berita_acara/download_batch_berita_acara')?>">With Attachment</a>
				</li>
				<li>
					<a target="_blank" href="<?=base_url('berita_acara/generate_evaluation')?>">Without Attachment</a>
				</li>
			</ul>
		</div>
		<?php } ?>
		<?php if($vendor->approval == $user_level && $user_level!=0 && $feature=="register") {  ?>
		<input type="button" id="btn_approved12" value="Approve" class="btn btn-success btn-sm"/>
		<input type="button" id="btn_rejected12" value="Reject" class="btn btn-danger btn-sm"/><?php } ?>	
</div>

<br/>
<?php echo  form_close(); ?>



<?php //$this->load->view('vendor/detail/notes',$data); ?>

</div>


