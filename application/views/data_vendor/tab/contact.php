<div id="container3">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>"; 
   
   $('#btn_create3').click(function(){ 
   		$("#container3").load(baseUrl+'data_vendor/form_createContact');   		
    }); 
   
 });  
 
  function form_update(id){
  	   var baseUrl  = "<?php echo base_url(); ?>";
  	   $("#container3").load(baseUrl+'data_vendor/form_updateContact/'+id);
  } 

  function delete_contact(id){
 	var baseUrl = "<?php echo base_url(); ?>";
 	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
 	
 	if (confirm('Are you sure you want to delete contact?')) {	
		$.post( baseUrl + "data_vendor/delete_contact", { csrf: token, id: id } );
		$("#container3").load(baseUrl+'data_vendor/load_contact', function(){$('#cmsg3').html('Data akan berubah setelah proses approval disetujui')});		
	}	
 }
  
</script>

<div id="cmsg3" class="bg-warning text-center text-danger"></div>
<br>

<div style="text-align: right">
 
 <?php if($this->permit->upven){ ?><button type="button" id="btn_create3" class="btn btn-info btn-primary">Add Contact</button><?php } ?>

</div>

<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Name</th>
		<th>Position</th>
		<th>Mobile</th>
		<th>Email</th>
		<?php  if($this->permit->upven){ ?><th>Action</th><?php } ?>
	</tr>
	</thead>
	<tbody>

 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->fullname; ?></td>	
			<td><?php echo $row->position; ?></td>
			<td><?php echo $row->mobile; ?></td>
			<td><?php echo $row->email; ?></td>
			<?php  if($this->permit->upven){ ?>
			<td style="text-align: center;">
			<?php	echo '<a href="javascript:void(0)"><i class="fa fa-pencil" onclick="form_update('.$row->id.')"></i></a> |'; 			
					echo '<a href="javascript:void(0)"><i class="fa fa-trash"  onclick="delete_contact('.$row->id.')"></i></a>';	
			?></td>	
			<?php } ?>								
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="6">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>