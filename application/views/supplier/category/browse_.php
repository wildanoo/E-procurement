<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 
$(document).ready(function(){	 
	 $(function(){  });
		var baseUrl = "<?php echo base_url(); ?>";

		$('#init_code').change(function(){ 
			var init_code = $('#init_code').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'category/set_sess_category',			   
               data : 'field=init_code&category='+init_code,
               success: function(data){ 
                   //alert(category);	
					 //$('#debug').html(resp);
					 //dialogRef.close();				 	
				 	document.location.href= baseUrl + 'category/';
			   }		   
            });	
		
		});

		$('#category2').change(function(){ 
			var category = $('#category2').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'category/set_sess_category',			   
               data : 'field=id&category='+category,
               success: function(data){ 
                   //alert(category);	
					 //$('#debug').html(resp);
					 //dialogRef.close();				 	
				 	document.location.href= baseUrl + 'category/';
			   }		   
            });	
		
		});

		

		$("#srch-term").autocomplete({
			source: baseUrl + 'category/get_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
	        var search = $("#srch-term").val(); // alert(search);
	        var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
	        $.ajax({
	           url  : baseUrl + 'category/set_sess_search',
	           type : "POST",              		               
	           data : csrf +'='+ token +'&search='+search,
	           success: function(resp){    $("#debug").html(resp);            		            
				    document.location.href= baseUrl + 'category/';
	           }
	        });
	    });
	    	 
	 $('#btn_create').click(function(){ 
	 	  form_create();
	 	  //alert('test');
	 });	
});

	function form_create(){
		var baseUrl = "<?php echo base_url(); ?>";		 
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		BootstrapDialog.show({
		title: 'Create Category',
        message: $('<div></div>').load(baseUrl + "category/form_create/"),
        buttons: [{ label: 'Submit',
	                                                   cssClass: 'btn btn-primary btn-sm',		               
	                action: function(dialogRef){
	                	 var category  = $("#category").val(); 
	                	 //alert(category);
	                	 $.post( baseUrl + 'category/is_exist', { csrf: token , category : category})
						 .done(function( resp ) {
								 //$('#debug').html(resp);
								 //dialogRef.close(); 
								var json = $.parseJSON(resp);
								if (json.status=='true'){
				                	var values = $("#form").serializeArray();
				                	values.push({ name: csrf,value: token });
									values = jQuery.param(values);
									 $.ajax({
							               url  : baseUrl + 'category/create',
							               type : "POST",              		               
							               data : values,
							               success: function(resp){
							               		document.location.href= baseUrl + 'category/';	               				               		  
									       }
							            });   
								} else { 	
								 $("#msg").html(json.msg); 
							}
	               		 });  
	                }
	            }, {
	            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
			                action: function(dialogRef) {
			                	document.location.href= baseUrl + 'category/';
			                    dialogRef.close(); 
			                }
		        }]
    });
}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Category',
            message: $('<div></div>').load(baseUrl + "category/form_update/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef)
		                {
		                	 var category  = $("#category").val(); 
		                	 
		                	 $.post( baseUrl + 'category/is_exist', { csrf: token , category : category})
							 .done(function( resp ) {
//									 $('#debug').html(resp);
//									 dialogRef.close(); 
									var json = $.parseJSON(resp);
									if (json.status=='true'){
					                	var values = $("#form").serializeArray();
					                	values.push({ name: csrf,value: token });
										values = jQuery.param(values);
										 $.ajax({
								               url  : baseUrl + 'category/update',
								               type : "POST",              		               
								               data : values,
								               success: function(resp){
								               		document.location.href= baseUrl + 'category/';	               				               		  
										       }
								            });   
									} else { 	
									 $("#msg").html(json.msg); 
								}
		               		 });  
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'category/';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}	
	
</script>
<script src="<?php echo base_url(); ?>assets/js/sorttable.js"></script>
</head>

<body>
 <div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Category</h3>
              </div><!-- /.box-header -->

                <br>
                    <?php if($this->session->flashdata('msg_warning')!=null) { ?>
                	<div class="alert alert-warning">
  						<strong>Warning!</strong> <?php echo $this->session->flashdata('msg_warning'); ?>
					</div>
					<?php } ?>
                <div class="col-md-2 pull-left">
                    <input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Category" value="New Category" style="width:130px;" title="Create New Category"/>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
				<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
                </div>

                <div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('category/search',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>category" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		        </div>
		        </div>   
     
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover text-center sortable">
                    <thead>
                      <tr>
                        <!--th><input type="checkbox" name="cba[]" id="cba" /></th-->
                        <th class="sorttable_sorted_reverse">No</th>
                        <th class="">Code</th>
                        <th class="">Category</th>
                        <th class="">Created Date</th>
                        <th class="">Updated Date</th>
                        <th class="">Updated By</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    
                    <tbody>
<?php
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td><?php echo $num = $num+1;; ?></td>
				<td><?php echo $row->init_code; ?></td>
				<td><?php echo $row->category; ?></td>
				<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>	
				<td><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>
				<td align="center"><?php echo $row->created_id; ?></td>	
				<td><?php				
					echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete category ?')){ return true; } else {return false; }";
					echo anchor('category/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Subcategory','onclick'=>$js));?>
				</td>
				
			</tr>
<?php	$i++;  } ?>	
</table>
<tr><div colspan="9" style="text-align: center;"><?php echo $pagination;?></div></tr>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>  
	</table>  
                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->