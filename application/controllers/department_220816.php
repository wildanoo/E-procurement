<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Department extends Controller {

	function __construct()
	{
		parent::__construct();			
		$this->load->model('m_user');
		islogged_in();
		$this->m_user->authenticate(array(41));
	}	
		
	function index(){
		//if ($this->tank_auth->is_logged_in()) {

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			
			//$sess_like  = $this->session->flashdata('like');
			
			$sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
				
			
			$field      = $sess_field != '' ? $sess_field : "dept_name";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
			
			if($sess_cat != ''){	$where = array('dept_name'=>$sess_cat);
			} else { $where = "";	}
			
	 		$table 	    = "departemen";		
			$page       = $this->uri->segment(3);
			$per_page   = 5;		
			$offset     = $this->crud->set_offset($page,$per_page);
			$total_rows = $this->crud->get_total_record("",$table);
			$set_config = array('base_url'=> base_url().'/department/index','total_rows'=>$total_rows,'per_page'=>5,'uri_segment'=>3);
			$config     = $this->crud->set_config($set_config);		
				 
			$this->load->library('pagination');			 
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();			
				
			$order 			     = array('field'=>'created_date','order'=>'DESC');
			$data['pagination']  = $paging;
			$data['num']         = $offset; 
			$select 			 = "id,(SELECT office_name FROM offices WHERE id=id_office)as id_office,dept_code,dept_name,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
			
			//if($sess_like){
			//$like   = array('col'=>'dept_name','field'=>$sess_like);
			//$browse = $this->crud->browse("",$table." l","","","true",$select,"","","",$like);	} else {
			//$browse = $this->crud->browse_with_paging("",$table." l","","","true",$select,"",$order,$config['per_page'],$offset); }
			
			//$data['browse'] = $browse;
			
			$sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			$data['browse'] = $browse;
			
			$departemen = $this->crud->browse("","departemen","","","true","id,dept_name,dept_code");
			if(!$departemen) $departemen = array();
			$options = array();
			$select = array(''=>'All');
			foreach($departemen as $val){ $options[$val->id] = $val->dept_name; }
			$data['departemen'] = $select + $options;
			
	 		$data['view']	= "department/browse"; 		
 			$this->load->view('layout/template',$data);
 		
	 		
// 		} else {  
// 			$this->session->set_flashdata('message','user not authorized'); 
// 			redirect('/auth/login/'); 	 }
	}

	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
	
		$search_conditions = array(
				'dept_code'		=> $_POST['search_term'],
				'dept_name'		=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		// 		$data['category'] 		 = $this->getCategories();
		// 		$data['subcategory'] 	 = $this->getSubcategories();
		// 		$data['subcontent_type'] = $this->getSubcontentTypes();
		// 		$data['status_ref'] 	 = $this->getStatusCondition();
	
		$data['view'] = "department/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{	
		/* get data for table view from database with pagination */
	
		$table 	     = "departemen t1";
		$select 	 = "t1.id,dept_code,dept_name,remark,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'department/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
		/* get data for table view from database with pagination */
	
		$table 	     = "departemen t1";
		$select 	 = "t1.id,(SELECT office_name FROM offices WHERE id=id_office)as id_office,dept_code,dept_name,remark,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,$where,$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'department/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	function get_tags(){ 	
	 	$term   = $_GET['term'];  	
	 	$order  = array('field'=>'dept_name','sort'=>'ASC'); $select = "dept_name as name";
	 	$result = $this->crud->autocomplete("","departemen",$select,"dept_name",$term,"",$order);  	
	 	echo json_encode($result);   	 		
	}
	
	function set_sess_search(){
		$like = $_POST['search']; 	
	 	$this->session->set_flashdata('like',$like); 
	}
	
	function set_sess_departemen(){
	
		$type = $_POST['dept_name'];
		if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
		} else { $this->session->unset_userdata('sess_cat'); }
	
		$type = $_POST['field'];
		if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
		} else { $this->session->unset_userdata('sess_field'); }
	
	}
	
	function is_exist(){
		$id_office    = $_POST['id_office'];
		$dept_code	  = $_POST['dept_code'];
		$dept_name    = $_POST['dept_name'];	
		$is_exist['1']= !$id_office ? "false" : "true";	
		$is_exist['2']= !$dept_code ? "false" : "true";
		$is_exist['3']= !$dept_name ? "false" : "true";		
		$msg['1']	  = !$id_office ? "office name required" : "";
		$msg['2']	  = !$dept_code ? "department code required" : "";
		$msg['3']	  = !$dept_name ? "department name required" : "";
		
		if ($is_exist['1']=='true' && $is_exist['2']=='true' && $is_exist['3']=='true'){
			$where   = array('dept_code'=>$dept_code, 'dept_name'=>$dept_name, 'id_office'=>$id_office);
			$checked = $this->crud->is_exist("","departemen","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$is_exist['2']    = !$checked ? "true" : "false";
			$is_exist['3']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate office name" : "";
			$msg['2']	= $checked ? "duplicate department code" : "";
			$msg['3']	= $checked ? "duplicate department name" : "";
				
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2'], 'msg3' =>$msg['3']);
	
		echo json_encode($result);
	}
	
	function create(){

		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array('id'=>null,'id_office'=>$_POST['id_office'],'dept_code'=>$_POST['dept_code'],'dept_name'=>$_POST['dept_name'],
					  'created_id'=>$userID,'created_date'=>$curr_date,'last_updated'=>$curr_date);
		
// 		$check_dept_code = $this->crud->browse("","departemen","id","","false","dept_code");
// 		foreach($check_dept_code as $val){};
// 		$data['check'] = $val;
		$where = array('dept_code'=>$_POST['dept_code']);
		$checked = $this->crud->is_exist("","departemen","id",$where);
		//print_r($checked);exit;
		if ($checked == 'true'){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		} else {
				$id = $this->crud->insert("","departemen",$data);
			}
		$this->session->set_flashdata('message','1 data success insert');
	}
	
	function form_create(){
		//$data['data'] = $this->crud->browse("","departemen","","","true","id,dept_code,dept_name");
		$office = $this->crud->browse("","offices","","","true","id,office_name");
		//print_r($office);
		if(!$office) $office = array();
		$select = array(''=>'-- Select --');
		foreach($office as $val){ $options[$val->id] = $val->office_name; }
		$data['office'] = $select + $options;
		
 		$this->load->view('department/form_create',$data);
	}
	
	function update(){
		$curr_date =  date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data = array('id_office'=>$_POST['id_office'],'dept_code'=>$_POST['dept_code'],'dept_name'=>$_POST['dept_name'],'created_id'=>$userID,'last_updated'=>$curr_date);
		$this->crud->update("","departemen","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
		redirect('department/','refresh');
	}
	
	function form_update(){
		$id = $this->uri->segment(3);
		$select 			 = "id,(SELECT id FROM offices WHERE id=id_office) as id_office,dept_code,dept_name,remark,created_id,created_date,last_updated";
		$def = $this->crud->browse("","departemen l","id",$id,"false",$select);
		$data['def'] = $def ;
		$office = $this->crud->browse("","offices","","","true","id,office_name");
		//print_r($data['def']);
		if(!$office) $office = array();
		$select = array(''=>'-- Select --');
		foreach($office as $val){ $options[$val->id] = $val->office_name; }
		$data['office'] = $select + $options;
		$id = $this->uri->segment(3);
		$data['data'] = $this->crud->browse("","departemen","id",$id,"false");
 		$this->load->view('department/form_update',$data);
 		
	}
	
	function delete_(){
	
		$id = $this->uri->segment(3);
		$this->crud->delete("","departemen","id",$id);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('department/','refresh');
	
	}
	
	function delete(){
	
		$id = $this->uri->segment(3);
	
		$where = array('id_dept'=>$id);
		$checked1 = $this->crud->is_exist("","profiles","id",$where);
		if ($checked1){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		} else {
			$this->crud->delete("","departemen","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
	
		redirect('department/','refresh');
	}
}