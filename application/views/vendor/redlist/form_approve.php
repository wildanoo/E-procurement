

<div id="#main_container">


<div class="page-header">
  <h3>Vendor Redlist</h3>
</div>
<div id="debug"></div>
<?php echo form_open_multipart('redlist/approve',array('id'=>'form', 'class'=>'form-horizontal','role'=>'form')); ?>
  <input type="hidden" name="red_id" value="<?php echo (isset($redlist->id_redlist))?$redlist->id_redlist:''; ?>">
  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Select Vendor</label>
    <div class="col-sm-8">
      <select readonly name="ven_id" id="ven_id" class="form-control js-example-basic-single" required="required">

        <?php
        if(isset($redlist->id_redlist))
        {
          echo "<option value='$redlist->id'>$redlist->vendor_name</option>";
        }
        else
        {
            echo "<option value=''>--Select Vendor--</option>";
          foreach ($vendor as $ven)
          {
            echo "<option value='$ven->id'>$ven->vendor_name</option>";
          }
        }

        ?>
      </select>
    </div>
  </div>

  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Start Date</label>
    <div class="col-sm-8">
      <input type="text" readonly value="<?php echo (isset($redlist->start_date))?$redlist->start_date:''; ?>" required name="start_date" id="sd" class="form-control" placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Counter</label>
    <div class="col-sm-8">
      <input style="border:none;" readonly type="text" value="<?php echo (isset($redlist->counter))?$redlist->counter: 2 ; ?>" required name="counter" class="form-control"  placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Reason</label>
    <div class="col-sm-8">
      <textarea readonly required name="remark" id="remark" class="form-control" rows="3"><?php echo (isset($redlist->remark))?$redlist->remark:''; ?></textarea>
    </div>
  </div>

  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Attachment</label>
    <div class="col-sm-8">
     <?php
        foreach ($redlist_docs as $key => $value) 
        {
          echo '<div>'.$value->filename.'</div>';
        }
     ?>
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <a type="button" href="javascript:void(0)" onclick="load_data()" class="btn btn-warning">Back</a>
      <button type="submit" class="btn btn-primary">Approve</button>
    </div>
  </div>
<?php echo form_close(); ?>

</div>


<script type="text/javascript">
    $(document).ready(function() {
      //CKEDITOR.replace( 'editor1' );
    });
</script>
