<div id="container11">
<?php $doc = $this->session->flashdata('doc');   ?>
<?php //print_r($data); ?>

<script type="text/javascript">
$(function () {  
	var baseUrl     = "<?php echo base_url(); ?>";
	var sts_visit   = '<?php echo $visit; ?>'; 
	
	$("input[name=visit][value='"+ sts_visit +"']").prop("checked",true);
	
	if(sts_visit==1) var disabled = 2;
	else if(sts_visit==2) var disabled = 1;
	
	$("input[name=visit][value='"+ disabled +"']").prop( "disabled", true );
	
	if(sts_visit!=0){ 
		$("#btn_confirm").hide();
		//$("#btn_confirm").addClass("btn btn-default btn-sm");
		//$("#btn_confirm").prop( "disabled", true ); 
   }
	
	

	$("#content1").load(baseUrl+'due_diligence/is_visit/');	
	
	$('#btn_rejected').click(function(){ 
	   		
	   	$("#container11").load(baseUrl+'vendor/form_rejected');   		
	});		
		
	$('#btn_approved').click(function(){ 
	   		if (confirm('Are you sure you want to approved vendor ?')) {
		   		$.post( baseUrl + "vendor/approved", function( resp ) {			  		
				  		document.location.href= baseUrl + 'vendor/';
				});
			}				   		
	});			
	
	$("input:radio[name=visit]").click(function () {
		var opt = $("input:radio[name=visit]:checked").val(); //alert(opt);
		if (opt == '2'){
			$("#content1").load(baseUrl+'due_diligence/non_visit_form');	 	
		} else {	
			$("#content1").load(baseUrl+'due_diligence/visit_browse'); }
    });

	$('#btn_confirm').click(function(){ 
	   		if (confirm('Are you sure you want to confirm status ?')) {
		   		
				var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
			    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	    
			    var values   = $("#form_confirm").serializeArray();
				values.push({name: csrf,value: token});
				
				$.post( baseUrl + "due_diligence/confirm_status_visit", values ).done(function( resp ) {
				   document.location.href= baseUrl + 'register/detail/';
				});
				
			}				   		
	});	

}); 
</script>

<div id="debug11"></div>

<?php echo form_open('#',array('id'=>'form_confirm')); ?>
	
Whether to do visit ? &nbsp;&nbsp;
<input type="radio" name="visit" value="1"> Yes &nbsp;&nbsp;
<input type="radio" name="visit" value="2"> No <br/>
<button type="button" id="btn_confirm" class="btn btn-primary btn-sm" style="width: 100px;">Confirm</button>
<?php echo form_close(); ?>
<hr />

<?php 
$user_group  = $this->session->userdata('user_group');
$user_level  = $this->m_vendor->get_user_level($user_group);

if($this->permit->approved && ($vendor->approval==$user_level)){ ?>
Recommendation : &nbsp;
<?php 
	$venID  = $this->session->userdata("venID");	
	$result = $this->crud->browse("","vendor_recomend","id_vendor",$venID,"false","result")->result;	
	echo strtoupper($result);
 ?>&nbsp;&nbsp;
 
<button type="button" id="btn_approved" class="btn btn-success btn-sm">Approved</button>
<button type="button" id="btn_rejected" class="btn btn-warning btn-sm">Reject</button>

<?php }  ?>

<div id="content1">

<?php //$this->load->view('due_diligence/'.$view); ?>
	
</div>




</div>