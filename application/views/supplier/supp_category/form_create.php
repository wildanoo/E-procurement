
<script type="text/javascript"> 	
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";		 
	  $(function(){  });
	  
	  $('#id_cat').change(function(){  
   		var id_cat = $('#id_cat').val(); 
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
           $.ajax({
               url  : baseUrl + 'category/get_subcategory',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_cat='+id_cat,
               success: function(resp){
               	  $('#id_subcat').html(resp);
               }
            });
     });
});	  
</script>

<?php echo form_open('#',array('id'=>'form')); ?>
<table cellpadding="5" cellspacing="5" > 	
<tr>
	<td><label>Category</label></td><td>:</td>
	<td><?php
		$prop_category = 'class="form-control" id="id_cat" style="width:220px"';
	 	echo form_dropdown("id_cat",$category,'default',$prop_category); ?>
	 </td>
	 <td id="msg1"></td>		
</tr>
<tr>
	<td><label>Sub Category</label></td><td>:</td>
	<td>
		<select name="id_subcat" id="id_subcat" class="form-control" style="width:220px">
			<option value="">--Select Category--</option> 		
		</select>	
	</td>
	<td id="msg2"></td>	
</tr>
</table>