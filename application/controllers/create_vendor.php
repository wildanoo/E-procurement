<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Create_vendor extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model(array('m_create_vendor','codec','m_vendor'));

		$this->permit 			= $this->crud->get_permissions("19");
	}

	public function index()
	{
		if($this->m_create_vendor->authenticate()){
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			$select     = "id,id_cat,(SELECT category FROM category WHERE id=id_cat) as category,subcategory";
			$order      = array('field'=>'id_cat','sort'=>'ASC');
			$db_subcat  = $this->crud->browse("","subcategory","","","true",$select,$where="",$order);
			$db_subcat  = $this->crud->grouping_objdata($db_subcat,"category");

			$data['category'] =  $db_subcat;

			$data['view'] 	= "vendor/createvendor/create";
			$this->load->view('layout/template',$data);
		}else{
			$this->session->set_flashdata('message','user not authorized');
			redirect('/auth/login/');
		}
	}

	function create(){
		$curr_date 	 = date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$footer		 = array('created_id'=>$userID,'created_date'=>$curr_date,'last_updated'=>$curr_date);	
		$regNum      	= $this->codec->register(6);
		$post 			= array(
			'register_num' 		=> $regNum,//
			'id_stsreg'			=> 1,
			'vendor_type'		=> "new",//
			'vendor_num'		=> "",//
			'vendor_name'		=> !$_POST['vendor_name']		? "" : $_POST['vendor_name'],//
			'vendor_address'	=> !$_POST['vendor_address']	? "" : $_POST['vendor_address'],//postcode ga ada
			'phone'				=> !$_POST['phone'] 			? "" : $_POST['phone'],//
			'fax'				=> !$_POST['fax'] 				? "" : $_POST['fax'],//
			'email'				=> !$_POST['email'] 			? "" : $_POST['email'],//
			'overseas'			=> $_POST['overseas'],
			'npwp'				=> !$_POST['npwp'] 				? "" : $_POST['npwp'],	
			'status'			=> "active"
			);

		$data  = $footer + $post;
		$id_vendor 	= $this->crud->insert("","vendor",$data);

		$con_person = array(
			'id_vendor'		=> $id_vendor,
			'fullname'		=> $_POST['cp_name'],
			'mobile'		=> $_POST['cp_mobile'],
			'email'			=> $_POST['cp_email'],
			'status'		=> 1
			);

		$data2 = $con_person + $footer;
		$this->crud->insert("","contact_person",$data2);

		$subcat = $_POST['id_subcat'];
		foreach($subcat as $sub){
			$cat_id 	= $this->crud->browse("","subcategory","id",$sub,"false","id_cat")->id_cat;
			$data_cat 	= array(
				'id_vendor'		=> $id_vendor,
				'id_category'	=> $cat_id,
				'id_subcat'		=> $sub,
				'status'		=> 0
				);

			$data3 = $footer + $data_cat;
			$this->crud->insert("","vendor_category",$data3);
		}

		$session   = uniqid();				 	  
		$register  = array('id_vendor'		=> $id_vendor,
			'session'		=> $session,
			'status'			=> 1,				 	 
			'created_date'	=> date('Y-m-d H:i:s'),
			'last_updated'	=> date('Y-m-d H:i:s'));							 	  
		$this->crud->insert("","vendor_register",$register);

		$this->m_vendor->register_tracking($id_vendor,'Registration');

		redirect('register');

	}

}

/* End of file create_vendor.php */
/* Location: ./application/controllers/create_vendor.php */