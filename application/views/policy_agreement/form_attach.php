<div class="page-header">
  <h3>New Vendor Policy</h3>
</div>
<div id="debug"></div>
<?php echo form_open_multipart('policy_agreement/add_file',array('id'=>'form_upload')); //$current_date = date('Y-m-d'); ?>
<table cellpadding="2" cellspacing="2">
<tr>
	<td><label>Type</label></td><td>:</td><?php echo form_hidden("id_type",$this->uri->segment(3)); ?>
	<td><?php //$type = $this->uri->segment(3); ?><?php //if ($type==4) echo "Direct Selection"; else if ($type==3) echo "Open Bidding"; else if ($type==2) echo "Open Sourcing"; else if ($type==1) echo "General"; ?>
	<?php echo $def->type; ?>
	</td>
	<!-- td><input type="text" name="deskripsi" id="deskripsi" placeholder="" class="form-control" class="required" required/></td> -->
	<td id="msg"></td>	
</tr>
<tr>
	<td><label>Deskripsi</label></td><td>:</td>
	<td><input type="text" name="deskripsi" id="deskripsi" placeholder="" class="form-control" class="required" required/></td>	
	<td id="msg"></td>	
</tr>
<tr>
	<td><label>Keterangan</label></td><td>:</td>
	<td><input type="text" name="keterangan" id="keterangan" placeholder="" class="form-control" class="required" required/></td>	
	<td id="msg"></td>	
</tr>
<tr> 
	<td><?php	
		$data  = array('type'=>'file','name'=> 'pdffile','id' => 'file_upload','value'=>'true','content'=>'Import'); 
		echo form_input($data); ?> <i>*.pdf</i>
	</td>
</tr>
<tr>
<td><input type="submit" value="Submit" class="btn btn-primary btn-sm" />
<input type="hidden" id="id" name="id" value="<?php echo $this->uri->segment(3); ?>"></input>
<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>'Cancel'); ?>
<?php  echo anchor('policy_agreement/form_update/'.$this->uri->segment(3),'Cancel',$prop); ?></td>
</tr>
</table>
<?php form_close(); ?>