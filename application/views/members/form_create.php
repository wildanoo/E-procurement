
<script type="text/javascript"> 	
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";		 
	  $(function(){  });
	  
	  $('#id_country').change(function(){  
   		var id_country = $('#id_country').val(); 
   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
           $.ajax({
               url  : baseUrl + 'member/get_office',
               type : "POST",              		               
               data : csrf +'='+ token +'&id_country='+id_country,
               success: function(resp){
               	  $('#id_office').html(resp);
               }
            });
     });
});	  
</script>

<?php echo form_open('member/create',array('id'=>'form')); ?>
<table cellpadding="2" cellspacing="2">
<tr>
	<td><label>First Name</label></td><td>:</td>
	<td><input type="text" name="firstname" id="firstname" class="form-control" placeholder="first name" required/></td><td id="msg1"></td>	
</tr>
<tr>
	<td><label>Last Name</label></td><td>:</td>
	<td><input type="text" name="lastname" id="lastname" class="form-control" placeholder="last name" required/></td><td id="msg2"></td>	
</tr>
<tr>
	<td><label>Country</label></td><td>:</td>
	<td><?php 
		$prop_country = 'class="form-control" id="id_country" style="width:220px"';
		echo form_dropdown('id_country',$country,'default',$prop_country); ?>			
	</td><td id="msg3"></td>	
</tr>
<tr>
	<td><label>Office</label></td><td>:</td>
	<td>
		<select name="id_office" id="id_office" class="form-control" style="width:220px">
			<option value="">--Select Country--</option> 		
		</select>		
	</td><td id="msg4"></td>		
</tr>
<tr>
	<td><label>Departemen</label></td><td>:</td>
	<td><?php 
		$prop_dept = 'class="form-control" id="id_dept" style="width:220px"';
		echo form_dropdown('id_dept',$dept,'default',$prop_dept); ?>			
	</td><td id="msg5"></td>	
</tr>
<tr>
	<td><label>Level</label></td><td>:</td>
	<td><?php 
		$prop_level = 'class="form-control" id="id_level" style="width:220px"';
		echo form_dropdown('id_level',$level,'default',$prop_level); ?>			
	</td><td id="msg6"></td>	
</tr>
<tr>
	<td><label>Email</label></td><td>:</td>
	<td><input type="email" name="email" id="email" class="form-control" placeholder="Email" required/></td>
	<td id="msg7"></td>	
</tr>
<tr>
	<td><label>Username</label></td><td>:</td>
	<td><input type="text" name="username" id="username" class="form-control" placeholder="user name" required/></td>
	<td id="msg8"></td>	
</tr>
<tr>
	<td><label>Password</label></td><td>:</td>
	<td><input type="password" name="password" id="password" class="form-control" placeholder="password" required/></td>
	<td id="msg9"></td>	
</tr>
<tr>
	<td><label>Confirm Password</label></td><td>:</td>
	<td><input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="confirm password" required/></td>
	<td id="msg10"></td>	
</tr>
</table>