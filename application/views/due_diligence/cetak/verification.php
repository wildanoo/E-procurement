<?php
//url logo
if(base_url() == '/')
{
	$base = "http://$_SERVER[HTTP_HOST]";
	$url_logo = $base.'/assets/imagesv2/logo.png';
}
else
{
	$url_logo = base_url('assets/imagesv2/logo.png');
}

//page 1
$content .= '<page>';
$content .= '
<p style="text-align:Center;font-weight:bold;">
<img src="'.$url_logo.'" style="margin:0 auto;" >
<br><br>
BERITA ACARA <br/>
VERIFICATION DATA REGISTRATION <br/>
PT. GARUDA INDONESIA (PERSERO) Tbk <br/>
</p>
<hr>
<br>';
$content .= '
<h4>GENERAL INFORMATION</h4>

<table border="2" style="border-collapse:collapse;">
<tr>
<td style="width:150px;">Vendor type </td>
<td style="width:400px;">'.ucfirst($vendor->type).'</td>
</tr>
<tr>
<td>Name </td>
<td>'.ucwords($vendor->name).'</td>
</tr>
<tr>
<td>Address </td>
<td>'.ucwords($vendor->address).'</td>
</tr>
<tr>
<td>Post Code </td>
<td>'.ucwords($vendor->postcode).'</td>
</tr>
<tr>
<td>NPWP </td>
<td>'.ucwords($vendor->npwp).'</td>
</tr>
<tr>
<td>NPWP Address </td>
<td>'.ucwords($vendor->npwp_address).'</td>

</tr>
<tr>
<td>NPWP Post Code </td>
<td>'.ucwords($vendor->npwp_postcode).'</td>
</tr>
<tr>
<td>Phone </td>
<td>'.ucwords($vendor->phone).'</td>
</tr>
<tr>
<td>Fax </td>
<td>'.ucwords($vendor->fax).'</td>
</tr>
<tr>
<td>Email </td>
<td>'.strtolower($vendor->email).'</td>
</tr>
<tr>
<td>Web </td>
<td>'.ucwords($vendor->web).'</td>
</tr>

</table>';

$content .= '
<h4>BANK INFORMATION</h4>

<table border="2" style="border-collapse:collapse;">
<tr>
<td style="width:150px;"><label>Account Holder</label></td>
<td style="width:400px;">'.ucfirst($bank->acc_name).'</td>
</tr>
<tr>
<td><label>Account No</label></td>
<td>'.ucwords($bank->acc_number).'</td>
</tr>
<tr>
<td><label>Bank Name</label></td>
<td>'.ucwords($bank->bank_name).'</td>
</tr>
<tr>
<td><label>Country</label></td>
<td>'.ucwords($bank->acc_country).'</td>
</tr>
<tr>
<td><label>Branch Name</label></td>
<td>'.ucwords($bank->branch_name).'</td>
</tr>
</table>
';


$content .= '</page>';


//load library html2pdf
require_once APPPATH.'./libraries/html2pdf/html2pdf.class.php';
//config pdf
$html2pdf = new HTML2PDF('P','A4','en', true, 'UTF-8', array(30, 10, 30, 10));
$namefile = 'berita_acara_'.$vendor->name.'_'.$vendor_visit->visit_date;
$html2pdf->pdf->SetTitle("{$namefile}");

$html2pdf->WriteHTML($content);

$html2pdf->Output("$namefile.pdf");




 ?>
