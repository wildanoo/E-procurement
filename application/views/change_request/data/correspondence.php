<div id="corresp">

<div id="corresp_msg" class="text-center text-danger bg-warning"></div>	

<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Document Name</th>
		<th>Document Number</th>
		<th>Description</th>
		<th>Effective Date</th>
		<th>Expired Date</th>
		<th>Remark</th>
		<th>Approval</th>
		<th>Status</th>
		<th>#</th>
	</tr>
	</thead>
	<tbody>

 <?php  
 if($correspondence){ $i=1;
 	foreach($correspondence as $row){ ?>
 		<tr>
 			<td><?= $i; ?></td>
			<td><?= $row->doc_name; ?></td>	
			<td><?= $row->doc_num; ?></td>
			<td><?= $row->desc; ?></td>
			<td><?= $row->eff_date; ?></td>
			<td><?= $row->exp_date; ?></td>
			<td><?= $row->remark ?></td>
			<td><?= $row->approval ?></td>
			<td><span class="label label-warning"><?= $stat = ($row->status == '0') ? 'request':'approved'; ?></span></td>
			<td style="text-align: center;">			
				<?php 
				echo '<button onclick="approve_corresp([\''.$row->id.'\',\''.$row->approval.'\',\''.$row->id_tbl.'\'])" type="button" class="btn btn-default"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>';
				echo '<button onclick="reject_temp_table('.$row->id.',\'temp_correspondence\')" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>';
				?>
			</td>	
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="10">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>