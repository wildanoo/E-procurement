<div id="container4">

<script type="text/javascript">
 $(function () {
   	var baseUrl  = "<?php echo base_url(); ?>";
   	$('#update4').hide();
  	
   	$('#change4').click(function(){	
   	
   		$("#acc_number").css("background","#ffffff");
	    $("#acc_number").css("color","#827e85");             
	    $("#acc_number").prop('readonly',false);
	    
	    $("#acc_name").css("background","#ffffff");
	    $("#acc_name").css("color","#827e85");             
	    $("#acc_name").prop('readonly',false);
	    
	    $("#acc_address").css("background","#ffffff");
	    $("#acc_address").css("color","#827e85");             
	    $("#acc_address").prop('readonly',false);
	    
	    $("#bank_name").css("background","#ffffff");
	    $("#bank_name").css("color","#827e85");             
	    $("#bank_name").prop('readonly',false);
	    
	    
	    $("#branch_name").css("background","#ffffff");
	    $("#branch_name").css("color","#827e85");             
	    $("#branch_name").prop('readonly',false);	    
   	
   		$('#update4').show();
   		$('#change4').hide();
   		
   	});
   	
   	$('#update4').click(function(){	  	
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
   	    var values = $("#form4").serializeArray();
		values.push({name: csrf,value: token});
		values = jQuery.param(values); 
			$.ajax({
               url  : baseUrl + 'vendor/bank_update',
               type : "POST",              		               
               data : values,
               success: function(resp){
               	  $("#container4").load(baseUrl+'vendor/load_bank_account');
               }
            });
			
   		$('#update4').hide();
   		$('#change4').show();
   	});

    $('#verify').click(function(){
    	 if (confirm('Are you sure verify this vendor ?')) {    	 	
    	 	$.post( baseUrl + "vendor/is_bankaccount_filled", function( resp ) { 		  			
			   	if(resp=="true"){ 
			   		$.post( baseUrl + "vendor/verification_to_avl");
			   		document.location.href= baseUrl + 'register/';
			   	} else { $('#msg4').html('Canceled !, Bank ccount not completed');	}		   		
			});
		 }
    });
    
    $('#resend_email').click(function(){
    	 if (confirm('Are you sure re-send email verification ?')) {
	    	$.post( baseUrl + "vendor/verification", function( resp ) {		  	
			  	document.location.href= baseUrl + 'register/detail';
			});
		 }
    });
	 	   
 });
 
 </script>
 
<div id="debug2"></div>
<div id="msg4" style="color: red;"></div>
<?php echo form_open('#',array('id'=>'form4'));  ?>
<table width="100%">
<tr>
<td><label>Account Number</label></td><td>:</td>
<td>	
	<?php $acc_number = array( 'name' => "acc_number" ,
								'id'   => "acc_number", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> ucfirst($bank->acc_number)); 
	 echo form_input($acc_number);	?>
</td>
</tr>
<tr>
<td><label>Account Name</label></td><td>:</td>
<td>
	<?php $acc_name = array( 'name' => "acc_name" ,
							'id'   => "acc_name", 
							'class'=> "form-control", 
							'readonly'=> "true", 
							'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> ucfirst($bank->acc_name)); 
	 echo form_input($acc_name);	?>
</td>
</tr>
<tr>
<td><label>Account Address</label></td><td>:</td>
<td>
	<?php $acc_address = array( 'name' => "acc_address" ,
							'id'   => "acc_address", 
							'class'=> "form-control", 
							'readonly'=> "true", 
							'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> ucfirst($bank->acc_address)); 
	 echo form_input($acc_address);	?>	
</td>
</tr>
<tr>
<td><label>Bank Name</label></td><td>:</td>
<td>
	<?php $bank_name = array( 'name' => "bank_name" ,
							'id'   => "bank_name", 
							'class'=> "form-control", 
							'readonly'=> "true", 
							'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> ucfirst($bank->bank_name)); 
	 echo form_input($bank_name);	?>	
</td>
</tr>
<tr>
<td><label>Branch Name</label></td><td>:</td>
<td><?php $branch_name = array( 'name' => "branch_name" ,
							'id'   => "branch_name", 
							'class'=> "form-control", 
							'readonly'=> "true", 
							'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
							'value'=> ucfirst($bank->branch_name)); 
	 echo form_input($branch_name);	?></td>
</tr>
</table>

<div style="text-align: right;">
	<?php if($this->global->active){ ?>
	<button type="button" id="change4" class="btn btn-info btn-primary">Edit</button> 
    <button type="button" id="update4" class="btn btn-info btn-primary">Update</button>
    <?php } ?>
    <?php if($this->global->register=="5" && $this->permit->vba){ ?>
    <button type="button" id="resend_email" class="btn btn-primary">Re-send Email</button>
    <button type="button" id="verify" class="btn btn-warning">Verification</button><?php } ?>
</div>
<?php echo  form_close(); ?>

</div>