<script type="text/javascript">
	$(function () { 
		var baseUrl  = "<?php echo base_url(); ?>";   

		$('#btn_cancel').click(function(){ 
			var id_visit = $('#id_visit').val();
			$("#container11").load(baseUrl+'due_diligence/visit_docs/' + id_visit);
		}); 

		$("form#form_doc_dd").submit(function(){

			var formData = new FormData($(this)[0]);
			var id 		 = $('#id_visit').val();
			$.ajax({
				url: baseUrl+'due_diligence/add_doc_duedill',
				type: 'POST',
				data: formData,
				async: false,
				success: function (data) {
					$("#container11").load(baseUrl+'due_diligence/visit_docs/'+id);
				},
				cache: false,
				contentType: false,
				processData: false
			});

			/*return false;*/
		});
		
	});

</script>
<div id="debug"></div>
<?php echo form_open_multipart('#',array('id'=>'form_doc_dd', 'onsubmit'=>'return false'));
$csrf = array(
        'name' => $this->security->get_csrf_token_name(),
        'hash' => $this->security->get_csrf_hash()
); ?>
<input type="hidden" name="id_visit" id="id_visit" value="<?php echo $id_visit; ?>" />
<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>">
<table width="50%">
	<tr>
		<td><label>Document Name</label></td><td>:</td>
		<td><input type="text" name="doc_name" id="doc_name" placeholder="Document Name" class="form-control" required/></td>
		<td id="msg1">&nbsp;</td>	
	</tr>
	<tr>
		<td><label>Remark</label></td><td>:</td>
		<td><input type="text" name="remark" id="remark" placeholder="Remark" class="form-control" required/></td>
	</tr>
	<tr>
		<td><label>Attachment</label></td><td>:</td>
		<td><?php	
			$style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";
			$data  = array('type'=>'file','name'=> 'userfile','id' => 'file_upload','value'=>'true','content'=>'Import','style'=>$style); 
			echo form_input($data);  ?>			
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="button" id="btn_cancel" value="Cancel" class="btn btn-default btn-sm"/>
			<?php if($this->permit->md3){ ?><input id="sub-btn" type="submit"  value="Submit" class="btn btn-primary btn-sm"/><?php } ?>		
		</td>	
	</tr>
</table><br/><br/>
<?php echo  form_close(); ?>


