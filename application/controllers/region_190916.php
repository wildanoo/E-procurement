<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Region extends Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_user');		
		islogged_in();		
		$this->m_user->authenticate(array(41));
	}	
		
	function index() {
		//if ($this->permit->mc ) {
				
			$data ['user_id'] = $this->tank_auth->get_user_id ();
			$data ['username'] = $this->tank_auth->get_username ();
			
			//$sess_like  = $this->session->flashdata('like');
               
            $sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
			
				
			$field      = $sess_field != '' ? $sess_field : "region_name";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
				
			if($sess_cat != ''){	$where = array('region_name'=>$sess_cat,'status'=>'1');
			} else { $where = array('status'=>'1');	}   
            
			$table = "region";
			$page = $this->uri->segment ( 3 );
			$per_page = 10;
			$offset = $this->crud->set_offset ( $page, $per_page, $where );
			$total_rows = $this->crud->get_total_record ("",$table,$where);
			$set_config = array (
					'base_url' => base_url () . '/region/index',
					'total_rows' => $total_rows,
					'per_page' => $per_page,
					'uri_segment' => 3
			);
			$config = $this->crud->set_config ( $set_config );
				
			$this->load->library ( 'pagination' );
			$this->pagination->initialize ( $config );
			$paging = $this->pagination->create_links ();
				
			$order = array (
					'field' => 'created_date',
					'order' => 'DESC'
			);
			$data ['pagination'] = $paging;
			$data ['num'] = $offset;
			$select = "id,(SELECT country_name FROM country WHERE id=id_country) as country_name,region_name,remark,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
				
			//if($sess_like){
			//$like   = array('col'=>'region_name','field'=>$sess_like);
			//$browse = $this->crud->browse("",$table." l","","","true",$select,"","","",$like);	} else {
			//$browse = $this->crud->browse_with_paging("",$table." l","","","true",$select,"",$order,$config['per_page'],$offset); }			
					
			//$data['browse'] = $browse;
            $sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
				
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			$data['browse'] = $browse;

			$region = $this->crud->browse("","region","","","true","id,region_name");
			if(!$region) $region = array();
			$select = array(''=>'All');
			foreach($region as $val){ $options[$val->id] = $val->region_name; }
			$data['region'] = $select + $options;
            
	 		$data['view']	= "region/browse"; 		
 			$this->load->view('layout/template',$data);
 			
// 		} else {
// 			$this->session->set_flashdata('message','user not authorized');
// 			redirect ( '/auth/login/' );
// 		}
	}
	
	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
	
		//$search_dateranges = array('publish_date',$_POST['start_date'],$_POST['end_date']);
		$search_conditions = array(
				'country_name'		=> $_POST['search_term'],
				'region_name'		=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		// 		$data['category'] 		 = $this->getCategories();
		// 		$data['subcategory'] 	 = $this->getSubcategories();
		// 		$data['subcontent_type'] = $this->getSubcontentTypes();
		// 		$data['status_ref'] 	 = $this->getStatusCondition();
		
	
		$data['view'] = "region/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{
		// 		$subcontent_allowed = globalNeedAanwidzing();
	
		// 		$sess_cat = $this->session->userdata('sess_cat');
		// 		$sessID   = $this->session->flashdata('anID');
			
		// 		$field    = $sess_cat ? "id_cat" : "";
		// 		$id_cat   = $sess_cat ? $sess_cat  : "";
	
		// 		if($sess_cat){
		// 			$where = "t1.subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'";
		// 		} else {
		// 			$where = !$this->session->userdata('is_admin') ? "subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'":"";
		// 		}
	
		/* get data for table view from database with pagination */
	
		$table 	     = "region t1";
		$select 	 = "t1.id,(SELECT country_name FROM country WHERE id=id_country) as id_country,t1.region_name,t1.remark,t1.status,t1.created_id,t1.created_date,t1.last_updated";
		//$select      = "t1.id,(SELECT country_name FROM country WHERE id=id_country) as t1.country_name,region_name,t1.remark,(SELECT username FROM users WHERE id=created_id)as t1.created_id,t1.created_date,t1.last_updated";
		//$order 		 = array('field'=>'t1.publish_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		// if($sessID)	$getData = $this->crud->browse("",$table,"id",$sessID,"true",$select,$where);
		// else  {
				$joins[0][0] = 'country t2';
				$joins[0][1] = 't2.id = t1.id_country';
				$joins[0][2] = 'left';
				
				//SELECT t1.id_country, t2.country_name FROM region t1 left join country t2 on t1.id_country = t2.id
	
		// $where		 = !$this->session->userdata('is_admin') ? "t1.subcontent_type IN ('4','5') AND t1.status = '3'":"";
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'region/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
		// 		$subcontent_allowed = globalNeedAanwidzing();
	
		// 		$sess_cat = $this->session->userdata('sess_cat');
		// 		$sessID   = $this->session->flashdata('anID');
			
		// 		$field    = $sess_cat ? "id_cat" : "";
		// 		$id_cat   = $sess_cat ? $sess_cat  : "";
	
		// 		if($sess_cat){
		// 			$where = "t1.subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'";
		// 		} else {
		// 			$where = !$this->session->userdata('is_admin') ? "subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'":"";
		// 		}
		$joins[0][0] = 'country t2';
		$joins[0][1] = 't2.id = t1.id_country';
		$joins[0][2] = 'left';
		/* get data for table view from database with pagination */
	
		$table 	     = "region t1";
		$select 	 = "t1.id,(SELECT country_name FROM country WHERE id=id_country) as country_name,t1.region_name,t1.remark,t1.status,(SELECT username FROM users WHERE id=t1.created_id) as created_id,t1.created_date,t1.last_updated";
		//$select      = "t1.id,(SELECT country_name FROM country WHERE id=id_country) as t1.country_name,region_name,t1.remark,(SELECT username FROM users WHERE id=created_id)as t1.created_id,t1.created_date,t1.last_updated";
		//$order 		 = array('field'=>'t1.publish_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse_join('',$table,"COUNT(t1.id) AS COUNT",$joins,$where,$dateranges,$wherearray);
	
		// 		$joins[0][0] = 'announcement_level_approval t2';
		// 		$joins[0][1] = 't2.id_announcement = t1.id';
		// 		$joins[0][2] = 'left';
		// 		$joins[1][0] = 'level t3';
		// 		$joins[1][1] = 't2.id_level = t3.id';
		// 		$joins[1][2] = 'left';
		
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,$where,$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'region/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
		return $data;
	
	}
	
	function get_region_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'region_name','sort'=>'ASC'); $select = "region_name as name";
		$result = $this->crud->autocomplete("","region",$select,"region_name",$term,"",$order);
		echo json_encode($result);
	}
	
	function set_sess_search(){
		$like = $_POST['search'];
		$this->session->set_flashdata('like',$like);
	}
    
    function set_sess_region(){
	
		$type = $_POST['region_name'];
		if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
		} else { $this->session->unset_userdata('sess_cat'); }
	
		$type = $_POST['field'];
		if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
		} else { $this->session->unset_userdata('sess_field'); }
	
	}
	
	function is_exist(){
		$country	  = $_POST['id_country'];
		$region       = $_POST['region_name'];
		$is_exist['1']= !$country ? "false" : "true";
		$is_exist['2']= !$region ? "false" : "true";		
		$msg['1']	  = !$country ? "country name required" : "";
		$msg['2']	  = !$region ? "region name required" : "";
		
		if ($is_exist['1']=='true' && $is_exist['2']=='true'){
			$where   = array('region_name'=>$region, 'id_country'=>$country,'status'=>'1');
			$checked = $this->crud->is_exist("","region","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$is_exist['2']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate country name" : "";
			$msg['2']	= $checked ? "duplicate region name" : "";
				
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2']);
	
		echo json_encode($result);
	}
	
	function form_create(){	
		$country = $this->crud->browse("","country","","","true","id,country_name");
		if(!$country) $country = array();
		$select = array(''=>'-- Select --');
		foreach($country as $val){ $options[$val->id] = $val->country_name; }
		$data['country'] = $select + $options;
	
		$this->load->view('region/form_create',$data);
	
	}
	
	function create(){
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array(
				'id' => null,
				'id_country' => $_POST ['id_country'],
				'region_name' => $_POST['region_name'],
				'created_id' => $userID,
				'created_date' => $curr_date,
				'last_updated' => $curr_date
		);
		$where   = array('region_name'=>$_POST['region_name'],'status'=>'0');
		$checked = $this->crud->is_exist("","region","id",$where);
	
		if ($checked){ 
			$id = $this->crud->browse("","region","region_name",$_POST['region_name'],"false","id")->id;
			$update = array('status'=>'1','last_updated'=>$curr_date);
			$this->crud->update("","region","id",$id,$update);
		} else {	$id = $this->crud->insert("","region",$data);	}
		$this->session->set_flashdata('message','1 data success insert');
	}
	
	function form_update(){
		$id = $this->uri->segment(3);
		$select 			 = "id,(SELECT id FROM country WHERE id=id_country) as id_country,region_name,remark,created_id,created_date,last_updated";
		$def = $this->crud->browse("","region l","id",$id,"false",$select);
		$data['def'] = $def ;
		$country = $this->crud->browse("","country","","","true","id,country_name");
		//print_r($data['def']);
		if(!$country) $country = array();
		$select = array(''=>'-- Select --');
		foreach($country as $val){ $options[$val->id] = $val->country_name; }
		$data['country'] = $select + $options;
		$this->load->view('region/form_update',$data);
			
	}
	
	function update_(){		
		$curr_date 	= date('Y-m-d H:i:s');
		$id   = $this->uri->segment(3);
		$data = array('id_country'  =>$_POST['id_country'],
					  'region_name' =>$_POST['region_name'],
				 	  'remark' 		=>$_POST['remark'],
					  'last_updated'=> $curr_date);
		$select = "(SELECT id FROM country WHERE id=id_country) as id_country,region_name,remark";
		$check  = $this->crud->browse("","region","id",$id,"false",$select);
		//print_r($check);
		if (($check->id_country==$_POST['id_country']) && ($check->region==$_POST['region']) && ($check->remark==$_POST['remark'])){
			//echo "berhasil";exit;
			$this->crud->update("","region","id",$_POST['id'],$data);
			$this->session->set_flashdata('message','1 data success update');
		}

	}
	
	function update(){
		$curr_date 	= date('Y-m-d H:i:s');
		$id   = $this->uri->segment(3);
		$data = array('id_country'  =>$_POST['id_country'],
				'region_name' =>$_POST['region_name'],
				'status' 		=>'1',
				'last_updated'=> $curr_date);
		$this->crud->update("","region","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
	}
	
	function delete(){
	
		$id = $this->uri->segment(3);
		$status = array('status'=>'0');
		$this->crud->update("","region","id",$id,$status);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('region/','refresh');
	}
	
	function delete_(){
	
		$id = $this->uri->segment(3);
		
		$where = array('id_region'=>$id);
		$checked1 = $this->crud->is_exist("","offices","id",$where);
		//$checked2 = $this->crud->is_exist("","vendor","id",$where);
		
		//if ($checked1 || $checked2){
		if ($checked1){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		} else {
			$this->crud->delete("","region","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
		
// 		$data = array('status'=>'0');
// 		$this->crud->update("","region","id",$id,$data);
		redirect('region/','refresh');
	}
	
}
?>