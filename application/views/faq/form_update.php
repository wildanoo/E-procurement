 
 
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">

              <div class="box-header with-border">
                <h3 class="box-title">Update Content FAQ</h3>
              </div><br/>
              <!--form class = "form-horizontal"-->
              <?php echo form_open("faq/update",array('class'=>'form-horizontal')); ?>
              <?php echo form_hidden('id',$def->id); ?>
              <table width="50%">
              <tr>
				<ul class="nav nav-tabs test2">
				    <li class="active"><a data-toggle="tab" href="#idn" aria-expanded="true">Indonesia</a></li>
				    <li class=""><a data-toggle="tab" href="#eng" aria-expanded="false">English</a></li>
				</ul>

				<div class="tab-content">
				    <div id="idn" class="tab-pane fade active in" style="padding:15px;">
				    <input type="text" name="question_ind" id="question_ind" value="<?php  echo $def->question_ind; ?>" class="form-control" required/>
				      <?php $data['content'] = $contents[0]; $this->load->view('announce/note',$data); ?>
				    </div>
				    
				    <div id="eng" class="tab-pane fade" style="padding:15px;">
				    <input type="text" name="question_eng" id="question_eng" value="<?php  echo $def->question_eng; ?>" class="form-control" required/>
				      <?php $data['content'] = $contents[1]; $this->load->view('announce/note',$data); ?>
				    </div>
				</div>
			</tr>
              </table>
                  </div><!-- col-md-6 -->                
                </div><!-- /.box-body -->
                <div class="box-footer" style="padding-left: 30px;">
                    <?php  $prop = array('class'=>'btn btn-sm btn-default','title'=>'Cancel'); ?>					 
					<?php  echo anchor('faq/','Cancel',$prop); ?>
                    <?php  echo form_submit('submit', 'Update','class="btn btn-sm btn-primary"'); ?>
                </div><!-- /.box-footer -->
              <?php echo form_close(); ?>
            </div><!-- /.box -->
          </div>
        </div>
      