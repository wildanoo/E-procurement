<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>"; 
   
   $('#btn_approved').hide(); 
   
   $('#type2').attr('disabled', true);	
   
   $('#btn_mchange').click(function(){ 
   		$('#btn_approved').show(); 
   		$('#btn_mchange').hide();
   		
   		$("#note2").css("background","#ffffff");
	    $("#note2").css("color","#827e85");             
	    $("#note2").prop('readonly',false); 
	    
	    $('#type2').attr('disabled', false);
   
   });
        
   $('#btn_mcancel').click(function(){ 
   		 $("#tabs_container").load(baseUrl+'vendor/load_main_tabs');
     });     
	   
   });

</script>
<h2>Vendor Approved</h2>
<div id="debug15"></div>
<?php echo form_open('vendor/add_approved',array('id'=>'form_approved')); $current_date = date('Y-m-d'); ?>
<input type="hidden" name="id" id="id" value="<?php echo $recmd->id;  ?>"/>
<input type="hidden" name="level" id="level" value="<?php echo $level;  ?>"/>
<table width="50%">
<tr>
	<td><label>Recommendation</label></td><td>:</td>
	<td><?php 
		$options = array('0'=>'-- Select --','avl'=>'AVL','shortlist'=>'Shortlist','reject'=>'Reject');
		$prop = 'class="form-control" id="type2" style="width:220px"';
		echo form_dropdown('type2',$options,$recmd->type,$prop); ?></td>
	<td id="msg1">&nbsp;</td>	
</tr>
<tr>
	<td style="vertical-align: top;"><label>Note</label></td><td style="vertical-align: top;">:</td>
	<td><textarea name="note2" id="note2" cols="20" rows="5" placeholder="Note"  class="form-control" required readonly><?php echo $recmd->note; ?></textarea></td>
</tr>
<tr>
	<td colspan="3">
		
		<input type="button" id="btn_mcancel" value="Cancel" class="btn btn-default btn-sm"/>
		<?php if($approved){ ?>	
			<input type="button" id="btn_mchange" value="Change" class="btn btn-primary btn-sm"/>
			<input type="submit" id="btn_approved" value="Submit" class="btn btn-primary btn-sm"/>
		<?php } ?>		
	</td>	
</tr>
</table><br/><br/>
<?php echo  form_close(); ?>


