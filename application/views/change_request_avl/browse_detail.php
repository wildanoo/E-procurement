<script>
	$(function() {
      var baseUrl = "<?=base_url()?>";
	    $('#ck_all').on('click', function(){
			     $(this).closest('table').find(':checkbox').prop('checked', this.checked);
		  });
      $('#myform').submit(function(event) {
        var sdata = $(this).serialize();
        var surl = $(this).attr('action');
        $.post(surl, sdata, function(data, textStatus, xhr) {
          
          $.get(baseUrl+'change_request_avl/send_approveMail', function(xdata, textStatus, xhr) { load_data(); });

        });
      });
	});

   function reject_all()
  {
        var sdata = $('#myform').serialize();
        var surl = baseUrl+'change_request_avl/reject_all';
        $.post(surl, sdata, function(data, textStatus, xhr) {
          
          $.get(baseUrl+'change_request_avl/send_rejectMail', function(xdata, textStatus, xhr) { load_data(); });
          
        });
  }

	function load_data()
	{
    var id = <?=$this->input->get('id')?>;
		$('#main_container').load(baseUrl+'change_request_avl/detail/?id='+id);
	}

</script>
<div id="main_container">

<div id="debug"></div>
<div class="page-header"><h3>Vendor <?php echo $title; ?></h3></div>

<div class="panel panel-default">
<div class="panel-heading"></div>
  <div class="panel-body">
     <div class="form-group">
      <label for="inputEmail3" class="col-xs-6 control-label">Supplier Name</label>
      <div class="col-xs-6">
        <label for="">: <?=$vendor->name?></label>
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail3" class="col-xs-6 control-label">Registration Code</label>
      <div class="col-xs-6">
        <label for="">: <?=$vendor->reg_num?></label>
      </div>
    </div>
  </div>
</div>
<a href="<?=base_url('change_request_avl')?>" type="button" class="btn btn-default"> <span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span> back to list</a>
<?php echo form_open('change_request_avl/approve_all',array('id'=>'myform','onsubmit'=>'return false')); ?>

<button class="btn btn-primary pull-right" type="submit">Approve</button>
<button onclick="reject_all()" class="btn btn-danger pull-right" type="button">Reject</button>

<?php echo $this->session->flashdata("message"); ?>
<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
        <tr>
        	<th><input type="checkbox" id="ck_all" value="1"></th>
            <th>No</th>
            <th>Changes Name</th>
            <th>Action</th>
        </tr>
	</thead>
    <tbody>
<?php
// echo form_hidden('total',count($browse));
if($browse){ $i=1;
	foreach($browse as $row){ $label = ($row->status == 'request') ? 'warning':'success'; ?>
			<tr>
        <td> <input type="checkbox" class="ck_ex" name="my_ck[]" id="ck<?=$i?>" value="<?=$row->sess_name?>"></td>
				<td><?=$num = $num+1; ?></td>
        <td><?= '<b>Perubahan Data '.$row->sess_name.'</b>' ?></td>
				<td> <a href="<?=base_url('change_request_avl/more_detail/?id=').$row->sess_name?>" type="button" class="btn btn-default">view detail <span class="glyphicon glyphicon-search" aria-hidden="true"></span></a> </td>

			</tr>
<?php	$i++;  } ?>
<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php  } else { ?>
	<tr><td colspan="9" align="center">-</td></tr>
<?php } ?>
    </tbody>
</table>

<?php echo form_close();?>






