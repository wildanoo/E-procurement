<div id="aff">
<div id="aff_msg" class="text-center text-danger bg-warning"></div>

<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Company Name</th>
		<th>Competency</th>
		<th>Contact</th>
		<th>Remark</th>
		<th>Approval</th>
		<th>Status</th>
		<th>#</th>
	</tr>
	</thead>
	<tbody>
 <?php  //$aff = array();
 if($aff){ $i=1;
 	foreach($aff as $row){ $stat = ($row->status == '0') ? 'request':'approved'; ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->company_name; ?></td>	
			<td><?php echo $row->competency; ?></td>
			<td><?php echo $row->contact; ?></td>
			<td><?php echo $row->remark; ?></td>
			<td><?php echo $row->approval; ?></td>
			<td><span class="label label-warning"><?php echo $stat; ?></span></td>
			<td style="text-align: center;">
			<?php
				echo '<button onclick="approve_aff([\''.$row->id.'\',\''.$row->approval.'\',\''.$row->id_tbl.'\'])" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>';
				echo '<button onclick="reject_temp_table('.$row->id.',\'temp_affiliates_avl\')" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>';
			?>
			</td>	
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="8">-</td></tr>
<?php } ?>
     </tbody>
</table>
</div>

