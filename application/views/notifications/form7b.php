<?php $this->load->view('notifications/header');?>

<br/>
<p style="margin-bottom: 0;">Dengan hormat,</p>
<p><strong><?php echo $vname;?><strong><br>
<?php echo $address;?></p>
<p>Dengan ini kami informasikan bahwa :</p>
<br>
<p class="callout">
<strong>Terdapat Perubahan Informasi pada RFP dan Aanwijzing, sebagai berikut:
<br><br>
<?php if ($data['aanwijzing_ind']) { ?>
    <h5>Aanwijzing Agenda</h5>
    <?php echo $data['aanwijzing_ind']; ?>
<?php } ?>
<br><br>
<?php if ($data['rfpfile']) { ?>
<h5>RFP File</h5>
<a href="<?php echo base_url().'webcontent/openFileRFP/'.$data['rfpfile'] ?>" target="_blank">RFP Link</a><br>
<?php } ?>
</strong>
<br><br>
</p>
<br>
<p>Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih</p>
<br/>
<br/>
<?php echo $sender;?>.
    
<?php $this->load->view('notifications/footer');?>