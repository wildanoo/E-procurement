<html lang="en-US">
<head>
  <title>E-Procurement Garuda</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/style.css">
  <link rel="icon" href="<?php echo base_url(); ?>assets/favicon-grd.png" sizes="16x16">
</head>
<body class="hold-transition login-bgcolor">  
  <div class="content-afterregis-policy row">
      <div class="col-md-8 col-md-push-2 box box-info" id = "white-header">
        <div class="box-header with-border">
          <h2 class="box-title col-left" style="margin-top: 10px;"><strong style="color: #368ccc;font-size:20px;">Terms and Conditions</strong></h2>
        </div>
        <?php echo form_open_multipart('register/confirmation',array('id'=>'form','class'=>'form-horizontal','method'=>'post'));  $current_date = date('Y/m/d')?>
          <div class="box-body policy-statement">
            <div class = "col-md-12 col-left">
                <?php echo $contents; ?>
              <br><br>
            </div>
          </div>
          <input type="checkbox" id="cb_confirmation" name="policy_confirmation" value="1" style="margin-top: -3px;margin-right: 10px;">I accept the Terms and Conditions
          <input type="hidden" name="trans_id" value="<?php echo $trans_id;?>">
          <input type="hidden" name="key" value="<?php echo $key;?>">
          <hr>
          <div class="box-footer pad-footer">
            <button type="submit" class="btn-primary btn-customs vcenter" id="btn_submit" style="float:left;margin:0 0 30px 15px;">Confirm</button>
            <span class="col-md-1 text-center vcenter"> or </span>
            <a class="col-md-6 row vcenter" id="btn_cancel" href="<?php echo base_url();?>register/destroy/<?php echo $trans_id;?>">back to home</a>
          </div>
        </div>
      </form>
      </div>
    </div>
    <footer class="main-footer footer-line container-fluid text-center">
      Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
    </footer>
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.color.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-easing-1.3.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/SmoothScroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/classie.js"></script>

    <!-- jangan lupa dibuka lagi -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarEffects.js"></script>
    <!-- jangan lupa dibuka lagi -->

    <script src="<?php echo base_url(); ?>assets/js/index.js"></script>
    <script type="text/javascript">
      $("#form").submit(function(e){
        if (!$("#cb_confirmation").is(":checked")) {
          alert("You must accept the terms and conditions first.");
          e.preventDefault();
          return false;
        }else{
          return true;
        }
      });
      $("#btn_cancel").click(function(e){
        if(!confirm('Are You Sure ?')){
          e.preventDefault();
          return false;
        }else{
          return true;
        }
      });
    </script>

  </body>
  </html>