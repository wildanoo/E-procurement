<div id="container10">
<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";   
	   
   $('#btn_create10').click(function(){
    	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		var values  = $("#form_ref").serializeArray();
				values.push({name: csrf,value: token});
				values = jQuery.param(values); 
						$.ajax({
						    url  : baseUrl + 'data_avl/update_affiliate',
						    type : "POST",              		               
						    data : values,
						    success: function(resp){
						        $("#container10").load(baseUrl+'data_avl/load_aff', function(){$('#aff_msg').html('Data akan ditampilkan setelah proses approval disetujui');});
						    }
						});							     	
     }); 
     
   $('#btn_cancel10').click(function(){ 
   		 $("#container10").load(baseUrl+'data_avl/load_aff');   		 
     });     
	   
   });

</script>
<div id="debug10"></div>
<?php echo form_open('#',array('id'=>'form_ref')); $current_date = date('Y-m-d'); ?>
<table width="50%">
<input type="hidden"  name="id" id="id" value="<?php echo $default->id; ?> "/>
<tr>
	<td><label>Company Name</label></td><td>:</td>
	<td><input type="text" name="company_name" id="company_name" value="<?php echo $default->company_name; ?>" placeholder="Company Name" class="form-control" required/></td>
	<td id="msg1">&nbsp;</td>	
</tr>
<tr>
	<td><label>Competency</label></td><td>:</td>
	<td><input type="text" name="competency" id="competency" value="<?php echo $default->competency; ?>" placeholder="Competency" class="form-control" required/></td>
	<td id="msg2">&nbsp;</td>	
</tr>
<tr>
	<td><label>Contact</label></td><td>:</td>
	<td><input type="text" name="contact" id="contact" value="<?php echo $default->contact; ?>" placeholder="Contact" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Remark</label></td><td>:</td>
	<td><input type="text" name="remark" id="remark" value="<?php echo $default->remark; ?>" placeholder="Remark" class="form-control" required/></td>
</tr>
</table>

<div style="text-align: right; ">
	<input type="button" id="btn_cancel10" value="Cancel" class="btn btn-default btn-sm"/>
	<input type="button" id="btn_create10" value="Update" class="btn btn-primary btn-sm"/>
</div>
<br/>
<?php echo  form_close(); ?>
</div>


