<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
</head>
<script type="text/javascript"> 	
$(document).ready(function(){	 
	 $(function(){  });

		var baseUrl = "<?php echo base_url(); ?>";

		$("#srch-term").autocomplete({
			source: baseUrl + 'category/get_cat_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
	        var search = $("#srch-term").val(); // alert(search);
	        var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
	        $.ajax({
	           url  : baseUrl + 'category/set_sess_search',
	           type : "POST",              		               
	           data : csrf +'='+ token +'&search='+search,
	           success: function(resp){    $("#debug").html(resp);            		            
				    document.location.href= baseUrl + 'category/supplier_category';
	           }
	        });
	    });
	 
	 $('#btn_create').click(function(){ 
	 	  form_create();	 	  
	 });	
});

function form_create(){
	var baseUrl = "<?php echo base_url(); ?>";
	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
    var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	BootstrapDialog.show({
	title: 'Create New Browse Category',
    message: $('<div></div>').load(baseUrl + "category/form_create_suppcat/"),       
    buttons: [{			            	
    	label: 'Submit',
        cssClass: 'btn btn-primary btn-sm',		               
        action: function(dialogRef){
        	 var id_cat  = $("#id_cat").val(); 
        	 var id_subcat = $("#id_subcat").val();
        	 
        	 $.post( baseUrl + 'category/is_exist_suppcat', { csrf: token , id_cat : id_cat, id_subcat : id_subcat })
			 .done(function( resp ) {
//					 $('#debug').html(resp);
//					 dialogRef.close(); 
					var json = $.parseJSON(resp);
					if (json.status=='true'){
	                	var values = $("#form").serializeArray();
	                	values.push({ name: csrf,value: token });
						values = jQuery.param(values);
						 $.ajax({
				               url  : baseUrl + 'category/create_suppcat',
				               type : "POST",              		               
				               data : values,
				               success: function(resp){
				               		document.location.href= baseUrl + 'category/supplier_category';	               				               		  
						       }
				            });   
					} else { 	
					 $("#msg1").html(json.msg1); 	
					 $("#msg2").html(json.msg2);
				}
       		 });  
        }
            }, {
            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
		                action: function(dialogRef) {
		                	document.location.href= baseUrl + 'category/supplier_category';
		                    dialogRef.close(); 
		                }
	        }]
});
}

function form_update(id){
	var baseUrl = "<?php echo base_url(); ?>";
	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
    var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
 
	BootstrapDialog.show({
	title: 'Update Browse Category',
    message: $('<div></div>').load(baseUrl + "category/form_update_suppcat/" + id),  
    buttons: [{			            	
                label: 'Submit',
                cssClass: 'btn btn-primary btn-sm',		               
                action: function(dialogRef){
                	 var id_cat  = $("#id_cat").val(); 
                	 var id_subcat = $("#id_subcat").val();
                	 
                	 $.post( baseUrl + 'category/is_exist_suppcat', { csrf: token , id_cat : id_cat, id_subcat : id_subcat })
					 .done(function( resp ) {
//							 $('#debug').html(resp);
//							 dialogRef.close(); 
							var json = $.parseJSON(resp);
							if (json.status=='true'){
			                	var values = $("#form").serializeArray();
			                	values.push({ name: csrf,value: token });
								values = jQuery.param(values);
								 $.ajax({
						               url  : baseUrl + 'category/update_suppcat',
						               type : "POST",              		               
						               data : values,
						               success: function(resp){
						               		document.location.href= baseUrl + 'category/supplier_category';	               				               		  
								       }
						            });   
							} else { 	
							 $("#msg1").html(json.msg1); 	
							 $("#msg2").html(json.msg2);
						}
               		 });            
                }
            }, {
            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
		                action: function(dialogRef) {
		                	document.location.href= baseUrl + 'category/supplier_category';
		                    dialogRef.close(); 
		                }
	        }]
	});
}


</script>

<body>
 <div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Vendor Category</h3>
              </div><!-- /.box-header -->

                <br>

                <div class="col-md-2 pull-left">
                    <input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Vendor Category" value="New Vendor Category" style="width:150px;" title="Create New Category"/>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
				<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
                </div>
                
                <div class = "col-md-10 pull-right">
                  <div class="input-group pull-right" style="width: 200px;">
                      <input type="text" class="form-control input-sm pull-right" name="srch-term" id="srch-term" 
                      	placeholder="Search" style="height: 28px;">
                      <div class="input-group-btn">
                        <button id="btn_search" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                </div>  

<div class="box-body">
<table border="1" id="browse" class="table table-bordered table-hover text-center" cellspacing="0" width="80%" style="font-size: 13px">
	<thead>
		<tr>
            <th style="width:4%;text-align:center;">No</th>
            <th>Category</th>
            <th>Sub Category</th>
            <th>Status</th>
            <th>Created Date</th>
            <th style="text-align: center;">Action</th>
        </tr>
     </thead>
     <tbody>
 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td style="text-align:center;"><?php echo $num = $num+1; ?></td>
			<td><?php echo $row->category; ?></td>
			<td><?php echo $row->subcategory; ?></td>
			<td align="center"><?php   if($row->status==1) echo "Active";  else echo "Disabled"; ?></td>
 			<td align="center"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>						
				<?php	$del_ico   = '<img src="'.base_url().'assets/images/delete.png" width="20" height="20"/>'; 
						$edit_ico  = '<img src="'.base_url().'assets/images/edit.png" width="20" height="20"/>'; ?>				
				<td><?php				
					echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete category ?')){ return true; } else {return false; }";
					echo anchor('category/delete_suppcat/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Subcategory','onclick'=>$js));?>
				</td>					
		</tr>
<?php	$i++;  } ?>	
<tr><td colspan="8" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?><tr><td colspan="8">-</td></tr><?php } ?>
     </tbody>
                  </table>
                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->
