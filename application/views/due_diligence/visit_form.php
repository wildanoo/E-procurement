<script type="text/javascript">
  $(function () { 
   	  var baseUrl  = "<?php echo base_url(); ?>"; 
   	     
     $("#visit_date").datetimepicker({
		format:'Y/m/d H:i',
		//minDate: 0,
		onChangeDateTime: function (dp,selectedDate) {
			var time = $("#visit_date").val();
        }
	});
     
     $('#btn_cancel_visit').click(function(){ 
   			$("#container11").load(baseUrl+'due_diligence/load_visit_form');   		
   	  }); 
   	  
   	 $('#btn_visit').click(function(){ 
   	  
   	  		var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	    
		    var values   = $("#form_visit").serializeArray();
			values.push({name: csrf,value: token});
			values = jQuery.param(values);
			 	$.post( baseUrl+ "due_diligence/save_visit", values )
				  .done(function( resp ) {
				     $("#container11").load(baseUrl+'due_diligence/load_visit_form');
				     //$("#debug11").html(resp); 
				});
	     	
	     });    
     
  });

</script>

<div id="debug11"></div>

<?php echo form_open('#',array('id'=>'form_visit')); $current_date = date('Y/m/d H:i'); ?>
<table width="80%">
	<tr>
		<td><label>Jenis Kantor</label></td><td>:</td>
		<td><input type="text" name="office_type" id="office_type" class="form-control" placeholder="Jenis Kantor" required/></td>
	</tr>	
	<tr>
		<td style="vertical-align: top;"><label>Alamat Kantor</label></td><td style="vertical-align: top;">:</td>
		<td><textarea name="office_address" id="office_address" cols="20" rows="5" class="form-control" placeholder="Alamat Kantor" required></textarea></td>
	</tr>
	<tr>
		<td><label>PIC</label></td><td>:</td>
		<td><input type="text" name="pic" id="pic" class="form-control" placeholder="PIC" required/></td>
	</tr>
	<tr>
		<td><label>No. Telp</label></td><td>:</td>
		<td><input type="text" name="office_telp" id="office_telp" class="form-control" placeholder="No. Telephone" required/></td>
	</tr>
	<tr>
		<td><label>No. Fax</label></td><td>:</td>
		<td><input type="text" name="office_fax" id="office_fax" class="form-control" placeholder="No Fax" required/></td>
	</tr>
	<tr>
		<td><label>Waktu Kedatangan</label></td><td>:</td>
		<td>
			
			<input type="text" name="visit_date" id="visit_date"  value="<?php echo $current_date; ?>" style="width:130px;height: 25px;" />	
			<!--input type="text" name="visit_hour" id="visit_hour"  placeholder="Jam"   style="width:80px;height: 25px;" />:
			<input type="text" name="visit_minute" id="visit_minute"  placeholder="Menit"   style="width:80px;height: 25px;" /-->
		</td>
	</tr>
	<tr>
		<td style="vertical-align: top;"><label>Dalam Rangka</label></td><td style="vertical-align: top;">:</td>
		<td><textarea name="remark" id="remark" cols="20" rows="5" class="form-control" placeholder="Keterangan" required></textarea></td>
	</tr>
	<tr>
		<td align="right" colspan="3">
			<button type="button" id="btn_cancel_visit" class="btn btn-default">Cancel</button>
			<?php if($this->permit->md3){ ?><button type="button" id="btn_visit" class="btn btn-info btn-primary">Create</button><?php } ?>			
		</td>
	</tr>
</table>

