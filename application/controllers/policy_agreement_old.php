<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Policy_agreement extends Controller {

	function __construct()
	{
		parent::__construct();		
		$this->load->helper('file');
		$this->load->model('m_policy_agreement');					
	}

 function index(){
 	if ($this->m_policy_agreement->authenticate()) {
 			
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$sess_cat   = $this->session->userdata('sess_cat');			
			
		$field      = $sess_cat != '' ? "type" : "";
		$id_cat     = $sess_cat != '' ? $sess_cat  : "";			
		
		if($sess_cat != ''){	$where = array('type'=>$sess_cat);
		} else { $where = "";	}

 		$table 	    = "policy_agreement";		
		$page       = $this->uri->segment(3);
		$per_page   = 5;		
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table,$where);
		$set_config = array('base_url'=> base_url().'policy_agreement/index','total_rows'=>$total_rows,'per_page'=>5,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);
			 
		$this->load->library('pagination');			 
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();			
			
		$order 			     = array('field'=>'type','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset; 
		$select 			 = "id,status,type,created_date,detail";
		
		$sessID  = $this->session->flashdata('anID');
		$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
		
		if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
		else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
		$data['browse'] = $browse;
 		
 		$data['status']   = array('1'=>'Create','2'=>'Review','3'=>'Approve','4'=>'Publish','5'=>'Edit');
 		//$data['category'] = array('0' => 'All','1'=>'General','2'=>'Open Sourcing','3'=>'Open Bidding','4'=>'Direct Selection');
 		$data['category'] = array('' => 'All','0'=>'General','2'=>'Open Sourcing','3'=>'Open Bidding','4'=>'Direct Selection');
 		$data['view']	  = "policy_agreement/browse"; 		
 		$this->load->view('layout/template',$data);
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	} 	
 } 
 
 function form_create(){ 	
 
 	$data['user_id'] = $this->tank_auth->get_user_id();
	$data['username']= $this->tank_auth->get_username();
	$select = "id";
 	
 	//$data['category'] = array('0' => 'All','1'=>'General','2'=>'Open Sourcing','3'=>'Open Bidding','4'=>'Direct Selection');
 	$category = array('0'=>'General','1'=>'News','2'=>'Open Sourcing','3'=>'Open Bidding','4'=>'Direct Selection');
 	$data['category'] = $category;
 	
 	$data['content']= ""; 
 	$data['view']	= "policy_agreement/form_create"; 		
 	$this->load->view('layout/template',$data);
 		
 } 

 function form_update(){ 	
 	$id  = $this->uri->segment(3);
 	$def =  $this->crud->browse("","policy_agreement","id",$id,"false");
 	$data['def'] = $def;
 	
 	$data['user_id'] = $this->tank_auth->get_user_id();
	$data['username']= $this->tank_auth->get_username();
	
	$select   = "id";
	
	if($def->file){

		$ctpath_id  = "./uploads/policy_agreement/".$def->file."_ind.txt";		
		$ctpath_eng  = "./uploads/policy_agreement/".$def->file."_eng.txt";		
		
 		$contents[0] = file_exists($ctpath_id) ? file_get_contents($ctpath_id) : "-";
 		$contents[1] = file_exists($ctpath_eng) ? file_get_contents($ctpath_eng) : "-";
 	}

 	$category = $this->crud->browse("","category","","","true","id,category"); 
 	if(!$category) $category = array(); 	
 	$select = array('0'=>'-- Select --');
 	foreach($category as $val){ $options[$val->id] = $val->category; } 
 	$data['category'] = $select + $options; 	
 	// $data['subcat']  = $this->crud->browse("","subcategory","id",$def->id_subcat,"false","id,subcategory");
 	$data['contents'] = !$contents ? "-" : $contents; 
 	$data['view']	 = "policy_agreement/form_update";
 	$this->load->view('layout/template',$data);
 		
 } 
 
 function get_office(){
 	$id_comp = $_POST['id_comp']; //echo "=>".$id_comp;
 	$select  = " id,office_name,(SELECT region_name FROM region r WHERE r.id=l.id_region) as region";				
 	$company = $this->crud->browse("","offices l","id_company",$id_comp,"true",$select); 	
	echo "<option>-- Select Office --</option>";
	foreach ($company as $row){
		echo "<option value='$row->id'>$row->office_name / $row->region</option>";	
	}
 }
 
 function get_subcategory(){
 	$id_cat  = $_POST['id_cat']; 
 	$select  = "(SELECT id FROM subcategory r WHERE r.id=l.id_subcat) as id_subcat,
 				(SELECT subcategory FROM subcategory r WHERE r.id=l.id_subcat) as subcat";
 	$subcat  = $this->crud->browse("","supplier_category l","id_cat",$id_cat,"true",$select); //print_r($subcat);	
	echo "<option>-- Select Sub Category --</option>";
	foreach ($subcat as $row){
		echo "<option value='$row->id_subcat'>$row->subcat</option>";	
	}
 }

  function get_vendor(){

 	$id_cat  = $_POST['id_cat'];
   	
   	$select  = "vendor.id,vendor_name";
   	$where = "vendor_detail.id_category = ".$id_cat." AND vendor_type='avl'";  		

   	$joins[0][0] = "vendor_detail";
   	$joins[0][1] = "vendor_detail.id_vendor = vendor.id";
   	$joins[0][2] = "left";
   	$joins[1][0] = "category";
   	$joins[1][1] = "vendor_detail.id_category = category.id";
   	$joins[1][2] = "left";

   	$vendor  = $this->crud->browse_join("","vendor","","","true",$select,$joins,$where);
   	$element = '';
  	foreach ($vendor as $row){
  		$element .= "<li>
 				<input id='$row->id' name='cbvendor[".$row->id."]' type='checkbox' value='".$row->vendor_name."'>
 				<label for='$row->id'>
 					<span>$row->vendor_name</span>
 				</label>
 			</li>";
  	}

  	echo $element;
   }

   function get_vendor_with_return($flag = null, $policy_agreement_id){

 	$id_cat  = $flag;
   	
   	$select  = "vendor.id,vendor_name";
   	$where = "vendor_detail.id_category = ".$id_cat." AND vendor_type='avl'";  		

   	$joins[0][0] = "vendor_detail";
   	$joins[0][1] = "vendor_detail.id_vendor = vendor.id";
   	$joins[0][2] = "left";
   	$joins[1][0] = "category";
   	$joins[1][1] = "vendor_detail.id_category = category.id";
   	$joins[1][2] = "left";

   	$direct_biddings  = $this->crud->browse("","direct_bidding","","","true","id_vendor","id_policy_agreement = ".$policy_agreement_id);
   	$direct_bidding = array();
   	foreach ($direct_biddings as $val){array_push($direct_bidding, $val->id_vendor);}	

   	$vendor  = $this->crud->browse_join("","vendor","","","true",$select,$joins,$where);
   	$element = '';
  	foreach ($vendor as $row){
  		if (in_array($row->id, $direct_bidding)) {
  			$element .= "<li>
 				<input id='$row->id' checked name='cbvendor[".$row->id."]' type='checkbox' value='".$row->vendor_name."'>
 				<label for='$row->id'>
 					<span>$row->vendor_name</span>
 				</label>
 			</li>";
  		}else{
  			$element .= "<li>
 				<input id='$row->id' name='cbvendor[".$row->id."]' type='checkbox' value='".$row->vendor_name."'>
 				<label for='$row->id'>
 					<span>$row->vendor_name</span>
 				</label>
 			</li>";
  		}
  	}

  	return $element;
   }
 
 function create(){
 	$filename 	= $this->m_policy_agreement->file_name("policy_agreement");
 	$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
 	$data 		= array('id'			=> null,
					 	'type'			=> $_POST['type'],
					 	'detail'		=> $_POST['detail'],
					 	'creator'		=> $_POST['creator'],
					 	'file'			=> $filename,
					 	'status'		=> '1',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date);
 				  		
 	if($_POST['content'] == "1"){ 		//echo "from typing";
	 	$editor = $_POST['editor'];	 		
	 	$root   = $_SERVER['DOCUMENT_ROOT'];
	 	$path   = $root.SRC_PATH."/uploads/policy_agreement/";
	 	for ($i = 0;$i<2;$i++) {
	 		if ($i==0) {
	 			$myfile = fopen($path.$filename."_ind.txt", "w");
	 		}else{
	 			$myfile = fopen($path.$filename."_eng.txt", "w");
	 		}
	 		$txt    = fwrite($myfile,$editor[$i]);	
	 	}
 	} else if ($_POST['content'] == '2' && $_FILES['contentfileind'] && $_FILES['contentfileeng']) { 		//echo "from upload"; 
 		$ctnfile[0]  = $_FILES['contentfileind'];
 		$ctnfile[1]  = $_FILES['contentfileeng'];
 		for ($i=0; $i < 2; $i++) { 
 			$_FILES['userfile']  = $ctnfile[$i];
	 		$path 	  = './uploads/policy_agreement/';
	 		if ($i==0) {
	 			$config   = $this->m_policy_agreement->set_config($filename."_ind.txt",$path,'txt');
	 		}else{
	 			$config   = $this->m_policy_agreement->set_config($filename."_eng.txt",$path,'txt');
	 		}
	 		$this->load->library('upload', $config);
	 		$this->upload->initialize($config);
	 		
	 		$msg['error_content'] = "";  $msg['success_content']   = array();
			if ( ! $this->upload->do_upload()){
				$msg['error_content']   = $this->upload->display_errors();
			} else {
				$msg['success_content'] = $this->upload->data(); }
 		}
 	}

 	$id = $this->crud->insert("","policy_agreement",$data);

 	$this->session->set_flashdata('message','1 data success insert');
 	redirect('policy_agreement/','refresh');
 }  
 
 function update(){ 	
 	$def        = $this->crud->browse("","policy_agreement","id",$_POST['id'],"false");
 	$filename 	= !$def->file ? $this->m_policy_agreement->file_name("policy_agreement") : $def->file;
 	$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();

 	$data 		= array('type'			=> !$_POST['type'] ? $def->type : $_POST['type'],
					 	'detail'		=> !$_POST['detail'] ? $def->detail : $_POST['detail'],
					 	'creator'		=> !$_POST['creator'] ? $def->creator : $_POST['creator'],
					 	'file'			=> $filename,
					 	'status'		=> '1',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date);

 				  		
 	if($_POST['content'] == "1"){ 		//echo "from typing";
	 	$editor = $_POST['editor'];
	 	$root   = $_SERVER['DOCUMENT_ROOT'];
	 	$path   = $root.SRC_PATH."/uploads/policy_agreement/";
	 	for ($i = 0;$i<2;$i++) {
	 		if ($i==0) {
	 			$myfile = fopen($path.$filename."_ind.txt", "w");
	 		}else{
	 			$myfile = fopen($path.$filename."_eng.txt", "w");
	 		}
	 		$txt    = fwrite($myfile,$editor[$i]);	
	 	}
 	} else if ($_POST['content'] == '2' && $_FILES['contentfileind'] && $_FILES['contentfileeng']) { 		//echo "from upload"; 
 		$ctnfile[0]  = $_FILES['contentfileind'];
 		$ctnfile[1]  = $_FILES['contentfileeng'];
 		for ($i=0; $i < 2; $i++) { 
 			$_FILES['userfile']  = $ctnfile[$i];
	 		$path 	  = './uploads/policy_agreement/';
	 		if ($i==0) {
	 			$config   = $this->m_policy_agreement->set_config($filename."_ind.txt",$path,'txt');
	 		}else{
	 			$config   = $this->m_policy_agreement->set_config($filename."_eng.txt",$path,'txt');
	 		}
	 		$this->load->library('upload', $config);
	 		$this->upload->initialize($config);
	 		
	 		$msg['error_content'] = "";  $msg['success_content']   = array();
			if ( ! $this->upload->do_upload()){
				$msg['error_content']   = $this->upload->display_errors();
			} else {
				$msg['success_content'] = $this->upload->data(); }
 		}
 	}

 	$this->crud->update("","policy_agreement","id",$_POST['id'],$data); 	
 	$this->session->set_flashdata('message','1 data success insert');
 	redirect('policy_agreement/','refresh');
 }

 function get_tags(){ 	
 	$term   = $_GET['term'];  	
 	$order  = array('field'=>'code','sort'=>'ASC'); $select = "code as name";
 	$result = $this->crud->autocomplete("","policy_agreement",$select,"code",$term,"",$order);  	
 	echo json_encode($result);   	 		
 }

 function reviewed(){
 	if ($this->m_policy_agreement->authenticate()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
	 	$id        = $this->uri->segment(3);
	 	$select	   = "id,type,detail,creator,file,
					  status,created_id,created_date,last_updated";						
	 	$default   = $this->crud->browse("","policy_agreement l","id",$id,"false",$select); 

	 	if($default->file){

			$ctpath_id  = "./uploads/policy_agreement/".$default->file."_ind.txt";		
			$ctpath_eng  = "./uploads/policy_agreement/".$default->file."_eng.txt";		
			
	 		$contents[0] = file_exists($ctpath_id) ? file_get_contents($ctpath_id) : "-";
	 		$contents[1] = file_exists($ctpath_eng) ? file_get_contents($ctpath_eng) : "-";
	 	}

	 	$data['content'] = !$contents ? "-" : $contents; 		 	
	 	
	 	$data['type']    = array('0'=>'General','1'=>'News','2'=>'Open Sourcing','3'=>'Open Bidding','4'=>'Direct Selection');
	 	$data['default'] = $default;
	 	$data['view'] 	 = "policy_agreement/form"; 		
	 	$this->load->view('layout/template',$data);
	 	
	 } else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	} 	
 	
 }
 
 function open_content_ind(){ 	
 	$id       = $this->uri->segment(3);
 	$default  = $this->crud->browse("","policy_agreement","id",$id,"false","file"); 	
	$ctpath   = "./uploads/policy_agreement/".$default->file."_ind.txt";
	echo file_get_contents($ctpath);
 }

  function open_content_eng(){ 	
  	$id       = $this->uri->segment(3);
  	$default  = $this->crud->browse("","policy_agreement","id",$id,"false","file"); 	
 	$ctpath   = "./uploads/policy_agreement/".$default->file."_eng.txt";
 	echo file_get_contents($ctpath);
  }
 
 function open_file(){
 	
 	$index    = $this->uri->segment(3);
 	$id       = $this->uri->segment(4);
 	$default  = $this->crud->browse("","policy_agreement","id",$id,"false","file"); 
	$folder   = $index=="1" ? "attach" : "rfp";
	$file     = "./uploads/".$folder."/".$default->file.".pdf";
	$filename = $default->file.".pdf";

	header('Content-type: application/pdf');
	header('Content-Disposition: inline; filename="' . $filename . '"');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($file));
	header('Accept-Ranges: bytes');
	@readfile($file);

 }

 function update_status(){
 	
 	$date   = date('Y-m-d H:i:s');
 	$update = array('status'=>$_POST['id_status'],'last_updated'=>$date);

 	$this->crud->update("","policy_agreement","id",$_POST['id'],$update); 	
 	$this->session->set_flashdata('message','1 data success update');
 	redirect('policy_agreement/','refresh');
 	
 }
 
 function set_browse_session(){
 	$code     = $_POST['search'];	
 	$policy_agreement = $this->crud->browse("","policy_agreement","code",$code,"false","id");
 		
 	if($policy_agreement) $this->session->set_flashdata('anID',$policy_agreement->id); 
 	
 }
 
 function set_sess_category(){
 	
 	$type = $_POST['type'];
 	if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
 	} else { $this->session->unset_userdata('sess_cat'); }
 	
 } 
 
}
?>