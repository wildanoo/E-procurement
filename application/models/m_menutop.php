<?php 
class m_menutop extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('crud');
	}

	function authenticate(){
		$allowed  	= array(139);
		$permit  	= $this->session->userdata('permissions');
		$permit 	= explode(",",$permit);
		foreach($permit as $str){
			$permissions[] = preg_replace("/[^0-9]/","",$str);
		}
		$result 	= array_intersect($allowed, $permissions);
		if($result){ return true;} return false;
	}

	function show1($id_up){
		$table2 		= "menu2 t3";
		$select2 		= "t3.id_up as menu2ID,t3.menu2";
		$joins2[0][0] 	= "menu1 t2";
		$joins2[0][1] 	= "t3.id_up=t2.id";
		$joins2[0][2] 	= "left";
		$where 			= "t3.id_up = $id_up";
		$browse2 		= $this->crud->browse_join("",$table2,"","","",$select2,$joins2,$where);

		return $browse2;
	}

	function checkshow($idm1="",$dbmenu){
		$query = "SELECT * FROM $dbmenu WHERE id_up = $idm1";
		$data = $this->db->query($query)->result();
		foreach($data as $dat){
			$data2[]= $dat->url;
		}
		$data3 = array('#');
		$result = array_diff($data2,$data3);
		if(empty($result)){
			$show = "true";
		}else{
			$show = "false";
		}
			return $show;
	}
}

