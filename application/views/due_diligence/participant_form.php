<script type="text/javascript">
  $(function () { 
   	  var baseUrl  = "<?php echo base_url(); ?>"; 
   	  
   	   $('#btn_cancel').click(function(){ 
   			$("#container11").load(baseUrl+'due_diligence/load_visit_form');   		
   	   }); 
   	   
   	   $('#btn_visit').click(function(){
   	  
   	  		var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	    
		    var values   = $("#form_visitor").serializeArray();
			values.push({name: csrf,value: token});
			values = jQuery.param(values);
			 	$.post( baseUrl+ "due_diligence/save_participant", values )
				  .done(function( resp ) {
				    	var id_visit   = $("#id_visit").val();
				    	$("#container11").load(baseUrl+'due_diligence/load_participant/'+ id_visit); 
				});
	     	
	     });    
   	  
     
  });

</script>

<div id="debug_visit"></div>

<?php echo form_open('#',array('id'=>'form_visitor')); ?>
<input type="hidden" name="id_visit" id="id_visit" value="<?php echo $id_visit; ?>"/>
<table width="80%">
	<tr>
		<td><label>Visitor Name</label></td><td>:</td>
		<td><input type="text" name="visitor_name" id="visitor_name" class="form-control" placeholder="Visitor Name" required/></td>
	</tr>	
	<tr>
		<td><label>Visitor Unit</label></td><td>:</td>
		<td><input type="text" name="visitor_unit" id="visitor_unit" class="form-control" placeholder="Visitor Unit" required/></td>
	</tr>
	<tr>
		<td align="right" colspan="3">
			<button type="button" id="btn_cancel" class="btn btn-default">Cancel</button>
			<?php if($this->permit->md3){ ?><button type="button" id="btn_visit" class="btn btn-info btn-primary">Create</button><?php } ?>			
		</td>
	</tr>
</table>

