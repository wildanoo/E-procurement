<html>
<head>
<title></title>
<meta name="" content="">
</head>
<body>
Dengan hormat,<br/>
<b><?php echo $vendor_name; ?></b><br/><br/>

Pertama tama kami mengucapkan terima kasih atas minat Bapak/Ibu untuk mendaftar menjadi Rekanan PT Garuda Indonesia (Persero) Tbk ("Garuda�).<br/> 
Berdasarkan evaluasi yang telah kami lakukan, bersama ini kami sampaikan bahwa Perusahaan Bapak/Ibu belum dapat kami evaluasi ke tahapan lebih lanjut. <br/>

Adapun data perusahaan yang telah Bapak/Ibu daftarkan akan kami simpan di dalam database kami dan akan kami hubungi kembali 
apabila Garuda membutuhkan kategori yang sesuai dengan Perusahaan Bapak/Ibu.<br/><br/>

Sekali lagi kami sampaikan terima kasih atas partisipasi Perusahaan Bapak/Ibu, semoga kita dapat bekerja sama untuk  kesempatan mendatang.<br/>


Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/>
PT Garuda Indonesia (Persero) Tbk <br/> 
Business Support / JKTIBGA <br/>
Gedung Management Garuda, Lantai Dasar <br/>
Garuda City,  Bandara Soekarno Hatta <br/>
Cengkareng 19120, PO BOX 1004 TNG BUSH



</body>
</html>