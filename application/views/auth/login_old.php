<body class="page-header-fixed2 page-container-bg-solid2">
    <div id="loader-overlay"><img src="<?php echo base_url()?>assets_public/images/plane.gif" alt="Loading" /></div>
    <div class="page-header2 navbar navbar-fixed-top2">
        <header class="clearfix header5">
            <div class="container">
                <div class="navbar navbar-default navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#"><img alt="" src="<?php echo base_url()?>assets_public/images/garuda-logo.png">
                                <p class="cos-custom"><span style="color:#428bca">e-Procurement</span> Garuda Indonesia</p>
                            </a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right2">
                                <li class="active"><a href="#login">LOGIN</a></li>
                                <li><a href="registration.html#top">REGISTRATION</a></li>
                                <li><a href="registration.html#top">VENDOR POLICY</a></li>
                            </ul>
                            <ul class="social-icons">
                                <li class="eng img-lang" title="English"><a href="index.html"></a></li>
                                <li class="ind img-lang" title="Indonesian"><a href="index.html"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <div class="clearfix"> </div>
    <div class="page-container">
        <div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse">
                <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                    <li class="nav-item start active open">
                        <a href="#top" class="nav-link nav-toggle">
                            <i class="fa fa-home"></i>
                            <span class="title">Home</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="#news" class="nav-link nav-toggle">
                            <i class="fa fa-bullhorn"></i>
                            <span class="title">News</span>
                            <span class="title">and</span>
                            <span class="title">Announ</span>
                            <span class="title">cement</span>
                            <span class="arrow"></span>
                        </a>
                    </li>  
                    <li class="nav-item  ">
                        <a href="#faq" class="nav-link nav-toggle">
                            <i class="fa fa-question-circle"></i>
                            <span class="title">FAQs</span>
                            <span class="arrow"></span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="#contact" class="nav-link nav-toggle">
                            <i class="fa fa-map-marker"></i>
                            <span class="title">Contact Us</span>
                            <span class="arrow"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="page-content-wrapper">
            <div class="page-content">
                <header>
                    <div class="header-bg">
                        <div id="search-overlay">
                            <div class="container2">
                              <div id="close">X</div>
                              <input id="hidden-search" type="text" placeholder="Start Typing..." autofocus autocomplete="off"  />
                              <input id="display-search" type="text" placeholder="Start Typing..." autofocus autocomplete="off" />
                          </div>
                      </div>
                  </div>
              </header>

              <div class="complete-content">  
                <div class="container2 full-width-container ihome-banner">
                    <div class="banner col-sm-12 col-xs-12 col-md-12">
                        <ul>
                            <li data-transition="papercut" data-slotamount="7">
                                <img src="<?php echo base_url()?>assets_public/images/new-slider/s1-bg.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                                <div class="tp-caption lfb skewtoright imed-sl1"
                                data-x="left"
                                data-y="bottom"
                                data-hoffset="0"
                                data-speed="1000"
                                data-start="1000"
                                data-easing="Power4.easeOut"
                                data-endspeed="400"
                                data-endeasing="Power1.easeIn"
                                ><img src="<?php echo base_url()?>assets_public/images/client3.png" alt="" class="img-responsive">
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption lft skewtoright imed-sl1"
                            data-x="left"
                            data-y="85"
                            data-hoffset="83"
                            data-speed="1200"
                            data-start="1300"
                            data-easing="Power4.easeOut"
                            data-endspeed="400"
                            data-endeasing="Power1.easeIn"
                            ><!-- <img src="images/client3.png" alt="" class="img-responsive"> -->
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption lft skewtoright imed-sl1"
                        data-x="left"
                        data-y="74"
                        data-hoffset="45"
                        data-speed="1400"
                        data-start="1600"
                        data-easing="Power4.easeOut"
                        data-endspeed="400"
                        data-endeasing="Power1.easeIn"
                        ><!-- <img src="images/new-slider/s1-img2.png" alt="" class="img-responsive"> -->
                    </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption lft skewtoright imed-sl1"
                    data-x="left"
                    data-y="45"
                    data-hoffset="61"
                    data-speed="1600"
                    data-start="1900"
                    data-easing="Power4.easeOut"
                    data-endspeed="400"
                    data-endeasing="Power1.easeIn"
                    ><!-- <img src="images/new-slider/s1-img1.png" alt="" class="img-responsive"> -->
                </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption col-md-push-4 col-md-8"
                data-x="right"
                data-y="115"
                data-hoffset="-60"
                data-speed="1000"
                data-start="2400"
                data-easing="Back.easeOut"
                data-endspeed="400"
                data-endeasing="Power1.easeIn"
                >

                <div class="iconlist-wrap">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                        <div class="col-md-12 no-pad">
                            <!-- <div class="col-md-6 no-pad"> -->
                            <ul>
                                <li class="news-custom">Welcome to</li>
                            </ul>

                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>


            <!-- LAYER NR. 6 -->
            <div class="tp-caption col-md-push-4 col-md-8"
            data-x="right"
            data-y="222"
            data-hoffset="-10"
            data-speed="1000"
            data-start="2900"
            data-easing="Back.easeOut"
            data-endspeed="400"
            data-endeasing="Power1.easeIn"
            >
            <div class="subtitle notViewed wow fadeInRight animated" data-wow-delay="0.5s" data-wow-offset="20" style="visibility: visible;-webkit-animation-delay: 0.5s; -moz-animation-delay: 0.5s; animation-delay: 0.5s;"><span class="iconlist-mid-title" style="margin-left: -52px !important;margin-bottom:15px;margin-top:-20px;">e-Procurement</span> Garuda Indonesia</div>
        </div>


        <!-- LAYER NR. 7 -->
        <div class="tp-caption col-md-push-4 col-md-8"
        data-x="right"
        data-y="280"
        data-hoffset="-60"
        data-speed="1000"
        data-start="3400"
        data-easing="Back.easeOut"
        data-endspeed="400"
        data-endeasing="Power1.easeIn"
        >

        <div class="col-md-12 col-xs-12 col-sm-12 no-pad heading-content wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200" style="visibility: visible;-webkit-animation-delay: 0.5s; -moz-animation-delay: 0.5s; animation-delay: 0.5s;">
            <p>e-Procurement is a user-friendly, Internet-based purchasing system that offers electronic purchase order processing and enhanced administrative functions to buyers and suppliers, resulting in operational efficiencies and potential cost savings.<br>
                The e-Procurement Service features the most modern business and commercial practices from the private sector and the most current technical capabilities offered by the Internet that will provide cost-saving opportunities to both suppliers and PT Garuda Indonesia (Persero), Tbk.</p>
            </div>
        </div>

    </li> 

</ul>

</div>
</div>
<div class="about-content-wrap pull-left" id="#">
    <div style="margin-top:-85px;position:absolute;">&nbsp;
        <a name="news"><span></a>
    </div>
    <div class="head">
        <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;">NEWS AND ANNOUNCEMENTS</h1>
        <a href="#news" class="next-section about animate2">
            <i class="fa fa-angle-double-down"></i>
        </a>
    </div>
    <div class="container">



        <div class="col-xs-12 col-sm-10 col-md-10 col-md-push-1 content-tabs-wrap wow fadeInUp animated" data-wow-delay="1.7s" data-wow-offset="200">
            <div class="content-tabs">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab-home" data-toggle="tab">News</a></li>
                  <li><a href="#tab-profile" data-toggle="tab">Announcements</a></li>
              </ul>
              <div class="tab-content">
                  <div class="tab-pane fade in active" id="tab-home">
                      <div class="latest-post-wrap pull-left wow fadeInLeft" data-wow-delay="0.5s" data-wow-offset="100">
                          <div class="post-item-wrap pull-left col-sm-12 col-md-12 col-xs-12">
                              <img src="<?php echo base_url()?>assets_public/images/news-1.jpg" class="img-responsive post-author-img" alt="" />
                              <div class="post-content1 col-md-9 col-sm-9 col-xs-8">
                                <div class="post-title pull-left"><a href="news-detail.html">Prohibition of Gifts Ied Fitri 1437H</a></div>
                                <div class="post-meta-top pull-left">
                                    <ul>
                                        <li><i class="icon-calendar"></i>23 JUN 2016</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="post-content2 pull-left">                   
                                <p>In order to fulfill the commitment as the Public Company that always conform and uphold the principles of Good Corporate Governance, Business Ethics, and Work Ethics...<br />
                                    <span class="post-meta-bottom"><a href="news-detail.html">Continue Reading</a></span></p>
                                </div>
                            </div>
                            <div class="post-item-wrap pull-left col-sm-12 col-md-12 col-xs-12" style="margin-top: 25px;">
                              <img src="<?php echo base_url()?>assets_public/images/news-2.jpg" class="img-responsive post-author-img" alt="" />
                              <div class="post-content1 col-md-9 col-sm-9 col-xs-8">
                                <div class="post-title pull-left"><a href="news-detail.html">e-Auction - Procurement Company Merchandise</a></div>
                                <div class="post-meta-top pull-left">
                                    <ul>
                                        <li><i class="icon-calendar"></i>02 JUN 2016</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="post-content2 pull-left">                   
                                <p>As a manifestation of the commitment PT. Garuda Indonesia (persero),Tbk. to carry out the procurement of goods / services in a clean and transparent and to improve the effectiveness of the processes that run. on May 27, 2016 at 09:30 AM, at Bidding Room...<br />
                                    <span class="post-meta-bottom"><a href="news-detail.html">Continue Reading</a></span></p>
                                </div>
                            </div>

                            <div class="post-item-wrap pull-left col-sm-12 col-md-12 col-xs-12">
                              <img src="<?php echo base_url()?>assets_public/images/news-2.jpg" class="img-responsive post-author-img" alt="" />
                              <div class="post-content1 col-md-9 col-sm-9 col-xs-8">
                                <div class="post-title pull-left"><a href="news-detail.html">Sales & Marketing Training Provider Registration</a></div>
                                <div class="post-meta-top pull-left">
                                    <ul>
                                        <li><i class="icon-calendar"></i>28 APR 2016</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="post-content2 pull-left">                   
                                <p>In order to support our sales development activities, PT Garuda Indonesia (Persero) Tbk (“Garuda”) is looking for a qualified company to provide Sales & Marketing Training with qualifications...<br />
                                    <span class="post-meta-bottom"><a href="news-detail.html">Continue Reading</a></span></p>
                                </div>
                            </div>

                            <a href="news.html" class="dept-details-butt posts-showall">Show All</a>

                        </div>

                    </div>
                    <div class="tab-pane fade" id="tab-profile">
                      <div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active" id="ui-accordion-imedica-dep-accordion-panel-0" aria-labelledby="ui-accordion-imedica-dep-accordion-header-0" role="tabpanel" aria-expanded="true" aria-hidden="false" style="display: block;">                    
                          <img src="<?php echo base_url()?>assets_public/images/dep-dummy.jpg" class="img-responsive dept-author-img-desk col-md-4" alt="">
                          <div class="dept-content pull-left col-md-7 col-lg-8">
                              <div class="dept-title pull-left">Garuda Indonesia requires iPad Mounting Provider</div> 
                              <p>In order to support and enhance our operational safety level, PT Garuda Indonesia (Persero) Tbk (“Garuda”) is looking for a qualified company to provide iPad Mounting with qualifications...</p>
                              <a href="announcement-detail.html" class="dept-details-butt">Details</a>
                              <div class="vspacer"></div>
                          </div>
                          <img src="<?php echo base_url()?>assets_public/images/dep-dummy.jpg" class="img-responsive dept-author-img-desk col-md-4" alt="">
                          <div class="dept-content pull-left col-md-7 col-lg-8">
                              <div class="dept-title pull-left">Garuda Indonesia requires iPad Mounting Provider</div> 
                              <p>In order to support and enhance our operational safety level, PT Garuda Indonesia (Persero) Tbk (“Garuda”) is looking for a qualified company to provide iPad Mounting with qualifications...</p>
                              <a href="announcement-detail.html" class="dept-details-butt">Details</a>
                              <div class="vspacer"></div>
                          </div>
                          <img src="<?php echo base_url()?>assets_public/images/dep-dummy.jpg" class="img-responsive dept-author-img-desk col-md-4" alt="">
                          <div class="dept-content pull-left col-md-7 col-lg-8">
                              <div class="dept-title pull-left">Garuda Indonesia requires iPad Mounting Provider</div> 
                              <p>In order to support and enhance our operational safety level, PT Garuda Indonesia (Persero) Tbk (“Garuda”) is looking for a qualified company to provide iPad Mounting with qualifications...</p>
                              <a href="announcement-detail.html" class="dept-details-butt">Details</a>
                              <div class="vspacer"></div>
                          </div>
                      </div>
                      <a href="announcement.html" class="dept-details-butt posts-showall announcement-custom" style="margin:2% 0 2% 35%;">Show All</a>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<section class="">
    <div class="about-intro-wrap pull-left">
        <a name="faq"><span></a>
        <div class="head" style="margin-top: 85px;padding-left:0;padding-right:0;">
            <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;">FAQS</h1>
            <a href="#faq" class="next-section about animate2">
                <i class="fa fa-angle-double-down"></i>
            </a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 faq-tabs-wrap wow fadeInUp animated" data-wow-delay="0.5s" data-wow-offset="200">
                    <div class="tabbable tabs-left">
                        <ul class="nav nav-tabs col-md-4 col-sm-4 col-xs-5">
                          <li class="active"><a href="#a" data-toggle="tab"><span class="faq-ques">Bagaimana Rekanan dapat mengikuti proses pengadaan Garuda?</span><i class="right-arr"></i></a></li>
                          <li><a href="#b" data-toggle="tab"><span class="faq-ques">Dimana Rekanan dapat mengetahui kebutuhan pengadaan Garuda?</span><i class="right-arr"></i></a></li>
                          <li><a href="#c" data-toggle="tab"><span class="faq-ques">Mengapa Calon Rekanan tidak bisa menjadi rekanan Garuda?</span><i class="right-arr"></i></a></li>
                          <li><a href="#d" data-toggle="tab"><span class="faq-ques">Siapa saja yang dapat menjadi rekanan Garuda?</span><i class="right-arr"></i></a></li>
                          <li><a href="#e" data-toggle="tab"><span class="faq-ques">Apa persyaratan untuk menjadi rekanan Garuda?</span><i class="right-arr"></i></a></li>
                      </ul>

                      <div class="tab-content col-md-8 col-sm-8 col-xs-7 pull-right">
                        <div class="tab-pane active" id="a">
                            <div class="dept-title-tabs">Bagaimana Rekanan dapat mengikuti proses pengadaan Garuda?</div>
                            <p>Setiap rekanan yang ingin mengikuti proses pengadaan di PT Garuda Indonesia harus melakukan registrasi terlebih dahulu dengan mengirimkan data profile perusahaan dan kelengkapan bukti dokumen sesuai dengan bidang usahanya.</p>
                        </div>

                        <div class="tab-pane" id="b">
                            <div class="dept-title-tabs">Dimana Rekanan dapat mengetahui kebutuhan pengadaan Garuda?</div>
                            <p>Kebutuhan pengadaan Garuda dapat diketahui dari pengumuman web pengadaan Garuda atau akan diberitahukan melalui undangan ke masing-masing alamat peserta.</p>
                        </div>

                        <div class="tab-pane" id="c">
                            <div class="dept-title-tabs">Mengapa Calon Rekanan tidak bisa menjadi rekanan Garuda?</div>
                            <p>Beberapa alasan mengapa perusahaan yang meregister tidak bisa menjadi rekanan Garuda antara lain :</p>
                            <p>Pada saat tertentu Garuda belum memerlukan komoditi tertentu, sehingga calon rekanan dengan bidang usaha yang bersangkutan belum dipertimbangkan untuk menjadi rekanan<br>
                                Jumlah rekanan yang terdaftar di Garuda pada saat tertentu sudah mencukupi untuk memenuhi kebutuhan Garuda<br>
                                Persyaratan untuk menjadi rekanan Garuda belum dapat dipenuhi atau oleh calon rekanan<br>
                                Tidak sesuai dengan kebijakan pengadaan<br>
                                Pernah dan atau sedang ada permasalahan yang menyangkut kinerja yang tidak memuaskan dengan pengadaan Garuda sebelumnya<br>
                                Berdasarkan penilaian (assessment) atau referensi, bahwa calon rekanan yang bersangkutan mempunyai kinerja yang tidak baik atau masih perlu dipertimbangkan untuk dapat menjadi rekanan.</p>
                            </div>

                            <div class="tab-pane" id="d">
                                <div class="dept-title-tabs">Siapa saja yang dapat menjadi rekanan Garuda?</div>
                                <p>Setiap perusahaan dengan bidang usaha yang sesuai dengan kebutuhan Garuda dan mempunyai kinerja yang baik dapat menjadi rekanan Garuda. Dalam memilih rekanannya, kebijakan Garuda lebih menitikberatkan pengadaan secara langsung ke sumber penghasil barang/jasa, seperti pabrikan atau distributor resmi dan menghindari pengadaan barang/jasa melalui perantara yang tidak memberikan nilai tambah.</p>
                            </div>

                            <div class="tab-pane" id="e">
                                <div class="dept-title-tabs">Apa persyaratan untuk menjadi rekanan Garuda?</div>
                                <p>Persyaratan untuk menjadi rekanan Garuda antara lain:</p>
                                <p>Menunjukkan bukti otentik sebagai badan usaha yang jelas di bidang usahanya<br>Mempunyai NPWP dan PKP yang masih berlaku untuk perusahaan dalam negeri atau certificate of domicile untuk perusahaan asing.<br>
                                    Tidak sedang terlibat dalam kasus pelanggaran hukum, termasuk tetapi tidak terbatas pada palanggaran hukum pidana, perdata, perpajakan dan atau permasalahan lain<br>
                                    Memenuhi persyaratan administrasi dan teknis yang diperlukan oleh Garuda.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="complete-content content-footer-space">
        <a name="contact"><span></a>
        <div class="about-intro-wrap pull-left">
            <div class="head" style="margin-top: 85px;">
                <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;">CONTACT US</h1>
                <a href="#contact" class="next-section about animate2">
                    <i class="fa fa-angle-double-down"></i>
                </a>
            </div>
            <div class="container" style="padding-top: 30px;">
                <div class="row">
                    <div class="col-xs-12 col-lg-12  col-sm-12 col-md-12 pull-left contact2-wrap">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 no-pad wow fadeInLeft" data-wow-delay="0.5s" data-wow-offset="100">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="side-bar-contact">
                                    <div class="form-title-text wow fadeInRight dept-title-tabs" data-wow-delay="0.5s" data-wow-offset="100">Contact</div>
                                    <ul class="contact-page-list wow fadeInRight" data-wow-delay="0.5s" data-wow-offset="100">
                                        <li>
                                            <i class="icon-globe contact-side-icon iside-icon-contact"></i>
                                            <span class="contact-side-txt">PT. Garuda Indonesia<br/>
                                                Business Support / JKTIBGA<br/>
                                                Lt. Dasar, Gedung Manajemen Garuda<br/>
                                                Bandara Soekarno-Hatta, Jakarta<br/>
                                            </span>
                                        </li>
                                        <li>
                                            <i class="icon-mail contact-side-icon"></i>
                                            <span class="contact-side-txt">Email: <a href="mailto:business-support@garuda-indonesia.com">business-support@garuda-indonesia.com</a></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <form class="contact2-page-form col-lg-8 col-sm-12 col-md-8 col-xs-12 no-pad contact-v2" id="contactForm">
                                <iframe style="width:100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.9838043279096!2d106.64042241419267!3d-6.132877961835144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a02682025eb33%3A0x840e34d171919f67!2sGaruda+Indonesia+City+Center!5e0!3m2!1sid!2sid!4v1464577237176" height="260" frameborder="0" style="border:0" allowfullscreen></iframe> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="complete-content content-footer-space">
        <div class="about-intro-wrap pull-left">
            <div class="head">
                <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;">LOGIN</h1>
                <a href="#login" class="next-section about animate2">
                    <i class="fa fa-angle-double-down"></i>
                </a>
            </div>
            <a name="login"><span></a>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInRight animated" data-animate="fadeInLeft" style="">
                        <form id="contact-form">
                            <h1>Member Login</h1>
                            <div class="text-fields">
                                <div class="float-input">
                                    <input name="username" id="firstname" type="text" placeholder="Username">
                                    <span><i class="fa fa-user"></i></span>
                                </div>
                                <div class="float-input">
                                    <input name="password" id="password" type="password" placeholder="Password">
                                    <span><i class="fa fa-lock"></i></span>
                                </div>

                                <input type="submit" id="submit_login" class="main-button" value="Login">
                            </div>
                        </form>
                    </div>

                    <div class="col-md-6 wow fadeInRight animated" data-animate="fadeInLeft" style="">
                        <form id="contact-form">
                            <h1>Registration and Vendor Policy</h1>
                            <div class="text-fields">
                                <p class="first-line">How can I register my Company as a Vendor?</p>
                                <p>You can register your company by fill the form on <a href="#">this link</a></p>
                                <br>
                                <p class="first-line">How can I find out which policy is being applied to Vendor?</p>
                                <p>You can read the following Vendor Policies by clicking <a href="#">this link</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>
</div>
</div>
</div>
<a href="javascript:;" class="page-quick-sidebar-toggler">
    <i class="icon-login"></i>
</a>
<div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
    <div class="page-quick-sidebar">
    </div>
</div>
</div>