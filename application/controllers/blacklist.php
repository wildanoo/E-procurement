<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blacklist extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('m_vendor','m_announce','m_redlist','m_blacklist','m_user'));	islogged_in();
		$this->permit = new stdClass();

		$this->permit= $this->crud->get_permissions('5');

	}

	function index()
	{
		if ($this->m_blacklist->authenticate()) { 
			$this->session->set_userdata('feature', 'blacklist');
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			$data['view'] = "vendor/blacklist/home";
			$this->load->view('layout/template',$data);
		} 
		else 
		{ 
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/');
		}
	}

	function browse()
	{

		$search_vals = array('start_date'=>'','end_date'=>'','search_term'=>'', 'id_cat'=> '');
		$this->session->unset_userdata($search_vals);
		$this->session->set_userdata('start_date','');
		$this->session->set_userdata('end_date','');
		$this->session->set_userdata('id_cat','');
		$this->session->set_userdata('term','');
		$user_id = $this->tank_auth->get_user_id();

		$table 	     = "vendor_blacklist a";
		$select 	 = "a.id AS id_blacklist, a.id_vendor, a.start_date,a.remark,a.approval AS approv,a.status AS stt, a.doc_name, a.doc_type, b.* ";
		$order 		 = array('field'=>'a.start_date','order'=>'DESC');
		$joins[0][0] = 'vendor b';
		$joins[0][1] = 'a.id_vendor = b.id';
		$joins[0][2] = 'left';
		/*$joins[1][0] = 'vendor_category c';
		$joins[1][1] = 'c.id_vendor = b.id';
		$joins[1][2] = 'left';*/

		$where = 'a.approval <= 4 ' ;
		if($this->permit->smibk ) $where = 'a.approval >=2 ' ;
		if($this->permit->vpib  ) $where = 'a.approval >=3 ' ;

		$page       = $this->uri->segment(3);
		$per_page   = 10;
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table,""); //print_r($where);
		$set_config = array('base_url'=>base_url().'blacklist/browse','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();

		$order 			     = array('field'=>'a.created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;

		$browse = $this->crud->browse_join_with_paging("",$table,"","","true",$select,$joins,$where,$order,$per_page,$offset,"");
		
		$select2 	 = "a.id AS idcat, a.category, b.id AS idsub, b.subcategory";
		$order2 	 = array('field'=>'a.id','order'=>'ASC');
		$joins2 	 = array();
		$joins2[0][0]= 'category a';
		$joins2[0][1]= 'a.id = b.id_cat';
		$joins2[0][2]= 'left';

		$category = $this->crud->browse_join($db="","subcategory b","a.status","1","true",$select2,$joins2,$where="",$order2,$group_by="",$like="");
		
		$data['title']    = 'Blacklist';
		$data['browse']   = $browse;
		$data['category'] = $category;

		$data['view'] = "vendor/blacklist/browse";
		$data['page']   = $page; 			

		if($this->input->is_ajax_request())
		{
			$this->load->view($data['view'], $data);
 			// print_r($this->permit);
 			// print_r($this->session->userdata('permissions'));
		}
		else
		{
			$this->load->view('layout/template',$data);
 			// print_r($this->permit);
		}

	}

	function browse_logs()
	{
		$user_id = $this->tank_auth->get_user_id();
		$idx 	= $this->input->post('idx');
		$ven_id = $this->input->post('idv')?$this->input->post('idv'):$this->session->userdata('ven_id');
		
		$this->session->set_userdata( array('ven_id'=>$ven_id) );
		$table 	     = "vendor_logs a";
		$select 	 = "*,a.created_date as  log_date, (select c.username from users c where c.id=a.user_id) as username, (select d.name from groups d where d.id=a.group_id) as group_name ";
		$order 		 = array('field'=>'a.created_date','order'=>'DESC');
		$joins[0][0] = 'vendor b';
		$joins[0][1] = 'a.id_vendor = b.id';
		$joins[0][2] = 'left';
		if(!empty($ven_id))
		{
			$where = array('a.id_vendor'=>$ven_id);
		}
		$page       = $this->uri->segment(3);
		$per_page   = 10;
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table,$where); //print_r($where);
		$set_config = array('base_url'=>base_url().'blacklist/browse_logs','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();

		$order 			     = array('field'=>'a.created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;

		$browse = $this->crud->browse_join_with_paging("",$table,"","","true",$select,$joins,$where,$order,$per_page,$offset,"");
		$vendor = $this->crud->browse($db="",'vendor','id',$ven_id,false,"vendor_name",$where="","","","");
		$data['title']    = 'Logs ' . $vendor->vendor_name;
		$data['browse']   = $browse;

		$data['view'] = "vendor/blacklist/data_logs";

		if($this->input->is_ajax_request())
		{
			$this->load->view($data['view'], $data);
		}
		else
		{
			$this->load->view('layout/template',$data);
		}

	}

	function form_add()
	{
		$id = $this->uri->segment(3);
		if(!empty($id))
		{
			$table 	     = "vendor_blacklist a";
			$select 	 = "a.id AS id_blacklist,a.start_date,a.remark, a.doc_name,a.doc_type, b.* ";
			$order 		 = array('field'=>'a.start_date','order'=>'DESC');
			$joins[0][0] = 'vendor b';
			$joins[0][1] = 'a.id_vendor = b.id';
			$joins[0][2] = 'left';

			$blacklist = $this->crud->browse_join("",$table,"a.id",$id,"false",$select,$joins,$where,"","","");

			$data = array('blacklist' => $blacklist);
			$data['title']    = 'Edit Item Vendor Blacklist';		}
			else
			{
				$data['title']    = 'Add Vendor To Blacklist';
				$select2 	 = "a.id AS idven, a.vendor_name";
				$joins[0][0] = 'vendor_blacklist b';
				$joins[0][1] = 'a.id = b.id_vendor';
				$joins[0][2] = 'left';
				$where = 'b.id_vendor is null';

				$data['vendor'] = $this->crud->browse_join("","vendor a","a.vendor_type","avl","true",$select2,$joins,$where,"","","");
			//print_r($data['vendor']);
			//exit();
			}


		//$data['vendor'] = $this->crud->browse($db="",'vendor','vendor_type','avl',true,"",$where="","","","");
			$this->load->view('vendor/blacklist/form_create',$data);
		}


		function create()
		{	
			$id 		 = $this->input->post('blacklist_id');
			$upload_file = $_FILES['userfile']['name'];
			$file_type 	 = $this->input->post('doc_type');
			$filename 	 = $this->input->post('doc_name');
			$venID 		 = $this->input->post('ven_id');
			$namefile	 = $this->input->post('namefile');
			$deletedfile = $this->input->post('deleted_file');
			$userID      = $this->tank_auth->get_user_id();
			$curr_date 	 = date('Y-m-d H:i:s');
			$path		 = './uploads/blacklist_attachment/'; 
			$filename    = '';
			$file_type 	 = '';
			$flag 		 = 0 ;
			if($namefile)
			{
				foreach ($namefile as $file) {
					$attac     = explode('.',$file);
					if($flag==0){
						$filename .= $attac[0];
						$file_type.= $attac[1];
						$flag=1 ;
					}else{
						$filename .= ','. $attac[0];
						$file_type.= ','. $attac[1];
					}	
				}
			}

			if ($_FILES['userfile']['size']!=0)
				{ 	$i 		= 0 ;
					$datax  = $this->m_vendor->arrange_files($_FILES); 
					foreach($datax as $key3=>$file)
						{	$i += 1 ;
							$_FILES['userfile'] = $file;
							$nameFile = explode(".",$file['name']); 
							$ext 	 = $nameFile[1] ;
							if($flag==0){
								$filename .= 'blacklist_'.$venID.date('YmdHis').$i;
								$file_type.= $ext;
								$flag=1 ;
							}else{
								$filename .= ',blacklist_'.$venID.date('YmdHis').$i;
								$file_type.= ','. $ext;
							}

							$filenamex= 'blacklist_'.$venID.date('YmdHis').$i.'.'.$ext ;

							$files[]  = $filenamex;
							$config   = $this->m_vendor->set_config($filenamex,$path,$ext); 		 
							$this->load->library('upload', $config);
							$this->upload->initialize($config); 

							if ( ! $this->upload->do_upload()) exit();
						}
					} 

					$data 	= array('id'			=> null,
						'id_vendor'		=> $venID,
						'start_date'	=> $this->input->post('start_date'),
						'remark'		=> $this->input->post('remark'),
						'doc_name'		=> $filename,
						'doc_type'		=> $file_type,
						'approval'		=> '0',
						'status'		=> 'Wait',
						'created_id'	=> $userID,
						'created_date'	=> $curr_date,
						'last_updated'	=> $curr_date);
					if($deletedfile)
					{
						foreach ($deletedfile as $filez) {
							$path = $path.$filez;
							@unlink($path);
						}
					}

					if(empty($id))
					{
						$lastID = $this->crud->insert("","vendor_blacklist",$data);
						$logs 		= array('id'	=> null,
							'id_vendor'		=> $venID,
							'user_id'		=> $userID,
							'description'	=> 'add vendor to blacklist',
							'reason'		=> $this->input->post('remark'),
							'status'		=> 1,
							'created_date'	=> $curr_date,
							'action'		=> 'blacklist' );
						$this->m_blacklist->record_activity('vendor_logs', $logs);
						$this->sendEmailToApproval($venID, "sm_ibs",$lastID);
					}
					else
					{
						unset($data['id']);
						$this->crud->update("","vendor_blacklist","id",$id, $data);
						$logs 		= array('id'	=> null,
							'id_vendor'		=> $venID,
							'user_id'		=> $userID,
							'description'	=> 'update data blacklist vendor',
							'reason'		=> $this->input->post('remark'),
							'status'		=> 1,
							'created_date'	=> $curr_date,
							'action'		=> 'blacklist' );
						$this->m_blacklist->record_activity('vendor_logs', $logs);
						$this->sendEmailToApproval($venID, "sm_ibs",$id);
					}
					redirect("blacklist");

				}





				function view_detail_blacklist()
				{	$id = $this->uri->segment(3);
					if(!empty($id))
					{
						$table 	    = "vendor_blacklist a";
						$select 	= "a.id AS id_blacklist,a.start_date,a.remark, a.approval AS approv, a.status AS stt, a.doc_name,a.doc_type, b.* ";
						$order 		 = array('field'=>'a.start_date','order'=>'DESC');
						$joins[0][0] = 'vendor b';
						$joins[0][1] = 'a.id_vendor = b.id';
						$joins[0][2] = 'left';

						$blacklist = $this->crud->browse_join("",$table,"a.id",$id,"false",$select,$joins,$where,"","","");

						$data = array('blacklist' => $blacklist);
						$data['title']    = 'Detail Vendor Blacklist';
						$data['vendor'] = $this->crud->browse($db="",'vendor','vendor_type','avl',true,"",$where="","","","");
						$this->load->view('vendor/blacklist/form_create',$data);
					}
					else
					{
						redirect("blacklist");
						exit();
					}


				}

				function view_detail_vendor()
				{	$venID  = $this->uri->segment(3);
					if(is_numeric($venID))
					{	
						$this->session->set_userdata('venID',$venID);
						redirect("vendor/detail");
					}
					else
					{
						redirect(base_url());
					}
				}




				function reject()
				{
					$id = $this->input->post('id');
					$userID = $this->tank_auth->get_user_id();
					$vendor = $this->crud->browse("", 'vendor_blacklist', 'id', $id, 'false', 'id_vendor','','','','');
					$vendor = $vendor->id_vendor;
					if(!empty($id))
					{	
						$curr_date 	= date('Y-m-d H:i:s');

						if($this->permit->smibk)
						{
							$data = array(  'status'		=> 'Rejected',
								'approval'		=> '1',
								'last_updated'	=> $curr_date);
							$logs = array('id'	=> null,
								'id_vendor'		=> $vendor,
								'user_id'		=> $userID,
								'description'	=> 'Rejected From Blacklist',
								'reason'		=> '',
								'status'		=> 1,
								'created_date'	=> $curr_date,
								'action'		=> 'blacklist' );
						}
						elseif($this->permit->vpib)
						{
							$data = array(  'status'		=> 'Rejected',
								'approval'		=> '2',
								'last_updated'	=> $curr_date);
							$logs = array('id'		=> null,
								'id_vendor'		=> $vendor,
								'user_id'		=> $userID,
								'description'	=> 'Rejected From Blacklist',
								'reason'		=> '',
								'status'		=> 1,
								'created_date'	=> $curr_date,
								'action'		=> 'blacklist' );
						}
						else
						{
							$data = array(  'status'		=> 'Rejected',
								'approval'		=> '0',
								'last_updated'	=> $curr_date);
							$logs = array('id'		=> null,
								'id_vendor'		=> $vendor,
								'user_id'		=> $userID,
								'description'	=> 'Rejected From Blacklist',
								'reason'		=> '',
								'status'		=> 1,
								'created_date'	=> $curr_date,
								'action'		=> 'blacklist' );
						}
						$this->m_blacklist->record_activity('vendor_logs', $logs);
						$this->crud->update("","vendor_blacklist","id",$id, $data);
						$this->sendEmailToADMSourcing($vendor,'rejected',$id);
					}
				}





				function remove()
				{
					$id = $this->input->post('id');
					$vendor = $this->crud->browse("", 'vendor_blacklist', 'id', $id, 'false', '','','','','');
					$userID = $this->tank_auth->get_user_id();
					$curr_date 	= date('Y-m-d H:i:s');

					if(!empty($id))
					{
						$logs = array('id'		=> null,
							'id_vendor'		=> $vendor->id_vendor,
							'user_id'		=> $userID,
							'description'	=> 'Cancel Blacklist',
							'reason'		=> '',
							'status'		=> 1,
							'created_date'	=> $curr_date,
							'action'		=> 'blacklist' );
						$this->m_blacklist->record_activity('vendor_logs', $logs);

						$path = "./uploads/blacklist_attachment/".$vendor->doc_name.'.'.$vendor->doc_type; 		
						if(file_exists($path)) unlink($path); 
					}

					$this->crud->delete("","vendor_blacklist","id",$id);
				}



				function approv()
				{	$id = $this->input->post('id');
				$userID = $this->tank_auth->get_user_id();
				$vendor = $this->crud->browse("", "vendor_blacklist", "id", $id, "false", "id_vendor","","","","");
				$id_vendor = $vendor->id_vendor;
				if(!empty($id))
				{	
					$curr_date 	= date('Y-m-d H:i:s');

					if($this->permit->smibk)
					{
						$data = array(  'status'		=> 'Wait',
							'approval'		=> '2',
							'last_updated'	=> $curr_date);
						$logs = array('id'	=> null,
							'id_vendor'		=> $id_vendor,
							'user_id'		=> $userID,
							'description'	=> 'Approve Blacklist step 2',
							'reason'		=> '',
							'status'		=> 1,
							'created_date'	=> $curr_date,
							'action'		=> 'blacklist' );

						$this->sendEmailToApproval($id_vendor, "vp_ib",$id);
					}
					elseif($this->permit->vpib)
					{
						$data = array(  'status'		=> 'Approved',
							'approval'		=> '3',
							'last_updated'	=> $curr_date);
						$data2= array(  'status'		=> 'blacklist',
							'last_updated'	=> $curr_date);
						$logs = array('id'	=> null,
							'id_vendor'		=> $id_vendor,
							'user_id'		=> $userID,
							'description'	=> 'Approve Blacklist step 3',
							'reason'		=> '',
							'status'		=> 1,
							'created_date'	=> $curr_date,
							'action'		=> 'blacklist' );
						$this->crud->update("","vendor","id",$id_vendor, $data2);
						$this->sendEmailToADMSourcing($id_vendor,'approved',$id);
						$this->sendEmailToVendor($id_vendor);

					}
					else
					{
						$data = array(  'status'		=> 'Wait',
							'approval'		=> '1',
							'last_updated'	=> $curr_date);
						$logs = array('id'	=> null,
							'id_vendor'		=> $id_vendor,
							'user_id'		=> $userID,
							'description'	=> 'Approve Blacklist step 1',
							'reason'		=> '',
							'status'		=> 1,
							'created_date'	=> $curr_date,
							'action'		=> 'blacklist' );

						$this->sendEmailToApproval($id_vendor, "sm_ibk",$id);
					}
					$this->m_blacklist->record_activity('vendor_logs', $logs);
					$this->crud->update("","vendor_blacklist","id",$id, $data);
				}
			}


			function search()
			{
				$start_date = ($this->input->post('start_date') ) ? $this->input->post('start_date') : $this->session->userdata('start_date');
				$end_date = ($this->input->post('end_date'))? $this->input->post('end_date') : $this->session->userdata('end_date');

				$term  = ($this->input->post('search_term'))? $this->input->post('search_term') : $this->session->userdata('term');

				$subcategory  = ($this->input->post('id_cat'))? $this->input->post('id_cat') : $this->session->userdata('id_cat');


				$search_vals = array('start_date' => $start_date, 'end_date' => $end_date, 'term'=> $term, 'idcat'=> $subcategory);
				$this->session->set_userdata($search_vals);
				$this->session->set_userdata('start_date',$start_date);
				$this->session->set_userdata('end_date',$end_date);
				$this->session->set_userdata('id_cat',$subcategory);
				$this->session->set_userdata('term',$term);

				$table 	     = "vendor_blacklist a";
				$select 	 = "a.id AS id_blacklist,a.start_date,a.remark,a.approval AS approv,a.status AS stt, b.* ";
				$order 		 = array('field'=>'a.start_date','order'=>'DESC');
				$joins[0][0] = 'vendor b';
				$joins[0][1] = 'a.id_vendor = b.id';
				$joins[0][2] = 'left';
				$joins[1][0] = '(select id_vendor, id_subcat from vendor_category where id_subcat='.$subcategory.' group by id_vendor) c';
				if($subcategory=='' || $subcategory==null)
					$joins[1][0] = '(select id_vendor, id_subcat from vendor_category group by id_vendor) c';
				$joins[1][1] = 'a.id_vendor = c.id_vendor';
				$joins[1][2] = 'left';

				$where = 'a.approval <= 4 ' ;
				if($this->permit->smibk ) $where = 'a.approval >=2 ' ;
				if($this->permit->vpib  ) $where = 'a.approval >=3 ' ;

				$blackID  = $this->session->userdata('blackID');

				if(empty($blackID)){
					if(!empty($start_date) && !empty($end_date))
					{
						$where .= "and a.start_date between '$start_date' and '$end_date' ";
						if(!empty($term))
						{
							$where .= "and b.vendor_name like '%$term%' or b.vendor_num like '%$term%' ";
						}
						if($subcategory=='all' || $subcategory==null){$where .= "";}else{$where.=" and c.id_subcat=".$subcategory;}
					}

					if(empty($start_date) && empty($end_date) && !empty($term))
					{
						$where .= "and b.vendor_name like '%$term%' or b.vendor_num like '%$term%' ";
						if($subcategory=='all' || $subcategory==null){$where .= "";}else{$where.=" and c.id_subcat=".$subcategory;}
					}
				}else{
					$where .= " AND a.id = $blackID";
				}


				$page       = $this->uri->segment(3);
				$per_page   = 10;
				$offset     = $this->crud->set_offset($page,$per_page);
				$total_rows = count($this->crud->browse_join("",$table,"c.id_subcat",$subcategory,"true",$select,$joins,$where,$order,"",""));
				$set_config = array('base_url'=>base_url().'blacklist/search','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
				$config     = $this->crud->set_config($set_config);

				$this->load->library('pagination');
				$this->pagination->initialize($config);
				$paging = $this->pagination->create_links();

				$order 			     = array('field'=>'a.created_date','order'=>'DESC');
				$data['pagination']  = $paging;
				$data['num']         = $offset;
				if($subcategory=='all') $subcategory = '';
				$browse = $this->crud->browse_join_with_paging("",$table,"c.id_subcat",$subcategory,"true",$select,$joins,$where,$order,$per_page,$offset,"");

				$select2 	 = "a.id AS idcat, a.category, b.id AS idsub, b.subcategory";
				$order2 	 = array('field'=>'a.id','order'=>'ASC');
				$joins2[0][0]= 'category a';
				$joins2[0][1]= 'a.id = b.id_cat';
				$joins2[0][2]= 'left';


				$data['title']    = 'Blacklist';
				$data['browse']   = $browse;
				$data['category'] = $this->crud->browse_join($db="","subcategory b","a.status","1","true",$select2,$joins2,"",$order2,$group_by="",$like="");
				$data['id_subcat'] = $subcategory ;

				$data['view'] = "vendor/blacklist/browse";


				$this->load->view('layout/template',$data);

			}


			function subcategory()
			{
				$start_date = ($this->input->post('start_date') ) ? $this->input->post('start_date') : $this->session->userdata('start_date');
				$end_date = ($this->input->post('end_date'))? $this->input->post('end_date') : $this->session->userdata('end_date');

		//$term  = ($this->input->post('search_term'))? $this->input->post('search_term') : $this->session->userdata('term');
				$this->session->set_userdata('term','');

				$subcategory  = ($this->input->post('id_cat'))? $this->input->post('id_cat') : $this->session->userdata('id_cat');

				$search_vals = array('start_date' => $start_date, 'end_date' => $end_date, 'term'=> $term, 'id_cat'=> $subcategory);
				$this->session->set_userdata('start_date',$start_date);
				$this->session->set_userdata('end_date',$end_date);
				$this->session->set_userdata('id_cat',$subcategory);
				$this->session->set_userdata('term',$term);
				$this->session->set_userdata($search_vals);
				if($subcategory=='all')
				{
					redirect(base_url('blacklist'));
				}
				else
				{
					redirect(base_url('blacklist/search'));
				}

			}



			function export_excel()
			{
				$ck 		= $this->input->post('mycheckbox');
				$filter 	= implode(',', $ck);
				$table 	    = "vendor_blacklist a";
				$select     = "a.id AS id_blacklist,a.start_date,a.remark,a.status AS stt, b.* ";
				$order 		= array('field'=>'a.created_date','order'=>'DESC');
				$joins[0][0]= 'vendor b';
				$joins[0][1]= 'a.id_vendor = b.id';
				$joins[0][2]= 'left';

				$where 	= "a.id IN($filter)";

		//$order  = array('field'=>'a.created_date','order'=>'DESC');

				if(!empty($ck))
				{
					$data['blacklist'] = $this->crud->browse_join("",$table,"","","true",$select,$joins,$where,$order,"","");
					$this->load->view('vendor/blacklist/excel', $data );
				}
				else
				{
					redirect("blacklist");
				}

			}

			function download()
			{ 
				$id = $this->uri->segment(3);
				$code= $this->uri->segment(4);
				$code1 = md5($id);
				$id = substr($id,2,10);
				$path  = 'uploads/blacklist_attachment/';
				$browse = $this->crud->browse("","vendor_blacklist","id",$id,"false","doc_name,doc_type");
				$attac 	= explode(',',$browse->doc_name); $num1 = count($attac) ;
				$tipe 	= explode(',',$browse->doc_type); $num2 = count($tipe) ;
				if($num1==$num2 && $code==$code1)
				{
					for ($i = 0; $i < $num1; $i++)
					{
						$data[] = $path.$attac[$i].'.'.$tipe[$i];
					}
				}

				$filename = 'filename.zip';
				unlink($path.$filename);
				$this->m_blacklist->create_zip($data,'uploads/blacklist_attachment/'.$filename);

				$this->load->helper('download');
				$data2 = file_get_contents("uploads/blacklist_attachment/".$filename);  
				force_download($filename, $data2); 
			}

			private function sendEmailToApproval($venID, $TogroupID, $lastID="") 
			{   /*  send Email To Approval for approve blacklist vendor  */

				$select2 	 = "v.register_num, v.vendor_name, b.remark";
				$joins2[0][0]= 'vendor v';
				$joins2[0][1]= 'b.id_vendor = v.id';
				$joins2[0][2]= 'left';
				$vendor  	 = $this->crud->browse_join("","vendor_blacklist b","b.id_vendor",$venID,"false",$select2,$joins2,"","","","");

				$select2 	= "c.category AS cate_name";
				$joins[0][0]= 'category c';
				$joins[0][1]= 'v.id_category = c.id';
				$joins[0][2]= 'left';
				$browsecat  = $this->crud->browse_join("","vendor_category v","v.id_vendor",$venID,"true",$select2,$joins,"","","v.id_category","");

				$unt_name 	= $this->crud->browse("","groups","code",$TogroupID,"false","name")->name;
				$to2 		= $this->m_user->get_userdata_group($TogroupID);

				$subject 	=  "Permintaan Approval Blacklist - E-procurement Garuda Indonesia";
				$user_id 	= $this->tank_auth->get_user_id();
				$curr_date  = date('Y-m-d H:i:s');

				foreach ($to2 as $value)
				{	
					$user    =  $this->users->get_user_by_email($value->email);
					if($user){			 			
						$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'blacklist_vend','param'=>$lastID);							 
						$link    = $this->m_vendor->create_session_link((object) $require);
						$to 		  = $value->email ;

						$param  = array('toname'	 => $value->username,
							'unit'		 => $unt_name,
							'vendor_name'=> $vendor->vendor_name,
							'venID'  	 => $vendor->register_num,
							'category'   => $browsecat,
							'reason'  	 => $vendor->remark,
							'from'  	 => $this->tank_auth->get_username(),
							'link'		 => $link);

						$param['view']  = NOTIF_PATH."EPCGA022";
						$message 		=  $this->load->view(NOTIF_TMPL,$param,true);

						if(filter_var($to, FILTER_VALIDATE_EMAIL) && $to!='' )
							{ $this->crud->sendMail($to,$subject,$message); }
					}
				}
			}


			private function sendEmailToADMSourcing($venID,$notifications,$lastID="") 
			{   /*  send Email To ADMSourcing   */

				$select2 	 = "v.register_num, v.vendor_name, b.remark";
				$joins2[0][0]= 'vendor v';
				$joins2[0][1]= 'b.id_vendor = v.id';
				$joins2[0][2]= 'left';
				$vendor  	 = $this->crud->browse_join("","vendor_blacklist b","b.id_vendor",$venID,"false",$select2,$joins2,"","","","");

				$select2 	= "c.category AS cate_name";
				$joins[0][0]= 'category c';
				$joins[0][1]= 'v.id_category = c.id';
				$joins[0][2]= 'left';
				$browsecat  = $this->crud->browse_join("","vendor_category v","v.id_vendor",$venID,"true",$select2,$joins,"","","v.id_category","");

				$unt_name 	= $this->crud->browse("","groups","code","as","false","name")->name;
				$to2 		= $this->m_user->get_userdata_group("as");

				if($notifications=='approved'){ $form = '2'; $notif='Approve'; }else{ $form = '3'; $notif='Reject'; }
				$subject 	= "Vendor Blacklist Telah Di ".$notif." E-procurement Garuda Indonesia";

				if($notifications=='approved')
					{$strmsg .= '<span style="color:red">Telah disetujui untuk menjadi blacklist sampai waktu yang tidak ditentukan.</span>';}
				else{$strmsg .= '<span style="color:red">Dimohon untuk divaluasi kembali.</span>';}

				foreach ($to2 as $value)
				{	
					$user    =  $this->users->get_user_by_email($value->email);
					if($user){			 			
						$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'blacklist_vend','param'=>$lastID);							 
						$link    = $this->m_vendor->create_session_link((object) $require);

						$to 	= $value->email ;

						$param  = array('toname'	 => $value->username,
							'admin'		 => $unt_name,
							'vendor_name'=> $vendor->vendor_name,
							'venID'  	 => $vendor->register_num,
							'category'   => $browsecat,
							'reason'  	 => $vendor->remark,
							'strmsg'	 => $strmsg,
							'link'		 => $link);

						if($notifications=='approved'){ 
							$param['view']  = NOTIF_PATH."EPCGA023";
							$message 		=  $this->load->view(NOTIF_TMPL,$param,true);
						}else{ 
							$param['view']  = NOTIF_PATH."EPCGA024";
							$message 		=  $this->load->view(NOTIF_TMPL,$param,true);
						}

						if(filter_var($to, FILTER_VALIDATE_EMAIL) && $to!='' )
							{ $this->crud->sendMail($to,$subject,$message); }
					}
				}
			}


			private function sendEmailToVendor($venID) 
			{   /*  send Email To Vendor   */

				$select2 	 = "v.vendor_name, v.vendor_address, v.email, b.remark, b.doc_name, b.doc_type";
				$joins2[0][0]= 'vendor v';
				$joins2[0][1]= 'b.id_vendor = v.id';
				$joins2[0][2]= 'left';
				$vendor  	 = $this->crud->browse_join("","vendor_blacklist b","b.id_vendor",$venID,"false",$select2,$joins2,"","","","");
				$subject 	 =  "Notifications Blacklist E-procurement Garuda Indonesia";	

				if(!empty($vendor->doc_name) && !empty($vendor->doc_type)) { $filename = base_url('attachment/'.$vendor->doc_name.'.'.$vendor->doc_type); }else{ $filename = ''; }

				$param  = array('vendor_name'=> $vendor->vendor_name,
					'address'  	 => $vendor->vendor_address,
					'reason'  	 => $vendor->remark,
					'file'	 	 => $filename);
				$to 	= $vendor->email ;
				$param['view']  = NOTIF_PATH."EPCGA025";
				$message 		=  $this->load->view(NOTIF_TMPL,$param,true);

				if(filter_var($to, FILTER_VALIDATE_EMAIL) && $to!='' )
					{ $this->crud->sendMail($to,$subject,$message); }
			}

			function validate_vendor()
			{
				$venID = $this->input->post('id_ven');
				print $chcek = $this->m_user->is_request_allowed('2', $venID);
			}

		}