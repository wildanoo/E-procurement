<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Content extends Controller {

	function __construct()
	{
		parent::__construct();	
		//$this->load->model('m_category', 'm_category');
		$this->load->model('crud', 'crud');
		$this->load->library('session');
		$this->limit = 5;
	}	
	
	function index(){
		if ($this->tank_auth->is_logged_in()) {
	
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
				
			//$sess_like  = $this->session->flashdata('like');\
			
			$sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
			
				
			$field      = $sess_field != '' ? $sess_field : "type_name";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
				
			if($sess_cat != ''){	$where = array('type_name'=>$sess_cat);
			} else { $where = "";	}
	
			$table 	    = "content_type";
			$page       = $this->uri->segment(3);
			$per_page   = 5;
			$offset     = $this->crud->set_offset($page,$per_page);
			$total_rows = $this->crud->get_total_record("",$table);
			$set_config = array('base_url'=> base_url().'/content/index','total_rows'=>$total_rows,'per_page'=>5,'uri_segment'=>3);
			$config     = $this->crud->set_config($set_config);
				
			$this->load->library('pagination');
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();
	
			$order 			     = array('field'=>'created_date','order'=>'DESC');
			$data['pagination']  = $paging;
			$data['num']         = $offset;
			$select 			 = "id,type_name,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
				
// 			if($sess_like){
// 			$like   = array('col'=>'type_name','field'=>$sess_like);
// 			$browse = $this->crud->browse("",$table." l","","","true",$select,"","","",$like);	} else {
// 			$browse = $this->crud->browse_with_paging("",$table." l","","","true",$select,"",$order,$config['per_page'],$offset); }
	
// 			$data['browse'] = $browse;

			$sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
				
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			$data['browse'] = $browse;
				
			$content = $this->crud->browse("","content_type","","","true","id,type_name");
			if(!$content) $content = array();
			$select = array(''=>'All');
            $options = array();
			foreach($content as $val){ $options[$val->id] = $val->type_name; }
			$data['content'] = $select + $options;
			
			$data['view']	= "content/browse";
			$this->load->view('layout/template',$data);
	
	
		} else {
			$this->session->set_flashdata('message','user not authorized');
			redirect('/auth/login/');
		}
	}
	
	function get_cat_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'type_name','sort'=>'ASC'); $select = "type_name as name";
		$result = $this->crud->autocomplete("","content_type",$select,"type_name",$term,"",$order);
		echo json_encode($result);
	}
	
	function get_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'type_name','sort'=>'ASC'); $select = "type_name as name";
		$result = $this->crud->autocomplete("","content_type",$select,"type_name",$term,"",$order);
		echo json_encode($result);
	}
	
	function set_sess_search(){
		$like = $_POST['search'];
		$this->session->set_flashdata('like',$like);
	}
	
	function set_sess_content(){
	
		$type = $_POST['type_name'];
		if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
		} else { $this->session->unset_userdata('sess_cat'); }
	
		$type = $_POST['field'];
		if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
		} else { $this->session->unset_userdata('sess_field'); }
	
	}
	
	function is_exist(){
		$content	  = $_POST['type_name'];
		$is_exist['1']= !$content ? "false" : "true";
		$msg['1']	  = !$content ? "
				content name required" : "";
	
		if ($is_exist['1']=='true'){
			$where   = array('type_name'=>$content);
			$checked = $this->crud->is_exist("","content_type","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate content name" : "";
	
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg' =>$msg['1']);
	
		echo json_encode($result);
	}
	
	function create(){
		//$init_code  = $this->m_category->category_code("category");
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array('id'=>null,
							'type_name'=>$_POST['type_name'],
							//'status'=>'1',
							'created_id'=>$userID,
							'created_date'=>$curr_date,
							'last_updated'=>$curr_date);
		$id = $this->crud->insert("","content_type",$data);
		$this->session->set_flashdata('message','1 data success insert');
	}
	
	function form_update(){
		$id 				   = $this->uri->segment(3);
		$select 			   = "id,(SELECT id FROM content_type r WHERE r.id=l.content_id) as content_id, subtype_name,created_date,last_updated";
		$def				   = $this->crud->browse("","subcontent_type l","id",$id,"false",$select);
		$data['def']  		   = $def ;
		$content 			   = $this->crud->browse("","content_type","","","true","id,type_name");
		//print_r($content);
		$id   				   = $this->uri->segment(3);
		$select 			   = "id,type_name,created_id,last_updated";
		$data ['def'] 		   = $this->crud->browse ( "", "content_type", "id", $id, "false", $select);
		$this->load->view ('content/form_update', $data );
	}
	
	function form_update1(){
		$id 				   = $this->uri->segment(3);
		$select 			   = "id,init_code,(SELECT id FROM category r WHERE r.id=l.id_cat) as id_cat, subcategory,created_date,last_updated";
		$def				   = $this->crud->browse("","subcategory l","id",$id,"false",$select);
		$data['def']  		   = $def ;
		$category 			   = $this->crud->browse("","category","","","true","id,category");
	
		$id   = $this->uri->segment(3);
		$select = "id,init_code,category,created_id,last_updated";
		$data ['def'] = $this->crud->browse ( "", "category", "id", $id, "false", $select);
		$this->load->view ( 'supplier/category/form_update', $data );
	}
	
	function form_create(){
		$data ['user_id'] = $this->tank_auth->get_user_id ();
		$data ['username'] = $this->tank_auth->get_username ();
		$this->load->view('content/form_create', $data);
	}
	
	function update(){
		$curr_date =  date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data = array('type_name'=>$_POST['type_name'],'last_updated'=>$curr_date);
		$this->crud->update("","content_type","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
	}
	
	function delete(){
	
		$id = $this->uri->segment(3);
		$where = array('content_id'=>$id);
		$checked = $this->crud->is_exist("","subcontent_type","id",$where);
		if ($checked) {
			$this->session->set_flashdata('msg_warning','This data is already in use');
		}
		else {
			$this->crud->delete("","content_type","id",$id);
		}
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('content/','refresh');
	
	}
	
	function subcontent(){
		if ($this->tank_auth->is_logged_in()) {
	
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
	
			$sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
			
				
			$field      = $sess_field != '' ? $sess_field : "subtype_name";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
				
			if($sess_cat != ''){	$where = array('subtype_name'=>$sess_cat);
			} else { $where = "";	}
	
			$table 	    = "subcontent_type";
			$page       = $this->uri->segment(3);
			$per_page   = 5;
			$offset     = $this->crud->set_offset($page,$per_page);
			$total_rows = $this->crud->get_total_record("",$table);
			$set_config = array('base_url'=> base_url().'/content/subcontent','total_rows'=>$total_rows,'per_page'=>5,'uri_segment'=>3);
			$config     = $this->crud->set_config($set_config);
	
			$this->load->library('pagination');
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();
	
			$order 			     = array('field'=>'created_date','order'=>'DESC');
			$data['pagination']  = $paging;
			$data['num']         = $offset;
			$select 			 = "id,(SELECT type_name FROM content_type r WHERE r.id=l.content_id)as content_id,
									subtype_name,(SELECT username FROM users WHERE id=created_id)as created_id,
									document_req_id,created_date,last_updated";
	
// 			if($sess_like){
// 			$like   = array('col'=>'subtype_name','field'=>$sess_like);
// 			$browse = $this->crud->browse("",$table." l","","","true",$select,"","","",$like);	} else {
// 			$browse = $this->crud->browse_with_paging("",$table." l","","","true",$select,"",$order,$config['per_page'],$offset); }
	
// 			$data['browse'] = $browse;
			$sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			$data['browse'] = $browse;
			//print_r($data['browse']);
			$subcontent = $this->crud->browse("","subcontent_type","","","true","id,subtype_name");
			if(!$subcontent) $subcontent = array();
			$select = array(''=>'All');
            $options = array();
			foreach($subcontent as $val){ $options[$val->id] = $val->subtype_name; }
			$data['subcontent'] = $select + $options;
			
			$data['view']	= "subcontent/browse";
			$this->load->view('layout/template',$data);
	
	
		} else {
			$this->session->set_flashdata('message','user not authorized');
			redirect('/auth/login/');
		}
	}
	
	function get_subcontent_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'subtype_name','sort'=>'ASC'); $select = "subtype_name as name";
		$result = $this->crud->autocomplete("","subcontent_type",$select,"subtype_name",$term,"",$order);
		echo json_encode($result);
	}
	
	function set_sess_search_subcontent(){
		$like = $_POST['search'];
		$this->session->set_flashdata('like',$like);
	}
	
	function set_sess_subcontent(){
	
		$type = $_POST['subtype_name'];
		if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
		} else { $this->session->unset_userdata('sess_cat'); }
	
		$type = $_POST['field'];
		if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
		} else { $this->session->unset_userdata('sess_field'); }
	
	}
	
	function is_exist_subcontent(){
		$content	  = $_POST['content_id'];
		$subcontent	  = $_POST['subtype_name'];
		$is_exist['1']= !$content ? "false" : "true";
		$is_exist['2']= !$subcontent ? "false" : "true";
		$msg['1']	  = !$content ? "content name required" : "";
		$msg['2']	  = !$subcontent ? "subcontent name required" : "";
	
		if ($is_exist['1']=='true' && $is_exist['2']=='true'){
			$where   = array('content_id'=>$content, 'subtype_name'=>$subcontent);
			$checked = $this->crud->is_exist("","subcontent_type","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$is_exist['2']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate content name" : "";
			$msg['2']	= $checked ? "duplicate subcontent name" : "";
	
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2']);
	
		echo json_encode($result);
	}
	
	function form_create_subcontent(){
		$content = $this->crud->browse("","content_type","","","true","id,type_name");
		if(!$content) $content = array();
		$select = array(''=>'-- Select --');
        $options = array();
		foreach($content as $val){ $options[$val->id] = $val->type_name; }
		$data['content'] = $select + $options;
		
		$behavior = $this->crud->browse("","behavior","","","true","id,behavior");
		if(!$behavior) $behavior = array();
		$select2 = array(''=>'-- Select --');
		foreach($behavior as $val){ $options2[$val->id] = $val->behavior; }
		$data['behavior'] = $select2 + $options2;
		
		$data['doc']  = $this->crud->browse("","document_req","","","true","id,document_req");
		$this->load->view('subcontent/form_create',$data);
	}
	
	function create_subcontent(){
		$ttl   		  = $_POST['ttl'];
		$subtype  	  = $_POST['subtype_name'];
		$document_req = "";
		$where 	      = array('subtype_name'=>$subtype);
		$is_exist     = $this->crud->is_exist("","subcontent_type","id",$where);
		
		for ($i = 1; $i <= $ttl; $i++) {
			$value = $this->input->post('check'.$i);
			if($value) $doc[] = $value; 
			}
			if($doc){ $document_req = implode(",",$doc); }
			$data = array('id'			 => null,
							'content_id'	=> $_POST['content_id'],
							'subtype_name'	=> $_POST['subtype_name'],
							'behavior_id'	=> $_POST['behavior_id'],
							'document_req_id'	=> $document_req,
							'created_id'	=> $this->tank_auth->get_user_id(),
							'created_date'  => date('Y-m-d H:i:s'),
							'last_updated'  => date('Y-m-d H:i:s'));
		
			if(!$is_exist){
				$this->crud->insert("","subcontent_type",$data); }
					
				$msg1 = "succesfully add new document";
				$msg2 = "duplicate group document";
				$msg  = !$is_exist ? $msg1 : $msg2;
				//print_r($data);exit;
		
		$this->session->set_flashdata('message','1 data success insert');
	}

	
	function form_update_subcontent(){

		$id = $this->uri->segment(3);
		$select = "id,(SELECT id FROM content_type WHERE id=content_id) as content_id,subtype_name,document_req_id,created_id,created_date,last_updated";
		$def    = $this->crud->browse("","subcontent_type l","id",$id,"false",$select);
		$data['def'] = $def ;
		$content = $this->crud->browse("","content_type","","","true","id,type_name");
		if(!$content) $content = array();
		$select = array(''=>'-- Select --');
		foreach($content as $val){ $options[$val->id] = $val->type_name; }
		$data['content'] = $select + $options;
		
		$order  = array('field'=>'document_req','sort'=>'ASC');
		
		$data['doc']  = $this->crud->browse("","document_req","","","true","id,document_req","",$order);
		
		$this->load->view('subcontent/form_update',$data);
	}
	
	function update_subcontent(){
		$ttl   		 = $_POST['ttl'];
		$id  		 = $_POST['id'];
		$doc		 = "";
		$document	 = "";
		for ($i = 1; $i <= $ttl; $i++) {
			$value = $this->input->post('check'.$i);
			if($value) $doc[] = $value; }
			
			if($doc) $document = implode(",",$doc);
			$update = array('document_req_id'  => !$document ? "" : $document,
							'id'			=>$id,
							'content_id'	=>$_POST['content_id'],
							'subtype_name'  =>$_POST['subtype_name'],
							'last_updated'  => date('Y-m-d H:i:s'));
			 $this->crud->update("","subcontent_type","id",$id,$update);
			//print_r($update);
			
		$this->session->set_flashdata('message','1 data success update');
		redirect('content/','refresh');
	}
	
	function delete_subcontent(){
	
		$id = $this->uri->segment(3);
		$this->crud->delete("","subcontent_type","id",$id);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('content/subcontent/','refresh');
	
	}
}