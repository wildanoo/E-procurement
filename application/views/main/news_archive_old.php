<div class="about-content-wrap pull-left" id="#">
  <div style="margin-top:-85px;position:absolute;">&nbsp;
    <a name="news"><span></a>
  </div>
  <div class="head">
    <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;"><?php echo $this->lang->line('news_title');?></h1>
    <a href="#news" class="next-section about animate2">
      <i class="fa fa-angle-double-down"></i>
    </a>
  </div>
  <div class="container">
    <div class="row">   
      <div class="container">
        <div class="dept-title-tabs" style="margin-bottom: 30px;"><?php echo $this->lang->line('news_subtitle');?></div>
        <div class="col-xs-12 col-sm-12 col-md-12 no-pad wow fadeInRight animated" data-wow-delay="1s" data-wow-offset="200">
          <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 test-box" style="background-color:#d3d3d3;padding: 24px 0 0 0;">
            <div class="form-group">
              <div class="col-md-12">
                <div class="col-md-5">
                  <div class="form-group">
                    <div class="input-group date" id="datetimepicker1">
                      <input type="text" class="form-control">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="col-md-2 text-center">
                  <label class="control-label"><?php echo $this->lang->line('global_search_to_tag');?></label>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <div class="input-group date" id="datetimepicker2">
                      <input type="text" class="form-control">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 test-box" style="padding-bottom: 19px;background-color:#d3d3d3;">
            <div class="form-group">
              <div class="col-md-12">
                <div class="input-group">
                  <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default btn-src"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 no-pad wow fadeInRight animated" data-wow-delay="1s" data-wow-offset="200">

          <?php if (count($browse) > 0) {
          foreach ($browse as $content) { ?>
          <div class="col-xs-12 col-sm-12 col-md-12 department-wrap box-news">
            <div class="wow fadeInRight">
              <!-- <img src="images/dep-dummy.jpg" class="img-responsive dept-author-img-desk col-md-4" alt="" /> -->
              <div class="dept-content pull-right col-md-12 col-lg-12">
                <div class="dept-title pull-right"><?php echo $content->title; ?></div>
                <div class="post-meta-top pull-left">
                  <ul>
                    <?php 
                    $date = str_replace('/', '-', $content->publish_date);
                    ?>
                    <li><i class="icon-calendar"></i><?php echo date('M d, Y h:i:s A', strtotime($date)); ?></li>
                  </ul>
                </div>
                <p><?php echo $content->detail; ?></p>
                <a href="news-detail.html" class="dept-details-butt"><?php echo $this->lang->line('global_readmore_tag');?></a>
                <div class="purchase-strip-blue dept-apponit-butt"><div class="color-4">
                </div></div>
                <div class="vspacer"></div>
              </div>
              <hr>
            </div>
          </div>
          <hr>
          <?php }}else{ echo "<p class='col-xs-12 col-sm-12 col-md-12 department-wrap box-news'>No Event(s) yet.</p>"; }?>
          <!-- <div class="col-xs-12 col-sm-12 col-md-12 department-wrap box-news">
            <div class="wow fadeInRight">
              <img src="images/dep-dummy.jpg" class="img-responsive dept-author-img-desk col-md-4" alt="" />
              <div class="dept-content pull-right col-md-7 col-lg-8">
                <div class="dept-title pull-right">Garuda Indonesia requires iPad Mounting Provider</div> 
                <div class="post-meta-top pull-left">
                  <ul>
                    <li><i class="icon-calendar"></i>25 DEC 2013</li>
                  </ul>
                </div>
                <p>In order to support and enhance our operational safety level, PT Garuda Indonesia (Persero) Tbk (“Garuda”) is looking for a qualified company to provide iPad Mounting with qualifications</p>
                <a href="news-detail.html" class="dept-details-butt"><?php //echo $this->lang->line('news_readmore');?></a>
                <div class="purchase-strip-blue dept-apponit-butt"><div class="color-4">
                </div></div>
                <div class="vspacer"></div>
              </div>
              <hr>
            </div>
          </div>
          <hr> -->
          <!-- <div class="col-xs-12 col-sm-12 col-md-12 department-wrap box-news">
            <div class="wow fadeInRight">
              <img src="images/dep-dummy.jpg" class="img-responsive dept-author-img-desk col-md-4" alt="" />
              <div class="dept-content pull-right col-md-7 col-lg-8">
                <div class="dept-title pull-right">Garuda Indonesia requires iPad Mounting Provider</div> 
                <div class="post-meta-top pull-left">
                  <ul>
                    <li><i class="icon-calendar"></i>25 DEC 2013</li>
                  </ul>
                </div>
                <p>In order to support and enhance our operational safety level, PT Garuda Indonesia (Persero) Tbk (“Garuda”) is looking for a qualified company to provide iPad Mounting with qualifications</p>
                <a href="news-detail.html" class="dept-details-butt"><?php //echo $this->lang->line('news_readmore');?></a>
                <div class="purchase-strip-blue dept-apponit-butt"><div class="color-4">
                </div></div>
                <div class="vspacer"></div>
              </div>
              <hr>
            </div>
          </div> -->
        </div>
      </div> 
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate" style="text-align: center;">
          <ul class="pagination">
            <li class="paginate_button previous disabled" id="example2_previous">
              <a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0"><?php echo $this->lang->line('global_pagingprev_tag');?></a>
            </li>
            <li class="paginate_button active">
              <a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0">1</a>
            </li>
            <li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0">2</a>
            </li>
            <li class="paginate_button next" id="example2_next"><a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0"><?php echo $this->lang->line('global_pagingnext_tag');?></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>