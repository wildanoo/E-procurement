<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){	 
	 $(function(){  });
		var baseUrl = "<?php echo base_url(); ?>";
	    	 
	 $('#btn_create').click(function(){ 
	 	  form_create();
	 	  //alert('test');
	 });	
});

	function form_create(){
		var baseUrl = "<?php echo base_url(); ?>";		 
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		BootstrapDialog.show({
		title: 'Create Department',
        message: $('<div></div>').load(baseUrl + "department/form_create/"),
        buttons: [{ label: 'Submit',
	                cssClass: 'btn btn-primary btn-sm',		               
	                action: function(dialogRef){
		                 var id_divisiont  	   = $("#id_division").val(); 
	                	 var dept_code  = $("#dept_code").val();
	                	 var dept_name  = $("#dept_name").val(); 
	                	 
	                	 $.post( baseUrl + 'department/is_exist', { csrf: token , id_division : id_division , dept_code : dept_code , dept_name : dept_name })
						 .done(function( resp ) {
								 //$('#debug').html(resp);
								 //dialogRef.close();
								var json = $.parseJSON(resp);
								if (json.status=='true'){
				                	var values = $("#form").serializeArray();
				                	values.push({ name: csrf,value: token });
									values = jQuery.param(values);
									 $.ajax({
							               url  : baseUrl + 'department/create',
							               type : "POST",              		               
							               data : values,
							               success: function(resp){
							               		document.location.href= baseUrl + 'department/';	               				               		  
									       }
							            });   
								} else { 	
									 $("#msg1").html(json.msg1); 
									 $("#msg2").html(json.msg2); 
									 $("#msg3").html(json.msg3); 
							}
	               		 });  
	                }
	            }, {
	            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
			                action: function(dialogRef) {
			                	document.location.href= baseUrl + 'department/';
			                    dialogRef.close(); 
			                }
		        }]
    });
}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Department',
            message: $('<div></div>').load(baseUrl + "department/form_update/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef)
		                {
			                 var id_division    = $("#id_division").val();
		                	 var dept_code  = $("#dept_code").val(); 
		                	 var dept_name  = $("#dept_name").val(); 
		                	 
		                	 $.post( baseUrl + 'division/is_exist', { csrf: token , id_division : id_division , dept_code : dept_code , dept_name : dept_name })
							 .done(function( resp ) {
//									 $('#debug').html(resp);
//									 dialogRef.close(); 
									var json = $.parseJSON(resp);
									if (json.status=='true'){
					                	var values = $("#form").serializeArray();
					                	values.push({ name: csrf,value: token });
										values = jQuery.param(values);
										 $.ajax({
								               url  : baseUrl + 'department/update',
								               type : "POST",              		               
								               data : values,
								               success: function(resp){
								               		document.location.href= baseUrl + 'department/';	               				               		  
										       }
								            });   
									} else { 	
									 $("#msg1").html(json.msg1); 
									 $("#msg2").html(json.msg2); 
									 $("#msg3").html(json.msg3); 
								}
		               		 });  
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'department/';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>
<script src="<?php echo base_url()?>assets/js/sorttable.js"></script>
</head>
<body>
<div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Manage Department</h3>
              </div><!-- /.box-header -->
                <br>
				    <?php if($this->session->flashdata('msg_warning')!=null) { ?>
                	<div class="alert alert-warning">
  						<strong>Warning!</strong> <?php echo $this->session->flashdata('msg_warning'); ?>
					</div>
					<?php } ?>
                <div class="col-md-2 pull-left">
                    <input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Department" value="New Department" style="width:130px;" title="Create New Department"/>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
				<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
                </div>
                    
                <div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('department/search',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>department" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		        </div>
		        </div> 
                    
<div class="box-body">
<table border="1" id="example2" class="table table-bordered table-hover text-center sortable" cellspacing="0" width="80%" style="font-size: 13px">
	<thead>
		<tr>
            <th class="sorttable-sorted-reverse">No</th>
            <th class="">Division Name</th>
            <th class="">Department Code</th>
            <th class="">Department Name</th>
            <th class="">Created Date</th>
            <th class="">Updated Date</th>
            <th class="">Updated By</th>
            <th>Action</th>
        </tr>
     </thead>
     <tbody>
 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td style="text-align:center;"><?php echo $num = $num+1; ?></td>
			<td><?php echo $row->id_division; ?></td>	
			<td><?php echo $row->dept_code; ?></td>	
			<td><?php echo $row->dept_name; ?></td>
 			<td align="center"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>	
 			<td align="center"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>	
 			<td align="center"><?php echo $row->created_id; ?></td>						
				<td><?php				
					echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete department ?')){ return true; } else {return false; }";
					echo anchor('department/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Department','onclick'=>$js));?>
				</td>					
		</tr>
<?php	$i++;  } ?>	
</table>
<tr><div colspan="9" style="text-align: center;"><?php echo $pagination;?></div></tr>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>  
	</table>  
                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->
