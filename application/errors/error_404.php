<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $heading; ?> | e-Procurement Garuda Indonesia</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="icon" type="image/png" href="/assets_public/images/favicon-grd.png">
	<link rel="stylesheet" type="text/css" href="/assets_/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="/assets_/css/bootstrap-admin.css" />
	<link rel="stylesheet" type="text/css" href="/assets_/css/bootstrap-responsive.min.css">
	<link rel="stylesheet" type="text/css" href="/assets_/css/custom/afterlogin.min.css">
	<link rel="stylesheet" type="text/css" href="/assets_/css/custom/afterloginstyle.css">
</head>
<body>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="brand" href="/"><img src = "/assets_/favicon-white.png" style="margin-right:10px;"/>e-Procurement Garuda Indonesia</a>
			</div>
		</div>
	</div>
	<div class="container">
		<br/><br/><br/><br/>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default main-footer navbar-inner">
					<div class="panel-heading">
						<h6><?php echo $heading; ?></h6>
					</div>
					<div class="panel-body">
						<h4>
							<?php echo $message; ?>
							<div>Go to our <a href="/">home page</a> or go back to <a href="javascript:window.history.go(-1)">previous page</a>.</div>
						</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="main-footer footer-line" style="position: absolute; bottom: 0px; left: 0; right: 0;">
		Copyright &copy; 2016 <strong><a href="https://www.garuda-indonesia.com" target="_blank" style="text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/" target="_blank"><img src="/assets_/images/gallery/logo-asyst.png" style="vertical-align: middle;"/></a>
	</footer>
</body>
</html>