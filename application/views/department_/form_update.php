<?php echo form_open('#',array('id'=>'form')); ?>
<table cellpadding="2" cellspacing="2">
<?php echo form_hidden('id',$data->id); ?>
<tr>
	<td><label>Office</label></td><td>:</td>
	<td><?php 
		$prop_office = 'class="form-control" id="id_office" style="width:220px"';
		echo form_dropdown('id_office',$office,$def->id_office,$prop_office); ?>			
	</td>
	<td id="msg1"></td>	
</tr>
<tr>
	<td><label>Department Code</label></td><td>:</td>
	<td><input type="text" name="dept_code" id="dept_code" value="<?php  echo $data->dept_code; ?>" class="form-control" required/></td>
	<td id="msg2"></td>	
</tr>
<tr>
	<td><label>Department Name</label></td><td>:</td>
	<td><input type="text" name="dept_name" id="dept_name" value="<?php  echo $data->dept_name; ?>" class="form-control" required/></td>
	<td id="msg3"></td>	
</tr>
</table>