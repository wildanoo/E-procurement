<?php 
class m_verify extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('crud');		
	}
	
 function authenticate(){      	
 	$allowed = array(91);	
	$permit  = $this->session->userdata('permissions');
	$permit  = explode(",",$permit);
	foreach($permit as $str){
	$permissions[] = preg_replace("/[^0-9]/","",$str);}
 	$result        = array_intersect($allowed, $permissions);
 	
	if( $result ){ return true;
	} return false;
 }
 
 function attribute_account($status){
 	
 	switch ($status) {
	    case "recon":
	        $msg = "Completed Reconciliation Account";	
 			$to  = "stf_wa2"; $form = "EPCGA011";	
	        break;
	    case "sap":
	        $msg = "Completed Vendor Nr. SAP"; 	
 			$to  = "stf_ibk"; $form = "EPCGA012";
	        break;    
	    default:
	        $msg = "Completed SRM Username dan Password"; 
 		    $to  = "as"; $form = "EPCGA013";
	}
	
	$result = array('msg'=>$msg,'to'=>$to,'form'=>$form);
	
	return (object)$result;
 	
 }

}	
?>