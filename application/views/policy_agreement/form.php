 
 <?php echo form_open('policy_agreement/update_status',array('id'=>'form','class'=>'form-horizontal'));
 
 	echo form_hidden('id',$default->id);
 ?>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">

              <div class="box-header with-border">
                <h3 class="box-title">Review Content</h3>
              </div>

              <!--form class = "form-horizontal"-->
                <div class="box-body">

                  <div class="form-group clear-pad-top">
                    <label for="category" class="col-sm-3 control-label inputbox-size vcenter">Type</label>                   
                    <div class="col-sm-6 control-label clear-pad-top row vcenter"> 
                    <?php  echo $default->type; ?>

                    </div>
                  </div>

                  <div class="form-group">
                    <label for="namedesc" class="col-md-3 control-label inputbox-size vcenter">Creator</label>
                    <div class="col-md-9 row vcenter">
                      <?php  echo ucwords($default->creator);  ?>
                    </div>
                  </div>

                  <div class = "form-group">
                    <label for="serviceclass" class="col-sm-3 control-label inputbox-size vcenter" >Content Indonesia</label>
                      <div class="col-lg-9 col-sm-6 col-12 row inputbox-size vcenter">                       
                        <?php  
                        	if($content) echo anchor('policy_agreement/open_content_ind/'.$default->id,"Viewed",array('class'=>'default_link','title'=>'Viewed')); ?>
                      </div>
                  </div>

                  <div class = "form-group">
                    <label for="serviceclass" class="col-sm-3 control-label inputbox-size vcenter" >Content English</label>
                      <div class="col-lg-9 col-sm-6 col-12 row inputbox-size vcenter">                       
                        <?php  
                          if($content) echo anchor('policy_agreement/open_content_eng/'.$default->id,"Viewed",array('class'=>'default_link','title'=>'Viewed')); ?>
                      </div>
                  </div>

                  <div class="form-group">
                      <label for="authority" class="col-md-3 control-label inputbox-size vcenter">Detail</label>
                      <div class="col-md-9 row vcenter">
                       <?php echo $default->detail; ?>
                      </div>
                    </div>
                 
                  </div><!-- col-md-6 -->
                  </div>
                </div><!-- /.box-body -->

                <div class="box-footer" style="padding-left: 30px;">
                   
                    <?php  $prop = array('class'=>'btn btn-default','title'=>'Back'); 
                           echo anchor('policy_agreement/','Back',$prop);  ?>	
                    <?php echo  form_close(); ?>
                </div><!-- /.box-footer -->
              <!--/form-->
            </div><!-- /.box -->
          </div>
        </div>