<div id="container7">
<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   
   $('#btn_create7').click(function(){
    	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		var values  = $("#form_org").serializeArray();
				values.push({name: csrf,value: token});
				values = jQuery.param(values); 
						$.ajax({
						    url  : baseUrl + 'data_avl/update_person_org',
						    type : "POST",              		               
						    data : values,
						    success: function(resp){
						        $("#container7").load(baseUrl+'data_avl/load_org', function(){$('#org_msg').html('Data akan ditampilkan setelah proses approval disetujui');});
						    }
						});							     	
     });     
     
   $('#btn_cancel7').click(function(){ 
   		 $("#container7").load(baseUrl+'data_avl/load_org');
     });     
	   
   });

</script>
<div id="debug7"></div>
<?php echo form_open_multipart('#',array('id'=>'form_org')); //$current_date = date('Y-m-d'); ?>
<input type="hidden" name="id" id="id" value="<?php echo $default->id; ?>"/>
<table width="50%">
<tr>
	<td><label>Name</label></td><td>:</td>
	<td><input type="text" name="prsn_name" id="prsn_name" value="<?php echo $default->name; ?>" placeholder="Document Name" class="form-control" required/></td>
	<td id="msg1">&nbsp;</td>	
</tr>
<tr>
	<td><label>Address</label></td><td>:</td>
	<td><input type="text" name="prsn_addrs" id="prsn_addrs" value="<?php echo $default->address; ?>" placeholder="Document Number" class="form-control" required/></td>
	<td id="msg2">&nbsp;</td>	
</tr>
<tr>
	<td><label>Phone</label></td><td>:</td>
	<td><input type="text" name="prsn_phone" id="prsn_phone" value="<?php echo $default->phone; ?>"placeholder="Description" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Position</label></td><td>:</td>
	<td><input type="text" name="prsn_post" id="prsn_post" value="<?php echo $default->position; ?>"placeholder="Description" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Remark</label></td><td>:</td>
	<td><input type="text" name="remark" id="remark" value="<?php echo $default->remark; ?>" placeholder="Remark" class="form-control" required/></td>
</tr>
</table>

<div style="text-align: right;">
		<input type="button" id="btn_cancel7" value="Cancel" class="btn btn-default btn-sm"/>
		<input type="button" id="btn_create7" value="Update" class="btn btn-primary btn-sm"/>
</div>
<br/>
<?php echo  form_close(); ?>
</div>


