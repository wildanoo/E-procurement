<script type="text/javascript"> 
	$(document).ready(function(){	
		var baseUrl   = "<?php echo base_url(); ?>";	
		$(function(){	});	
		
		$('#id_cat').change(function(){ 
			var id_cat = $('#id_cat').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'bidding/set_sess_category',			   
               data : 'id_cat='+id_cat,
               success: function(data){ 				 	
				 	document.location.href= baseUrl + 'bidding/';
			   }		   
            });	
		
		});

		$("#srch-term").autocomplete({
			source: baseUrl + 'bidding/get_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'bidding/set_browse_session',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){                		            
				    document.location.href= baseUrl + 'bidding/';
               }
            });
        });  

		$("#start_date").datetimepicker({
			format:'Y/m/d',
			timepicker:false,
			maxDate: $("#end_date").val(),
			onChangeDateTime: function (dp,selectedDate) {
				var time = $("#start_date").val();

	            $("#end_date").datetimepicker({minDate: selectedDate.val()});
	        }
		});

		$("#end_date").datetimepicker({
			format:'Y/m/d',
			minDate: 0,
			timepicker:false,
			onChangeDateTime: function (dp,selectedDate) {
				var time = $("#start_date").val();
				var init = selectedDate.val();

	            $("#start_date").datetimepicker({maxDate: selectedDate.val()});
	        }
		});

		$('#id_type').change(function(){
   		var type 	 = $('#id_type').val();
		var dir 	 = window.location.pathname.split( '/' );
		var uri		 = 'bidding';
		if (typeof dir[4] !== "undefined") {uri = dir[4]};
   		 	$.ajax({
	            type: "POST",
	            url: '<?php echo base_url();?>announce/getSubcontent/'+uri,
	            data: "data=" + $(this).val(),
	            dataType:"json",
	            success: function(result){
	            	if (result.length>0) {
		                $('#id_subtype option').remove();
		                var opt = $('<option />');
		                opt.val('');
		                opt.text('-- Select Sub Content --');
		                $('#id_subtype').append(opt);
		                $.each(result,function(i,result){
		                    opt = $('<option />');
		                    opt.val(result.id);
		                    opt.text(result.subtype_name);
		                    $('#id_subtype').append(opt);
		               });
		               $('#id_subtype').removeAttr('disabled');
		            }else{
		            	$('#id_subtype option').remove();
		                var opt = $('<option />');
		                opt.val('');
		                opt.text('-- No Sub Type Found(s) --');
		                $('#id_subtype').append(opt);
		               	$('#id_subtype').attr('disabled','true');
		            }
	            },
	            error: function(XMLHttpRequest, textStatus, errorThrown) {
	            }
	        });
	    });
	});

</script>

<div id="debug"></div>

<?php 	$msg = $this->session->flashdata('message');
		if(strpos($msg,'failed')) $color = "#bc0505";
		else $color = "#487952"; 		
	 	 ?>	
<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>

<div class="page-header">
  <h3>Bidding Management</h3>
</div>

<!-- Search Term -->
<div class="table">
	<div class="panel panel-default">
	    <div class="panel-heading">
	        <i class="fa fa-search"></i> <strong>Search Section</strong>
	    </div>
	    <style type="text/css">
	    .list-inline li{
	    	vertical-align: middle !important;
	    }
	    .btn-srch{
	    	margin-bottom: 0;
	    }
	    </style>
	    <?php echo form_open_multipart('bidding/search',array('id'=>'form-search','novalidate' => 'true'));	//$current_date = date('Y/m/d')?>
	    <div class="panel-body">
	    	<ul class="list-inline">
	    		<li style="width:24%">
	    			<div class="input-group">
	    			<input type="text" value="" placeholder="date start" class="form-control date_picker" id="start_date" name="start_date">
	    			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	    			</div>
	    		</li>
	    		<li style="width:2%">to</li>
	    		<li style="width:24%">
	    			<div class="input-group">
	    			<input type="text" value="" placeholder="date end" class="form-control date_picker" id="end_date" name="end_date">
	    			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
	    			</div>
	    		</li>
	    		<li style="width:34%"><div class="input-group" style="width:100%"><input placeholder="search by code or title..." type="text" value="" class="form-control" id="search_term" name="search_term"></div></li>
	    		<li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search">Search</button></div></li>
	    		<li style="width:7%"><div class="input-group"><a href="<?php echo base_url()?>bidding" class="btn btn-srch btn-warning btn-sm">Show All</a></div></li>
	    	</ul>
	    </div>
	    <?php echo form_close()?>
	    <!-- <div class="panel-footer text-center">
	        <a href="#" class="btn btn-primary" style="margin-bottom:0;">Change Password</a>
	    </div> -->
	</div>
</div>
<hr/>
<!-- Search Term -->

<?php if ($this->permit->cc) { ?>
<?php echo form_open_multipart('announce/portal_create',array('id'=>'form'));?>
<div id="create-content" class="modal fade" role="dialog">
    <div class="modal-dialog">
      	<div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Create Content</h4>
	        </div>
	        <div class="modal-body">
	          	<div class="col-sm-12">
	          		<label class="control-label col-sm-4" for="edit_vendor_reg">Content Type</label>
					<div class="col-sm-8">
					<?php
						$prop_content = 'class="form-control" id="id_type" style="width:100%"';
						echo form_dropdown('id_type',$content_type,"",$prop_content);
					?>
					</div>
					<label class="control-label col-sm-4" for="edit_company_name">Sub Content Type</label>
					<div class="col-sm-8">
						<?php
							$prop_subcontent = 'class="form-control" id="id_subtype" disabled style="width:100%"';
							echo form_dropdown('id_subtype',$subcontent_type,"",$prop_subcontent); 
						?>
					</div>
	        	</div>
	      	</div>
	      	<div class="panel-footer text-right">
				<button type="submit" class="btn btn-primary" name="button-process" style="margin-bottom:0;">Create</button>
          		<button type="button" class="btn btn-danger" style="margin-bottom:0;" data-dismiss="modal">Cancel</button>		
			</div>
    	</div>
	</div>
</div>
<?php echo form_close();?>
<a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#create-content" id="btn_create" title="Create">Create</a>
<?php }?>

<?php
	$sess_cat = $this->session->userdata('sess_cat');
	$id_cat   = !$sess_cat ? 0 : $sess_cat;	            
	$prop_cat = 'id=id_cat  style="width: 120px; height: 25px; font-size: 13px"'; ?>
	<?php echo form_dropdown('id_cat', $category, $id_cat,$prop_cat); ?>

<table id="browse" class="table table-striped table-bordered" style="font-size: 13px">
	<thead>
        <tr>
            <th>No</th>
            <th>Code</th>
            <th>Title</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Published Date</th>
            <th>Category</th>
            <th>Subcategory</th>
            <th>Type of Invitation</th>
            <th>Bidding Status</th>
        </tr>
	</thead>
    <tbody>
<?php			
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
		<tr>
			<td><?php echo $num = $num+1; ?></td>
			<td>
				<?php
				$pencil = '<i class="fa fa-pencil"></i>';
				echo anchor('bidding/detail/'.$row->id, $row->code?$row->code:'-',array('class'=>'default_link','title'=>'Edit Announcement')); 
				?>
			</td>
			<td>
				<?php echo $row->title_ind ? $row->title_ind.' <strong>(Indonesia)</strong>':'- <strong>(Indonesia)</strong>'; ?>
				<br>
				<?php echo $row->title_eng ? $row->title_eng.' <strong>(English)</strong>':'- <strong>(English)</strong>'; ?>
			</td>
			<td><?php echo date('M d, Y',strtotime($row->first_valdate)); ?></td>
			<td><?php echo date('M d, Y',strtotime($row->end_valdate)); ?></td>
			<td><?php echo date('M d, Y',strtotime($row->publish_date)); ?></td>
			<td><?php echo array_key_exists($row->id_cat, $category)?$category[$row->id_cat]:'-'; ?></td>
			<td><?php echo array_key_exists($row->id_subcat, $subcategory)?$subcategory[$row->id_subcat]:'-'; ?></td>
			<td><?php echo array_key_exists($row->subcontent_type, $subcontent_type)?$subcontent_type[$row->subcontent_type]:'-'; ?></td>
			<?php
			if ($row->status == 7){$status = 4;}
			elseif (date('Y-m-d H:i:s') < $row->publish_date){$status = 0;}
			elseif (date('Y-m-d H:i:s') >= $row->publish_date && date('Y-m-d H:i:s') < $row->first_valdate){$status = 1;}
			elseif (date('Y-m-d H:i:s') >= $row->first_valdate && date('Y-m-d H:i:s') <= $row->end_valdate){$status = 2;}
			elseif (date('Y-m-d H:i:s') > $row->end_valdate && ($row->status != 3 || $row->status != 4)){$status = 3;}
			elseif ($row->status == 8){$status = 99;}
			?>
			<td><?php echo array_key_exists($status, $status_ref)?$status_ref[$status]:'-'; ?></td>
		</tr>
<?php	$i++;  } ?>	
<tr><td colspan="10" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?><tr><td colspan="10">-</td></tr><?php }?>
    </tbody>
</table>
