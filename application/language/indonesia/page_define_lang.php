<?php

/* author  : richo mahardika */
/* created : 10 august 2016 17:07 */

//PAGES
// $lang['home_page']		 = 'auth/ind/login';
// $lang['navbar_top']		 = 'main/ind/navbar_top';
// $lang['sidebar_nav']	 = 'main/ind/sidebar_nav';

//PAGE STRUCTURE GLOBAL
$lang['global_registration_tag'] 	 = 'Registrasi';
$lang['global_vendor_tag']		 	 = 'Kebijakan Vendor';
$lang['global_home_tag'] 			 = 'Beranda';
$lang['global_newsannouncement_tag'] = 'Berita & Pengumuman';
$lang['global_contactus_tag']  		 = 'Hubungi Kami';
$lang['global_recent_post_tag'] 	 = 'Posting Terkini';
$lang['global_search_to_tag']		 = 'hingga';
$lang['global_readmore_tag']		 = 'Lebih Lanjut';
$lang['global_pagingprev_tag']  	 = 'Sebelumnya';
$lang['global_pagingnext_tag']  	 = 'Selanjutnya';
$lang['global_category_tag']		 = 'Kategori';
$lang['global_subcategory_tag']		 = 'Sub Kategori';
$lang['global_title_tag']			 = 'Judul';
$lang['global_regnum_tag']			 = 'No. Registrasi';
$lang['global_startdate_tag']		 = 'Tanggal Mulai';
$lang['global_enddate_tag']		 	 = 'Tanggal Berakhir';
$lang['global_publishdate_tag']	 	 = 'Tanggal Publish';
$lang['global_technical_tag'] 	 	 = 'Petunjuk Teknis';

//PAGE STRUCTURE HOME
$lang['section1_sub1']	 = 'SELAMAT DATANG DI';
$lang['section2']		 = 'BERITA DAN PENGUMUMAN';
$lang['section2_sub1']	 = 'BERITA';
$lang['section2_sub2']	 = 'PENGUMUMAN';
$lang['section2_sub3']	 = 'Lanjut Membaca';
$lang['section2_sub4']	 = 'Detail';
$lang['section2_sub5']	 = 'Tampilkan Semua';
$lang['section4']		 = 'HUBUNGI KAMI';
$lang['section4_sub1']	 = 'Lt. Dasar, Gedung Manajemen Garuda';
$lang['section4_sub2']	 = 'Bandara Soekarno-Hatta, Jakarta';
$lang['section4_sub3']	 = 'Kontak';
$lang['section5_sub1']	 = 'Login Member';
$lang['section5_sub2']	 = 'Registrasi dan Vendor Policy';
$lang['section5_sub3']	 = 'Bagaimana caranya saya bisa mendaftar sebagai Vendor?';
$lang['section5_sub4']	 = 'Anda dapat mendaftarkan perusahaan anda dengan mengisi formulir pendaftaran pada';
$lang['section5_sub5']	 = 'link ini';
$lang['section5_sub6']	 = 'Bagaimana caranya saya bisa mencari tahu peraturan yang berlaku untuk Vendor?';
$lang['section5_sub7']	 = 'Anda dapat membaca Vendor Policy dengan mengklik';
$lang['section6']		 = 'Registrasi';
$lang['section6_sub1']   = 'Cek Status Registrasi';
$lang['section7']		 = 'Kebijakan Vendor';


//PAGE STRUCTURE NEWS
$lang['news_title'] 	 = 'BERITA';
$lang['news_subtitle'] 	 = 'Daftar Berita';

//PAGE STRUCTURE ANNOUNCEMENT
$lang['announcement_title'] 	   = 'PENGUMUMAN';
$lang['announcement_subtitle'] 	   = 'Daftar Pengumuman';
$lang['announcement_thead_catsub'] = 'Kategori/Sub Kategori';
$lang['announcement_thead_type']   = 'Tipe Undangan';

//PAGE STRUCTURE PAGE DETAIL
$lang['page_detail_prev_article']  = 'Artikel sebelumnya';
$lang['page_detail_next_article']  = 'Artikel selanjutnya';

//FILES
$lang['about_us_file']   = 'uploads/cms/aboutus_ind.txt';
$lang['contact_us_file'] = 'uploads/cms/contact_ind.txt';
$lang['faq_file']		 = 'uploads/cms/faq_ind.txt';
$lang['general_file']	 = 'uploads/policy_agreement/general_ind';
$lang['open_sourcing']	 = 'uploads/policy_agreement/open_sourcing_ind';
$lang['open_bidding']	 = 'uploads/policy_agreement/open_bidding_ind';
$lang['direct_selection']= 'uploads/policy_agreement/direct_selection_ind';
$lang['question']		 = 'question_ind';
$lang['answer']			 = 'answer_ind';

//
$lang['header_file_technical'] = "Petunjuk Teknis Registrasi";