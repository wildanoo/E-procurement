<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_assesment extends CI_Model {

	function authenticate(){      	
		$allowed = array(143);	
		$permit  = $this->session->userdata('permissions');
		$permit  = explode(",",$permit);
		foreach($permit as $str){
			$permissions[] = preg_replace("/[^0-9]/","",$str);}
			$result        = array_intersect($allowed, $permissions);

			if( $this->tank_auth->is_logged_in() 
				&& $result ){ return true;
			} return false;
	}
	

}

/* End of file m_assesment.php */
/* Location: ./application/models/m_assesment.php */