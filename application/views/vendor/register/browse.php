<div id="container">

	<script type="text/javascript"> 
		var baseUrl   = "<?php echo base_url(); ?>";
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token     = '<?php echo $this->security->get_csrf_hash(); ?>';	
		
		$(document).ready(function(){	
			
			$('.checkall').on('click', function(){
				$(this).closest('table').find(':checkbox').prop('checked', this.checked);
			});
			
			$("#start_date").datetimepicker({
				format:'Y/m/d',
				timepicker:false,
				maxDate: $("#end_date").val(),
				onChangeDateTime: function (dp,selectedDate) {
					var time = $("#start_date").val();

					$("#end_date").datetimepicker({minDate: selectedDate.val()});
				}
			});

			$("#end_date").datetimepicker({
				format:'Y/m/d',
				minDate: 0,
				timepicker:false,
				onChangeDateTime: function (dp,selectedDate) {
					var time = $("#start_date").val();
					var init = selectedDate.val();

					$("#start_date").datetimepicker({maxDate: selectedDate.val()});
				}
			});

			$("#search_term").autocomplete({ 
				source: baseUrl + 'register/get_tags',
				minLength:1
			}); 

			$('#reset_session').click(function(){ 
				$.post( baseUrl + 'register/reset_browse', function( resp ) {
					document.location.href= baseUrl + 'register/';
				});
			});
			
			$('#id_status').change(function(){ 
				var id_status = $('#id_status').val(); 
				$.post( baseUrl + 'register/set_sess_status', { csrf: token , id_status: id_status })
				  .done(function( resp ) {
				    	document.location.href= baseUrl + 'register/';
				    	//$('#debug').html(resp);
				});
				
				
			});		
			
			
		});	
		
		function exporttofile(){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
			var token   = '<?php echo $this->security->get_csrf_hash(); ?>';		
			var values  = $("#form").serializeArray();
			
			var atLeastOneIsChecked = $('input[name="selected[]"]:checked').length > 0;
			
			if(atLeastOneIsChecked){

				values.push({name: csrf,value: token});		
				values 	   = jQuery.param(values);
				$.ajax({
					type	: "POST",
					url		: baseUrl + "export/multivendor",
					data	: values,
					cache	: false,
					beforeSend: function(){	},
				success: function(resp){ //$("#debug").html(resp);
				var json = $.parseJSON(resp);		
				$.post( baseUrl + "export/clean_file",{ csrf: token, path: json.path }).done(function( resp2 ) {
					window.location.href = json.url;	
				});
				
			}
		}); 	
				
			} else { alert('please select one or more vendor in the list !'); }
		}
		
	</script> 
	<style type="text/css">
		.list-inline li{
			vertical-align: middle !important;   }
			.btn-srch{	margin-bottom: 0;    }
		</style>

		<div id="debug"></div>


		<div class="table">
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-search"></i> <strong>Search Section</strong>
				</div>	    
				<?php echo form_open('register/set_session_browse',array('id'=>'form-search','novalidate' => 'true')); ?>
				<div class="panel-body">
					<ul class="list-inline">
						<li style="width:24%">
							<div class="input-group">
								<input type="text"  placeholder="date start" class="form-control date_picker" id="start_date" value="" name="start_date">
								<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
							</div>
						</li>
						<li style="width:2%">to</li>
						<li style="width:24%">
							<div class="input-group">
								<input type="text"  placeholder="date end" class="form-control date_picker" id="end_date" value="" name="end_date">
								<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
							</div>
						</li>
						<li style="width:34%"><div class="input-group" style="width:100%">
							<input placeholder="search by code or title..." type="text"  class="form-control" id="search_term" name="search_term"></div></li>
							<li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search">Search</button></div></li>
							<li style="width:7%"><div class="input-group">
								<button class="btn btn-srch btn-warning btn-sm" id="reset_session" title="Show All">Show All</button>
							</div></li>
						</ul>
					</div>
					<?php echo form_close()?>
				</div>
			</div>
			<hr/>
			<div class="page-header">
				<h3>Vendor Registration
				
				<?php 
				if($user_level==0){
				$id_stsreg = $this->session->userdata('id_stsreg'); ?>				
				<div style="float:right">
				<select name="id_status" id="id_status" class="form-control">
					<option value="all">- All Status -</option>	
					<?php foreach($status as $id=>$val){ ?>
						<?php $checked = ($id==$id_stsreg) ? "selected" : ""; ?>			
						<option  value="<?php echo $id; ?>" <?php echo $checked; ?>><?php echo ucwords(strtolower($val)); ?></option>	
					<?php } ?>
					</select>
				</div>
				<?php } ?>
				
				
					<div style="float:right">
						<?php if($this->permit->cvb){ ?><a href="<?php echo base_url() ?>create_vendor" class="btn btn-primary btn-sm btn-srch">Create Vendor</a><?php } ?>
						<a href="javascript:;" onclick="exporttofile()" class="btn btn-srch btn-success btn-sm">
							<i class="fa fa-file-excel-o" style="color:#FFFFFF"></i> Export To Excel</a>
						</div>
					</h3>
				</div>
				<?php echo $this->session->flashdata("message"); ?>
				<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
					<thead>
						<tr>
							<th style="text-align: center;" >No</th>
							<th style="text-align: center;"> &nbsp; <input id="selectAll" class="checkall" type="checkbox" /> &nbsp; </th>
							<th>Registration Num.</th> 
							<?php if(!$this->permit->apvr.$user_level){ ?><th>Vendor Num.</th><?php } ?>            
							<th>Vendor Name</th> 
							<th>Category - Sub Category</th> 
							<th>Register Status</th>
							<th>Register Date</th> 
							<?php //$header = $this->permit->apvr.$user_level ? "Status" : "Action" ; ?>          
							<?php if($this->permit->apvr.$user_level){	?><th style="text-align: center;">Status</th><?php } ?>
							<th style="text-align: center;">Action</th>
						</tr>       
					</thead>
					<tbody>
						<?php
						
						echo form_open('#',array('id'=>'form'));
						echo form_hidden('total',count($browse));
						if($browse){ $i=1;
							foreach($browse as $row){ ?>	
								<tr>
									<td style="text-align: center;"><?php  echo $num = $num+1; ?></td>
									<td style="text-align: center;"> <input type="checkbox" name="selected[]" id="checkbox-<?php  echo $num; ?>" value="<?php echo $row->id ; ?>" /> </td>
									<td><?php echo $row->reg_num ?></td>
									<?php if(!$this->permit->apvr.$user_level){ ?><td><?php if($row->vendor_num) echo $row->vendor_num; else echo "-"; ?></td><?php } ?> 		
									<td><?php  $vendor_name = !$row->vendor_name ? "-" : $row->vendor_name; echo $vendor_name;
									/*if($row->vendor_name) {
										echo anchor("register/set_vendor_session/".$row->id."/register",$row->vendor_name);  
									} else { echo "-"; }*/
									?></td>
									<td><?php
										$select   = "(SELECT category  FROM category r WHERE r.id=l.id_category) as catName,
										(SELECT subcategory  FROM subcategory s WHERE s.id=l.id_subcat) as subcatName"; 
										$category = $this->crud->browse("","vendor_category l","id_vendor",$row->id,"true",$select); $j = 1 ; 						
										foreach($category as $val){	echo $j.". ".$val->catName." - ".$val->subcatName."<br/>"; $j++; } unset($j);					 
										?></td>
										<td><?php echo ucwords(strtolower($row->sts_reg)); ?></td>
										<td style="text-align: center;"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
										
										<?php if($this->permit->apvr.$user_level){	?>	
										<td style="text-align: center;">														
											<?php	$label = $row->approval != $user_level ? "Approved" : "Waiting Approval";
												$class = $row->approval != $user_level ? "label label-success" : "label label-warning";	?>
												<i class="<?php echo $class; ?>"><?php echo $label; ?></i>											
										</td><?php } ?>
										<td style="text-align: center;">									
												<div class = "btn-group dropup">										 						   
												   <div class = "btn-group dropup">
												      <button type = "button" class = "btn btn-primary dropdown-toggle" data-toggle = "dropdown">
												         Action
												         <span class = "caret"></span>
												      </button>										      
												      <ul class = "dropdown-menu">
												         <li><!--a href = "#">View Detail</a-->
												         <?php echo anchor("register/set_vendor_session/".$row->id."/register","View Detail"); ?>
												         </li>
												         <li><!--a href = "#">View Logs</a-->
												         <?php echo anchor("register/view_tracking/".$row->id."/register","View Logs"); ?>
												         </li>
												      </ul>
												   </div>										  
												</div>				
										</td> 
													
												</tr>
												<?php	$i++;  } ?>	
												<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
												<?php } else { ?>
													<tr><td colspan="9" align="center">-</td></tr>
													<?php } ?>
													<?php echo form_close();?>

												</tbody>
												
											</table>

										</div>

