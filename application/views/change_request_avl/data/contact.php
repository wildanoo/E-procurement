<div id="contact">

<div id="contact_msg" class="text-center text-danger bg-warning"></div>	

<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Name</th>
		<th>Position</th>
		<th>Mobile</th>
		<th>Email</th>
		<th>Approval</th>
		<th>Status</th>
		<th>#</th>
	</tr>
	</thead>
	<tbody>

 <?php 
 if($cp){ $i=1;
 	foreach($cp as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->fullname; ?></td>	
			<td><?php echo $row->position; ?></td>
			<td><?php echo $row->mobile; ?></td>
			<td><?php echo $row->email; ?></td>
			<td><?php echo $row->approval; ?></td>
			<td><span class="label label-warning"><?= $stat = ($row->status == '0' || $row->status == '1') ? 'request':'approved'; ?></span></td>
			<td style="text-align: center;">
			<?php 
				echo '<button onclick="approve_contact([\''.$row->id.'\',\''.$row->approval.'\',\''.$row->id_tbl.'\'])" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>';
				echo '<button onclick="reject_temp_table('.$row->id.',\'temp_contact_person_avl\')" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>';
			?>
			</td>	
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="8">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>
