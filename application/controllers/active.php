<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Active extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model(array('m_active','m_user','m_vendor')); islogged_in();
		$this->permit = new stdClass();

		$this->permit 			= $this->crud->get_permissions("4");
	}

	public function index()
	{
		if($this->m_active->authenticate()){
			$this->session->set_userdata('feature', 'active');
			$data['user_id']			= $this->tank_auth->get_user_id();
			$data['username']			= $this->tank_auth->get_username();
			$data['view']				= "vendor/active/home";
			$this->load->view('layout/template',$data);
		}else{
			$this->session->set_flashdata('message','user not authorized');
			redirect('/auth/login/');
		}
	}

	function remove_sess_sort(){
		$search_val = array(
			'id'=>'',
			'search_term'=>'',
			'start_date'=>'',
			'end_date'=>'',
			'inaID'=>''
			);
		$this->session->set_userdata($search_val);
	}

	function browse(){
		
		$this->remove_sess_sort();

		$table 					= "vendor_inactive t1";
		$select 				= "t1.id as inaID,t2.id as vID,t2.register_num,vendor_num,vendor_name,t1.last_updated,t1.reason,t1.status,t1.approval";
		$joins[0][0]			= 'vendor t2';
		$joins[0][1]			= 't1.id_vendor=t2.id';
		$joins[0][2]			= 'left';
		$groupby 				= 'inaID';
		$orderby				= array('field'=>'t1.last_updated','order'=>'DESC');

		$where  				= "process = 'active'";

		if($this->permit->iibs && empty($this->permit->iibk) && empty($this->permit->ivpib)){
			$where  .= " AND t1.approval in ('3','2','1')";
		}elseif($this->permit->iibk && empty($this->permit->iibs) && empty($this->permit->ivpib)){
			$where  .= " AND t1.approval in ('2','3')";
		}elseif($this->permit->ivpib && empty($this->permit->iibk) && empty($this->permit->iibs)){
			$where  .= " AND t1.approval = '3'";
		}elseif($this->permit->iibs && $this->permit->iibk && $this->permit->ivpib){
			$where  .= "";
		}

		$page 					= $this->uri->segment(3);
		$per_page	 			= 10;
		$offset 				= $this->crud->set_offset($page,$per_page);
		$total_rows				= count($this->crud->browse_join("",$table,"","","true",$select,$joins,$where,$orderby,$groupby));
		$set_config 			= array('base_url'=>base_url().'active/browse','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>'3');
		$config     			= $this->crud->set_config($set_config);

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();

		$data['pagination'] 	= $paging;
		$data['num']			= $offset;

		$browse					= $this->crud->browse_join_with_paging("",$table,"","","true",$select,$joins,$where,$orderby,$per_page,$offset,$groupby);
		
		$data['browse']			= $browse;
		//category select
		$selectsub 				= 'a.id as idsub,id_cat,subcategory,a.`status`,b.id,category';
		$tablecat				= 'subcategory a';
		$joinsub[0][0]			= 'category b';
		$joinsub[0][1]			= 'a.id_cat=b.id';
		$joinsub[0][2]			= 'left';
		$data['subcat']			= $this->crud->browse_join("",$tablecat,"","","",$selectsub,$joinsub);

		//category & subcat query
		$detail = array();
		foreach($browse as $row){
			if(!empty($row->vID)){
				$category 			= $this->category_hover($row->vID);
				$detail[$row->vID] 	= $category;
			}
		}

		
		$data['detail']			= $detail;
		$data['view']			= 'vendor/active/browse';

		echo $this->m_active->ina_status();

		if($this->input->is_ajax_request()){
			$this->load->view($data['view'],$data);

		}else{
			$this->load->view('layout/template',$data);
		}
	}

	function sorting(){
		
		$subcat_select 		= ($this->input->post('subcat')) ? $this->input->post('subcat') : $this->session->userdata('id');
		$search_term 		= ($this->input->post('search_term')) ? $this->input->post('search_term') : $this->session->userdata('search_term');
		$start_date	 		= ($this->input->post('start_date')) ? $this->input->post('start_date') : $this->session->userdata('start_date');
		$end_date 			= ($this->input->post('end_date')) ? $this->input->post('end_date').' 23:59:59.000000' : $this->session->userdata('end_date');

		$search_val 		= array('id'=>$subcat_select,'search_term'=>$search_term, 'start_date'=>$start_date, 'end_date'=>$end_date );

		$this->session->set_userdata($search_val);

		$table 					= "vendor_inactive t1";
		$select 				= "t1.id as inaID,t2.id as vID,t2.register_num,vendor_num,vendor_name,t1.last_updated,t1.reason,t1.status,t1.approval,t3.id_category,t3.id_subcat";
		$joins[0][0]			= 'vendor t2';
		$joins[0][1]			= 't1.id_vendor=t2.id';
		$joins[0][2]			= 'left';
		$joins[1][0]			= 'vendor_category t3';
		$joins[1][1]			= 't3.id_vendor=t2.id';
		$joins[1][2]			= 'left';
		$orderby				= array('field'=>'t1.last_updated','order'=>'DESC');
		$groupby 				= 'inaID';

		$inaID 					= $this->session->userdata('inaID');

		if(empty($inaID)){
			$where 				= "process = 'active'";
		}else{
			$where 				= "process = 'active' AND t1.id = $inaID";
		}

		if($this->permit->iibs && empty($this->permit->iibk) && empty($this->permit->ivpib)){
			$where  .= " AND t1.approval in ('3','2','1')";
		}elseif($this->permit->iibk && empty($this->permit->iibs) && empty($this->permit->ivpib)){
			$where  .= " AND t1.approval in ('2','3')";
		}elseif($this->permit->ivpib && empty($this->permit->iibk) && empty($this->permit->iibs)){
			$where  .= " AND t1.approval = '3'";
		}elseif($this->permit->iibs && $this->permit->iibk && $this->permit->ivpib){
			$where  .= " AND t1.id != 'NULL'";
		}

		if(empty($inaID)){
			$where 	.= $this->m_active->search_condition($subcat_select,$search_term,$start_date,$end_date);
		}

		$page 					= $this->uri->segment(3);
		$per_page	 			= 10;
		$offset 				= $this->crud->set_offset($page,$per_page);
		$total_rows				= count($this->crud->browse_join("",$table,"","","true",$select,$joins,$where,"",$groupby));
		$set_config 			= array('base_url'=>base_url().'active/sorting','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>'3');
		$config     			= $this->crud->set_config($set_config);

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();

		$data['pagination'] 	= $paging;
		$data['num']			= $offset;

		$browse					= $this->crud->browse_join_with_paging("",$table,"","","true",$select,$joins,$where,$orderby,$per_page,$offset,$groupby);

		$data['browse']			= $browse;
		
		//category select
		$selectsub 				= 'a.id as idsub,id_cat,subcategory,a.`status`,b.id,category';
		$tablecat				= 'subcategory a';
		$joinsub[0][0]			= 'category b';
		$joinsub[0][1]			= 'a.id_cat=b.id';
		$joinsub[0][2]			= 'left';
		$data['subcat']			= $this->crud->browse_join("",$tablecat,"","","",$selectsub,$joinsub);

		//category & subcat query
		$detail = array();
		foreach($browse as $row){
			if(!empty($row->vID)){
				$category 			= $this->category_hover($row->vID);
				$detail[$row->vID] 	= $category;
			}
		}
		
		$data['detail']			= $detail;
		$data['view']			= 'vendor/active/browse';

		if($this->input->is_ajax_request()){
			$this->load->view('vendor/active/v_data',$data);

		}else{
			$this->load->view('layout/template',$data);
		}
	}

	function ina_approve_form(){
		$id 			= $this->uri->segment(3);
		$table 			= "vendor_inactive t1";
		$select 		= "t1.id as inaID, t2.id as venID,t2.vendor_name,t2.register_num,t2.vendor_num,t2.vendor_type,t1.reason";
		$joins[0][0] 	= "vendor t2";
		$joins[0][1]	= "t2.id=t1.id_vendor";
		$joins[0][2]	= "left";

		$browse 		= $this->crud->browse_join("",$table,"t1.id",$id,"false",$select,$joins);
		$data['browse']	= $browse;

		$this->load->view('vendor/active/active_form',$data);

	}

	function ina_approve_act(){
		$id 		= $this->input->post('id_ina');
		$id_ven 	= $this->input->post('id_ven');
		$id_user 	= $this->tank_auth->get_user_id();
		$reason 	= $this->input->post('reason');
		$table 		= "vendor_inactive";
		$table2 	= "vendor";
		$subject 	= "Notification Active E-procurement Garuda";
		$action 	= "approve";

		$logs 		= array('id'	=> null,
			'id_vendor'		=> $id_ven,
			'user_id'		=> $id_user,
			'action'		=> 'active',
			'reason'		=> $reason,
			'status'		=> 1,
			'created_date'	=> date('Y-m-d H:i:s'));

		if($this->permit->iibs){
			$app  					= "2";
			$logs['description'] 	= "Approved and process to SM IBK";

			$group 					= "sm_ibk";
		}elseif($this->permit->iibk){
			$app  = "3";
			$logs['description'] 	= "Approved and process to VP IB";

			$group 					= "vp_ib";
		}elseif($this->permit->ivpib){
			$app  					= "3";
			$data['status'] 		= "approved";
			$data2['id']			= $id_ven;
			$data2['status'] 		= "active";
			$logs['description'] 	= "Approved to active";

			$group 					= "as";

			$this->crud->update("",$table2,"id",$id_ven,$data2);
		}
		$data['approval'] 		= $app;
		$data['last_updated']	= date('Y-m-d H:i:s');

		$this->sendingMail($group,$id_ven,$reason,$subject,$action,$id);

		$this->crud->update("",$table,"id",$id,$data);
		$this->m_active->create_log("vendor_logs",$logs);
	}

	function ina_reject_act(){
		$id 		= $this->input->post('id_ina');
		$id_ven 	= $this->input->post('id_ven');
		$id_user 	= $this->tank_auth->get_user_id();
		$reason 	= $this->input->post('reason');
		$table 		= "vendor_inactive";
		$action 	= "reject";
		$subject 	= "Notification Reject Active E-procurement Garuda";

		$data 		= array(
			'status'		=> 'rejected',
			'last_updated'	=> date('Y-m-d H:i:s')
			);

		$logs 		= array('id'	=> null,
			'id_vendor'		=> $id_ven,
			'user_id'		=> $id_user,
			'reason'		=> $reason,
			'action'		=> "active",
			'description'	=> "Rejected active process",
			'status'		=> 1,
			'created_date'	=> date('Y-m-d H:i:s'));

		$this->sendingMail('as',$id_ven,$reason,$subject,$action,$id);

		$this->m_active->create_log("vendor_logs",$logs);
		$this->crud->update("",$table,"id",$id,$data);
	}

	function export_excel(){
		$id 					= $this->uri->segment(3);
		$check 					= !$id ? $this->input->post('ck_list') : $id;
		$filter 				= !$id ? implode(',',$check) : $id;

		$table 					= "vendor_inactive t1";
		$select 				= "t1.id as inaID,t2.id as vID,t2.register_num,vendor_num,vendor_name,t1.last_updated,t1.reason,t1.status,t1.approval";
		$joins[0][0]			= 'vendor t2';
		$joins[0][1]			= 't1.id_vendor=t2.id';
		$joins[0][2]			= 'left';

		$groupby 				= 'inaID';
		$orderby				= array('field'=>'t1.last_updated','order'=>'DESC');
		$where 					= "t1.id IN($filter)";


		if(!empty($check)){
			$browse 		 	= $this->crud->browse_join("",$table,"","","true",$select,$joins,$where,$order);
			$data['inaexcel'] 	= $browse;

			$detail = array();
			foreach($browse as $row){
				if(!empty($row->vID)){
					$category 			= $this->category_hover($row->vID,2);
					$detail[$row->vID] 	= $category;
				}
			}

			$data['detail']			= $detail;


			$this->load->view('vendor/active/export_excel',$data);

		}else{
			$this->index();
		}
	}

	private function category_hover($id,$mode){

		$table 			= "vendor t1";
		$select 		= "t1.id,register_num,vendor_num,vendor_name,t1.created_date,(SELECT category FROM category WHERE id=t2.id_category) as category,(SELECT subcategory FROM subcategory WHERE id=t2.id_subcat) as subcategory,vendor_type,t1.`status`,t2.id_subcat";
		$joins[0][0]	= 'vendor_category t2';
		$joins[0][1]	= 't1.id=t2.id_vendor';
		$joins[0][2]	= 'left';
		$joins[1][0]	= 'category t3';
		$joins[1][1]	= 't2.id_category=t3.id';
		$joins[1][2]	= 'left';
		$joins[2][0]	= 'subcategory t4';
		$joins[2][1]	= 't4.id=t2.id_subcat';
		$joins[2][2]	= 'left';

		$where 		= "vendor_type != 'new' AND t2.id_vendor= $id";

		$browse 	= $this->crud->browse_join("",$table,"","","true",$select,$joins,$where);

		$num = 1;

		if(!empty($mode)){
			foreach($browse as $row){
				$cat .= $num.'.'.$row->category.' - '.$row->subcategory.' ';
				$num++;
			}
		}else{
			foreach($browse as $row){
				$cat .= $num.'.'.$row->category.' - '.$row->subcategory.'<br>';
				$num++;
			}
		}

		return $cat;
	}

	function sendingMail($group,$idven,$reason,$subject,$action,$inaID=""){
		$select  = "vendor_num as num,vendor_name as name,email";
		$vendor  = $this->crud->browse("","vendor","id",$idven,"false",$select);
		$ven_num = (!empty($vendor->num)) ? $vendor->num : " - ";

		$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
		(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";
		$cat 	 = $this->crud->browse("","vendor_category l","id_vendor",$idven,"true",$select2);

		$recipient = $this->m_user->get_userdata_group($group);

		foreach($recipient as $value)
		{
			if(filter_var($value->email, FILTER_VALIDATE_EMAIL))
			{
				$user    =  $this->users->get_user_by_email($value->email);
				if($user){			 			
					$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'active_vend','param'=>$inaID);							 
					$link    = $this->m_vendor->create_session_link((object) $require);
					$to   	 =  $value->email;
					$param   =  array('ven_num'		=> $ven_num,
						'vendor_name' 	=> $vendor->name,
						'category'		=> $cat,
						'reason'		=> $reason,
						'link'			=> $link,
						'to'			=> $value->username);
					if($action == 'approve'){
						if($group != "as"){
							$param['view']  = NOTIF_PATH."EPCGA044";
							$message =  $this->load->view(NOTIF_TMPL,$param,true);

							/*$message =  $this->load->view('vendor/active/notification/form1',$param,true);*/
						}elseif($group == "as"){
							$param['view']  = NOTIF_PATH."EPCGA046";
							$message =  $this->load->view(NOTIF_TMPL,$param,true);

							/*$message =  $this->load->view('vendor/active/notification/form2',$param,true);*/
						}
					}elseif($action == 'reject'){
						$param['view']  = NOTIF_PATH."EPCGA047";
						$message =  $this->load->view(NOTIF_TMPL,$param,true);

						/*$message =  $this->load->view('vendor/active/notification/form3',$param,true);*/

					}

					$this->crud->sendMail($to,$subject,$message);
				}
			}
		}

	}

}

/* End of file active.php */
/* Location: ./application/controllers/active.php */