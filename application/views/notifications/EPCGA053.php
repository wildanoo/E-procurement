
Dengan hormat,<br/>
<b>Bapak/Ibu <?php echo $to; ?></b><br/>

Bersama ini kami sampaikan bahwa telah terjadi perubahan data vendor yang membutuhkan proses approval dari Bapak/Ibu <?php echo $to; ?>. 
<br/><br/>
Berikut ini adalah informasi detail vendor.
<br/>
<table>
	<tr>
		<td>Nama Perusahaan</td>
		<td>:</td>
		<td><?php echo $vendor_name; ?></td>
	</tr>
	<tr>
		<td>No. Registration</td>
		<td>:</td>
		<td><?php echo $reg_num ?></td>
	</tr>
</table>

Untuk proses persetujuan dan detail informasi perusahaan dapat diakses :<br/><br/>
<?php echo $link; ?><br/>

Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/><br/>

(&nbsp;<b><?php echo ucfirst( $this->tank_auth->get_username()); ?></b>&nbsp;)

