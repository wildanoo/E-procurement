 
 
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">

              <div class="box-header with-border">
                <h3 class="box-title">Update Content Level</h3>
              </div><br/>
              <!--form class = "form-horizontal"-->
              <?php echo form_open("level/update",array('class'=>'form-horizontal')); ?>
              <table width="50%">
              	<tr><td>Group</td><td>:</td>
				<td><?php 
					$prop_cat = 'class="form-control" id="id_group" style="width:220px"';
					echo form_dropdown('id_group',$group,$def->id_group,$prop_cat); ?>		
				</td>           	
              	<tr><td>Subcontent </td><td>:</td>
              	<td><?php 
              			 echo form_hidden("id",$def->id);
              			 echo form_hidden("ttl",count($sub));
              			 $dbchecked = explode(",",$def->id_subcontent);
              			 $i = 1;
                         foreach($sub as $row){
						 	$value = $row->id;
						 	$checked = in_array($value,$dbchecked) ? true: false ;
						 	echo form_checkbox('check'.$i,$value, $checked); echo $row->subtype_name."<br/>";
						$i++; }  ?>   
              	</td></tr>
              </table>
                  </div><!-- col-md-6 -->                
                </div><!-- /.box-body -->
                <div class="box-footer" style="padding-left: 30px;">
                    <?php  $prop = array('class'=>'btn btn-sm btn-default','title'=>'Cancel'); ?>					 
					<?php  echo anchor('level/','Cancel',$prop); ?>
                    <?php  echo form_submit('submit', 'Update','class="btn btn-sm btn-primary"'); ?>
                </div><!-- /.box-footer -->
              <?php echo form_close(); ?>
            </div><!-- /.box -->
          </div>
        </div>
      