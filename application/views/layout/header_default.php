<head>
	<title>E-Procurement Garuda</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/skins/red-blue/blue.css" id="colors" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/layerslider.css"/> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/component.css" />
	<!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/iCheck/square/blues.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reset.css">
	<link rel="icon" href="<?php echo base_url(); ?>assets/favicon-grd.png" sizes="16x16">
	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#my-select').searchableOptionList();
	 		$('#create').click(function(){
 				var baseUrl   = "<?php echo base_url(); ?>";
 				var login     = $('#login').val();
 				var password  = $('#password').val();
 				var remember  = $('#remember').val();
 				var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>';
				var token     = '<?php echo $this->security->get_csrf_hash(); ?>';

				$.ajax({
	            	url  : baseUrl + 'auth/validate_login',
	            	type : "POST",
	            	data : csrf +'='+ token +'&login='+login+'&password='+password+'&remember='+remember,
	            	success: function(resp){
	               		//$('#debug').html(resp);
	               		var json = $.parseJSON(resp);
	               		if(json.status=="false"){
							$("#message").html(json.errors);
						} else {
							document.location.href= baseUrl + 'auth/login';
						}
					}
	        	});
			});
		});
	</script>
	<script src="<?php echo base_url(); ?>assets_/js/sol.js"></script>	
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets_/css/sol.css"/>