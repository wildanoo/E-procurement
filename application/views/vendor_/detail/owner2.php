<div id="container4">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";	
   
   $('#btn_create4').click(function(){  
   		$("#container4").load(baseUrl+'vendor/form_owner');   		
   });             	
   
 }); 
 
 function update_owner(id){
 	
 	$("#container4").load(baseUrl+'vendor/form_update_owner/'+id); 
 }
 
 function delete_owner(id){
 	var baseUrl = "<?php echo base_url(); ?>";
 	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
 	
 	if (confirm('Are you sure you want to delete owner?')) {	
		$.post( baseUrl + "vendor/delete_owner", { csrf: token, id: id } );
		$("#container4").load(baseUrl+'vendor/load_owner');		
	}
	
 } 
  
 </script>
 
<button type="button" id="btn_create4" class="btn btn-info btn-primary">Add New</button> 
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Owner Name</th>
		<th>Owner Address</th>
		<th>Owner Phone</th>
		<th>Owner Position</th>
		<th>Owner Shared</th>
		<th>Remark</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>

 <?php  //$owner = array();
 if($owner){ $i=1;
 	foreach($owner as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->owner_name; ?></td>	
			<td><?php echo $row->owner_address; ?></td>
			<td><?php echo $row->owner_phone; ?></td>
			<td><?php echo $row->owner_position; ?></td>
			<td><?php echo $row->owner_shared; ?></td>				
			<td><?php echo $row->remark ?></td>
			<td style="text-align: center;">
			<?php
					echo '<i class="fa fa-pencil" onclick="update_owner('.$row->id.')"></i>'; echo '&nbsp;&nbsp;&nbsp;';					
					echo '<i class="fa fa-trash"  onclick="delete_owner('.$row->id.')"></i>';	
			?></td>											
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>