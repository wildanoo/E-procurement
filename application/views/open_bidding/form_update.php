<meta name="csrf-token" content="<?php echo $this->security->get_csrf_token_name(); ?>">
<meta name="csrf-hash" content="<?php echo $this->security->get_csrf_hash(); ?>">
<meta name="form-decider" content="form-update">
<script src="<?php echo base_url() ?>assets_/js/open_bidding.js"></script>

<style type="text/css">
	.valign-top td{
		vertical-align: top !important;
	}
</style>

<div class="page-header" style="vertical-align: middle">
  <h3 style="display:inline-block">Update Content - Open Bidding</h3>
</div> 
<?php
if($this->session->flashdata('error_file_upload')) {
echo '<div class="alert alert-warning">';
  echo '<strong><i class="fa fa-exclamation-triangle fa-1x" aria-hidden="true"></i> </strong>'.$this->session->flashdata('error_file_upload');
echo '</div>';
echo '<br>';
}
?>
<div id="debug"></div>
<?php echo form_open_multipart('open_bidding/update',array('id'=>'form'));$current_date = date('Y/m/d')?>

<table width="100%" border="0">
<tr class="valign-top">
	<td><label>Title</label></td><td>:</td>
	<td class="form_tab_content" width="75%;">
		<div style="margin-top: -5px;">
			<ul class="nav nav-tabs test2">
			    <li class="active"><a data-toggle="tab" href="#t_idn" aria-expanded="true">Indonesia</a></li>
			    <li class=""><a data-toggle="tab" href="#t_eng" aria-expanded="false">English</a></li>
			</ul>

			<div class="tab-content">
			    <div id="t_idn" class="tab-pane fade active in" style="padding:15px;">
			      <input class="form-control" value="<?php echo $def->title_ind; ?>" type="text" name="title_ind" id="title" placeholder="Judul" class="form-control" required/>
			    </div>
			    <div id="t_eng" class="tab-pane fade" style="padding:15px;">
			      <input class="form-control" value="<?php echo $def->title_eng; ?>" type="text" name="title_eng" id="title" placeholder="Title" class="form-control" />
			    </div>
			</div>
		</div>
	</td>
</tr>
<tr>
	<td><label>Category</label></td><td>:</td>
	<td><?php 
		$prop_cat = 'class="form-control" id="id_cat" required';
		echo form_dropdown('id_cat',$category,$def->id_cat,$prop_cat); ?>
	</td>	
</tr>
<tr>
	<td><label>Sub Category</label></td><td>:</td>
	<td>
		<select name="id_subcat" id="id_subcat" class="form-control" required>
			<?php if($subcat){ ?>		
			<option value="<?php echo $subcat->id; ?>"><?php echo $subcat->subcategory; ?></option> <?php } else { ?>
			<option value="">--Select Category--</option><?php } ?> 		
		</select>		
	</td>	
</tr>
<?php echo form_hidden('id',$def->id); ?>
<tr id="company_area">
	<td><label>Company</label></td><td>:</td>
	<td>
		<?php
		$prop_company = 'class="form-control" id="id_company" required';			
		echo form_dropdown('id_company',$company,$def->id_company,$prop_company); ?>
	</td>
</tr>
<tr id="area">
	<td><label>Area</label></td><td>:</td>
	<td><?php
		$prop_area = 'class="form-control" id="id_area" required';
		echo form_dropdown('id_area',$areas,$def->id_region,$prop_area); ?>
	</td>
</tr>
<tr id="office_area">
	<td><label>Office</label></td><td>:</td>
	<td><?php
		$prop_office = 'class="form-control" id="id_office" required="true"';
		echo form_dropdown('id_office',$offices,$def->id_office,$prop_office); ?>
	</td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr class="valign-top">
	<td><label>Overview</label></td><td>:</td>
	<td class="form_tab_content" width="75%;">
		<div style="margin-top: -5px;">
			<ul class="nav nav-tabs">
			    <li class="active"><a data-toggle="tab" href="#d_idn" aria-expanded="true">Indonesia</a></li>
			    <li class=""><a data-toggle="tab" href="#d_eng" aria-expanded="false">English</a></li>
			</ul>

			<div class="tab-content">
			    <div id="d_idn" class="tab-pane fade active in" style="padding:15px;">
			      (<span id="count-char"><strong><?php echo (250-strlen($def->detail_ind)); ?></strong></span> chars left)
			      <textarea name="detail_ind" id="detail_ind" maxlength="250" cols="20" rows="5" class="form-control detail-section" required><?php echo $def->detail_ind; ?></textarea>
			    </div>
			    <div id="d_eng" class="tab-pane fade" style="padding:15px;">
			      (<span id="count-char"><strong><?php echo (250-strlen($def->detail_eng)); ?></strong></span> chars left)
			      <textarea name="detail_eng" id="detail_eng" maxlength="250" cols="20" rows="5" class="form-control detail-section" ><?php echo $def->detail_eng; ?></textarea>
			    </div>
			</div>
		</div>
	</td>
</tr>
<?php echo form_hidden("creator",$username); ?>
<tr id="option">
 <td><label>Description</label></td><td>:</td>
  <td>
	  <input type="radio" name="content" value="1" id="radio1" onblur="lostfocus()" checked> Typing &nbsp;&nbsp;
	  <input type="radio" name="content" value="2" id="radio2" onblur="lostfocus()"> Upload<br>
  </td>
</tr>
<tr id="wyswyg">
	<td colspan="2">&nbsp;</td>
	<td class="form_tab_content" width="75%;">
		<div>
			<ul class="nav nav-tabs test2">
			    <li class="active"><a data-toggle="tab" href="#idn" aria-expanded="true">Indonesia</a></li>
			    <li class=""><a data-toggle="tab" href="#eng" aria-expanded="false">English</a></li>
			</ul>

			<div class="tab-content">
			    <div id="idn" class="tab-pane fade active in" style="padding:15px;">
			      <?php $data['ideditor']='editor_ind';$data['lbl_editor']='';$data['content'] = $contents[0]; $this->load->view('announce/note',$data); ?>
			    </div>
			    <div id="eng" class="tab-pane fade" style="padding:15px;">
			      <?php $data['ideditor']='editor_eng';$data['lbl_editor']='';$data['content'] = $contents[1]; $this->load->view('announce/note',$data); ?>
			    </div>
			</div>
		</div>
	</td>
</tr>
<tr id="upload1">
	<td colspan="2">&nbsp;</td>
	<td class="form_tab_content" width="75%;">
		<div>
			<ul class="nav nav-tabs test2">
			    <li class="active"><a data-toggle="tab" href="#upidn" aria-expanded="true">Indonesia</a></li>
			    <li class=""><a data-toggle="tab" href="#upeng" aria-expanded="false">English</a></li>
			</ul>
			<?php $style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";?>
			<div class="tab-content">
			    <div id="upidn" class="tab-pane fade active in" style="padding:15px;">
			      <?php	
					$data  = array('type'=>'file','name'=> 'contentfileind','id' => 'file_upload_ind','value'=>'true','content'=>'Import','style'=>$style); 
					echo form_input($data); ?> <i>*.txt</i>
			    </div>
			    <div id="upeng" class="tab-pane fade" style="padding:15px;">
			      <?php	
					$data  = array('type'=>'file','name'=> 'contentfileeng','id' => 'file_upload_eng','value'=>'true','content'=>'Import','style'=>$style); 
					echo form_input($data); ?> <i>*.txt</i>
			    </div>
			</div>
		</div>
	</td>
</tr>
<tr id="upload2">
	<td><label>Image</label></td><td>:</td>
	<td><?php	
		$data2  = array('type'=>'file','name'=> 'imagefile','id' => 'file_upload','value'=>'true','content'=>'Import','style'=>$style); 
		echo form_input($data2); ?><?php echo $imgfile; ?>&nbsp;<i>*.jpg,gif,png</i>
	</td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr class="valign-top">
	<td><label>Validity Period</label></td><td>:</td>	
	<td>
		<ul class="list-inline" style="width: 100%">
		    <li style="width: 48%; vertical-align: middle;"><input type="text" onkeydown="return false" value="<?php echo str_replace("-","/",$def->first_valdate); ?>" class="form-control date_picker" id="first_valdate" name="first_valdate"></li>
		    <li class="text-center" style="width: 3%; vertical-align: middle;">to</li>
		    <li style="width: 48%; vertical-align: middle;"><input type="text" onkeydown="return false" value="<?php echo str_replace("-","/",$def->end_valdate); ?>" class="form-control date_picker" id="end_valdate" name="end_valdate"></li>
	    </ul>
	</td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr class="valign-top">
	<td><label>Publish Date</label></td><td>:</td>
	<td><input type="text" value="<?php echo str_replace("-","/",$def->publish_date); ?>"  class="form-control" id="publish_date" onkeydown="return false" name="publish_date"></td>
</tr>
<tr><td colspan="3"><hr/></td></tr>
<tr id="aanwidzing_option" class="valign-top">
 	<td><label>Aanwijzing Agenda</label></td><td>:</td>
 	<td class="form_tab_content" width="75%;">
		<div style="margin-top: -5px;">
			<ul class="nav nav-tabs test2">
			    <li class="active"><a data-toggle="tab" href="#aanwidzing_idn" aria-expanded="true">Indonesia</a></li>
			    <li class=""><a data-toggle="tab" href="#aanwidzing_eng" aria-expanded="false">English</a></li>
			</ul>
			<div class="tab-content">
			    <div id="aanwidzing_idn" class="tab-pane fade active in" style="padding:15px;">
 			    	<?php $data['content'] = $aanwijzing[0]; $data['idaanwidzing']='idaanwidzing_ind'; $this->load->view('announce/aanwidzing',$data); ?>
 			    </div>
 			    <div id="aanwidzing_eng" class="tab-pane fade" style="padding:15px;">
 			    	<?php $data['content'] = $aanwijzing[1]; $data['idaanwidzing']='idaanwidzing_eng'; $this->load->view('announce/aanwidzing',$data); ?>
 			    </div>
			</div>
		</div>
	</td>
</tr>
<tr>
	<td><label>RFP</label></td><td>:</td>
	<td><?php	
		$datarfp  = array('type'=>'file','name'=> 'rfpfile','id' => 'file_upload_pdf','value'=>'true','content'=>'Import','style'=>$style); 
		echo form_input($datarfp); ?><?php echo $rfpfile; ?>&nbsp;<i>*.pdf</i>
	</td>
</tr>
<tr><td colspan="3"><hr/></td></tr>
</table>
<div class="panel panel-default">
<div class="panel-heading"><strong>Attachment</strong></div>
<div class="panel-body">
<a style="width: 10%;" data-toggle="modal" data-target="#dialog-attachment" class="btn btn-primary btn-sm" aria-expanded="false">Add</a>
<div class="container-attachment"> <?php $this->load->view('open_bidding/attachment',$data); ?> </div>
</div>
</div>
<hr/>
<table width="100%" style="height:80px;">
	<tr>
		<td colspan="3" style="text-align: right;">
			<button style="vertical-align: baseline;" type="submit" value="submit" name="button-process" class="btn btn-primary btn-sm"/>Submit</button>
			<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>"Cancel", 'style'=>"vertical-align: baseline;"); ?>
			<?php  echo anchor('announce/','Cancel',$prop); ?>
		</td>
	</tr>
</table>
<?php echo form_close(); ?>
<?php echo form_open_multipart('',array('id'=>'attachment-upload'));?>
<div id="dialog-attachment" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Attachment</h4>
      </div>
      <div class="modal-body form-horizontal">
      	<div class="form-group">
      		<label style="line-height: 35px;" class="col-sm-2" for="attachment-description">Description:</label>
      		<div class="col-sm-10">
      			<input type="text" name="attachment-description" id="attachment-description" class="form-control"></input>
      		</div>
      	</div>
      	<div class="form-group">
      		<label style="line-height: 35px;" class="col-sm-2" for="attachment-remarks">Remarks:</label>
      		<div class="col-sm-10">
      			<input type="text" name="attachment-remarks" id="attachment-remarks" class="form-control"></input>
      		</div>
      	</div>
      	<div class="form-group">
      		<label style="line-height: 35px;" class="col-sm-2" for="attachment-file">Attachment:</label>
      		<div class="col-sm-10">
      			<div id="attachment-uploader">
      			</div>
      			<i>*.pdf</i>
      		</div>
      	</div>
      </div>
      <div class="modal-footer">
      	<button type="submit" class="btn btn-primary process-winner" id="submit-winner" style="margin-bottom:0;">Submit</button>
        <button type="button" class="btn btn-warning" style="margin-bottom:0;" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<?php echo form_close();?>