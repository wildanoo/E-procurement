<script type="text/javascript">
$(document).ready(function(){
	var baseUrl   = "<?php echo base_url(); ?>";		 
   $(function(){    });
	  
   $('#id_office').change(function(){  
	   		var id_office = $('#id_office').val(); //alert(id_office);
	   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	           $.ajax({
	               url  : baseUrl + 'auth/get_division',
	               type : "POST",              		               
	               data : csrf +'='+ token +'&id_office='+id_office,
	               success: function(resp){
	               	  $('#id_div').html(resp);
	               	  $("#id_dept").val(""); 
	               	  //$('#debug').html(resp);
	               }
	            });
	     });

   $('#id_div').change(function(){  
	   		var id_division = $('#id_div').val(); 
	   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	           $.ajax({
	               url  : baseUrl + 'auth/get_department',
	               type : "POST",              		               
	               data : csrf +'='+ token +'&id_division='+id_division,
	               success: function(resp){
	               	  $('#id_dept').html(resp);  
	               	  //$('#debug').html(resp);             	  
	               }
	            });
	     });
   
   $('#nopeg').change(function(){  
  		var nopeg = $('#nopeg').val();
  		var id    = $('#id').val(); 
  		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
          $.ajax({
              url  : baseUrl + 'employee/validate_nopeg',
              type : "POST",              		               
              data : csrf +'='+ token +'&nopeg=' + nopeg + '&id=' + id, 
              success: function(resp){
            	  //$('#debug').html(resp); 
                  if (resp == "false") { 
                	  $("#btn_submit").attr("disabled", true);
                      var msg = "Employee number is exist";
                      $("#msg").html(msg);
                      } else { $("#btn_submit").attr("disabled", false); $("#msg").html(''); }
					            	  
              }
           });
    });

   $('#btn_submit').click(function(){	   	  
	  	var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	    
	    var values   = $("#form").serializeArray();
		values.push({name: csrf,value: token});
		values = jQuery.param(values);			
		 	$.post( baseUrl+ "employee/validate_update", values ).done(function( resp ) {	//$("#debug").html(resp);
			  		var json = $.parseJSON(resp); 
			  		if(json.status=="true"){ $("#form").submit();						
					} else {					
						$("#msg1").html(json.msg1);
						$("#msg2").html(json.msg2);
						$("#msg3").html(json.msg3);
						$("#msg4").html(json.msg4);	
						$("#msg4").html(json.msg5);	
					}		    	
			    	
			});
			
			
    	
    });
});	
</script>
<div id="debug"></div>
<div class="page-header">
<h3>Edit Employee</h3>
</div>
<?php echo form_open('employee/update',array('id'=>'form')); ?>
<input type="hidden" id="id" name="id" value="<?php echo $def->id; ?>"/>
<table cellpadding="2" cellspacing="2" width="80%">
<tr>
	<td><label>Nomor Pegawai</label></td><td>:</td>
	<td><input type="text" name="nopeg" id="nopeg" placeholder="Nomor Pegawai" value="<?php  echo $def->nopeg; ?>" class="form-control" required/>		
	</td>	
	<td><div id="msg" style="color: red;"></div></td>
</tr>
<tr>
	<td><label>Nama Lengkap</label></td><td>:</td>
	<td><input type="text" name="fullname" id="fullname" placeholder="Nama Lengkap" value="<?php  echo $def->fullname; ?>" class="form-control" required/>		
	</td>	<td><div id="msg1" style="color: red;"></div></td>	
</tr>
<tr>
	<td><label>Office</label></td><td>:</td>
	<td><?php $prop_office = 'id="id_office" class="form-control"';
          echo form_dropdown("id_office",$office,$def_office->id_office,$prop_office);?></td>
    <td><div id="msg2" style="color: red;"></div></td>	
</tr>
<tr>
	<td><label>Division</label></td><td>:</td>
	<td><?php $prop_div = 'id="id_div" class="form-control"';
          echo form_dropdown("id_div",$division,$def_div->id_div,$prop_div);?></td>	
    <td><div id="msg3" style="color: red;"></div></td>
</tr>
<tr>
	<td><label>Department</label></td><td>:</td>
	<td><?php $prop_dept = 'id="id_dept" class="form-control"';
          echo form_dropdown("id_dept",$department,$def_dept->id_dept,$prop_dept);?></td>
	<td><div id="msg4" style="color: red;"></div></td>	
</tr>
<tr>
<tr>
	<td><label>Position</label></td><td>:</td>
	<td><?php $prop_position = 'id="id_position" class="form-control"';
        echo form_dropdown("id_position",$position,$def_position->id_position,$prop_position);?></td>	
        <td><div id="msg5" style="color: red;"></div></td>
</tr>	
</tr>
<tr>
	<td><label>Functional</label></td><td>:</td>
	<td><?php $prop_functional = 'id="id_functional" class="form-control"';
        echo form_dropdown("id_functional",$functional,$def_functional->id_functional,$prop_functional);?></td>	
</tr>
<tr>	
	<td colspan="3" style="text-align: right;">
		<input type="button" id="btn_submit" value="Submit" class="btn btn-primary btn-sm"/>
		<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>'Cancel'); ?>					 
		<?php  echo anchor('employee/','Cancel',$prop); ?></td>
</tr>
</table>
<?php echo form_close(); ?>