<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_vendorlog extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('crud');
	}

	function authenticate(){
		$allowed  	= array(74);
		$permit  	= $this->session->userdata('permissions');
		$permit 	= explode(",",$permit);
		foreach($permit as $str){
			$permissions[] = preg_replace("/[^0-9]/","",$str);
		}
		$result 	= array_intersect($allowed, $permissions);
		if($result){ return true;} return false;
	}

	function sort_stat(){
		$stat = array(
			'redlist' => 'Redlist',
			'blacklist' => 'Blacklist',
			'active' => 'Active',
			'inactive' => 'Inactive'
			);

		return $stat;
	}

	function get_logs($params = array(), $venID, $stat="",$ID_stat=""){

		$q = "SELECT t1.id,t1.id_vendor,t1.user_id,t1.created_date,t3.username,t2.vendor_name,t1.action,t1.description,t1.reason
		FROM vendor_logs t1
		LEFT JOIN vendor t2
		ON t1.id_vendor=t2.id
		LEFT JOIN users t3
		ON t3.id=t1.user_id
		";

		if($stat == 'true'){
			$q .= " WHERE t1.id_vendor = $venID AND t1.action = '$ID_stat'";
		}else{
			$q .= " WHERE t1.id_vendor = $venID ";
		}

		$q .= " ORDER BY t1.id DESC";

		if(array_key_exists("start",$params) && array_key_exists("limit",$params))
		{
			$q .= " LIMIT $params[start],$params[limit] ";
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
		{
			$q .= " LIMIT $params[limit] ";
		}
		/*print $q;
		exit();*/
		$r = $this->db->query($q);
		return $r->result();
	}

}

/* End of file m_vendorlog.php */
/* Location: ./application/models/m_vendorlog.php */