<script type="text/javascript">
  $(function () { 
   	  var baseUrl  = "<?php echo base_url(); ?>"; 
   	  
   	  $('#btn_cancel').click(function(){ 
   			$("#container11").load(baseUrl+'due_diligence/load_visit_form');   		
   	   }); 
   	  
   	  $('#btn_visitor').click(function(){ 
   	   		var id_visit = $("#id_visit").val();
   			$("#container11").load(baseUrl+'due_diligence/add_participant/' + id_visit);   		
   	  });    	  
   	  
  });
  
  function delete_participant(id){  	
 		var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';  	
  		if (confirm('Are you sure you want to delete this?')) {  			
  			$.post( baseUrl+ "due_diligence/delete_participant", { csrf: token , id: id } )
			  .done(function( resp ) {
			  		var id_visit = $("#id_visit").val();
			    	$("#container11").load(baseUrl+'due_diligence/load_participant/'+ id_visit);
			});
  		}	  	
  }

</script>

<div style="text-align: right;">
<button type="button" id="btn_cancel" class="btn btn-warning">Cancel</button> 
<?php if($this->permit->md3){ ?>
<button type="button" id="btn_visitor" class="btn btn-info btn-primary">Add New</button> 
<?php } ?>
</div>

<input type="hidden" name="id_visit" id="id_visit" value="<?php echo $id_visit; ?>"/>
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Visitor Name</th>		
		<th>Unit</th>
		<th>Created Date</th>
		<?php if($this->permit->md3){ ?><th>Action</th><?php } ?>
	</tr>
	</thead>
	<tbody>
 <?php   
 if($participant){ $i=1;
 	foreach($participant as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->visitor_name; ?></td>	
			<td><?php echo $row->unit; ?></td>		
			<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
			<?php if($this->permit->md3){ ?>			
			<td style="text-align: center;">			
			<?php 
				echo '<i class="fa fa-trash"  title="Delete Participant" onclick="delete_participant('.$row->id.')"></i>'; 
			?></td><?php } ?>
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="5">-</td></tr>
<?php } ?>
     </tbody>
</table>

