<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Category extends Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_category', 'm_category');
		$this->load->model('crud', 'crud');
		$this->load->library('session');
		
		$this->limit = 5;
		islogged_in();
		$this->load->model('m_user');
		islogged_in();
		$this->m_user->authenticate(array(43));
	}	
		
	function index(){
		//if ($this->tank_auth->is_logged_in()) {

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			
			//$where =  array('status'=>'1');
			
			$sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');

			
			$field      = $sess_field != '' ? $sess_field : "init_code";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
			
			if($sess_cat != ''){	$where = array('init_code'=>$sess_cat);
			} else { $where = "";	}
			
			//$sess_like  = $this->session->flashdata('like');

	 		$table 	    = "category";		
			$page       = $this->uri->segment(3);
			$per_page   = 5;		
			$offset     = $this->crud->set_offset($page,$per_page);
			$total_rows = $this->crud->get_total_record("",$table,$where);
			$set_config = array('base_url'=> base_url().'/category/index','total_rows'=>$total_rows,'per_page'=>5,'uri_segment'=>3);
			$config     = $this->crud->set_config($set_config);		
				 
			$this->load->library('pagination');			 
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();			
				
			$order 			     = array('field'=>'created_date','order'=>'DESC');
			$data['pagination']  = $paging;
			$data['num']         = $offset; 
			$select 			 = "id,init_code,category,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
			
			$sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			$data['browse'] = $browse;
			
			$category = $this->crud->browse("","category","","","true","id,init_code");
			if(!$category) $category = array();
			$select = array(''=>'All');
			foreach($category as $val){ $options[$val->init_code] = $val->init_code; }
			$data['init_code'] = $select + $options;
			
			$category_ = $this->crud->browse("","category","","","true","id,category,init_code");
			if(!$category_) $category_ = array();
			$select_ = array(''=>'All');
			foreach($category_ as $val_){ $options_[$val_->id] = $val_->category; }
			$data['category'] = $select_ + $options_;
						
			$data['view']	= "supplier/category/browse";
			$this->load->view('layout/template',$data);

	 		
// 		} else {  
// 			$this->session->set_flashdata('message','user not authorized');
// 			redirect('/auth/login/');	 
// 		}
	}	
	
	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
	
		//$search_dateranges = array('publish_date',$_POST['start_date'],$_POST['end_date']);
		$search_conditions = array(
				'category'		=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		// 		$data['category'] 		 = $this->getCategories();
		// 		$data['subcategory'] 	 = $this->getSubcategories();
		// 		$data['subcontent_type'] = $this->getSubcontentTypes();
		// 		$data['status_ref'] 	 = $this->getStatusCondition();
	
		$data['view'] = "supplier/category/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{
		// 		$subcontent_allowed = globalNeedAanwidzing();
	
		// 		$sess_cat = $this->session->userdata('sess_cat');
		// 		$sessID   = $this->session->flashdata('anID');
			
		// 		$field    = $sess_cat ? "id_cat" : "";
		// 		$id_cat   = $sess_cat ? $sess_cat  : "";
	
		// 		if($sess_cat){
		// 			$where = "t1.subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'";
		// 		} else {
		// 			$where = !$this->session->userdata('is_admin') ? "subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'":"";
		// 		}
	
		/* get data for table view from database with pagination */
	
		$table 	     = "category t1";
		$select 	 = "id,init_code,category,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		//$order 		 = array('field'=>'t1.publish_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		// if($sessID)	$getData = $this->crud->browse("",$table,"id",$sessID,"true",$select,$where);
		// else  {
		// 		$joins[0][0] = 'announcement_level_approval t2';
		// 		$joins[0][1] = 't2.id_announcement = t1.id';
		// 		$joins[0][2] = 'left';
		// 		$joins[1][0] = 'level t3';
		// 		$joins[1][1] = 't2.id_level = t3.id';
		// 		$joins[1][2] = 'left';
	
		// $where		 = !$this->session->userdata('is_admin') ? "t1.subcontent_type IN ('4','5') AND t1.status = '3'":"";
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'country/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
		// 		$subcontent_allowed = globalNeedAanwidzing();
	
		// 		$sess_cat = $this->session->userdata('sess_cat');
		// 		$sessID   = $this->session->flashdata('anID');
			
		// 		$field    = $sess_cat ? "id_cat" : "";
		// 		$id_cat   = $sess_cat ? $sess_cat  : "";
	
		// 		if($sess_cat){
		// 			$where = "t1.subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'";
		// 		} else {
		// 			$where = !$this->session->userdata('is_admin') ? "subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'":"";
		// 		}
	
		/* get data for table view from database with pagination */
	
		$table 	     = "category t1";
		$select 	 = "id,init_code,category,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		//$order 		 = array('field'=>'t1.publish_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);
	
		// 		$joins[0][0] = 'announcement_level_approval t2';
		// 		$joins[0][1] = 't2.id_announcement = t1.id';
		// 		$joins[0][2] = 'left';
		// 		$joins[1][0] = 'level t3';
		// 		$joins[1][1] = 't2.id_level = t3.id';
		// 		$joins[1][2] = 'left';
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,$where,$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'category/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
    
    function get_cat_tags(){ 	
	 	$term   = $_GET['term'];  	
	 	$order  = array('field'=>'category','sort'=>'ASC'); $select = "category as name";
	 	$result = $this->crud->autocomplete("","category",$select,"category",$term,"",$order);  	
	 	echo json_encode($result);   	 		
	}
	
	function get_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'category','sort'=>'ASC'); $select = "category as name";
		$result = $this->crud->autocomplete("","category",$select,"category",$term,"",$order);
		echo json_encode($result);
	}
	
	function set_sess_search(){
		$like = $_POST['search']; 	
	 	$this->session->set_flashdata('like',$like); 
	}
	
	function is_exist(){
		$category	  = $_POST['category'];
		$is_exist['1']= !$category ? "false" : "true";
		$msg['1']	  = !$category ? "category name required" : "";
	
		if ($is_exist['1']=='true'){
			$where   = array('category'=>$category,'status'=>'1');
			$checked = $this->crud->is_exist("","category","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate category name" : "";
	
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status, 'msg'=>$msg['1']);
	
		echo json_encode($result);
	}
	
	function create(){
		$init_code  = $this->m_category->category_code("category");
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array('id'=>null,
							'init_code'=>$init_code,
							'category'=>$_POST['category'],
							'created_id'=>$userID,
							'created_date'=>$curr_date,
							'last_updated'=>$curr_date);
		$id = $this->crud->insert("","category",$data);
		$this->session->set_flashdata('message','1 data success insert');
	}
	
	function form_update(){
		$id 				   = $this->uri->segment(3);
		$select 			   = "id,init_code,(SELECT id FROM category r WHERE r.id=l.id_cat) as id_cat, subcategory,created_date,last_updated";
		$def				   = $this->crud->browse("","subcategory l","id",$id,"false",$select);
		$data['def']  		   = $def ;
		$category 			   = $this->crud->browse("","category","","","true","id,category");
		
		$id   = $this->uri->segment(3);
		$select = "id,init_code,category,created_id,last_updated";
		$data ['def'] = $this->crud->browse ( "", "category", "id", $id, "false", $select);
		$this->load->view ( 'supplier/category/form_update', $data );
	}	
	
	function form_create(){
		$data ['user_id'] = $this->tank_auth->get_user_id ();
		$data ['username'] = $this->tank_auth->get_username ();		
		$this->load->view('supplier/category/form_create', $data);
	}
	
	function update(){
		$curr_date =  date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data = array('category'	=>$_POST['category'],'last_updated'=>$curr_date);
		$this->crud->update("","category","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
	}
	
	function delete(){
	
		$id = $this->uri->segment(3);
	
		$where   = array('id_cat'=>$id);
		$checked1 = $this->crud->is_exist("","subcategory","id",$where);
		
		//$where1  = array('id'=>$id);
		$checked2 = $this->crud->is_exist("","announce","id",$where);
		$where1	  = array('id_category'=>$id);
		//$checked3 = $this->crud->is_exist("","vendor","id",$where1);
	
		//if ($checked1 || $checked2 || $checked3){
		if ($checked1 || $checked2){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		}
		else {
			$this->crud->delete("","category","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
		
		redirect('category/','refresh');
	}
	
	function set_sess_category(){
	
		$type = $_POST['category'];
		if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
		} else { $this->session->unset_userdata('sess_cat'); }
		
		$type = $_POST['field'];
		if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
		} else { $this->session->unset_userdata('sess_field'); }
	
	}
	
	function subcategory(){
		if ($this->tank_auth->is_logged_in()) {
	
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			//$where      = array('status'=>'1');
			
			//$sess_like  = $this->session->flashdata('like');
			
			$sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
			
				
			$field      = $sess_field != '' ? $sess_field : "init_code";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
				
			if($sess_cat != ''){	$where = array('init_code'=>$sess_cat);
			} else { $where = "";	}
	
			$table 	    = "subcategory";
			$page       = $this->uri->segment(3);
			$per_page   = 5;
			$offset     = $this->crud->set_offset($page,$per_page);
			$total_rows = $this->crud->get_total_record("",$table,$where);
			$set_config = array('base_url'=> base_url().'/category/subcategory','total_rows'=>$total_rows,'per_page'=>5,'uri_segment'=>3);
			$config     = $this->crud->set_config($set_config);
				
			$this->load->library('pagination');
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();
	
			$order 			     = array('field'=>'created_date','order'=>'DESC');
			$data['pagination']  = $paging;
			$data['num']         = $offset;
			$select 			 = "id,init_code,(SELECT category FROM category r WHERE r.id=l.id_cat) as id_cat, subcategory,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
	
			$sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
			$data['browse'] = $browse;
			
			$subcategory = $this->crud->browse("","subcategory","","","true","id,init_code");
			if(!$subcategory) $subcategory = array();
			$select = array(''=>'All');
			foreach($subcategory as $val){ $options[$val->init_code] = $val->init_code; }
			$data['init_code'] = $select + $options;
			
			$subcategory_ = $this->crud->browse("","subcategory","","","true","id,subcategory");
			if(!$subcategory_) $subcategory_ = array();
			$select_ = array(''=>'All');
			foreach($subcategory_ as $val_){ $options_[$val_->id] = $val_->subcategory; }
			$data['subcategory'] = $select_ + $options_;

			//$data['browse']      = $browse;	
		 	$data['view']	= "supplier/subcategory/browse"; 		
 			$this->load->view('layout/template',$data);
	
		} else {  
			$this->session->set_flashdata('message','user not authorized');
			redirect ( '/auth/login/' ); 
		}
	}
	
	function search2(){
	
		/* initiate search inputs */
	
		//$search_dateranges = array('publish_date',$_POST['start_date'],$_POST['end_date']);
		$search_conditions = array(
				'category'			=> $_POST['search_term'],
				'subcategory'		=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch2(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		// 		$data['category'] 		 = $this->getCategories();
		// 		$data['subcategory'] 	 = $this->getSubcategories();
		// 		$data['subcontent_type'] = $this->getSubcontentTypes();
		// 		$data['status_ref'] 	 = $this->getStatusCondition();
	
	
		$data['view'] = "supplier/subcategory/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables2($limit = NULL)
	{
		/* get data for table view from database with pagination */
	
		$table 	     = "subcategory t1";
		$select 	 = "t1.id,init_code,(SELECT category FROM category r WHERE r.id=t1.id_cat) as id_cat, subcategory,(SELECT username FROM users WHERE id=t1.created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		// if($sessID)	$getData = $this->crud->browse("",$table,"id",$sessID,"true",$select,$where);
		// else  {
		$joins[0][0] = 'category t2';
		$joins[0][1] = 't2.id = t1.id_cat';
		$joins[0][2] = 'left';
	
		//SELECT t1.id_country, t2.country_name FROM region t1 left join country t2 on t1.id_country = t2.id
	
		// $where		 = !$this->session->userdata('is_admin') ? "t1.subcontent_type IN ('4','5') AND t1.status = '3'":"";
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'/category/subcategory','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch2($limit = NULL,$dateranges = array(),$wherearray = array())
	{
		// 		$subcontent_allowed = globalNeedAanwidzing();
	
		// 		$sess_cat = $this->session->userdata('sess_cat');
		// 		$sessID   = $this->session->flashdata('anID');
			
		// 		$field    = $sess_cat ? "id_cat" : "";
		// 		$id_cat   = $sess_cat ? $sess_cat  : "";
	
		// 		if($sess_cat){
		// 			$where = "t1.subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'";
		// 		} else {
		// 			$where = !$this->session->userdata('is_admin') ? "subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'":"";
		// 		}
		$joins[0][0] = 'category t2';
		$joins[0][1] = 't2.id = t1.id_cat';
		$joins[0][2] = 'left';
		/* get data for table view from database with pagination */
	
		$table 	     = "subcategory t1";
		$select 	 = "t1.id,t1.init_code,(SELECT category FROM category r WHERE r.id=t1.id_cat) as id_cat, subcategory,(SELECT username FROM users WHERE id=t1.created_id)as created_id,t1.created_date,t1.last_updated";
		//$select      = "t1.id,(SELECT country_name FROM country WHERE id=id_country) as t1.country_name,region_name,t1.remark,(SELECT username FROM users WHERE id=created_id)as t1.created_id,t1.created_date,t1.last_updated";
		//$order 		 = array('field'=>'t1.publish_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse_join('',$table,"COUNT(t1.id) AS COUNT",$joins,$where,$dateranges,$wherearray);
	
		// 		$joins[0][0] = 'announcement_level_approval t2';
		// 		$joins[0][1] = 't2.id_announcement = t1.id';
		// 		$joins[0][2] = 'left';
		// 		$joins[1][0] = 'level t3';
		// 		$joins[1][1] = 't2.id_level = t3.id';
		// 		$joins[1][2] = 'left';
	
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,$where,$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'category/search2/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
		return $data;
	
	}
	
	function set_sess_subcategory(){
	
		$type = $_POST['subcategory'];
		if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
		} else { $this->session->unset_userdata('sess_cat'); }
	
		$type = $_POST['field'];
		if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
		} else { $this->session->unset_userdata('sess_field'); }
	
	}
	
	function get_subcat_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'subcategory','sort'=>'ASC'); $select = "subcategory as name";
		$result = $this->crud->autocomplete("","subcategory",$select,"subcategory",$term,"",$order);
		echo json_encode($result);
	}
	
	function set_sess_search_subcat(){
		$like = $_POST['search'];
		$this->session->set_flashdata('like',$like);
	}
	
	function is_exist_subcat(){
		$category	  = $_POST['id_cat'];
		$subcategory  = $_POST['subcategory'];
		$is_exist['1']= !$category ? "false" : "true";
		$is_exist['2']= !$subcategory ? "false" : "true";
		$msg['1']	  = !$category ? "category name required" : "";
		$msg['2']	  = !$subcategory ? "subcategory name required" : "";
	
		if ($is_exist['1']=='true' && $is_exist['2']=='true'){
			$where   = array('id_cat'=>$category,'subcategory'=>$subcategory,'status'=>'1');
			$checked = $this->crud->is_exist("","subcategory","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$is_exist['2']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate category name" : "";
			$msg['2']	= $checked ? "duplicate subcategory name" : "";
	
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2']);
	
		echo json_encode($result);
	}
	
	function form_create_subcat(){
		$category = $this->crud->browse("","category","","","true","id,category");
		if(!$category) $category = array();
		$select = array(''=>'-- Select --');
		foreach($category as $val){ $options[$val->id] = $val->category; }
		$data['category'] = $select + $options;
	
		$this->load->view('supplier/subcategory/form_create',$data);
	}
	
	function create_subcat(){
		$init_code  = $this->m_category->category_code("subcategory");
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array('id'=>null,
							'init_code'   =>$init_code,
							'id_cat'	  =>$_POST['id_cat'],
							'subcategory' =>$_POST['subcategory'],
							'created_id'  =>$userID,
							'created_date'=>$curr_date,
							'last_updated'=>$curr_date);
		$id = $this->crud->insert("","subcategory",$data);
		$this->session->set_flashdata('message','1 data success insert');
	}
	
	function update_subcat(){
		$curr_date 	= date('Y-m-d H:i:s');
		$id   = $this->uri->segment(3);
		$data = array('id_cat'  =>$_POST['id_cat'],
					  'subcategory' =>$_POST['subcategory'],
					  'last_updated'=> $curr_date);
		$this->crud->update("","subcategory","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
	}
	
	function form_update_subcat(){
		$id 				   = $this->uri->segment(3);
		$select 			   = "id,init_code,(SELECT id FROM category r WHERE r.id=l.id_cat) as id_cat, subcategory,created_date,last_updated";
		$def				   = $this->crud->browse("","subcategory l","id",$id,"false",$select);
		$data['def']  		   = $def ;
		$category 			   = $this->crud->browse("","category","","","true","id,category");
		if(!$category) $category = array();
		$select				   = array(''=>'-- Select --');
		foreach($category as $val){ $options[$val->id] = $val->category; }
		$data['category'] 	  = $select + $options;
		$this->load->view('supplier/subcategory/form_update',$data);
	}
	
	function delete_subcat_(){
	
		$id = $this->uri->segment(3);
		$this->crud->delete("","subcategory","id",$id);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('category/subcategory/','refresh');
	}

	function delete_subcat(){
	
		$id = $this->uri->segment(3);
		
		$where1   = array('id_subcat'=>$id);
		$checked1 = $this->crud->is_exist("","announce","id",$where1);
		
		//$where2   = array('id_subcat'=>$id);
		//$checked2 = $this->crud->is_exist("","vendor","id",$id,$where2);
		
		if ($checked1){ //|| $checked2){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		}
		else {
			$this->crud->delete("","subcategory","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
		
		//$data = array('status'=>'0');
		//$this->crud->update("","subcategory","id",$id,$data);
		redirect('category/subcategory/','refresh');
	}
	
	function supplier_category() {
		if ($this->tank_auth->is_logged_in ()) {
			
			$data ['user_id'] = $this->tank_auth->get_user_id ();
			$data ['username'] = $this->tank_auth->get_username ();
			
			
			$sess_like  = $this->session->flashdata('like'); $catID = "";
			if($sess_like){
				$like  = array('col'=>'category','field'=>$sess_like);
				$cat   = $this->crud->browse("","category","","","false","id","","","",$like);
				$catID = $cat[0]->id; 
			}
			
			$field = ! $catID ? "" : "id_cat";
			$ID    = ! $catID ? "" : $catID;
				
			if ($catID)	$where = array ($field => $ID);
			else $where = "";
					
				$table 		= "supplier_category";
				$page 		= $this->uri->segment ( 3 );
				$per_page 	= 3;
				$offset 	= $this->crud->set_offset ( $page, $per_page );
				$total_rows = $this->crud->get_total_record ( "", $table, $where );
				$set_config = array ('base_url'	=> base_url () . 'category/supplier_category','total_rows'=> $total_rows,'per_page'	=> 3,'uri_segment'=>3);				
				$config     = $this->crud->set_config ( $set_config );	
				
								
				$this->load->library ( 'pagination' );
				$this->pagination->initialize ( $config );
				$paging = $this->pagination->create_links ();					
				$order  = array(	'field' => 'created_date','order' => 'DESC');
				$data ['pagination'] = $paging;
				$data ['num'] = $offset;
				$select = "id,(SELECT category FROM category WHERE id=id_cat) as category, 
						  (SELECT subcategory FROM subcategory WHERE id=id_subcat) as subcategory,
						   status,created_id,created_date,last_updated ";
						   
				if ($catID) {
					$browse = $this->crud->browse ("", $table, $field, $ID, "true", $select );	} else {
					$browse = $this->crud->browse_with_paging ( "", $table, $field, $ID, "true", $select, "", $order, $config ['per_page'], $offset ); }
					
				$data['browse'] = $browse;				
				$data['view']	= "supplier/supp_category/browse"; 		
 				$this->load->view('layout/template',$data);
		} else {
			$this->session->set_flashdata('message','user not authorized'); 
			redirect('/auth/login/','refresh');	 	 }
	}    
	
	function get_suppcat_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'id_cat','sort'=>'ASC'); $select = "id_cat as name";
		$result = $this->crud->autocomplete("","supplier_category",$select,"id_cat",$term,"",$order);

		echo json_encode($result);
	}
	
	function set_sess_search_suppcat(){
		$like = $_POST['search'];
		$this->session->set_flashdata('like',$like);
	}
	
	function is_exist_suppcat(){
		$category	  = $_POST['id_cat'];
		$subcategory  = $_POST['id_subcat'];
		$is_exist['1']= (!$category || $category==0) ? "false" : "true";
		$is_exist['2']= (!$subcategory || $subcategory==0) ? "false" : "true";
		$msg['1']	  = (!$category || $category==0) ? "category name required" : "";
		$msg['2']	  = (!$subcategory || $subcategory==0) ? "subcategory name required" : "";
	
		if ($is_exist['1']=='true' && $is_exist['2']=='true'){
			$where   = array('id_cat'=>$category, 'id_subcat'=>$subcategory);
			$checked = $this->crud->is_exist("","supplier_category","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$is_exist['2']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate category name" : "";
			$msg['2']	= $checked ? "duplicate subcategory name" : "";
	
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2']);
	
		echo json_encode($result);
	}
	
	function is_exist_suppcat2(){
		$category	  = $_POST['id_cat'];
		$subcategory  = $_POST['id_subcat'];
		$is_exist['1']= !$category ? "false" : "true";
		$is_exist['2']= (!$subcategory || $subcategory==0) ? "false" : "true";
		$msg['1']	  = !$category ? "category name required" : "";
		$msg['2']	  = (!$subcategory || $subcategory==0) ? "subcategory name required" : "";
	
		if ($is_exist['1']=='true' && $is_exist['2']=='true'){
			$where   = array('id_cat'=>$category, 'id_subcat'=>$subcategory);
			$checked = $this->crud->is_exist("","supplier_category","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$is_exist['2']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate category name" : "";
			$msg['2']	= $checked ? "duplicate subcategory name" : "";
	
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2']);
	
		echo json_encode($result);
	}
	
	
	function form_create_suppcat(){
	
		$category = $this->crud->browse("","category","","","true","id,category");
		if(!$category) $category = array();
		$select = array(''=>'-- Select --');
		foreach($category as $val){ $options[$val->id] = $val->category; }
		$data['category'] = $select + $options;
		
		$subcategory = $this->crud->browse("","subcategory","","","true","id,subcategory");
		if(!$subcategory) $subcategory = array();
		$select = array(''=>'-- Select --');
		foreach($subcategory as $val){ $options[$val->id] = $val->subcategory; }
		$data['subcategory'] = $select + $options;
	
		$this->load->view('supplier/supp_category/form_create',$data);
	
	}
	
	function get_subcategory(){
		$id_cat = $_POST['id_cat']; //echo "=>".$id_comp;
		$select  = " id,subcategory";
		$subcategory  = $this->crud->browse("","subcategory","id_cat",$id_cat,"true",$select);
		echo "<option>-- Select Subcategory --</option>";
		foreach ($subcategory as $row){
			echo "<option value='$row->id'>$row->subcategory</option>";
		}
	}

	function create_suppcat() {
		$curr_date = date ( 'Y-m-d H:i:s' );
		$userID = $this->tank_auth->get_user_id ();
		$data = array (
				'id' => null,
				'id_cat' => $_POST ['id_cat'],
				'id_subcat' => $_POST ['id_subcat'],
				'status' => '1',
				'created_id' => $userID,
				'created_date' => $curr_date,
				'last_updated' => $curr_date
		);
		
		$id = $this->crud->insert ( "", "supplier_category", $data );
		$this->session->set_flashdata ( 'message', '1 data success insert' );
		redirect ( 'category/supplier_category/', 'refresh' );
	}

	function update_suppcat() {
		$curr_date = date( 'Y-m-d H:i:s' );
		$userID = $this->tank_auth->get_user_id();
		$data = array(
				'id_cat' => $_POST['id_cat'],
				'id_subcat' => $_POST['id_subcat'],
				'status' => '1',
				'created_id' => $userID,
				'created_date' => $curr_date,
				'last_updated' => $curr_date
		);
		$this->crud->update("","supplier_category","id",$_POST['id'],$data);
		$this->session->set_flashdata ( 'message', '1 data success update' );
	}

	function form_update_suppcat() {
		$id = $this->uri->segment ( 3 );
		$select = "id, (SELECT id FROM subcategory r WHERE r.id=l.id_subcat) as id_subcat, 
				  (SELECT id_cat FROM subcategory r WHERE r.id=l.id_subcat) as id_cat, 
				  (SELECT category FROM category s where s.id=id_cat) as category,
				  status,created_id,created_date,last_updated ";
		$def = $this->crud->browse("","supplier_category l","id",$id,"false",$select);
		$data['def'] = $def ;
		
		$category = $this->crud->browse("","category","","","true","id,category");
		if(!$category) $category = array();
		$select_cat = array(''=>'-- Select --');
		foreach($category as $val){ $option_cat[$val->id] = $val->category; }
		$data['category'] = $select_cat + $option_cat;
		
		$subcategory = $this->crud->browse("","subcategory","","","true","id,subcategory");
		if(!$subcategory) $subcategory = array();
		$select_subcat = array(''=>'--Select--');
		foreach($subcategory as $val){ $option_subcat[$val->id] = $val->subcategory; }
		$data['subcategory'] = $select_subcat + $option_subcat;
		
		$this->load->view('supplier/supp_category/form_update',$data);
	}

	function delete_suppcat() {
		$id = $this->uri->segment ( 3 );
		$this->crud->delete ( "", "supplier_category", "id", $id );
		$this->session->set_flashdata ( 'message', '1 data success deleted' );
		redirect ( 'category/supplier_category/', 'refresh' );
	}
}
?>