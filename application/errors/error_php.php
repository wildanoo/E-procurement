<div style="border:1px solid red;padding-left:20px;margin:0 0 10px 0;" class="alert-danger">

<h4>A PHP Error was encountered</h4>

<?php if(ENVIRONMENT == 'development') { ?>
	<span><u><?php echo $severity; ?> :</u></span>
	<span><?php echo $message.' on '.$filepath.' line number '.$line; ?></span>
<?php } else echo 'This message is hidden because you are in a production environment.'; ?>

</div>