<?php echo form_open('#',array('id'=>'form')); ?>
<table cellpadding="2" cellspacing="2">
<?php echo form_hidden('id',$data->id); ?>
<tr>
	<td><label>Division</label></td><td>:</td>
	<td><?php 
		$prop_division = 'class="form-control" id="id_division" style="width:220px"';
		echo form_dropdown('id_division',$division,$def->id_division,$prop_division); ?>			
	</td>
	<td id="msg1"></td>	
</tr>
<tr>
	<td><label>Department Code</label></td><td>:</td>
	<td><input type="text" name="dept_code" id="dept_code" value="<?php  echo $data->dept_code; ?>" class="form-control" required/></td>
	<td id="msg2"></td>	
</tr>
<tr>
	<td><label>Department Name</label></td><td>:</td>
	<td><input type="text" name="dept_name" id="dept_name" value="<?php  echo $data->dept_name; ?>" class="form-control" required/></td>
	<td id="msg3"></td>	
</tr>
</table>