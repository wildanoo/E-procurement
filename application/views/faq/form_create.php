<div class="page-header">
  <h3>New Content FAQ</h3>
</div>
<?php echo form_open('faq/create',array('id'=>'form')); ?>
<table cellpadding="2" cellspacing="2">
<tr>
				<ul class="nav nav-tabs test2">
				    <li class="active"><a data-toggle="tab" href="#idn" aria-expanded="true">Indonesia</a></li>
				    <li class=""><a data-toggle="tab" href="#eng" aria-expanded="false">English</a></li>
				</ul>

				<div class="tab-content">
				    <div id="idn" class="tab-pane fade active in" style="padding:15px;">
				    <input type="text" name="question_ind" id="question_ind" placeholder="Question Indonesia" class="form-control" required/>
				      <?php $this->load->view('announce/note',$data); ?>
				    </div>
				    <div id="eng" class="tab-pane fade" style="padding:15px;">
				    <input type="text" name="question_eng" id="question_eng" placeholder="Question English" class="form-control" required/>
				      <?php $this->load->view('announce/note',$data); ?>
				    </div>
				</div>
			</tr>
<tr>	
	<td colspan="3" style="text-align: right;">
		<input type="submit" value="Submit" class="btn btn-primary btn-sm"/>
		<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>'Cancel'); ?>					 
		<?php  echo anchor('faq/','Cancel',$prop); ?>
	</td>
</tr>
</table>