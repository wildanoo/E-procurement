<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor extends CI_Controller
{
	var $permit;
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('m_register','m_vendor','codec','m_announce','m_user'));
		$this->permit = new stdClass(); $this->global = new stdClass();	
		$this->permit = $this->crud->get_permissions("1"); /* 1 adalah function untuk registrasi  */		
		
		$venID   = $this->session->userdata("venID"); $stsID = 0;	
		if($venID) $stsID = $this->crud->browse("","vendor","id",$venID,"false","id_stsreg")->id_stsreg;		
		$this->global->register = $stsID;
		$this->global->active   = ($this->permit->uvd && $stsID=="3" ) ? true : false;
		$this->permit->md3      = ($this->permit->md3 && $stsID=="4" ) ? true : false;		

	}
	
	function index(){
		redirect('/auth/login/');	
		
 	/*$groupID = "6";	$i=1; 
 	$order 	 = array('field'=>'name','sort'=>'ASC');
 	$group   = $this->crud->browse("","groups","id",$groupID,"false","permissions as permit","",$order);  //print_r($group); 
 	$permit  = explode(",",$group->permit);
 	foreach($permit as $val){	echo $i.".".$val."<br/>"; $i++; }*/
 	
 	/*if ($this->m_vendor->authenticate()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$session 	= $this->session->userdata('sess_vendor');
		$sess_id 	= $this->session->userdata('search');  
		$search  	= $this->m_vendor->search_browse($session,$sess_id); 	
		$where   	= $search->where;	$like = $search->like;	//print_r($like);	

 		$table 	    = !$sess_id ? "vendor" : "vendor_search";		
		$page       = $this->uri->segment(3);
		$per_page   = 10;		
		$offset     = $this->crud->set_offset($page,$per_page); 
		$total_rows = $this->crud->get_total_record("",$table,$where); //print_r($where);  
		$set_config = array('base_url'=>base_url().'vendor/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);		
			 
		$this->load->library('pagination');			 
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();			
			
		$order 			     = array('field'=>'created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;		
		$select          	 = "id,register_num as reg_num,vendor_num,vendor_name,vendor_type,id_stsreg,approval,													
								(SELECT recon_number FROM vendor_account u WHERE u.id_vendor=l.id) as recon_number,
								(SELECT sap_number FROM vendor_account v WHERE v.id_vendor=l.id) as sap_number,		
							    (SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as register,
							    (SELECT result FROM vendor_recomend t WHERE t.id_vendor=l.id LIMIT 1) as result,
							    (SELECT id FROM vendor_rejected s WHERE s.id_vendor=l.id AND status=1 LIMIT 1) as rejected,
							    (SELECT id FROM vendor_account  u WHERE u.id_vendor=l.id AND status=1) as completed,	
							    created_date,last_updated";			

		$browse = $this->crud->browse_with_paging("",$table." l",$field="",$ID="","true",$select,$where,$order,$config['per_page'],$offset,"",$like); 
		$data['title']    = $this->m_vendor->get_title();
		$data['category'] = $this->m_vendor->get_category();
		$data['register'] = $this->m_vendor->get_sts_register();
		$data['status']   = $this->m_vendor->register_status(); 
		$data['browse']   = $browse;		
		$data['view']	  = "vendor/browse"; 		
 		$this->load->view('layout/template',$data); 
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/'); } 	*/
	}
	
	function listing(){ 	
		$session = array('vendor_type'=>'all');
		$this->session->set_userdata('sess_vendor',$session);  
		$this->session->unset_userdata('page');
		redirect('vendor/','refresh') ; 	
	}

	function register(){ 	
		$session = array('vendor_type'=>'new');
		$this->session->set_userdata('page','register');
		$this->session->set_userdata('sess_vendor',$session);  
		redirect('vendor/','refresh') ;
	}
	
	function verify(){ 	
		$session = array('id_stsreg'=>'6');
		$this->session->set_userdata('page','verify');
		$this->session->set_userdata('sess_vendor',$session);
		redirect('vendor/','refresh') ; 	
	}
	
	function set_sess_search(){
		
		$index    = $_POST['srch-term']; 	$temp = array();
		$is_exist = $this->crud->is_exist("","vendor","id",array('vendor_num'=>$index));
		$field    = $is_exist && $index ? "vendor_num" : "vendor_name"; 	
		$session  = $this->m_vendor->set_session_search($field,$index,$_POST); 		
		$sess_vdr = $this->session->set_userdata('sess_vendor',$session); 	
		$sess_id  = $this->session->userdata('search');  
		
		if($sess_id){
			$this->crud->multiple_delete("","vendor_search",array('session'=>$sess_id)); 
			$this->session->unset_userdata('search'); } 
			
			if (array_key_exists("id_category",$session)){ 		
				$id_cat    = $_POST['id_cat'];  
				$id_subcat = $_POST['id_subcat'];  $where = "";
				$id_search = array_key_exists("id_subcat",$session) ? $id_subcat : $id_cat ;
				$field     = array_key_exists("id_subcat",$session) ? "id_subcat": "id_category";		
				$result    = $this->crud->browse("","vendor_category",$field,$id_search,"true","id_vendor as id");
				
				if($result){		
					foreach($result as $val){ $param[] = "`id`='$val->id'"; }		
					if(count($param)>1){ $where  = implode(" OR ",$param); unset($param);
				} else { $where = $param[0]; }
				
				$sess_id = uniqid();
				$this->session->set_userdata('search', $sess_id );
				$temp    = $this->crud->free_sql("","*","vendor",$where,true); 
				foreach($temp as $row){	$row->session = $sess_id;	
					$this->crud->insert("","vendor_search",$row); 	}			
				}	
				
			}	

		}
		
		function reset_filter(){
			
			$sess_id  = $this->session->userdata('search');
			if($sess_id){
				$this->crud->multiple_delete("","vendor_search",array('session'=>$sess_id)); 
				$this->session->unset_userdata('search'); 	} 
				
				$this->session->unset_userdata('sess_vendor');
				$page = $this->session->userdata('page');
				
				if($page=="register"){
					$session = array('vendor_type'=>'new');
					$this->session->set_userdata('sess_vendor',$session);
				} elseif($page=="verify"){
					$session = array('id_stsreg'=>'6');
					$this->session->set_userdata('sess_vendor',$session);	} 
					
					redirect('vendor/','refresh');
				}
				
				function get_tags(){ 	
					$term   = $_GET['term'];  	
					$order  = array('field'=>'vendor_name','sort'=>'ASC'); $select = "vendor_num as name";
					$result = $this->crud->autocomplete("","vendor",$select,"vendor_num",$term,"",$order);  	
					echo json_encode($result);   	 		
				} 
				
				function set_browse_session(){
					
					$code   = $_POST['search'];	
					$vendor = $this->crud->browse("","vendor","reg_num",$code,"false","id");
					if($vendor) $this->session->set_flashdata('id_vendor',$vendor->id); 
					
				}
				
				function set_browse_status(){ 	
					
					$id_sts = $_POST['id_sts'];
					if($id_sts == "0") $this->session->unset_userdata('sess_sts');
					else $this->session->set_userdata('sess_sts',$id_sts);
					
					$sess_sts = $this->session->userdata('sess_sts');
					
					echo $sess_sts;
				}

				function set_sess_vendor(){
					$id = $this->uri->segment(3);
					$this->session->set_userdata("venID",$id);
					redirect("vendor/detail");
					
				}
				
				function review_category(){
					$id = $this->uri->segment(3);
					$data['user_id']	= $this->tank_auth->get_user_id();
					$data['username']	= $this->tank_auth->get_username();
					$select				= "vendor_name, vendor_address,
					(SELECT category FROM category WHERE id=id_category) as category,
					(SELECT subcategory FROM subcategory WHERE id=id_subcat) as subcategory,
					contact_person,phone,fax,email,npwp";
 	//browse($db="",$table,$field="",$id="",$multi="",$select="",$where="",$order="",$group_by="",$like="")
					$data['data'] = $this->crud->browse("","vendor","id",$id,"false",$select);
					$this->load->view('vendor/detail/category',$data);
				}

				function detail(){
					
					if ($this->m_vendor->authenticate()) { 	
						$data['user_id']  = $this->tank_auth->get_user_id();
						$data['username'] = $this->tank_auth->get_username();
						
						$venID   = $this->session->userdata("venID");	
						$select	 = "id,vendor_name as name,register_num as reg_num,vendor_type as type,vendor_num as num,vendor_address as address,
						(SELECT id FROM vendor_rejected s WHERE s.id_vendor=l.id AND status=1 LIMIT 1) as rejected,	
						(SELECT id FROM vendor_account  t WHERE t.id_vendor=l.id AND status=1) as completed,
						(SELECT result FROM vendor_recomend u WHERE u.id_vendor=l.id LIMIT 1) as result,				
						(SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as reg_sts,approval,
						id_stsreg,web,phone,fax,email,postcode,npwp,npwp_address,npwp_postcode";					
						
						$none_bank 		= array('acc_number'	=> "none",
							'acc_name'		=> "none",
							'acc_address'	=> "none",
							'bank_name'		=> "none",
							'branch_name'	=> "none");
						
						$none_akta 		= array('notaris_name'		=> "none",
							'notaris_address'	=> "none",
							'notaris_phone'		=> "none",
							'notaris_number'	=> "none",
							'notaris_date'		=> date('Y-m-d'),
							'remark'			=> "none");							 	
						
						$owner          = $this->crud->browse("","owner","id_vendor",$venID,"true");					 	
						$bank           = $this->crud->browse("","bank_account","id_vendor",$venID,"false");
						$akta           = $this->crud->browse("","akta","id_vendor",$venID,"false");
						$data['bank']   = !$bank ? (object) $none_bank : $bank;		
						$data['akta']   = !$akta ? (object) $none_akta : $akta;
						$data['owner']  = $owner; 		
						
						$data['correspondence'] = $this->crud->browse("","correspondence","id_vendor",$venID,"true");	
						$data['org']   = $this->crud->browse("","organization","id_vendor",$venID,"true");
						$data['ref']   = $this->crud->browse("","references","id_vendor",$venID,"true");
						$data['aff']   = $this->crud->browse("","affiliates","id_vendor",$venID,"true");
						$data['due_dill']   = $this->crud->browse("","due_dilligence","id_vendor",$venID,"true");
						
						$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
						(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";					
						$data['categories'] = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2); 		
						$data['vendor'] = $this->crud->browse("","vendor l","id",$venID,"false",$select);		
						$data['browse'] = $this->crud->browse("","contact_person","id_vendor",$venID,"true");		
						
						$reg_status = $this->crud->browse("","register_status","","","true","id,status");
						foreach($reg_status as $val){	$status[$val->id] = ucwords($val->status);}
						$data['status'] = $status;

						$default_visit	= array('status'=>"0",'note'=>"none",'files'=> "none");								
						$db_visit = $this->crud->browse("","vendor_visit_status","id_vendor",$venID,"false","status,note,files");
						$visit    = !$db_visit ? (object) $default_visit  : $db_visit;	 	
						$data['visit'] = $visit;
						
						$data['vendor_visit'] = $this->crud->browse("","vendor_visit","id_vendor",$venID,"true");
						
	 	/*$select2 = "id,(SELECT remark FROM approval_level WHERE level=approval) as note_from,notes,status,created_date";
	 	$data['notes'] = $this->crud->browse("","vendor_rejected","id_vendor",$venID,"true",$select2);	*/ 	
	 	
	 	$data['view']	= "vendor/tab"; 		
	 	$this->load->view('layout/template',$data); 
	 	
	 } else { 
	 	$this->session->set_flashdata('message','user not authorized'); 
	 	redirect('/auth/login/'); 
	 }	
	 
	}
	
	function load_main_tabs(){
		
		if ($this->m_vendor->authenticate()) { 	
			$data['user_id']  = $this->tank_auth->get_user_id();
			$data['username'] = $this->tank_auth->get_username();
			
			$venID   = $this->session->userdata("venID");		
			$select	 = "id,vendor_name as name,register_num as reg_num,vendor_type as type,vendor_num as num,vendor_address as address,
			(SELECT id FROM vendor_rejected s WHERE s.id_vendor=l.id AND status=1 LIMIT 1) as rejected,	
			(SELECT id FROM vendor_account  t WHERE t.id_vendor=l.id AND status=1) as completed,
			(SELECT result FROM vendor_recomend u WHERE u.id_vendor=l.id LIMIT 1) as result,				
			(SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as reg_sts,approval,
			id_stsreg,web,phone,fax,email,postcode,npwp,npwp_address,npwp_postcode";					
			
			$none_bank 		= array('acc_number'	=> "none",
				'acc_name'		=> "none",
				'acc_address'	=> "none",
				'bank_name'		=> "none",
				'branch_name'	=> "none");
			
			$none_akta 		= array('notaris_name'		=> "none",
				'notaris_address'	=> "none",
				'notaris_phone'		=> "none",
				'notaris_number'	=> "none",
				'notaris_date'		=> date('Y-m-d'),
				'remark'			=> "none");							 	
			
			$owner          = $this->crud->browse("","owner","id_vendor",$venID,"true");					 	
			$bank           = $this->crud->browse("","bank_account","id_vendor",$venID,"false");
			$akta           = $this->crud->browse("","akta","id_vendor",$venID,"false");
			$data['bank']   = !$bank ? (object) $none_bank : $bank;		
			$data['akta']   = !$akta ? (object) $none_akta : $akta;
			$data['owner']  = $owner; 		
			
			$data['correspondence'] = $this->crud->browse("","correspondence","id_vendor",$venID,"true");	
			$data['org']   = $this->crud->browse("","organization","id_vendor",$venID,"true");
			$data['ref']   = $this->crud->browse("","references","id_vendor",$venID,"true");
			$data['aff']   = $this->crud->browse("","affiliates","id_vendor",$venID,"true");
			$data['due_dill']   = $this->crud->browse("","due_dilligence","id_vendor",$venID,"true");
			
			$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
			(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";					
			$data['categories'] = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2); 		
			$data['vendor'] = $this->crud->browse("","vendor l","id",$venID,"false",$select);		
			$data['browse'] = $this->crud->browse("","contact_person","id_vendor",$venID,"true");		
			
			$reg_status = $this->crud->browse("","register_status","","","true","id,status");
			foreach($reg_status as $val){	$status[$val->id] = ucwords($val->status);}
			$data['status'] = $status;

			$default_visit	= array('status'=>"0",'note'=>"none",'files'=> "none");								
			$db_visit = $this->crud->browse("","vendor_visit_status","id_vendor",$venID,"false","status,note,files");
			$visit    = !$db_visit ? (object) $default_visit  : $db_visit;	 	
			$data['visit'] = $visit;
			
			$data['vendor_visit'] = $this->crud->browse("","vendor_visit","id_vendor",$venID,"true");
			
	 	/*$select2 = "id,(SELECT remark FROM approval_level WHERE level=approval) as note_from,notes,status,created_date";
	 	$data['notes'] = $this->crud->browse("","vendor_rejected","id_vendor",$venID,"true",$select2);	*/ 
	 	
	 	$select2 = "(SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as reg_sts,
	 	id,created_id,notes,status,created_date";
	 	$data['notes'] = $this->crud->browse("","vendor_register_tracking l","id_vendor",$venID,"true",$select2);	
	 	
	 	$this->load->view('vendor/tab',$data); 
	 	
	 } else { 
	 	$this->session->set_flashdata('message','user not authorized'); 
	 	redirect('/auth/login/'); 
	 }	
	 
	}

	function approved_process(){ 	
		
		$venID      = $this->session->userdata("venID"); 	
		$process    = $_POST['next_id'];
		$update     = array('id_stsreg'=>$process,'last_updated'=>date('Y-m-d H:i:s'));
		$this->crud->update("","vendor","id",$venID,$update); 
		
		$reg_status = $this->crud->browse("","register_status","","","true","id,status");
		foreach($reg_status as $val){ $status[$val->id] = ucwords(strtolower($val->status));}
		
		$value  = $process;	$notes  = "Change status with ".$status[$value];
		$this->m_vendor->register_tracking($venID,$notes);

		$send_email = array(2,3);
		if(in_array($process,$send_email)){		
			$select  = "register_num as num,vendor_name as name,email";
			$vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);
			$to      = $vendor->email; $link = "";
			$subject = "Notification E-procurement Garuda";
			
			if($process==3){
				$session  = $this->crud->browse("","vendor_register","id_vendor",$venID,"false","session")->session;	
				$update2  = array('status'=>'0','last_updated'=>date('Y-m-d H:i:s'));
				$this->crud->update("","vendor_register","id_vendor",$venID,$update2); 
				$link     =  base_url()."register/status/".$session;	
			}
			
			$lang   		= $this->session->userdata('lang'); echo $lang;
			$index  		= !$lang ? "ind" : $lang; 	$select = "id,desc_".$index." as point";
			$require_doc    = $this->crud->browse("","notification","","","true",$select);			
			$FORM 			= $process==2 ? "EPCGA002" : "EPCGA004";	
			$param			= array('reg_num'=>$vendor->num,'vendor_name'=>$vendor->name,'link'=>$link,'require_doc'=>$require_doc);
			$param['view']  = NOTIF_PATH.$FORM; 		
			$message 		= $this->load->view(NOTIF_TMPL,$param,true); 		
			$this->crud->sendMail($to,$subject,$message);				
		}	
		
	}
	
	function rejected_vendor(){
		
		$venID   = $this->session->userdata("venID"); 	
		$update  = array('vendor_type'=>'reject','last_updated'=>date('Y-m-d H:i:s'));
		$this->crud->update("","vendor","id",$venID,$update); 
		
		$notes = "Rejected Vendor Registration";
		$this->m_vendor->register_tracking($venID,$notes);	
		
		$select  = "register_num as num,vendor_name as name,email";
		$vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);		 	
		$to      =  $vendor->email;
		$subject =  "Notification E-procurement Garuda";

		$param = array('reg_num'=>$vendor->num,'vendor_name'=>$vendor->name);
		$param['view']  = NOTIF_PATH."EPCGA003"; 		
		$message 		= $this->load->view(NOTIF_TMPL,$param,true); 			
		$this->crud->sendMail($to,$subject,$message);		
		
	}
	
	function load_status_info(){
		
		$venID   = $this->session->userdata("venID");	
		$select	 = "vendor_name as name,register_num as reg_num,vendor_type as type,vendor_num as num,vendor_address as address,				
		(SELECT status FROM register_status WHERE id=id_stsreg) as reg_sts,
		id_stsreg,web,phone,fax,email,postcode,npwp,npwp_address,npwp_postcode";	
		
		$data['vendor'] = $this->crud->browse("","vendor l","id",$venID,"false",$select); 	
		$reg_status = $this->crud->browse("","register_status","","","true","id,status");
		foreach($reg_status as $val){	$status[$val->id] = ucwords($val->status);}
		$data['status'] = $status;
		
		$this->load->view('vendor/register_status',$data);
	}

	function general_update(){
		
		$venID = $this->session->userdata("venID");	
		$post = array(	'vendor_name'		=> $_POST['vendor_name'],
			'vendor_address'	=> $_POST['vendor_address'],	
			'postcode'			=> $_POST['vendor_postcode'],
			'npwp'				=> $_POST['npwp'],
			'npwp_address'		=> $_POST['npwp_address'],
			'npwp_postcode'		=> $_POST['npwp_postcode'],
			'phone'				=> $_POST['phone'],
			'fax'				=> $_POST['fax'],
			'email'				=> $_POST['email'],								
			'web'			    => $_POST['web'],
			'last_updated'	    => date('Y-m-d H:i:s'));
		
		$this->crud->update("","vendor","id",$venID,$post); 					
	} 
	
	function load_general(){
		
		$venID   = $this->session->userdata("venID");	
		$select	 = "id,vendor_name as name,register_num as reg_num,vendor_type as type,vendor_num as num,vendor_address as address,
		(SELECT id FROM vendor_rejected s WHERE s.id_vendor=l.id AND status=1 LIMIT 1) as rejected,	
		(SELECT id FROM vendor_account  t WHERE t.id_vendor=l.id AND status=1) as completed,
		(SELECT result FROM vendor_recomend u WHERE u.id_vendor=l.id LIMIT 1) as result,				
		(SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as reg_sts,approval,
		id_stsreg,web,phone,fax,email,postcode,npwp,npwp_address,npwp_postcode";					
		
		$none_bank 		= array('acc_number'	=> "none",
			'acc_name'		=> "none",
			'acc_address'	=> "none",
			'bank_name'		=> "none",
			'branch_name'	=> "none");
		
		$none_akta 		= array('notaris_name'		=> "none",
			'notaris_address'	=> "none",
			'notaris_phone'		=> "none",
			'notaris_number'	=> "none",
			'notaris_date'		=> date('Y-m-d'),
			'remark'			=> "none");							 	
		
		$owner          = $this->crud->browse("","owner","id_vendor",$venID,"true");					 	
		$bank           = $this->crud->browse("","bank_account","id_vendor",$venID,"false");
		$akta           = $this->crud->browse("","akta","id_vendor",$venID,"false");
		$data['bank']   = !$bank ? (object) $none_bank : $bank;		
		$data['akta']   = !$akta ? (object) $none_akta : $akta;
		$data['owner']  = $owner; 		
		
		$selectcor				= "*,(SELECT z.notif FROM reminder_doc z WHERE a.id=z.id_doc)as remindcount";
		$data['correspondence'] = $this->crud->browse("","correspondence a","id_vendor",$venID,"true",$selectcor);
		$whererem				= "id_vendor = $venID AND (CURDATE() BETWEEN a.exp_date - INTERVAL 90 DAY AND a.exp_date OR CURDATE() > a.exp_date)";
		$data['remindbut']		= $this->crud->browse("","correspondence a","","","true",$selectcor,$whererem);
		$data['remindbtn']		= $this->m_register->remindbtn($data['remindbut']);
		
		$data['org']   = $this->crud->browse("","organization","id_vendor",$venID,"true");
		$data['ref']   = $this->crud->browse("","references","id_vendor",$venID,"true");
		$data['aff']   = $this->crud->browse("","affiliates","id_vendor",$venID,"true");
		$data['due_dill']   = $this->crud->browse("","due_dilligence","id_vendor",$venID,"true");
		
		$select2 = "id,id_category as id_cat,(SELECT category FROM category r WHERE r.id=l.id_category) as category,id_subcat,
		(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";					
		$data['categories'] = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2); 		
		$data['vendor'] = $this->crud->browse("","vendor l","id",$venID,"false",$select);		
		$data['browse'] = $this->crud->browse("","contact_person","id_vendor",$venID,"true");		
		
		$reg_status = $this->crud->browse("","register_status","","","true","id,status");
		foreach($reg_status as $val){	$status[$val->id] = ucwords($val->status);}
		$data['status'] = $status;

		$default_visit	   = array('note'=>"none");								
	 	$nonvisit          = $this->crud->browse("","non_visit_notes","id_vendor",$venID,"false","note"); //print_r($nonvisit);
	 	$data['nonvisit']  = !$nonvisit ? (object) $default_visit  : $nonvisit;	 		 	
	 	$data['vendor_visit'] = $this->crud->browse("","vendor_visit","id_vendor",$venID,"true");
	 	$data['files']     = $this->crud->browse("","non_visit_files","id_vendor",$venID,"true","id,filename as file");		
	 	
	 	
	 	$db_visit          = $this->crud->browse("","vendor_visit_status","id_vendor",$venID,"false","status"); 
	 	$visit             = !$db_visit ?  "0"  : $db_visit->status;	
	 	$data['visit']     = $visit;	 	
	 	

	 	$select2 = "(SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as reg_sts,
	 	id,created_id,notes,status,created_date";
	 	$data['notes'] = $this->crud->browse("","vendor_register_tracking l","id_vendor",$venID,"true",$select2);
	 	$data['recmd'] = $this->crud->browse("","vendor_recomend","id_vendor",$venID,"false","result as type,note");
	 	
	 	$user_group  = $this->session->userdata('user_group'); 
	 	$user_level  = $this->m_vendor->get_user_level($user_group); $data['user_level'] = $user_level;			
	 	$acc_default	= array('recon_number'	=> null,
	 		'sap_number'	=> null,
	 		'srm_username'	=> null,
	 		'srm_password'	=> "",'status'=>'0');	 	
	 	$account = $this->crud->browse("","vendor_account","id_vendor",$venID,"false");
	 	$data['account'] = !$account ? (object) $acc_default : $account; 	
	 	
	 	$this->load->view('vendor/detail/general',$data); 
	 }
	 
	 function load_contact(){ 	
	 	$venID   = $this->session->userdata("venID");	
	 	$data['browse'] = $this->crud->browse("","contact_person","id_vendor",$venID,"true");	
	 	$this->load->view('vendor/detail/contact',$data); 
	 }
	 
	 function load_bank_account(){ 	
	 	$venID   = $this->session->userdata("venID");	
	 	$data['bank'] = $this->crud->browse("","bank_account","id_vendor",$venID,"false");	
	 	$this->load->view('vendor/detail/bank_account',$data); 
	 }
	 
	 function load_akta(){ 	
	 	$venID   = $this->session->userdata("venID"); 		
	 	$none_akta 		= array('notaris_name'		=> "none",
	 		'notaris_address'	=> "none",
	 		'notaris_phone'		=> "none",
	 		'notaris_number'	=> "none",
	 		'notaris_date'		=> date('Y-m-d'),
	 		'remark'			=> "none");
	 	
	 	$akta  = $this->crud->browse("","akta","id_vendor",$venID,"false");	
	 	$data['akta']   = !$akta ? (object) $none_akta : $akta;	 	
	 	
	 	
	 	$this->load->view('vendor/detail/akta',$data); 
	 }
	 
	 function load_recomendation(){  		
	 	
	 	$venID   = $this->session->userdata("venID"); 
 		//$select2 = "id,(SELECT remark FROM approval_level WHERE level=approval) as note_from,notes,status,created_date";	 			
	 	$select  = "id,id_category as id_cat,(SELECT category FROM category r WHERE r.id=l.id_category) as category,id_subcat,
	 	(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";								
	 	$data['categories'] = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select); 
	 	$data['recmd'] = $this->crud->browse("","vendor_recomend","id_vendor",$venID,"false","result as type,note");
 		//$data['notes'] = $this->crud->browse("","vendor_rejected","id_vendor",$venID,"true",$select2);	
	 	$data['vendor'] = $this->crud->browse("","vendor","id",$venID,"false","approval");
	 	
	 	$user_group  = $this->session->userdata('user_group'); 
	 	$user_level  = $this->m_vendor->get_user_level($user_group); $data['user_level'] = $user_level;
	 	
	 	$this->load->view('due_diligence/form_recmd',$data); 
	 }
	 
	 function validate_recmd(){
	 	
	 	$ttl = $_POST['ttl']; $validate = array();  
	 	for ($i = 1; $i <= $ttl; $i++) {  	
	 		$val = $_POST['opt'.$i];  
	 		if($val) { $validate[] = 'true'; break; }			
	 	}
	 	
	 	$venID      = $this->session->userdata("venID");
	 	$where      = array('id_vendor'=>$venID);
	 	$is_exist   = $this->crud->is_exist("","vendor_visit_status","id",$where); 
	 	
	 	if(!$validate) $validate[] = 'false';
	 	$validate[] = $_POST['type']=="0" ? "false" : "true"; 	
	 	$result = in_array("false",$validate) || !$is_exist ? "false" : "true";
	 	
	 	echo $result; 	
	 }
	 
	 function form_create(){
	 	$venID  = $this->session->userdata("venID");
	 	$data['vendor'] = $this->crud->browse("","vendor","id",$venID,"false","vendor_name as name");		
	 	$this->load->view('vendor/detail/general/form_create',$data);
	 }
	 
	 function form_update(){
	 	$contactID  = $this->uri->segment(3);
	 	$venID      = $this->session->userdata("venID");
	 	
	 	$data['default'] = $this->crud->browse("","contact_person","id",$contactID,"false"); 	
	 	$data['vendor'] = $this->crud->browse("","vendor","id",$venID,"false","vendor_name as name");		
	 	$this->load->view('vendor/detail/general/form_update',$data);
	 }
	 
	 function add_contact(){	

	 	$curr_date 	= date('Y-m-d H:i:s'); 
	 	$userID     = $this->tank_auth->get_user_id();
	 	$venID      = $this->session->userdata("venID");
	 	$data 		= array('id'			=> null, 						
	 		'id_vendor'		=> $venID,
	 		'fullname'		=> $_POST['name'],
	 		'position'		=> $_POST['position'],
	 		'mobile'		=> $_POST['mobile'],
	 		'email'			=> $_POST['email'],
	 		'status'		=> '1',
	 		'created_id'	=> $userID,
	 		'created_date'	=> $curr_date,
	 		'last_updated'	=> $curr_date);
	 	
	 	$this->crud->insert("","contact_person",$data); 					 	
	 }
	 
	 function update_contact(){	

	 	$curr_date 	= date('Y-m-d H:i:s'); 
	 	$userID     = $this->tank_auth->get_user_id();
	 	$venID      = $this->session->userdata("venID");
	 	$data 		= array('fullname'		=> $_POST['name'],
	 		'position'		=> $_POST['position'],
	 		'mobile'		=> $_POST['mobile'],
	 		'email'			=> $_POST['email'],					 					 	
	 		'last_updated'	=> $curr_date);	
	 	
	 	$this->crud->update("","contact_person","id",$_POST['id'],$data); 				 	
	 	
	 }
	 
	 function delete_contact(){ 
	 	
	 	$this->crud->delete("","contact_person","id",$_POST['id']); 	
	 	
	 }

	 function bank_update(){
	 	$venID      = $this->session->userdata("venID");
	 	$where      = array('id_vendor'=>$venID);
	 	$is_exist   = $this->crud->is_exist("","bank_account","id",$where); 	
	 	$curr_date 	= date('Y-m-d H:i:s'); 
	 	$userID     = $this->tank_auth->get_user_id();
	 	$venID      = $this->session->userdata("venID");
	 	$data 		= array('id_vendor'		=> $venID,
	 		'acc_number'	=> $_POST['acc_number'],
	 		'acc_name'		=> $_POST['acc_name'],
	 		'acc_address'	=> $_POST['acc_address'],
	 		'bank_name'		=> $_POST['bank_name'],
	 		'branch_name'	=> $_POST['branch_name'],
	 		'status'		=> '0',					 	
	 		'created_id'	=> $this->tank_auth->get_user_id(),		
	 		'created_date'  => $curr_date,							 					 	
	 		'last_updated'	=> $curr_date);	
	 	
	 	if($is_exist){
	 		unset($data['id_vendor']); unset($data['created_id']); unset($data['created_date']);
	 		$this->crud->update("","bank_account","id_vendor",$venID,$data); 
	 	} else { $this->crud->insert("","bank_account",$data); }		 	
	 	
	 }

	 function akta_update(){
	 	
	 	$venID      = $this->session->userdata("venID");
	 	$where      = array('id_vendor'=>$venID);
	 	$is_exist   = $this->crud->is_exist("","akta","id",$where); 	
	 	$curr_date 	= date('Y-m-d H:i:s'); 
	 	$userID     = $this->tank_auth->get_user_id();
	 	$data 		= array('id_vendor'		    => $venID,
	 		'notaris_name'		=> $_POST['notaris_name'],
	 		'notaris_address'	=> $_POST['notaris_address'],
	 		'notaris_phone'		=> $_POST['notaris_phone'],
	 		'notaris_number'	=> $_POST['notaris_number'],
	 		'notaris_date'		=> $_POST['notaris_date'],
	 		'remark'			=> $_POST['remark'],					 	
	 		'created_id'		=> $this->tank_auth->get_user_id(),		
	 		'created_date'  	=> $curr_date,							 					 	
	 		'last_updated'		=> $curr_date);	
	 	
	 	if($is_exist){ 
	 		unset($data['id_vendor']); unset($data['created_id']); unset($data['created_date']);
	 		$this->crud->update("","akta","id_vendor",$venID,$data); 
	 	} else { $this->crud->insert("","akta",$data); }
	 	
	 }

	 function form_owner(){
	 	
	 	$this->load->view('vendor/detail/owner/form_create'); 
	 }
	 
	 function load_owner(){
	 	
	 	$venID   = $this->session->userdata("venID"); 
	 	$data['owner'] = $this->crud->browse("","owner","id_vendor",$venID,"true");		
	 	$this->load->view('vendor/detail/owner',$data); 	
	 	
	 }
	 
	 function add_owner(){
	 	
	 	$curr_date 	= date('Y-m-d H:i:s'); 
	 	$userID     = $this->tank_auth->get_user_id();
	 	$venID      = $this->session->userdata("venID");
	 	$data 		= array('id'			=> null, 						
	 		'id_vendor'		=> $venID,
	 		'owner_name'	=> $_POST['owner_name'],
	 		'owner_address'	=> $_POST['owner_address'],
	 		'owner_phone'	=> $_POST['owner_phone'],
	 		'owner_position'=> $_POST['owner_position'],
	 		'owner_shared'	=> $_POST['owner_shared'],
	 		'remark'		=> $_POST['remark'],
	 		'status'		=> '1',
	 		'created_id'	=> $userID,
	 		'created_date'	=> $curr_date,
	 		'last_updated'	=> $curr_date);					 	
	 	
	 	$this->crud->insert("","owner",$data); 		
	 	
	 }

	 function form_update_owner(){
	 	
	 	$prsnID  = $this->uri->segment(3);
	 	$venID   = $this->session->userdata("venID"); 	
	 	$data['default'] = $this->crud->browse("","owner","id",$prsnID,"false"); 	 	

	 	$this->load->view('vendor/detail/owner/form_update',$data);
	 	
	 }
	 
	 function update_owner(){
	 	
	 	$venID      = $this->session->userdata("venID"); 	
	 	$curr_date 	= date('Y-m-d H:i:s'); 	
	 	
	 	$data 		= array('owner_name'	=> $_POST['owner_name'],
	 		'owner_address'	=> $_POST['owner_address'],
	 		'owner_phone'	=> $_POST['owner_phone'],
	 		'owner_position'=> $_POST['owner_position'],
	 		'owner_shared'	=> $_POST['owner_shared'],
	 		'remark'		=> $_POST['remark'],						 							 					 	
	 		'last_updated'	=> $curr_date);
	 	
	 	$this->crud->update("","owner","id",$_POST['id'],$data); 
	 	
	 }

	 function delete_owner(){
	 	
	 	$this->crud->delete("","owner","id",$_POST['id']); 
	 }

	 function owner_update(){
	 	
	 	$venID      = $this->session->userdata("venID");
	 	$where      = array('id_vendor'=>$venID);
	 	$is_exist   = $this->crud->is_exist("","owner","id",$where); 	
	 	$curr_date 	= date('Y-m-d H:i:s'); 
	 	$userID     = $this->tank_auth->get_user_id();
	 	
	 	$data 		= array('id_vendor'		=> $venID,
	 		'owner_name'	=> $_POST['owner_name'],
	 		'owner_address'	=> $_POST['owner_address'],
	 		'owner_phone'	=> $_POST['owner_phone'],
	 		'owner_position'=> $_POST['owner_position'],
	 		'owner_shared'	=> $_POST['owner_shared'],
	 		'remark'		=> $_POST['remark4'],	
	 		'status'		=> "1",				 	
	 		'created_id'	=> $this->tank_auth->get_user_id(),		
	 		'created_date'  => $curr_date,							 					 	
	 		'last_updated'	=> $curr_date);	
	 	
	 	if($is_exist){
	 		unset($data['id_vendor']); unset($data['created_id']); unset($data['created_date']);
	 		$this->crud->update("","owner","id_vendor",$venID,$data); 
	 	} else { $this->crud->insert("","owner",$data); }
	 	
	 }

	 function form_correspondence(){  	
	 	$this->load->view('vendor/detail/correspondence/form_create'); 	
	 } 
	 
	 function form_update_correspondence(){ 
	 	
	 	$id = $this->uri->segment(3);
	 	$data['default'] = $this->crud->browse("","correspondence","id",$id,"false");	  	
	 	
	 	$this->load->view('vendor/detail/correspondence/form_update',$data); 	
	 } 
	 
 function add_document(){ 	//print_r($_POST);  exit;
 	
 	$venID      = $this->session->userdata("venID");
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$file_type  = "-";
 	$remainder   = $this->input->post('remainder');
 	
 	if($_FILES){ 
 		$file_type = $this->m_vendor->get_file_type($_FILES); }
 		
 		$insert = array( 	'id_vendor' => $venID,
 			'doc_name'  => $_POST['doc_name'],
 			'doc_num'   => $_POST['doc_num'],
 			'desc'      => $_POST['desc'],
 			'eff_date'  => $_POST['eff_date'],
 			'exp_date'  => $_POST['exp_date'],
 			'remark'    => $_POST['remark'],
 			'filetype'  => !$file_type ? "-" : $file_type,
 			'remainder' => !$remainder ? 0 : $remainder,
 			'status'    => "1",
 			'created_id'   => $userID,
 			'created_date' => $curr_date,
 			'last_updated' => $curr_date);
 		
 		$where1    = array('doc_num'=>$_POST['doc_num'],'id_vendor' => $venID);
 		$where2    = array('doc_num'=>$_POST['doc_name'],'id_vendor' => $venID);
 		
 		$is_exist1 = $this->crud->is_exist("","correspondence","id",$where1);	 
 		$is_exist2 = $this->crud->is_exist("","correspondence","id",$where2);  
 		$docID     = $this->crud->insert("","correspondence",$insert);

     $id_table 	= 1;//correspondece
     $insert2 	= array(
     	'id_vendor' 	=> $venID,
     	'remark'    	=> $_POST['remark'],
     	'filetype'  	=> $file_type,
     	'status'    	=> "1",
     	'created_id'   	=> $userID,
     	'created_date' 	=> $curr_date,
     	'last_updated' 	=> $curr_date,
     	'filename' 		=> $_POST['doc_name'],
     	'id_table' 		=> $id_table,
     	'id_docs'		=> $docID,
     	'filesize'		=> $_FILES['userfile']['size']
     	); 	
     if($_FILES && $docID && !$is_exist1 && !$is_exist2){ 	
     	
     	$where3    = array('type'=>$file_type);
     	$is_exist3 = $this->crud->is_exist("","file_type","id",$where3); 	    
     	if(!$is_exist3) $this->crud->insert("","file_type",array('type'=>$file_type)); 	    
     	$ftype = $this->crud->browse("","file_type","","","true","type"); 	    
     	foreach($ftype as $val){ $type[] =  $val->type;	}
     	$type = implode("|",$type);
     	
     	$filename =	$docID.".".$file_type;			
     	$path 	  = './uploads/documents/'; 		
     	$config   = $this->m_announce->set_config($filename,$path,$type);
     	
     	$this->load->library('upload', $config);
     	$this->upload->initialize($config); 
     	
     	$msg['error']     = array();
     	$msg['success']   = array();		
     	if ( ! $this->upload->do_upload()){
     		$msg['error']   = $this->upload->display_errors();
     	} else {
     		$msg['success'] = $this->upload->data(); }			
     	}
     	$this->crud->insert('','vendor_docs',$insert2); 	
     	$this->session->set_flashdata('clps','collapse7');
     	redirect("register/detail");
     }
     
     function load_correspondence(){ 	
     	$venID   = $this->session->userdata("venID");
     	$selectcor				= "*,(SELECT z.notif FROM reminder_doc z WHERE a.id=z.id_doc)as remindcount";
     	$data['correspondence'] = $this->crud->browse("","correspondence a","id_vendor",$venID,"true",$selectcor);
     	$whererem				= "id_vendor = $venID AND (CURDATE() BETWEEN a.exp_date - INTERVAL 90 DAY AND a.exp_date OR CURDATE() > a.exp_date)";
     	$data['remindbut']		= $this->crud->browse("","correspondence a","","","true",$selectcor,$whererem);
     	$data['remindbtn']		= $this->m_register->remindbtn($data['remindbut']);
     	$data['correspondence'] = $this->crud->browse("","correspondence","id_vendor",$venID,"true");		
     	$this->load->view('vendor/detail/correspondence',$data); 
     }

     function update_document(){
     	
     	$docID      = $_POST['docID'];
     	$doc_name   = $_POST['doc_name'];
     	$doc_num    = $_POST['doc_num']; 
     	$attcfile   = $_POST['attcfile'];
     	$file_type  = "-";	
     	$upload_file = $_FILES['userfile']['name'];
     	$remainder   = $this->input->post('remainder');
     	
     	if($upload_file && $attcfile){  	    	
     		$file_type = $this->m_vendor->get_file_type($_FILES); 	
     		$path 	   = "./uploads/documents/".$attcfile; 		
     		if (file_exists($path)) unlink($path); 
     	}
     	$update = array( 	'doc_name'  => $_POST['doc_name'],
     		'doc_num'   => $_POST['doc_num'],
     		'desc'      => $_POST['desc'],
     		'eff_date'  => $_POST['eff_date'],
     		'exp_date'  => $_POST['exp_date'],
     		'remark'    => $_POST['remark'],
     		'filetype'  => $file_type,
     		'remainder' => !$remainder ? 0 : $remainder,
     		'status'    => "1",						
     		'last_updated' => date('Y-m-d H:i:s'));

     	$update2 = array(
     		'remark'    	=> $_POST['remark'],
     		'filetype'  	=> $file_type,
     		'status'    	=> "1",						
     		'last_updated' 	=> date('Y-m-d H:i:s'),
     		'filename'		=> $_POST['doc_name']
     		);								
     	if($file_type=="-") unset($update['filetype']);				 
     	
     	$is_exist  = $this->m_vendor->validate_upd_vendor($_POST); 	 
     	if(!$is_exist){ 
     		$this->crud->update("","correspondence","id",$docID,$update);

     		if($upload_file && $attcfile){ 
     			
     			$update2['filesize'] 	= $_FILES['userfile']['size'];
     		}
     		$this->crud->update("","vendor_docs",'id_docs',$docID,$update2);
     	}	 	
     	if($upload_file && !$is_exist){ 	
     		
     		$where3    = array('type'=>$file_type);
     		$is_exist3 = $this->crud->is_exist("","file_type","id",$where3); 	    
     		if(!$is_exist3) $this->crud->insert("","file_type",array('type'=>$file_type)); 	    
     		$ftype = $this->crud->browse("","file_type","","","true","type"); 	    
     		foreach($ftype as $val){ $type[] =  $val->type;	}
     		$type = implode("|",$type);
     		
     		$filename =	$docID.".".$file_type;			
     		$path 	  = './uploads/documents/'; 		
     		$config   = $this->m_announce->set_config($filename,$path,$type);
     		
     		$this->load->library('upload', $config);
     		$this->upload->initialize($config); 
     		
     		$msg['error']     = array();
     		$msg['success']   = array();		
     		if ( ! $this->upload->do_upload()){
     			$msg['error']   = $this->upload->display_errors();
     		} else {
     			$msg['success'] = $this->upload->data(); }			
     		} 	
     		
     		$this->session->set_flashdata('clps','collapse7');
     		redirect("register/detail");
     		
     	}

     	function delete_document(){
     		$docID     = $_POST['id'];
     		$type      = $this->crud->browse("","correspondence","id",$docID,"false","filetype as type")->type;
     		$filename  = $docID.".".$type;
     		$path 	   = "./uploads/documents/".$filename; 		
     		if (file_exists($path)) unlink($path); 
     		$this->crud->delete("","correspondence","id",$docID);
     		$this->crud->delete("","vendor_docs","id_docs",$docID); }

     		function form_org(){
     			
     			$this->load->view('vendor/detail/org/form_create'); 
     		}
     		
     		function load_org(){ 
     			
     			$venID   = $this->session->userdata("venID");	
     			$data['org'] = $this->crud->browse("","organization","id_vendor",$venID,"true");		
     			$this->load->view('vendor/detail/org',$data);  	
     		}
     		
     		function add_person_org(){
     			
     			$curr_date 	= date('Y-m-d H:i:s'); 
     			$userID     = $this->tank_auth->get_user_id();
     			$venID      = $this->session->userdata("venID");
     			$data 		= array('id'			=> null, 						
     				'id_vendor'		=> $venID,
     				'name'			=> $_POST['prsn_name'],
     				'address'		=> $_POST['prsn_addrss'],
     				'phone'			=> $_POST['prsn_phone'],
     				'position'		=> $_POST['prsn_post'],
     				'remark'		=> $_POST['remark3'],
     				'status'		=> '1',
     				'created_id'	=> $userID,
     				'created_date'	=> $curr_date,
     				'last_updated'	=> $curr_date);
     			
     			$this->crud->insert("","organization",$data); 		
     		}

     		function form_update_prsn(){
     			
     			$prsnID  = $this->uri->segment(3);
     			$venID      = $this->session->userdata("venID");
     			
     			$data['default'] = $this->crud->browse("","organization","id",$prsnID,"false"); 	 	
     			$data['vendor'] = $this->crud->browse("","vendor","id",$venID,"false","vendor_name as name");		
     			$this->load->view('vendor/detail/org/form_update',$data);
     			
     		}
     		
     		function update_person_org(){
     			
     			$curr_date 	= date('Y-m-d H:i:s'); 
     			$userID     = $this->tank_auth->get_user_id();
     			$data 		= array('name'		    => $_POST['prsn_name'],
     				'address'		=> $_POST['prsn_addrs'],
     				'phone'		    => $_POST['prsn_phone'],
     				'position'		=> $_POST['prsn_post'],
     				'remark'		=> $_POST['remark'],					 					 					 	
     				'last_updated'	=> $curr_date);	
     			
     			$this->crud->update("","organization","id",$_POST['id'],$data); 
     			
     		}

     		function delete_person(){ 
     			
     			$this->crud->delete("","organization","id",$_POST['id']); 	
     			
     		}

     		function form_ref(){
     			
     			$this->load->view('vendor/detail/ref/form_create');
     			
     		}
     		
     		function load_ref(){ 
     			
     			$venID   = $this->session->userdata("venID");	
     			$data['ref'] = $this->crud->browse("","references","id_vendor",$venID,"true");		
     			$this->load->view('vendor/detail/reference',$data);  	
     			
     		}

     		function add_reference(){
     			
     			$curr_date 	= date('Y-m-d H:i:s'); 
     			$userID     = $this->tank_auth->get_user_id();
     			$venID      = $this->session->userdata("venID");
     			$data 		= array('id'			=> null, 						
     				'id_vendor'		=> $venID,
     				'cust_name'		=> $_POST['cust_name'],
     				'project'		=> $_POST['project'],
     				'point'			=> $_POST['point'],
     				'date'			=> $_POST['pjdate'],
     				'remark'		=> $_POST['remark'],
     				'status'		=> '1',
     				'created_id'	=> $userID,
     				'created_date'	=> $curr_date,
     				'last_updated'	=> $curr_date);
     			
     			$this->crud->insert("","references",$data); 
     			
     		}

     		function form_update_ref(){
     			
     			$refID     = $this->uri->segment(3); 	
     			$data['default'] = $this->crud->browse("","references","id",$refID,"false"); 
     			$this->load->view('vendor/detail/ref/form_update',$data);
     		}
     		
     		function update_reference(){
     			
     			$curr_date 	= date('Y-m-d H:i:s'); 
     			$userID     = $this->tank_auth->get_user_id();
     			$data 		= array('cust_name'		=> $_POST['cust_name'],
     				'project'		=> $_POST['project'],
     				'point'			=> $_POST['point'],
     				'date'			=> $_POST['pjdate'],
     				'remark'		=> $_POST['remark'],					 	
     				'last_updated'	=> $curr_date);
     			
     			$this->crud->update("","references","id",$_POST['id'],$data); 
     		}
     		
     		function delete_ref(){ 
     			
     			$this->crud->delete("","references","id",$_POST['id']); 	
     			
     		}

     		function form_aff(){
     			
     			$this->load->view('vendor/detail/aff/form_create');
     			
     		}
     		
     		function load_aff(){  	
     			$venID   = $this->session->userdata("venID");	
     			$data['aff'] = $this->crud->browse("","affiliates","id_vendor",$venID,"true");		
     			$this->load->view('vendor/detail/affiliate',$data);  	
     		}
     		
     		function add_affiliate(){
     			
     			$curr_date 	= date('Y-m-d H:i:s'); 
     			$userID     = $this->tank_auth->get_user_id();
     			$venID      = $this->session->userdata("venID");
     			$data 		= array('id'			=> null, 						
     				'id_vendor'		=> $venID,
     				'company_name'	=> $_POST['company_name'],
     				'competency'	=> $_POST['competency'],
     				'contact'		=> $_POST['contact'],					 	
     				'remark'		=> $_POST['remark'],
     				'status'		=> '1',
     				'created_id'	=> $userID,
     				'created_date'	=> $curr_date,
     				'last_updated'	=> $curr_date);
     			
     			$this->crud->insert("","affiliates",$data); 
     			
     		}

     		function form_update_aff(){
     			
     			$affID  = $this->uri->segment(3); 	
     			$data['default'] = $this->crud->browse("","affiliates","id",$affID,"false"); 
     			$this->load->view('vendor/detail/aff/form_update',$data); 	
     		}
     		
     		function update_affiliate(){
     			
     			$curr_date 	= date('Y-m-d H:i:s'); 
     			$userID     = $this->tank_auth->get_user_id();
     			$data 		= array('company_name'	=> $_POST['company_name'],
     				'competency'	=> $_POST['competency'],
     				'contact'		=> $_POST['contact'],					 	
     				'remark'		=> $_POST['remark'],					 						 	
     				'last_updated'	=> $curr_date);
     			
     			$this->crud->update("","affiliates","id",$_POST['id'],$data); 
     			
     		}
     		
     		function delete_aff(){
     			
     			$this->crud->delete("","affiliates","id",$_POST['id']); 	
     			
     		}

     		function load_categories(){
     			
     			$venID   = $this->session->userdata("venID");	
     			$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
     			(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";					
     			$data['categories'] = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2);	
     			
     			$this->load->view('vendor/detail/categories',$data);
     			
     		}

     		function form_categories(){
     			
     			$default  = array(''=>'--Select Category--');
     			$dbcat = $this->crud->browse("","category","","","true","id,category");
     			foreach($dbcat as $val){ $category[$val->id] = $val->category; }
     			$data['category'] = $default + $category;
     			
     			$this->load->view('vendor/detail/categories/form_create',$data);
     			
     		}
     		
     		function is_exist_category(){

     			$venID      = $this->session->userdata("venID");
     			$id_cat     = $_POST['id_category'];
     			$id_subcat  = $_POST['id_subcat'];

     			$where    = array('id_vendor'=>$venID,'id_category'=>$id_cat,'id_subcat'=>$id_subcat);
     			$is_exist = $this->crud->is_exist("","vendor_category","id",$where);
     			$result   = (!$is_exist  && $id_subcat) ? "true" : "false"; 
     			
     			$array['status']  =  $result;	
     			echo json_encode($array);

     		}
     		
     		function get_subcategory(){
     			$id_cat  = $_POST['id_cat']; $select = "id,subcategory as subcat"; 	
     			$subcat  = $this->crud->browse("","subcategory","id_cat",$id_cat,"true",$select); 	
     			if($subcat) { 	
     				echo "<option value=''>-- Select Sub Category --</option>";	
     				foreach ($subcat as $row){	echo "<option value='$row->id'>$row->subcat</option>"; }	
     			} else { echo "<option value=''>-- Empty --</option>";	}
     		}
     		
     		function create_category(){
     			
     			$curr_date 	= date('Y-m-d H:i:s'); 
     			$userID     = $this->tank_auth->get_user_id();
     			$venID      = $this->session->userdata("venID");
     			$data 		= array('id'			=> null, 						
     				'id_vendor'		=> $venID,
     				'id_category'	=> $_POST['id_category'],
     				'id_subcat'		=> $_POST['id_subcat'],					 	
     				'status'		=> '1',
     				'created_id'	=> $userID,
     				'created_date'	=> $curr_date,
     				'last_updated'	=> $curr_date);
     			
     			$this->crud->insert("","vendor_category",$data); 
     			
     		}

     		function form_update_category(){
     			
     			$id      = $this->uri->segment(3); 
     			$select  = "id,id_category as id_cat,id_subcat,(SELECT subcategory FROM subcategory r WHERE r.id=l.id_subcat) as subcategory";	
     			$default = $this->crud->browse("","vendor_category l","id",$id,"false",$select);
     			
     			$subcat  = array($default->id_subcat => $default->subcategory);
     			
     			$dbcat = $this->crud->browse("","category","","","true","id,category");
     			foreach($dbcat as $val){ $category[$val->id] = $val->category; }
     			
     			$data['default']     =  $default;
     			$data['category']    =  $category;
     			$data['subcategory'] =  $subcat;
     			
     			$this->load->view('vendor/detail/categories/form_update',$data);
     			
     		}

     		function update_category(){
     			
     			$curr_date 	= date('Y-m-d H:i:s'); 
     			$userID     = $this->tank_auth->get_user_id();
     			$data 		= array('id_category'	=> $_POST['id_category'],
     				'id_subcat'		=> $_POST['id_subcat'],					 						 						 	
     				'last_updated'	=> $curr_date);
     			
     			$this->crud->update("","vendor_category","id",$_POST['id'],$data); 
     			
     		}  
     		
     		function delete_subcategory(){
     			
     			$this->crud->delete("","vendor_category","id",$_POST['id']); 	
     			
     			
     		}

     		function download_file(){
     			
     			$id = $this->uri->segment(3);
     			$this->load->helper('download');
     			
     			$file     = $this->crud->browse("","correspondence","id",$id,"false","id,filetype as type"); 	
     			$filename = $file->id.".".$file->type; 
     			$path 	  = './uploads/documents/'.$filename;			
     			$is_exist = file_exists($path) ? "true" : "false";
     			
     			if($is_exist){
     				$data = file_get_contents($path); 
     				force_download($filename, $data);	} 	

     				$this->session->set_flashdata('clps','collapse7');
     				redirect("register/detail");
     				
     			}

     			function form_document(){
     				
     				$this->load->view('vendor/detail/due_dilligence/form_create');
     			}
     			
     			function load_due_dilligence(){
     				$venID   = $this->session->userdata("venID");	
     				
     				$none    = array('type'=>'0','note'=>'none');
     				$dbrecmd = $this->crud->browse("","vendor_recommend","id_vendor",$venID,"false","type,note");
     				$recmd   = !$dbrecmd ? (object) $none : $dbrecmd;
     				$data['recmd'] = $recmd;
     				
     				
     				$data['due_dill'] = $this->crud->browse("","due_dilligence","id_vendor",$venID,"true");
     				$this->load->view('vendor/detail/due_dilligence',$data);
     			}
     			
     			function add_doc_duedill(){
     				
     				$venID      = $this->session->userdata("venID");
     				$curr_date 	= date('Y-m-d H:i:s'); 
     				$userID     = $this->tank_auth->get_user_id();
     				$file_type  = "-";
     				
     				if($_FILES){
     					$file_type = $this->m_vendor->get_file_type($_FILES); }
     					
     					$insert = array( 	'id_vendor' => $venID,
     						'doc_name'  => $_POST['doc_name'],						
     						'remark'    => $_POST['remark'],
     						'filetype'  => $file_type,
     						'status'    => "1",
     						'created_id'   => $userID,
     						'created_date' => $curr_date,
     						'last_updated' => $curr_date);
     					
     					$where   = array('doc_name'=>$_POST['doc_name'],'id_vendor' => $venID);
     					
     					$is_exist  = $this->crud->is_exist("","due_dilligence","id",$where);  
     					$docID     = $this->crud->insert("","due_dilligence",$insert);	
     					
     					if($_FILES && $docID && !$is_exist){ 	
     						
     						$where3    = array('type'=>$file_type);
     						$is_exist3 = $this->crud->is_exist("","file_type","id",$where3); 	    
     						if(!$is_exist3) $this->crud->insert("","file_type",array('type'=>$file_type)); 	    
     						$ftype = $this->crud->browse("","file_type","","","true","type"); 	    
     						foreach($ftype as $val){ $type[] =  $val->type;	}
     						$type = implode("|",$type);
     						
     						$filename =	$docID.".".$file_type;			
     						$path 	  = './uploads/due_dilligence/'; 		
     						$config   = $this->m_announce->set_config($filename,$path,$type);
     						
     						$this->load->library('upload', $config);
     						$this->upload->initialize($config); 
     						
     						$msg['error']     = array();
     						$msg['success']   = array();		
     						if ( ! $this->upload->do_upload()){
     							$msg['error']   = $this->upload->display_errors();
     						} else {
     							$msg['success'] = $this->upload->data(); }			
     						}
     						
     						$this->session->set_userdata('tab','menu0');
     						redirect("vendor/detail");
     						
     					}

     					function form_update_duedill(){
     						
     						$id  = $this->uri->segment(3);
     						$data['default'] = $this->crud->browse("","due_dilligence","id",$id,"false");
     						$this->load->view('vendor/detail/due_dilligence/form_update',$data);	
     						
     					}
     					
     					function update_duedill(){
     						
     						$docID      = $_POST['docID'];
     						$doc_name   = $_POST['doc_name'];	
     						$attcfile   = $_POST['attcfile'];
     						$file_type  = "-";	
     						$upload_file = $_FILES['userfile']['name'];
     						
     						if($upload_file && $attcfile){  	    	
     							$file_type = $this->m_vendor->get_file_type($_FILES); 	
     							$path 	   = "./uploads/due_dilligence/".$attcfile; 		
     							if (file_exists($path)) unlink($path); 
     						}
     						$update = array( 	'doc_name'  => $_POST['doc_name'],												
     							'remark'    => $_POST['remark'],
     							'filetype'  => $file_type,
     							'status'    => "1",						
     							'last_updated' => date('Y-m-d H:i:s'));	
     						
     						if($file_type=="-") unset($update['filetype']);				 
     						
     						$is_exist  = $this->m_vendor->validate_upd_duedill($_POST); 	 
     						if(!$is_exist){ 
     							$this->crud->update("","due_dilligence","id",$docID,$update); }	
     							
     							if($upload_file && !$is_exist){ 	
     								
     								$where3    = array('type'=>$file_type);
     								$is_exist3 = $this->crud->is_exist("","file_type","id",$where3); 	    
     								if(!$is_exist3) $this->crud->insert("","file_type",array('type'=>$file_type)); 	    
     								$ftype = $this->crud->browse("","file_type","","","true","type"); 	    
     								foreach($ftype as $val){ $type[] =  $val->type;	}
     								$type = implode("|",$type);
     								
     								$filename =	$docID.".".$file_type;			
     								$path 	  = './uploads/due_dilligence/'; 		
     								$config   = $this->m_announce->set_config($filename,$path,$type);
     								
     								$this->load->library('upload', $config);
     								$this->upload->initialize($config); 
     								
     								$msg['error']     = array();
     								$msg['success']   = array();		
     								if ( ! $this->upload->do_upload()){
     									$msg['error']   = $this->upload->display_errors();
     								} else {
     									$msg['success'] = $this->upload->data(); }			
     								}
     								
     								$this->session->set_userdata('tab','menu0');
     								redirect("vendor/detail");	
     								
     							}

     							function delete_duedill(){
     								$docID     = $_POST['id'];
     								$type      = $this->crud->browse("","due_dilligence","id",$docID,"false","filetype as type")->type;
     								$filename  = $docID.".".$type;
     								$path 	   = "./uploads/due_dilligence/".$filename; 		
     								if (file_exists($path)) unlink($path); 
     								$this->crud->delete("","due_dilligence","id",$docID); 
     							}
     							
     							function download_duedill(){
     								
     								$id = $this->uri->segment(3);
     								$this->load->helper('download');
     								
     								$file     = $this->crud->browse("","due_dilligence","id",$id,"false","id,filetype as type"); 	
     								$filename = $file->id.".".$file->type; 
     								$path 	  = './uploads/due_dilligence/'.$filename;			
     								$is_exist = file_exists($path) ? "true" : "false";
     								
     								if($is_exist){
     									$data = file_get_contents($path); 
     									force_download($filename, $data);	} 		
     									
     									$this->session->set_userdata('tab','menu0');
     									redirect("vendor/detail");	
     									
     								}
     								
     								function form_recommend(){
     									
     									$venID   = $this->session->userdata("venID");
     									$none    = array('type'=>'0','note'=>'none');
     									$default = $this->crud->browse("","vendor_recommend","id_vendor",$venID,"false","type,note");
     									$result  = !$default ? (object) $none : $default;
     									$data['recmd'] = $result;
     									
     									$this->load->view('vendor/detail/due_dilligence/form_recmd',$data);
     									
     								}
     								
     								function add_recommend(){
     									
     									$curr_date 	= date('Y-m-d H:i:s'); 
     									$userID     = $this->tank_auth->get_user_id();
     									$venID      = $this->session->userdata("venID");
     									$data 		= array('id_vendor'		=> $venID,
     										'type'			=> $_POST['type'],
     										'note'			=> $_POST['note'],					 	
     										'created_id'	=> $userID,
     										'created_date'	=> $curr_date,
     										'last_updated'	=> $curr_date);
     									
     									
     									$where      = array('id_vendor'=>$venID);
     									$is_exist   = $this->crud->is_exist("","vendor_recommend","id",$where); 
     									
     									if($is_exist){
     										unset($data['id_vendor']); unset($data['created_id']); unset($data['created_date']);
     										$this->crud->update("","vendor_recommend","id_vendor",$venID,$data); 
     									} else { $this->crud->insert("","vendor_recommend",$data); }						 	
     									
     									
     									$this->session->set_userdata('tab','menu0');
     									redirect("vendor/detail");		
     									
     								}
     								
     								function form_approved(){
     									
     									$venID      = $this->session->userdata("venID");
     									$user_group = $this->session->userdata('user_group');
     									
     									$none       = array('type'=>'0','note'=>'none');
     									$order      = array('field'=>'created_date','sort'=>'DESC');
     									$dbrecmd    = $this->crud->browse("","vendor_approval","id_vendor",$venID,"false","id,type,note,level","",$order);
     									$vendor     = $this->crud->browse("","vendor","id",$venID,"false","approval_level as level");		
	$user       = $this->crud->browse("","approval_level","id_group",$user_group,"false","level");  //echo $user->level;
	$level      = !$user ? 0 : $user->level;
	$approved   = ($level==$vendor->level && $level!=0) ? true : false;	
	$data['approved'] = $approved; 
	
	$recmd   = !$dbrecmd ? (object) $none : $dbrecmd;
	$data['level'] = $vendor->level;
	$data['recmd'] = $recmd;
	$this->load->view('vendor/form_approved',$data);
}

function add_approved(){ 	
	
	$venID      = $this->session->userdata("venID"); 	
	$level    	= $_POST['level']; 	$next_level = $level + 1;  
	$type 		= $_POST['type2'];	 	
	
	$insert   = array(  'id_vendor'    => $venID,
		'type'  	   => $type,						
		'note'    	   => $_POST['note2'],
		'level'  	   => $level,						
		'created_id'   => $this->tank_auth->get_user_id(),
		'created_date' => date('Y-m-d H:i:s'),
		'last_updated' => date('Y-m-d H:i:s'));					
	
	
	$where    = array('id_vendor'=>$venID,'type'=>'none');
	$is_exist = $this->crud->is_exist("","vendor_approval","id",$where); 					
	
	if($is_exist){
		$update  = array('type'=> $type,'note' => $_POST['note2'] );							
		$this->crud->update("","vendor_approval","id",$_POST['id'],$update);	
		$this->crud->update("","vendor","id",$venID,array('approval_level'=>$next_level));		
		
	} else if($type=="avl"){	
		
		$this->crud->insert("","vendor_approval",$insert);
		if($level!="3")	$this->crud->update("","vendor","id",$venID,array('approval_level'=>$next_level));			
		if($level=="3") $this->crud->update("","vendor","id",$venID,array('approval_type'=>'avl','approval_level'=>"4",/*'vendor_type'=>'avl',*/'id_stsreg'=>'5'));
		
	} else if($type=="shortlist"){
		
		$this->crud->insert("","vendor_approval",$insert);	
		$update = array('approval_type'=>'shortlist'/*,'vendor_type'=>'shortlist'*/,'approval_level'=>$level);	
		$this->crud->update("","vendor","id",$venID,$update);
	} else if($type=="reject"){	
		
		$this->crud->insert("","vendor_approval",$insert);	
		$update = array('approval_type'=>'reject'/*,'vendor_type'=>'fail'*/ );
		$this->crud->update("","vendor","id",$venID,$update); 	}
		
		
		redirect("vendor/"); 	
	}

	function approved(){ 	
		$venID      = $this->session->userdata("venID"); $mail_cc = array();
		$approval   = $this->crud->browse("","vendor","id",$venID,"false","approval")->approval; 	
		$result     = $this->crud->browse("","vendor_recomend","id_vendor",$venID,"false","result")->result;
		$currentdate= date('Y-m-d H:i:s');

		if($result=="avl"){	
			$level  = $this->m_vendor->get_level_approval($approval);				
			$update = array('approval'=>$level,'last_updated'=>$currentdate);
			$this->crud->update("","vendor","id",$venID,$update);		
			if($level!="4"){		
				$sender    = $level == "2" ? "sm_ibk" : "vp_ib";	
				$mail_cc   = $this->m_user->get_userdata_group($sender);
			}		 	
		} else if($result=="shortlist"){
			$update = array('approval'=>"2",'last_updated'=>$currentdate);		
			$this->crud->update("","vendor","id",$venID,$update);		
			$mail_cc   = $this->m_user->get_userdata_group("sm_ibk");
	} else {	// approved reject from admin sourcing
		$update = array('id_stsreg'=>'4','approval'=>'0','last_updated'=>$currentdate);				
		$this->crud->update("","vendor","id",$venID,$update); 	
	}
	
	$notes = ucfirst($result)." Vendor Approved";
	$this->m_vendor->register_tracking($venID,$notes);				
	//NOTIFIKASI PERSETUJUAN EVALUASI VENDOR TO ADM SRC FROM IBS/IBK/IB	
	//-------------------------------------------
	$select    = "register_num as num,vendor_name as name,email";
	$vendor    = $this->crud->browse("","vendor","id",$venID,"false",$select);
	$subject   = "Notification E-procurement Garuda";
	$select2   = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
	(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory,result,note";
	$list      = $this->crud->browse("","vendor_recomend l","id_vendor",$venID,"true",$select2);					
	$adm_src   = $this->m_user->get_userdata_group("as"); // as is admin sourcing code	
	$to        = array_merge($adm_src,$mail_cc); //print_r($to); exit;
	foreach($to as $value){			
		if(is_valid_email($value->email)){	
			$user    = $this->users->get_user_by_email($value->email);
			if($user){			 			
				$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'recommend','param'=>$venID);							 
				$link    = $this->m_vendor->create_session_link((object) $require);
				
				$param	 =  array('reg_num'		=> $vendor->num,
					'vendor_name' => $vendor->name,
					'category'	=> $list,
					'link'		=> $link,
					'to'			=> $value->username);	
				
				$param['view']  = NOTIF_PATH."EPCGA005"; 		
				$message 		= $this->load->view(NOTIF_TMPL,$param,true); 
				$this->crud->sendMail($value->email,$subject,$message); }
			}	
		}	
	//-----------------------------------------------------
		if($result=="avl" && $approval=="3"){		
			redirect('vendor/verification'); }
			
		}
		
		function form_rejected(){
			$venID   = $this->session->userdata("venID");		
			$data['vendor'] = $this->crud->browse("","vendor","id",$venID,"false","approval");		
			$this->load->view('vendor/reject_form',$data);
		}
		
		function rejected(){ 	
			$venID = $this->session->userdata("venID");
			$notes = $this->input->post('notes');
			$this->m_vendor->register_tracking($venID,$notes);						

			$update = array('approval'=>'0','id_stsreg'=>'4','last_updated'=>date('Y-m-d H:i:s'));				
			$this->crud->update("","vendor","id",$venID,$update);	
	//NOTIFIKASI PERSETUJUAN EVALUASI VENDOR TO ADM SRC FROM IBS/IBK/IB	
	//-------------------------------------------
			$select  = "register_num as num,vendor_name as name,email";
			$vendor  = $this->crud->browse("","vendor","id",$venID,"false",$select);
			$subject =  "Notification E-procurement Garuda";
			$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
			(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory,result,note";
			$list    =  $this->crud->browse("","vendor_recomend l","id_vendor",$venID,"true",$select2);
			$adm_src = $this->m_user->get_userdata_group("as");
			foreach($adm_src as $value){			
				if(is_valid_email($value->email)){						
					$param   =  array('reg_num'	     => $vendor->num,
						'vendor_name'  => $vendor->name,
						'category'	 => $list,
						'notes'		 => $notes,
						'to'			 => $value->username);
					
					$param['view']  = NOTIF_PATH."EPCGA006"; 		
					$message 		= $this->load->view(NOTIF_TMPL,$param,true); 
					$this->crud->sendMail($value->email,$subject,$message);				
				}	
			}	
			
		}
		
		function set_notes_tab(){
			$id = $this->uri->segment(3);
			$this->session->set_userdata("venID",$id);	
			$this->session->set_userdata('tab','menu9');
			redirect("vendor/detail");
		}
		
		function verification(){ 	
			$venID   = $this->session->userdata("venID");	
			$expired = date('Y-m-d H:i:s', strtotime("+10 days")); $session = uniqid();  $table = "bank_account_mail";
			$footer  = array( 'created_id'   => $this->tank_auth->get_user_id(),
				'created_date' => date('Y-m-d H:i:s'),
				'last_updated' => date('Y-m-d H:i:s'));	
			$insert  = array( 'session'		 => $session,
				'id_vendor'    => $venID,
				'expired_date' => $expired);					  					  
			
			$select  = "register_num as num,vendor_name as name,email,vendor_type as type,
			(SELECT result FROM vendor_recomend r WHERE r.id_vendor=l.id LIMIT 1) as result"; 	
			$vendor  = $this->crud->browse("","vendor l","id",$venID,"false",$select);
			
			if($vendor->result=="avl") { 
				$this->crud->insert("","session_verification",$insert + $footer);				
				$link    		= base_url()."verification/bank_account/".$session;	 	
				$subject 		= "Notification E-procurement Garuda";		
				$param          = array('reg_num'=>$vendor->num,'vendor_name'=>$vendor->name,'link'=>$link);			  
				$param['view']  = NOTIF_PATH."EPCGA007"; 		
				$message 		= $this->load->view(NOTIF_TMPL,$param,true);		
				$this->crud->sendMail($vendor->email,$subject,$message);		
		//------------------- Set expired date for robot mail---------------------------------------
				$is_exist = $this->crud->browse("",$table,"id_vendor",$venID,"false","id,count");
				$count    = $is_exist ? ($is_exist->count + 1) : 1 ;
				$insert2  = array('id_vendor'=>$venID,'count'=>$count,'expired_date'=> $expired,'status'=>'1');
				if($is_exist){	unset($insert2['id_vendor']); 			
				$this->crud->update("",$table,"id",$is_exist->id,$insert2); 
			} else { $this->crud->insert("",$table,$insert2 + $footer);	}
		//--------------------------------------------------------------			
		} 
		
	}

	function is_bankaccount_filled(){
		
		$venID      = $this->session->userdata("venID");
		$where      = array('id_vendor'=>$venID); 	
		$filter     = array('id','id_vendor','status','created_id','created_date','last_updated');
		$check      = $this->crud->get_field("","bank_account",$filter_field="");
		
		foreach($check as $field){
			$is_exist   = $this->crud->is_exist("","bank_account",$field,$where);
			$validate[] = $is_exist ? "true" : "false"; }
			
			$result = in_array("false",$validate) ? "false" : "true";
			
			echo $result; 	
		}
		
		function verification_to_avl(){ 	
			$venID   = $this->session->userdata("venID");	 
			$update = array('id_stsreg'=>'6','last_updated'=>date('Y-m-d H:i:s'));  
			$this->crud->update("","vendor","id",$venID,$update);	
	//NOTIFIKASI PERMINTAAN RECON KE WA1	
	//-------------------------------------------
			$select    = "register_num as num,vendor_name as name,email";
			$vendor    = $this->crud->browse("","vendor","id",$venID,"false",$select);
			$subject   =  "Notification E-procurement Garuda";
			$select2   = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
			(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory,result,note";
			$list      = $this->crud->browse("","vendor_recomend l","id_vendor",$venID,"true",$select2);					
	$stf_wa1   = $this->m_user->get_userdata_group("stf_wa1"); // 
	foreach($stf_wa1 as $value){			
		if(is_valid_email($value->email)){			
			$user    = $this->users->get_user_by_email($value->email);
			if($user){			 			
				$require = array('email'=>$user->email,'username'=>$user->username,'request'=>'account_number','param'=>$venID);							 
				$link    = $this->m_vendor->create_session_link((object) $require);	
				
				$param   =  array('reg_num'		=> $vendor->num,
					'vendor_name' => $vendor->name,
					'category'	=> $list,
					'link'		=> $link,
					'to'			=> $value->username);	
				
				$param['view']  = NOTIF_PATH."EPCGA010"; 		
				$message 		= $this->load->view(NOTIF_TMPL,$param,true);				  
				$this->crud->sendMail($value->email,$subject,$message); }
			}	
		}	
	//---------------------------------------------
		$notes = "Vendor Verification";
		$this->m_vendor->register_tracking($venID,$notes);
		
	}  

	function finalization(){ 	
		$venID     = $this->input->post("id_vendor");	 $account = array(); 
		$select    = "register_num as num,vendor_name as name,email,vendor_type as type,vendor_num,
		(SELECT status FROM vendor_account r WHERE r.id_vendor=l.id) as status,
		(SELECT result FROM vendor_recomend t WHERE t.id_vendor=l.id LIMIT 1) as result,
		(SELECT id FROM vendor_rejected s WHERE s.id_vendor=l.id AND status=1 LIMIT 1) as rejected"; 	
	$vendor     = $this->crud->browse("","vendor l","id",$venID,"false",$select);  //print_r($vendor); exit;	
	$vendor_num = $this->m_vendor->get_vendor_number();
	$currentdate= date('Y-m-d H:i:s');
	$user_id    = $this->tank_auth->get_user_id();
	$footer     = array('created_id'=>$user_id,'created_date'=>$currentdate,'last_updated' =>$currentdate);	
	
	$res = "true"; $msg = ""; 
	
	if($vendor->result=="avl"){		
		$email_activation = $this->config->item('email_activation','tank_auth');
		$username 		  = $this->m_vendor->random_username($vendor->name); 
		$password 		  = uniqid();	
		$email 			  = strtolower($vendor->email);		
		
		$account  = array(	'vendor_num'	=> $vendor->vendor_num,
			'username'		=> $username,
			'email'			=> $email,
			'password'		=> $password); 				
		$success = $this->tank_auth->create_user($username,$email,$password,$email_activation); 
		if($success){	
			$user_id  = $success['user_id'];		
			$update1  = array('vendor_type'=>'avl','id_stsreg'=>'8','vendor_num'=>$vendor_num,
				'status'=>'active','approved_date'=>$currentdate,'last_updated'=>$currentdate);									
			$update2  = array('activated'=>'1','id_vendor'=>$venID);								
			$insert   = array('user_id'=>$user_id,'group_id'=>'8');	
			
			$this->crud->update("","vendor","id",$venID,$update1);	  
			$this->crud->update("","users","id",$user_id,$update2);
			$this->crud->insert("","users_groups",$insert + $footer); 
			
			$is_promote = $this->crud->browse("","vendor_promote","id_vendor",$venID,"false","id",array('status'=>'1'));
			if($is_promote){
				$update3 = array('status'=>'0','last_updated'=>$currentdate); 
				$this->crud->update("","vendor_promote","id",$is_promote->id,$update3); }			
			} else { 		
				$errors = $this->tank_auth->get_error_message();
				foreach ($errors as $k => $v) {
					$word = ucwords(str_replace("_"," ",$v)); $words[] = $word; } 
					$msg  = implode(",",$words)." , Finalization Canceled !"; $res = "false";
				} 	
	// finalization for "reject" and "shortlist"
	//------------------------------------------		
			} else { 				
				$update = array('vendor_type'=>$vendor->result,'status'=>'active','approved_date'=>$currentdate,'last_updated'=>$currentdate);
				if($vendor->result=="shortlist") { $update['vendor_num'] = $vendor_num; }											
				$this->crud->update("","vendor","id",$venID,$update); }
				
				if($res=="true"){		
					$this->crud->multiple_delete("","vendor_category",array('id_vendor'=>$venID,'status'=>'0'));			
					$FORM    =  $this->m_vendor->define_form($vendor->result);	
					$subject =  "Notification E-procurement Garuda";			
					$param   =  array('reg_num'=>$vendor->num,'vendor_name'=>$vendor->name,'account'=> (object) $account);			  
					$param['view']  = NOTIF_PATH.$FORM; 		
					$message 		= $this->load->view(NOTIF_TMPL,$param,true);				
					$this->crud->sendMail($email,$subject,$message);
					
					$this->m_vendor->register_tracking($venID,"Vendor Finalization");
				}

				$array['status']  =  $res;	
				$array['msg']     =  $msg;	
				echo json_encode($array);
			}

		}

