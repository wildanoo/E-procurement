<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_document extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('crud');
	}

	function authenticate(){
		$allowed  	= array(135);
		$permit  	= $this->session->userdata('permissions');
		$permit 	= explode(",",$permit);
		foreach($permit as $str){
			$permissions[] = preg_replace("/[^0-9]/","",$str);
		}
		$result 	= array_intersect($allowed, $permissions);
		if($result){ return true;} return false;
	}

	function convert_mb($size){
		
		$size = number_format($size / 1048576, 2) . ' MB';
		return $size;
		
	}

	function convert_kb($size){
		$size = number_format($size / 1024,2) . ' KB';
		return $size;
	}

	function get_user_level($user_group){

		$user       = $this->crud->browse("","approval_level","id_group",$user_group,"false","level");
		$user_level = !$user ? 0 : $user->level;

		return $user_level;

	}

	function get_total_record_search($db,$table,$where="",$group_by="",$select="",$joins=array(),$like=""){ 
		if(!$db)$db="db";
		$this->$db->db_select();
		if($where){ $this->$db->where($where); }
		if($like) $this->$db->like($like['col'],$like['field']); 
		if($group_by){ $this->$db->group_by($group_by); }
		if(!$select){ $select ="*"; } $this->$db->select($select);
		if (count($joins) > 0) {
			foreach ($joins as $join) $this->db->join($join[0], $join[1], $join[2]);
		}

		return $this->$db->get($table)->num_rows();
	}

	function browse_search_join_with_paging($db="",$table,$field="",$id="",$multi="",$select="",$joins=array(),$where="",$order_by="",$num="",$offset="",$group_by="",$like=""){  

		if(!$db)$db="db";
		$this->$db->db_select();	
		if($where)$this->$db->where($where);
		if($order_by)$this->$db->order_by($order_by['field'], $order_by['order']);
		if($like) $this->$db->like($like['col'],$like['field']);
		if($group_by){ $this->$db->group_by($group_by); }
		if(!$select){ $select ="*"; } $this->$db->select($select);
		if (count($joins) > 0) {
			foreach ($joins as $join) $this->db->join($join[0], $join[1], $join[2]);
		}
		if($id || $id!="") {
			if($multi=="true"){ $result = $this->$db->get_where($table, array($field => $id))->result(); 
			} else { $result = $this->$db->get_where($table, array($field => $id))->row();}
		} else 	{ $result = $this->$db->get($table,$num,$offset)->result();	}
		return $result;	
	}

}

/* End of file m_document.php */
/* Location: ./application/models/m_document.php */