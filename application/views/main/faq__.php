<div class="about-intro-wrap pull-left">
    <a name="faq"><span></a>
    <div class="head" style="margin-top: 85px;padding-left:0;padding-right:0;">
        <h1>FAQS</h1>
        <a href="#faq" class="next-section about animate2">
            <i class="fa fa-angle-double-down"></i>
        </a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 faq-tabs-wrap wow fadeInUp animated" data-wow-delay="0.5s" data-wow-offset="200">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs col-md-4 col-sm-4 col-xs-5">
                        <li class="active"><a href="#a" data-toggle="tab"><span class="faq-ques">Bagaimana Rekanan dapat mengikuti proses pengadaan Garuda?</span><i class="right-arr"></i></a></li>
                        <li><a href="#b" data-toggle="tab"><span class="faq-ques">Dimana Rekanan dapat mengetahui kebutuhan pengadaan Garuda?</span><i class="right-arr"></i></a></li>
                        <li><a href="#c" data-toggle="tab"><span class="faq-ques">Mengapa Calon Rekanan tidak bisa menjadi rekanan Garuda?</span><i class="right-arr"></i></a></li>
                        <li><a href="#d" data-toggle="tab"><span class="faq-ques">Siapa saja yang dapat menjadi rekanan Garuda?</span><i class="right-arr"></i></a></li>
                        <li><a href="#e" data-toggle="tab"><span class="faq-ques">Apa persyaratan untuk menjadi rekanan Garuda?</span><i class="right-arr"></i></a></li>
                    </ul>

                    <div class="tab-content col-md-8 col-sm-8 col-xs-7 pull-right">
                        <div class="tab-pane active" id="a">
                            <div class="dept-title-tabs">Bagaimana Rekanan dapat mengikuti proses pengadaan Garuda?</div>
                            <p>Setiap rekanan yang ingin mengikuti proses pengadaan di PT Garuda Indonesia harus melakukan registrasi terlebih dahulu dengan mengirimkan data profile perusahaan dan kelengkapan bukti dokumen sesuai dengan bidang usahanya.</p>
                        </div>

                        <div class="tab-pane" id="b">
                            <div class="dept-title-tabs">Dimana Rekanan dapat mengetahui kebutuhan pengadaan Garuda?</div>
                            <p>Kebutuhan pengadaan Garuda dapat diketahui dari pengumuman web pengadaan Garuda atau akan diberitahukan melalui undangan ke masing-masing alamat peserta.</p>
                        </div>

                        <div class="tab-pane" id="c">
                            <div class="dept-title-tabs">Mengapa Calon Rekanan tidak bisa menjadi rekanan Garuda?</div>
                            <p>Beberapa alasan mengapa perusahaan yang meregister tidak bisa menjadi rekanan Garuda antara lain :</p>
                            <p>Pada saat tertentu Garuda belum memerlukan komoditi tertentu, sehingga calon rekanan dengan bidang usaha yang bersangkutan belum dipertimbangkan untuk menjadi rekanan<br>
                                Jumlah rekanan yang terdaftar di Garuda pada saat tertentu sudah mencukupi untuk memenuhi kebutuhan Garuda<br>
                                Persyaratan untuk menjadi rekanan Garuda belum dapat dipenuhi atau oleh calon rekanan<br>
                                Tidak sesuai dengan kebijakan pengadaan<br>
                                Pernah dan atau sedang ada permasalahan yang menyangkut kinerja yang tidak memuaskan dengan pengadaan Garuda sebelumnya<br>
                                Berdasarkan penilaian (assessment) atau referensi, bahwa calon rekanan yang bersangkutan mempunyai kinerja yang tidak baik atau masih perlu dipertimbangkan untuk dapat menjadi rekanan.</p>
                        </div>

                        <div class="tab-pane" id="d">
                            <div class="dept-title-tabs">Siapa saja yang dapat menjadi rekanan Garuda?</div>
                            <p>Setiap perusahaan dengan bidang usaha yang sesuai dengan kebutuhan Garuda dan mempunyai kinerja yang baik dapat menjadi rekanan Garuda. Dalam memilih rekanannya, kebijakan Garuda lebih menitikberatkan pengadaan secara langsung ke sumber penghasil barang/jasa, seperti pabrikan atau distributor resmi dan menghindari pengadaan barang/jasa melalui perantara yang tidak memberikan nilai tambah.</p>
                        </div>

                        <div class="tab-pane" id="e">
                            <div class="dept-title-tabs">Apa persyaratan untuk menjadi rekanan Garuda?</div>
                            <p>Persyaratan untuk menjadi rekanan Garuda antara lain:</p>
                            <p>Menunjukkan bukti otentik sebagai badan usaha yang jelas di bidang usahanya<br>Mempunyai NPWP dan PKP yang masih berlaku untuk perusahaan dalam negeri atau certificate of domicile untuk perusahaan asing.<br>
                                Tidak sedang terlibat dalam kasus pelanggaran hukum, termasuk tetapi tidak terbatas pada palanggaran hukum pidana, perdata, perpajakan dan atau permasalahan lain<br>
                                Memenuhi persyaratan administrasi dan teknis yang diperlukan oleh Garuda.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>