<div class="page-header" style="vertical-align: middle">
  <h3 style="display:inline-block">Detail <?php echo $form; ?></h3>
</div> 

<script type="text/javascript">
$(document).ready(function(){
$(".btn_delete").click(function() {
    var arg;
    arg = $(this).attr("data");
	$.ajax({
	   type: "POST",
	   data: {arg:arg},
	   url: baseUrl + 'sourcing/deleteDocumentReq',
	   success: function(msg){
	     location.reload();
	   }
	});
});
});
</script>

<div id="debug"></div>
<style type="text/css">
.control-label{
	line-height: 2.6;
}
</style>
<input class="form-control" value="<?php echo $this->uri->segment(3); ?>" type="hidden" name="id_announce" id="id_announce" />
<div class="col-sm-12">
	<label class="control-label col-sm-2" for="invitation_number"><strong>Invitation Number</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo $def->code; ?>" type="text" name="invitation_number" id="invitation_number" readonly/>
	</div>

	<label class="control-label col-sm-2" for="space"> </label>
	<div class="col-sm-4">
	</div>
</div>

<div class="col-sm-12">
	<label class="control-label col-sm-2" for="content_type"><strong>Category</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo $def->category; ?>" type="text" name="content_type" id="content_type" readonly/>
	</div>

	<label class="control-label col-sm-2" for="subcontent_type"><strong>Sub Category</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo $def->subcategory; ?>" type="text" name="subcontent_type" id="subcontent_type" readonly/>
	</div>
</div>

<div class="col-sm-12">
	<label class="control-label col-sm-2" for="start_date"><strong>Start Date</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo $def->first_valdate; ?>" type="text" name="start_date" id="start_date" readonly/>
	</div>

	<label class="control-label col-sm-2" for="end_date"><strong>End Date</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo $def->end_valdate; ?>" type="text" name="end_date" id="end_date" readonly/>
	</div>
</div>

<div class="col-sm-12">
	<?php
	if (date('Y-m-d H:i:s') < $def->publish_date){$status = 0;}
	elseif (date('Y-m-d H:i:s') >= $def->publish_date && date('Y-m-d H:i:s') < $def->first_valdate){$status = 1;}
	elseif (date('Y-m-d H:i:s') >= $def->first_valdate && date('Y-m-d H:i:s') <= $def->end_valdate){$status = 2;}
	elseif ($def->status == 7){$status = 4;}
	elseif ($def->status == 8){$status = 99;}
	else {$status = 3;}
	?>
	<label class="control-label col-sm-2" for="start_date"><strong>Status</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo array_key_exists($status, $status_ref)?$status_ref[$status]:'-'; ?>" type="text" name="start_date" id="start_date" readonly/>
	</div>
</div>
<div class="col-sm-12"><div class="row"><hr/></div></div>
<div class="col-sm-12">
	<div class="col-sm-6">
		<label class="control-label" for="end_date"><strong>Title in Bahasa</strong></label>
		<input class="form-control" value="<?php echo $def->title_ind; ?>" type="text" name="title_ind" id="title_ind" readonly/>
	</div>
	<div class="col-sm-6">
		<label class="control-label" for="end_date"><strong>Title in English</strong></label>
		<input class="form-control" value="<?php echo $def->title_eng; ?>" type="text" name="title_eng" id="title_eng" readonly/>
	</div>
</div>

<div class="col-sm-12">
	<div class="col-sm-6">
		<label class="control-label" for="end_date"><strong>Description in Bahasa</strong></label>
		<span class="form-control" style="max-height: 300px;min-height:100px;overflow-y:auto;"><?php echo $content_ind; ?></span>
	</div>

	<div class="col-sm-6">
		<label class="control-label" for="end_date"><strong>Description in English</strong></label>
		<span class="form-control" style="max-height: 300px;min-height:100px;overflow-y:auto;"><?php echo $content_eng; ?></span>
	</div>
</div>
<div class="col-sm-12"><div class="row"><hr/></div></div>
<div class="col-sm-12 form_tab_content" style="padding-left:0px;">
	<div>
		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#list_document" aria-expanded="true"><strong>Document Required</strong></a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane fade active in" style="padding:15px 15px 0;">
				<table id="browse" class="table table-striped table-bordered" style="font-size: 13px">
					<thead>
				        <tr>
				            <th>No</th>
				            <th>Document Name</th>
				            <th>Description</th>
				            <th>Remarks</th>
				            <th>Created Date</th>
				            <th>Attachment</th>         
				            <th>Action</th>
				        </tr>
					</thead>
				    <tbody>
				    	<?php			
						if(count($documents)>0){ $num = 1;
							foreach($documents as $row){ ?>
								<tr>
									<td><?php echo $num; ?></td>
									<td><?php echo $row->filename.'_'.$row->counter; ?></td>
									<td><?php echo $row->description; ?></td>
									<td><?php echo $row->remarks; ?></td>
									<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
									<?php 
									$docreqs 	 = "./uploads/attach/".$row->filename."_".$row->counter.".pdf";
									$docreqs_url = $row->filename."_".$row->counter;
									echo file_exists($docreqs)?
									"<td><a target='_blank' href=".base_url().'uploads/attach/'.$docreqs_url.'.pdf'.">View</a></td>":
									"<td>-</td>";
									?>
									<td width="9%" class="text-center">
										<input type="button" data="<?php echo $docreqs_url;?>" id="btn_delete" class="btn btn-info btn-sm btn_delete" title="Delete" value="Delete" style="width:80px;">
									</td>
								</tr>
						<?php $num++; } ?>
						<?php } else { ?><tr><td colspan="7" style="text-align:center">[ No Documents Entry ]</td></tr><?php } ?>
					</tbody>
				</table>
		    </div>
		</div>
	</div>
</div>
</div>
<div class="container" style="padding-left:0px;"><hr/></div>
<div class="col-sm-12 form_tab_content" style="padding-right:0px;">
	<div>
		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#list_participant" aria-expanded="true"><strong>List of Participants</strong></a></li>
		</ul>

		<div class="tab-content">
			<div id="list_participant" class="tab-pane fade active in" style="padding:15px 15px 0;">
				<table id="browse" class="table table-striped table-bordered" style="font-size: 13px">
					<thead>
				        <tr>
				            <th>Vendor Registration</th>
				            <th>Company Name</th>
				            <th>Joined Date</th>
				            <th>Status</th>
				        </tr>
					</thead>
				    <tbody>
				    	<?php			
						if($def_detail){
							foreach($def_detail as $row){ ?>	
							<tr>
								<td><?php echo $row->register_num ?></td>
								<td><?php echo $row->vendor_name ?></td>
								<td><?php echo date( 'M d Y', strtotime($row->last_updated)) ?></td>
								<td><?php echo array_key_exists($row->id_stsreg, $status_reference)? $status_reference[$row->id_stsreg]:"-" ?></td>
							</tr>
						<?php } ?>
						<?php } else { ?><tr><td colspan="7">-</td></tr><?php } ?>
					</tbody>
				</table>
		    </div>
		</div>
	</div>
</div>