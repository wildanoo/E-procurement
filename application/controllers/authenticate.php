<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authenticate extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('cookie');
    }
    
 function index(){
 	redirect('/auth/login/'); 
 } 

 function login(){ 	
 	if ($this->tank_auth->is_logged_in()) {	// logged in
 		$table 	   = "session_notification"; 
 		$token     = $this->uri->segment(3); $table = "session_notification";
 		$session   = $this->crud->browse("",$table,"token",$token,"false");  		
 		
 		$username = $this->tank_auth->get_username();
 		if($session->username==$username){
			$update    = array('status'=>"0",'last_updated'=>$currentdate);		
			$this->crud->update("",$table,"token",$token,$update);
			$request   = "authenticate/".$session->request."/".$session->parameter;    
       		redirect($request);     
		}  else { redirect('/auth/login/');  }

 	} else { redirect('/auth/login/');  } 	
 }
 
 function session(){ 
 	$token     = $this->uri->segment(3); $table = "session_notification";
 	$session   = $this->crud->browse("",$table,"token",$token,"false"); 
 	$current   = strtotime("now"); $expired = strtotime($session->expired_date); 	

 	if($expired >= $current && $session->status==1){ 
 		$this->tank_auth->logout();		
 		$data['token'] = $token;
 		$data['view']  = 'main/login_link';
		$this->load->view('layout/template_public',$data);        
	} else {  redirect('/auth/login/');  }
 	
 }

 function data_vendor(){
 	islogged_in();
 	$venID = $this->uri->segment(3);
 	redirect('data_vendor/getList/'.$venID);
 }

 function change_request(){
 	islogged_in();
 	$venID = $this->uri->segment(3);
 	redirect('change_request/detail/?id='.$venID);
 }

 function change_request_avl(){
 	islogged_in();
 	$venID = $this->uri->segment(3);
 	redirect('change_request_avl/detail/?id='.$venID);
 }

 function redlist(){
 	islogged_in();
  	$venID   = $this->uri->segment(3);
  
  	$this->session->set_userdata('sess_auth', $venID);
  	redirect('redlist/search');		
 }

 function redlist_detail(){
 	islogged_in();
  	$id   = $this->uri->segment(3);
  
  	redirect('redlist/detail_redlist/'.$id);
 }
 
 function recommend(){
  	islogged_in();
  	$uri   = $this->uri->segment(3);
  	$param = explode("/",$uri);	$venID = $param['0']; 
  	 	 	
 	$this->session->set_userdata("venID",$venID);
	$this->session->set_userdata('tab','menu2');
		
	$response = "register/detail/".$venID."/register";
	redirect($response); 	
  }
  
 function bank_account(){
	  	islogged_in();
	  	$uri   = $this->uri->segment(3);
	  	$param = explode("/",$uri);	$venID = $param['0']; 
	  	
	  	$this->session->set_userdata("venID",$venID);
	  	$this->session->set_flashdata('clps','collapse3');
	  	
	  	$response = "register/detail/".$venID."/register";
		redirect($response);   	
  }
  
 function account_number(){
	  	islogged_in();
	  	$uri   = $this->uri->segment(3);
	  	$param = explode("/",$uri);	$venID = $param['0']; 
	  		  	
	  	$this->session->set_userdata("venID",$venID);	  	
	  	$response = "verification/account_form/";
		redirect($response);   	
  }

 function inactive_vend(){
		islogged_in();
		$uri 	= $this->uri->segment(3);
		$param = explode("/",$uri);	$inaID = $param['0'];

		$this->session->set_userdata('inaID',$inaID);

  		$response = "inactive/sorting";
  		redirect($response); 
  	}

 function active_vend(){
  		islogged_in();
  		$uri 	= $this->uri->segment(3);
  		$param = explode("/",$uri);	$inaID = $param['0'];

  		$this->session->set_userdata('inaID',$inaID);

  		$response = "active/sorting";
  		redirect($response); 
  }

 function del_vend_app(){
  		islogged_in();
  		$uri 	= $this->uri->segment(3);
  		$param = explode("/",$uri);	$venID = $param['0'];

  		$this->session->set_userdata("venID",$venID);

  		$response = "vendorlist/sorting";
  		redirect($response); 
  	}	

 function del_vend_req(){
  		islogged_in();
  		$uri 	= $this->uri->segment(3);
  		$param = explode("/",$uri);	$venID = $param['0'];

  		$this->session->set_userdata("venID",$venID);

  		$response = "vendordelete/sorting";
  		redirect($response); 
  	}

 function blacklist_vend(){
      islogged_in();
      $uri  = $this->uri->segment(3);
      $param = explode("/",$uri); $venID = $param['0'];

      $this->session->set_userdata("blackID",$venID);

      $response = "blacklist/search";
      redirect($response); 
    }   

}

