<script type="text/javascript">
  $(function () { 
   	  var baseUrl  = "<?php echo base_url(); ?>"; 
   	  
   	   $('#btn_cancel12').click(function(){    			
	   		$("#container12").load(baseUrl+'vendor/load_recomendation');
	   }); 
   	   
   	   $('#btn_rejected12').click(function(){
   	  
   	  		var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	    
		    var values   = $("#form_reject").serializeArray();
			values.push({name: csrf,value: token});
			values = jQuery.param(values);
			 	$.post( baseUrl+ "vendor/rejected", values )
				  .done(function( resp ) {	
				  		document.location.href= baseUrl + 'register/';		    	
				    	//$("#debug_reject").html(resp);
				});
	     	
	     });    
   	  
     
  });

</script>

<div id="debug_reject"></div>
<h3>FORM REJECT</h3><hr />
<?php echo form_open('#',array('id'=>'form_reject')); ?>
<table width="60%">
	<tr>
		<td style="vertical-align: top;"><label>Notes</label></td><td style="vertical-align: top;">:</td>
		<td><textarea name="notes" id="notes" cols="20" rows="5" class="form-control" placeholder="Notes" required></textarea></td>
	</tr>
	<tr>
		<td align="right" colspan="3">
			<button type="button" id="btn_cancel12" class="btn btn-default">Cancel</button>
			<button type="button" id="btn_rejected12" class="btn btn-info btn-primary">Submit</button>			
		</td>
	</tr>
</table>

