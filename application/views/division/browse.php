<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){	 
	 $(function(){  });
		var baseUrl = "<?php echo base_url(); ?>";

		$('#departemen').change(function(){ 
			var departemen = $('#departemen').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'department/set_sess_departemen',			   
               data : 'field=id&dept_name='+departemen,
               success: function(data){ 
                   //alert(category);	
					 //$('#debug').html(resp);
					 //dialogRef.close();				 	
				 	document.location.href= baseUrl + 'department/';
			   }		   
            });	
		
		});
		
		$("#srch-term").autocomplete({
			source: baseUrl + 'department/get_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
	        var search = $("#srch-term").val(); // alert(search);
	        var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
	        $.ajax({
	           url  : baseUrl + 'department/set_sess_search',
	           type : "POST",              		               
	           data : csrf +'='+ token +'&search='+search,
	           success: function(resp){    //$("#debug").html(resp);            		            
				    document.location.href= baseUrl + 'department/';
	           }
	        });
	    });
	    	 
	 $('#btn_create').click(function(){ 
	 	  form_create();
	 	  //alert('test');
	 });	
});

	function form_create(){
		var baseUrl = "<?php echo base_url(); ?>";		 
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		BootstrapDialog.show({
		title: 'Create Division',
        message: $('<div></div>').load(baseUrl + "division/form_create/"),
        buttons: [{ label: 'Submit',
	                cssClass: 'btn btn-primary btn-sm',		               
	                action: function(dialogRef){
		                 var id_office  = $("#id_office").val(); 
	                	 var division_code  = $("#division_code").val();
	                	 var division_name  = $("#division_name").val(); 
	                	 
	                	 $.post( baseUrl + 'division/is_exist', { csrf: token , id_office : id_office , division_code : division_code , division_name : division_name })
						 .done(function( resp ) {
								 //$('#debug').html(resp);
								 //dialogRef.close();
								var json = $.parseJSON(resp);
								if (json.status=='true'){
				                	var values = $("#form").serializeArray();
				                	values.push({ name: csrf,value: token });
									values = jQuery.param(values);
									 $.ajax({
							               url  : baseUrl + 'division/create',
							               type : "POST",              		               
							               data : values,
							               success: function(resp){
							               		document.location.href= baseUrl + 'division/';	               				               		  
									       }
							            });   
								} else { 	
									 $("#msg1").html(json.msg1); 
									 $("#msg2").html(json.msg2); 
									 $("#msg3").html(json.msg3); 
							}
	               		 });  
	                }
	            }, {
	            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
			                action: function(dialogRef) {
			                	document.location.href= baseUrl + 'division/';
			                    dialogRef.close(); 
			                }
		        }]
    });
}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Division',
            message: $('<div></div>').load(baseUrl + "division/form_update/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef)
		                {
			                 var id_office  = $("#id_office").val();
		                	 var division_code  = $("#division_code").val(); 
		                	 var division_name  = $("#division_name").val(); 
		                	 
		                	 $.post( baseUrl + 'division/is_exist', { csrf: token , id_office : id_office , division_code : division_code , division_name : division_name })
							 .done(function( resp ) {
//									 $('#debug').html(resp);
//									 dialogRef.close(); 
									var json = $.parseJSON(resp);
									if (json.status=='true'){
					                	var values = $("#form").serializeArray();
					                	values.push({ name: csrf,value: token });
										values = jQuery.param(values);
										 $.ajax({
								               url  : baseUrl + 'division/update',
								               type : "POST",              		               
								               data : values,
								               success: function(resp){
								               		document.location.href= baseUrl + 'division/';	               				               		  
										       }
								            });   
									} else { 	
									 $("#msg1").html(json.msg1); 
									 $("#msg2").html(json.msg2); 
									 $("#msg3").html(json.msg3); 
								}
		               		 });  
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'division/';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>
<script src="<?php echo base_url()?>assets/js/sorttable.js"></script>
</head>
<body>
<div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Manage Division</h3>
              </div><!-- /.box-header -->
                <br>
				    <?php if($this->session->flashdata('msg_warning')!=null) { ?>
                	<div class="alert alert-warning">
  						<strong>Warning!</strong> <?php echo $this->session->flashdata('msg_warning'); ?>
					</div>
					<?php } ?>
                <div class="col-md-2 pull-left">
                    <input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Division" value="New Division" style="width:130px;" title="Create New Division"/>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
				<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
                </div>
                    
                <div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('division/search',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>division" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		        </div>
		        </div> 
                    
<div class="box-body">
<table border="1" id="example2" class="table table-bordered table-hover sortable" cellspacing="0" width="80%" style="font-size: 13px">
	<thead>
		<tr>
            <th class="sorttable-sorted-reverse">No</th>
            <th class="">Office Name</th>
            <th class="">Division Code</th>
            <th class="">Division Name</th>
            <!--th class="">Status</th>-->
            <th class="">Created Date</th>
            <th class="">Updated Date</th>
            <th class="">Updated By</th>
            <th>Action</th>
        </tr>
     </thead>
     <tbody>
 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td><?php echo $num = $num+1; ?></td>
			<td><?php echo $row->id_office; ?></td>	
			<td><?php echo $row->division_code; ?></td>	
			<td><?php echo $row->division_name; ?></td>
 			<!--td><?php echo $row->status==1 ? "Active" : "Not Active"; ?></td>-->
 			<td align="center"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>	
 			<td align="center"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>	
 			<td align="center"><?php echo $row->created_id; ?></td>						
				<td><?php				
					echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete division ?')){ return true; } else {return false; }";
					echo anchor('division/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Division','onclick'=>$js));?>
				</td>					
		</tr>
<?php	$i++;  } ?>	
</table>
<tr><div colspan="9" style="text-align: center;"><?php echo $pagination;?></div></tr>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>  
	</table>  
                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->
