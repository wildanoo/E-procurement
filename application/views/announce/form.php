 
 <?php echo form_open('announce/update_status',array('id'=>'form','class'=>'form-horizontal'));
 
 	echo form_hidden('id',$default->id);
 ?>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">

              <div class="box-header with-border">
                <h3 class="box-title">Review Content</h3>
              </div>

              <!--form class = "form-horizontal"-->
                <div class="box-body">

                  <div class = "col-md-6">
                    <div class="form-group">
                      <label for="title" class="col-md-3 vcenter control-label inputbox-size">Title</label>
                      <div class="col-md-9 row vcenter">
                      	<?php  echo $default->title; ?>
                      </div> 
                    </div>

                  <div class="form-group clear-pad-top">
                    <label for="category" class="col-sm-3 vcenter control-label inputbox-size">Content Type</label>                   
                    <div class="col-sm-6 control-label vcenter clear-pad-top row"> 
                      <?php echo $content_type[$default->content_type]; ?>
                    </div>
                  </div>

                  <div class="form-group clear-pad-top">
                    <label for="category" class="col-sm-3 vcenter control-label inputbox-size">Subcontent Type</label>                   
                    <div class="col-sm-6 control-label vcenter clear-pad-top row"> 
                      <?php echo $subcontent_type[$default->subcontent_type]; ?>
                    </div>
                  </div>

                  <div class="form-group clear-pad-top">
                    <label for="category" class="col-sm-3 vcenter control-label inputbox-size">Company Name</label>
                    <?php echo $default->company_name; ?>                   
                  </div>

                  <div class="form-group clear-pad-top">
                    <label for="category" class="col-sm-3 vcenter control-label inputbox-size">Office Region</label>
                    <div class="col-sm-6 control-label vcenter clear-pad-top row">
                    	<?php echo $default->office_name; ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="namedesc" class="col-md-3 vcenter control-label inputbox-size">Creator</label>
                    <div class="col-md-9 row vcenter">
                      <?php  echo ucwords($default->creator);  ?>
                    </div>
                  </div>

                  <div class = "form-group">
                    <label for="serviceclass" class="col-sm-3 vcenter control-label inputbox-size" >Content Indonesia</label>
                      <div class="col-lg-9 col-sm-6 col-12 vcenter row inputbox-size">                       
                        <?php
                        	if($content) echo anchor('announce/open_content_ind/'.$default->id,"Viewed",array('class'=>'default_link','title'=>'Viewed')); ?>
                      </div>
                  </div>

                  <div class = "form-group">
                    <label for="serviceclass" class="col-sm-3 vcenter control-label inputbox-size" >Content English</label>
                      <div class="col-lg-9 col-sm-6 col-12 vcenter row inputbox-size">                       
                        <?php
                          if($content) echo anchor('announce/open_content_eng/'.$default->id,"Viewed",array('class'=>'default_link','title'=>'Viewed')); ?>
                      </div>
                  </div>
                 
                  </div><!-- col-md-6 -->
                  <div class = "col-md-6">                    
                    <div class="form-group">
                      <label for="effectivedate" class="col-md-3 inputbox-size vcenter">Validity Period</label>
                      <div class="col-md-9 vcenter row">
                        <span class="vcenter">
                        	<?php echo date('M d, Y',strtotime($default->first_valdate)); ?>
                        </span>
                        <label class="vcenter">&nbsp;&nbsp;&nbsp; to &nbsp;&nbsp;&nbsp;</label>
                        <span class="vcenter">
                        	<?php echo date('M d, Y',strtotime($default->end_valdate)); ?>
                        <span>
                      </div>
                    </div><!-- form-group -->

                    <div class="form-group">
                      <label for="effectivedate" class="col-md-3 inputbox-size control-label vcenter">Publish Date</label>
                      <div class="col-md-9 row vcenter">
                          <?php   echo date('M d, Y',strtotime($default->publish_date)); ?>
                      </div>
                    </div><!-- form-group -->

                    <div class="form-group">
                      <label for="authority" class="col-md-3 vcenter control-label inputbox-size">Detail</label>
                      <div class="col-md-9 row vcenter">
                       <?php   echo $default->detail; ?>
                      </div>
                    </div>

                    <div class = "form-group">
                      <label for="serviceclass" class="col-sm-3 vcenter control-label inputbox-size">Attachment</label>
                        <div class="col-lg-9 col-sm-6 col-12 row vcenter">                         	
                         	<?php if($attfile!="-") echo anchor('announce/open_file/1/'.$default->id,$attfile,array('class'=>'default_link','title'=>'Viewed')); ?><br/>
                         	<?php if($rfpfile!="-") echo anchor('announce/open_file/2/'.$default->id,$rfpfile,array('class'=>'default_link','title'=>'Viewed')); ?>                                    </div>
                    </div><!-- form-group -->
                    
                    <div class = "form-group">
                      <label for="serviceclass" class="col-sm-3 control-label inputbox-size">Status</label>
                        <div class="col-lg-9 col-sm-6 col-12 row">                         
                        <?php echo form_dropdown('id_status', $status,$default->status,'class="form-control"'); ?>                                               
                        </div>
                    </div><!-- form-group -->
                  </div><!-- col-md-6 -->
                </div><!-- /.box-body -->                

                <div class="box-footer" style="padding-left: 30px;">
                   
                    <?php  $prop = array('class'=>'btn btn-default','title'=>'Cancel'); 
                           echo anchor('announce/','Cancel',$prop);  ?>	
                    <button type="submit" class="btn btn-info btn-primary">Submit</button>
                    <?php echo  form_close(); ?>
                </div><!-- /.box-footer -->
              <!--/form-->
            </div><!-- /.box -->
          </div>
        </div>
      </div>
    </div><!-- /row --> 