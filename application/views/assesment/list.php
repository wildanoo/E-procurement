<div class="row">
	<div class="col-md-12">
		<div class="widget widget-nopad">
			<div class="col-md-12">
				<div class="box box-info">

					<div class="box-header with-border">
						<h3 class="box-title">Manage Assesment</h3>
					</div><!-- /.box-header -->
					<br>
					<?php if($this->session->flashdata('msg_warning')!=null) { ?>
					<div class="alert alert-warning">
						<strong>Warning!</strong> <?php echo $this->session->flashdata('msg_warning'); ?>
					</div>
					<?php } ?>
					<div class="col-md-2 pull-left">
						<button type="button" onclick="form_create2()" id="btn_create" class="btn btn-primary btn-sm" style="width:130px;" title="Create New Assesment">Create New</button>
						<br>
						<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>
						<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
					</div>

					<div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
						<div class="input-group pull-right" style="width: 150px;">
							<div class="input-group-btn">
								<?php echo form_open_multipart('assesment/search',array('id'=>'form-search','novalidate' => 'true'));?>
								<input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
								<button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
								<a href="<?php echo base_url()?>assesment" class="btn btn-srch btn-warning btn-sm">Show All</a>
							</div>
						</div>
					</div>

					<div class="box-body">
						<table id="example2" class="table table-bordered table-hover sortable">
							<thead>
								<tr>
									<th class="sorttable_sorted_reverse">No</th>
									<th class="">Assesment</th>
									<th class="">Status</th>
									<th class="">Created Date</th>
									<th class="">Updated Date</th>
									<th class="">Updated By</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($browse){ $i=1;
									foreach($browse as $row){ ?>
									<tr>
										<td><?php echo $num = $num+1; ?></td>
										<td><?php echo $row->description; ?></td>
										<td><?php echo $row->status == '1' ? 'Active' : 'Not Active'; ?></td>
										<td align="center"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
										<td align="center"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>
										<td align="center"><?php echo $row->created_id; ?></td>
										<td><?php
											echo '<a onclick="form_update2('.$row->id.')" href="javascript:void(0)"><i class="fa fa-pencil"></i></a> | ';
											$trash  = '<i class="fa fa-trash"></i>';
											$js     = "if(confirm('Are you sure to delete assesment ?')){ return true; } else {return false; }";
											echo anchor('assesment/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Assesment','onclick'=>$js));?>
										</td>
									</tr>
									<?php	$i++;  } ?>
								</table>
								<div align="center"><?php echo $pagination;?></div>
								<?php } else { ?>
								<tr><td colspan="7">-</td></tr>
								<?php } ?>
							</tbody>
						</table>
					</div><!-- /.box-body -->

				</div><!-- /.box-info-->
			</div><!-- /col-md-12 -->
		</div><!-- /.widget -->
	</div><!-- /.col-md-12-->
</div><!-- /row -->
