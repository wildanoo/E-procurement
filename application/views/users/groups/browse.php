  <script type="text/javascript"> 
	$(function(){
		var baseUrl   = "<?php echo base_url(); ?>";	
		
		$("#srch-term").autocomplete({
			source: baseUrl + 'user/get_group_tags',
			minLength:1
		});		
		
 	});			
</script>
<script src="<?php echo base_url()?>assets/js/sorttable.js"></script>
 
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Groups</h3>
              </div><!-- /.box-header -->

                <br>
                    <?php if($this->session->flashdata('msg_warning')!=null) { ?>
                	<div class="alert alert-warning">
  						<strong>Warning!</strong> <?php echo $this->session->flashdata('msg_warning'); ?>
					</div>
					<?php } ?>                
                <div class="col-md-2 pull-left">
                    <!--a href="<?php echo base_url(); ?>auth/register"><button class="btn btn-sm btn-primary" id = "btn-pd">New User</button></a-->
                    <!-- WARNING!, DON'T CREATE NEW GROUP WITH MANUAL PROCESS,
                    GROUP AUTOMATICALLY CREATED WHEN YOU SET NEW USER IF NOT EXIST APPROPRIATE YOUR POSITION (ANGGUN W)-->
                    <?php  $prop = array('class'=>'btn btn-sm btn-primary','title'=>'New Group'); ?>					 
					<?php  //echo anchor('user/form_group','New Group',$prop); ?>                    
                </div>

                <div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('user/search2',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>user/groups" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		        </div>
		        </div>

                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover sortable">
                    <thead>
                      <tr>
                        <!--th><input type="checkbox" name="cba[]" id="cba" /></th-->
                        <th class="sorttable-sorted-reverse">No</th>
                        <th class="">Group Name</th>
                        <th class="">permissions</th>                        
                        <!--th>Created Date</th-->
                        <th>Action</th>
                      </tr>
                    </thead>

                    <tbody>
<?php
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td style="vertical-align: text-top;"><?php echo $num = $num+1;; ?></td>
				<td style="vertical-align: text-top;text-align: left;width: 20%"><a href="<?php echo base_url().'menu/browse/'.$row->id;?>" title=""><?php echo ucwords($row->name); ?></a></td>
				<td style="text-align: left;"><?php 
				
					if($row->permissions){
						$permit = explode(",",$row->permissions); $j=1;					
						foreach($permit as $val){ echo $j.". ".$val."<br/>";	$j++; } unset($j);
					} else { echo "<b><i>No Defined</i></b>";	}
				
				
				?></td>					
				<!--td><?php echo $row->created_date; ?></td-->		
				<td style="vertical-align: text-top;"><?php				
					$pencil = '<i class="fa fa-pencil"></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete user ?')){ return true; } else {return false; }";
					echo anchor('user/form_edit_perm/'.$row->id, $pencil,array('class'=>'default_link','title'=>'Edit Pemissions'));
					echo anchor('user/delete_group/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Group','onclick'=>$js));?>
				</td>
				
			</tr>
<?php	$i++;  } ?>	
</table>
<tr><div colspan="9" style="text-align: center;"><?php echo $pagination;?></div></tr>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>  
	</table>  
                  </table>
                </div><!-- /.box-body -->
              </form>
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->