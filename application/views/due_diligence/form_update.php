<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   
   $('#eff_date').datepicker({  
        changeMonth: true, changeYear: true,
		dateFormat: "yy/mm/dd" ,
     });      
     
   $('#exp_date').datepicker({  
        changeMonth: true, changeYear: true,
		dateFormat: "yy/mm/dd" ,
    });     
       
   $('#btn_cancel').click(function(){ 
   		var id_visit = $('#id_visit').val();
   		$("#container11").load(baseUrl+'due_diligence/visit_docs/' + id_visit);
    });      
	   
   });

</script>
<div id="debug10"></div>
<?php echo form_open_multipart('due_diligence/update_duedill',array('id'=>'form_doc')); ?>
<input type="hidden" name="id_visit" id="id_visit" value="<?php echo $default->id_visit; ?>" />
<input type="hidden" name="docID" id="docID" value="<?php echo $default->id; ?>"/>
<table width="50%">
<tr>
	<td><label>Document Name</label></td><td>:</td>
	<td><input type="text" name="doc_name" id="doc_name" value="<?php echo $default->doc_name; ?>" placeholder="Document Name" class="form-control" required/></td>
	<td id="msg1">&nbsp;</td>	
</tr>
<tr>
	<td><label>Remark</label></td><td>:</td>
	<td><input type="text" name="remark" id="remark" value="<?php echo $default->remark; ?>" placeholder="Remark" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Attachment</label></td><td>:</td>
	<td><?php	
			$style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";
			$data  = array('type'=>'file','name'=> 'userfile','id' => 'file_upload','value'=>'true','content'=>'Import','style'=>$style); 
			echo form_input($data); 
			
			if($default->filetype!="-"){
				$file =  $default->id.".".$default->filetype;
				echo $file;
			} else { echo "empty file"; $file = ""; }
			
			echo form_hidden("attcfile",$file);			
	?></td>
</tr>
<tr>
	<td colspan="3">
		<input type="button" id="btn_cancel" value="Cancel" class="btn btn-default btn-sm"/>
		<?php if($this->permit->md3) {  ?><input type="submit"  value="Update" class="btn btn-primary btn-sm"/><?php } ?>		
	</td>	
</tr>
</table><br/><br/>
<?php echo  form_close(); ?>


