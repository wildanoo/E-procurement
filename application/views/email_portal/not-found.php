<style type="text/css">
body{
	height: 100%;
}
.navbar,
.main-footer{
	height: 10%;
}
.main{
	height: 80%;
}
.subnavbar,
.nav-collapse .nav,
.nav-collapse .search-query{
	display: none;
}
.notfound-page{
	padding: 40px 0px;
}
</style>
<div class="notfound-page">
    <div class="page-header" style="vertical-align: middle">
	  <h3 style="display:inline-block">Ups, sorry we can't find the page you are looking for.</h3>
	</div>
	<div class="notes">
		<small><a href="<?php echo base_url()?>">Click Here</a> to redirect to home.</small>
	</div>
</div>