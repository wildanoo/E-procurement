<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Webcontent extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('security');
		
		if (!$this->session->userdata('lang')) {
			$this->session->set_userdata('lang','eng');
		}
	}

	function index()
	{	
		$this->load->view('content_public/page');
	}

	function page()
	{
		$param = $this->uri->segment(3);

		if ($this->session->userdata('lang') == 'eng') {
			$title  = "title_eng";
			$detail = "detail_eng";
		}elseif ($this->session->userdata('lang') == 'ind') {
			$title  = "title_ind";
			$detail = "detail_ind";
		}

		$joins[0][0] = 'subcontent_type';
		$joins[0][1] = 'subcontent_type.id = announce.subcontent_type';
		$joins[0][2] = 'left';

		$where 		 = "status = '3' AND announce.id = '".$param."'";
		// $where 		 = "status = '3' AND (CURDATE() BETWEEN DATE_FORMAT(publish_date, '%Y-%m-%d') AND DATE_FORMAT(end_valdate, '%Y-%m-%d')) AND announce.id = ".$param;
		$select 	 = "announce.id,file,id_cat,id_subcat,code,content_type,subcontent_type,subcontent_type.subtype_name,".$title." AS title,".$detail." AS detail,first_valdate,end_valdate,publish_date,subcontent_type.behavior_id,status,announce.created_date";
		$browse_temp = $this->crud->browse_join("","announce","","","false",$select,$joins,$where);

		foreach ($browse_temp as $browse) {}

		if ($browse_temp) {

			$curdatetime = date('Y-m-d H:i:s');
			$startdate = $browse->first_valdate;
			$endate = $browse->end_valdate;

		    if ($curdatetime<=$endate && $curdatetime>=$startdate) {
		    	$data['btn_join'] = true;
		    }else{
		    	$data['btn_join'] = false;
		    }

			$data['browse'] = $browse;		

			if($browse->file){
				if ($this->session->userdata('lang') == 'eng') {
					$ctpath  = "./uploads/contents/".$browse->file."_eng.txt";
				}elseif ($this->session->userdata('lang') == 'ind') {
					$ctpath  = "./uploads/contents/".$browse->file."_ind.txt";
				}

				$attpath = "./uploads/attach/".$browse->file.".pdf";
				
		 		$data['contents'] 	= file_exists($ctpath) ? file_get_contents($ctpath) : "-";
		 		$data['attachment'] = file_exists($attpath) ? true : false;
		 			
		 	}

		 	$data['category'] 	 = globalGetCategory();
			$data['subcategory'] = globalGetSubcategory();
			$data['subcontent']  = globalGetSubtype();
		 	$data['recent_post'] = globalRecentPost();
		 	$data['status']		 = $this->getStatusCondition();
		 	if ($browse->content_type) {
		 		$data['articles'] = $this->getPrevNextArticle($param,$browse->content_type);
		 	}else{
		 		$data['articles'] = array();
		 	}

		 	$data['view'] 		 = 'main/page';

			$this->load->view('layout/template_public',$data);	
		}else{
			echo "not-found-richo";
			// redirect('page/error');
		}
	}

	function download(){
		$param = $this->uri->segment(3);
		$file  = 'uploads/attach/'.$param;

		if (file_exists($file)) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		}
	}

	function login()
	{
		if ($this->tank_auth->is_logged_in()) {	// logged in
			redirect('home/');
		} else {
			$data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND $this->config->item('use_username', 'tank_auth'));
			$data['login_by_email']    = $this->config->item('login_by_email', 'tank_auth');

			if ($this->session->userdata('lang') == 'eng') {
				$title = "title_eng";
			}elseif ($this->session->userdata('lang') == 'ind') {
				$title = "title_ind";
			}

			$joins[0][0] 	= 'subcontent_type';
			$joins[0][1] 	= 'subcontent_type.id = announce.subcontent_type';
			$joins[0][2] 	= 'left';

			$order 		 	= array('field'=>'created_date','order'=>'DESC');
			$select 	 	= "announce.id,code,content_type,subcontent_type,subcontent_type.subtype_name,".$title." AS title,detail,first_valdate,end_valdate,publish_date,status,announce.created_date";

			$contents 	 	= $this->crud->browse("","content_type","","","true","id");

			$data['browse'] = array();

			foreach ($contents as $content) {
				$where 						= "status = '3' AND (CURDATE() BETWEEN DATE_FORMAT(publish_date, '%Y-%m-%d') AND DATE_FORMAT(end_valdate, '%Y-%m-%d')) AND subcontent_type.content_id = ".$content->id;
				$browse_temp[$content->id]  = $this->crud->browse_join_with_paging("","announce","","","true",$select,$joins,$where,$order,"5");
			}

			$data['browse']   = $browse_temp;
			
			$data['archived'] = $this->crud->browse("","announce","","","true");
				
			$data['type']     = array('1'=>'News','2'=>'Open Sourcing','3'=>'Open Bidding');
			
			$homepath         = file_get_contents("./uploads/cms/home.txt");
			$data['content']  = $homepath;
			
			$dbcategory  	  = $this->crud->browse("","category","","","true","id,category");
			$option      	  = array(''=>'-- Category --');
			foreach($dbcategory as $val){ $category[$val->id] = $val->category;	}
			$data['category'] = $option + $category;
			
			$this->load->view('auth/login', $data);
		}
	}

	function archive()
	{
		$indicator = $this->uri->segment(3);

		if ($indicator == 'news') {
			$extract 			 = $this->getNewsArchive(10);
			$browse 	 		 = $extract['getData'];
			$data['pagination']	 = $extract['pagination'];
			$data['num']		 = $extract['num'];

			$data['view'] 		 = 'main/news_archive';
		}elseif ($indicator == 'announcement') {
			$extract 			 = $this->getAnnouncementArchive(10);
			$browse 	 		 = $extract['getData'];
			$data['pagination']	 = $extract['pagination'];
			$data['num']		 = $extract['num'];
			$data['status']		 = $this->getStatusCondition();

			$data['view']		 = 'main/announcement_archive';

			$data['category'] 	 = globalGetCategory();
			$data['subcategory'] = globalGetSubcategory();
			$data['subcontent']  = globalGetSubtype();
			// $data['status']	  	 = globalGetSubtype();
		}

		$data['browse']		 = $browse;
		$data['recent_post'] = globalRecentPost();
		$data['title']		 = ucfirst($indicator);

		$this->load->view('layout/template_public',$data);
	}

	private function getNewsArchive($limit = NULL){

		/* check vendor currently login */

		$sess_vendor = $this->session->userdata('cek_vendor_auth');

		/* ===== */

		/* check title based on 'lang' session */

		if ($this->session->userdata('lang') == 'eng') {
			$title  = "title_eng";
			$detail = "detail_eng";
		}elseif ($this->session->userdata('lang') == 'ind') {
			$title  = "title_ind";
			$detail = "detail_ind";
		}

		/* ===== */

		/* get news data from database with pagination */

		$select 	 = "announce.id,code,content_type,subcontent_type,subcontent_type.subtype_name,".$title." AS title,".$detail." AS detail,first_valdate,end_valdate,publish_date,status,announce.created_date";
		$joins[0][0] = 'subcontent_type';
		$joins[0][1] = 'subcontent_type.id = announce.subcontent_type';
		$joins[0][2] = 'left';
		$order 		 = array('field'=>'announce.publish_date','order'=>'DESC');
		$page        = $this->uri->segment(4);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
			
		$where 		 = "status = '3' AND content_type = '1' AND subcontent_type != '2' AND (CURDATE() >= DATE_FORMAT(publish_date, '%Y-%m-%d'))";

		$count_rows  = $this->crud->browse_join('',"announce",'','','false',"COUNT(announce.id) AS COUNT",$joins,$where);

		$getData 	 = $this->crud->browse_join_with_paging('',"announce",'','','true',$select,$joins,$where,$order,$per_page, $offset);

		$total_rows  = count($count_rows)>0?$count_rows[0]->COUNT:0;

		$set_config  = array('base_url'=> base_url().'webcontent/archive/news/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>4);

		$config      = $this->crud->set_config($set_config);

		/** setup for pagination **/

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();

		/** ===== **/
		/* ===== */

		/* setup variable to used in another functions */

		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;

		/* ===== */

		return $data;
			
	}

	private function getAnnouncementArchive($limit = NULL){

		/* check vendor currently login */

		$sess_vendor = $this->session->userdata('cek_vendor_auth');

		/* ===== */

		/* check title based on 'lang' session */

		if ($this->session->userdata('lang') == 'eng') {
			$title 	 = "title_eng";
			$detail	 = "detail_eng";
		}elseif ($this->session->userdata('lang') == 'ind') {
			$title 	 = "title_ind";
			$detail	 = "detail_ind";
		}

		/* ===== */

		/* get news data from database with pagination */

		$select 	 = "announce.id,id_cat,id_subcat,code,content_type,subcontent_type,subcontent_type.subtype_name,".$title." AS title,".$detail." AS detail,first_valdate,end_valdate,publish_date,status,announce.created_date";
		$joins[0][0] = 'subcontent_type';
		$joins[0][1] = 'subcontent_type.id = announce.subcontent_type';
		$joins[0][2] = 'left';
		$order 		 = array('field'=>'announce.publish_date','order'=>'DESC');
		$page        = $this->uri->segment(4);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
			
		$where 		 = "status = '3' AND content_type = '2'";
		// $where 		 = "status = '3' AND content_type = '2' AND (NOW() BETWEEN DATE_FORMAT(publish_date, '%Y-%m-%d %H:%i:%s') AND DATE_FORMAT(end_valdate, '%Y-%m-%d %H:%i:%s'))";

		$count_rows  = $this->crud->browse_join('',"announce",'','','false',"COUNT(announce.id) AS COUNT",$joins,$where);

		$getData 	 = $this->crud->browse_join_with_paging('',"announce",'','','true',$select,$joins,$where,$order,$per_page, $offset);

		$total_rows  = count($count_rows)>0?$count_rows[0]->COUNT:0;

		$set_config  = array('base_url'=> base_url().'webcontent/archive/announcement/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>4);

		$config      = $this->crud->set_config($set_config);

		/** setup for pagination **/

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();

		/** ===== **/
		/* ===== */

		/* setup variable to used in another functions */

		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;

		/* ===== */

		return $data;
			
	}

	private function getStatusCondition(){
		$data = array();

		$data = array(
					0  => 'New',
					1  => 'Published',
					2  => 'Open for Joining',
					3  => 'Ongoing',
					4  => 'Closed',
					99 => 'Canceled'
				);

		return $data;
	}

	private function getPrevNextArticle($id,$flag){
		$data['string1']   = '';
		$data['string2']   = '';
      	$data['title1']    = '';
      	$data['title2']    = '';
      	$data['separator'] = '';

		if ($this->session->userdata('lang') == 'eng') {
			$title = "title_eng";
		}elseif ($this->session->userdata('lang') == 'ind') {
			$title = "title_ind";
		}

		$where 	 = "( 
				        id = IFNULL((SELECT min(id) FROM announce WHERE id > ".$id." AND content_type = ".$flag."),0) 
				        OR  id = IFNULL((SELECT max(id) FROM announce WHERE id < ".$id." AND content_type = ".$flag."),0)
			      	)";
		$select  = "id,".$title." AS title";
		$order 	 = array('field'=>'id','order'=>'ASC');

		$getData = $this->crud->browse('',"announce",'','','true',$select,$where,$order);

		if (count($getData) == 2) {
          $data['string1']   = $this->lang->line('page_detail_prev_article');
          $data['string2']   = $this->lang->line('page_detail_next_article');
          $data['title1']    = $getData[0]->title;
          $data['title2']    = $getData[1]->title;
          $data['separator'] = '<div class="next-prev-separator"></div>';
        }elseif (count($getData) == 1) {
          if ($getData[0]->id<$browse->id) {
            $data['string1']   = $this->lang->line('page_detail_prev_article');
            $data['string2']   = '';
            $data['title1']    = $getData[0]->title;
            $data['title2']    = '';
            $data['separator'] = '';
          }else{
            $data['string1']   = '';
            $data['string2']   = $this->lang->line('page_detail_next_article');
            $data['title1']    = '';
            $data['title2']    = $getData[0]->title;
            $data['separator'] = '';
          }
        }

		return $data;
	}

}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */