<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_vendordelete extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('crud');
	}

	function authenticate(){
		$allowed  	= array(125);
		$permit  	= $this->session->userdata('permissions');
		$permit 	= explode(",",$permit);
		foreach($permit as $str){
			$permissions[] = preg_replace("/[^0-9]/","",$str);
		}
		$result 	= array_intersect($allowed, $permissions);
		if($result){ return true;} return false;
	}

	function sort_stat(){
		$stat = array(
			'request' => 'Request',
			'approved' => 'Approved',
			'start' => 'Start',
			'completed' => 'Completed'
			);

		return $stat;
	}

	function search_cond($A,$B,$C,$D){

		if($A !='all' && empty($B) && empty($C) && empty($D)){
			$where = "status = '$A'";
		}elseif($A !='all' && !empty($B) && empty($C) && empty($D)){
			$where = "status = '$A' AND (vendor_name LIKE '%$B%' OR vendor_num LIKE '%$B%')";
		}elseif($A !='all' && !empty($B) && !empty($C) && !empty($D)){
			$where = "status = '$A' AND (vendor_name LIKE '%$B%' OR vendor_num LIKE '%$B%') AND (last_updated between '$C' AND '$D')";
		}elseif(!empty($B) && empty($C) && empty($D) && $A =='all'){
			$where = "(vendor_name LIKE '%$B%' OR vendor_num LIKE '%$B%')";
		}elseif(!empty($B) && !empty($C) && !empty($D) && $A =='all'){
			$where = "last_updated between '$C' AND '$D' AND (vendor_name LIKE '%$B%' OR vendor_num LIKE '%$B%')";
		}elseif(!empty($C) && !empty($D) && $A =='all' && empty($B)){
			$where = "last_updated between '$C' AND '$D'";
		}elseif(!empty($C) && !empty($D) && $A !='all' && empty($B)){
			$where = "last_updated between '$C' AND '$D' AND status = '$A'";
		}else{}

		return $where;
	}

	function list_db(){
		$table 		= array(
			'affiliates',
			'references',
			'correspondence',
			'organization',
			'owner',
			'akta',
			'bank_account',
			'contact_person',
			'vendor_category',
			'announce_join_participant',
			'announce_newsblast_participant',
			'dd_conclutions',
			'dd_docs',
			'dd_notes',
			'dd_participant',
			'dd_summary',
			'dd_vdr_assesment',
			'direct_bidding',
			'due_dilligence',
			'session_register',
			'session_verification',
			'subcat_notes',
			'subcat_resumes',
			'temp_affiliates',
			'temp_contact_person',
			'temp_correspondence',
			'temp_organization',
			'temp_owner',
			'temp_references',
			'temp_vendor_category',
			'vendor_account',
			'vendor_approval',
			'vendor_blacklist',
			'vendor_changedata',
			'vendor_inactive',
			'vendor_logs',
			'vendor_recomend',
			'vendor_redlist',
			'vendor_register',
			'vendor_rejected',
			'vendor_visit',
			'vendor_visit_status'
			);

		return $table;
	}

}

/* End of file m_vendordelete.php */
/* Location: ./application/models/m_vendordelete.php */