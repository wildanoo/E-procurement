 <div id="container_profiles">
 
 <script type="text/javascript"> 
	$(function() {
		var baseUrl = "<?php echo base_url(); ?>";	
        
        $("#update_profiles").hide();
        
        $("#change_profiles").click(function(){
        	
        	$("#name").focus();
        	$("#name").css("background","#ffffff");
			$("#name").css("color","#827e85"); 				  			   
			$("#name").prop('readonly',false);

        	$("#nopeg").css("background","#ffffff");
			$("#nopeg").css("color","#827e85"); 				  			   
			$("#nopeg").prop('readonly',false);	

	        $("#id_office").attr("disabled", false);
			
	        $("#id_dept").attr("disabled", false);
			
			$("#phone").css("background","#ffffff");
			$("#phone").css("color","#827e85"); 				  			   
			$("#phone").prop('readonly',false);
			
			$("#mobile").css("background","#ffffff");
			$("#mobile").css("color","#827e85"); 				  			   
			$("#mobile").prop('readonly',false);
			
			$("#fax").css("background","#ffffff");
			$("#fax").css("color","#827e85"); 				  			   
			$("#fax").prop('readonly',false);
	        
	        $('#update_profiles').show();
	        $('#change_profiles').hide();    
	      
        });	

       	$('#update_profiles').click(function(){	  	
    		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
       	    var values = $("#form_profiles").serializeArray();
    		values.push({name: csrf,value: token});
    		values = jQuery.param(values); 
    			$.ajax({
                   url  : baseUrl + 'profiles/profiles_update',
                   type : "POST",              		               
                   data : values,
                   success: function(resp){
                   	  //$('#debug3').html(resp);
                   	  $("#container_profiles").load(baseUrl+'profiles/load_profiles');
                   }
                });
    			
       		$('#update_profiles').hide();
       		$('#change_profiles').show();
       	});
		$("#cancel").click(function(){
			document.location.href= baseUrl + 'home/';
		});

     });
  	        
</script>                
            <div class="row">
              <div class = "col-md-12">
                <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">Account Information</h3>
                  </div><!-- /.box-header -->

                    <!-- form start -->
                    <!--form class="form-horizontal"-->
                    <div id="debug"></div>
					<?php echo form_open('#',array('id'=>'form_profiles','class'=>'form-horizontal'));  ?> 
					<?php echo form_hidden('id',$profiles->id); ?>
					<table width="100%">
                      
<tr>
<td><label class="col-md-3 control-label">Name </label></td><td>:</td>
<td><?php $name = array( 'name' => "name" ,
								'id'   => "name", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> ucfirst($profiles->name)); 
	 echo form_input($name);	?>
</td>
</tr>

<tr>
<td><label for="nopeg" class="col-md-3 control-label">Nomor Pegawai </label></td><td>:</td>
<td><?php $nopeg = array( 'name' => "nopeg" ,
								'id'   => "nopeg", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> $profiles->nopeg); 
	 echo form_input($nopeg);	?>
</td>
</tr>
<tr>
<td><label for="phone" class="col-md-3 control-label">Phone </label></td><td>:</td>
<td><?php $phone = array( 'name' => "phone" ,
								'id'   => "phone", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> $profiles->phone); 
	 echo form_input($phone);	?>
</td>
</tr>
<tr>
<td><label for="mobile" class="col-md-3 control-label">Mobile </label></td><td>:</td>
<td><?php $mobile = array( 'name' => "mobile" ,
								'id'   => "mobile", 
								'class'=> "form-control", 
								'readonly'=> "true", 
								'style'=> "width: 250px; height: 27px; background-color: #e9e9e9 !important;", 
								'value'=> $profiles->mobile); 
	 echo form_input($mobile);	?>
</td>
</tr>
<tr>
<td><label for="id_office"  class="col-md-3 control-label">Office </label></td><td>:</td>
<td><?php $prop = 'id="id_office" class="form-control" disabled="disabled"';
          echo form_dropdown("id_office",$offices,$profiles->id_office,$prop); ?>
</td></tr>
<tr>
<td><label for="id_dept"  class="col-md-3 control-label">Department </label></td><td>:</td>
<td><?php $prop = 'id="id_dept" class="form-control" disabled="disabled"';
          echo form_dropdown("id_dept",$department,$profiles->id_dept,$prop); ?>
</td></tr>

</table>
<div class="box-footer">
<br>
<button type="button" class="btn btn-default" id="cancel">Cancel</button>
<button type="button" id="change_profiles" class="btn btn-info btn-primary">Change</button> 
<button type="button" id="update_profiles" class="btn btn-info btn-primary">Update</button>
</div>
<?php echo form_close(); ?>
          

</div>       
</div>
</div>
</div>       