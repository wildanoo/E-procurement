<div class="module form-module">
	<div id="toggle-close1" class="toggle-close">
		<i class="">Close [x]</i>
	</div>
	<div class="">
			<form class="step-progress" action="<?php echo base_url().$this->uri->uri_string();?>" method="post" accept-charset="utf-8">
				<div style="display:none">
				<input type="hidden" name="csrf_test_name" value="89c66c29ba9ff5d3fec7e5cb6fd5cd2c">
			</div>
			<?php
			$login = array(	'type'  		=> 'email',
							'class'  		=> 'form-control-lg',
							'placeholder'  	=> 'Username',
							'name'			=> 'login',
							'id'			=> 'login',
							'value' 		=> set_value('login'),
							'maxlength'		=> 80,
							'size'			=> 30,);
			if ($login_by_username AND $login_by_email) {
				$login_label = 'Email or login';
			} else if ($login_by_username) {
				$login_label = 'Login';
			} else {
				$login_label = 'Email';
			}
			$password = array(	'type'  		=> 'password',
								'class'  		=> 'form-control-lg',
								'placeholder'  	=> 'Password',
								'name'			=> 'password',
								'id'			=> 'password',
								'size'			=> 30,);
			$remember = array(
				'type'	=> 'checkbox',
				'name'	=> 'remember',
				'id'	=> 'remember',
				'value'	=> 1,
				'checked'	=> set_value('remember'),
				'class' => 'icheckbox_square-blue',
			);
			$captcha = array(
				'name'	=> 'captcha',
				'id'	=> 'captcha',
				'maxlength'	=> 8,
			);
		?>
							  
		<?php echo form_open($this->uri->uri_string(),array('id'=>'form_login')); ?>
			<div class="form">
		    	<h2>LOGIN TO YOUR ACCOUNT</h2>    	  					      					    				      
		    	<?php echo form_input($login); ?>
		    	<?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
		    	<?php echo form_password($password); ?>
		    	<?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
		    	<div id="message"></div>		      
		    	<input type="button" value="Login" id="create" class="btn-primary">
		      	<ul class="col-md-12">
		      		<li class="span checkbox pull-left" style="margin-left: 18px;">
							<input type="checkbox" name="remember" value="1" id="remember" class="icheckbox_square-blue">
							<p style="font-size:12px;float:left;margin-top: 6px;margin-left: 3px;">Remember Me</p>
					</li>
					<li class="pull-right" style="margin-top: 18px;">
						<div class="cta">
							<a href="http://localhost/eprocga_backup/auth/forgot_password">Forgot Password?</a>
						</div>
					</li>
		      	</ul>
		 	</div>
	 	</form>
	 <?php echo form_close(); ?>
	</div>
</div>