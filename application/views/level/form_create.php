<div class="page-header">
  <h3>New Content Level</h3>
</div>
<?php echo form_open('level/create',array('id'=>'form')); ?>
<table cellpadding="2" cellspacing="2">
<tr>
	<td><label>Group</label></td><td>:</td>
	<td><?php 
		$prop_cat = 'class="form-control" id="id_group" style="width:220px"';
		echo form_dropdown('id_group',$group,'0',$prop_cat); ?>			
	</td>	
</tr>
<tr>
	<td><label>Subcontent</label></td><td>:</td>
	<td><?php 
		 echo form_hidden("ttl",count($sub));
         $i = 1;
         foreach($sub as $row){
		 $value = $row->id;
		 echo form_checkbox('check'.$i,$value, false); echo $row->subtype_name."<br/>";
		 $i++; }  
		 ?>	
	</td>
</tr>
<tr>	
	<td colspan="3" style="text-align: right;">
		<input type="submit" value="Submit" class="btn btn-primary btn-sm"/>
		<?php  $prop = array('class'=>'btn btn-primary btn-sm','title'=>'Cancel'); ?>					 
		<?php  echo anchor('level/','Cancel',$prop); ?>
	</td>
</tr>
</table>