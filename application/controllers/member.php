<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_register');	
    }
    
 function index(){
 	if ($this->tank_auth->is_logged_in()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
				
		$memID      = $this->session->userdata('memID');
		$field      = $memID ? "id" : "";
		$id         = $memID ? $memID  : "";		
		
 		$table 	    = "member";		
		$page       = $this->uri->segment(3);
		$per_page   = 5;		
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table);
		$set_config = array('base_url'=> base_url().'member/index','total_rows'=>$total_rows,'per_page'=>5,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);		
			 
		$this->load->library('pagination');			 
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();			
			
		$order 			     = array('field'=>'created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset; 
		$select 			 = "";		
		
			$select  = "id,fullname,
						  (SELECT office_name FROM offices WHERE id=id_office) as office, 
						  (SELECT id_region FROM offices WHERE id=id_office) as id_region, 
						  (SELECT region_name FROM region WHERE id=id_region) as region,
						  (SELECT dept_name FROM departemen WHERE id=id_dept) as dept_name,
						  (SELECT level FROM level WHERE id=id_level) as level,
						   created_id,created_date,last_updated ";
				
		if($memID)	$browse = $this->crud->browse("",$table,"id",$memID,"true");
		else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id,"true",$select,"",$order,$config['per_page'],$offset);
		$data['browse']      = $browse; 		
 		
 		$data['view']	  = "members/browse"; 		
 		$this->load->view('template',$data);
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	} 	
 } 
 
 function get_office(){
		$id_country = $_POST['id_country']; //echo "=>".$id_comp;
	 	$select  = " id,office_name";				
	 	$region  = $this->crud->browse("","offices","id_country",$id_country,"true",$select); 	
		echo "<option>-- Select Office --</option>";
		foreach ($region as $row){
			echo "<option value='$row->id'>$row->office_name</option>";	
		}		
	}	
 
 function form_create(){
 	
 	$country = $this->crud->browse("","country","","","true","id,country_name");
 	$dept    = $this->crud->browse("","departemen","","","true","id,dept_name"); 
 	$level   = $this->crud->browse("","level","","","true","id,level"); 	
 	
	if(!$country) $country = array(); 	
	$select = array(''=>'-- Select --');
	foreach($country as $val){ $options[$val->id] = $val->country_name; }
	foreach($dept as $val){ $options2[$val->id] = $val->dept_name; } 
	foreach($level as $val){ $options3[$val->id] = $val->level; } 	
	
	$data['dept'] = $select + $options2; 
	$data['level'] = $select + $options3; 
	
	$data['country'] = $select + $options; 
 	$this->load->view('members/form_create',$data);	
 	
 }

 function validate(){
 	
 	$firstname 	= $_POST['firstname']; 
 	$lastname 	= $_POST['lastname'];
 	$id_country	= $_POST['id_country'];
 	$id_office	= $_POST['id_office'];
 	$id_dept	= $_POST['id_dept']; 
 	$id_level	= $_POST['id_level']; 
 	$email		= $_POST['email']; 
 	$username	= $_POST['username'];
 	$password	= $_POST['password'];  
 	$confirm	= $_POST['confirm_password']; 	
 	$pattern    = '/^[A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z]{2,4}$/'; 	
 	
 	$val['firstname'] 	= !$firstname 	? "false" : "true"; 
 	$val['lastname']  	= !$lastname	? "false" : "true"; 
 	$val['id_country'] 	= !$id_country	? "false" : "true"; 
 	$val['id_office'] 	= !$id_office	? "false" : "true"; 
 	$val['id_dept'] 	= !$id_dept		? "false" : "true"; 
 	$val['id_level'] 	= !$id_level	? "false" : "true"; 
 	$val['username'] 	= !$username	? "false" : "true"; 
 	$val['password'] 	= !$password	? "false" : "true"; 
 	$val['confirm'] 	= !(!$confirm || ($password==$confirm))			? "false" : "true";
 	$val['email'] 		= preg_match($pattern , $email)	? "true" : "false"; 
 	
 	
 	$msg['msg1'] 	= !$firstname 	? "*firstname required" : ""; 
 	$msg['msg2']  	= !$lastname	? "*lastname required" : ""; 
 	$msg['msg3'] 	= !$id_country	? "*country required" : ""; 
 	$msg['msg4'] 	= !$id_office	? "*office required" : ""; 
 	$msg['msg5'] 	= !$id_dept		? "*departemen required" : ""; 
 	$msg['msg6'] 	= !$id_level	? "*level required" : "";
 	$msg['msg7'] 	= preg_match($pattern , $email)	? "" : "wrong email";  
 	$msg['msg8'] 	= !$username	? "*username required" : ""; 
 	$msg['msg9'] 	= !$password	? "*password required" : ""; 
 	$msg['msg10'] 	= (!$confirm || ($password!=$confirm))	? "*confirm password required or invalid" : "";
 	
 	$msg['status']  = in_array("false",$val) ? "false" : "true"; 	
 	

 	echo json_encode($msg);
 	
 }
 
 function create(){
 	
 	$firstname 	= $_POST['firstname']; 
 	$lastname 	= $_POST['lastname']; 	
 	$id_office	= $_POST['id_office'];
 	$id_dept	= $_POST['id_dept']; 
 	$id_level	= $_POST['id_level']; 
 	$email		= $_POST['email']; 
 	$username	= $_POST['username'];
 	$password	= $_POST['password'];  
 	
 	
 	$this->tank_auth->create_user($username,$email,$password,true);
 	$member = $this->users->get_user_by_email($email);
 	
 	$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
	$footer		= array('created_id'=>$userID,'created_date'=>$curr_date,'last_updated'=>$curr_date);	
 	
 	$post = array(	'userID'		=> $member->id,
 					'firstname'		=> $firstname,
					'lastname'		=> $lastname,	
					'fullname'		=> $firstname." ".$lastname,					
					'id_office'		=> $id_office,
					'id_dept'		=> $id_dept,
					'id_level'		=> $id_level);
					
	$data = $post + $footer;  	
 	$vdrID = $this->crud->insert("","member",$data); 	
	
 	$this->session->set_flashdata('message','successfully create new member');  		
 	redirect('member/');		 
 	
 }

 function update_validate(){
 	
 	$firstname 	= $_POST['firstname']; 
 	$lastname 	= $_POST['lastname'];
 	$id_country	= $_POST['id_country'];
 	$id_office	= $_POST['id_office'];
 	$id_dept	= $_POST['id_dept']; 
 	$id_level	= $_POST['id_level']; 
 		
 	
 	$val['firstname'] 	= !$firstname 	? "false" : "true"; 
 	$val['lastname']  	= !$lastname	? "false" : "true"; 
 	$val['id_country'] 	= !$id_country	? "false" : "true"; 
 	$val['id_office'] 	= !$id_office	? "false" : "true"; 
 	$val['id_dept'] 	= !$id_dept		? "false" : "true"; 
 	$val['id_level'] 	= !$id_level	? "false" : "true"; 
 	
 	
 	$msg['msg1'] 	= !$firstname 	? "*firstname required" : ""; 
 	$msg['msg2']  	= !$lastname	? "*lastname required" : ""; 
 	$msg['msg3'] 	= !$id_country	? "*country required" : ""; 
 	$msg['msg4'] 	= !$id_office	? "*office required" : ""; 
 	$msg['msg5'] 	= !$id_dept		? "*departemen required" : ""; 
 	$msg['msg6'] 	= !$id_level	? "*level required" : "";
 	
 	
 	$msg['status']  = in_array("false",$val) ? "false" : "true"; 	
 	

 	echo json_encode($msg);
 	
 }
 
 function form_update(){
 	
 	$id  = $this->uri->segment(3);
 	$def =  $this->crud->browse("","member","id",$id,"false");
 	$data['def'] = $def;
 	
 	$country = $this->crud->browse("","country","","","true","id,country_name");
 	$dept    = $this->crud->browse("","departemen","","","true","id,dept_name"); 
 	$level   = $this->crud->browse("","level","","","true","id,level"); 
 	$office  = $this->crud->browse("","offices","id",$def->id_office,"false","id,office_name"); 	
 	
	if(!$country) $country = array(); 	
	$select = array(''=>'-- Select --');
	foreach($country as $val){ $options[$val->id] = $val->country_name; }
	foreach($dept as $val){ $options2[$val->id] = $val->dept_name; } 
	foreach($level as $val){ $options3[$val->id] = $val->level; } 	
	
	$data['dept']  = $select + $options2; 
	$data['level'] = $select + $options3; 
	$data['office'] = array($office->id => $office->office_name); 
	
	$data['country'] = $select + $options; 
 	$this->load->view('members/form_update',$data);	
 	
 	
 }

 function update(){
 	
 	$memID	 	= $_POST['memID']; 
 	$firstname 	= $_POST['firstname']; 
 	$lastname 	= $_POST['lastname']; 	
 	$id_office	= $_POST['id_office'];
 	$id_dept	= $_POST['id_dept']; 
 	$id_level	= $_POST['id_level'];  	
 		
 	$update = array('firstname'		=> $firstname,
					'lastname'		=> $lastname,	
					'fullname'		=> $firstname." ".$lastname,					
					'id_office'		=> $id_office,
					'id_dept'		=> $id_dept,
					'id_level'		=> $id_level,
					'last_updated'  => $curr_date);		
 	
 	$this->crud->update("","member","id",$memID,$update); 	
 	$this->session->set_flashdata('message','successfully update new member');  		
 	redirect('member/');		 
 	
 }
 
 function delete(){
 	
 	$id   = $this->uri->segment(3);
 	$data = $this->crud->browse("","member ","id",$id,"false","userID as uID");
 	 
 	$this->crud->delete("","member","id",$id);
 	$this->crud->delete("","users","id",$data->uID); 	
 	
 	$this->session->set_flashdata('message','1 data success deleted');
 	redirect('member/','refresh'); 	
 	
 }

}

