<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<title>E-Procurement Garuda</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/skins/red-blue/blue.css" id="colors" />
<!-- 	<link rel="stylesheet" type="text/css" href="css/component.css" /> -->
	<link rel="icon" href="favicon-grd.png" sizes="16x16">
</head>
<body>
<div id="wrapper">
	<header id="header" class="container-fluid">
	  <div class="header-bg">
		<div class="container">	  
			<div class="row">
				<div id="logo" class="col-md-5 col-md-offset-1"><a href="login" class="logo" alt="E-Procurement Garuda Indonesia"></a></div>
				<div class="col-md-4 col-md-offset-1">
					<ul class="social-icons">
					<?php if ($this->session->userdata('lang') == 'eng') { ?>
						<li class="lang eng" data="lang-eng" title="English"><a style="top:0 !important;" href="#">English</a></li>
						<li class="lang idn" data="lang-ind" title="Indonesian"><a class="inactive" href="#" id="lang_ind">Indonesian</a></li>
					<?php }elseif ($this->session->userdata('lang') == 'ind') { ?>
						<li class="lang eng" data="lang-eng" title="English"><a class="inactive" href="#" id="lang_eng">English</a></li>
						<li class="lang idn" data="lang-ind" title="Indonesian"><a style="top:0 !important;" href="#">Indonesian</a></li>
					<?php }?>
					</ul>	
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="select-menu hidden-lg">
						<select id="selectMenu">
							<option selected value="#">Home</option>			
							<option value="<?php echo base_url(); ?>main/news">News</option>						
							<option value="#">FAQs</option>
							<option value="#">Supplier Registration</option>
							<option value="login.html">Log In</option>
							<option value="<?php echo base_url(); ?>main/contact_us">Contact us</option>
						</select>
					</div>
					<ul id="menu" class="visible-lg" style="font-size: 13px;">
						<li>
							<a href="#">HOME</a>
						</li>
						<li><?php  echo anchor('main/news','News'); ?></li>
						<li><a href="#">FAQs</a></li>
						<li id="st-trigger-effects2">
							<a data-effect="st-effect-2">SUPPLIER REGISTRATION</a>
						</li>
						<li id="st-trigger-effects1">
							<a data-effect="st-effect-1">LOG IN</a>
						</li>
						<li><?php  echo anchor('main/contact_us','CONTACT US'); ?></li>
						<!-- Search Form -->
						<li class="search-form visible-desktop" style="float: right;top: 10px;"\>
							<form method="get" action="#">
								<input type="text" value="Search" class="search-text-box"/>
								<input type="submit" value="" class="search-text-submit"/>
							</form>
						</li>
					</ul>
				</div>
			</div>
		</div>
	  </div>
	</header>
	<section id="content" class="container-fluid">
	<div class="container" style="width: 940px;margin: 0 auto;padding:0;">
		<div class="row">
			<div class="col-md-8">
				<div class="post">
					<div class="title">
						<h2 style="display:inline-block;vertical-align:baseline;"><?php echo $browse->title; ?></h2>
						<div style="display:inline-block;float:right;padding:25px 0 0 0;" id="crumbs"><a href="index.html">Home</a> / <a href="#" class="active">News</a></div> 
					</div>
					<div class="blog-line">
						<a href="#" class="date item"><?php 
						$date = str_replace('/', '-', $browse->publish_date);
						echo date('M d / Y', strtotime($date));
						echo '';?></a>
						<a href="#" class="category item">Category: <?php echo $browse->subtype_name; ?></a>
					</div>
					<?php if ($browse->behavior_id != 1) { ?>
					<div class="header-announcement">
						<div class="row">
							<div class="col-md-6">
								<h4><?php echo $browse->code;?></h4>
								<label>Category: </label>&nbsp;<span><?php echo $category[$browse->id_cat];?></span><br>
								<label>Start Date: </label>&nbsp;<span><?php echo date('d/m/Y h:i', strtotime($browse->first_valdate));?></span><br>
								<label>Status: </label><span></span><br>
							</div>
							<div class="col-md-6">
								<h4>&nbsp;</h4>
								<label>Publish Date: </label>&nbsp;<span><?php echo date('d/m/Y h:i', strtotime($browse->publish_date));?></span><br>
								<label>Sub Category: </label>&nbsp;<span><?php echo $subcategory[$browse->id_subcat];?></span><br>
								<label>End Date: </label>&nbsp;<span><?php echo date('d/m/Y h:i', strtotime($browse->end_valdate));?></span><br>
							</div>
						</div>
					</div>
					<br>
					<?php } ?>
					<div class="thumbnail">
						<div id="myCarousel" class="carousel slide">
							<div class="carousel-inner">
								<div class="item active"> 
									<img alt="" src="<?php echo base_url(); ?>uploads/images/<?php echo $browse->file;?>.jpg" >
								</div>
							</div>					
						</div>
					</div>
					<p style="text-align:justify;"><?php echo isset($contents)? $contents:'' ; ?></p>
					<br>
					<br>
					<?php if ($attachment) { ?>
					<section class="row attachment-section">
						<div class="col-md-2"><strong>Attachment: </strong></div>
						<div class="col-md-10 row">
							<span><a href="<?php echo base_url().'webcontent/download/'.$browse->file.'.pdf' ?>"><?php echo $browse->file.'.pdf'; ?></a></span>
						</div>
					</section>
					<?php }?>
					<?php if ($browse->content_type != '1' && $btn_join) { ?>
					<hr/>
					<a href="<?php echo base_url().'webcontent/registration/'.$this->uri->segment(3) ?>" id="btn_approve" class="col-md-offset-10 btn btn-primary btn-sm" style="width:80px;">Join</a>
					<hr/>
					<?php } ?>
				</div>			
			</div>
			<?php $this->load->view('content_public/recent_post',$recent_post); ?>
		</div>
	</div>
	</section>
</div>
<footer class="main-footer footer-line container-fluid text-center">
	Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
</footer>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.color.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/SmoothScroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>
</html>