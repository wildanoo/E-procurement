<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";		 
	  $(function(){  });

		$('#country').change(function(){ 
			var country = $('#country').val();
			$.ajax({
               type : "POST",
               url  : baseUrl + 'country/set_sess_country',			   
               data : 'field=id&country_name='+country,
               success: function(data){ 
                   //alert(category);	
					 //$('#debug').html(resp);
					 //dialogRef.close();				 	
				 	document.location.href= baseUrl + 'country/';
			   }		   
            });	
		
		});      
     
	  $("#srch-term").autocomplete({
			source: baseUrl + 'country/get_country_tags',
			minLength:1
	  });
	   
	  $("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'country/set_sess_search',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){               		            
				   document.location.href= baseUrl + 'country/';
               }
            });
        });  
	 
	  $('#btn_create').click(function(){  form_create();  });	
});

	

	function form_create(){
			var baseUrl = "<?php echo base_url(); ?>";		 
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
			BootstrapDialog.show({
			title: 'Create Country',
            message: $('<div></div>').load(baseUrl + "country/form_create/"),
            buttons: [{ label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){
		                	 var country_name = $("#country_name").val(); 

		                	 $.post( baseUrl + 'country/is_exist', { csrf: token , country_name : country_name })
							 .done(function( resp ) {

							var json = $.parseJSON(resp);
							if (json.status=='true'){
			                	var values = $("#form").serializeArray();
			                	values.push({ name: csrf,value: token });
								values = jQuery.param(values);	
								 $.ajax({
						               url  : baseUrl + 'country/create',
						               type : "POST",              		               
						               data : values,
						               success: function(resp){
						               		document.location.href= baseUrl + 'country/';	               				               		  
								       }
						            });   
							} else { 	
								 $("#msg1").html(json.msg);
							}

							});   
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'country/';
				                    dialogRef.close(); 
				                }
			        }]
        });
	}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Country',
            message: $('<div></div>').load(baseUrl + "country/form_update/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){
		                	var country_name = $("#country_name").val();    
		                	//var id = $("#remark").val();
							//alert(remark);
							
		                	 $.post( baseUrl + 'country/is_exist', { csrf: token , country_name : country_name })
							 .done(function( resp ) {

							var json = $.parseJSON(resp);
							if (json.status=='true'){
			                	var values = $("#form").serializeArray();
			                	values.push({ name: csrf,value: token });
								values = jQuery.param(values);	
								 $.ajax({
						               url  : baseUrl + 'country/update',
						               type : "POST",              		               
						               data : values,
						               success: function(resp){

						            	   //$("#debug").html(resp);
						            	   //dialogRef.close()
						               	   document.location.href= baseUrl + 'country/';	               				               		  
								       }
						            });   
							} else { 	
								 $("#msg1").html(json.msg);
							}

							});   
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'country/';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>
<script src="<?php echo base_url(); ?>assets/js/sorttable.js"></script>
</head>
<body>
<div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Country</h3>
              </div><!-- /.box-header -->
                <br>
                    <?php if($this->session->flashdata('msg_warning')!=null) { ?>
                	<div class="alert alert-warning">
  						<strong>Warning!</strong> <?php echo $this->session->flashdata('msg_warning'); ?>
					</div>
					<?php } ?>
                <div class="col-md-2 pull-left">
                    <input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Country" value="New Country" style="width:100px;" title="Create New Country"/>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
				<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
                </div>

                  <div class = "col-md-10 pull-right">
                  <div class="input-group pull-right" style="width: 200px;">
                      <!-- input type="text" class="form-control input-sm pull-right" name="srch-term" id="srch-term" 
                      	placeholder="Search" style="height: 28px;">
                      <div class="input-group-btn">
                        <button id="btn_search" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div> -->
                        <?php
						$sess_cat = $this->session->userdata('sess_cat');
						$id_cat   = $sess_cat == '' ? '' : $sess_cat;	            
						$prop_cat = 'id=country  style="width: 120px; height: 25px; font-size: 13px"'; ?>
						<?php echo form_dropdown('country', $country, $id_cat,$prop_cat); ?>
                    </div>
                    </div>
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover text-center sortable">
	<thead>
		<tr>
            <th class="sorttable_sorted_reverse">No</th>
            <th class="">Country</th>
            <!-- th>Remark</th> -->
            <th class="">Created Date</th>
            <th class="">Updated Date</th>
            <th class="">Updated By</th>
            <th>Action</th>
        </tr>
     </thead>
     <tbody>
 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td style="text-align:center;"><?php echo $num = $num+1; ?></td>
			<td><?php echo $row->country_name; ?></td>	
			<!-- td><?php //echo $row->remark; ?></td> -->
 			<td align="center"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>		
 			<td align="center"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>	
 			<td align="center"><?php echo $row->created_id; ?></td>				
				<td><?php				
					echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete country ?')){ return true; } else {return false; }";
					echo anchor('country/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Country','onclick'=>$js));?>
				</td>						
		</tr>
<?php	$i++;  } ?>	
</table>
<div align="center"><?php echo $pagination;?></div>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>
     </tbody>
                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->
