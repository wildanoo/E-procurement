<?php 
class numbering extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('crud');
	}
	
 function general($table,$chr,$where="",$group_by=""){	
	
	$Mdate    = date("m-d"); 
	$year 	  = date('Y'); 
	$yearCode = substr($year,2,4);
 	$dateNow  = $yearCode.$Mdate;
 	$dateNow  = str_replace("-","", $dateNow); 	
 	$counter  = $this->total_record("",$table,$where="",$group_by=""); 
 	$counter  = $counter==0 ? 1 : $counter+1; 	
 	
 	if(strlen($counter)==1){ $pre='00';	
 	} elseif(strlen($counter)==2){ $pre='0'; } else { $pre=''; }
 		
 	$seq     = $pre.$counter;
 	$gen_num = $dateNow.'.'.$chr.$seq;
 	
 	return $gen_num;
 	
 } 
 
 function total_record($db,$table,$where="",$group_by=""){ 
	if(!$db)$db="db";
	$this->$db->db_select();
	if($where) 	  $this->$db->where($where); 
	if($group_by) $this->$db->group_by($group_by); 
	
	return $this->$db->get($table)->num_rows();
 }
 
 function register_vendor($id_subcat){
 	
 	$counter  = $this->total_record("","vendor_register");
 	$counter  = $counter==0 ? 1 : $counter+1; 	
 	
 	if(strlen($counter)==1){ $pre='00';	
 	} elseif(strlen($counter)==2){ $pre='0'; } else { $pre=''; }
			 
 	$code    = $this->crud->browse("","subcategory","id",$id_subcat,"false","init_code"); 	
 	$result  = $code->init_code.".".$pre.$counter;
 	
 	return $result; 	
 }
 
 function category($table){
 	
 	$counter  = $this->total_record("",$table); 
 	$counter  = $counter==0 ? 1 : $counter+1; 	
 	
 	if(strlen($counter)==1){ $pre='00';	
 	} elseif(strlen($counter)==2){ $pre='0'; } else { $pre=''; }
 	
 	$result = $pre.$counter; 	
 	return $result; 	
 } 
 
 	
}	
?>