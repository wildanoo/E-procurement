<script type="text/javascript">
  $(function () { 
   	  var baseUrl  = "<?php echo base_url(); ?>"; 
   	  
   	  $('#btn_cancel').click(function(){ 
   			$("#container11").load(baseUrl+'due_diligence/load_visit_form');   		
   	   }); 
   	  
   	  $('#btn_notes').click(function(){ 
   	   		var id_visit = $("#id_visit").val();
   			$("#container11").load(baseUrl+'due_diligence/add_notes/' + id_visit);   		
   	  });    	  
   	  
  });  
  
  function delete_notes(id){  	
 		var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	    var token    = '<?php echo $this->security->get_csrf_hash(); ?>';  	
  		if (confirm('Are you sure you want to delete this?')) {  			
  			$.post( baseUrl+ "due_diligence/delete_notes", { csrf: token , id: id } )
			  .done(function( resp ) {
			  		var id_visit = $("#id_visit").val();
			    	$("#container11").load(baseUrl+'due_diligence/load_view_notes/'+ id_visit);
			});
  		}	  	
  }

</script>
<input type="hidden" name="id_visit" id="id_visit" value="<?php echo $id_visit; ?>"/>
<div style="text-align: right;">
<button type="button" id="btn_cancel" class="btn btn-default">Cancel</button> 
<?php if($this->permit->md3){ ?>
<button type="button" id="btn_notes" class="btn btn-info btn-primary">Add Notes</button> 
<?php } ?>
</div>

<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Notes</th>		
		<th>Created Date</th>
		<?php if($this->permit->md3){ ?><th>Action</th><?php } ?>
	</tr>
	</thead>
	<tbody>
 <?php   
 if($notes){ $i=1;
 	foreach($notes as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->notes; ?></td>						
			<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
			<?php if($this->permit->md3){ ?>			
			<td style="text-align: center;">			
			<?php echo '<i class="fa fa-trash"  title="Delete Notes" onclick="delete_notes('.$row->id.')"></i>'; ?></td>
			<?php } ?>
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="5">-</td></tr>
<?php } ?>
     </tbody>
</table>

