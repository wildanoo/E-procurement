<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Functional extends Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_user');
		islogged_in();
		$this->m_user->authenticate(array(41));
	}

	function index() {
		//if ($this->permit->mc) {
			
		$data ['user_id'] = $this->tank_auth->get_user_id ();
		$data ['username'] = $this->tank_auth->get_username ();
			
		$sess_cat   = $this->session->userdata('sess_cat');
		$sess_field   = $this->session->userdata('sess_field');
			

		$field      = $sess_field != '' ? $sess_field : "functional";
		$id_cat     = $sess_cat != '' ? $sess_cat  : "";

		if($sess_cat != ''){	$where = array('functional'=>$sess_cat,'status'=>'1');
		} else { $where = array('status'=>'1');	}
			
		$table = "functional";
		$page = $this->uri->segment ( 3 );
		$per_page = 10;
		$offset = $this->crud->set_offset ( $page, $per_page,$where );
		$total_rows = $this->crud->get_total_record ("",$table,$where);
		$set_config = array (
				'base_url' => base_url() . '/functional/index',
				'total_rows' => $total_rows,
				'per_page' => $per_page,
				'uri_segment' => 3
		);
		$config = $this->crud->set_config ( $set_config );
			
		$this->load->library ( 'pagination' );
		$this->pagination->initialize ( $config );
		$paging = $this->pagination->create_links ();
			
		$order = array (
				'field' => 'created_date',
				'order' => 'DESC'
		);
		$data ['pagination'] = $paging;
		$data ['num'] = $offset;
		$select = "id,code,functional,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";

		$sessID  = $this->session->flashdata('anID');
		$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);

		if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true",$where);
		else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
		$data['browse'] = $browse;

		$functional = $this->crud->browse("","functional","","","true","id,functional");
		if(!$functional) $functional = array();
		$select = array(''=>'All');
		foreach($functional as $val){ $options[$val->id] = $val->functional; }
		$data['functional'] = $select + $options;
		$id = $this->tank_auth->get_user_id();
		$data['profiles'] = $this->crud->browse("","profiles","id",$id,"false",'name');
		//print_r($data['profiles']);
		//$data['browse'] = $browse;
		$data['view']	= "functional/browse";
		$this->load->view('layout/template',$data);
			
		// 		} else {
		// 			$this->session->set_flashdata('message','user not authorized');
		// 			redirect ( '/auth/login/' );
		// 		}
	}

	private function search_input($search_dateranges = array(),$search_conditions = array()){

		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');

			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}

		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');

			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}

		$getData = array($search_dateranges,$search_conditions);

		return $getData;
	}


	function search(){

		/* initiate search inputs */

		//$search_dateranges = array('publish_date',$_POST['start_date'],$_POST['end_date']);
		$search_conditions = array(
				'code'				=> $_POST['search_term'],
				'functional'		=> $_POST['search_term']
		);

		$where_conditions  = $this->search_input("",$search_conditions);

		/* ==== */

		/* get data from defined function for table view */

		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);

		/* ==== */

		/* preparing data for display */

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];

		$data['view'] = "functional/browse";

		$this->load->view('layout/template',$data);

		/* ==== */

	}

	private function getDataTables($limit = NULL)
	{

		/* get data for table view from database with pagination */

		$table 	     = "functional t1";
		$select 	 = "t1.id,code,functional,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		//$order 		 = array('field'=>'t1.publish_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);

		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);

		$getData 	 = $this->crud->browse_join_with_paging("",$table,'status','1',"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }

		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'functional/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);

		/** setup for pagination **/

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();

		/** ===== **/
		/* ===== */

		/* setup variable to used in another functions */

		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;

		/* ===== */

		return $data;

	}

	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{

		/* get data for table view from database with pagination */

		$table 	     = "functional t1";
		$select 	 = "t1.id,code,functional,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		//$order 		 = array('field'=>'t1.publish_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);

		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);

		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,'t1.status = "1"',$dateranges,$wherearray,"",$per_page,$offset,"t1.id");

		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'functional/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);

		/** setup for pagination **/

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$paging = $this->pagination->create_links();

		/** ===== **/
		/* ===== */

		/* setup variable to used in another functions */

		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;

		/* ===== */

		return $data;

	}
	
	function validate_code(){
	
		$code    = $_POST['code'];
		$is_exist = $this->crud->is_exist("","functional","id",array('code'=>$code,'status'=>'1'));
		$status   = $is_exist || !$code ? "false" : "true";
	
		$result['status']  =  $status;
		echo json_encode($result);
	
	}
	
	function is_exist(){
		$code      = $_POST['code'];
		$functional = $_POST['functional'];
		$is_exist['1']= !$code? "false" : "true";
		$is_exist['2']= !$functional? "false" : "true";
		$msg['1']	  = !$code ? "code required" : "";
		$msg['2']	  = !$functional ? "functional required" : "";

		if ($is_exist['1']=='true' && $is_exist['2']=='true'){
			$where    = array('code'=>$code,'functional'=>$functional,'status'=>'1');
			$checked = $this->crud->is_exist("","functional","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			$is_exist['2']    = !$checked ? "true" : "false";
			$msg['1']	= $checked ? "duplicate code" : "";
			$msg['2']	= $checked ? "duplicate functional" : "";
				
		}

		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2']);
	
		echo json_encode($result);
	}

	function is_exist_(){
		$code = $_POST['code'];
		$where   = array('code'=>$code,'status'=>'1');
		$check   = $this->crud->is_exist("","functional","id",$where);
		$result  = !$check && $code  ? "true" : "false";

		$return    =  $result=="true" ? "true" : "false";
		$msg       =  $result=="true" ? "invalid code" : "";

		$freturn = array('status'=>$return,'msg' =>$msg);

		echo json_encode($freturn);
	}

	function get_country_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'country_name','sort'=>'ASC'); $select = "country_name as name";
		$result = $this->crud->autocomplete("","country",$select,"country_name",$term,"",$order);
		echo json_encode($result);
	}

	function set_sess_country(){

		$type = $_POST['country_name'];
		if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
		} else { $this->session->unset_userdata('sess_cat'); }

		$type = $_POST['field'];
		if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
		} else { $this->session->unset_userdata('sess_field'); }

	}

	function set_sess_search(){
		$like = $_POST['search'];
		$this->session->set_flashdata('like',$like);
	}

	function create(){
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$data 		= array('id'=>null,'code'=>$_POST['code'],'functional'=>$_POST['functional'],
				'created_id'=>$userID,'created_date'=>$curr_date,'last_updated'=>$curr_date);
		$country = $this->crud->browse("","functional","","","true","id,functional");
		$where   = array('code'=>$_POST['code'],'status'=>'0');
		$checked = $this->crud->is_exist("","functional","id",$where);

		if ($checked){
			$id = $this->crud->browse("","functional","code",$_POST['code'],"false","id")->id;
			$update = array('status'=>'1','last_updated'=>$curr_date,'functional'=>$_POST['functional']);
			$this->crud->update("","functional","id",$id,$update);
		} else {	$id = $this->crud->insert("","functional",$data);	}

		$this->session->set_flashdata('message','1 data success insert');
	}

	function form_create() {
		$data ['user_id'] = $this->tank_auth->get_user_id ();
		$data ['username'] = $this->tank_auth->get_username ();
		$this->load->view('functional/form_create',$data);
	}

	function update(){
		$curr_date 	= date('Y-m-d H:i:s');
		$userID = $this->tank_auth->get_user_id();
		$id   = $this->uri->segment(3);
		$data = array('code'=>$_POST['code'],'functional'=>$_POST['functional'],'status'=>'1',
				'last_updated'	=> $curr_date,'created_id'=>$userID);
		$this->crud->update("","functional","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
	}

	function form_update(){
		$id   = $this->uri->segment(3);
		$select = "id,code,functional,created_id,created_date,last_updated";
		$data ['def'] = $this->crud->browse ( "", "functional", "id", $id, "false", $select);
		$this->load->view ( 'functional/form_update', $data );
	}

	function delete(){
		$id = $this->uri->segment(3);
		$status = array('status'=>'0');
		$this->crud->update("","functional","id",$id,$status);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('functional/','refresh');
	}
}