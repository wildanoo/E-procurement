<table id="v_tab" class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
		<tr>
			<th>No</th>
			<th>Registration Num.</th>
			<th>Vendor Num.</th>
			<th width="200">Vendor Name</th>
			<th>Last Updated</th>
			<th>Reason</th>
			<th>Delete Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$i = 1;
		foreach($browse as $row){
			if($row->status == 'completed'){
				$text = "<span class='label label-inverse'>Deleted</span>";
			}else if($row->status == 'start'){
				$text = '<span class="label label-default">Failed to delete</span>';
			}else if($row->approval == '1' && $row->status == 'request'){
				$text = '<span class="label label-warning">Waiting</span>';
			}else if($row->approval == '2' && $row->status == 'request'){
				$text = '<i class="fa fa-check-circle" style="color:#00a65a"></i> <span class="label label-warning">Waiting</span>';
			}else if($row->approval == '2' && $row->status == 'approved'){
				$text = '<span class="label label-success">Approved</span>';
			}
			?>
			<tr>
				<td><?= $num  = $num+1 ;?></td>
				<td><?= $row->register_num ?></td>
				<td><?php if(empty($row->vendor_num)){echo " - ";}else{echo $row->vendor_num ;} ?></td>
				<td><?= $row->vendor_name?></td>
				<td><?= date('M d, Y',strtotime($row->last_updated))?></td>
				<td><?= $row->remark?></td>
				<td><?php echo ucfirst($text) ?></td>
				<td>
					<div class="btn-group dropup">
						<button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?php echo base_url('register/set_vendor_session/'.$row->id_vendor); ?>">View Detail</a></li>
							<?php if($row->status == 'request' && !$this->permit->vvdibk){ if($row->approval != '2'){ ?>
							<li><a href="javascript:void(0)" onclick="del_ven_ibs(<?= $row->id_vendor.',\''.$row->remark.'\''.','.$row->id;?>)">Approve Delete</a></li>
							<?php } }else if($row->status == 'request' && $this->permit->vvdibk){?>
							<li><a href="javascript:void(0)" onclick="del_vendor(<?php echo $row->id_vendor.',\''.$row->remark.'\'';?>)">Approve Delete</a></li>
							<?php } ?>
						</ul>
					</div>
				</td>
			</tr>
			<?php  $i++; } ?>
			<tr>
				<td colspan="9" style="text-align: center;"><?php echo $pagination ; ?></td>
			</tr>
		</tbody>


	</table>