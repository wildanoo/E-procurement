<div class="page-header">
  <h3>FAQ</h3>
</div>	
<?php echo form_open_multipart('cms/change_content_faq',array('id'=>'form')); ?> 
		<td class="form_tab_content" width="75%;">
		<td colspan="2">&nbsp;</td>
			<div>
				<ul class="nav nav-tabs test2">
				    <li class="active"><a data-toggle="tab" href="#idn" aria-expanded="true">Indonesia</a></li>
				    <li class=""><a data-toggle="tab" href="#eng" aria-expanded="false">English</a></li>
				</ul>

				<div class="tab-content">
				    <div id="idn" class="tab-pane fade active in" style="padding:15px;">
				      <?php $data['content'] = $contents[0]; $this->load->view('announce/note',$data); ?>
				    </div>
				    <div id="eng" class="tab-pane fade" style="padding:15px;">
				      <?php $data['content'] = $contents[1]; $this->load->view('announce/note',$data); ?>
				    </div>
				</div>
			</div>
		</td>

		<tr>
		<td>
		<!-- td><label>Attachment</label></td><td>:</td> -->
		<?php $style = "cursor:pointer; width:280px; height:25px; font-family: Arial bold; font-size: 13px;";?>
		<?php	
			$data  = array('type'=>'file','name'=> 'pdffile','id' => 'file_upload','value'=>'true','content'=>'Import', 'style'=>$style); 
			echo form_input($data); ?> <i>*.pdf</i>
		</td>
		</tr>
		<br>
<input type="hidden" name="filename" value="faq"/>
<input type="submit" value="Submit" class="btn btn-primary btn-sm"/>

<?php if($attfile!="-") echo anchor('cms/open_file/attach/faq',$attfile,array('class'=>'default_link','title'=>'Viewed')); ?>
<?php echo form_close(); ?>


	