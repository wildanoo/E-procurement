  <?php
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'class' => 'form-control',
		'placeholder' => 'Username',
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
		'size'	=> 30,
	);
}
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'class' => 'form-control',
	'placeholder' => 'Email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
// $department = array(
// 		'name'	=> 'id_dept',
// 		'id'	=> 'id_dept',
// 		'class' => 'form-control',
// 		'placeholder' => 'Department',
// 		'value'	=> set_value('id_dept'),
// 		'maxlength'	=> 80,
// 		'size'	=> 30,
// );
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'class' => 'form-control',
	'placeholder' => 'Password',
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'class' => 'form-control',
	'placeholder' => 'Confirm Password',
	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
/*$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'class' => 'form-control',
	'style' => 'width:200px;',
	'placeholder' => 'Captcha',
	'maxlength'	=> 8,
);*/
?>
 
 
 
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">

              <div class="box-header with-border">
                <h3 class="box-title">Create User</h3>
              </div>

              <!--form class = "form-horizontal"-->
              <?php echo form_open($this->uri->uri_string(),array('class'=>'form-horizontal')); ?>
                <div class="box-body">

                  <div class = "col-md-6">
                  
                    <div class="form-group">
                      <label for="username" class="col-md-3 control-label inputbox-size"><?php echo form_label('Username', $username['id']); ?></label>
                      <div class="col-md-9 row">
                        <?php echo form_input($username); ?>
                        <?php echo form_error($username['name']); ?>
                        <?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?>
                      </div> 
                    </div>
                    
                     <div class="form-group">
                      <label for="email" class="col-md-3 control-label inputbox-size"><?php echo form_label('Email Address', $email['id']); ?></label>
                      <div class="col-md-9 row">
                        <?php echo form_input($email); ?>
                        <?php echo form_error($email['name']); ?>
                        <?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?>
                      </div> 
                    </div>
                    
                    <div class="form-group">
                      <label for="department" class="col-md-3 control-label inputbox-size"><?php echo form_label('Department'); ?></label>
                      <div class="col-md-9 row">
                        <?php //echo form_input($department); ?>
                        <?php $prop = 'id="id_dept" class="form-control"';
          				echo form_dropdown("id_dept",$department,$profiles->id_dept,$prop);?>
                        <?php //echo form_error($department['name']); ?>
                        <?php //echo isset($errors[$department['name']])?$errors[$department['name']]:''; ?>
                      </div> 
                    </div>
                    
                    <div class="form-group">
                      <label for="password" class="col-md-3 control-label inputbox-size"> <?php echo form_label('Password', $password['id']); ?></label>
                      <div class="col-md-9 row">
                        <?php echo form_password($password); ?>
                        <?php echo form_error($password['name']); ?>
                      </div> 
                    </div>
                    
                    
                     <div class="form-group">
                      <label for="confirm_password" class="col-md-3 control-label inputbox-size"><?php echo form_label('Confirm Password', $confirm_password['id']); ?></label>
                      <div class="col-md-9 row">
                        <?php echo form_password($confirm_password); ?>
                        <?php echo form_error($confirm_password['name']); ?>
                      </div> 
                    </div>              
                                               


<?php /*if ($captcha_registration) {
		if ($use_recaptcha) { ?>	
			<div id="recaptcha_image"></div>		
			<a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
			<div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
			<div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>		
			<div class="recaptcha_only_if_image">Enter the words above</div>
			<div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
		<input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
		<?php echo form_error('recaptcha_response_field'); ?>
		<?php echo $recaptcha_html;*/ ?>
	
	<?php //} else { ?>
	
		<!--p>Enter the code exactly as it appears:</p-->
		<?php //echo $captcha_html; ?>		
		<?php //echo form_label('Confirmation Code', $captcha['id']); ?>
		<?php //echo form_input($captcha); ?>
		<?php //echo form_error($captcha['name']); ?>
	
	<?php //}	} ?>
                 
                </div><!-- col-md-6 -->                
                </div><!-- /.box-body -->
                <div class="box-footer" style="padding-left: 30px;">
                    <?php  $prop = array('class'=>'btn btn-default','title'=>'Cancel'); ?>					 
					<?php  echo anchor('user/','Cancel',$prop); ?>
                    <?php echo form_submit('register', 'Register','class="btn btn-info btn-danger"'); ?>
                </div><!-- /.box-footer -->
              <!--/form-->
              <?php echo form_close(); ?>
            </div><!-- /.box -->
          </div>
        </div>
      </div>
    </div><!-- /row --> 