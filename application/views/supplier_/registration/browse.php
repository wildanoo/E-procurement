<html>
<head>
<title>Browse</title>
<script type="text/javascript"> 
	$(document).ready(function(){	
		var baseUrl   = "<?php echo base_url(); ?>";	
		$(function() {				
			$('.checkall').on('click', function(){
					$(this).closest('table').find(':checkbox').prop('checked', this.checked);
			});		
		});	      
		
		$("#btn_search").click(function(){
            var search = $("#search").val();  //alert(search_code);
            $.ajax({
               url  : baseUrl + 'supplier/set_browse_session',
               type : "POST",              
               data : 'search='+search,
               success: function(resp){ 
               		//$("#debug").html(resp);                 
				    document.location.href= baseUrl + 'supplier/';
               }
            });
        });
		
		<?php for ($j = 1; $j <= count($browse); $j++) { ?>			

			$('#select'+<?php echo $j; ?>).click(function(){
					var status  = this.checked;	
					if(status){								     
					var select = $('#select'+<?php echo $j; ?>).val(); 
						if(confirm('Supplier Approved ?')){    				         	 				
							$.ajax({
				                url     : baseUrl + 'supplier/approved',
				                type    : 'POST',												
				                data    : 'select='+select,
				                success : function(resp){							
				                      document.location.href= baseUrl + 'supplier/';			                        		
				                }
							});						
						} else { return false; }
					} else { alert('cancel ignored !'); $( '#select'+<?php echo $j; ?> ).prop( "checked", true ); }	
						
		      }); 		
	<?php }?>
	
	});
	
	function newPopup(url) { //alert(url);
		popupWindow = window.open(
		url,'popUpWindow','height=650,width=900,left=200,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
	}
</script>
<style type="text/css">
.ui-autocomplete{
	font-size:13px;
	max-height: 180px;
	max-width: 280px;
	overflow: auto;
}
.ui-corner-all{
	border-radius:0;
}
.ui-menu-item a:hover{
	background-image:none;
	background-color: #f1d597;
	border: none;			
	cursor:pointer;
}
</style>
</head>
<body>
<div id="debug"></div>
<h1 class="title">Supplier Registration

</h1>
<div style="text-align: center;">
<?php 	$msg = $this->session->flashdata('message');
	if(strpos($msg,'failed')!==false) $color = "#bc0505";
	else $color = "#487952"; ?>	
</div>
<font color='<?php echo $color; ?>'><i><?php echo $msg; ?></i></font>
<div style="text-align: right;">
<?php
$add_ico  = '<img src="'.base_url().'assets/images/add.png" width="20" height="20"/>';
$prop     = array('class'=>'default_link','title'=>'SAP Account'); echo anchor('sap/',$add_ico,$prop);?>

<label>Search : <input type="text" name="search" id="search" style="width:150px;"/>
<input type="button" name="btn_search" id="btn_search" class="btn btn-primary" value="Search"/>
</label></div>
<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
        <tr>
            <th style="text-align: center;" >No</th>
            <th style="text-align: center;"><input type="checkbox" class="checkall"> Check All</th>
            <th>Supplier Num</th>            
            <th>Supplier Name</th>  
            <th>Category</th>          
            <th>Approved</th>
            <th>Update<br/>Profile</th>     
            <th>Joined</th>  
        </tr>       
	</thead>
    <tbody>
<?php	
//$browse = array();
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td style="text-align: center;"><?php  echo $num = $num+1;; ?></td>
				<td style="text-align: center; width:10%;"><?php  $checked = $row->status==1 ? "checked" : "";  ?>
					<input type="checkbox"  <?php echo $checked; ?> name="<?php echo "select".$i;?>" id="<?php echo "select".$i;?>" value="<? echo $row->id;?> "></td>
				<td><?php  $prop = array('class'=>'default_link','title'=>'Registration');
						echo anchor('register/form_create/'.$row->id,$row->reg_num,$prop);  ?></td>				
				<td><?php 	echo anchor('register/form_create/'.$row->id,$row->supplier_name,$prop);  ?></td>
				<td><?php echo $row->cat; ?></td>				
				<td align="center"><?php   if($row->status==0) echo "Waiting";  else echo "Yes"; ?></td>
				<td style="text-align: center;"><?php  if($row->jum==0) { echo $row->jum;   ?>
					 <?php  } else { $prop = array('class'=>'default_link','title'=>'Check Profile Update');
						echo anchor('profile/check_update/'.$row->id,$row->jum,$prop);  }?>					
				</td>				
				<td style="text-align: center;"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
				
			</tr>
<?php	$i++;  } ?>	
<tr><td colspan="8" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?>
	<tr><td colspan="8" align="center">-</td></tr>
<?php } ?>
    </tbody>
</table>


</body>
</html>