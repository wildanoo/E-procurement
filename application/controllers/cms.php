<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Cms extends Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('file');
		$this->load->model(array('m_announce','codec'));
	}	 

 public function index(){ 	
 	 if ($this->tank_auth->is_logged_in()) {  	 		
 	 
 	 	$data['user_id']  = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
		$homepath         = file_get_contents("./uploads/cms/home_ind.txt");
		$data['content']  = $homepath;  	
		$filename 		  = "home";
		$homepath_id      = "./uploads/cms/".$filename."_ind.txt";
		$homepath_eng     = "./uploads/cms/".$filename."_eng.txt";
			
		$contents[0] = file_exists($homepath_id) ? file_get_contents($homepath_id) : "-";
		$contents[1] = file_exists($homepath_eng) ? file_get_contents($homepath_eng) : "-";
		
		$data['contents'] = !$contents ? "-" : $contents;
		
 	 	$data['view']	  = "cms/form_home"; 		
 		$this->load->view('layout/template',$data); 	 	
 	 	
 	 } else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	}
 } 

 public function contact(){ 	
 	 if ($this->tank_auth->is_logged_in()) { 	
            
        $attpath = "./uploads/attach/faq.pdf";
 	 	$attfile = file_exists($attpath) ? "true" : "false";
 	 	$data['attfile'] = $attfile=="false" ? "-"  : "faq.pdf";    
        
	 	$data['user_id']  = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
		$contactpath  	  = file_get_contents("./uploads/cms/contact_ind.txt");
		$data['content']  = $contactpath;  	
		$filename		  = "contact";
		$contactpath_id   = "./uploads/cms/".$filename."_ind.txt";
		$contactpath_eng  = "./uploads/cms/".$filename."_eng.txt";
		
		$contents[0] = file_exists($contactpath_id) ? file_get_contents($contactpath_id) : "-";
		$contents[1] = file_exists($contactpath_eng) ? file_get_contents($contactpath_eng) : "-";
		
		$data['contents'] = !$contents ? "-" : $contents;
		
 	 	$data['view']	  = "cms/form_contact"; 		
 		$this->load->view('layout/template',$data); 	 	
 	 	
 	 } else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	}
 } 
 
   public function faq(){ 	
 	 if ($this->tank_auth->is_logged_in()) { 	
//  	 	$id        = $this->uri->segment(3);
//  	 	$select	   = "id,code,type,id_cat,id_subcat,
// 	 	              (SELECT company_name FROM company s WHERE s.id=l.id_company) as company_name,
// 					  (SELECT office_name FROM offices r WHERE r.id=l.id_office) as office_name,
// 					  title,detail,first_valdate,end_valdate,publish_date,creator,file,
// 					  status,created_id,created_date,last_updated";
//  	 	$default   = $this->crud->browse("","announce l","id",$id,"false",$select);
//  	 	print_r($default);
//  	 	$data['default'] = $default;
 	 	$attpath = "./uploads/attach/faq.pdf";
 	 	$attfile = file_exists($attpath) ? "true" : "false";
 	 	$data['attfile'] = $attfile=="false" ? "-"  : "faq.pdf";
 	 	
 	 	$data['user_id']  = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
		$faqpath  		  = file_get_contents("./uploads/cms/faq_ind.txt");
		$data['content']  = $faqpath; 
		$filename		  = "faq";
		$faqpath_id   = "./uploads/cms/".$filename."_ind.txt";
		$faqpath_eng  = "./uploads/cms/".$filename."_eng.txt";
		
		$contents[0] = file_exists($faqpath_id) ? file_get_contents($faqpath_id) : "-";
		$contents[1] = file_exists($faqpath_eng) ? file_get_contents($faqpath_eng) : "-";
		
		$data['contents'] = !$contents ? "-" : $contents;
 	 	$data['view']	  = "cms/form_faq"; 		
 		$this->load->view('layout/template',$data); 	 	
 	 	
 	 } else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	}
 }

   public function aboutus(){ 	
 	 if ($this->tank_auth->is_logged_in()) { 	
 	 
 	 	$data['user_id']  = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
		$aboutuspath      = file_get_contents("./uploads/cms/aboutus_ind.txt");
		$data['content']  = $aboutuspath; 
		$filename		  = "aboutus";
		$aboutuspath_id       = "./uploads/cms/".$filename."_ind.txt";
		$aboutuspath_eng      = "./uploads/cms/".$filename."_eng.txt";
		
		$contents[0] = file_exists($aboutuspath_id) ? file_get_contents($aboutuspath_id) : "-";
		$contents[1] = file_exists($aboutuspath_eng) ? file_get_contents($aboutuspath_eng) : "-";
		
		$data['contents'] = !$contents ? "-" : $contents;
 	 	$data['view']	  = "cms/form_aboutus"; 		
 		$this->load->view('layout/template',$data); 	 	
 	 	
 	 } else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/');	 
	}
 } 
 
  public function terms(){
 	if ($this->tank_auth->is_logged_in()) {
 			
 		$data['user_id']  = $this->tank_auth->get_user_id();
 		$data['username'] = $this->tank_auth->get_username();
 		$termspath      = file_get_contents("./uploads/cms/terms_ind.txt");
 		$data['content']  = $termspath;
 		$filename		  = "terms";
 		$termspath_id       = "./uploads/cms/".$filename."_ind.txt";
 		$termspath_eng      = "./uploads/cms/".$filename."_eng.txt";
 
 		$contents[0] = file_exists($termspath_id) ? file_get_contents($termspath_id) : "-";
 		$contents[1] = file_exists($termspath_eng) ? file_get_contents($termspath_eng) : "-";
 
 		$data['contents'] = !$contents ? "-" : $contents;
 		$data['view']	  = "cms/form_terms";
 		$this->load->view('layout/template',$data);
 
 	} else {
 		$this->session->set_flashdata('message','user not authorized');
 		redirect('/auth/login/');
 	}
 }
 
  public function technical(){
 	if ($this->tank_auth->is_logged_in()) {
 
 		$data['user_id']  = $this->tank_auth->get_user_id();
 		$data['username'] = $this->tank_auth->get_username();
 		$technicalpath      = file_get_contents("./uploads/cms/technical_ind.txt");
 		$data['content']  = $technicalpath;
 		$filename		  = "technical";
 		$technicalpath_id       = "./uploads/cms/".$filename."_ind.txt";
 		$technicalpath_eng      = "./uploads/cms/".$filename."_eng.txt";
 
 		$contents[0] = file_exists($technicalpath_id) ? file_get_contents($technicalpath_id) : "-";
 		$contents[1] = file_exists($technicalpath_eng) ? file_get_contents($technicalpath_eng) : "-";
 
 		$data['contents'] = !$contents ? "-" : $contents;
 		$data['view']	  = "cms/form_technical";
 		$this->load->view('layout/template',$data);
 
 	} else {
 		$this->session->set_flashdata('message','user not authorized');
 		redirect('/auth/login/');
 	}
 }
 
 public function change_content(){
 	if ($this->tank_auth->is_logged_in()) {
 		
 		$filename = $_POST['filename'];
 		$editor   = $_POST['editor'];
 		$root     = $_SERVER['DOCUMENT_ROOT'];
 		$path	  = $root.SRC_PATH."/uploads/cms/";
 		$myfile   = "";
 		
 		for ($i = 0;$i<2;$i++){
 			if ($i == 0) {
 				$myfile = fopen($path.$filename."_ind.txt", "w");
 			}else{
 				$myfile = fopen($path.$filename."_eng.txt", "w");
 			}
 			$txt    = fwrite($myfile,$editor[$i]);
 		
 		//print_r($editor[$i]);
 		}
 		//if($_FILES['pdffile']){

 		//}
 		//exit;
 		$url = 	$filename=="home" ? "" : $filename;
 		redirect('cms/'.$url,'refresh');
 	} else {
 		$this->session->set_flashdata('message','user not authorized');
 		redirect('/auth/login/');
 	}
 }
 
 function change_content_faq(){
 	if ($this->tank_auth->is_logged_in()) {
 			
 		$filename = $_POST['filename'];
 		$editor   = $_POST['editor'];
 		$root     = $_SERVER['DOCUMENT_ROOT'];
 		$path     = $root."/uploads/cms/";
 		$myfile   = "";
 			
 		for ($i = 0;$i<2;$i++){
 			if ($i == 0) {
 				$myfile = fopen($path.$filename."_ind.txt", "w");
 			}else{
 				$myfile = fopen($path.$filename."_eng.txt", "w");
 			}
 			$txt    = fwrite($myfile,$editor[$i]);
 				
 			//print_r($editor[$i]);
 		}
 		
 		$pdffile = $_FILES['pdffile'];
 		$root	= $_SERVER['DOCUMENT_ROOT'];
 		$_FILES['userfile']  = $pdffile;
 		$path 	 = './uploads/attach/';
 		$config  = $this->m_announce->set_config($filename.".pdf",$path,'pdf');
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config);
 			
 		$msg['error_pdf'] = "";  $msg['success_pdf']   = array();
 		if ( ! $this->upload->do_upload()){
 			$msg['error_pdf']   = $this->upload->display_errors();
 		} else {
 			$msg['success_pdf'] = $this->upload->data(); 
 		}
 		$url = 	$filename=="home" ? "" : $filename;
 		redirect('cms/'.$url,'refresh');
 		
 		} else {
 			$this->session->set_flashdata('message','user not authorized');
 			redirect('/auth/login/');
 		}
 }
 
  function open_file(){
 	
 	$root     = $_SERVER['DOCUMENT_ROOT'];
 	$file     = $root."/uploads/attach/faq.pdf";
 	$filename = "faq.pdf";
 
 	header('Content-type: application/pdf');
 	header('Content-Disposition: inline; filename="' . $filename . '"');
 	header('Content-Transfer-Encoding: binary');
 	header('Content-Length: ' . filesize($file));
 	header('Accept-Ranges: bytes');
 	@readfile($file);
 
 }
 
	
}
 
?>