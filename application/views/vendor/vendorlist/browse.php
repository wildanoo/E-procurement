<script type="text/javascript">
	$(function() {
		var baseUrl   = "<?php echo base_url(); ?>";
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token     = '<?php echo $this->security->get_csrf_hash(); ?>';

		$('#idsubcat').change(function(){
			$('#form_search').submit();
		});

		$('#sortstatus').change(function(){
			$('#form_search').submit();
		});

		$('#venstat').change(function(){
			$('#form_search').submit();
		});

		$('#form_search').submit(function(){
			var sdata=$(this).serialize();
			var v_url = baseUrl+'vendorlist/sorting';
			$.post(v_url, sdata, function(data){
				$('#v_tab').html(data);
			});
		});

		$('#start_date, #end_date').datepicker({
			changeMonth: true, changeYear: true, dateFormat:"yy-mm-dd"
		});

		$('#excelbtn').click(function(){
			var fields = $("input[name='ck_list[]']").serializeArray(); 
			if (fields.length === 0){ 
				alert('Select one or more data!'); 
				return false;
			}else{ 
				$('#myform').submit();
			}
		});

	});

	function load_index(){
		var baseUrl   = "<?php echo base_url(); ?>"; 
		$('#form_search')[0].reset();
		$('#v_data').load(baseUrl+'vendorlist');
	}

	function inactive(id){
		var cek_url = baseUrl+'vendorlist/validate_ina_vendor';
		$.post(cek_url,{id_ven : id}, function(data, textStatus, xhr){
			if(data == 'false'){
				alert('Vendor yang anda pilih sedang dalam proses lain. Silahkan pilih vendor lain');
			}else{
				$("#main_container").load(baseUrl+'vendorlist/inactive_form/'+id, function(){
					$("#formin").attr('action',baseUrl+'vendorlist/inactive_act/');
					$("#titleform").html('Change Vendor Status To <span id="titletop">Inactive</span>');
				});
			}
		});
	}

	function active(id){
		var cek_url = baseUrl+'vendorlist/validate_ina_vendor';
		$.post(cek_url, {id_ven : id}, function(data, textStatus, xhr){
			if(data == 'false')
			{
				alert('Vendor yang anda pilih sedang dalam proses lain. Silahkan pilih vendor lain');
			}else{
				$("#main_container").load(baseUrl+'vendorlist/inactive_form/'+id, function(){
					$("#formin").attr('action',baseUrl+'vendorlist/active_act/');
					$("#titleform").html('Change Vendor Status To <span id="titletop">Active</span>');
				});
			}
		});
	}

	function delete_ven(id){
		var cek_url = baseUrl+'vendorlist/validate_del_vendor';
		$.post(cek_url, {id_ven : id}, function(data, textStatus, xhr){
			if(data == 'false'){
				alert('Vendor yang anda pilih sedang dalam proses lain. Silahkan pilih vendor lain');
			}else{
				$("#main_container").load(baseUrl+'vendorlist/inactive_form/'+id, function(){
					$("#formin").attr('action',baseUrl+'vendorlist/delete_act/');
					$("#titleform").html('Request To <span id="titletop">Delete Vendor</span>');
				});
			}
		});
	}

	function prom_avl(id){
		var v_url 	= baseUrl+'vendorlist/promote_avl_act';
		var id_ven	= id;
		var check	= confirm("Are you sure want to promote this vendor to AVL?");
		if(check){
			$.post(v_url, {id : id_ven}, function(data){
				$('#main_container').load(baseUrl+'vendorlist/browse');
			});
		}
		
	}

</script>
<div id="main_container">
	<div class="page-header">
		<h3>Vendor List</h3>
	</div>
	<?php echo form_open('',array('id'=>'form_search','novalidate'=>'true','onsubmit'=>'return false')); ?>
	<div class="table">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-search"></i> <strong>Search Section</strong>
			</div>
			<style type="text/css">
				.list-inline li{
					vertical-align: middle !important;
				}
				.btn-srch{
					margin-bottom: 0;
				}
			</style>
			
			<div class="panel-body">
				<ul class="list-inline">
					<li style="width:24%">
						<div class="input-group">
							<input type="date" value="" placeholder="date start" class="form-control date_picker" id="start_date" name="start_date">
							<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
						</div>
					</li>
					<li style="width:2%">to</li>
					<li style="width:24%">
						<div class="input-group">
							<input type="date" value="" placeholder="date end" class="form-control date_picker" id="end_date" name="end_date">
							<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
						</div>
					</li>
					<li style="width:34%"><div class="input-group" style="width:100%"><input placeholder="search by vendor name..." type="text" value="" class="form-control" id="search_term" name="search_term"></div></li>
					<li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search" id="searchbtn">Search</button></div></li>
					<li style="width:7%"><div class="input-group"><a href="<?php echo base_url() ?>vendorlist" class="btn btn-srch btn-warning btn-sm">View All</a></div></li>
				</ul>
			</div>
			
		</div>
	</div>
	<hr>

	<!-- search part -->

	<div class="col-md-3">
		<select name="subcat" id="idsubcat" class="form-control">
			<option value="all">- All Subcategories -</option>
			<?php foreach($subcat as $sc){ if($idcategory != $sc->id){?>
			<optgroup label="<?php echo $sc->category ?>"><?php } ?>
				<option  value="<?php echo $sc->idsub  ?>"<?php if($sc->idsub==$this->session->userdata('id')){echo "selected";} ?> ><?php echo $sc->subcategory; ?></option>
				<?php $idcategory = $sc->id;} ?>
			</select>
		</div>
		<div class="col-md-3">
			<select name="status" id="sortstatus" class="form-control">
				<option value="all">- All Type -</option>
				<?php foreach($status as $sk => $sv){ ?>
				<option value="<?php echo $sk ?>"<?php if($sk==$this->session->userdata('status_select')){echo "selected";} ?> ><?= $sv ?></option>
				<?php  } ?>
			</select>
		</div>
		<div class="col-md-3">
			<select name="venstat" id="venstat" class="form-control">
				<option value="all">- All Status -</option>
				<?php foreach($status2 as $s2){ if(!empty($s2->status)){?>
				<option value="<?php echo $s2->status ?>"<?php if($s2->status==$this->session->userdata('venstat_select')){echo "selected";} ?> ><?= ucfirst($s2->status); ?></option>
				<?php  } } ?>
			</select>
		</div>
		<?php echo form_close(); ?>
		<?php echo form_open('vendorlist/export_excel',array('id'=>'myform')); ?>
		<button class="btn btn-success pull-right" type="button"  id="excelbtn" title="Export Vendor List"><i class="fa fa-file-excel-o" style="color:#FFFFFF"></i>Export Excel</button>
		
		<?php require_once('v_data.php'); ?>
		
		<?php echo form_close(); ?>
	</div>