<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
<script type="text/javascript"> 	
$(document).ready(function(){	 
	 $(function(){  });
		var baseUrl = "<?php echo base_url(); ?>";

		$("#srch-term").autocomplete({
			source: baseUrl + 'document_req/get_tags',
			minLength:1
		});
		
		$("#btn_search").click(function(){
	        var search = $("#srch-term").val(); // alert(search);
	        var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
	        $.ajax({
	           url  : baseUrl + 'document_req/set_sess_search',
	           type : "POST",              		               
	           data : csrf +'='+ token +'&search='+search,
	           success: function(resp){    //$("#debug").html(resp);            		            
				    document.location.href= baseUrl + 'document_req/';
	           }
	        });
	    });
	    	 
	 $('#btn_create').click(function(){ 
	 	  form_create();
	 	  //alert('test');
	 });	
});

	function form_create(){
		var baseUrl = "<?php echo base_url(); ?>";		 
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		BootstrapDialog.show({
		title: 'Create New Document',
        message: $('<div></div>').load(baseUrl + "document_req/form_create/"),
        buttons: [{ label: 'Submit',
	                cssClass: 'btn btn-primary btn-sm',		               
	                action: function(dialogRef){
	                	 var document_req  = $("#document_req").val();
	                	 
	                	 $.post( baseUrl + 'document_req/is_exist', { csrf: token , document_req : document_req })
						 .done(function( resp ) {
//								 $('#debug').html(resp);
//								 dialogRef.close(); 
								var json = $.parseJSON(resp);
								if (json.status=='true'){
				                	var values = $("#form").serializeArray();
				                	values.push({ name: csrf,value: token });
									values = jQuery.param(values);
									 $.ajax({
							               url  : baseUrl + 'document_req/create',
							               type : "POST",              		               
							               data : values,
							               success: function(resp){
								               //alert(values);
							               	   document.location.href= baseUrl + 'document_req/';	               				               		  
									       }
							            });   
								} else { 	
									 $("#msg1").html(json.msg); 
							}
	               		 });  
	                }
	            }, {
	            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
			                action: function(dialogRef) {
			                	document.location.href= baseUrl + 'document_req/';
			                    dialogRef.close(); 
			                }
		        }]
    });
}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Document Requirement',
            message: $('<div></div>').load(baseUrl + "document_req/form_update/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef)
		                {
		                	 var document_req  = $("#document_req").val(); 
		                	 
		                	 $.post( baseUrl + 'document_req/is_exist', { csrf: token ,  document_req : document_req })
							 .done(function( resp ) {
//									 $('#debug').html(resp);
//									 dialogRef.close(); 
									var json = $.parseJSON(resp);
									if (json.status=='true'){
					                	var values = $("#form").serializeArray();
					                	values.push({ name: csrf,value: token });
										values = jQuery.param(values);
										 $.ajax({
								               url  : baseUrl + 'document_req/update',
								               type : "POST",              		               
								               data : values,
								               success: function(resp){
								               		document.location.href= baseUrl + 'document_req/';	               				               		  
										       }
								            });   
									} else { 	
									 $("#msg1").html(json.msg); 
								}
		               		 });  
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'document_req/';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>
<script src="<?php echo base_url()?>assets/js/sorttable.js"></script>
</head>
<body>
<div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Manage Document Requirement</h3>
              <!-- /.box-header -->
              </div>
                <br>
                
                    <?php if($this->session->flashdata('msg_warning')!=null) { ?>
                	<div class="alert alert-warning">
  						<strong>Warning!</strong> <?php echo $this->session->flashdata('msg_warning'); ?>
					</div>
					<?php } ?> 
                <div class="col-md-2 pull-left">
                    <input type="button" id="btn_create" class="btn btn-primary btn-sm" title="Create New Document" value="New Document Requirement" style="width:190px;" title="Create New Document"/>
					<?php $msg = $this->session->flashdata('message');
						if(strpos($msg,'failed')) $color="#bc0505";
						else $color = "#487952"; ?>	
				<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
                </div>


                <div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('document_req/search',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>document_req" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		        </div>
		        </div>

        
<div class="box-body">
<table border="1" id="example2" class="table table-bordered table-hover text-center sortable" cellspacing="0" width="80%" style="font-size: 13px">
	<thead>
		<tr>
            <th class="table-sorted-reverse">No</th>
            <th class="">Document Name</th>
            <th class="">Status</th>
            <th class="">Created Date</th>
            <th class="">Updated Date</th>
            <th class="">Updated By</th>
            <th class="">Action</th>
        </tr>
     </thead>
     <tbody>
 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td style="text-align:center;"><?php echo $num = $num+1; ?></td>
			<td><?php echo $row->document_req; ?></td>	
			<td><?php echo $row->status==1 ? 'Active' : 'Not Active'; ?></td>	
 			<td align="center"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>	
 			<td align="center"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>	
 			<td align="center"><?php echo $row->created_id; ?></td>						
				<td><?php				
					echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/></i>';					
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete document ?')){ return true; } else {return false; }";
					echo anchor('document_req/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete Document','onclick'=>$js));?>
				</td>					
		</tr>
<?php	$i++;  } ?>	
</table><div align= "center"><?php echo $pagination;?></div>
<?php } else { ?>
	<tr><td colspan="8">-</td></tr>
<?php } ?>
     </tbody>
                  </table>
                  
                </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->
