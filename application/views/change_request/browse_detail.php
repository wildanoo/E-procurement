<script>
	$(function() {
      var baseUrl = "<?=base_url()?>";
	    $('#ck_all').on('click', function(){
			     $(this).closest('table').find(':checkbox').prop('checked', this.checked);
		  });
      $('#myform').submit(function(event) {
        var sdata = $(this).serialize();
        var surl = $(this).attr('action');
        $.post(surl, sdata, function(data, textStatus, xhr) {
          $.get(baseUrl+'change_request/send_approveMail', function(xdata, textStatus, xhr) { load_data(); });
        });
      });
	});

   function reject_datachange()
  {
        var sdata = $('#myform').serialize();
        var surl = baseUrl+'change_request/reject_datachange';
        $.post(surl, sdata, function(data, textStatus, xhr) {
           $.get(baseUrl+'change_request/send_rejectMail', function(xdata, textStatus, xhr) { load_data(); });
        });
  }

   function reject_temp_table(id,table)
  {
        var surl = baseUrl+'change_request/reject_temp_table';
        $.post(surl, {'id':id,'table':table}, function(data, textStatus, xhr) {
          $('#'+data).load(baseUrl+'change_request/load_temp_table/'+data, function(){
            $('#'+data+'_msg').html('data has been rejected');
             $.get(baseUrl+'change_request/send_rejectMail');
          });
      
        });
  }

	function load_data()
	{
    var id = <?=$this->input->get('id')?>;
		$('#main_container').load(baseUrl+'change_request/detail/?id='+id);
	}

  function approve_category(data)
  {
    var url = baseUrl+'change_request/approve_category'
    $.post(url, {'data': data}, function(data, textStatus, xhr) {
      $('#cat').load(baseUrl+'change_request/load_category',
        function(){
          $('#cat_msg').html('data has been approved');
          $.get(baseUrl+'change_request/send_approveMail');
      });
      
    });
  }

   function approve_contact(data)
  {
    var url = baseUrl+'change_request/approve_contact'
    $.post(url, {'data': data}, function(data, textStatus, xhr) {
      $('#contact').load(baseUrl+'change_request/load_contact',
        function(){
          $('#contact_msg').html('data has been approved');
          $.get(baseUrl+'change_request/send_approveMail');
      });
      
    });
  }

   function approve_owner(data)
  {
    var url = baseUrl+'change_request/approve_owner'
    $.post(url, {'data': data}, function(data, textStatus, xhr) {
      $('#owner').load(baseUrl+'change_request/load_owner',
        function(){
          $('#owner_msg').html('data has been approved');
          $.get(baseUrl+'change_request/send_approveMail');
      });
      
    });
  }

  function approve_org(data)
  {
    var url = baseUrl+'change_request/approve_org'
    $.post(url, {'data': data}, function(data, textStatus, xhr) {
      $('#org').load(baseUrl+'change_request/load_org',
        function(){
          $('#org_msg').html('data has been approved');
          $.get(baseUrl+'change_request/send_approveMail');
      });
      
    });
  }

  function approve_ref(data)
  {
    var url = baseUrl+'change_request/approve_ref'
    $.post(url, {'data': data}, function(data, textStatus, xhr) {
      $('#ref').load(baseUrl+'change_request/load_ref',
        function(){
          $('#ref_msg').html('data has been approved');
          $.get(baseUrl+'change_request/send_approveMail');
      });
      
    });
  }

  function approve_aff(data)
  {
    var url = baseUrl+'change_request/approve_aff'
    $.post(url, {'data': data}, function(data, textStatus, xhr) {
      $('#aff').load(baseUrl+'change_request/load_aff',
        function(){
          $('#aff_msg').html('data has been approved');
          $.get(baseUrl+'change_request/send_approveMail');
      });
      
    });
  }

  function approve_corresp(data)
  {
    var url = baseUrl+'change_request/approve_corresp'
    $.post(url, {'data': data}, function(data, textStatus, xhr) {
      $('#corresp').load(baseUrl+'change_request/load_corresp',
        function(){
          console.log('data corresp saved');
          $('#corresp_msg').html('data has been approved');
          $.get(baseUrl+'change_request/send_approveMail');
      });
      
    });
  }

</script>
<div id="main_container">

<div id="debug"></div>
<div class="page-header"><h3>Vendor <?php echo $title; ?></h3></div>

<div class="panel panel-default">
<div class="panel-heading"></div>
  <div class="panel-body">
     <div class="form-group">
      <label for="inputEmail3" class="col-xs-6 control-label">Supplier Name</label>
      <div class="col-xs-6">
        <label for="">: <?=$vendor->name?></label>
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail3" class="col-xs-6 control-label">Registration Code</label>
      <div class="col-xs-6">
        <label for="">: <?=$vendor->reg_num?></label>
      </div>
    </div>
  </div>
</div>
<a href="<?=base_url('change_request')?>" type="button" class="btn btn-default"> <span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span> back to list</a>
<?php echo form_open('change_request/approve',array('id'=>'myform','onsubmit'=>'return false')); ?>

<button class="btn btn-primary pull-right" type="submit">Approve</button>
<button class="btn btn-danger pull-right" onclick="reject_datachange()" type="button">Reject</button>

<?php echo $this->session->flashdata("message"); ?>
<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
        <tr>
        	<th><input type="checkbox" id="ck_all" value="1"></th>
            <th>No</th>
            <th>Group Data</th>
            <th>Data</th>
            <th>From</th>
            <th>To</th>
            <th>Change By</th>
            <th>Change Date</th>
            <th>Status</th>
        </tr>
	</thead>
    <tbody>
<?php
// echo form_hidden('total',count($browse));
if($browse){ $i=1;
	foreach($browse as $row){ $label = ($row->status == 'request') ? 'warning':'success'; ?>
			<tr>
        <td> <input type="checkbox" class="ck_ex" name="my_ck[]" id="ck<?=$i?>" value="<?=$row->id?>"></td>
				<td style="text-align: center;"><?php  echo $num = $num+1; ?></td>
        <td>
          <?= $this->m_change_request->getGroupdata($row->table) ?>
        </td>
        <td><?= $row->fieldname;  ?></td>
				<td><?= $this->m_change_request->getFieldName($row->table,$row->fieldname, $row->before) ?></td>
				<td><?= $this->m_change_request->getFieldName($row->table,$row->fieldname,$row->after) ?></td>
				<td><?= $row->username ?></td>
				<td><?= $row->created_date ?></td>
				<td> <span class="label label-<?=$label?>"><?= $row->status ?></span> </td>

			</tr>
<?php	$i++;  } ?>
<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php  } else { ?>
	<tr><td colspan="9" align="center">-</td></tr>
<?php } ?>
    </tbody>
</table>

<?php echo form_close();?>



<div class="bs-example">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">1. Category</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p><?php $this->load->view('change_request/data/categories'); ?></p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">2. Contact Person</a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php $this->load->view('change_request/data/contact'); ?></p>
                </div>
            </div>
        </div>
           
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">3. Owner</a>
                </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('change_request/data/owner'); ?></p>
                </div>
            </div>
        </div>
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">4. Pengurus</a>
                </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('change_request/data/org'); ?></p>
                </div>
            </div>
        </div>
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">5. Surat Legalitas</a>
                </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('change_request/data/correspondence'); ?></p>
                </div>
            </div>
        </div>
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">6. Referensi</a>
                </h4>
            </div>
            <div id="collapse6" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('change_request/data/reference'); ?></p>
                </div>
            </div>
        </div>
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">7. Afiliasi</a>
                </h4>
            </div>
            <div id="collapse7" class="panel-collapse collapse">
                <div class="panel-body">
                    <p><?php  $this->load->view('change_request/data/affiliate'); ?></p>
                </div>
            </div>
        </div>
    </div>
 
</div>



