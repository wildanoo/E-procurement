-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 19 Okt 2016 pada 06.00
-- Versi Server: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ga_eproc`
--

DELIMITER $$
--
-- Prosedur
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `test_multi_sets`()
    DETERMINISTIC
begin
        select user() as first_col;
        select user() as first_col, now() as second_col;
        select user() as first_col, now() as second_col, now() as third_col;
        end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `affiliates`
--

CREATE TABLE IF NOT EXISTS `affiliates` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `competency` varchar(50) NOT NULL,
  `contact` smallint(5) NOT NULL,
  `remark` varchar(100) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `affiliates`
--

INSERT INTO `affiliates` (`id`, `id_vendor`, `company_name`, `competency`, `contact`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 3, 'PT. Arif Systems Indonesia', 'IT', 0, 'Perusahaan IT', '1', 2, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(2, 5, 'PT. Angin Ribut', 'Hotel', 0, 'Test', '1', 2, '2016-09-09 18:58:56', '2016-09-09 18:58:56'),
(4, 18, ';EJR;QJW`;QIJET', 'FQJ;GIJQ', 32767, 'G;QEJG;', '1', 2, '2016-09-20 18:24:57', '2016-09-20 18:25:13'),
(5, 18, 'PT ADES', 'BAGUS', 32767, 'YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY', '1', 2, '2016-09-26 10:08:43', '2016-09-26 10:08:43'),
(6, 17, 'lkjhjkljlkj', 'klj', 0, 'lkj', '1', 2, '2016-09-30 11:16:20', '2016-09-30 11:16:28'),
(7, 17, 'klj', 'lkj', 0, 'lkj', '0', 19, '2016-09-30 11:20:13', '2016-09-30 11:21:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `akta`
--

CREATE TABLE IF NOT EXISTS `akta` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `notaris_name` varchar(50) NOT NULL,
  `notaris_address` varchar(100) NOT NULL,
  `notaris_phone` varchar(20) NOT NULL,
  `notaris_number` varchar(30) NOT NULL,
  `notaris_date` date NOT NULL,
  `remark` varchar(50) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `akta`
--

INSERT INTO `akta` (`id`, `id_vendor`, `notaris_name`, `notaris_address`, `notaris_phone`, `notaris_number`, `notaris_date`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 3, 'Sukanto, SH MKn', 'Ciputat', '0213344553', '32NH8983928', '2016-09-09', 'Akta pendirian', '1', 2, '2016-09-09 15:47:04', '2016-09-09 15:47:04'),
(3, 5, 'Amin SH', 'Bandung', '0226708234', '2020210', '2016-09-09', 'Test Akta', '1', 2, '2016-09-09 18:56:27', '2016-09-09 18:56:27'),
(4, 18, 'NoneFJNG', 'NoneEFLWE', 'NoneLFELE', 'NoneLEFJELK', '2026-09-17', 'NoneAEFAEFLJ', '1', 2, '2016-09-20 17:38:26', '2016-09-20 17:38:42'),
(5, 17, 'not name', 'not addr', 'not phone', '123456', '2016-10-12', 'erwr', '1', 19, '2016-09-30 11:15:10', '2016-10-03 09:21:10'),
(6, 22, 'NoneNoneNUNONI', 'NoneNUNONI', 'NoneNUNONI', 'NoneNUNONI', '2016-10-06', 'NoneNoneNUNONI', '1', 2, '2016-10-12 13:51:32', '2016-10-12 13:51:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `announce`
--

CREATE TABLE IF NOT EXISTS `announce` (
`id` mediumint(11) unsigned NOT NULL,
  `code` varchar(30) NOT NULL,
  `content_type` smallint(5) NOT NULL,
  `subcontent_type` smallint(5) NOT NULL DEFAULT '1',
  `id_cat` smallint(5) NOT NULL,
  `id_subcat` smallint(5) NOT NULL,
  `id_company` smallint(5) NOT NULL,
  `id_office` smallint(5) NOT NULL,
  `title_ind` varchar(100) NOT NULL,
  `title_eng` varchar(100) NOT NULL,
  `detail_eng` varchar(230) NOT NULL,
  `detail_ind` varchar(230) NOT NULL,
  `first_valdate` datetime NOT NULL,
  `end_valdate` datetime NOT NULL,
  `publish_date` datetime NOT NULL,
  `creator` varchar(30) NOT NULL,
  `file` varchar(15) NOT NULL,
  `status` enum('10','9','8','7','6','5','4','3','2','1','99') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `remarks_reject` text NOT NULL,
  `id_departement` smallint(5) NOT NULL,
  `id_region` smallint(5) NOT NULL,
  `counter` smallint(5) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data untuk tabel `announce`
--

INSERT INTO `announce` (`id`, `code`, `content_type`, `subcontent_type`, `id_cat`, `id_subcat`, `id_company`, `id_office`, `title_ind`, `title_eng`, `detail_eng`, `detail_ind`, `first_valdate`, `end_valdate`, `publish_date`, `creator`, `file`, `status`, `created_id`, `created_date`, `last_updated`, `remarks_reject`, `id_departement`, `id_region`, `counter`) VALUES
(1, 'EPROC-IBS/2016/10/DS/0055', 2, 3, 1, 2, 1, 2, 'Penawaran Terbuka', 'Open Bidding', 'Overview', 'Peninjauan', '2016-11-04 13:20:00', '0000-00-00 00:00:00', '2016-11-04 13:05:00', 'Staff_IBS', '041016001', '3', 12, '2016-10-13 13:29:38', '2016-10-13 13:29:38', '', 3, 1, 1),
(2, 'EPROC-IBS/2016/10/DS/0002', 3, 5, 3, 3, 1, 1, 'Invitation Direct Selection Indo', 'Invitation Direct Selection English', 'Overview English', 'Overview Indo', '2016-10-14 10:50:00', '2016-10-31 00:00:00', '2016-10-14 10:45:00', 'smibs', '061016002', '3', 3, '2016-10-13 14:28:08', '2016-10-13 14:28:16', '', 3, 1, 2),
(3, 'EPROC-IBS/2016/10/LB/0001', 3, 6, 3, 3, 1, 1, 'Limited Bidding Indonesia', 'Limited Bidding English', 'Overview English', 'Overview Indonesia', '2016-10-14 18:48:00', '2016-10-31 00:00:00', '2016-10-14 17:36:00', 'smibs', '061016003', '3', 3, '2016-10-14 14:45:34', '2016-10-14 14:46:02', '', 3, 1, 1),
(4, 'EPROC-IBS/2016/10/SO/0003', 2, 4, 1, 1, 1, 1, 'Announcement Open Sourching Indo', 'Announcement Open Sourching English', 'Overview English', 'Overview indo', '2016-11-10 09:59:00', '2016-11-12 09:59:00', '2016-11-10 09:58:00', 'smibs', '101016004', '2', 3, '2016-10-15 16:49:44', '2016-10-15 17:50:01', '', 3, 1, 3),
(5, 'EPROC-IBS/2016/10/SO/0002', 2, 4, 1, 1, 1, 1, 'gywsetg', 'tfgeawr', 'tgrfwe', 'twsxr', '2016-10-15 00:00:00', '2016-10-10 00:00:00', '2016-10-14 00:00:00', 'smibs', '101016005', '3', 3, '2016-10-13 14:27:16', '2016-10-13 14:27:37', '', 3, 1, 2),
(6, 'EPROC-IBS/2016/10/DS/0001', 3, 5, 2, 8, 1, 1, '', 'dad', 'fasxd', '', '2016-10-12 13:40:00', '2016-10-12 13:45:00', '2016-10-12 13:35:00', 'smibs', '121016006', '3', 3, '2016-10-12 13:31:46', '2016-10-12 13:33:10', '', 3, 1, 1),
(7, '', 3, 6, 1, 14, 1, 6, '', 'meh', 'meh', '', '2016-10-12 00:00:00', '2016-10-12 00:00:00', '2016-10-12 00:00:00', 'Staff_IBS', '121016007', '2', 12, '2016-10-12 22:07:39', '2016-10-12 22:07:39', '', 3, 1, 0),
(8, '', 1, 1, 0, 0, 1, 1, 'test news public', 'tes', 'Overview Test', 'Overview', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-10-15 10:25:00', 'Adminasyst', '131016008', '2', 14, '2016-10-14 09:07:06', '2016-10-14 09:07:06', '', 1, 1, 0),
(9, '', 3, 6, 1, 1, 1, 2, '', 'dfawd', 'fgwe', '', '2016-10-13 00:00:00', '2016-10-13 00:00:00', '2016-10-13 00:00:00', 'Staff_IBS', '131016009', '2', 12, '2016-10-13 13:46:54', '2016-10-13 13:46:54', '', 3, 1, 0),
(10, '', 3, 6, 1, 1, 1, 2, '', 'dfawd', 'fgwe', '', '2016-10-13 00:00:00', '2016-10-13 00:00:00', '2016-10-13 00:00:00', 'Staff_IBS', '131016010', '2', 12, '2016-10-13 14:23:27', '2016-10-13 14:23:27', '', 3, 1, 0),
(14, '', 1, 1, 0, 0, 1, 1, 'test', 'y34w5', 'w3qty', 'fgwfw', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-10-15 00:00:00', 'Staff_IBK', '141016011', '2', 15, '2016-10-14 14:12:53', '2016-10-14 14:12:53', ' cek ulaang', 1, 1, 0),
(15, 'EPROC-IBK/2016/10/NW/0001', 1, 1, 0, 0, 1, 1, 'Pelaksanaan e-Auction - Pengadaan Perusahaan Merchandise', ' Implementation of e-Auction - Procurement Company Merchandise', 'News Public', 'Berita', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-10-17 09:21:00', 'smibk', '171016012', '3', 4, '2016-10-17 09:19:27', '2016-10-17 09:20:24', '', 1, 1, 1),
(16, 'EPROC-IBK/2016/10/NW/0002', 1, 2, 0, 0, 1, 1, '', 'Prohibition of Accepting and Giving Gifts/Presents of Ied Fitri 1437H', 'News Blast', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-10-17 14:42:00', 'smibk', '171016013', '3', 4, '2016-10-17 14:40:17', '2016-10-17 14:40:45', '', 1, 1, 2),
(17, 'EPROC-IBS/2016/10/SO/0004', 2, 4, 1, 1, 1, 1, 'PROSES BISNIS OUTSOURCING TENDER PENGUMUMAN', 'BUSINESS PROCESS OUTSOURCING TENDER ANNOUNCEMENT', 'Open Sourcing', 'Sourcing', '2016-10-17 11:31:00', '2016-10-21 11:00:00', '2016-10-17 11:30:00', 'smibs', '171016014', '3', 3, '2016-10-17 11:28:49', '2016-10-17 11:28:56', '', 3, 1, 4),
(19, '', 2, 4, 1, 1, 1, 1, 'Sourcing 1 bahasa', 'ok', 'ok', 'Overview', '2016-10-18 11:17:00', '2016-10-17 00:00:00', '2016-10-17 11:15:00', 'Adminasyst', '171016016', '2', 14, '2016-10-17 11:09:21', '2016-10-17 11:09:21', '', 3, 1, 0),
(20, '', 2, 3, 1, 1, 1, 1, 'test 1 bahasa', 'ok', 'ok', 'Overview', '2016-10-18 11:50:00', '2016-10-31 00:00:00', '2016-10-18 11:48:00', 'Adminasyst', '171016017', '2', 14, '2016-10-17 11:43:20', '2016-10-17 11:43:20', '', 3, 1, 0),
(21, 'EPROC-IBS/2016/10/BD/0002', 2, 3, 3, 3, 1, 1, 'Bidding Terbuka', 'Open Bidding', 'Test Overview', 'Tes', '2016-10-17 11:58:00', '2016-10-31 00:00:00', '2016-10-17 11:57:00', 'smibs', '171016018', '3', 3, '2016-10-17 11:56:25', '2016-10-17 11:56:32', '', 3, 1, 2),
(22, '', 1, 1, 0, 0, 1, 1, 'tgw3', 't3w', 'tg', 'tgw3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-10-17 00:00:00', 'Staff_IBK', '171016018', '2', 15, '2016-10-17 13:45:01', '2016-10-17 13:45:01', '', 1, 1, 0),
(23, '', 1, 1, 0, 0, 1, 1, '1', '1', '1', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-10-17 00:00:00', 'Staff_IBK', '171016019', '2', 15, '2016-10-17 13:46:16', '2016-10-17 13:46:16', '', 1, 1, 0),
(24, 'EPROC-IBS/2016/10/DS/0003', 3, 5, 1, 1, 1, 1, '', '1', 'fws', '', '2016-10-17 14:02:00', '2016-10-31 00:00:00', '2016-10-17 14:00:00', 'smibs', '171016020', '3', 3, '2016-10-17 13:58:28', '2016-10-17 13:59:28', '', 3, 1, 3),
(25, '', 1, 1, 0, 0, 1, 2, 'vg', 'vg', 'gd', 'gv', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-10-17 00:00:00', 'Staff_IBK', '171016021', '2', 15, '2016-10-17 14:02:22', '2016-10-17 14:02:22', '', 1, 1, 0),
(26, '', 2, 4, 1, 1, 1, 1, 'gdsg', 'gdfsg', 'hegdrg', 'hgsdtfg', '2016-10-17 00:00:00', '2016-10-17 00:00:00', '2016-10-17 00:00:00', 'Staff_IBS', '171016022', '2', 12, '2016-10-17 14:44:32', '2016-10-17 14:44:32', '', 3, 1, 0),
(27, '', 2, 3, 1, 1, 1, 1, 'fewf', 'waef', 'fwed', 'fvesaf', '2016-10-17 00:00:00', '2016-10-17 00:00:00', '2016-10-17 00:00:00', 'Staff_IBS', '171016023', '2', 12, '2016-10-17 14:57:12', '2016-10-17 14:57:12', '', 3, 1, 0),
(28, '', 2, 3, 1, 1, 1, 1, 'test 1 bahasa', 'Ok', 'OK', 'overview', '2016-10-17 15:42:00', '2016-10-31 00:00:00', '2016-10-17 15:39:00', 'Adminasyst', '171016024', '2', 14, '2016-10-17 15:37:04', '2016-10-17 15:37:04', '', 3, 1, 0),
(29, 'EPROC-IBS/2016/10/BD/0003', 2, 3, 1, 1, 1, 1, 'test', 'english', 'english', 'overview', '2016-10-17 15:44:00', '2016-10-31 00:00:00', '2016-10-17 15:43:00', 'smibs', '171016025', '3', 3, '2016-10-17 15:41:24', '2016-10-17 15:42:05', '', 3, 1, 3),
(31, '', 2, 4, 1, 1, 1, 1, 'Sourcing 1 bahasa', 'OK', 'OK', 'Overview 1 bahasa', '2016-10-19 00:00:00', '2016-10-31 00:00:00', '2016-10-19 00:00:00', 'Adminasyst', '181016026', '2', 14, '2016-10-18 08:58:13', '2016-10-18 08:58:13', '', 3, 1, 0),
(32, '', 2, 3, 1, 1, 1, 1, '1 bahasa', 'OK', 'OK', '1 bahasa', '2016-10-19 00:00:00', '2016-10-31 00:00:00', '2016-10-19 00:00:00', 'Adminasyst', '181016027', '2', 14, '2016-10-18 08:57:55', '2016-10-18 08:57:55', '', 3, 1, 0),
(33, 'EPROC-IBS/2016/10/SO/0005', 2, 4, 1, 1, 1, 1, 'PROSES BISNIS OUTSOURCING TENDER PENGUMUMAN', 'BUSINESS PROCESS OUTSOURCING TENDER ANNOUNCEMENT', 'Overview', 'Overview', '2016-10-18 10:00:00', '2016-10-18 11:00:00', '2016-10-18 09:45:00', 'smibs', '181016028', '3', 3, '2016-10-18 09:35:53', '2016-10-18 09:36:43', '', 3, 1, 5),
(34, 'EPROC-IBS/2016/10/BD/0004', 2, 3, 1, 1, 1, 2, 'Pelaksanaan e-Auction - Pengadaan Perusahaan Merchandise', ' Implementation of e-Auction - Procurement Company Merchandise', 'Overview', 'Overview', '2016-10-18 09:55:00', '2016-10-18 11:00:00', '2016-10-18 09:50:00', 'smibs', '181016029', '3', 3, '2016-10-18 09:44:43', '2016-10-18 09:45:04', '', 3, 1, 4),
(35, '', 1, 1, 0, 0, 1, 1, 'rw', 'hetdgy', 'hyet', 'fgews', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-10-19 00:00:00', 'Adminasyst', '181016030', '2', 14, '2016-10-18 10:43:45', '2016-10-18 10:43:45', '', 1, 1, 0),
(36, '', 3, 6, 1, 1, 1, 1, '', 'fe', 'Overview', '', '2016-10-18 14:22:00', '2016-10-31 00:00:00', '2016-10-18 14:20:00', 'Staff_IBS', '181016031', '2', 12, '2016-10-18 14:23:54', '2016-10-18 14:23:54', '', 3, 1, 2),
(37, 'EPROC-IBS/2016/10/DS/0004', 3, 5, 1, 1, 1, 1, '', 'Test', 'Overview', '', '2016-10-18 14:46:00', '2016-10-31 00:00:00', '2016-10-18 14:45:00', 'smibs', '181016032', '3', 3, '2016-10-18 14:43:14', '2016-10-18 14:43:40', '', 3, 1, 4),
(39, 'EPROC-IBS/2016/10/DS/0005', 3, 5, 1, 14, 1, 2, '', 'sd', 'sdsd', '', '2016-09-18 00:00:00', '2016-11-18 00:00:00', '2016-10-20 00:00:00', 'smibs', '181016033', '3', 3, '2016-10-18 22:57:59', '2016-10-18 23:40:03', '', 3, 1, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `announcement_level_approval`
--

CREATE TABLE IF NOT EXISTS `announcement_level_approval` (
`id` smallint(5) unsigned NOT NULL,
  `id_announcement` smallint(5) NOT NULL,
  `id_level` smallint(5) NOT NULL,
  `status` enum('5','4','3','2','1') NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data untuk tabel `announcement_level_approval`
--

INSERT INTO `announcement_level_approval` (`id`, `id_announcement`, `id_level`, `status`, `created_date`) VALUES
(1, 1, 94, '2', '2016-10-04 11:30:25'),
(2, 2, 94, '2', '2016-10-06 10:06:49'),
(3, 3, 94, '2', '2016-10-06 13:32:31'),
(4, 4, 95, '2', '2016-10-10 09:36:18'),
(5, 5, 95, '2', '2016-10-10 10:41:13'),
(6, 6, 94, '2', '2016-10-12 13:31:46'),
(7, 7, 94, '2', '2016-10-12 22:07:39'),
(8, 8, 84, '2', '2016-10-13 10:18:15'),
(9, 11, 94, '2', '2016-10-13 14:14:50'),
(10, 12, 94, '2', '2016-10-14 11:28:05'),
(11, 13, 94, '2', '2016-10-14 11:28:50'),
(12, 14, 84, '2', '2016-10-14 14:03:29'),
(13, 15, 84, '2', '2016-10-17 09:08:05'),
(14, 16, 84, '2', '2016-10-17 09:16:20'),
(15, 17, 95, '2', '2016-10-17 09:33:11'),
(16, 18, 84, '2', '2016-10-17 10:57:35'),
(17, 19, 95, '2', '2016-10-17 11:05:34'),
(18, 20, 94, '2', '2016-10-17 11:32:50'),
(19, 21, 94, '2', '2016-10-17 11:55:12'),
(20, 22, 84, '2', '2016-10-17 13:45:01'),
(21, 23, 84, '2', '2016-10-17 13:46:16'),
(22, 24, 94, '2', '2016-10-17 13:58:28'),
(23, 25, 84, '2', '2016-10-17 14:02:22'),
(24, 26, 95, '2', '2016-10-17 14:44:33'),
(25, 27, 94, '2', '2016-10-17 14:57:12'),
(26, 28, 94, '2', '2016-10-17 15:34:59'),
(27, 29, 94, '2', '2016-10-17 15:41:24'),
(28, 30, 84, '2', '2016-10-18 08:45:38'),
(29, 31, 95, '2', '2016-10-18 08:55:18'),
(30, 32, 94, '2', '2016-10-18 08:56:52'),
(31, 33, 95, '2', '2016-10-18 09:35:53'),
(32, 34, 94, '2', '2016-10-18 09:44:43'),
(33, 35, 84, '2', '2016-10-18 10:42:53'),
(34, 36, 94, '2', '2016-10-18 14:18:00'),
(35, 37, 94, '2', '2016-10-18 14:43:14'),
(36, 38, 94, '2', '2016-10-18 21:38:45'),
(37, 39, 94, '2', '2016-10-18 22:40:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `announce_join_participant`
--

CREATE TABLE IF NOT EXISTS `announce_join_participant` (
`id` int(10) unsigned NOT NULL,
  `id_announce` smallint(5) NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_subcontent` smallint(5) NOT NULL,
  `status` enum('','0','1','2','3','4','5','6','80','81','82','83','84','90','91') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '0=>rejected;\n1=>allowed;\n2=>winner unconfirmed;\n3=>winner;\n4=>reject unconfirmed winner;\n80=>joined direct selection;\n81=>reject direct selection;\n82=>waiting;\n83=>no response;\n90=>not yet responded;\n91=>vendor registered/joined;',
  `created_id` smallint(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=169 ;

--
-- Dumping data untuk tabel `announce_join_participant`
--

INSERT INTO `announce_join_participant` (`id`, `id_announce`, `id_vendor`, `id_subcontent`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(8, 1, 62, 3, '1', 0, '2016-10-07 06:08:59', '2016-10-07 06:08:59'),
(9, 4, 63, 4, '1', 0, '2016-10-10 02:47:44', '2016-10-10 02:47:44'),
(10, 4, 64, 4, '1', 0, '2016-10-10 03:03:52', '2016-10-10 03:03:52'),
(20, 1, 68, 3, '1', 0, '2016-10-12 01:34:25', '2016-10-12 01:34:25'),
(25, 6, 2, 5, '82', 12, '2016-10-12 06:31:46', '2016-10-12 06:31:46'),
(26, 7, 2, 6, '82', 12, '2016-10-12 15:07:39', '2016-10-12 15:07:39'),
(27, 7, 69, 6, '82', 12, '2016-10-12 15:07:39', '2016-10-12 15:07:39'),
(72, 10, 26, 6, '82', 12, '2016-10-13 07:23:27', '2016-10-13 07:23:27'),
(79, 2, 28, 5, '6', 12, '2016-10-13 07:28:08', '2016-10-13 07:28:08'),
(137, 3, 28, 6, '82', 12, '2016-10-14 07:45:34', '2016-10-14 07:45:34'),
(138, 3, 100, 6, '82', 12, '2016-10-14 07:45:34', '2016-10-14 07:45:34'),
(141, 17, 102, 4, '1', 0, '2016-10-17 02:53:59', '2016-10-17 02:53:59'),
(143, 17, 103, 4, '1', 0, '2016-10-17 04:33:28', '2016-10-17 04:33:28'),
(144, 21, 28, 3, '1', 30, '2016-10-17 04:59:20', '2016-10-17 06:47:00'),
(145, 21, 104, 3, '1', 0, '2016-10-17 05:02:39', '2016-10-17 05:02:39'),
(146, 24, 28, 5, '82', 12, '2016-10-17 06:58:28', '2016-10-17 06:58:28'),
(149, 16, 28, 2, '82', 15, '2016-10-17 07:40:17', '2016-10-17 07:40:17'),
(150, 17, 105, 4, '1', 0, '2016-10-17 07:46:11', '2016-10-17 07:46:11'),
(151, 29, 28, 3, '1', 30, '2016-10-17 08:51:12', '2016-10-17 08:53:03'),
(152, 34, 28, 3, '1', 30, '2016-10-18 02:57:59', '2016-10-18 02:59:52'),
(153, 33, 106, 4, '1', 0, '2016-10-18 03:07:13', '2016-10-18 03:07:13'),
(154, 33, 107, 4, '1', 0, '2016-10-18 03:56:21', '2016-10-18 03:56:21'),
(157, 36, 101, 6, '82', 12, '2016-10-18 07:23:54', '2016-10-18 07:23:54'),
(158, 36, 109, 6, '82', 12, '2016-10-18 07:23:54', '2016-10-18 07:23:54'),
(159, 37, 101, 5, '6', 12, '2016-10-18 07:43:14', '2016-10-18 07:43:14'),
(165, 39, 110, 5, '6', 12, '2016-10-18 15:57:59', '2016-10-18 15:57:59'),
(166, 39, 113, 5, '6', 12, '2016-10-18 15:57:59', '2016-10-18 15:57:59'),
(167, 39, 26, 5, '6', 12, '2016-10-18 15:57:59', '2016-10-18 15:57:59'),
(168, 39, 73, 5, '6', 12, '2016-10-18 15:57:59', '2016-10-18 15:57:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `announce_newsblast_participant`
--

CREATE TABLE IF NOT EXISTS `announce_newsblast_participant` (
`id` int(10) unsigned NOT NULL,
  `id_announce` smallint(5) NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_subcontent` smallint(5) NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `announce_news_detail`
--

CREATE TABLE IF NOT EXISTS `announce_news_detail` (
`id` smallint(5) NOT NULL,
  `id_announce` smallint(5) NOT NULL,
  `id_subcontent` smallint(5) NOT NULL,
  `id_content` smallint(5) DEFAULT NULL,
  `status` enum('1','0') DEFAULT '1',
  `created_id` smallint(5) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data untuk tabel `announce_news_detail`
--

INSERT INTO `announce_news_detail` (`id`, `id_announce`, `id_subcontent`, `id_content`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(14, 77, 0, NULL, '1', 1, '2016-07-14 20:34:47', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `approval_level`
--

CREATE TABLE IF NOT EXISTS `approval_level` (
`id` smallint(5) unsigned NOT NULL,
  `id_group` smallint(5) NOT NULL,
  `level` enum('3','2','1') NOT NULL DEFAULT '1',
  `remark` varchar(30) DEFAULT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `approval_level`
--

INSERT INTO `approval_level` (`id`, `id_group`, `level`, `remark`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 5, '1', 'SM IBS', 1, '2016-06-02 22:03:24', '2016-06-02 22:03:24'),
(2, 4, '2', 'SM IBK', 1, '2016-06-02 22:03:24', '2016-06-02 22:03:24'),
(3, 7, '3', 'VP IB', 1, '2016-06-02 22:03:24', '2016-06-02 22:03:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `avl_changedata`
--

CREATE TABLE IF NOT EXISTS `avl_changedata` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `sess_name` smallint(5) DEFAULT NULL,
  `table` varchar(30) DEFAULT NULL,
  `index` smallint(5) DEFAULT NULL COMMENT 'id table',
  `fieldname` varchar(30) DEFAULT NULL,
  `before` varchar(50) DEFAULT '',
  `after` varchar(50) DEFAULT NULL,
  `status` enum('wait','request','request2','approved','rejected') NOT NULL DEFAULT 'approved',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=151 ;

--
-- Dumping data untuk tabel `avl_changedata`
--

INSERT INTO `avl_changedata` (`id`, `id_vendor`, `sess_name`, `table`, `index`, `fieldname`, `before`, `after`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(107, 17, 1, 'vendor', NULL, 'postcode', NULL, '7879', 'rejected', 19, '2016-09-28 20:45:33', '2016-09-28 20:45:33'),
(108, 17, 1, 'vendor', NULL, 'npwp_address', NULL, 'jakarta', 'approved', 19, '2016-09-28 20:45:33', '2016-09-28 20:45:33'),
(109, 17, 1, 'vendor', NULL, 'npwp_postcode', NULL, '89798', 'rejected', 19, '2016-09-28 20:45:33', '2016-09-28 20:45:33'),
(110, 17, 2, 'vendor', NULL, 'phone', '908989089', '90898908', 'approved', 19, '2016-09-30 11:19:10', '2016-09-30 11:19:10'),
(111, 17, 2, 'vendor', NULL, 'fax', '789789788', '78978978', 'approved', 19, '2016-09-30 11:19:10', '2016-09-30 11:19:10'),
(112, 17, 2, 'vendor', NULL, 'web', 'testing.com', 'www.testing.com', 'approved', 19, '2016-09-30 11:19:10', '2016-09-30 11:19:10'),
(113, 17, 2, 'bank_account', NULL, 'acc_address', '', 'acc addr', 'approved', 19, '2016-09-30 11:19:37', '2016-09-30 11:19:37'),
(114, 17, 2, 'akta', NULL, 'notaris_phone', '', 'not phone', 'approved', 19, '2016-09-30 11:19:44', '2016-09-30 11:19:44'),
(115, 17, 3, 'vendor', NULL, 'npwp', '7889789789789', '78897897897', 'approved', 19, '2016-09-30 13:47:58', '2016-09-30 13:47:58'),
(116, 17, 3, 'vendor', NULL, 'npwp_postcode', NULL, '2342342', 'approved', 19, '2016-09-30 13:47:58', '2016-09-30 13:47:58'),
(117, 17, 3, 'vendor', NULL, 'web', 'www.testing.com', 'www.erer.com', 'approved', 19, '2016-09-30 13:47:58', '2016-09-30 13:47:58'),
(118, 17, 3, 'bank_account', NULL, 'bank_name', NULL, 'rewr', 'approved', 19, '2016-09-30 13:48:23', '2016-09-30 13:48:23'),
(119, 17, 3, 'bank_account', NULL, 'branch_name', '', 'werw', 'rejected', 19, '2016-09-30 13:48:23', '2016-09-30 13:48:23'),
(120, 17, 3, 'akta', NULL, 'remark', NULL, 'erwr', 'approved', 19, '2016-09-30 13:48:27', '2016-09-30 13:48:27'),
(121, 17, 4, 'vendor', NULL, 'postcode', '78798908', '123456', 'approved', 19, '2016-10-03 09:17:12', '2016-10-03 09:17:12'),
(122, 17, 4, 'vendor', NULL, 'npwp', '78897897', '123456', 'approved', 19, '2016-10-03 09:17:12', '2016-10-03 09:17:12'),
(123, 17, 4, 'vendor', NULL, 'npwp_address', 'Jakaruta', 'Jakarta', 'approved', 19, '2016-10-03 09:17:12', '2016-10-03 09:17:12'),
(124, 17, 4, 'vendor', NULL, 'npwp_postcode', 'jkljkkj', '123456', 'approved', 19, '2016-10-03 09:17:12', '2016-10-03 09:17:12'),
(125, 17, 5, 'bank_account', NULL, 'acc_address', 'acc addr', 'Jakarta', 'rejected', 19, '2016-10-03 09:19:46', '2016-10-03 09:19:46'),
(126, 17, 5, 'bank_account', NULL, 'bank_name', 'rewr', 'BCA', 'rejected', 19, '2016-10-03 09:19:46', '2016-10-03 09:19:46'),
(127, 17, 5, 'bank_account', NULL, 'branch_name', '', 'Tebet', 'rejected', 19, '2016-10-03 09:19:46', '2016-10-03 09:19:46'),
(128, 17, 5, 'akta', NULL, 'notaris_number', '', '123456', 'approved', 19, '2016-10-03 09:20:06', '2016-10-03 09:20:06'),
(129, 17, 5, 'akta', NULL, 'notaris_date', '0000-00-00', '2016-10-12', 'approved', 19, '2016-10-03 09:20:06', '2016-10-03 09:20:06'),
(130, 17, 5, 'akta', NULL, 'remark', 'erwr', 'Remark', 'rejected', 19, '2016-10-03 09:20:06', '2016-10-03 09:20:06'),
(131, 73, 6, 'vendor', NULL, 'vendor_name', 'PT Edit VENDOR BY VENDOR ITU SENDIRI', 'PT VENDOR NYA MAU NGEDIT PROFILE SENDIRI', 'approved', 36, '2016-10-13 14:45:53', '2016-10-13 14:45:53'),
(132, 73, 6, 'vendor', NULL, 'vendor_address', 'JAYA', 'JALAN PEMBANGUNAN JAYA 1', 'approved', 36, '2016-10-13 14:45:53', '2016-10-13 14:45:53'),
(133, 73, 6, 'vendor', NULL, 'postcode', NULL, '15060', 'approved', 36, '2016-10-13 14:45:53', '2016-10-13 14:45:53'),
(134, 73, 6, 'vendor', NULL, 'npwp_address', NULL, 'PAMULANG JAYA', 'approved', 36, '2016-10-13 14:45:53', '2016-10-13 14:45:53'),
(135, 73, 6, 'vendor', NULL, 'npwp_postcode', NULL, '15417', 'approved', 36, '2016-10-13 14:45:53', '2016-10-13 14:45:53'),
(136, 73, 6, 'vendor', NULL, 'web', NULL, 'GARUDA-INDONESIA.COM', 'approved', 36, '2016-10-13 14:45:53', '2016-10-13 14:45:53'),
(137, 73, 6, 'vendor_category', 157, 'id_category', '1', '5', 'approved', 36, '2016-10-13 15:30:34', '2016-10-13 15:30:34'),
(138, 73, 6, 'vendor_category', 157, 'id_subcat', '14', '5', 'approved', 36, '2016-10-13 15:30:34', '2016-10-13 15:30:34'),
(139, 73, 6, 'vendor_category', 156, 'id_category', '1', '5', 'approved', 36, '2016-10-13 15:30:48', '2016-10-13 15:30:48'),
(140, 73, 6, 'vendor_category', 156, 'id_subcat', '15', '6', 'approved', 36, '2016-10-13 15:30:48', '2016-10-13 15:30:48'),
(141, 73, 6, 'vendor_category', 155, 'id_category', '1', '6', 'approved', 36, '2016-10-13 15:30:58', '2016-10-13 15:30:58'),
(142, 73, 6, 'vendor_category', 155, 'id_subcat', '1', '4', 'approved', 36, '2016-10-13 15:30:58', '2016-10-13 15:30:58'),
(143, 73, 6, 'contact_person', 69, 'position', '-', 'SENIOR TESTER', 'approved', 36, '2016-10-13 15:31:49', '2016-10-13 15:31:49'),
(144, 73, 6, 'bank_account', NULL, 'bank_name', 'MARILAH SEJENAK KITA MENUNDUKKAN KEPALA KITA AGAR ', 'MARI DI EDIT', 'approved', 36, '2016-10-13 15:32:04', '2016-10-13 15:32:04'),
(145, 73, 6, 'bank_account', NULL, 'branch_name', 'JANGAN LUPA NANTI PULANG NYA LEWAT JALAN SUDIRMAN', 'DI EDIT DULU', 'approved', 36, '2016-10-13 15:32:04', '2016-10-13 15:32:04'),
(146, 73, 8, 'akta', NULL, 'notaris_name', 'none', 'NoneEFJQI', 'request', 36, '2016-10-13 15:52:52', '2016-10-13 15:52:52'),
(147, 73, 8, 'akta', NULL, 'notaris_address', 'none', 'PUQPUPQU', 'request', 36, '2016-10-13 15:52:52', '2016-10-13 15:52:52'),
(148, 73, 8, 'akta', NULL, 'notaris_phone', 'none', 'PIJQPIUJGPQI', 'request', 36, '2016-10-13 15:52:52', '2016-10-13 15:52:52'),
(149, 73, 8, 'akta', NULL, 'notaris_number', 'none', 'JQPIJGQ', 'request', 36, '2016-10-13 15:52:52', '2016-10-13 15:52:52'),
(150, 73, 8, 'akta', NULL, 'notaris_date', '2016-10-13', '2016-10-11', 'request', 36, '2016-10-13 15:52:52', '2016-10-13 15:52:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bank_account`
--

CREATE TABLE IF NOT EXISTS `bank_account` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `acc_number` varchar(50) NOT NULL,
  `acc_name` varchar(50) NOT NULL,
  `acc_address` varchar(100) NOT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `branch_name` varchar(50) NOT NULL,
  `acc_country` varchar(50) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data untuk tabel `bank_account`
--

INSERT INTO `bank_account` (`id`, `id_vendor`, `acc_number`, `acc_name`, `acc_address`, `bank_name`, `branch_name`, `acc_country`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 2, '123456789', 'Testing', 'Depok', 'BCA', 'BCA Cinere', 'Indonesia', '1', 2, '2016-09-08 14:59:06', '2016-09-08 14:59:06'),
(3, 5, '00102340505', 'Amin', 'Jl Jend Sudirman Kav. 9 Jakarta', 'BCA', 'Jakarta', 'Indonesia', '1', 2, '2016-09-09 19:20:26', '2016-09-09 19:20:26'),
(4, 6, '13098734547989', 'wisnu arimurti', 'fhakfjh', 'PT Bank Mandiri', 'Sudirman', 'Indonesia', '1', 2, '2016-09-14 20:23:43', '2016-09-14 20:23:43'),
(5, 26, 'Nonejtjg', 'Nonejtjetjh', 'Noneetjjetj', 'Nonetejetj', 'Nonetjetjet', NULL, '0', 2, '2016-09-21 11:37:34', '2016-09-21 11:37:34'),
(6, 17, 'acc number', 'acc name', 'acc addr', 'rewr', '', NULL, '1', 19, '2016-09-30 11:14:50', '2016-09-30 14:37:15'),
(9, 54, 'jwfliwjelfi', 'fnwljeflwi', 'fljwhlj', 'mandir', 'fhqljnef', 'fljwnlg', '1', 0, '2016-10-05 15:38:07', '2016-10-05 15:38:07'),
(10, 44, '34634637', 'Vendor Redlist', 'Jl. Markoni 1 No. 26, KOMP.KS', 'Mandiri', 'Tangerang Selatan', 'Indonesia', '1', 0, '2016-10-05 11:06:55', '2016-10-05 11:06:55'),
(11, 61, '13098734547989', 'wisnu arimurti', 'jaya', 'BNI', 'Sudirman', 'Indonesia', '1', 0, '2016-10-07 14:26:28', '2016-10-07 14:26:28'),
(12, 22, 'NoneNUNONI', 'NoneNUNONI', 'NoneNUNONI', 'NoneNUNONI', 'NoneNUNONI', NULL, '1', 2, '2016-10-12 13:51:32', '2016-10-12 13:51:39'),
(13, 0, 'jhjk', 'hjkh', 'jkhj', 'h', 'jkhk', 'jkh', '1', 0, '2016-10-13 11:34:22', '2016-10-13 11:34:22'),
(14, 73, '13098734547989', 'PT Testing', 'safa', 'PT Bank Mandiri', 'sudirman', 'Indonesia', '1', 0, '2016-10-14 13:51:16', '2016-10-14 13:51:16'),
(15, 101, '235276', 'rioqurh', 'hjweuht', 'madniri', 'sudireman', 'jtlqh', '1', 0, '2016-10-17 09:23:35', '2016-10-17 09:23:35'),
(16, 113, '12321423', 'Active', 'Disini', 'Mandiri', '2134152351', 'Indonesia', '1', 0, '2016-10-18 20:33:31', '2016-10-18 20:33:31'),
(17, 110, '5635231', 'Blacklist', 'Makassar', 'BCA', 'Sultan Salahuddin', 'Indonesia', '1', 6, '2016-10-18 21:01:16', '2016-10-18 21:01:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bank_account_mail`
--

CREATE TABLE IF NOT EXISTS `bank_account_mail` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `count` smallint(1) NOT NULL DEFAULT '1',
  `expired_date` varchar(35) NOT NULL DEFAULT 'CURRENT_TIMESTAMP',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `behavior`
--

CREATE TABLE IF NOT EXISTS `behavior` (
`id` smallint(5) NOT NULL,
  `behavior` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `behavior`
--

INSERT INTO `behavior` (`id`, `behavior`, `created_date`) VALUES
(1, 'News', '2016-06-07 00:00:00'),
(2, 'Bidding', '2016-06-07 00:00:00'),
(3, 'Sourcing', '2016-06-07 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`id` smallint(5) unsigned NOT NULL,
  `init_code` varchar(20) NOT NULL,
  `category` varchar(30) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`id`, `init_code`, `category`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, '011', 'Hotel & Motel', '', '1', 1, '2016-06-10 04:53:38', '2016-06-10 04:53:38'),
(2, '012', 'Packaging & Shipping', '', '1', 1, '2016-06-24 04:57:32', '2016-06-24 04:57:32'),
(3, '013', 'Desserts, Catering & Supplies', '', '1', 1, '2016-06-24 08:37:46', '2016-06-24 08:37:46'),
(4, '014', 'Moving & Storage', '', '1', 1, '2016-06-24 08:39:38', '2016-06-24 08:39:38'),
(5, '015', 'Travel & Tourism', '', '1', 1, '2016-06-24 08:39:59', '2016-06-24 08:39:59'),
(6, '016', 'Fast Food & Carry Out', 'description', '1', 1, '2016-06-24 08:40:31', '2016-09-01 13:30:19'),
(7, '017', 'Category2907', 'category', '1', 17, '2016-07-29 05:05:33', '2016-09-01 13:29:55'),
(8, '018', 'test', 'description', '1', 17, '2016-07-29 05:09:04', '2016-09-02 11:10:30'),
(10, '019', 'New Category', 'Description of', '0', 1, '2016-10-14 10:40:43', '2016-10-14 10:46:23'),
(11, '010', 'Tesssss', 'Tes Kateg', '0', 1, '2016-10-14 11:28:38', '2016-10-14 11:29:07'),
(12, '011', 'Teeeeeess', 'Teseu', '0', 1, '2016-10-14 13:13:55', '2016-10-14 13:18:59'),
(13, '012', 'Coba', 'Tes Kategori', '1', 1, '2016-10-14 13:19:45', '2016-10-14 13:19:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('016a01c48408f41b75fc9b74e19e05ba', '172.25.138.217', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36', 1476848513, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:3:"eng";}'),
('0ca540a4637dfed8b822cdbbe8b779a4', '172.25.138.160', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36', 1476845984, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:3:"eng";}'),
('23b041a9d359fa7b40d3d7a1ba575a8c', '172.25.138.156', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:42.0) Gecko/20100101 Firefox/42.0', 1476847005, 'a:16:{s:9:"user_data";s:0:"";s:4:"lang";s:3:"eng";s:15:"cek_vendor_auth";s:2:"17";s:7:"user_id";s:2:"19";s:8:"username";s:5:"ririn";s:8:"is_admin";b:0;s:11:"permissions";s:21:"update-vendor-data:18";s:10:"user_group";s:1:"8";s:7:"id_dept";N;s:11:"code_groups";s:10:"vendor_avl";s:6:"status";s:1:"1";s:5:"venID";s:2:"17";s:6:"is_avl";b:1;s:11:"sess_change";i:9;s:7:"tb_name";s:10:"affiliates";s:3:"tab";s:5:"menu2";}'),
('29a90a5c67f66ca7b1d2dd74422b6e14', '172.25.138.200', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0', 1476846585, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:3:"eng";}'),
('61ffc85c575278a8e9dc7a146855ed02', '172.25.206.30', 'WWW-Mechanize/1.0.0 (http://rubyforge.org/projects/mechanize/)', 1476842645, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:3:"eng";}'),
('787ad3e63b5137a2232e40aa09325952', '172.25.139.57', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36', 1476848289, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:3:"eng";}'),
('9a0f4eb4303bfca99d53f86b82f30053', '172.25.139.91', 'Mozilla/5.0 (Windows NT 6.1; rv:43.0) Gecko/20100101 Firefox/43.0', 1476846212, 'a:18:{s:9:"user_data";s:0:"";s:4:"lang";s:3:"eng";s:7:"user_id";s:1:"3";s:8:"username";s:5:"smibs";s:8:"is_admin";b:0;s:11:"permissions";s:523:"approve/-reject-content-:13,red-list-vendor-approval:15,manage-redlist:56,black-list-vendor-approval:16,view-detail-vendor-black-list:64,reject-black-list:67,manage-black-list:70,approval-change-data-vendor-request-byavl:133,approval-change-data-vendor-request-byadmsrc:134,view-inactive/active:59,inactive/active-sm-ibs:61,view-vendor-list:71,vendor-list-export-excel:72,approve-vendor-registration-1:76,cetak-berita-acara-evaluasi-sourcing:78,view-data -due-diligence:90,view-vendor-registration:92,view-vendor-delete:125";s:10:"user_group";s:1:"5";s:7:"id_dept";N;s:11:"code_groups";s:6:"sm_ibs";s:6:"status";s:1:"1";s:7:"feature";s:8:"inactive";s:2:"id";s:0:"";s:11:"search_term";s:0:"";s:10:"start_date";s:0:"";s:8:"end_date";s:0:"";s:5:"inaID";s:0:"";s:5:"venID";s:2:"22";s:6:"statID";s:8:"inactive";}'),
('b9b3c3cf11a323b6bfd31850fbed6e59', '172.25.206.30', 'WWW-Mechanize/1.0.0 (http://rubyforge.org/projects/mechanize/)', 1476846229, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:3:"eng";}'),
('bf874daaa378b898e51b97b9eca471c5', '172.25.206.30', 'WWW-Mechanize/1.0.0 (http://rubyforge.org/projects/mechanize/)', 1476846229, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:3:"eng";}'),
('bfd3c2b4b30da659387d929f1d9b3fc5', '172.25.206.30', 'WWW-Mechanize/1.0.0 (http://rubyforge.org/projects/mechanize/)', 1476842645, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:3:"eng";}'),
('dd3a21d0f6ea28f8b62c8c1d72919a7c', '172.25.138.94', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36', 1476848806, 'a:10:{s:9:"user_data";s:0:"";s:4:"lang";s:3:"eng";s:7:"user_id";s:1:"1";s:8:"username";s:5:"admin";s:8:"is_admin";b:1;s:11:"permissions";s:1161:"red-list-vendor-approval:15,add-vendor-to-red-list:23,manage-redlist:56,edit-data-vendor-redlist:57,cancel-vendor-redlist:58,black-list-vendor-approval:16,add-vendor-to-black-list:22,view-detail-vendor-black-list:64,edit-black-list:65,cancel-black-list:66,reject-black-list:67,view-black-list-on-level-sm-ibk:68,view-black-list-on-level-sm-vpib:69,manage-black-list:70,manage-company:41,manage-area:108,manage-office:109,manage-division:110,manage-department:111,manage-position:112,manage-employee:137,manage-functional:138,manage-user:42,manage-group:113,manage-permission-group:114,manage-permission:115,manage-category:43,manage-status-registration:45,manage-subcategory:116,due-diligence-assesment:118,list-document-evaluation:119,manage-home:44,manage-about-us:120,manage-contact:121,manage-faq:122,manage-vendor-policy:123,manage-terms-and-conditions:124,manage-content-type:46,manage-sub-content-type:47,manage-content-level:117,view-inactive/active:59,inactive/active-sm-ibk:60,inactive/active-sm-ibs:61,inactive/active-vp-ib:63,view-vendor-list:71,vendor-list-button-admin-sourcing:73,view-vendor-document:135,view-manage-menu:139,view-promote-avl:144";s:10:"user_group";s:2:"11";s:7:"id_dept";N;s:11:"code_groups";s:0:"";s:6:"status";s:1:"1";}'),
('e7f0d2f603e8f512f83b9611de11f21e', '172.25.138.200', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0', 1476848320, 'a:20:{s:9:"user_data";s:0:"";s:4:"lang";s:3:"eng";s:7:"user_id";s:2:"31";s:8:"username";s:10:"wildantest";s:8:"is_admin";b:0;s:11:"permissions";s:523:"approve/-reject-content-:13,red-list-vendor-approval:15,manage-redlist:56,black-list-vendor-approval:16,view-detail-vendor-black-list:64,reject-black-list:67,manage-black-list:70,approval-change-data-vendor-request-byavl:133,approval-change-data-vendor-request-byadmsrc:134,view-inactive/active:59,inactive/active-sm-ibs:61,view-vendor-list:71,vendor-list-export-excel:72,approve-vendor-registration-1:76,cetak-berita-acara-evaluasi-sourcing:78,view-data -due-diligence:90,view-vendor-registration:92,view-vendor-delete:125";s:10:"user_group";s:1:"5";s:7:"id_dept";N;s:11:"code_groups";s:2:"as";s:6:"status";s:1:"1";s:7:"feature";s:7:"redlist";s:4:"term";s:0:"";s:10:"start_date";s:0:"";s:8:"end_date";s:0:"";s:6:"id_cat";s:0:"";s:9:"logged_in";i:1;s:7:"blackID";s:1:"8";s:5:"idcat";s:0:"";s:6:"subcat";s:0:"";s:9:"sess_auth";s:0:"";}');

-- --------------------------------------------------------

--
-- Struktur dari tabel `company`
--

CREATE TABLE IF NOT EXISTS `company` (
`id` smallint(5) unsigned NOT NULL,
  `code` varchar(20) NOT NULL,
  `company_name` varchar(40) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `company`
--

INSERT INTO `company` (`id`, `code`, `company_name`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, '001', 'PT. Garuda Indonesia (Persero), Tbk.', '1', 1, '2016-04-04 09:40:35', '2016-04-04 09:40:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contact_person`
--

CREATE TABLE IF NOT EXISTS `contact_person` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=83 ;

--
-- Dumping data untuk tabel `contact_person`
--

INSERT INTO `contact_person` (`id`, `id_vendor`, `fullname`, `position`, `mobile`, `email`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 1, 'wisnu arimurti', '-', '081295057000', 'wsn.arimurti@gmail.com', '1', 0, '2016-09-07 17:01:14', '2016-09-07 17:01:14'),
(2, 2, 'Ririn', '-', '12321432', '', '1', 0, '2016-09-08 10:15:14', '2016-09-08 10:15:14'),
(3, 3, 'Ditya Firmansyah', '-', '0852643848803', 'ditya.firmansyah@gmail.com', '1', 0, '2016-09-09 15:42:11', '2016-09-09 15:42:11'),
(5, 5, 'Anjar', '-', '08119090160', 'anj4r.7070@gmail.com', '1', 0, '2016-09-09 18:53:29', '2016-09-09 18:53:29'),
(6, 6, 'wisnu arimurti', 'Tester', '081295057000', 'wsn.arimurti@gmail.com', '1', 0, '2016-09-14 10:35:23', '2016-09-14 13:15:52'),
(7, 7, 'ditya F', '-', '956563728', 'wisnu.arimurti@asyst.co.id', '', 0, '2016-09-15 15:35:05', '2016-09-15 15:35:05'),
(8, 8, 'wisnu', '-', '572387527', 'sfeah@eakgh.com', '', 0, '2016-09-15 15:52:01', '2016-09-15 15:52:01'),
(9, 9, 'gsg', '-', '235', 'gs@gs.com', '', 0, '2016-09-15 18:58:20', '2016-09-15 18:58:20'),
(10, 10, 'wisnu', '-', '081295057000', 'wsn.arimurti@gmail.com', '', 0, '2016-09-16 10:13:44', '2016-09-16 10:13:44'),
(11, 11, 'wisnu', '-', '358729', 'wsn.arimurti@gjamf.com', '', 0, '2016-09-16 10:36:42', '2016-09-16 10:36:42'),
(12, 12, 'Rahmat', '-', '0812345678', 'wabimantoro@gmail.com', '', 0, '2016-09-16 13:56:40', '2016-09-16 13:56:40'),
(13, 13, 'wisnu arimurti', '-', '423', 'sdajk@fadf.com', '', 0, '2016-09-16 14:23:32', '2016-09-16 14:23:32'),
(14, 14, 'Gmail Md', '-', '0812145125', 'sr.madusari@gmail.com', '', 0, '2016-09-19 16:36:22', '2016-09-19 16:36:22'),
(15, 15, 'kamal', '', '08217365389', 'sr.madusari@gmail.com', '1', 2, '2016-09-20 10:34:46', '2016-09-20 10:34:46'),
(16, 17, 'ririn', '-', '68768767868767868', 'sr.madusari@gmail.com', '', 0, '2016-09-20 13:27:30', '2016-09-20 13:27:30'),
(17, 18, 'haryo', '', '08127483479', 'haryo@gmail.com', '1', 2, '2016-09-20 14:19:32', '2016-09-20 18:56:55'),
(18, 18, 'WISNU', 'TSTER', '081295057000R', 'WSN.ARIMURTI@GMAIL.COM', '1', 2, '2016-09-20 17:32:52', '2016-09-20 17:32:52'),
(19, 19, 'WISNU', '-', '385173580', 'NFJAF@JLAKEJ.COOM', '', 0, '2016-09-20 18:59:07', '2016-09-20 18:59:07'),
(21, 21, 'RHFLJQH', '-', '4672', 'FALKJFL@EKFJA.LGUTW', '', 0, '2016-09-20 20:12:37', '2016-09-20 20:12:37'),
(22, 22, 'WISNU', 'TESTER OSE 3', '081295057000', 'WISNU.ARIMURTI@ASYST.CO.ID', '', 2, '2016-09-20 20:18:47', '2016-10-12 13:51:39'),
(23, 23, 'FLQNL', '-', '2789375', 'FJKQJEF@JKEQLFELF.FLWQEHJ', '', 0, '2016-09-20 20:20:34', '2016-09-20 20:20:34'),
(24, 24, 'FKW', '-', '7498523', 'DJQLF@FILQJIEL.EGW', '', 0, '2016-09-20 20:33:19', '2016-09-20 20:33:19'),
(26, 26, 'SGSGSJH`', '', '528528', '2562HFSNM@FAKGJ.KDGA', '1', 2, '2016-09-21 11:02:01', '2016-09-21 11:02:01'),
(27, 26, 'jmsj', 'srtjt', 'rsj', 'jtgj', '1', 2, '2016-09-21 11:37:13', '2016-09-21 11:37:13'),
(28, 27, 'Wisnu', '', '0987633334', 'wisnu@asyst.co.id', '1', 2, '2016-09-27 08:50:58', '2016-09-27 08:50:58'),
(29, 28, 'Muhammad Arif', '-', '098767658656', 'muhammadariv@gmail.com', '', 0, '2016-09-27 14:21:55', '2016-09-27 14:21:55'),
(30, 29, '1', '', '1', 'richo.m17@gmail.com', '1', 0, '2016-09-27 17:02:25', '2016-09-27 17:02:25'),
(31, 30, 'asasa', '', '1231', 'abc@abc.com', '1', 0, '2016-09-28 08:28:25', '2016-09-28 08:28:25'),
(32, 31, 'asasa', '', '1231', 'abc@abc.com', '1', 0, '2016-09-28 08:30:39', '2016-09-28 08:30:39'),
(33, 32, 'Ririn', '', '447878', 'zulandraririn@gmail.com', '1', 0, '2016-09-28 15:31:22', '2016-09-28 15:31:22'),
(34, 33, 'Ririn', '', '656896', 'zulandraririn@gmail.com', '1', 0, '2016-09-28 15:32:52', '2016-09-28 15:32:52'),
(35, 34, '1', '', '1', 'richo.m17@gmail.com', '1', 0, '2016-09-28 15:51:27', '2016-09-28 15:51:27'),
(36, 35, '1', '', '1', 'richo.m17@gmail.com', '1', 0, '2016-09-28 16:01:51', '2016-09-28 16:01:51'),
(37, 36, '1', '', '1', 'richo.m17@gmail.com', '1', 0, '2016-09-28 16:09:45', '2016-09-28 16:09:45'),
(38, 37, '2', '', '2', 'richo.m17@gmail.com', '1', 0, '2016-09-28 16:23:51', '2016-09-28 16:23:51'),
(39, 38, '33', '', '33', 'richo.m17@gmail.com', '1', 0, '2016-09-28 16:26:07', '2016-09-28 16:26:07'),
(40, 39, '3444', '', '081291117399', 'richo.m17@gmail.com', '1', 0, '2016-09-28 16:31:28', '2016-09-28 16:31:28'),
(41, 40, '77', '', '77', 'richo.m17@gmail.com', '1', 0, '2016-09-28 16:40:39', '2016-09-28 16:40:39'),
(42, 41, 'Rina', '', '02145545', 'zulandraririn@gmail.com', '1', 0, '2016-09-29 10:03:39', '2016-09-29 10:03:39'),
(43, 42, 'ririn', '-', '5544558', 'zulandraririn@gmail.com', '', 0, '2016-09-29 10:33:00', '2016-09-29 10:33:00'),
(47, 43, 'Redlist', '-', '08212452431', 'sr.madusari@gmail.com', '', 0, '2016-09-30 11:30:28', '2016-09-30 11:30:28'),
(48, 44, 'Madu', '', '082134524549', 'sr.madusari@gmail.com', '1', 2, '2016-09-30 11:46:07', '2016-09-30 11:46:07'),
(49, 45, 'Ririn', '', '32142342', 'zulandraririn@gmail.com', '1', 0, '2016-09-30 14:43:50', '2016-09-30 14:43:50'),
(50, 46, 'Ririn', '', '125442', 'zulandraririn@gmail.com', '1', 0, '2016-10-03 09:31:50', '2016-10-03 09:31:50'),
(51, 47, 'GHI', '', '42354235', 'zulandraririn@gmail.com', '1', 0, '2016-10-03 10:28:11', '2016-10-03 10:28:11'),
(52, 48, 'asasa', '', '2343', 'abc@abc.com', '1', 0, '2016-10-03 11:21:55', '2016-10-03 11:21:55'),
(53, 49, 'ririn', '', '4134', 'zulandraririn@gmail.com', '1', 0, '2016-10-03 11:35:21', '2016-10-03 11:35:21'),
(54, 50, 'Ririn', '', '412323', 'zulandraririn@gmail.com', '1', 0, '2016-10-03 11:35:59', '2016-10-03 11:35:59'),
(55, 51, 'dfgdsf', '', '2343', 'abc@abc.com', '1', 0, '2016-10-03 11:38:59', '2016-10-03 11:38:59'),
(56, 52, 'asasa', '', '1231', 'abc@abc.com', '1', 0, '2016-10-03 13:32:58', '2016-10-03 13:32:58'),
(57, 53, '1', '', '1231', 'abc@abc.com', '1', 0, '2016-10-03 13:35:31', '2016-10-03 13:35:31'),
(58, 54, 'Dita', '', '3265546415', 'zulandraririn@gmail.com', '1', 0, '2016-10-03 13:36:27', '2016-10-03 13:36:27'),
(59, 55, 'Ririn', '', '43254353', 'zulandraririn@gmail.com', '1', 0, '2016-10-03 14:47:33', '2016-10-03 14:47:33'),
(60, 56, 'WISNU', '-', '2857981359', 'FNKFBKQ@gbqagb.vnadljg', '', 0, '2016-10-06 08:10:37', '2016-10-06 08:10:37'),
(61, 61, 'Ahmad', '-', '0853 241 15542', 'ahmad@gmail.com', '', 0, '2016-10-06 14:02:53', '2016-10-06 14:02:53'),
(62, 62, '1', '', '1', '1@1.com', '1', 0, '2016-10-07 13:08:59', '2016-10-07 13:08:59'),
(63, 63, 'Ririn', '', '3924873', 'zulandraririn@gmail.com', '1', 0, '2016-10-10 09:47:44', '2016-10-10 09:47:44'),
(64, 64, 'jrfyguh', '', '4675', 'zulandraririn@gmail.com', '1', 0, '2016-10-10 10:03:52', '2016-10-10 10:03:52'),
(65, 65, 'Budi', '-', '0853 241 15542', 'budi@gmail.com', '', 0, '2016-10-10 15:11:40', '2016-10-10 15:11:40'),
(66, 66, 'dharma', '-', '76587432652', 'wisnu.arimurti@asyst.co.id', '', 0, '2016-10-11 15:27:46', '2016-10-11 15:27:46'),
(67, 68, 'Ririn', '', '4049382958', 'zulandraririn@gmail.com', '1', 0, '2016-10-12 08:34:25', '2016-10-12 08:34:25'),
(68, 22, 'ERWIN', 'hrd', '8723486289', 'JGAJHF@FAEKJF.COM', '1', 2, '2016-10-12 13:52:54', '2016-10-12 13:53:18'),
(69, 73, 'WISNU', 'SENIOR TESTER', '30985710385', 'WSN.ARIMURTI@GMAIL.COM', '', 36, '2016-10-13 13:33:21', '2016-10-13 16:01:46'),
(70, 73, 'ARIF ABDUL HAKIM', 'ANALIS', '018373801398', 'MUHAMMAD.HAKIM@ASYST.CO.ID', '1', 36, '2016-10-13 15:31:36', '2016-10-13 16:01:58'),
(71, 95, '1', '-', '1', '1@1.xom', '', 0, '2016-10-13 22:26:03', '2016-10-13 22:26:03'),
(72, 101, 'wisnu', '-', '89574', 'wisnu.arimurti@asyst.co.id', '', 0, '2016-10-17 09:08:21', '2016-10-17 09:08:21'),
(73, 102, 'Ririn', '', '3564765', 'zulandraririn@gmail.com', '1', 0, '2016-10-17 09:53:59', '2016-10-17 09:53:59'),
(74, 103, 'tgertg', '', '5365', 'zulandraririn@gmail.com', '1', 0, '2016-10-17 11:33:28', '2016-10-17 11:33:28'),
(75, 104, '64444445', '', '34567', 'zulandraririn@gmail.com', '1', 0, '2016-10-17 12:02:39', '2016-10-17 12:02:39'),
(76, 105, '532', '', '432531', 'tes@gmail.com', '1', 0, '2016-10-17 14:46:11', '2016-10-17 14:46:11'),
(77, 106, 'fwet', '', '5634', 'zulandraririn@gmail.com', '1', 0, '2016-10-18 10:07:13', '2016-10-18 10:07:13'),
(78, 107, 'h4etfg', '', '4523', 'zulandraririn@gmail.com', '1', 0, '2016-10-18 10:56:21', '2016-10-18 10:56:21'),
(79, 110, 'Mr. Black', '-', '052546632445', 'madusama@hotmail.com', '', 0, '2016-10-18 18:46:34', '2016-10-18 18:46:34'),
(80, 111, 'Mr. Reject', '-', '35236247', 'srmadusari@naver.com', '', 0, '2016-10-18 18:52:06', '2016-10-18 18:52:06'),
(81, 112, 'Mr. Short', '-', '1351346', 'sr.madusari@gmail.com', '', 0, '2016-10-18 18:54:04', '2016-10-18 18:54:04'),
(82, 113, 'Mr. Active', '-', '3126134', 'sr.madusari@daum.net', '', 0, '2016-10-18 18:59:54', '2016-10-18 18:59:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `content_type`
--

CREATE TABLE IF NOT EXISTS `content_type` (
`id` smallint(5) unsigned NOT NULL,
  `type_name` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `content_type`
--

INSERT INTO `content_type` (`id`, `type_name`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 'News', '1', 1, '2016-06-07 08:41:08', '2016-06-07 08:41:08'),
(2, 'Announcement', '1', 1, '2016-06-07 08:41:26', '2016-10-17 09:34:16'),
(3, 'Invitation', '1', 17, '2016-07-14 02:59:46', '2016-09-01 13:32:53'),
(6, 'Test', '0', 1, '2016-10-17 09:33:39', '2016-10-17 09:33:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `correspondence`
--

CREATE TABLE IF NOT EXISTS `correspondence` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `doc_name` varchar(50) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `desc` varchar(100) NOT NULL,
  `eff_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `remark` varchar(100) NOT NULL,
  `filetype` varchar(50) NOT NULL,
  `remainder` enum('1','0') NOT NULL DEFAULT '0',
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data untuk tabel `correspondence`
--

INSERT INTO `correspondence` (`id`, `id_vendor`, `doc_name`, `doc_num`, `desc`, `eff_date`, `exp_date`, `remark`, `filetype`, `remainder`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 2, 'Salinan SIUP', '1', 'Surat Ijin Usaha Perdagangan', '2016-09-08', '2016-09-26', 'Testing', 'pdf', '1', '1', 2, '2016-09-08 10:21:05', '2016-09-23 09:07:49'),
(3, 2, 'TDP', '8989test', 'Salinan Tanda Daftar Perusahaan', '2016-09-08', '2016-09-26', 'testing doc', 'pdf', '1', '1', 2, '2016-09-08 10:30:57', '2016-09-08 10:30:57'),
(4, 2, 'PKP', 'PKP1234', 'Salinan Surat Penetapan Kena Pajak', '2016-09-08', '2016-09-26', 'Testing PKP', 'pdf', '0', '1', 2, '2016-09-08 10:34:04', '2016-09-08 10:34:04'),
(5, 3, 'SIUP', '3194943198', 'Surat Izin Usaha Pendirian', '2013-09-18', '2016-09-30', 'Surat Izin Usaha Pendirian', 'pdf', '1', '1', 2, '2016-09-09 15:51:20', '2016-09-09 15:51:20'),
(6, 5, 'Surat', '01', 'Surat', '2016-09-09', '2016-09-10', 'Surat Legal', 'jpg', '1', '1', 2, '2016-09-09 18:58:03', '2016-09-15 16:20:53'),
(8, 6, 'KTP', '3374062606900005', 'Ektp', '2016-09-14', '2016-09-12', 'hampir kadaluarsa', 'jpg', '1', '1', 2, '2016-09-14 13:17:53', '2016-09-14 14:17:10'),
(10, 6, 'excel', '123', 'excel', '2016-09-01', '2016-09-15', 'sdfaf', 'xlsx', '', '1', 2, '2016-09-14 14:18:23', '2016-09-14 14:18:23'),
(11, 6, 'PDF', 'fnak', 'askdgjwl', '2016-01-18', '2026-09-14', 'fqef', 'pdf', '', '1', 2, '2016-09-14 14:27:31', '2016-09-14 14:27:31'),
(12, 7, 'KTp', 'ktp', 'ktp', '2014-09-12', '2017-09-29', 'excel', 'xlsx', '1', '1', 2, '2016-09-15 15:44:31', '2016-09-15 15:44:31'),
(13, 18, 'WEHLGJHWQ', 'HLGHQ', 'HGJLHGL`', '2006-09-06', '2026-09-30', 'IRJQIEJ', 'jpg', '0', '1', 2, '2016-09-20 17:55:37', '2016-09-20 17:55:37'),
(14, 18, 'EHGILWE', 'IQEJI', 'JFQJGQ', '2016-09-08', '2016-09-28', 'EG', 'jpg', '0', '1', 2, '2016-09-20 17:56:36', '2016-09-20 17:56:36'),
(15, 18, 'EGFWELFHI', '3121', 'LWJG', '2016-09-05', '2021-09-22', 'EHFIL', 'jpg', '', '1', 2, '2016-09-26 10:04:35', '2016-09-26 10:04:35'),
(19, 2, 'FIle upload document baru', '453463452', 'fiel baru', '2016-10-04', '2016-10-28', 'upload doc baru', 'pdf', '1', '1', 2, '2016-10-04 08:31:57', '2016-10-04 08:31:57'),
(20, 2, 'cobain upload', '49853948', 'sinisenfin', '2016-10-04', '2016-10-28', 'srhstgserg', 'pdf', '', '1', 2, '2016-10-04 08:33:31', '2016-10-04 08:33:31'),
(25, 22, 'KTP', '3374062606900005', 'KTP ASLI LHO', '2016-10-11', '2016-10-26', 'INPUT KTP', 'jpg', '0', '1', 2, '2016-10-12 13:58:27', '2016-10-12 14:00:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `country`
--

CREATE TABLE IF NOT EXISTS `country` (
`id` smallint(5) unsigned NOT NULL,
  `country_name` varchar(30) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('0','1') DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data untuk tabel `country`
--

INSERT INTO `country` (`id`, `country_name`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 'Indonesia2', 'testing', '1', 1, '2015-12-28 07:51:56', '2016-06-01 09:56:15'),
(3, 'vietnam', 'test', '1', 1, '2016-06-01 10:06:36', '2016-06-01 10:06:36'),
(4, 'Singapura', 'test', '1', 1, '2016-06-01 09:10:53', '2016-06-01 09:10:53'),
(6, 'Malaysia', 'test', '1', 1, '2016-06-01 09:13:33', '2016-06-01 09:13:33'),
(7, 'country', 'test edit Country', '1', 17, '2016-06-01 10:07:36', '2016-08-09 19:19:37'),
(9, 'lalalaaa', 'test', '1', 1, '2016-06-01 10:10:58', '2016-10-06 13:33:50'),
(17, 'Test Country edit', '', '1', 17, '2016-07-15 04:13:02', '2016-08-09 17:42:57'),
(18, 'Country2907 Edit', '', '1', 17, '2016-07-29 03:42:29', '2016-07-29 03:47:04'),
(19, 'test3', '', '1', 17, '2016-07-29 03:48:48', '2016-07-29 03:48:48'),
(20, 'Palestina', '', '1', 17, '2016-08-08 08:49:08', '2016-08-08 08:49:08'),
(22, 'Test9Agus', '', '1', 17, '2016-08-09 17:25:30', '2016-09-01 10:58:03'),
(24, 'baru', '', '0', 1, '2016-09-19 13:05:07', '2016-09-19 14:41:43'),
(25, 'Jepang', '', '1', 1, '2016-10-06 13:33:30', '2016-10-06 13:42:47'),
(26, 'Negara', '', '1', 1, '2016-10-14 09:35:55', '2016-10-14 09:35:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dd_assesment`
--

CREATE TABLE IF NOT EXISTS `dd_assesment` (
`id` smallint(5) unsigned NOT NULL,
  `id_subcat` smallint(5) NOT NULL,
  `description` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data untuk tabel `dd_assesment`
--

INSERT INTO `dd_assesment` (`id`, `id_subcat`, `description`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(11, 3, 'test 1', '1', 1, '2016-09-15 08:47:34', '2016-09-15 08:47:34'),
(12, 3, 'test 2', '1', 1, '2016-09-15 08:52:01', '2016-09-15 08:52:01'),
(13, 3, 'test 3', '1', 1, '2016-09-15 08:56:00', '2016-09-15 08:56:00'),
(14, 2, 'test 4', '1', 1, '2016-09-15 08:47:34', '2016-09-15 08:47:34'),
(15, 2, 'test 5', '1', 1, '2016-09-15 08:47:34', '2016-09-15 08:47:34'),
(16, 2, 'test 6', '1', 1, '2016-09-15 08:47:34', '2016-09-15 08:47:34'),
(17, 1, 'test 6', '1', 1, '2016-09-15 14:33:17', '2016-09-15 14:33:17'),
(18, 1, 'Persyaratan memenuhi', '1', 1, '2016-09-21 14:09:39', '2016-09-21 14:09:39'),
(19, 5, 'KANTOR REPRESENTATIF', '1', 1, '2016-09-22 08:53:51', '2016-09-22 08:53:51'),
(20, 1, 'Tempat strategis', '1', 1, '2016-09-23 10:00:47', '2016-09-23 10:00:47'),
(21, 4, 'Asesmenteuuuaa', '0', 1, '2016-10-14 15:00:24', '2016-10-14 15:00:24'),
(22, 8, 'cobain', '0', 1, '2016-10-19 10:39:42', '2016-10-19 10:39:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dd_conclutions`
--

CREATE TABLE IF NOT EXISTS `dd_conclutions` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `conclutions` varchar(100) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dd_docs`
--

CREATE TABLE IF NOT EXISTS `dd_docs` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_visit` smallint(5) NOT NULL,
  `doc_name` varchar(50) NOT NULL,
  `remark` varchar(100) NOT NULL,
  `filetype` varchar(50) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data untuk tabel `dd_docs`
--

INSERT INTO `dd_docs` (`id`, `id_vendor`, `id_visit`, `doc_name`, `remark`, `filetype`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 2, 1, 'Doc site visit', 'site visit', 'pdf', '1', 2, '2016-09-08 10:48:18', '2016-09-08 10:48:18'),
(3, 3, 3, 'Bukti', 'laporan', 'pdf', '1', 2, '2016-09-09 15:56:14', '2016-09-09 15:56:14'),
(4, 3, 3, 'foto', 'foto', 'jpg', '1', 2, '2016-09-09 16:00:27', '2016-09-09 16:01:27'),
(5, 3, 3, 'aaqq', 'aasss', 'jpg', '1', 2, '2016-09-09 16:01:41', '2016-09-09 16:01:41'),
(6, 5, 4, 'Foto', 'Foto Kantor', 'jpg', '1', 2, '2016-09-09 19:02:52', '2016-09-09 19:02:52'),
(8, 6, 6, 'KTP', 'fafh', 'jpg', '1', 2, '2016-09-14 15:03:00', '2016-09-14 15:03:00'),
(10, 6, 6, 'pdf', 'coba pdf', 'pdf', '1', 2, '2016-09-14 15:05:36', '2016-09-14 15:05:36'),
(11, 6, 6, 'ppt', 'ppt', 'pptx', '1', 2, '2016-09-14 15:46:44', '2016-09-14 15:46:44'),
(12, 26, 10, 'foto kantor', 'sangat bagus', 'jpg', '1', 2, '2016-09-21 14:53:19', '2016-09-21 14:53:19'),
(13, 26, 10, 'management nya', 'biasa aja', 'jpg', '1', 2, '2016-09-21 14:54:20', '2016-09-21 14:54:20'),
(14, 28, 11, 'Surat', 'Note', 'jpg', '1', 2, '2016-09-27 14:31:21', '2016-09-27 14:31:21'),
(15, 10, 12, 'KK', 'kol', 'jpg', '1', 2, '2016-09-28 08:38:23', '2016-09-28 08:38:23'),
(16, 55, 17, 'ktpbagus', 'valid', 'jpg', '1', 2, '2016-10-03 15:02:17', '2016-10-03 15:03:14'),
(17, 55, 17, 'KK', 'valid', 'jpg', '1', 2, '2016-10-03 15:02:33', '2016-10-03 15:02:33'),
(18, 2, 1, 'Test upload DD docs', 'esorinh[', 'pdf', '1', 2, '2016-10-04 15:36:52', '2016-10-04 15:36:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dd_notes`
--

CREATE TABLE IF NOT EXISTS `dd_notes` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_visit` smallint(5) NOT NULL,
  `notes` varchar(100) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `dd_notes`
--

INSERT INTO `dd_notes` (`id`, `id_vendor`, `id_visit`, `notes`, `created_id`, `created_date`, `last_updated`) VALUES
(2, 2, 1, 'Catetan tambahan site visit 2', 2, '2016-09-08 10:46:57', '2016-09-08 10:46:57'),
(3, 3, 3, 'Catatan site visit 1', 2, '2016-09-09 15:55:04', '2016-09-09 15:55:04'),
(4, 3, 3, 'Catatan site visit 2', 2, '2016-09-09 15:55:10', '2016-09-09 15:55:10'),
(5, 3, 3, 'Catatan site visit 3', 2, '2016-09-09 15:55:15', '2016-09-09 15:55:15'),
(7, 6, 6, 'Mohon persiapkan makan siang, mobil, serta bingkisan', 2, '2016-09-14 14:54:22', '2016-09-14 14:54:22'),
(8, 26, 10, 'kantor nya bagus, tapi management nya tidak bagus', 2, '2016-09-21 14:50:10', '2016-09-21 14:50:10'),
(9, 28, 11, 'DuDil', 2, '2016-09-27 14:28:42', '2016-09-27 14:28:42'),
(10, 10, 12, 'note1', 2, '2016-09-28 08:34:05', '2016-09-28 08:34:05'),
(11, 55, 17, 'baik', 2, '2016-10-03 15:01:42', '2016-10-03 15:01:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dd_participant`
--

CREATE TABLE IF NOT EXISTS `dd_participant` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_visit` smallint(5) NOT NULL,
  `visitor_name` varchar(35) NOT NULL,
  `unit` varchar(30) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data untuk tabel `dd_participant`
--

INSERT INTO `dd_participant` (`id`, `id_vendor`, `id_visit`, `visitor_name`, `unit`, `created_id`, `created_date`, `last_updated`) VALUES
(2, 2, 1, 'Richo', 'IBR', 2, '2016-09-08 10:40:41', '2016-09-08 10:40:41'),
(3, 3, 3, 'Ridho', 'IBK', 2, '2016-09-09 15:54:14', '2016-09-09 15:54:14'),
(4, 3, 3, 'Rasyid', 'IBK', 2, '2016-09-09 15:54:22', '2016-09-09 15:54:22'),
(5, 3, 3, 'Tatang S', 'MX', 2, '2016-09-09 15:54:32', '2016-09-09 15:54:32'),
(7, 6, 6, 'richo', 'asysyt', 2, '2016-09-14 14:53:01', '2016-09-14 14:53:01'),
(9, 6, 6, 'wisnu', 'ose', 2, '2016-09-14 14:53:18', '2016-09-14 14:53:18'),
(10, 6, 6, 'archi', 'osa', 2, '2016-09-14 14:53:33', '2016-09-14 14:53:33'),
(11, 26, 10, 'wisnu', 'ose3', 2, '2016-09-21 14:49:20', '2016-09-21 14:49:20'),
(12, 26, 10, 'archi', 'ose2', 2, '2016-09-21 14:49:33', '2016-09-21 14:49:33'),
(13, 28, 11, 'Yudha', 'IBK', 2, '2016-09-27 14:29:07', '2016-09-27 14:29:07'),
(14, 55, 17, 'ditya', 'IB', 2, '2016-10-03 15:01:17', '2016-10-03 15:01:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dd_summary`
--

CREATE TABLE IF NOT EXISTS `dd_summary` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_visit` smallint(5) NOT NULL,
  `summary` varchar(100) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `dd_summary`
--

INSERT INTO `dd_summary` (`id`, `id_vendor`, `id_visit`, `summary`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 2, 1, 'Site Visit telah dilakukan', 2, '2016-09-08 10:47:33', '2016-09-08 10:47:33'),
(2, 3, 3, 'Ringkasan', 2, '2016-09-09 15:55:51', '2016-09-09 15:55:51'),
(4, 6, 6, 'So far so good', 2, '2016-09-14 15:02:41', '2016-09-14 15:02:41'),
(5, 26, 10, 'menurut saya, sebaik nya dijadikan shortlist dulu sampai ada aktivitas tindak lanjut lagi', 2, '2016-09-21 14:50:46', '2016-09-21 14:50:46'),
(6, 55, 17, 'semuanya baik', 2, '2016-10-03 15:01:56', '2016-10-03 15:01:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dd_vdr_assesment`
--

CREATE TABLE IF NOT EXISTS `dd_vdr_assesment` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_visit` smallint(5) NOT NULL,
  `id_subcat` smallint(5) NOT NULL,
  `id_assesment` smallint(5) NOT NULL,
  `result` enum('0','1') NOT NULL DEFAULT '0',
  `remark` varchar(100) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data untuk tabel `dd_vdr_assesment`
--

INSERT INTO `dd_vdr_assesment` (`id`, `id_vendor`, `id_visit`, `id_subcat`, `id_assesment`, `result`, `remark`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 2, 1, 3, 1, '1', 'Ok', 2, '2016-09-08 10:49:16', '2016-09-09 15:52:37'),
(2, 2, 1, 5, 1, '0', 'Tidak Ok', 2, '2016-09-08 10:53:41', '2016-09-09 15:52:37'),
(3, 2, 1, 6, 1, '1', 'none', 2, '2016-09-08 10:54:16', '2016-09-09 15:52:37'),
(4, 3, 3, 1, 1, '1', 'AC nya baru', 2, '2016-09-09 15:56:42', '2016-09-09 15:52:37'),
(5, 3, 3, 2, 1, '0', 'yang ini ga layak', 2, '2016-09-09 15:58:05', '2016-09-09 15:52:37'),
(7, 6, 6, 1, 1, '1', 'mayan kok kantor nya', 2, '2016-09-14 15:50:00', '2016-09-09 15:52:37'),
(8, 6, 6, 2, 1, '0', 'none jelek', 2, '2016-09-14 15:51:42', '2016-09-09 15:52:37'),
(9, 6, 6, 5, 1, '1', 'none nya tidak hilang', 2, '2016-09-14 15:52:00', '2016-09-09 15:52:37'),
(10, 5, 4, 2, 14, '1', 'none nya tidak hilang', 2, '2016-09-14 15:52:00', '2016-09-09 15:52:37'),
(11, 5, 4, 2, 15, '1', 'none nya tidak hilang', 2, '2016-09-14 15:52:00', '2016-09-09 15:52:37'),
(12, 5, 4, 2, 16, '1', 'none nya tidak hilang', 2, '2016-09-14 15:52:00', '2016-09-09 15:52:37'),
(13, 5, 4, 1, 17, '1', 'none nya tidak hilang', 2, '2016-09-14 15:52:00', '2016-09-09 15:52:37'),
(14, 26, 10, 1, 17, '1', 'SANGAT BAIK', 2, '2016-09-22 09:17:37', '2016-09-22 17:37:00'),
(15, 26, 10, 1, 18, '1', 'MEMUASKAN', 2, '2016-09-22 09:17:37', '2016-09-22 17:37:00'),
(16, 10, 2, 0, 11, '1', 'noneyou', 2, '2016-09-29 09:52:31', '0000-00-00 00:00:00'),
(17, 10, 2, 0, 12, '1', 'noneninuna', 2, '2016-09-29 09:52:31', '0000-00-00 00:00:00'),
(18, 10, 2, 0, 13, '0', 'none', 2, '2016-09-29 09:52:31', '0000-00-00 00:00:00'),
(19, 10, 2, 0, 14, '0', 'none', 2, '2016-09-29 09:52:31', '0000-00-00 00:00:00'),
(20, 10, 2, 0, 15, '0', 'none', 2, '2016-09-29 09:52:31', '0000-00-00 00:00:00'),
(21, 10, 2, 0, 16, '0', 'none', 2, '2016-09-29 09:52:31', '0000-00-00 00:00:00'),
(22, 10, 2, 0, 17, '0', 'none', 2, '2016-09-29 09:52:31', '0000-00-00 00:00:00'),
(23, 10, 2, 0, 18, '0', 'none', 2, '2016-09-29 09:52:31', '0000-00-00 00:00:00'),
(24, 10, 2, 0, 19, '0', 'none', 2, '2016-09-29 09:52:31', '0000-00-00 00:00:00'),
(25, 10, 2, 0, 20, '0', 'none', 2, '2016-09-29 09:52:31', '0000-00-00 00:00:00'),
(26, 55, 17, 1, 17, '1', 'noneyuk', 2, '2016-10-03 15:06:01', '2016-10-03 06:01:00'),
(27, 55, 17, 1, 18, '1', 'mantab', 2, '2016-10-03 15:06:01', '2016-10-03 06:01:00'),
(28, 55, 17, 1, 20, '1', 'bagus', 2, '2016-10-03 15:06:01', '2016-10-03 06:01:00'),
(29, 55, 17, 2, 14, '0', 'none', 2, '2016-10-03 15:07:16', '2016-10-03 07:16:00'),
(30, 55, 17, 2, 15, '0', 'none', 2, '2016-10-03 15:07:16', '2016-10-03 07:16:00'),
(31, 55, 17, 2, 16, '0', 'none', 2, '2016-10-03 15:07:16', '2016-10-03 07:16:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `departemen`
--

CREATE TABLE IF NOT EXISTS `departemen` (
`id` smallint(5) unsigned NOT NULL,
  `id_division` smallint(5) NOT NULL,
  `dept_code` varchar(10) NOT NULL,
  `dept_name` varchar(30) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `departemen`
--

INSERT INTO `departemen` (`id`, `id_division`, `dept_code`, `dept_name`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 1, 'IBK', 'IBK', '', '1', 17, '2016-09-09 15:52:37', '2016-09-19 16:15:46'),
(2, 1, 'IBR', 'IBR', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(3, 1, 'IBS', 'IBS', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(4, 2, 'WA1', 'WA-1', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(5, 2, 'WA2', 'WA-2', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(6, 2, 'ASYST', 'Admin Asyst', '', '1', 1, '2016-09-20 00:00:00', '2016-10-12 09:18:49'),
(7, 3, 'MD', 'MD', '', '0', 1, '2016-10-12 09:18:31', '2016-10-12 09:18:31'),
(8, 6, 'AADS', 'AAA', '', '1', 1, '2016-10-12 14:11:08', '2016-10-12 14:11:08'),
(9, 5, 'ABB', 'ABBC', '', '0', 1, '2016-10-18 18:54:19', '2016-10-18 18:54:27'),
(10, 6, 'asd', 'asdasasd', '', '0', 1, '2016-10-18 18:55:02', '2016-10-18 18:55:08'),
(11, 2, 'TES', 'NEW', '', '1', 1, '2016-10-18 20:26:31', '2016-10-18 20:26:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `departemen_`
--

CREATE TABLE IF NOT EXISTS `departemen_` (
`id` smallint(5) unsigned NOT NULL,
  `id_office` smallint(5) NOT NULL,
  `dept_code` varchar(10) NOT NULL,
  `dept_name` varchar(30) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `departemen_`
--

INSERT INTO `departemen_` (`id`, `id_office`, `dept_code`, `dept_name`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 3, 'IB', 'IB', '', '1', 17, '2016-09-09 15:52:37', '2016-08-13 10:23:17'),
(2, 3, 'WA', 'WA', '', '1', 1, '2016-09-09 15:52:37', '2016-09-09 15:52:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `direct_bidding`
--

CREATE TABLE IF NOT EXISTS `direct_bidding` (
`id` int(10) unsigned NOT NULL,
  `id_announce` smallint(5) NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `direct_bidding`
--

INSERT INTO `direct_bidding` (`id`, `id_announce`, `id_vendor`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 13, 70, '1', 0, '2016-05-17 23:22:24', '2016-05-17 23:22:24'),
(5, 12, 70, '1', 1, '2016-05-23 22:48:45', '2016-05-23 22:48:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `division`
--

CREATE TABLE IF NOT EXISTS `division` (
`id` smallint(5) unsigned NOT NULL,
  `id_office` smallint(5) NOT NULL,
  `division_code` varchar(10) NOT NULL,
  `division_name` varchar(30) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `division`
--

INSERT INTO `division` (`id`, `id_office`, `division_code`, `division_name`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 3, 'IB', 'IB', '', '1', 17, '2016-09-09 15:52:37', '2016-10-12 13:52:20'),
(2, 3, 'WA', 'WA', '', '1', 1, '2016-09-07 00:00:00', '2016-09-19 16:11:25'),
(3, 2, 'CKG', 'Cengkarengs', '', '1', 1, '2016-10-11 16:22:55', '2016-10-11 16:22:55'),
(4, 3, 'IBK', 'IB', '', '0', 1, '2016-10-12 08:54:23', '2016-10-12 08:54:34'),
(5, 1, 'AB', 'AB', '', '0', 1, '2016-10-12 08:54:52', '2016-10-12 08:54:52'),
(6, 3, 'IB', 'IBB', '', '1', 1, '2016-10-12 13:48:21', '2016-10-12 13:48:21'),
(7, 3, 'RDM', 'RNDM', '', '1', 1, '2016-10-12 13:51:36', '2016-10-12 13:52:05'),
(8, 3, 'IKK', 'IBB', '', '1', 1, '2016-10-12 13:55:03', '2016-10-12 13:55:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `document_req`
--

CREATE TABLE IF NOT EXISTS `document_req` (
`id` smallint(5) NOT NULL,
  `document_req` varchar(30) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `document_req`
--

INSERT INTO `document_req` (`id`, `document_req`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(2, 'RFP', '1', 17, '2016-08-01 09:23:43', '2016-08-01 09:23:43'),
(4, 'test', '1', 17, '2016-08-19 10:51:20', '2016-09-01 13:38:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `document_requirement`
--

CREATE TABLE IF NOT EXISTS `document_requirement` (
  `id` varchar(20) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `counter` smallint(5) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `document_requirement`
--

INSERT INTO `document_requirement` (`id`, `description`, `remarks`, `filename`, `counter`, `created_date`, `updated_date`, `file`) VALUES
('57e4a119cba7d', NULL, NULL, '230916017', 1, '2016-09-23 10:30:57', '2016-09-23 10:30:57', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('57e4a119cba7d', 'Doc1', 'Ini Doc 1', '230916017', 2, '2016-09-23 10:31:13', '2016-09-23 10:31:17', 'RFP Enhancement E-Procurement.pdf'),
('57e9f32fd6640', 'Doc 1', 'Dokumentasi', NULL, 1, '2016-09-27 14:04:26', '2016-09-27 14:04:34', 'RFP Enhancement E-Procurement.pdf'),
('57e9f32fd6640', 'Presentasi', 'Presentasi', NULL, 2, '2016-09-27 14:05:03', '2016-09-27 14:05:17', 'presentasi web eproc.pdf'),
('57e9f6640a0b8', NULL, NULL, NULL, 1, '2016-09-27 11:32:58', '2016-09-27 11:32:58', 'Getting Started with Laravel 4.pdf'),
('57ea1d39ad968', NULL, NULL, NULL, 1, '2016-09-27 14:18:29', '2016-09-27 14:18:29', 'Getting Started with Laravel 4.pdf'),
('57ea20245b9fa', 'Doc 1', 'Ini Doc 1', NULL, 1, '2016-09-27 14:34:24', '2016-09-27 14:34:33', 'RFP Enhancement E-Procurement.pdf'),
('57ea20245b9fa', 'Doc 2', 'Dokumentasi', NULL, 2, '2016-09-27 14:34:42', '2016-09-27 14:34:47', 'RFP Enhancement E-Procurement.pdf'),
('57ea211e64a4f', 'Doc 1', 'Ini Doc 1', '270916015', 1, '2016-09-27 14:36:48', '2016-09-27 14:36:49', 'RFP Enhancement E-Procurement.pdf'),
('57ea211e64a4f', 'Doc 2', 'Dokumentasi', '270916015', 2, '2016-09-27 14:36:56', '2016-09-27 14:37:05', 'presentasi web eproc.pdf'),
('57f329b6e2073', 'Doc 1', 'Test', '041016001', 1, '2016-10-04 11:29:34', '2016-10-04 11:29:36', 'presentasi web eproc.pdf'),
('57f329b6e2073', 'Doc 2', 'Dokumentasi', '041016001', 2, '2016-10-04 11:29:52', '2016-10-04 11:29:53', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('57f5be292c3bc', 'Doc 1', 'Dokumen', '061016002', 1, '2016-10-06 10:05:34', '2016-10-12 10:39:30', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('57fb03269300d', 'Doc 1', 'r234', NULL, 1, '2016-10-10 09:55:46', '2016-10-10 09:55:48', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('57fb03269300d', 'Doc 2', 'Dokumentasi', NULL, 2, '2016-10-10 09:56:01', '2016-10-10 09:56:02', 'RFP Enhancement E-Procurement.pdf'),
('57fb0cc472915', 'Doc 1', 'Dokumentasi', NULL, 1, '2016-10-10 10:36:50', '2016-10-10 10:36:55', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('57fb0d009a929', NULL, NULL, NULL, 1, '2016-10-10 10:37:44', '2016-10-10 10:37:44', 'RFP Enhancement E-Procurement.pdf'),
('57fb0d8b05116', 'Doc 2', 'Ini Doc 1', '101016005', 2, '2016-10-10 10:41:40', '2016-10-10 10:41:41', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('57fd87963704d', 'Doc 1', 'Dokumentasi', NULL, 1, '2016-10-12 07:45:20', '2016-10-12 07:45:22', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('57fddde85816f', 'b', 'b', NULL, 2, '2016-10-12 13:56:15', '2016-10-12 13:56:35', 'developing swift for beginner.pdf'),
('57fe4ff79e458', 'qqq', 'qqq', NULL, 1, '2016-10-12 22:01:07', '2016-10-12 22:01:08', 'Getting Started with Laravel 4.pdf'),
('57fe5132b1e15', 'eee', 'eee', '121016007', 1, '2016-10-12 22:06:57', '2016-10-12 22:06:58', 'Getting Started with Laravel 4.pdf'),
('57fe5132b1e15', 'rrr', 'rrr', '121016007', 2, '2016-10-12 22:07:09', '2016-10-12 22:07:10', 'Learn Swift 2 on the Mac, 2nd Edition.pdf'),
('57feeaa813df9', 'doc 1`', 'Dokumen', NULL, 1, '2016-10-13 09:01:04', '2016-10-13 09:01:07', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('57ff4b2b688d6', NULL, NULL, NULL, 1, '2016-10-13 15:52:00', '2016-10-13 15:52:00', 'RFP Enhancement E-Procurement.pdf'),
('5800a92f503c7', NULL, NULL, NULL, 1, '2016-10-14 16:45:35', '2016-10-14 16:45:35', '010816016_1.pdf'),
('5800a9bd31c68', NULL, NULL, NULL, 1, '2016-10-14 16:47:46', '2016-10-14 16:47:46', '010816016_1.pdf'),
('5804341417581', NULL, NULL, NULL, 1, '2016-10-17 09:14:52', '2016-10-17 09:14:52', '010816016_1.pdf'),
('5804369c38e9e', 'Doc 1', 'Dokumentasi', '171016014', 1, '2016-10-17 09:29:46', '2016-10-17 09:29:47', 'RFP Enhancement E-Procurement.pdf'),
('58044dc028aa6', 'Doc 1', 'Dokumentasi', '171016016', 1, '2016-10-17 11:05:29', '2016-10-17 11:05:30', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('580453e73c3f8', 'Doc 1', 'Dokumentasi', '171016017', 1, '2016-10-17 11:32:17', '2016-10-17 11:32:18', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('5804591f81009', 'Doc 1', 'Dokumentasi', '171016018', 1, '2016-10-17 11:55:09', '2016-10-17 11:55:10', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('58047591ede76', 'Doc 2', 'Dokumen', '171016020', 1, '2016-10-17 13:58:25', '2016-10-17 13:58:27', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('58048142eccea', 'Doc 2', 'Dokumentasi', '171016022', 1, '2016-10-17 14:44:28', '2016-10-17 14:44:31', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('5804842715c99', 'Doc 1', 'Dokumen', '171016023', 1, '2016-10-17 14:57:03', '2016-10-17 14:57:04', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('580580cb91dd2', 'Doc 1', 'Dokumentasi', '181016026', 1, '2016-10-18 08:55:14', '2016-10-18 08:55:15', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('5805812dda6c4', 'Doc 1', 'Dokumentasi', '181016027', 1, '2016-10-18 08:56:48', '2016-10-18 08:56:49', 'RFP Enhancement E-Procurement.pdf'),
('580589f1619d9', 'Doc 1', 'Dokumentasi', '181016028', 1, '2016-10-18 09:35:50', '2016-10-18 09:35:51', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('58058b9e2fa50', 'Doc 2', 'Test', '181016029', 1, '2016-10-18 09:44:25', '2016-10-18 09:44:27', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('5805cc32267ec', 'Doc 2', 'Dokumen', '181016031', 1, '2016-10-18 14:17:55', '2016-10-18 14:17:59', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf'),
('5805d24bf2ea3', 'Doc 1', 'Dokumentasi', '181016032', 1, '2016-10-18 14:43:08', '2016-10-18 14:43:12', 'PN-2015-AM-845_Proposal Teknis-  Enhancement Website E-Procurement GA_v3.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `due_dilligence`
--

CREATE TABLE IF NOT EXISTS `due_dilligence` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `doc_name` varchar(50) NOT NULL,
  `remark` varchar(100) NOT NULL,
  `filetype` varchar(50) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
`id` smallint(5) NOT NULL,
  `question_ind` varchar(500) NOT NULL,
  `question_eng` varchar(500) NOT NULL,
  `answer_ind` varchar(1000) NOT NULL,
  `answer_eng` varchar(1000) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `faq`
--

INSERT INTO `faq` (`id`, `question_ind`, `question_eng`, `answer_ind`, `answer_eng`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 'Bagaimana Rekanan dapat mengikuti proses pengadaan Garuda ?', 'How to Partner can follow the procurement process Garuda?', '<p>Setiap rekanan yang ingin mengikuti proses pengadaan di PT Garuda Indonesia harus melakukan registrasi terlebih dahulu dengan mengirimkan data profile perusahaan dan kelengkapan bukti dokumen sesuai dengan bidang usahanya?</p>', '<p>Setiap rekanan yang ingin mengikuti proses pengadaan di PT Garuda Indonesia harus melakukan registrasi terlebih dahulu dengan mengirimkan data profile perusahaan dan kelengkapan bukti dokumen sesuai dengan bidang usahanya</p>', '1', 1, '2016-08-30 09:37:30', '2016-09-29 09:57:38'),
(2, 'Dimana Rekanan dapat mengetahui kebutuhan pengadaan Garuda ?', 'Where partner can find out  the procurement needs Garuda?', '<p>Kebutuhan pengadaan Garuda dapat diketahui dari pengumuman web pengadaan Garuda atau akan diberitahukan melalui undangan ke masing-masing alamat peserta</p>\n', '<p>Kebutuhan pengadaan Garuda dapat diketahui dari pengumuman web pengadaan Garuda atau akan diberitahukan melalui undangan ke masing-masing alamat peserta</p>', '1', 1, '2016-08-30 09:53:08', '2016-09-29 09:56:30'),
(3, 'Mengapa Calon Rekanan tidak bisa menjadi rekanan Garuda?', 'Why Prospective Partner can not become a partner of Garuda?', '<p>Beberapa alasan mengapa perusahaan yang meregister tidak bisa menjadi rekanan Garuda antara lain :</p>\n\n<ul>\n	<li>Pada saat tertentu Garuda belum memerlukan komoditi tertentu, sehingga calon rekanan dengan bidang usaha yang bersangkutan belum dipertimbangkan untuk menjadi rekanan</li>\n	<li>Jumlah rekanan yang terdaftar di Garuda pada saat tertentu sudah mencukupi untuk memenuhi kebutuhan Garuda</li>\n	<li>Persyaratan untuk menjadi rekanan Garuda belum dapat dipenuhi atau oleh calon rekanan</li>\n	<li>Tidak sesuai dengan kebijakan pengadaan</li>\n	<li>Pernah dan atau sedang ada permasalahan yang menyangkut kinerja yang tidak memuaskan dengan pengadaan Garuda sebelumnya</li>\n	<li>Berdasarkan penilaian (assessment) atau referensi, bahwa calon rekanan yang bersangkutan mempunyai kinerja yang tidak baik atau masih perlu dipertimbangkan untuk dapat menjadi rekanan</li>\n</ul>\n', '<p>Beberapa alasan mengapa perusahaan yang meregister tidak bisa menjadi rekanan Garuda antara lain :</p><ul>	<li>Pada saat tertentu Garuda belum memerlukan komoditi tertentu, sehingga calon rekanan dengan bidang usaha yang bersangkutan belum dipertimbangkan untuk menjadi rekanan</li>	<li>Jumlah rekanan yang terdaftar di Garuda pada saat tertentu sudah mencukupi untuk memenuhi kebutuhan Garuda</li>	<li>Persyaratan untuk menjadi rekanan Garuda belum dapat dipenuhi atau oleh calon rekanan</li>	<li>Tidak sesuai dengan kebijakan pengadaan</li>	<li>Pernah dan atau sedang ada permasalahan yang menyangkut kinerja yang tidak memuaskan dengan pengadaan Garuda sebelumnya</li>	<li>Berdasarkan penilaian (assessment) atau referensi, bahwa calon rekanan yang bersangkutan mempunyai kinerja yang tidak baik atau masih perlu dipertimbangkan untuk dapat menjadi rekanan</li></ul>', '1', 1, '2016-09-28 08:54:54', '2016-09-29 09:54:58'),
(4, 'Siapa saja yang dapat menjadi rekanan Garuda?', 'Anyone can become a partner of Garuda?', '<p>Setiap perusahaan dengan bidang usaha yang sesuai dengan kebutuhan Garuda dan mempunyai kinerja yang baik dapat menjadi rekanan Garuda. Dalam memilih rekanannya, kebijakan Garuda lebih menitikberatkan pengadaan secara langsung ke sumber penghasil barang/jasa, seperti pabrikan atau distributor resmi dan menghindari pengadaan barang/jasa melalui perantara yang tidak memberikan nilai tambah.</p>\n', '<p>Setiap perusahaan dengan bidang usaha yang sesuai dengan kebutuhan Garuda dan mempunyai kinerja yang baik dapat menjadi rekanan Garuda. Dalam memilih rekanannya, kebijakan Garuda lebih menitikberatkan pengadaan secara langsung ke sumber penghasil barang/jasa, seperti pabrikan atau distributor resmi dan menghindari pengadaan barang/jasa melalui perantara yang tidak memberikan nilai tambah.</p>', '1', 1, '2016-09-28 08:56:10', '2016-09-29 09:57:58'),
(5, 'Apa persyaratan untuk menjadi rekanan Garuda?', 'What are the requirements to become a partner of Garuda?', '<p>Persyaratan untuk menjadi rekanan Garuda antara lain:</p><ul>	<li>Menunjukkan bukti otentik sebagai badan usaha yang jelas di bidang usahanya</li>	<li>Mempunyai NPWP dan PKP yang masih berlaku untuk perusahaan dalam negeri atau certificate of domicile untuk perusahaan asing</li>	<li>Tidak sedang terlibat dalam kasus pelanggaran hukum, termasuk tetapi tidak terbatas pada palanggaran hukum pidana, perdata, perpajakan dan atau permasalahan lain</li>	<li>Memenuhi persyaratan administrasi dan teknis yang diperlukan oleh Garuda</li></ul>', '<p>Persyaratan untuk menjadi rekanan Garuda antara lain:</p><ul>	<li>Menunjukkan bukti otentik sebagai badan usaha yang jelas di bidang usahanya</li>	<li>Mempunyai NPWP dan PKP yang masih berlaku untuk perusahaan dalam negeri atau certificate of domicile untuk perusahaan asing</li>	<li>Tidak sedang terlibat dalam kasus pelanggaran hukum, termasuk tetapi tidak terbatas pada palanggaran hukum pidana, perdata, perpajakan dan atau permasalahan lain</li>	<li>Memenuhi persyaratan administrasi dan teknis yang diperlukan oleh Garuda</li></ul>', '1', 1, '2016-09-28 08:57:26', '2016-09-28 08:57:26'),
(8, 'Apa itu Vendor?', 'What is a Vendor?', '<p>Vendor adalah instansi yang bekerja sama dengan kita. YAyayaya</p>\n', '<p>Vendor is an instance which have a contribution with us. Yupyupyupp</p>\n', '0', 1, '2016-10-17 13:20:55', '2016-10-17 13:21:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `file_type`
--

CREATE TABLE IF NOT EXISTS `file_type` (
`id` smallint(5) unsigned NOT NULL,
  `type` varchar(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data untuk tabel `file_type`
--

INSERT INTO `file_type` (`id`, `type`) VALUES
(2, 'jpg'),
(3, 'pdf'),
(4, 'png'),
(5, 'xlsx'),
(6, 'zip'),
(7, 'sql'),
(8, 'pptx'),
(9, '0'),
(10, '0'),
(11, '0'),
(12, '0'),
(13, '0'),
(14, '0'),
(15, '0'),
(16, '0'),
(17, 'txt'),
(18, '0'),
(19, '0'),
(20, '0'),
(21, '0'),
(22, '0'),
(23, '0'),
(24, '0'),
(25, 'export_exc');

-- --------------------------------------------------------

--
-- Struktur dari tabel `function`
--

CREATE TABLE IF NOT EXISTS `function` (
`id` smallint(5) unsigned NOT NULL,
  `function` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Dumping data untuk tabel `function`
--

INSERT INTO `function` (`id`, `function`, `status`, `created_date`, `created_id`, `last_updated`) VALUES
(1, 'Registration Vendor', '1', '2016-04-29 18:29:01', 0, '2016-09-09 15:52:37'),
(2, 'Verification Vendor', '1', '2016-04-29 18:29:01', 0, '2016-09-09 15:52:37'),
(3, 'Redlist Vendor', '1', '2016-09-09 15:52:37', 0, '2016-09-09 15:52:37'),
(4, 'Active/inactive Vendor', '1', '2016-08-30 00:00:00', 0, '2016-09-09 15:52:37'),
(5, 'Vendor Blacklist', '1', '2016-08-30 00:00:00', 0, '2016-09-09 15:52:37'),
(6, 'Vendor List', '1', '2016-09-01 11:29:09', 17, '2016-09-01 11:29:09'),
(7, 'Vendor Logs', '1', '2016-09-05 11:10:24', 17, '2016-09-05 11:10:24'),
(8, 'Proses Registrasi Vendor', '1', '2016-09-06 10:24:40', 17, '2016-09-06 10:24:40'),
(9, 'Content Management', '1', '2016-09-06 00:00:00', 1, '2016-09-06 00:00:00'),
(10, 'Manage Vendor Data', '1', '2016-09-06 00:00:00', 1, '2016-09-06 00:00:00'),
(11, 'Manage Company', '1', '2016-09-07 11:05:15', 1, '2016-09-07 11:05:15'),
(12, 'Manage Master Vendor', '1', '2016-09-07 11:38:31', 1, '2016-09-07 15:42:59'),
(13, 'Manage CMS', '1', '2016-09-07 11:38:41', 1, '2016-09-07 15:42:44'),
(14, 'Manage User', '1', '2016-09-07 11:39:23', 1, '2016-09-07 11:39:23'),
(15, 'Manage Content', '1', '2016-09-07 13:37:25', 1, '2016-09-07 13:37:25'),
(18, 'Vendor Delete', '1', '2016-09-13 15:51:09', 1, '2016-09-13 15:51:09'),
(19, 'Create Vendor', '1', '2016-09-20 10:26:00', 1, '2016-09-20 10:26:00'),
(20, 'Vendor Document', '1', '2016-10-04 15:53:32', 1, '2016-10-04 15:53:32'),
(21, 'Tambah Vendor', '1', '2016-10-05 16:36:39', 1, '2016-10-05 16:36:39'),
(22, 'Permission Group', '0', '2016-10-12 11:34:19', 1, '2016-10-14 09:36:36'),
(23, 'Manage Menu', '0', '2016-10-12 13:39:38', 1, '2016-10-12 15:41:05'),
(24, 'Addddding', '0', '2016-10-12 15:41:23', 1, '2016-10-12 15:42:19'),
(25, 'Permission Group Test', '0', '2016-10-14 09:35:38', 1, '2016-10-14 09:35:38'),
(26, 'Adddddd', '0', '2016-10-14 10:08:13', 1, '2016-10-14 10:08:13'),
(27, 'dddddd', '0', '2016-10-14 10:41:17', 1, '2016-10-14 10:41:17'),
(28, 'Addddding', '0', '2016-10-14 10:43:21', 1, '2016-10-14 10:43:21'),
(29, 'Promote AVL', '1', '2016-10-18 08:41:26', 1, '2016-10-18 08:41:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `functional`
--

CREATE TABLE IF NOT EXISTS `functional` (
`id` smallint(5) NOT NULL,
  `code` varchar(10) NOT NULL,
  `functional` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `functional`
--

INSERT INTO `functional` (`id`, `code`, `functional`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 'ac', 'Admin Content', '1', 1, '2016-09-16 00:00:00', '2016-09-16 00:00:00'),
(2, 'ap', 'Admin Purchasing', '1', 1, '2016-09-16 00:00:00', '2016-10-07 08:47:32'),
(3, 'as', 'Admin Sourcing', '1', 1, '2016-09-16 00:00:00', '2016-09-16 00:00:00'),
(4, 'au', 'Admin User', '1', 1, '2016-09-23 10:46:25', '2016-09-23 10:46:28'),
(5, 'sa', 'Super Admin', '1', 1, '2016-10-06 14:20:11', '2016-10-06 14:20:11'),
(6, 'md', 'Moderatorr', '0', 1, '2016-10-12 09:31:40', '2016-10-12 09:35:30'),
(7, 'mdtrsd', 'Moderator', '0', 1, '2016-10-12 09:32:56', '2016-10-12 09:33:05'),
(8, 'sg', 'sf', '0', 1, '2016-10-12 09:35:44', '2016-10-12 09:35:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ga_employee`
--

CREATE TABLE IF NOT EXISTS `ga_employee` (
`id` smallint(5) unsigned NOT NULL,
  `userID` smallint(5) NOT NULL,
  `nopeg` varchar(30) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `id_office` smallint(5) DEFAULT NULL,
  `id_div` smallint(5) DEFAULT NULL,
  `id_dept` smallint(5) DEFAULT NULL,
  `id_position` smallint(5) DEFAULT NULL,
  `id_functional` smallint(5) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data untuk tabel `ga_employee`
--

INSERT INTO `ga_employee` (`id`, `userID`, `nopeg`, `fullname`, `id_office`, `id_div`, `id_dept`, `id_position`, `id_functional`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 2, '900446', 'Wisnu Arimurti', 3, 1, 3, 4, 3, '1', 1, '2016-09-07 16:43:06', '2016-09-16 11:00:50'),
(2, 3, '900445', 'Mitra', 3, 1, 3, 2, 0, '1', 1, '2016-09-07 16:45:51', '2016-09-15 09:25:47'),
(3, 4, '900444', 'Ridho', 3, 1, 1, 2, 0, '1', 3, '2016-09-07 16:47:27', '2016-09-07 16:47:27'),
(4, 5, '900443', 'Ditya', 3, 1, 2, 3, 0, '1', 4, '2016-09-07 16:51:43', '2016-09-07 16:51:43'),
(5, 6, '9004231', 'Ikhsan', 3, 1, 3, 1, 0, '1', 14, '2016-09-07 16:52:57', '2016-09-15 08:13:15'),
(6, 7, '9004123', 'Watsapbroh', 3, 2, 4, 4, 0, '1', 6, '2016-09-07 16:54:12', '2016-09-07 16:54:12'),
(7, 8, '8575919', 'watsapmamen', 3, 2, 5, 4, 0, '1', 1, '2016-09-07 16:58:00', '2016-09-14 21:40:05'),
(8, 9, '123456', 'IBK', 0, NULL, 0, 0, 0, '1', 1, '2016-09-08 08:25:49', '2016-09-08 08:25:49'),
(9, 12, '113070300', 'Staff IBS', 2, 1, 3, 4, 0, '1', 1, '2016-09-14 14:00:10', '2016-10-03 10:04:43'),
(10, 13, '12423', 'gedf', 2, 1, 3, 4, 0, '1', 1, '2016-09-14 14:03:34', '2016-09-14 14:03:34'),
(11, 14, '3425', 'Dul Latif', 3, 0, 6, 4, 0, '1', 1, '2016-09-14 14:13:04', '2016-09-14 14:13:04'),
(12, 15, '909090', 'Staff IBK', 3, 1, 1, 4, 1, '1', 1, '2016-09-14 18:41:53', '2016-09-16 11:01:47'),
(13, 17, '900211', 'staff', 3, 1, 3, 4, 0, '0', 1, '2016-09-16 11:03:54', '2016-09-16 11:04:17'),
(14, 20, '113070321', 'Ririn Zulandra', 3, 1, 1, 4, 0, '1', 1, '2016-09-20 15:02:31', '2016-09-20 15:02:31'),
(15, 26, '1112222', 'Madu', 3, 1, 3, 4, 0, '1', 2, '2016-09-22 13:10:20', '2016-09-28 13:45:09'),
(16, 27, '12243254', 'Test Edit', 3, 1, 3, 1, 0, '0', 1, '2016-09-22 13:11:50', '2016-10-12 10:08:22'),
(17, 31, '09809801', 'wildanab', 3, 1, 3, 2, 0, '1', 1, '2016-09-28 15:50:29', '2016-10-12 10:08:49'),
(18, 35, '900506', 'Siti Rahmah Madusari', 3, 2, 5, 3, 3, '1', 1, '2016-10-12 11:19:21', '2016-10-18 18:56:38'),
(19, 40, '34234sdff', 'cobaa', 3, 2, 11, 3, 0, '1', 1, '2016-10-18 20:27:33', '2016-10-18 20:27:33'),
(20, 41, '3243', 'cobaa', 3, 2, 11, 3, 0, '1', 40, '2016-10-18 20:42:49', '2016-10-18 20:42:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id` int(10) unsigned NOT NULL,
  `code` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data untuk tabel `groups`
--

INSERT INTO `groups` (`id`, `code`, `name`, `permissions`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 'ac', 'Admin Content', 'manage-company:41,manage-user:42,manage-web:44,manage-content-type:46,manage-sub-content-type:47', '1', 1, '2016-09-07 02:15:30', '2016-09-07 06:50:55'),
(2, 'au', 'Admin User', 'manage-company:41,manage-area:108,manage-office:109,manage-division:110,manage-department:111,manage-position:112,manage-user:42,manage-group:113,manage-permission-group:114,manage-permission:115', '1', 1, '2016-09-07 02:15:30', '2016-10-14 11:55:16'),
(3, 'ap', 'Admin Purchase', 'manage-company:41,manage-user:42,manage-category:43', '1', 1, '2016-09-07 02:15:30', '2016-09-07 06:42:15'),
(4, 'sm_ibk', 'SM IBK', 'approve/-reject-content-:13,red-list-vendor-approval:15,manage-redlist:56,black-list-vendor-approval:16,view-detail-vendor-black-list:64,reject-black-list:67,view-black-list-on-level-sm-ibk:68,manage-black-list:70,view-inactive/active:59,inactive/active-sm-ibk:60,view-vendor-list:71,cetak-berita-acara-evaluasi-sourcing:78,view-data -due-diligence:90,view-vendor-registration:92,approve-vendor-registration-2:131', '1', 1, '2016-09-07 02:15:30', '2016-10-12 03:08:49'),
(5, 'sm_ibs', 'SM IBS', 'approve/-reject-content-:13,red-list-vendor-approval:15,manage-redlist:56,black-list-vendor-approval:16,view-detail-vendor-black-list:64,reject-black-list:67,manage-black-list:70,approval-change-data-vendor-request-byavl:133,approval-change-data-vendor-request-byadmsrc:134,view-inactive/active:59,inactive/active-sm-ibs:61,view-vendor-list:71,vendor-list-export-excel:72,approve-vendor-registration-1:76,cetak-berita-acara-evaluasi-sourcing:78,view-data -due-diligence:90,view-vendor-registration:92,view-vendor-delete:125', '1', 1, '2016-09-07 02:15:30', '2016-09-28 11:52:14'),
(6, 'as', 'Admin Sourcing', 'add-vendor-to-red-list:23,manage-redlist:56,edit-data-vendor-redlist:57,cancel-vendor-redlist:58,add-vendor-to-black-list:22,edit-black-list:65,cancel-black-list:66,manage-black-list:70,update-vendor-data:18,approval-change-data-vendor-request-byavl:133,manage-company:41,manage-area:108,manage-office:109,manage-division:110,manage-department:111,manage-position:112,manage-employee:137,manage-functional:138,manage-category:43,manage-status-registration:45,manage-subcategory:116,due-diligence-assesment:118,list-document-evaluation:119,view-inactive/active:59,inactive/active-admin-sourcing:62,view-vendor-list:71,vendor-list-button-admin-sourcing:73,cetak-berita-acara-due-diligence:77,cetak-berita-acara-evaluasi-sourcing:78,finalization-vendor-registration:79,get-admin-sourcing-notification:80,manage-data-due-diligence-vendor:81,manage-status-registration:82,review-vendor-registration:84,recomendation-vendor-status:85,update-vendor-data:88,verification-bank-account:89,view-data -due-diligence:90,view-vendor-registration:92,create-vendor-button:128,reminder-button:130,view-vendor-delete-detail:126,create-vendor:127,view-vendor-document:135,view-promote-avl:144', '1', 1, '2016-09-07 02:15:30', '2016-10-18 11:56:38'),
(7, 'vp_ib', 'VP IB', 'approve/-reject-content-:13,black-list-vendor-approval:16,view-detail-vendor-black-list:64,reject-black-list:67,view-black-list-on-level-sm-vpib:69,manage-black-list:70,view-inactive/active:59,inactive/active-vp-ib:63,view-vendor-list:71,cetak-berita-acara-evaluasi-sourcing:78,view-data -due-diligence:90,view-vendor-registration:92,approve-vendor-registration-3:132', '1', 1, '2016-09-07 02:15:30', '2016-10-12 03:08:22'),
(8, 'vendor_avl', 'AVL Vendor', 'update-vendor-data:18', '1', 1, '2016-09-07 02:15:30', '2016-09-28 13:29:57'),
(9, 'vendor_unreg', 'Unregistered  vendor', '', '1', 1, '2016-09-07 02:15:30', '2016-09-07 02:15:30'),
(10, 'sm_ibr', 'SM IBR', 'approve/-reject-content-:13,red-list-vendor-approval:15,manage-redlist:56,black-list-vendor-approval:16,view-detail-vendor-black-list:64,reject-black-list:67,view-black-list-on-level-sm-ibk:68,manage-black-list:70,view-vendor-list:71,vendor-list-export-excel:72,vendor-list-button-admin-sourcing:73', '1', 1, '2016-09-07 02:15:30', '2016-09-23 04:06:25'),
(11, '', 'Super Admin', 'red-list-vendor-approval:15,add-vendor-to-red-list:23,manage-redlist:56,edit-data-vendor-redlist:57,cancel-vendor-redlist:58,black-list-vendor-approval:16,add-vendor-to-black-list:22,view-detail-vendor-black-list:64,edit-black-list:65,cancel-black-list:66,reject-black-list:67,view-black-list-on-level-sm-ibk:68,view-black-list-on-level-sm-vpib:69,manage-black-list:70,manage-company:41,manage-area:108,manage-office:109,manage-division:110,manage-department:111,manage-position:112,manage-employee:137,manage-functional:138,manage-user:42,manage-group:113,manage-permission-group:114,manage-permission:115,manage-category:43,manage-status-registration:45,manage-subcategory:116,due-diligence-assesment:118,list-document-evaluation:119,manage-home:44,manage-about-us:120,manage-contact:121,manage-faq:122,manage-vendor-policy:123,manage-terms-and-conditions:124,manage-content-type:46,manage-sub-content-type:47,manage-content-level:117,view-inactive/active:59,inactive/active-sm-ibk:60,inactive/active-sm-ibs:61,inactive/active-vp-ib:63,view-vendor-list:71,vendor-list-button-admin-sourcing:73,view-vendor-document:135,view-manage-menu:139,view-promote-avl:144', '1', 1, '2016-09-07 02:15:30', '2016-10-18 01:45:50'),
(13, 'stf_wa1', 'WA-1', 'input-recon-number:83,view-verification:91', '1', 1, '2016-09-07 02:15:30', '2016-09-08 08:47:09'),
(14, 'stf_wa2', 'WA-2', 'input-sap-id:86,view-verification:91', '1', 1, '2016-09-07 02:15:30', '2016-09-07 02:36:29'),
(16, 'stf_ibk', 'Staff IBK', 'create-content:1,update-content-:2,review-content:9,publish-content:10,delete-content-:40,manage-content-type:46,manage-sub-content-type:47,manage-content-level:117,input-srm-account:87,view-verification:91', '1', 17, '2016-09-07 02:15:30', '2016-09-14 11:56:30'),
(17, '', 'Admin Content IBS', '', '1', 17, '2016-09-07 02:15:30', '2016-09-07 02:15:30'),
(18, '', 'Admin Content IBR', '', '1', 17, '2016-09-07 02:15:30', '2016-09-07 02:15:30'),
(19, '', 'Admin Content BP IB', '', '1', 17, '2016-09-07 02:15:30', '2016-09-07 02:15:30'),
(21, 'stf_ibs', 'Staff IBS', 'create-content:1,update-content-:2,review-content:9,publish-content:10,delete-content-:40,manage-company:41,manage-area:108,manage-office:109,manage-division:110,manage-department:111,manage-position:112,manage-user:42,manage-group:113,manage-permission-group:114,manage-permission:115,manage-category:43,manage-status-registration:45', '1', 1, '2016-09-14 06:47:34', '2016-10-03 03:04:43'),
(22, 'adm_asyst', 'Admin Asyst', 'update-content-:2', '1', 0, '2016-09-19 17:00:00', '2016-10-12 04:31:27'),
(24, 'mgr_new', 'MGR NEW', 'add-vendor-to-red-list:23,manage-redlist:56', '0', 40, '2016-10-18 13:42:49', '2016-10-18 13:43:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE IF NOT EXISTS `level` (
`id` smallint(5) unsigned NOT NULL,
  `id_group` smallint(5) NOT NULL,
  `id_subcontent` smallint(5) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `is_last` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=122 ;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id`, `id_group`, `id_subcontent`, `remark`, `created_id`, `created_date`, `last_updated`, `is_last`) VALUES
(16, 1, 4, '', 1, '2016-05-31 16:30:23', '2016-09-09 15:52:37', '0'),
(17, 1, 5, '', 1, '2016-05-31 16:30:33', '2016-09-09 15:52:37', '0'),
(21, 3, 5, '', 1, '2016-05-31 16:32:33', '2016-09-09 15:52:37', '0'),
(34, 10, 5, '', 17, '2016-08-03 06:00:03', '2016-08-03 06:00:03', '1'),
(39, 18, 5, '', 17, '2016-08-07 15:25:41', '2016-08-07 15:25:41', '0'),
(40, 18, 5, '', 17, '2016-08-07 15:39:19', '2016-08-07 15:39:19', '0'),
(41, 17, 4, '', 17, '2016-08-07 22:23:57', '2016-08-07 22:23:57', '0'),
(42, 17, 5, '', 17, '2016-08-07 22:23:57', '2016-08-07 22:23:57', '0'),
(56, 1, 4, '', 17, '2016-08-09 11:40:44', '2016-08-09 11:40:44', '0'),
(58, 1, 5, '', 17, '2016-08-09 11:40:59', '2016-08-09 11:40:59', '0'),
(62, 1, 5, '', 17, '2016-08-09 23:41:14', '2016-08-09 23:41:14', '0'),
(84, 16, 1, '', 1, '2016-09-08 08:30:04', '2016-09-08 08:30:04', '0'),
(85, 16, 2, '', 1, '2016-09-08 08:30:04', '2016-09-08 08:30:04', '0'),
(86, 4, 1, '', 1, '2016-09-14 11:30:47', '2016-09-14 11:30:47', '1'),
(87, 4, 2, '', 1, '2016-09-14 11:30:47', '2016-09-14 11:30:47', '1'),
(91, 7, 3, '', 1, '2016-09-14 11:35:46', '2016-09-14 11:35:46', '0'),
(92, 7, 5, '', 1, '2016-09-14 11:35:46', '2016-09-14 11:35:46', '0'),
(93, 3, 3, '', 1, '2016-09-14 11:42:55', '2016-09-14 11:42:55', '0'),
(94, 21, 3, '', 1, '2016-09-14 20:33:54', '2016-09-14 20:33:54', '0'),
(95, 21, 4, '', 1, '2016-09-14 20:33:54', '2016-09-14 20:33:54', '0'),
(96, 21, 5, '', 1, '2016-09-14 20:33:54', '2016-09-14 20:33:54', '0'),
(97, 11, 1, '', 1, '2016-09-15 10:16:02', '2016-09-15 10:16:02', '0'),
(98, 11, 2, '', 1, '2016-09-15 10:16:02', '2016-09-15 10:16:02', '0'),
(99, 11, 3, '', 1, '2016-09-15 10:16:02', '2016-09-15 10:16:02', '0'),
(100, 11, 4, '', 1, '2016-09-15 10:16:02', '2016-09-15 10:16:02', '0'),
(101, 11, 5, '', 1, '2016-09-15 10:16:02', '2016-09-15 10:16:02', '0'),
(102, 21, 6, '', 1, '2016-09-15 00:00:00', '2016-09-23 00:00:00', '0'),
(104, 5, 3, '', 1, '2016-10-12 07:48:22', '2016-10-12 07:48:22', '1'),
(105, 5, 4, '', 1, '2016-10-12 07:48:22', '2016-10-12 07:48:22', '1'),
(106, 5, 5, '', 1, '2016-10-12 07:48:22', '2016-10-12 07:48:22', '1'),
(107, 5, 6, '', 1, '2016-10-12 07:48:22', '2016-10-12 07:48:22', '1'),
(108, 22, 1, '', 1, '2016-10-13 10:31:21', '2016-10-13 10:31:21', '0'),
(109, 22, 3, '', 1, '2016-10-13 10:31:21', '2016-10-13 10:31:21', '0'),
(110, 22, 4, '', 1, '2016-10-13 10:31:21', '2016-10-13 10:31:21', '0'),
(116, 13, 4, '', 1, '2016-10-17 09:47:09', '2016-10-17 09:47:09', '0'),
(118, 13, 4, '', 1, '2016-10-17 09:48:33', '2016-10-17 09:48:33', '0'),
(119, 13, 4, '', 1, '2016-10-17 16:24:28', '2016-10-17 16:24:28', '0'),
(121, 13, 5, '', 1, '2016-10-18 16:02:27', '2016-10-18 16:02:27', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
`id` int(11) NOT NULL,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=483 ;

--
-- Dumping data untuk tabel `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(469, '172.25.138.183', 'muhammad.hakim@asyst.co.id', '2016-10-18 08:51:02'),
(470, '172.25.138.183', 'adminsourcing.ga@gmail.com', '2016-10-18 09:25:30'),
(475, '172.25.139.132', 'wa1', '2016-10-18 14:06:56'),
(476, '172.25.139.132', 'wa-1', '2016-10-18 14:07:01'),
(477, '172.25.139.132', 'ibk', '2016-10-18 14:15:43'),
(478, '172.25.139.132', 'staffibk', '2016-10-18 14:19:28'),
(479, '172.25.139.132', 'staff.ibk', '2016-10-18 14:19:33'),
(482, '172.25.139.57', 'smibk.ga', '2016-10-19 03:26:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `member`
--

CREATE TABLE IF NOT EXISTS `member` (
`id` smallint(5) unsigned NOT NULL,
  `userID` smallint(5) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `id_office` smallint(5) NOT NULL,
  `id_dept` smallint(5) NOT NULL,
  `id_level` smallint(5) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `member`
--

INSERT INTO `member` (`id`, `userID`, `firstname`, `lastname`, `fullname`, `id_office`, `id_dept`, `id_level`, `created_id`, `created_date`, `last_updated`) VALUES
(2, 6, 'anggun', 'wibowo', 'anggun wibowo', 3, 1, 2, 1, '2016-04-19 05:26:17', '2016-04-19 05:26:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu1`
--

CREATE TABLE IF NOT EXISTS `menu1` (
`id` smallint(5) unsigned NOT NULL,
  `menu1` varchar(25) NOT NULL,
  `class` varchar(50) NOT NULL,
  `url` varchar(30) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `menu1`
--

INSERT INTO `menu1` (`id`, `menu1`, `class`, `url`, `status`, `created_date`) VALUES
(1, 'Dashboard', 'fa fa-dashboard', 'home/', '1', '2016-07-20 16:18:25'),
(2, 'Content Management', 'fa fa-bullhorn fa-2x', 'announce/', '1', '2016-07-20 16:18:25'),
(3, 'Vendor Management', 'fa fa-folder-open', '#', '1', '2016-07-20 16:18:25'),
(4, 'Procurement Management', 'fa fa-truck', 'bidding/', '1', '2016-07-20 16:18:25'),
(5, 'Sourcing Management', 'fa fa-group', 'sourcing/', '1', '2016-07-20 16:18:25'),
(6, 'Master Data', 'fa fa-cogs', '#', '1', '2016-07-20 16:18:25'),
(7, 'Mailbox', 'fa fa-envelope', '#', '1', '2016-07-20 16:18:25'),
(8, 'Report', 'fa fa-files-o', '#', '1', '2016-07-26 00:00:00'),
(9, 'News (Blast)', 'fa fa-bullhorn', 'avl/news', '1', '2016-07-26 00:00:00'),
(10, 'Vendor Profile', 'fa fa-industry', 'data_vendor', '1', '2016-09-09 15:52:37'),
(11, 'Procurement Transaction', 'fa fa-truck', '#', '1', '2016-09-09 15:52:37'),
(12, 'SAP Transaction', 'fa fa-file-archive-o', '#', '1', '2016-09-09 15:52:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu2`
--

CREATE TABLE IF NOT EXISTS `menu2` (
`id` smallint(5) unsigned NOT NULL,
  `id_up` smallint(5) NOT NULL,
  `menu2` varchar(30) NOT NULL,
  `class` varchar(15) DEFAULT NULL,
  `url` varchar(30) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data untuk tabel `menu2`
--

INSERT INTO `menu2` (`id`, `id_up`, `menu2`, `class`, `url`, `status`, `created_date`) VALUES
(1, 3, 'Vendor Registration', NULL, 'register/', '1', '2016-07-20 16:29:38'),
(2, 3, 'Verification List', NULL, 'verification/', '1', '2016-07-20 16:29:38'),
(3, 3, 'Vendor List', NULL, 'vendorlist', '1', '2016-07-20 16:29:38'),
(4, 3, 'Redlist Vendor', NULL, 'redlist', '1', '2016-07-20 16:29:38'),
(5, 3, 'Blacklist Vendor', NULL, 'blacklist', '1', '2016-07-20 16:29:38'),
(6, 6, 'Company', NULL, '#', '1', '2016-07-20 16:29:38'),
(7, 6, 'Manage User', NULL, '#', '1', '2016-07-20 16:29:38'),
(8, 6, 'Master Vendor', NULL, '#', '1', '2016-07-20 16:29:38'),
(9, 6, 'Manage Content', NULL, '#', '1', '2016-07-20 16:29:38'),
(10, 6, 'CMS', NULL, '#', '1', '2016-07-20 16:29:38'),
(11, 8, 'News', NULL, '#', '1', '2016-07-26 05:00:00'),
(12, 8, 'Registration', NULL, '#', '1', '2016-07-26 00:00:00'),
(13, 8, 'Procurement', NULL, '#', '1', '2016-07-26 00:00:00'),
(14, 11, 'Incoming Bidding', NULL, 'avl/current_bidding', '1', '2016-07-26 00:00:00'),
(15, 11, 'Historical Bidding', NULL, 'avl/history_bidding', '1', '2016-07-26 00:00:00'),
(16, 6, 'Due Diligence', NULL, '#', '1', '2016-08-05 00:00:00'),
(17, 3, 'Inactive Vendor', NULL, 'inactive', '0', '2016-07-20 16:29:38'),
(18, 3, 'Active Vendor', NULL, 'active', '0', '2016-07-20 16:29:38'),
(19, 6, 'Notification', NULL, '#', '1', '2016-09-01 00:00:00'),
(21, 3, 'Data Vendor', NULL, 'data_vendor', '1', '2016-07-20 16:29:38'),
(23, 3, 'Vendor Delete', NULL, 'vendordelete', '0', '2016-07-20 16:29:38'),
(24, 3, 'Request Vendor ', NULL, '#', '1', '2016-07-20 16:29:38'),
(25, 3, 'Vendor Document  ', NULL, 'document', '1', '2016-07-20 16:29:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu3`
--

CREATE TABLE IF NOT EXISTS `menu3` (
`id` smallint(5) unsigned NOT NULL,
  `id_up` smallint(5) NOT NULL,
  `menu3` varchar(30) NOT NULL,
  `jid` varchar(20) DEFAULT NULL,
  `url` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data untuk tabel `menu3`
--

INSERT INTO `menu3` (`id`, `id_up`, `menu3`, `jid`, `url`, `status`, `created_date`) VALUES
(1, 6, 'Country', NULL, 'country/', '1', '2016-07-20 16:29:46'),
(2, 6, 'Area', NULL, 'region/', '1', '2016-07-20 16:29:46'),
(3, 6, 'Office', NULL, 'offices/', '1', '2016-07-20 16:29:46'),
(4, 6, 'Division', NULL, 'division/', '1', '2016-07-20 16:29:46'),
(5, 6, 'Department', NULL, 'department/', '1', '2016-07-20 16:29:46'),
(6, 7, 'User', NULL, 'user/', '1', '2016-07-20 16:29:46'),
(7, 7, 'Groups', NULL, 'user/groups/', '1', '2016-07-20 16:29:46'),
(8, 7, 'Permission Group', NULL, 'user/permission_group', '1', '2016-07-20 16:29:46'),
(9, 8, 'Category', NULL, 'category/', '1', '2016-07-20 16:29:46'),
(10, 8, 'Subcategory', NULL, 'category/subcategory/', '1', '2016-07-20 16:29:46'),
(11, 8, 'Registration Status', NULL, 'reg_status/', '1', '2016-07-20 16:29:46'),
(12, 9, 'Content Type', NULL, 'content/', '1', '2016-07-20 16:29:46'),
(13, 9, 'Sub Content Type', NULL, 'content/subcontent/', '1', '2016-07-20 16:29:46'),
(16, 10, 'Home', NULL, 'cms/', '1', '2016-07-20 16:29:46'),
(18, 10, 'Contact', NULL, 'cms/contact/', '1', '2016-07-20 16:29:46'),
(19, 10, 'FAQs', NULL, 'faq/', '1', '2016-07-20 16:29:46'),
(20, 10, 'Vendor Policy', NULL, 'policy_agreement/', '1', '2016-07-20 16:29:46'),
(22, 6, 'Position', NULL, 'position/', '1', '2016-08-15 00:00:00'),
(23, 6, 'Functional', NULL, 'functional/', '1', '2016-08-15 00:00:00'),
(24, 8, 'Vendor Document Requirement', NULL, 'notification/doc_req/', '1', '2016-09-01 00:00:00'),
(25, 7, 'Permissions', NULL, 'user/permissions/', '1', '2016-09-01 00:00:00'),
(26, 9, 'Content Level', NULL, 'level/', '1', '2016-09-01 00:00:00'),
(27, 10, 'Terms and Conditions', NULL, 'cms/terms', '1', '2016-09-05 00:00:00'),
(28, 8, 'Due Diligence Assessment', NULL, 'assesment/', '1', '2016-09-05 00:00:00'),
(29, 24, 'Delete Vendor', NULL, 'vendordelete', '1', '2016-09-05 00:00:00'),
(30, 24, 'Deactivation', NULL, 'inactive', '1', '2016-09-05 00:00:00'),
(31, 24, 'Activation', NULL, 'active', '1', '2016-09-05 00:00:00'),
(32, 10, 'Techincal Guidance', NULL, 'cms/technical', '1', '2016-09-19 00:00:00'),
(33, 24, 'Change Data', NULL, 'change_request_avl', '1', '2016-09-05 00:00:00'),
(34, 24, 'Data Change By ADM SRC', NULL, 'change_request', '1', '2016-09-05 00:00:00'),
(35, 6, 'Employee', '', 'employee/', '1', '2016-10-03 00:00:00'),
(36, 8, 'Notifications', NULL, 'notification/', '1', '0000-00-00 00:00:00'),
(37, 24, 'Promote to AVL', NULL, 'vendorpromote', '1', '2016-09-05 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_feature`
--

CREATE TABLE IF NOT EXISTS `menu_feature` (
`id` smallint(5) unsigned NOT NULL,
  `id_menu` smallint(5) NOT NULL,
  `level` smallint(1) NOT NULL,
  `groups` varchar(100) NOT NULL,
  `id_dept` varchar(30) NOT NULL,
  `id_sect` varchar(30) NOT NULL,
  `id_subsect` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Dumping data untuk tabel `menu_feature`
--

INSERT INTO `menu_feature` (`id`, `id_menu`, `level`, `groups`, `id_dept`, `id_sect`, `id_subsect`, `created_date`) VALUES
(1, 1, 1, '1,2,3,4,5,6,7,10,11,12,13,14,16,17,18,19,21,24', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(2, 2, 1, '11,1,5,4,10,12,7,16,17,18,19,21,22', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(3, 3, 1, '11,5,4,7', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(4, 4, 1, '11,3,5,4,10,12,21', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(5, 5, 1, '11,6,5,4,10,12,21', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(6, 6, 1, '11,2,21', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(7, 7, 1, '1,2,3,4,7,10,11,12,13,14,16,17,18,19,21', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(8, 1, 2, '11,6,5,4,10,12,7,16', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(9, 2, 2, '11,13,14,16', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(10, 3, 2, '11,5,4,10,12,7,16,6', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(11, 4, 2, '11,6,5,4,10,12,7,16', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(12, 5, 2, '11,6,5,4,10,12,7,16', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(13, 6, 2, '11,2,21', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(14, 7, 2, '11,2,21', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(15, 8, 2, '11,3,21', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(16, 9, 2, '11', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(17, 10, 2, '11,1,16,17,18,19', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(18, 1, 3, '11,2,6,24', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(19, 2, 3, '11,2,21,6,24', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(20, 3, 3, '11,2,21,6,24', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(21, 4, 3, '11,2,21,6,24', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(22, 5, 3, '11,2,21,6,24', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(23, 6, 3, '11,2,21', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(24, 7, 3, '11,2,21', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(25, 8, 3, '11,2,21', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(26, 9, 3, '11,6,3,21', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(27, 10, 3, '11,6,3', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(28, 11, 3, '11,6,21', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(29, 12, 3, '11,1,16,17,18,19', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(30, 13, 3, '11,1,16,17,18,19', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(31, 14, 3, '11,1,16,17,18,19', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(32, 15, 3, '11,1,16,17,18,19', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(33, 16, 3, '11,1,16,17,18,19', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(34, 17, 3, '11,1,16,17,18,19', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(35, 18, 3, '11,1,16,17,18,19', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(36, 19, 3, '11,1,16,17,18,19', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(37, 20, 3, '11,1,16,17,18,19', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(38, 8, 1, '12,4,10,12,7', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(39, 9, 1, '8', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(40, 10, 1, '8', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(41, 11, 1, '8', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(42, 12, 1, '8', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(43, 11, 2, '12,4,10,12,7', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(44, 12, 2, '12,4,10,12,7\r\n', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(45, 13, 2, '12,4,10,12,7\r\n', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(46, 14, 2, '8', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(47, 15, 2, '8', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(49, 21, 3, '', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(50, 22, 3, '11,2,21,6,24', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(51, 23, 3, '11,2,23,24', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(52, 17, 2, '', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(53, 18, 2, '', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(54, 24, 3, '11', 'all', 'all', 'all', '2016-09-09 15:52:37'),
(55, 25, 3, '11,21', 'all', 'all', 'all', '2016-09-01 00:00:00'),
(56, 26, 3, '11', 'all', 'all', 'all', '2016-09-01 00:00:00'),
(57, 27, 3, '11', 'all', 'all', 'all', '2016-09-05 00:00:00'),
(58, 28, 3, '11', 'all', 'all', 'all', '2016-09-05 00:00:00'),
(59, 21, 2, '6', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(61, 23, 2, '', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(62, 29, 3, '5,11', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(63, 30, 3, '4,5,6,7,11', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(64, 31, 3, '4,5,6,7,11', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(65, 32, 3, '11', 'all', 'all', 'all', '2016-09-19 00:00:00'),
(66, 33, 3, '5,6', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(67, 34, 3, '5', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(68, 35, 3, '11,6,24', 'all', 'all', 'all', '2016-10-03 00:00:00'),
(69, 25, 2, '6,11', 'all', 'all', 'all', '2016-07-21 14:43:51'),
(70, 36, 3, '11', 'all', 'all', 'all', '2016-10-05 09:24:23'),
(71, 37, 3, '11,6', 'all', 'all', 'all', '2016-07-21 14:43:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `non_visit_files`
--

CREATE TABLE IF NOT EXISTS `non_visit_files` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `non_visit_files`
--

INSERT INTO `non_visit_files` (`id`, `id_vendor`, `filename`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 53, 'gtkGrey.png', 2, '2016-10-04 10:16:30', '2016-10-04 10:16:30'),
(3, 48, 'gtkGrey_(1).png', 2, '2016-10-04 10:32:52', '2016-10-04 10:32:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `non_visit_notes`
--

CREATE TABLE IF NOT EXISTS `non_visit_notes` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `note` varchar(100) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `non_visit_notes`
--

INSERT INTO `non_visit_notes` (`id`, `id_vendor`, `note`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 53, '	sjfgsdkjfgsdkfgsdkj', 2, '2016-10-04 10:16:30', '2016-10-04 10:16:30'),
(2, 48, '	werewqrewqr', 2, '2016-10-04 10:26:38', '2016-10-04 10:26:38'),
(3, 54, 'testing jaj', 2, '2016-10-04 10:43:23', '2016-10-04 10:43:23'),
(4, 56, '	No recomend lagi', 1, '2016-10-06 13:51:05', '2016-10-06 13:51:05'),
(5, 61, '		coba kita edit lagi deh', 2, '2016-10-06 16:04:43', '2016-10-06 16:04:43'),
(6, 65, 'NO need for visiting', 2, '2016-10-11 09:38:47', '2016-10-11 09:38:47'),
(7, 66, '		noneni', 2, '2016-10-11 15:36:42', '2016-10-11 15:36:42'),
(8, 64, 'No recommend', 2, '2016-10-12 07:37:32', '2016-10-12 07:37:32'),
(9, 68, '	noneggggggggg', 2, '2016-10-13 11:29:44', '2016-10-13 11:29:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
`id` smallint(5) NOT NULL,
  `desc_ind` varchar(500) NOT NULL,
  `desc_eng` varchar(500) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `notification`
--

INSERT INTO `notification` (`id`, `desc_ind`, `desc_eng`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(0, 'apa', 'aja', '0', 17, '2016-08-30 19:07:32', '2016-10-14 14:34:10'),
(1, 'Salinan Akta Pendirian Perusahaan beserta perubahannya', 'English', '0', 17, '2016-08-30 16:19:49', '2016-08-30 16:19:49'),
(2, 'Salinan Akta Pendirian Perusahaan beserta perubahannya', 'A copy of the Deed of Establishment and its amendment', '1', 1, '2016-08-30 16:20:30', '2016-09-08 07:58:05'),
(3, 'test', 'test', '0', 17, '2016-08-30 16:55:05', '2016-10-06 14:09:51'),
(4, 'indonesia', 'english', '1', 17, '2016-08-30 18:50:11', '2016-08-31 09:40:25'),
(5, 'deskripsi indonesia', 'description english', '1', 17, '2016-08-30 18:58:14', '2016-08-31 09:40:18'),
(7, 'Indogo', 'Eigo', '1', 1, '2016-10-14 14:32:54', '2016-10-14 14:32:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
`id` smallint(5) unsigned NOT NULL,
  `notif_number` varchar(15) NOT NULL,
  `notifications` varchar(150) NOT NULL,
  `path` varchar(50) NOT NULL,
  `editable` enum('false','true') NOT NULL DEFAULT 'false',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Dumping data untuk tabel `notifications`
--

INSERT INTO `notifications` (`id`, `notif_number`, `notifications`, `path`, `editable`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 'EPCGA001', 'NOTIFIKASI PERTAMA KALIREGISTRASI', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-14 15:06:44'),
(2, 'EPCGA002', 'NOTIFIKASI PERUSAHAAN TERSEBUT DITINDAKLANJUTI KEPROSES SELANJUTNYA', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(3, 'EPCGA003', 'NOTIFIKASI PERUSAHAAN TERSEBUT TIDAK DITINDAKLANJUTI KEPROSES SELANJUTNYA ', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(4, 'EPCGA004', 'NOTIFIKASI DOKUMEN PERUSAHAAN TERSEBUT TELAH DITERIMA GARUDA', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(5, 'EPCGA005', 'NOTIFIKASI PERMINTAAN PERSETUJUAN EVALUASI VENDOR KE IBS/IBK/IB', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(6, 'EPCGA006', 'NOTIFIKASI REJECTED USULAN EVALUASI VENDOR DARI IBS/IBK/IB KE ADMIN SOURCING', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(7, 'EPCGA007', 'NOTIFIKASI VERIFIKASI INFORMASI BANK KE VENDOR', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(8, 'EPCGA008', 'NOTIFIKASI KE ADMIN SOURCING BAHWA VENDOR TELAH MENGISI BANK ACCOUNT', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(9, 'EPCGA009', 'NOTIFIKASI KE ADMIN SOURCING BAHWA VENDOR BELUM MENGISI BANK ACCOUNT', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(10, 'EPCGA010', 'NOTIFIKASI PERMINTAAN RECON KE WA1', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(11, 'EPCGA011', 'NOTIFIKASI PERMINTAAN NOMOR VENDOR KE WA2 ', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(12, 'EPCGA012', 'NOTIFIKASI PERMINTAAN VALIDASI NOMOR VENDOR/MAPPING ID KE IBK', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(13, 'EPCGA013', 'NOTIFIKASI VALIDASI ID COMPLETED KE ADMIN SOURCING', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(14, 'EPCGA014', 'NOTIFIKASI PERUSAHAAN TERSEBUT DITERIMA AVL DAN HARUS AKTIFASI', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(15, 'EPCGA015', 'NOTIFIKASI PERUSAHAAN TERSEBUT MENJADI SHORTLIST', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(16, 'EPCGA016', 'NOTIFIKASI PERUSAHAAN TERSEBUT MENJADI REJECTED', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(17, 'EPCGA017', 'NOTIFIKASI PERMINTAAN APPROVAL RED LIST', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(18, 'EPCGA018', 'NOTIFIKASI PERUSAHAAN YANG MASUK KE REDLIST', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(19, 'EPCGA019', 'NOTIFIKASI PERUSAHAAN REDLIST APPROVED KE ADMIN SOURCING', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(20, 'EPCGA020', 'NOTIFIKASIPERUSAHAAN DITOLAK REDLIST KE ADMIN SOURCING', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(21, 'EPCGA021', 'NOTIFIKASI PERUSAHAAN REDLIST KEMBALI AKTIF', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(22, 'EPCGA022', 'NOTIFIKASI PERMINTAAN APPROVAL BLACKLIST(SM IBS/SM IBK/VP IB)', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(23, 'EPCGA023', 'NOTIFIKASI PERUSAHAANBLACKLIST APPROVED KE ADMIN SOURCING', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(24, 'EPCGA024', 'NOTIFIKASIPERUSAHAAN DITOLAK BLACKLIST KE ADMIN SOURCING', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(25, 'EPCGA025', 'NOTIFIKASIPERUSAHAAN YANG MASUK KE BLACKLIST', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(26, 'EPCGA026', 'NOTIFIKASI PERMINTAAN APPROVAL VENDOR MENJADI INACTIVE (SM IBS/SM IBK/VP IB)', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(27, 'EPCGA027', 'NOTIFIKASI PERUSAHAAN INACTIVE KE ADMIN SOURCING', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(28, 'EPCGA028', 'NOTIFIKASIPERUSAHAAN DITOLAK INACTIVE KE ADMIN SOURCING', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(29, 'EPCGA029', 'NOTIFIKASIPERMINTAAN APPROVAL KONTEN', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(30, 'EPCGA030', 'NOTIFIKASI KONTEN APPROVED', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(31, 'EPCGA031', 'NOTIFIKASI KONTEN REJECTED', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(32, 'EPCGA032', 'NOTIFIKASIPERUSAHAAN YANG MASUK BIDDING', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(33, 'EPCGA033', 'NOTIFIKASI PENDAFTARAN BIDDING (BERHASIL)', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(34, 'EPCGA034', 'NOTIFIKASI APPROVED PARTICIPANT', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(35, 'EPCGA035', 'NOTIFIKASI REJECTED PARTICIPANT', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(36, 'EPCGA036', 'NOTIFIKASI BIDDING CANCELED', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(37, 'EPCGA037', 'NOTIFIKASI PERMINTAAN APPROVAL PEMENANG BIDDING', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(38, 'EPCGA038', 'NOTIFIKASI PEMENANG BIDDING APPROVED', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(39, 'EPCGA039', 'NOTIFIKASI PEMENANG BIDDING REJECTED', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(40, 'EPCGA040', 'NOTIFIKASI PEMENANG BIDDING', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(41, 'EPCGA041', 'NOTIFIKASITERIMAKASIH KEPADA PESERTA  BIDDING', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56'),
(42, 'EPCGA042', 'NOTIFIKASI PERMINTAAN REKOMENDASI REJECT EVALUASI VENDOR KE IBS', '', 'false', '1', 1, '2016-10-10 14:15:48', '2016-10-10 14:15:56'),
(43, 'EPCGA043', 'NOTIFIKASI PERMINTAAN REKOMENDASI APPROVAL EVALUASI VENDOR KE IBS	', '', 'false', '1', 1, '2016-10-10 14:15:52', '2016-10-10 14:16:02'),
(44, 'EPCGA044', 'NOTIFIKASI PERMINTAAN APPROVAL VENDOR MENJADI ACTIVE (SM IBS/SM IBK/VP IB)', '', 'false', '1', 1, '2016-10-14 00:00:00', '2016-10-14 00:00:00'),
(45, 'EPCGA045', 'NOTIFIKASI PERMINTAAN DELETE VENDOR (SM IBS)', '', 'false', '1', 1, '2016-10-14 00:00:00', '2016-10-14 00:00:00'),
(46, 'EPCGA046', 'NOTIFIKASI PERUSAHAAN ACTIVE KE ADMIN SOURCING', '', 'false', '1', 1, '2016-10-14 00:00:00', '2016-10-14 00:00:00'),
(47, 'EPCGA047', 'NOTIFIKASI PERUSAHAAN DITOLAK ACTIVE KE ADMIN SOURCING', '', 'false', '1', 1, '2016-10-14 00:00:00', '2016-10-14 00:00:00'),
(48, 'EPCGA048', 'NOTIFIKASI DELETE VENDOR KE ADMIN SOURCING', '', 'false', '1', 1, '2016-10-14 00:00:00', '2016-10-14 00:00:00'),
(49, 'EPCGA049', 'testing', 'test', 'false', '1', 1, '2016-10-17 11:34:11', '2016-10-17 11:34:34'),
(50, 'EPCGA050', '5we5w45', '4545 zssxsx', 'false', '0', 1, '2016-10-18 16:40:49', '2016-10-18 16:42:47'),
(51, 'EPCGA051', 'NOTIFIKASI PROSES REDLIST DI BATALKAN', '', 'false', '1', 1, '2016-10-05 16:15:56', '2016-10-05 16:15:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `offices`
--

CREATE TABLE IF NOT EXISTS `offices` (
`id` smallint(5) unsigned NOT NULL,
  `id_country` smallint(5) NOT NULL,
  `id_region` smallint(5) NOT NULL,
  `id_company` smallint(5) NOT NULL,
  `office_code` varchar(10) NOT NULL,
  `office_name` varchar(100) NOT NULL,
  `email` varchar(40) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `offices`
--

INSERT INTO `offices` (`id`, `id_country`, `id_region`, `id_company`, `office_code`, `office_name`, `email`, `phone`, `fax`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 3, 1, 1, 'HQ', 'Head Quarter', 'hq@garuda.co.id', '021-8871641', '021-8871641', '1', 17, '2016-08-12 14:12:40', '2016-09-01 13:20:41'),
(2, 3, 1, 1, 'CGK', 'Cengkareng', 'cgk@garuda.co.id', '0343451', '024234653', '1', 1, '2016-09-09 15:52:37', '2016-10-12 08:53:35'),
(3, 1, 1, 1, 'GCC', 'Garuda City Center', 'gcc@garuda.co.id', '', '', '1', 1, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(5, 3, 25, 1, 'GMF', 'Garuda MF', 'gmf@garuda.co.id', '0135665', '00564564', '1', 1, '2016-10-11 13:47:37', '2016-10-12 08:52:12'),
(6, 3, 1, 1, 'GSG', 'Garuda SG', 'gsg@garuda.co.id', '0248', '20651', '0', 1, '2016-10-11 14:34:47', '2016-10-11 14:34:47'),
(7, 4, 19, 1, 'GSO', 'Garuda SO', 'gso@garuda.co.id', '012043', '02435', '1', 1, '2016-10-12 08:51:13', '2016-10-12 08:51:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `organization`
--

CREATE TABLE IF NOT EXISTS `organization` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `position` varchar(50) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `organization`
--

INSERT INTO `organization` (`id`, `id_vendor`, `name`, `address`, `phone`, `position`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 3, 'Richo Mahardika', 'Bali', '09876256898', 'Tukang Koding', 'Programmer', '1', 2, '2016-09-09 15:49:41', '2016-09-09 15:49:41'),
(3, 5, 'Somad', 'Tangerang', '02129357777', 'OB', 'Cleaner', '1', 2, '2016-09-09 18:57:33', '2016-09-09 18:57:33'),
(5, 18, 'ALSDHILA', 'DKJG;EJG', 'JDSAG;JWREIG', 'DJG;IDJ', 'EEQKJ', '1', 2, '2016-09-20 17:54:12', '2016-09-20 17:54:12'),
(6, 17, 'lk', 'jklj', 'klj', 'klj', 'klj', '1', 2, '2016-09-30 11:15:47', '2016-09-30 11:15:54'),
(7, 17, 'lkj', 'lkj', 'lkj', 'klj', 'lkjl', '0', 19, '2016-09-30 11:19:59', '2016-09-30 11:21:38'),
(9, 22, 'WISNU', 'PEMBANGUNAN JAYA', '28751038', 'SMIBS', 'TAMBAH PENGURUS', '1', 2, '2016-10-12 13:57:06', '2016-10-12 13:57:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `owner`
--

CREATE TABLE IF NOT EXISTS `owner` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `owner_name` varchar(50) NOT NULL,
  `owner_address` varchar(100) NOT NULL,
  `owner_phone` varchar(20) NOT NULL,
  `owner_position` varchar(50) NOT NULL,
  `owner_shared` smallint(5) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `owner`
--

INSERT INTO `owner` (`id`, `id_vendor`, `owner_name`, `owner_address`, `owner_phone`, `owner_position`, `owner_shared`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 3, 'Ditya Firmansyah', 'Pamulang', '098736376426', 'CEO', 145, 'Tukang pukul', '1', 2, '2016-09-09 15:48:07', '2016-09-09 15:48:07'),
(2, 3, 'Muhammad Rasyid Ridho', 'Palembang', '09898763636', 'COO', 2, 'tukang protes', '1', 2, '2016-09-09 15:48:49', '2016-09-09 15:48:49'),
(4, 5, 'Darul', 'Purwokerto', '081910112425', 'COO', 50, 'Pemilik', '1', 2, '2016-09-09 18:57:09', '2016-09-09 18:57:09'),
(6, 18, 'JFLKASJ', 'ALAH', 'LF IAEHGL', 'AHGLIQH', 0, 'HGLQWHG', '1', 2, '2016-09-20 17:39:36', '2016-09-20 17:40:45'),
(7, 18, 'GWLFN', 'LQENMFLI', '3254235', 'DIREKTUR', 0, 'LWEJNE', '1', 2, '2016-09-26 09:59:20', '2016-09-26 09:59:20'),
(8, 17, 'hhjkh', 'kjhkj', 'hkj', 'hkj', 0, 'h', '1', 2, '2016-09-30 11:15:23', '2016-09-30 11:15:30'),
(11, 22, 'RICHO', 'M1', '9837T5198', 'HEAD', 50, 'NoneNUNONI', '1', 2, '2016-10-12 13:49:02', '2016-10-12 13:57:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
`id` int(10) unsigned NOT NULL,
  `code` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `id_function` smallint(5) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci DEFAULT '0',
  `created_id` smallint(5) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=145 ;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `code`, `id_function`, `name`, `value`, `description`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 'cc', 9, 'Create Content', 'create-content', 'Create Content', '1', 1, '2016-05-15 23:25:08', '2016-05-15 23:25:08'),
(2, 'uc', 9, 'Update Content ', 'update-content-', 'Update Content ', '1', 1, '2016-05-15 23:25:23', '2016-05-15 23:25:23'),
(9, '', 9, 'Review Content', 'review-content', 'Review Content', '1', 1, '2016-05-15 23:26:20', '2016-05-15 23:26:20'),
(10, '', 9, 'Publish Content', 'publish-content', 'Publish Content', '1', 1, '2016-05-15 23:26:27', '2016-05-15 23:26:27'),
(11, '', NULL, 'View Bidding Participant', 'view-bidding-participant', 'View Bidding Participant', '0', 1, '2016-05-15 23:26:35', '2016-05-15 23:26:35'),
(12, '', NULL, 'Approve or Reject Bidding Participant', 'approve-or-reject-bidding-participant', 'Approve or Reject Bidding Participant', '0', 1, '2016-05-15 23:27:04', '2016-05-15 23:27:04'),
(13, 'arc', 9, 'Approve/ Reject Content ', 'approve/-reject-content-', 'Approve/ Reject Content ', '1', 1, '2016-05-15 23:27:42', '2016-05-15 23:27:42'),
(15, 'vra', 3, 'Red list vendor approval', 'red-list-vendor-approval', 'Red list vendor approval', '1', 1, '2016-05-15 23:28:08', '2016-05-15 23:28:08'),
(16, 'approv', 5, 'Black list vendor approval', 'black-list-vendor-approval', 'Black list vendor approval', '1', 1, '2016-05-15 23:28:15', '2016-05-15 23:28:15'),
(17, '', NULL, 'Review vendor registration', 'review-vendor-registration', 'Review vendor registration', '0', 1, '2016-05-15 23:28:37', '2016-05-15 23:28:37'),
(18, 'upven', 10, 'Update vendor data', 'update-vendor-data', 'Update vendor data', '1', 1, '2016-05-15 23:28:47', '2016-05-15 23:28:47'),
(19, '', NULL, 'Upload  files', 'upload--files', 'Upload  files', '0', 1, '2016-05-15 23:28:56', '2016-05-15 23:28:56'),
(20, '', NULL, 'Finalization vendor registration', 'finalization-vendor-registration', 'Finalization vendor registration', '0', 1, '2016-05-15 23:29:03', '2016-05-15 23:29:03'),
(21, '', NULL, 'Approve vendor data changes', 'approve-vendor-data-changes', 'Approve vendor data changes', '0', 1, '2016-05-15 23:29:13', '2016-05-15 23:29:13'),
(22, 'add', 5, 'Add vendor to black list', 'add-vendor-to-black-list', 'Add vendor to black list', '1', 1, '2016-05-15 23:29:21', '2016-05-15 23:29:21'),
(23, 'avr', 3, 'Add vendor to red list', 'add-vendor-to-red-list', 'Add vendor to red list', '1', 1, '2016-05-15 23:29:28', '2016-05-15 23:29:28'),
(24, '', NULL, 'View vendor registration status', 'view-vendor-registration-status', 'View vendor registration status', '0', 1, '2016-05-15 23:29:46', '2016-05-15 23:29:46'),
(25, '', NULL, 'Change company profile', 'change-company-profile', 'Change company profile', '0', 1, '2016-05-15 23:30:03', '2016-05-15 23:30:03'),
(26, '', NULL, 'Change user profile', 'change-user-profile', 'Change user profile', '0', 1, '2016-05-15 23:30:10', '2016-05-15 23:30:10'),
(27, '', NULL, 'View SAP Transaction', 'view-sap-transaction', 'View SAP Transaction', '0', 1, '2016-05-15 23:30:17', '2016-05-15 23:30:17'),
(28, '', NULL, 'View Transaction', 'view-transaction', 'View Transaction', '0', 1, '2016-05-15 23:30:32', '2016-05-15 23:30:32'),
(29, '', NULL, 'View news', 'view-news', 'View news', '0', 1, '2016-05-15 23:30:40', '2016-05-15 23:30:40'),
(30, '', NULL, 'View announcement {Open Bidding, Open Sourcing}', 'view-announcement-{open-bidding,-open-sourcing}', 'View announcement {Open Bidding, Open Sourcing}', '0', 1, '2016-05-15 23:30:55', '2016-05-15 23:30:55'),
(31, '', NULL, 'FAQs', 'faqs', 'FAQs', '0', 1, '2016-05-15 23:31:02', '2016-05-15 23:31:02'),
(32, '', NULL, 'Self registration', 'self-registration', 'Self registration', '0', 1, '2016-05-16 01:30:15', '2016-05-16 01:30:15'),
(35, '', NULL, 'view vendor management', 'view-vendor-management', 'view vendor management', '0', 1, '2016-05-31 23:04:35', '2016-05-31 23:04:35'),
(36, '', NULL, 'Approve bidding/reject', 'approve-bidding-reject', 'Approve or reject bidding', '0', 1, '2016-07-14 20:58:32', '2016-07-14 20:58:32'),
(37, '', NULL, 'Input recon number', 'input-recon-number', 'Input recon number', '0', 1, '2016-07-22 02:37:47', '2016-07-22 02:37:47'),
(38, '', NULL, 'Input SAP ID', 'input-sap-id', 'Input SAP ID', '0', 1, '2016-07-22 02:38:55', '2016-07-22 02:38:55'),
(39, '', NULL, 'Unpublish Content', 'unpublish-content', 'Unpublish Content', '0', 1, '2016-07-22 02:39:55', '2016-07-22 02:39:55'),
(40, 'dc', 9, 'Delete content ', 'delete-content-', 'delete content ', '1', 1, '2016-07-22 03:01:59', '2016-07-22 03:01:59'),
(41, 'mc', 11, 'Manage Company', 'manage-company', 'Manage Company', '1', 4, '2016-07-24 23:11:53', '2016-07-24 23:11:53'),
(42, 'mu', 14, 'Manage User', 'manage-user', 'Manage User', '1', 4, '2016-07-24 23:13:13', '2016-07-24 23:13:13'),
(43, 'mcs', 12, 'Manage Category', 'manage-category', 'Manage Category', '1', 4, '2016-07-24 23:15:22', '2016-07-24 23:15:22'),
(44, 'mh', 13, 'Manage Home', 'manage-home', 'Manage Home', '1', 4, '2016-07-24 23:17:41', '2016-07-24 23:17:41'),
(45, 'msr', 12, 'Manage Status Registration', 'manage-status-registration', 'Manage Status Registration', '1', 4, '2016-07-24 23:18:51', '2016-07-24 23:18:51'),
(46, 'mct', 15, 'Manage Content Type', 'manage-content-type', 'Manage Content Type', '1', 4, '2016-07-24 23:19:15', '2016-07-24 23:19:15'),
(47, 'msct', 15, 'Manage Sub Content Type', 'manage-sub-content-type', 'Manage Sub Content Type', '1', 4, '2016-07-24 23:24:10', '2016-07-24 23:24:10'),
(48, '', NULL, 'Manage Document Requirement', 'manage-document-requirement', 'Manage Document Requirement', '0', 4, '2016-07-24 23:24:56', '2016-07-24 23:24:56'),
(49, '', NULL, 'Manage Notification', 'manage-notification', 'Manage Notification', '0', 4, '2016-07-24 23:25:42', '2016-07-24 23:25:42'),
(50, '', NULL, 'Select Winner', 'select-winner', 'Selecting WInner from Participants List', '0', 1, '2016-07-28 06:53:07', '2016-07-28 06:53:14'),
(51, '', NULL, 'Confirm Winner', 'confirm-winner', 'Confirm Winner from Winner List', '0', 1, '2016-07-28 06:53:53', '2016-07-28 06:54:00'),
(52, '', NULL, 'Input Vendor Account', 'input-vendor-account', 'Input Vendor Account', '0', 1, '2016-08-02 17:00:00', '2016-08-02 17:00:00'),
(53, '', NULL, 'Input SRM Account', 'input-srm-account', 'Input SRM Account', '0', 1, '2016-08-02 17:00:00', '2016-08-02 17:00:00'),
(54, '', NULL, 'Cetak Berita Acara', 'cetak-berita-acara', 'Cetak Berita Acara', '0', 1, '2016-08-08 17:00:00', '2016-08-08 17:00:00'),
(55, '', NULL, 'Download Excel Vendor', 'download-excel-vendor', 'Download Excel Vendor', '0', 1, '2016-08-09 14:29:38', '2016-08-09 14:29:42'),
(56, 'mnr', 3, 'Manage Redlist', 'manage-redlist', 'Manage Redlist', '1', 1, '2016-09-09 08:52:37', '2016-09-09 08:52:37'),
(57, 'evr', 3, 'Edit Data Vendor Redlist', 'edit-data-vendor-redlist', 'Edit Data Vendor Redlist', '1', 1, '2016-09-09 08:52:37', '2016-09-09 08:52:37'),
(58, 'cvr', 3, 'Cancel Vendor Redlist', 'cancel-vendor-redlist', 'Cancel Vendor Redlist', '1', 1, '2016-09-09 08:52:37', '2016-09-09 08:52:37'),
(59, 'vvi', 4, 'View Inactive/active', 'view-inactive/active', 'View Inactive/active', '1', 1, '2016-08-29 17:00:00', '2016-08-29 17:00:00'),
(60, 'iibk', 4, 'Inactive/active SM IBK', 'inactive/active-sm-ibk', 'Inactive/active SM IBK', '1', 1, '2016-08-29 17:00:00', '2016-08-29 17:00:00'),
(61, 'iibs', 4, 'Inactive/active SM IBS', 'inactive/active-sm-ibs', 'Inactive/active SM IBS', '1', 1, '2016-08-29 17:00:00', '2016-08-29 17:00:00'),
(62, 'ias', 4, 'Inactive/active Admin Sourcing', 'inactive/active-admin-sourcing', 'Inactive/active Admin Sourcing', '1', 1, '2016-08-29 17:00:00', '2016-08-29 17:00:00'),
(63, 'ivpib', 4, 'Inactive/active VP IB', 'inactive/active-vp-ib', 'Inactive/active VP IB', '1', 1, '2016-08-29 17:00:00', '2016-08-29 17:00:00'),
(64, 'viewv', 5, 'btn View Detail Vendor Black List', 'view-detail-vendor-black-list', 'btn View Detail Vendor Black List', '1', 17, '2016-08-30 17:00:00', '2016-08-30 17:00:00'),
(65, 'editb', 5, 'btn Edit Black List', 'edit-black-list', 'btn Edit Black List', '1', 17, '2016-08-30 17:00:00', '2016-08-30 17:00:00'),
(66, 'cancelb', 5, 'btn Cancel Black List', 'cancel-black-list', 'btn Cancel Black List', '1', 17, '2016-08-30 17:00:00', '2016-08-30 17:00:00'),
(67, 'reject', 5, 'btn Reject Black List', 'reject-black-list', 'btn Reject Black List', '1', 17, '2016-08-30 17:00:00', '2016-08-30 17:00:00'),
(68, 'smibk', 5, 'View Black List On Level SM IBK', 'view-black-list-on-level-sm-ibk', 'View Black List On Level SM IBK', '1', 17, '2016-08-30 17:00:00', '2016-08-30 17:00:00'),
(69, 'vpib', 5, 'View Black List On Level VP IB', 'view-black-list-on-level-sm-vpib', 'View Black List On Level VP IB', '1', 17, '2016-08-23 17:00:00', '2016-08-30 17:00:00'),
(70, 'm', 5, 'Manage Black List', 'manage-black-list', 'Manage Black List', '1', 17, '2016-08-30 17:00:00', '2016-08-30 17:00:00'),
(71, 'vvl', 6, 'View Vendor List', 'view-vendor-list', 'View Vendor List', '1', 17, '2016-09-01 04:31:13', '2016-09-01 04:31:13'),
(72, 'vlee', 6, 'Vendor List Export Excel', 'vendor-list-export-excel', 'Vendor List Export Excel', '1', 17, '2016-09-01 04:32:02', '2016-09-01 04:32:02'),
(73, 'vlbas', 6, 'Vendor List Button Admin Sourcing', 'vendor-list-button-admin-sourcing', 'Vendor List Button Admin Sourcing', '1', 17, '2016-09-01 04:32:57', '2016-09-01 04:32:57'),
(74, 'vvlog', 7, 'View Vendor Log', 'view-vendor-log', 'View Vendor Log', '1', 17, '2016-09-05 04:09:31', '2016-09-05 04:09:31'),
(75, 'logas', 7, 'Vendor Log Admin Sourcing', 'vendor-log-admin-sourcing', 'Vendor Log Admin Sourcing', '1', 17, '2016-09-05 04:09:54', '2016-09-05 04:09:54'),
(76, 'apvr1', 1, 'Approve vendor registration level-1', 'approve-vendor-registration-1', 'Approve vendor registration level-1', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(77, 'badd', 1, 'Cetak berita acara due diligence', 'cetak-berita-acara-due-diligence', 'Cetak berita acara due diligence', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(78, 'baes', 1, 'Cetak berita acara evaluasi sourcing', 'cetak-berita-acara-evaluasi-sourcing', 'Cetak berita acara evaluasi sourcing', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(79, 'fvr', 1, 'Finalization vendor registration', 'finalization-vendor-registration', 'Finalization vendor registration', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(80, 'gasn', 1, 'Get admin sourcing notification', 'get-admin-sourcing-notification', 'Get admin sourcing notification', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(81, 'md3', 1, 'Manage data due diligence vendor', 'manage-data-due-diligence-vendor', 'Manage data due diligence vendor', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(82, 'msr', 1, 'Manage Status Registration', 'manage-status-registration', 'Manage Status Registration', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(83, 'recon', 2, 'Input recon number', 'input-recon-number', 'Input recon number', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(84, 'rvr', 1, 'Review vendor registration', 'review-vendor-registration', 'Review vendor registration', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(85, 'rvs', 1, 'Recomendation vendor status ', 'recomendation-vendor-status', 'Recomendation vendor status', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(86, 'sap', 2, 'Input SAP ID', 'input-sap-id', 'Input SAP ID', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(87, 'srm', 2, 'Input SRM Account', 'input-srm-account', 'Input SRM Account', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(88, 'uvd', 1, 'Update vendor data', 'update-vendor-data', 'Update vendor data', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(89, 'vba', 1, 'Verification bank account', 'verification-bank-account', 'Verification bank account', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(90, 'vd3', 1, 'View data due diligence', 'view-data -due-diligence', 'View data due diligence', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(91, 'vver', 2, 'View verification', 'view-verification', 'View verification', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(92, 'vvr', 1, 'View vendor registration', 'view-vendor-registration', 'View vendor registration', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(108, 'ma', 11, 'Manage Area', 'manage-area', 'Manage Area', '1', 1, '2016-09-07 09:05:34', '2016-09-07 09:05:34'),
(109, 'mo', 11, 'Manage Office', 'manage-office', 'Manage Office', '1', 1, '2016-09-07 09:08:05', '2016-09-07 09:08:05'),
(110, 'md', 11, 'Manage Division', 'manage-division', 'Manage Division', '1', 1, '2016-09-07 09:08:46', '2016-09-07 09:08:46'),
(111, 'mdp', 11, 'Manage Department', 'manage-department', 'Manage Department', '1', 1, '2016-09-07 09:09:47', '2016-09-07 09:09:47'),
(112, 'mp', 11, 'Manage Posistion', 'manage-position', 'Manage Position', '1', 1, '2016-09-07 09:11:00', '2016-09-07 09:11:00'),
(113, 'mg', 14, 'Manage Group', 'manage-group', 'Manage Group', '1', 1, '2016-09-07 09:19:07', '2016-09-07 09:19:07'),
(114, 'mpg', 14, 'Manage Permission Group', 'manage-permission-group', 'Manage Permission Group', '1', 1, '2016-09-07 09:19:41', '2016-09-07 09:19:41'),
(115, 'mpr', 14, 'Manage Permission', 'manage-permission', 'Manage Permission', '1', 1, '2016-09-07 09:21:30', '2016-09-07 09:21:30'),
(116, 'msb', 12, 'Manage Subcategory', 'manage-subcategory', 'Manage Subcategory', '1', 1, '2016-09-07 09:32:57', '2016-09-07 09:32:57'),
(117, 'mcl', 15, 'Manage Content Level', 'manage-content-level', 'Manage Content Level', '1', 1, '2016-09-07 16:24:49', '2016-09-07 16:24:49'),
(118, 'dda', 12, 'Due Diligence Assesment', 'due-diligence-assesment', 'Due Diligence Assesment', '1', 1, '2016-09-07 16:28:08', '2016-09-07 16:28:08'),
(119, 'lde', 12, 'List Document Evaluation', 'list-document-evaluation', 'List Document Evaluation', '1', 1, '2016-09-07 16:29:36', '2016-09-07 16:29:36'),
(120, 'mau', 13, 'Manage About Us', 'manage-about-us', 'Manage About Us', '1', 1, '2016-09-07 16:31:22', '2016-09-07 16:31:22'),
(121, 'mco', 13, 'Manage Contact', 'manage-contact', 'Manage Contact', '1', 1, '2016-09-07 16:31:56', '2016-09-07 16:31:56'),
(122, 'mfaq', 13, 'Manage FAQs', 'manage-faq', 'Manage FAQs', '1', 1, '2016-09-07 16:32:26', '2016-09-07 16:32:26'),
(123, 'mvp', 13, 'Manage Vendor Policy', 'manage-vendor-policy', 'Manage Vendor Policy', '1', 1, '2016-09-07 16:33:40', '2016-09-07 16:33:40'),
(124, 'mtac', 13, 'Manage Terms and Conditions', 'manage-terms-and-conditions', 'Manage Terms and Conditions', '1', 1, '2016-09-07 16:34:27', '2016-09-07 16:34:27'),
(125, 'vvdel', 18, 'View Vendor Delete', 'view-vendor-delete', 'View Vendor Delete', '1', 1, '2016-09-13 08:52:16', '2016-09-13 08:52:16'),
(126, 'vvdd', 18, 'View Vendor Delete Detail', 'view-vendor-delete-detail', 'View Vendor Delete Detail', '1', 1, '2016-09-13 08:52:55', '2016-09-13 08:52:55'),
(127, 'cven', 19, 'Create Vendor', 'create-vendor', 'Create Vendor By Admin', '1', 1, '2016-09-20 03:26:43', '2016-09-20 03:26:43'),
(128, 'cvb', 1, 'Create Vendor Button', 'create-vendor-button', 'Create Vendor Button', '1', 1, '2016-09-20 03:27:17', '2016-09-20 03:27:17'),
(129, 'arl', 9, 'Reject Content English Completion', 'reject_contentlang', 'Reject Content English Completion', '1', 1, '2016-09-19 17:00:00', '2016-09-19 17:00:00'),
(130, 'rembtn', 1, ' 	Reminder Button', 'reminder-button', 'Reminder Button Manual', '1', 1, '2016-09-22 13:56:42', '2016-09-22 13:56:42'),
(131, 'apvr2', 1, 'Approve vendor registration level-2', 'approve-vendor-registration-2', 'Approve vendor registration level-2', '1', 1, '2016-09-26 08:32:11', '2016-09-26 08:32:18'),
(132, 'apvr3', 1, 'Approve vendor registration level-3', 'approve-vendor-registration-3', 'Approve vendor registration level-3', '1', 1, '2016-09-26 08:32:08', '2016-09-26 08:32:15'),
(133, 'acdvras', 10, 'Approval Change Data Vendor Request By AVL', 'approval-change-data-vendor-request-byavl', 'Approval Change Data Vendor Request By AVL', '1', 1, '2016-09-28 09:17:53', '2016-09-28 09:17:53'),
(134, 'acdvrsmibs', 10, 'Approval Change Data Vendor Request By ADM SRC', 'approval-change-data-vendor-request-byadmsrc', 'Approval Change Data Vendor Request By ADM SRC', '1', 1, '2016-09-28 09:18:39', '2016-09-28 09:18:39'),
(135, 'viewdoc', 20, 'View Vendor Document', 'view-vendor-document', 'View document vendor', '1', 1, '2016-10-04 08:54:24', '2016-10-04 08:54:24'),
(136, 'TV', 21, 'Mengizinkan tambah vendor', '1', 'Menambah vendor', '1', 1, '2016-10-05 09:37:10', '2016-10-05 09:37:10'),
(137, 'me', 11, 'Manage Employee', 'manage-employee', 'Manage Employee', '1', 33, '2016-10-06 07:53:39', '2016-10-06 07:53:39'),
(138, 'mf', 11, 'Manage Functional', 'manage-functional', 'Manage Functional', '1', 33, '2016-10-06 07:54:08', '2016-10-06 07:54:08'),
(139, 'vmm', 23, 'View Manage Menu', 'view-manage-menu', 'View for menu management', '1', 1, '2016-10-12 06:40:45', '2016-10-12 06:40:45'),
(140, 'Per Code', 7, 'Per Name', 'per-value', 'Deskripsi', '1', 1, '2016-10-12 08:48:22', '2016-10-12 08:48:22'),
(141, 'Code of Te', 7, 'Name of Testing', 'value-of-testing', 'Description of Testing', '0', 1, '2016-10-14 03:16:39', '2016-10-14 03:16:39'),
(142, 'Code of Te', 7, 'Name of Testing aaa', 'value-of-testingaaa', 'aaa						aaaaaaaaaaaa					', '0', 1, '2016-10-14 03:17:02', '2016-10-14 06:57:07'),
(143, 'arl', 9, 'Approve/reject Language of Content', 'approve-or-reject-language-of-content', 'Approve/reject Language of Content', '1', 1, '2016-10-15 11:03:05', '2016-10-15 11:03:08'),
(144, 'vpavl', 29, 'View Promote AVL', 'view-promote-avl', 'Permission to view promote vendor to AVL', '1', 1, '2016-10-18 01:42:32', '2016-10-18 01:42:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions_server`
--

CREATE TABLE IF NOT EXISTS `permissions_server` (
`id` int(10) unsigned NOT NULL,
  `code` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `id_function` smallint(5) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci DEFAULT '0',
  `created_id` smallint(5) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=91 ;

--
-- Dumping data untuk tabel `permissions_server`
--

INSERT INTO `permissions_server` (`id`, `code`, `id_function`, `name`, `value`, `description`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, '', NULL, 'Create Content', 'create-content', 'Create Content', '0', 1, '2016-05-15 23:25:08', '2016-05-15 23:25:08'),
(2, '', NULL, 'Update Content ', 'update-content-', 'Update Content ', '0', 1, '2016-05-15 23:25:23', '2016-05-15 23:25:23'),
(9, '', NULL, 'Review Content', 'review-content', 'Review Content', '0', 1, '2016-05-15 23:26:20', '2016-05-15 23:26:20'),
(10, '', NULL, 'Publish Content', 'publish-content', 'Publish Content', '0', 1, '2016-05-15 23:26:27', '2016-05-15 23:26:27'),
(11, '', NULL, 'View Bidding Participant', 'view-bidding-participant', 'View Bidding Participant', '0', 1, '2016-05-15 23:26:35', '2016-05-15 23:26:35'),
(12, '', NULL, 'Approve or Reject Bidding Participant', 'approve-or-reject-bidding-participant', 'Approve or Reject Bidding Participant', '0', 1, '2016-05-15 23:27:04', '2016-05-15 23:27:04'),
(13, '', NULL, 'Approve/ Reject Content ', 'approve/-reject-content-', 'Approve/ Reject Content ', '0', 1, '2016-05-15 23:27:42', '2016-05-15 23:27:42'),
(14, '', NULL, 'Approve vendor registration', 'approve-vendor-registration', 'Approve vendor registration', '0', 1, '2016-05-15 23:27:50', '2016-05-15 23:27:50'),
(15, 'vra', 3, 'Red list vendor approval', 'red-list-vendor-approval', 'Red list vendor approval', '1', 1, '2016-05-15 23:28:08', '2016-05-15 23:28:08'),
(16, 'approv', 5, 'Black list vendor approval', 'black-list-vendor-approval', 'Black list vendor approval', '1', 1, '2016-05-15 23:28:15', '2016-05-15 23:28:15'),
(17, '', NULL, 'Review vendor registration', 'review-vendor-registration', 'Review vendor registration', '0', 1, '2016-05-15 23:28:37', '2016-05-15 23:28:37'),
(18, '', NULL, 'Update vendor data', 'update-vendor-data', 'Update vendor data', '0', 1, '2016-05-15 23:28:47', '2016-05-15 23:28:47'),
(19, '', NULL, 'Upload  files', 'upload--files', 'Upload  files', '0', 1, '2016-05-15 23:28:56', '2016-05-15 23:28:56'),
(20, '', NULL, 'Finalization vendor registration', 'finalization-vendor-registration', 'Finalization vendor registration', '0', 1, '2016-05-15 23:29:03', '2016-05-15 23:29:03'),
(21, '', NULL, 'Approve vendor data changes', 'approve-vendor-data-changes', 'Approve vendor data changes', '0', 1, '2016-05-15 23:29:13', '2016-05-15 23:29:13'),
(22, 'add', 5, 'Add vendor to black list', 'add-vendor-to-black-list', 'Add vendor to black list', '1', 1, '2016-05-15 23:29:21', '2016-05-15 23:29:21'),
(23, 'avr', 3, 'Add vendor to red list', 'add-vendor-to-red-list', 'Add vendor to red list', '1', 1, '2016-05-15 23:29:28', '2016-05-15 23:29:28'),
(24, '', NULL, 'View vendor registration status', 'view-vendor-registration-status', 'View vendor registration status', '0', 1, '2016-05-15 23:29:46', '2016-05-15 23:29:46'),
(25, '', NULL, 'Change company profile', 'change-company-profile', 'Change company profile', '0', 1, '2016-05-15 23:30:03', '2016-05-15 23:30:03'),
(26, '', NULL, 'Change user profile', 'change-user-profile', 'Change user profile', '0', 1, '2016-05-15 23:30:10', '2016-05-15 23:30:10'),
(27, '', NULL, 'View SAP Transaction', 'view-sap-transaction', 'View SAP Transaction', '0', 1, '2016-05-15 23:30:17', '2016-05-15 23:30:17'),
(28, '', NULL, 'View Transaction', 'view-transaction', 'View Transaction', '0', 1, '2016-05-15 23:30:32', '2016-05-15 23:30:32'),
(29, '', NULL, 'View news', 'view-news', 'View news', '0', 1, '2016-05-15 23:30:40', '2016-05-15 23:30:40'),
(30, '', NULL, 'View announcement {Open Bidding, Open Sourcing}', 'view-announcement-{open-bidding,-open-sourcing}', 'View announcement {Open Bidding, Open Sourcing}', '0', 1, '2016-05-15 23:30:55', '2016-05-15 23:30:55'),
(31, '', NULL, 'FAQs', 'faqs', 'FAQs', '0', 1, '2016-05-15 23:31:02', '2016-05-15 23:31:02'),
(32, '', NULL, 'Self registration', 'self-registration', 'Self registration', '0', 1, '2016-05-16 01:30:15', '2016-05-16 01:30:15'),
(35, '', NULL, 'view vendor management', 'view-vendor-management', 'view vendor management', '0', 1, '2016-05-31 23:04:35', '2016-05-31 23:04:35'),
(36, '', NULL, 'Approve bidding/reject', 'approve-bidding-reject', 'Approve or reject bidding', '0', 1, '2016-07-14 20:58:32', '2016-07-14 20:58:32'),
(37, '', NULL, 'Input recon number', 'input-recon-number', 'Input recon number', '0', 1, '2016-07-22 02:37:47', '2016-07-22 02:37:47'),
(38, '', NULL, 'Input SAP ID', 'input-sap-id', 'Input SAP ID', '0', 1, '2016-07-22 02:38:55', '2016-07-22 02:38:55'),
(39, '', NULL, 'Unpublish Content', 'unpublish-content', 'Unpublish Content', '0', 1, '2016-07-22 02:39:55', '2016-07-22 02:39:55'),
(40, '', NULL, 'Delete content ', 'delete-content-', 'delete content ', '0', 1, '2016-07-22 03:01:59', '2016-07-22 03:01:59'),
(41, '', NULL, 'Manage Company', 'manage-company', 'Manage Company', '0', 4, '2016-07-24 23:11:53', '2016-07-24 23:11:53'),
(42, '', NULL, 'Manage User', 'manage-user', 'Manage User', '0', 4, '2016-07-24 23:13:13', '2016-07-24 23:13:13'),
(43, '', NULL, 'Manage Category', 'manage-category', 'Manage Category', '0', 4, '2016-07-24 23:15:22', '2016-07-24 23:15:22'),
(44, '', NULL, 'Manage Web', 'manage-web', 'Manage Web', '0', 4, '2016-07-24 23:17:41', '2016-07-24 23:17:41'),
(45, '', NULL, 'Manage Status Registration', 'manage-status-registration', 'Manage Status Registration', '0', 4, '2016-07-24 23:18:51', '2016-07-24 23:18:51'),
(46, '', NULL, 'Manage Content Type', 'manage-content-type', 'Manage Content Type', '0', 4, '2016-07-24 23:19:15', '2016-07-24 23:19:15'),
(47, '', NULL, 'Manage Sub Content Type', 'manage-sub-content-type', 'Manage Sub Content Type', '0', 4, '2016-07-24 23:24:10', '2016-07-24 23:24:10'),
(48, '', NULL, 'Manage Document Requirement', 'manage-document-requirement', 'Manage Document Requirement', '0', 4, '2016-07-24 23:24:56', '2016-07-24 23:24:56'),
(49, '', NULL, 'Manage Notification', 'manage-notification', 'Manage Notification', '0', 4, '2016-07-24 23:25:42', '2016-07-24 23:25:42'),
(50, '', NULL, 'Select Winner', 'select-winner', 'Selecting WInner from Participants List', '0', 1, '2016-07-28 06:53:07', '2016-07-28 06:53:14'),
(51, '', NULL, 'Confirm Winner', 'confirm-winner', 'Confirm Winner from Winner List', '0', 1, '2016-07-28 06:53:53', '2016-07-28 06:54:00'),
(52, '', NULL, 'Input Vendor Account', 'input-vendor-account', 'Input Vendor Account', '0', 1, '2016-08-02 17:00:00', '2016-08-02 17:00:00'),
(53, '', NULL, 'Input SRM Account', 'input-srm-account', 'Input SRM Account', '0', 1, '2016-08-02 17:00:00', '2016-08-02 17:00:00'),
(54, '', NULL, 'Cetak Berita Acara', 'cetak-berita-acara', 'Cetak Berita Acara', '0', 1, '2016-08-08 17:00:00', '2016-08-08 17:00:00'),
(55, '', NULL, 'Download Excel Vendor', 'download-excel-vendor', 'Download Excel Vendor', '0', 1, '2016-08-09 14:29:38', '2016-08-09 14:29:42'),
(56, 'mnr', 3, 'Manage Redlist', 'manage-redlist', 'Manage Redlist', '1', 1, '2016-09-09 08:52:37', '2016-09-09 08:52:37'),
(57, 'evr', 3, 'Edit Data Vendor Redlist', 'edit-data-vendor-redlist', 'Edit Data Vendor Redlist', '1', 1, '2016-09-09 08:52:37', '2016-09-09 08:52:37'),
(58, 'cvr', 3, 'Cancel Vendor Redlist', 'cancel-vendor-redlist', 'Cancel Vendor Redlist', '1', 1, '2016-09-09 08:52:37', '2016-09-09 08:52:37'),
(59, 'vvi', 4, 'View Inactive/active', 'view-inactive/active', 'View Inactive/active', '1', 1, '2016-08-29 17:00:00', '2016-08-29 17:00:00'),
(60, 'iibk', 4, 'Inactive/active SM IBK', 'inactive/active-sm-ibk', 'Inactive/active SM IBK', '1', 1, '2016-08-29 17:00:00', '2016-08-29 17:00:00'),
(61, 'iibs', 4, 'Inactive/active SM IBS', 'inactive/active-sm-ibs', 'Inactive/active SM IBS', '1', 1, '2016-08-29 17:00:00', '2016-08-29 17:00:00'),
(62, 'ias', 4, 'Inactive/active Admin Sourcing', 'inactive/active-admin-sourcing', 'Inactive/active Admin Sourcing', '1', 1, '2016-08-29 17:00:00', '2016-08-29 17:00:00'),
(63, 'ivpib', 4, 'Inactive/active VP IB', 'inactive/active-vp-ib', 'Inactive/active VP IB', '1', 1, '2016-08-29 17:00:00', '2016-08-29 17:00:00'),
(64, 'viewv', 5, 'btn View Detail Vendor Black List', 'view-detail-vendor-black-list', 'btn View Detail Vendor Black List', '1', 17, '2016-08-30 17:00:00', '2016-08-30 17:00:00'),
(65, 'editb', 5, 'btn Edit Black List', 'edit-black-list', 'btn Edit Black List', '1', 17, '2016-08-30 17:00:00', '2016-08-30 17:00:00'),
(66, 'cancelb', 5, 'btn Cancel Black List', 'cancel-black-list', 'btn Cancel Black List', '1', 17, '2016-08-30 17:00:00', '2016-08-30 17:00:00'),
(67, 'reject', 5, 'btn Reject Black List', 'reject-black-list', 'btn Reject Black List', '1', 17, '2016-08-30 17:00:00', '2016-08-30 17:00:00'),
(68, 'smibk', 5, 'View Black List On Level SM IBK', 'view-black-list-on-level-sm-ibk', 'View Black List On Level SM IBK', '1', 17, '2016-08-30 17:00:00', '2016-08-30 17:00:00'),
(69, 'vpib', 5, 'View Black List On Level VP IB', 'view-black-list-on-level-sm-vpib', 'View Black List On Level VP IB', '1', 17, '2016-08-23 17:00:00', '2016-08-30 17:00:00'),
(70, 'm', 5, 'Manage Black List', 'manage-black-list', 'Manage Black List', '1', 17, '2016-08-30 17:00:00', '2016-08-30 17:00:00'),
(71, 'vvl', 6, 'View Vendor List', 'view-vendor-list', 'View Vendor List', '1', 17, '2016-09-01 04:31:13', '2016-09-01 04:31:13'),
(72, 'vlee', 6, 'Vendor List Export Excel', 'vendor-list-export-excel', 'Vendor List Export Excel', '1', 17, '2016-09-01 04:32:02', '2016-09-01 04:32:02'),
(73, 'vlbas', 6, 'Vendor List Button Admin Sourcing', 'vendor-list-button-admin-sourcing', 'Vendor List Button Admin Sourcing', '1', 17, '2016-09-01 04:32:57', '2016-09-01 04:32:57'),
(74, 'apvr', 1, 'Approve vendor registration', 'approve-vendor-registration', 'Approve vendor registration', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(75, 'badd', 1, 'Cetak berita acara due diligence', 'cetak-berita-acara-due-diligence', 'Cetak berita acara due diligence', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(76, 'baes', 1, 'Cetak berita acara evaluasi sourcing', 'cetak-berita-acara-evaluasi-sourcing', 'Cetak berita acara evaluasi sourcing', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(77, 'fvr', 1, 'Finalization vendor registration', 'finalization-vendor-registration', 'Finalization vendor registration', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(78, 'gasn', 1, 'Get admin sourcing notification', 'get-admin-sourcing-notification', 'Get admin sourcing notification', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(79, 'md3', 1, 'Manage data due diligence vendor', 'manage-data-due-diligence-vendor', 'Manage data due diligence vendor', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(80, 'msr', 1, 'Manage Status Registration', 'manage-status-registration', 'Manage Status Registration', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(81, 'rvr', 1, 'Review vendor registration', 'review-vendor-registration', 'Review vendor registration', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(82, 'rvs', 1, 'Recomendation vendor status ', 'recomendation-vendor-status', 'Recomendation vendor status', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(83, 'uvd', 1, 'Update vendor data', 'update-vendor-data', 'Update vendor data', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(84, 'vba', 1, 'Verification bank account', 'verification-bank-account', 'Verification bank account', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(85, 'vd3', 1, 'View data due diligence', 'view-data -due-diligence', 'View data due diligence', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(86, 'vvr', 1, 'View vendor registration', 'view-vendor-registration', 'View vendor registration', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(87, 'recon', 2, 'Input recon number', 'input-recon-number', 'Input recon number', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(88, 'sap', 2, 'Input SAP ID', 'input-sap-id', 'Input SAP ID', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(89, 'srm', 2, 'Input SRM Account', 'input-srm-account', 'Input SRM Account', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19'),
(90, 'vver', 2, 'View verification', 'view-verification', 'View verification', '1', 1, '2016-09-02 09:24:19', '2016-09-02 09:24:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `policy_agreement`
--

CREATE TABLE IF NOT EXISTS `policy_agreement` (
`id` mediumint(11) unsigned NOT NULL,
  `type` smallint(5) NOT NULL,
  `creator` varchar(30) NOT NULL,
  `file` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `activated` tinyint(1) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `detail` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `policy_agreement`
--

INSERT INTO `policy_agreement` (`id`, `type`, `creator`, `file`, `title`, `status`, `activated`, `created_id`, `created_date`, `last_updated`, `detail`) VALUES
(1, 0, '', 'general', 'Vendor Policy for General', '1', 0, 1, '2016-10-18 09:41:41', '2016-10-18 09:41:41', ''),
(2, 1, '', 'news_public', 'Vendor Policy for News Public', '1', 0, 1, '2016-10-17 16:13:21', '2016-10-17 16:13:21', ''),
(3, 2, '', 'news_blast', 'Vendor Policy for News Blast', '1', 0, 1, '2016-10-17 11:17:29', '2016-10-17 11:17:29', ''),
(4, 3, '', 'open_bidding', 'Vendor Policy for Open Bidding', '1', 0, 1, '2016-10-18 09:47:43', '2016-10-18 09:47:43', ''),
(5, 4, '', 'open_sourcing', 'Vendor Policy for Open Sourcing', '1', 0, 1, '2016-10-17 14:27:06', '2016-10-17 14:27:06', ''),
(6, 5, '', 'direct_selection', 'Vendor Policy for Direct Selection', '1', 0, 1, '2016-10-17 14:27:23', '2016-10-17 14:27:23', ''),
(7, 6, '', 'limited_bidding', 'Vendor Policy for Limited Bidding', '1', 0, 1, '2016-10-17 14:27:42', '2016-10-17 14:27:42', ''),
(8, 9, '', 'testing', '', '1', 0, 1, '2016-10-18 17:08:18', '2016-10-18 17:08:18', ''),
(9, 0, '', 'test', '', '1', 0, 1, '2016-10-19 10:39:02', '2016-10-19 10:39:02', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `policy_agreement_`
--

CREATE TABLE IF NOT EXISTS `policy_agreement_` (
`id` mediumint(11) unsigned NOT NULL,
  `type` smallint(5) NOT NULL,
  `creator` varchar(30) NOT NULL,
  `file` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `activated` tinyint(1) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `detail` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `policy_agreement_`
--

INSERT INTO `policy_agreement_` (`id`, `type`, `creator`, `file`, `title`, `status`, `activated`, `created_id`, `created_date`, `last_updated`, `detail`) VALUES
(1, 4, 'Super_Admin', 'general', 'Vendor Policy for General', '1', 1, 17, '2016-08-05 03:21:27', '2016-09-01 13:13:57', ''),
(2, 3, 'super_admin', 'open_sourcing', 'Vendor Policy for Open Sourcing', '1', 1, 17, '2016-09-01 16:06:11', '2016-09-01 16:06:11', ''),
(3, 2, 'Super_Admin', 'open_bidding', 'Vendor Policy for Open Bidding', '1', 1, 17, '2016-08-05 03:22:06', '2016-08-05 03:22:06', ''),
(4, 1, 'Super_Admin', 'direct_selection', 'Vendor Policy for Direct Selection', '1', 1, 17, '2016-08-05 03:22:18', '2016-08-05 03:22:18', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `policy_agreement_old`
--

CREATE TABLE IF NOT EXISTS `policy_agreement_old` (
`id` mediumint(11) unsigned NOT NULL,
  `type` smallint(5) NOT NULL,
  `creator` varchar(30) NOT NULL,
  `file` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `activated` tinyint(1) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `detail` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `policy_agreement_old`
--

INSERT INTO `policy_agreement_old` (`id`, `type`, `creator`, `file`, `title`, `status`, `activated`, `created_id`, `created_date`, `last_updated`, `detail`) VALUES
(1, 1, 'Super_Admin', 'news_public', 'Vendor Policy for News Public', '1', 1, 1, '2016-10-18 07:31:42', '2016-10-18 07:31:42', ''),
(2, 2, 'super_admin', 'news_blast', 'Vendor Policy for News Blast', '1', 1, 1, '2016-10-18 07:31:56', '2016-10-18 07:31:56', ''),
(3, 3, 'Super_Admin', 'open_bidding', 'Vendor Policy for Open Bidding', '1', 1, 1, '2016-10-18 07:32:14', '2016-10-18 07:32:14', ''),
(4, 4, 'Super_Admin', 'open_sourcing', 'Vendor Policy for Open Sourcing', '1', 1, 1, '2016-10-18 07:32:26', '2016-10-18 07:32:26', ''),
(5, 5, 'admin', 'direct_selection', 'Vendor Policy for Direct Selection', '1', 1, 1, '2016-10-18 07:32:44', '2016-10-18 07:32:44', ''),
(6, 6, 'admin', 'limited_bidding', 'Vendor Policy for Limited Bidding', '1', 1, 1, '2016-10-18 07:32:56', '2016-10-18 07:32:56', ''),
(7, 7, '', 'testing_eproc', 'title', '1', 0, 1, '2016-10-18 07:33:54', '2016-10-18 07:33:54', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `policy_agreement_temp`
--

CREATE TABLE IF NOT EXISTS `policy_agreement_temp` (
`id` smallint(5) NOT NULL,
  `id_type` smallint(5) NOT NULL,
  `deskripsi` varchar(30) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `name_file` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `policy_agreement_temp`
--

INSERT INTO `policy_agreement_temp` (`id`, `id_type`, `deskripsi`, `keterangan`, `name_file`, `created_date`) VALUES
(1, 3, 'news_public1', 'News Public', '20161018_news_public1', '2016-10-18 00:00:00'),
(2, 3, 'open_bidding2', 'News Public', '20161018_open_bidding2', '2016-10-18 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `position`
--

CREATE TABLE IF NOT EXISTS `position` (
`id` smallint(5) unsigned NOT NULL,
  `post_code` varchar(10) NOT NULL,
  `position_name` varchar(30) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `position`
--

INSERT INTO `position` (`id`, `post_code`, `position_name`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 'VP', 'Vice President', '', '1', 17, '2016-09-09 15:52:37', '2016-08-15 08:34:09'),
(2, 'SM', 'Senior Manager', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(3, 'MGR', 'Manager', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(4, 'STF', 'Staff', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(5, 'test', 'test', '', '0', 17, '2016-08-15 08:34:27', '2016-09-19 16:16:54'),
(6, 'KB', 'KaBak', '', '0', 1, '2016-10-12 09:27:07', '2016-10-12 09:28:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
`id` smallint(5) unsigned NOT NULL,
  `userID` smallint(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `nopeg` varchar(30) NOT NULL,
  `id_office` smallint(5) NOT NULL,
  `id_dept` smallint(5) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data untuk tabel `profiles`
--

INSERT INTO `profiles` (`id`, `userID`, `name`, `nopeg`, `id_office`, `id_dept`, `phone`, `mobile`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 1, 'Ririn Zulandra ', '900001.', 18, 6, '0987654321.', '0987654321.', '1', 1, '2016-08-01 03:10:10', '2016-08-01 03:10:10'),
(2, 4, 'Richo Rattata', '9012314', 13, 1, '1239148109', '23804723042', '1', 4, '2016-07-27 10:03:14', '2016-07-27 10:03:14'),
(3, 11, 'SM IBK', '9001234', 13, 20, '472381429', '897429472', '1', 11, '2016-07-27 10:56:42', '2016-07-27 10:56:42'),
(4, 12, 'Alif', '9804065865', 13, 21, '8752075207', '280723072', '1', 12, '2016-07-27 10:59:04', '2016-07-27 10:59:04'),
(5, 23, 'SM IBR', '0016738920', 18, 6, '024-6701643', '081910002300', '1', 1, '2016-05-04 05:19:43', '2016-05-18 10:00:14'),
(6, 24, 'Nida (IBS)', '0016738925', 18, 21, '024-6701643', '081910002300', '1', 24, '2016-08-07 21:00:56', '2016-08-07 21:00:56'),
(7, 25, 'Pramita (IBK)', '0016738927', 18, 20, '024-6701643', '081910002300', '1', 25, '2016-08-07 21:01:33', '2016-08-07 21:01:33'),
(8, 27, '', '', 0, 6, '', '', '1', 0, '2016-09-09 15:52:37', '0000-00-00 00:00:00'),
(9, 28, '', '', 0, 0, '', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(10, 30, '', '', 0, 0, '', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(11, 31, '', '', 0, 22, '', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(12, 13, 'VP IB', '123456', 14, 22, '123456', '123456', '1', 13, '2016-08-08 09:51:46', '2016-08-08 09:51:46'),
(13, 32, '', '', 0, 0, '', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(14, 33, '', '', 0, 21, '', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(15, 34, '', '', 0, 14, '', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(16, 35, '', '', 0, 22, '', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(17, 36, 'Admin IBS', '123456', 14, 21, '6789', '123424', '1', 36, '2016-08-08 16:01:04', '2016-08-08 16:01:04'),
(18, 38, '', '', 0, 0, '', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(19, 39, '', '', 0, 18, '', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `redlist_docs`
--

CREATE TABLE IF NOT EXISTS `redlist_docs` (
`id` smallint(5) NOT NULL,
  `id_redlist` smallint(5) NOT NULL,
  `filename` varchar(150) NOT NULL,
  `status` smallint(5) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=126 ;

--
-- Dumping data untuk tabel `redlist_docs`
--

INSERT INTO `redlist_docs` (`id`, `id_redlist`, `filename`, `status`) VALUES
(121, 10, '20161005160553vendor_redlist_20160928141118.xlsx', 1),
(122, 10, '20161005160553vendor_redlist_20160928141916.xlsx', 1),
(123, 21, '2016100614243028092016.txt', 1),
(124, 21, '2016100614243029092016.txt', 1),
(125, 21, '2016100614243003102016.txt', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `references`
--

CREATE TABLE IF NOT EXISTS `references` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `cust_name` varchar(50) NOT NULL,
  `project` varchar(50) NOT NULL,
  `point` smallint(5) NOT NULL,
  `date` date NOT NULL,
  `remark` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `references`
--

INSERT INTO `references` (`id`, `id_vendor`, `cust_name`, `project`, `point`, `date`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 3, 'PT Tatang S', 'Bikin kartu', 1000, '2016-09-09', 'bikin kartu', '1', 2, '2016-09-09 15:51:59', '2016-09-09 15:51:59'),
(2, 5, 'Ridho', 'eproc', 1, '2016-09-09', 'info', '1', 2, '2016-09-09 18:58:33', '2016-09-09 18:58:33'),
(4, 18, 'EFJQ;KJ', ';GQJE;GKJ', 0, '2016-09-07', 'EJF;Q', '1', 2, '2016-09-20 17:57:34', '2016-09-20 17:57:34'),
(5, 17, 'jkl', 'jklj', 0, '2016-09-30', 'kl', '1', 2, '2016-09-30 11:16:13', '2016-09-30 11:16:25'),
(6, 17, 'kljlkj', 'lkjlk', 0, '2016-09-30', 'lkj', '0', 19, '2016-09-30 11:20:06', '2016-09-30 11:21:38'),
(8, 22, 'ARCHI', 'PEMBANGUNAN MRT', 95, '2016-10-10', 'SU FAR SO GOOD', '1', 2, '2016-10-12 13:58:59', '2016-10-12 14:00:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `region`
--

CREATE TABLE IF NOT EXISTS `region` (
`id` smallint(5) unsigned NOT NULL,
  `id_country` smallint(5) NOT NULL,
  `region_name` varchar(30) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data untuk tabel `region`
--

INSERT INTO `region` (`id`, `id_country`, `region_name`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 3, 'Surabaya', 'test', '1', 17, '2015-12-28 09:15:01', '2016-09-08 08:42:55'),
(2, 1, 'Jakarta', 'test', '1', 1, '2015-12-28 09:15:52', '2015-12-28 09:15:52'),
(3, 1, 'Cengkareng', 'test1', '1', 1, '2016-04-18 13:35:09', '2016-06-08 09:44:35'),
(7, 9, 'Malang', '', '1', 17, '2016-06-10 03:51:54', '2016-08-09 18:08:48'),
(16, 17, 'Sidoarjo', '', '1', 17, '2016-07-15 04:42:51', '2016-08-09 18:01:06'),
(17, 17, 'Region2907 edit', '', '1', 17, '2016-07-29 03:58:15', '2016-08-09 18:09:42'),
(18, 18, 'Region2907 edit', '', '1', 17, '2016-07-29 03:59:37', '2016-07-29 04:11:16'),
(19, 4, 'abc', '', '1', 17, '2016-07-29 04:34:16', '2016-09-19 15:04:29'),
(20, 1, 'lililili e', '', '1', 17, '2016-08-01 03:31:50', '2016-08-09 18:08:35'),
(21, 22, 'region', '', '1', 17, '2016-08-09 17:25:49', '2016-08-09 17:25:49'),
(22, 22, 'abc test', '', '1', 17, '2016-08-09 17:28:29', '2016-09-01 13:18:51'),
(25, 3, 'vietnam', '', '1', 1, '2016-09-19 13:20:09', '2016-09-19 13:20:09'),
(26, 1, 'baru', '', '0', 1, '2016-09-19 13:26:47', '2016-09-19 15:59:54'),
(29, 3, 'baru', '', '0', 1, '2016-10-11 14:02:35', '2016-10-11 14:02:35'),
(30, 7, 'Apa', '', '0', 1, '2016-10-11 15:46:33', '2016-10-11 15:46:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `register_history`
--

CREATE TABLE IF NOT EXISTS `register_history` (
`id` mediumint(11) unsigned NOT NULL,
  `regID` smallint(5) NOT NULL,
  `stsID` smallint(5) NOT NULL,
  `note` varchar(10) NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `register_status`
--

CREATE TABLE IF NOT EXISTS `register_status` (
`id` smallint(5) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `register_status`
--

INSERT INTO `register_status` (`id`, `status`, `remark`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 'NEW', '', 1, '2016-04-14 14:14:43', '2016-09-22 08:46:40'),
(2, 'GENERAL REVIEW', '', 1, '2016-04-14 14:14:43', '2016-09-22 08:47:40'),
(3, 'ADMINISTRATION REVIEW', '', 1, '2016-04-14 14:14:43', '2016-09-22 08:47:58'),
(4, 'DUE-DILIGENCE', '', 1, '2016-04-14 14:14:43', '2016-09-22 08:48:09'),
(5, 'FINAL EVALUATION', '', 1, '2016-04-14 14:14:43', '2016-09-22 08:48:23'),
(6, 'VERIFICATION', '', 1, '2016-04-14 14:14:43', '2016-09-22 08:49:33'),
(7, 'FINALIZATION', '', 1, '2016-04-14 14:14:43', '2016-09-22 08:49:49'),
(8, 'FINISH', 'Finish', 1, '2016-09-26 00:00:00', '2016-10-18 19:04:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `reminder_doc`
--

CREATE TABLE IF NOT EXISTS `reminder_doc` (
`id` int(11) NOT NULL,
  `id_vendor` int(11) NOT NULL,
  `id_doc` int(11) NOT NULL,
  `exp_date` date NOT NULL,
  `first_reminder` date NOT NULL,
  `last_reminder` date NOT NULL,
  `notif` int(10) NOT NULL DEFAULT '0',
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `created_id` smallint(6) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data untuk tabel `reminder_doc`
--

INSERT INTO `reminder_doc` (`id`, `id_vendor`, `id_doc`, `exp_date`, `first_reminder`, `last_reminder`, `notif`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(14, 2, 1, '2016-09-26', '2016-09-23', '2016-09-23', 1, '1', 0, '2016-09-23 09:39:02', '2016-09-23 09:39:02'),
(15, 2, 3, '2016-09-26', '2016-09-23', '2016-09-23', 1, '1', 0, '2016-09-23 09:39:02', '2016-09-23 09:39:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `section`
--

CREATE TABLE IF NOT EXISTS `section` (
`id` smallint(5) unsigned NOT NULL,
  `id_dept` smallint(5) NOT NULL,
  `section_code` varchar(10) NOT NULL,
  `section_name` varchar(30) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `section`
--

INSERT INTO `section` (`id`, `id_dept`, `section_code`, `section_name`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 1, 'IBK', 'IBK', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(2, 1, 'IBR', 'IBR', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(3, 1, 'IBS', 'IBS', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(4, 2, 'WA1', 'WA-1', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(5, 2, 'WA2', 'WA-2', '', '1', 0, '2016-09-09 15:52:37', '2016-09-09 15:52:37'),
(6, 1, 'test ', 'test', '', '1', 1, '2016-08-13 10:48:28', '2016-08-15 03:39:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `session_email`
--

CREATE TABLE IF NOT EXISTS `session_email` (
`id` smallint(5) unsigned NOT NULL,
  `token` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL,
  `method` varchar(100) DEFAULT '0000-00-00',
  `parameter` varchar(100) DEFAULT NULL,
  `expired_date` datetime DEFAULT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=649 ;

--
-- Dumping data untuk tabel `session_email`
--

INSERT INTO `session_email` (`id`, `token`, `email`, `username`, `controller`, `method`, `parameter`, `expired_date`, `created_id`, `created_date`, `last_updated`) VALUES
(7, '90ba7833306ab9a629c8b561f96cd350', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '1', NULL, 15, '2016-09-19 15:11:19', '2016-09-19 15:11:19'),
(8, '1f635c306887451bd27f257dd116e5e8', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 15:11:53', '2016-09-19 15:11:53'),
(9, '118f223f100afeb2c0e8d66a1464dbf8', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 15:19:42', '2016-09-19 15:19:42'),
(10, '3a62005116f17fc227b16e18090be994', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '1', NULL, 4, '2016-09-19 15:20:08', '2016-09-19 15:20:08'),
(11, '3ca31e11d74b2f70d31278c5aa42a708', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '2', NULL, 4, '2016-09-19 15:20:18', '2016-09-19 15:20:18'),
(12, 'e693741976611c70d27466eae55d45ed', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 15:41:21', '2016-09-19 15:41:21'),
(13, 'e7643d01542a96fb20af9aae9ade78ef', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 15:41:44', '2016-09-19 15:41:44'),
(14, '0921397eb62484ae93a214b75afdfcc6', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 15:43:04', '2016-09-19 15:43:04'),
(15, 'cfacc334254893ec3c6db3960ddaadd3', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 15:49:01', '2016-09-19 15:49:01'),
(16, 'd93f1d4ade3567ba31dc6a2af4d8436e', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 15:56:42', '2016-09-19 15:56:42'),
(17, 'cb96038870db69897b8e59b163afa40e', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 15:57:17', '2016-09-19 15:57:17'),
(18, '488fb415c2cd1b1cc883533e287dce46', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 16:11:16', '2016-09-19 16:11:16'),
(19, 'b7f583ae22dad15489e739074524eef4', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '2', NULL, 4, '2016-09-19 16:11:25', '2016-09-19 16:11:25'),
(20, 'de4c519f60df51e5ec024aebd6fa9d49', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 16:12:58', '2016-09-19 16:12:58'),
(21, 'ce9dcb636c82f96d591b4c45ae5201fe', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 16:22:43', '2016-09-19 16:22:43'),
(22, '53c96c0f6a8db32cc1902aaa360303ba', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '2', NULL, 4, '2016-09-19 16:22:52', '2016-09-19 16:22:52'),
(23, '65ad6ff97aee4541786972744aff66cc', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '3', NULL, 15, '2016-09-19 16:23:42', '2016-09-19 16:23:42'),
(24, 'ec2a5e90887368ac691bb829d03cc785', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '3', NULL, 4, '2016-09-19 16:25:47', '2016-09-19 16:25:47'),
(25, 'a780f8a48527eff0b12c8e46b6bfa64e', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '3', NULL, 15, '2016-09-19 16:26:41', '2016-09-19 16:26:41'),
(26, '3a6d63e3856395a184179b3fc7a6e9ae', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 16:30:35', '2016-09-19 16:30:35'),
(27, 'd26d5b26690f9b63f23846a6eed0275d', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '3', NULL, 4, '2016-09-19 16:30:45', '2016-09-19 16:30:45'),
(28, '62a7cd602b592624f87c9657b316cce0', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '2', NULL, 4, '2016-09-19 16:30:49', '2016-09-19 16:30:49'),
(29, 'decf74a0c8a4daa5c995f24ee72cbe03', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 16:31:54', '2016-09-19 16:31:54'),
(30, 'c758e4fb3dd86eda11073b2224dff2c4', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-19 16:33:57', '2016-09-19 16:33:57'),
(31, '5d304e33f4154dc96968cc31c1b68503', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '2', NULL, 4, '2016-09-19 16:38:51', '2016-09-19 16:38:51'),
(32, '2099ee4f6cadc244e9b678ff01c3b001', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '3', NULL, 15, '2016-09-19 16:39:09', '2016-09-19 16:39:09'),
(33, 'd5f03cd05571363b9087e5133a2c67df', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '3', NULL, 4, '2016-09-19 16:39:16', '2016-09-19 16:39:16'),
(34, '93e7a4d6ed4975936f57fb5875960763', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '3', NULL, 15, '2016-09-19 17:18:06', '2016-09-19 17:18:06'),
(35, 'cae8fc0a4e9edf1c7684ca9f9df77528', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '3', NULL, 15, '2016-09-19 17:24:00', '2016-09-19 17:24:00'),
(36, '7da39f99841c977c32c2c4571d7892bf', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '1', NULL, 15, '2016-09-20 11:23:47', '2016-09-20 11:23:47'),
(37, 'bacab12278747b6814b054ee595745f2', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '2', NULL, 15, '2016-09-20 11:25:34', '2016-09-20 11:25:34'),
(38, '49d74771541a5865a4aa0f21952cabe6', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '1', NULL, 15, '2016-09-20 11:26:45', '2016-09-20 11:26:45'),
(39, 'e896241e3a661f30af1d1a9e2e287296', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '4', NULL, 15, '2016-09-20 14:12:07', '2016-09-20 14:12:07'),
(40, 'aa67f1a0c3166b717642e88b920770ef', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '5', NULL, 15, '2016-09-20 14:13:05', '2016-09-20 14:13:05'),
(41, 'a94d94185087a6227faa22f1e510d82f', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '6', NULL, 15, '2016-09-20 14:14:28', '2016-09-20 14:14:28'),
(42, 'e4e917f36cdab38c30b2d6d94e85791c', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '7', NULL, 15, '2016-09-20 14:15:07', '2016-09-20 14:15:07'),
(43, '4880967933fa46dd0153418c8e3a8456', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '8', NULL, 15, '2016-09-20 14:16:03', '2016-09-20 14:16:03'),
(44, '51faae04d2996233fb1533c5dbc4b16d', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '1', NULL, 15, '2016-09-20 14:26:06', '2016-09-20 14:26:06'),
(45, '5a7525a8bc9b7a23ae17177fcaadaa9e', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '4', NULL, 15, '2016-09-20 14:27:34', '2016-09-20 14:27:34'),
(46, '76d53687d23721c361a59d7ff48812cc', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '4', NULL, 15, '2016-09-20 14:57:32', '2016-09-20 14:57:32'),
(47, 'fcfad817438c37c63fae47d6caeafc1b', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '1', NULL, 15, '2016-09-20 14:57:48', '2016-09-20 14:57:48'),
(48, '8df3192e107dbc2bdc11a3787bc72b34', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '4', NULL, 4, '2016-09-20 15:03:35', '2016-09-20 15:03:35'),
(49, 'd351d745a4363b016ef7fb3c9c7172e7', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '4', NULL, 4, '2016-09-20 15:03:35', '2016-09-20 15:03:35'),
(50, '41c3dc05914748ab077171c9fa8e5ada', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '1', NULL, 4, '2016-09-20 15:03:47', '2016-09-20 15:03:47'),
(51, '4001783eed713e691d3382a629bbcf5a', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '1', NULL, 4, '2016-09-20 15:03:47', '2016-09-20 15:03:47'),
(52, 'a22701e49b08cfb15866b14efe46e5c7', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '1', NULL, 15, '2016-09-20 15:07:13', '2016-09-20 15:07:13'),
(53, '4742ad0bcc17daba4b9c224f5440686c', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '1', NULL, 4, '2016-09-20 15:08:00', '2016-09-20 15:08:00'),
(54, 'c0a9bedd96e6defe26e781c522558ed7', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '1', NULL, 4, '2016-09-20 15:08:00', '2016-09-20 15:08:00'),
(55, 'aecbfa091d6464663dca271130fd7288', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '1', NULL, 15, '2016-09-20 15:13:07', '2016-09-20 15:13:07'),
(56, '8d3d6c7e211489e745f40cd244e28e78', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '1', NULL, 4, '2016-09-20 15:14:02', '2016-09-20 15:14:02'),
(57, 'f0ae4475d93d7c7a9f58ca04c5271694', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '1', NULL, 4, '2016-09-20 15:14:02', '2016-09-20 15:14:02'),
(58, '6256c7aaf2c77cf6fcf4e5f8f1394bd4', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '9', NULL, 15, '2016-09-20 15:21:47', '2016-09-20 15:21:47'),
(59, '8e8e5b2be5a0b4235e95965a99900ece', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '9', NULL, 4, '2016-09-20 15:22:18', '2016-09-20 15:22:18'),
(60, '6ee423fdc4b0a3d221ef2fc68f36e2d2', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '9', NULL, 4, '2016-09-20 15:22:23', '2016-09-20 15:22:23'),
(61, '933cb40f331acca594dc7d9ba7311c6a', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '9', NULL, 15, '2016-09-20 15:29:53', '2016-09-20 15:29:53'),
(62, '5f747aa7acaaacbd3979d7751fbbf9c6', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '9', NULL, 4, '2016-09-20 15:30:20', '2016-09-20 15:30:20'),
(63, '2c2440c3b83997620564d9aaacd09c2c', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '9', NULL, 4, '2016-09-20 15:30:20', '2016-09-20 15:30:20'),
(64, '1d329275b5a747d489ae11fd4d3dbb88', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '9', NULL, 15, '2016-09-20 15:32:01', '2016-09-20 15:32:01'),
(65, 'bdd3dc297fc760c0b14862a4400adc86', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '9', NULL, 15, '2016-09-20 15:37:01', '2016-09-20 15:37:01'),
(66, '160407733de0496aa9502aa6160463c4', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '9', NULL, 4, '2016-09-20 15:37:25', '2016-09-20 15:37:25'),
(67, '7c2fc5a20ffff9876b84f854f9a3d265', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '9', NULL, 4, '2016-09-20 15:37:26', '2016-09-20 15:37:26'),
(68, 'ccf4e3b153c47a9938ea551f3449f4e9', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '9', NULL, 15, '2016-09-20 15:37:43', '2016-09-20 15:37:43'),
(69, 'd80744e98848cc8d6853bbb15f105301', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '9', NULL, 4, '2016-09-20 15:37:58', '2016-09-20 15:37:58'),
(70, 'd8cb27ec72800c2a1f64ff733e23ad2d', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '9', NULL, 4, '2016-09-20 15:37:58', '2016-09-20 15:37:58'),
(71, '01ea8ed631bad8fc67698a900a5160bf', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '9', NULL, 15, '2016-09-20 15:55:54', '2016-09-20 15:55:54'),
(72, '348a9475b7e88a1318ddd388ba26f349', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '9', NULL, 4, '2016-09-20 15:56:38', '2016-09-20 15:56:38'),
(73, '3a4de2d5ffce5b5bd80e188b0c48ec27', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '9', NULL, 4, '2016-09-20 15:56:38', '2016-09-20 15:56:38'),
(74, '886d90b3e6502a652a04109e675c40ee', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '9', NULL, 15, '2016-09-20 15:57:07', '2016-09-20 15:57:07'),
(75, 'ca84c86b824a646ddb3793506148299c', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '9', NULL, 4, '2016-09-20 15:57:26', '2016-09-20 15:57:26'),
(76, '2f5ff12f687ed8875a1ed162f5f0bb9a', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '9', NULL, 4, '2016-09-20 15:57:26', '2016-09-20 15:57:26'),
(77, '307e52f1c23178c9800bf37dce618953', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '9', NULL, 15, '2016-09-20 16:20:21', '2016-09-20 16:20:21'),
(78, '1cc525874c29a890dc69d5c2dfac6f64', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '9', NULL, 15, '2016-09-20 16:21:20', '2016-09-20 16:21:20'),
(79, '6916ee15ebef69e26c10247789544803', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '9', NULL, 4, '2016-09-20 16:27:00', '2016-09-20 16:27:00'),
(80, 'fef9e0ddec4940e06f0e1ac83ac6f8de', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '9', NULL, 4, '2016-09-20 16:27:00', '2016-09-20 16:27:00'),
(81, '01ecb12cc5ccf55b7f97c4dbbdb9abf3', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '10', NULL, 15, '2016-09-21 09:37:02', '2016-09-21 09:37:02'),
(82, '4634fa7c781401f953a4572890e4cf96', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '10', NULL, 15, '2016-09-21 09:37:55', '2016-09-21 09:37:55'),
(83, '1f0c95b63b7942ed3f0f42a77a81a862', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '10', NULL, 4, '2016-09-21 09:38:04', '2016-09-21 09:38:04'),
(84, '26bf1fab678691803bd58d88d337dc4b', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '10', NULL, 4, '2016-09-21 09:38:04', '2016-09-21 09:38:04'),
(85, 'aa7c26eef2fbfdefc20693350d088a81', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '11', NULL, 15, '2016-09-21 10:52:12', '2016-09-21 10:52:12'),
(86, 'f40650f793b8db4a0e934c8a68c83168', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '11', NULL, 4, '2016-09-21 10:52:55', '2016-09-21 10:52:55'),
(87, 'c484cf0d1f841f53720e00ce8f681264', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '11', NULL, 4, '2016-09-21 10:52:55', '2016-09-21 10:52:55'),
(88, 'a8194d71e6d8170d83e0f65c5ff0f767', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '11', NULL, 15, '2016-09-21 10:53:10', '2016-09-21 10:53:10'),
(89, '9b8168811521360a7db9617446f54973', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '11', NULL, 4, '2016-09-21 10:53:25', '2016-09-21 10:53:25'),
(90, '0a46ec7c6390eaeccd174e3d8ce9eba4', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '11', NULL, 4, '2016-09-21 10:53:25', '2016-09-21 10:53:25'),
(91, 'e275a466143c88bb2e3f8e84e67274f3', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '12', NULL, 15, '2016-09-21 13:36:11', '2016-09-21 13:36:11'),
(92, 'b8cfc8ff361dc2106dd71aeb4bbc485b', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '12', NULL, 15, '2016-09-21 13:42:02', '2016-09-21 13:42:02'),
(93, '2b254342026ce469c9a2dfda30825fb7', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '12', NULL, 4, '2016-09-21 13:47:06', '2016-09-21 13:47:06'),
(94, '396a2f0de9baf5b45b2aad4b91b4e90c', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '12', NULL, 4, '2016-09-21 13:47:06', '2016-09-21 13:47:06'),
(95, '68fd50a5e37aa390dd5f31e56905f530', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '13', NULL, 15, '2016-09-21 14:08:00', '2016-09-21 14:08:00'),
(96, '03152e28332d55c377a2d26ec8ac2a62', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '12', NULL, 15, '2016-09-21 14:08:33', '2016-09-21 14:08:33'),
(97, '777bc33d3cf0c5413898ae6d0b36372a', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '13', NULL, 15, '2016-09-21 14:09:22', '2016-09-21 14:09:22'),
(98, '74ea6c226adc90a1945fa0c09b4978de', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '14', NULL, 15, '2016-09-21 14:10:50', '2016-09-21 14:10:50'),
(99, '121f8b80fd0c85e43b5497e1ff0c5858', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '12', NULL, 4, '2016-09-21 14:12:11', '2016-09-21 14:12:11'),
(100, '06063e5da347c7307f91e11060d550d3', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '12', NULL, 4, '2016-09-21 14:12:11', '2016-09-21 14:12:11'),
(101, 'b073d3b6fe48efa5b1f172e066bf3c27', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '14', NULL, 4, '2016-09-21 14:13:38', '2016-09-21 14:13:38'),
(102, '4b0402952dafbe1d747cbe86a6767737', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '14', NULL, 4, '2016-09-21 14:13:38', '2016-09-21 14:13:38'),
(103, '62f3824400ef98e3388544d9c0fed875', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '12', NULL, 15, '2016-09-21 14:15:48', '2016-09-21 14:15:48'),
(104, '6ba420647cfff3a0a59f66063ea212dc', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '12', NULL, 15, '2016-09-21 14:17:29', '2016-09-21 14:17:29'),
(105, '19945e7e31bcfbc416be76215e216cf7', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '12', NULL, 4, '2016-09-21 14:18:52', '2016-09-21 14:18:52'),
(106, 'dfc43dfddeafc2e30c6c802dd14aee4d', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '12', NULL, 4, '2016-09-21 14:18:52', '2016-09-21 14:18:52'),
(107, '35174bc05cea8ba3e18fcd1d85917599', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '9', NULL, 15, '2016-09-21 14:31:33', '2016-09-21 14:31:33'),
(108, 'be2c175e55de2f6a904113b270089785', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '9', NULL, 4, '2016-09-21 14:32:09', '2016-09-21 14:32:09'),
(109, '88fbe50fad1777ff805ee80b02b60961', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '9', NULL, 4, '2016-09-21 14:32:09', '2016-09-21 14:32:09'),
(110, '691a5588150502739282183510fa28e3', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '11', NULL, 15, '2016-09-22 10:23:12', '2016-09-22 10:23:12'),
(111, 'f2db52a3a43de3ad9d5a36f2b1894093', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '12', NULL, 15, '2016-09-22 10:24:14', '2016-09-22 10:24:14'),
(112, '6852f27dbac8ce7dd119e85a208cd8fc', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '14', NULL, 15, '2016-09-22 10:24:24', '2016-09-22 10:24:24'),
(113, '11ac7541e7dea2411031d16c6eea41ec', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '11', NULL, 15, '2016-09-22 10:26:40', '2016-09-22 10:26:40'),
(114, '6a8a4af109f7580c7da663e37da91c5a', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '14', NULL, 15, '2016-09-22 10:29:17', '2016-09-22 10:29:17'),
(115, 'bfa36e293727d19a9783be143005bd4d', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '12', NULL, 15, '2016-09-22 10:32:28', '2016-09-22 10:32:28'),
(116, '15d0ff5d09dbfa9945faa829309e1211', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '9', NULL, 15, '2016-09-22 10:32:56', '2016-09-22 10:32:56'),
(117, '9e6010ff75e57c98beab35dbc7631d30', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '9', NULL, 4, '2016-09-22 10:33:47', '2016-09-22 10:33:47'),
(118, '881a7a07e9b9bc580c6d71becfe75a44', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '9', NULL, 4, '2016-09-22 10:33:47', '2016-09-22 10:33:47'),
(119, 'bffd6911b52a4003baefffe7b9a172e9', 'wisnu.arimurti@asyst.co.id', 'ptas67', 'avl', 'news_detail', '11', NULL, 4, '2016-09-22 10:34:02', '2016-09-22 10:34:02'),
(120, 'adb3708b20079d4363920aa3bb6770d3', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '11', NULL, 4, '2016-09-22 10:34:02', '2016-09-22 10:34:02'),
(121, '26a7558e955898bcd68af1ec71b4e266', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '11', NULL, 4, '2016-09-22 10:34:02', '2016-09-22 10:34:02'),
(122, 'e530de2b604abfb2da65fda3e5f58d8e', 'wisnu.arimurti@asyst.co.id', 'ptas67', 'avl', 'news_detail', '14', NULL, 4, '2016-09-22 10:34:21', '2016-09-22 10:34:21'),
(123, 'b1943bd1629529fd4efc0e565728e801', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '14', NULL, 4, '2016-09-22 10:34:21', '2016-09-22 10:34:21'),
(124, '221148df6dcecc2de14f2e423c7c5bfd', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '14', NULL, 4, '2016-09-22 10:34:21', '2016-09-22 10:34:21'),
(125, 'cfde9e5bfabda4383a5c7570fc9a3e1f', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '14', NULL, 15, '2016-09-22 10:46:45', '2016-09-22 10:46:45'),
(126, '8c898b0e1a6fab61b8eab40a95b52d68', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '9', NULL, 15, '2016-09-22 10:47:03', '2016-09-22 10:47:03'),
(127, 'e2a94a15fa07944c57b5156d3a8bdc9e', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '11', NULL, 15, '2016-09-22 10:47:41', '2016-09-22 10:47:41'),
(128, '03fe8008614dab7c61e4f75affde03cc', 'wisnu.arimurti@asyst.co.id', 'ptas67', 'avl', 'news_detail', '14', NULL, 4, '2016-09-22 10:48:02', '2016-09-22 10:48:02'),
(129, '6f4b7bb0bf449cfb7ab2fe02334cd10b', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '14', NULL, 4, '2016-09-22 10:48:02', '2016-09-22 10:48:02'),
(130, '2001485350b2e137f380d217d9d2e4fd', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '14', NULL, 4, '2016-09-22 10:48:02', '2016-09-22 10:48:02'),
(131, '2fa7ac0808b3e8f51d30ca18abd8752a', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '9', NULL, 4, '2016-09-22 10:48:12', '2016-09-22 10:48:12'),
(132, 'aa71191b0599411191b7ed5dc89654c3', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '9', NULL, 4, '2016-09-22 10:48:12', '2016-09-22 10:48:12'),
(133, '887aeeff1356501596612d782b5da023', 'wisnu.arimurti@asyst.co.id', 'ptas67', 'avl', 'news_detail', '11', NULL, 4, '2016-09-22 10:48:24', '2016-09-22 10:48:24'),
(134, '3cf730b352a920c2dad556186e733ccb', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '11', NULL, 4, '2016-09-22 10:48:24', '2016-09-22 10:48:24'),
(135, '70e64998a70bcbcd3d925fca163bad8f', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '11', NULL, 4, '2016-09-22 10:48:24', '2016-09-22 10:48:24'),
(136, 'b70239272dddd7cfe3bd727d3b1dd517', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '12', NULL, 15, '2016-09-22 10:49:09', '2016-09-22 10:49:09'),
(137, '193c1e5bc6ba8d5343fda2ebdf5b27a0', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '12', NULL, 15, '2016-09-22 10:49:30', '2016-09-22 10:49:30'),
(138, '6402bef0d6e06953df103b8c2ba677fe', 'wisnu.arimurti@asyst.co.id', 'ptas67', 'avl', 'news_detail', '12', NULL, 4, '2016-09-22 10:51:06', '2016-09-22 10:51:06'),
(139, 'abca858a55b99c5b6d17cf3e9b04feb3', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '12', NULL, 4, '2016-09-22 10:51:06', '2016-09-22 10:51:06'),
(140, 'c0456c09f681b56004de65bde44571d4', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '12', NULL, 4, '2016-09-22 10:51:06', '2016-09-22 10:51:06'),
(141, '7f0aa88ad5f1171171c8d16bb2a66aef', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '11', NULL, 15, '2016-09-22 10:51:31', '2016-09-22 10:51:31'),
(142, 'eb3624e284e637fe1539f5ea048747c3', 'ririn.zulandra@asyst.co.id', 'ririn', 'avl', 'news_detail', '11', NULL, 4, '2016-09-22 10:51:51', '2016-09-22 10:51:51'),
(143, '98c0e9af2fcca3cd70f0d494ecb77ac3', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '11', NULL, 4, '2016-09-22 10:51:51', '2016-09-22 10:51:51'),
(144, '26d2dc88525d7b6c95168e41b536379c', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '11', NULL, 4, '2016-09-22 10:51:51', '2016-09-22 10:51:51'),
(145, '9eb5bae2014d5b96d3ae15ea0daf0063', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '20', NULL, 3, '2016-09-23 11:19:10', '2016-09-23 11:19:10'),
(146, '3c541143d51f9ea29a1b2ad185da472e', NULL, NULL, 'announce', 'preview', '20', NULL, 3, '2016-09-23 11:19:10', '2016-09-23 11:19:10'),
(147, 'f6f2b7bbc41fb8d4be62f6ab733b4dc3', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '20', NULL, 3, '2016-09-23 11:20:55', '2016-09-23 11:20:55'),
(148, '4e5ce3a907cd2718b913429a785e749d', NULL, NULL, 'announce', 'preview', '20', NULL, 3, '2016-09-23 11:20:55', '2016-09-23 11:20:55'),
(149, '414e29b4f85eadf6e00368818094f1cc', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '19', NULL, 12, '2016-09-23 15:11:30', '2016-09-23 15:11:30'),
(150, '5806be6e171c139244daa2178501d63e', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '19', NULL, 12, '2016-09-23 15:11:30', '2016-09-23 15:11:30'),
(151, 'c6a90495060f5e473044f4feceea3c60', NULL, NULL, 'announce', 'preview', '19', NULL, 12, '2016-09-23 15:11:30', '2016-09-23 15:11:30'),
(152, 'f21544dc11edec4c73d1c0ccb7948249', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '21', NULL, 12, '2016-09-23 15:12:46', '2016-09-23 15:12:46'),
(153, '570f66fdfc8ac580691ee5e22245499c', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '21', NULL, 12, '2016-09-23 15:12:46', '2016-09-23 15:12:46'),
(154, '03426a4d318650d05aac1b17dabfa4de', NULL, NULL, 'announce', 'preview', '21', NULL, 12, '2016-09-23 15:12:46', '2016-09-23 15:12:46'),
(155, 'd563882e00e42cedd60ea26a54073e43', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '22', NULL, 15, '2016-09-23 15:29:37', '2016-09-23 15:29:37'),
(156, 'b02fb7fe546bc60d7ac27924944e1f88', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '23', NULL, 12, '2016-09-23 16:50:58', '2016-09-23 16:50:58'),
(157, '2f426108eba699627ef2a8ce8074d7c9', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '23', NULL, 12, '2016-09-23 16:50:58', '2016-09-23 16:50:58'),
(158, '484e9465e0eaa34af85f50d6adf762d4', NULL, NULL, 'announce', 'preview', '23', NULL, 12, '2016-09-23 16:50:58', '2016-09-23 16:50:58'),
(159, '67966519a2a4ac44476b4c9478038496', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '24', NULL, 12, '2016-09-23 16:52:39', '2016-09-23 16:52:39'),
(160, '27a14ae7ef6540ad49ad167bea00d4a9', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '24', NULL, 12, '2016-09-23 16:52:39', '2016-09-23 16:52:39'),
(161, '98fdfb87e2ed5c482a33644af0f6c034', NULL, NULL, 'announce', 'preview', '24', NULL, 12, '2016-09-23 16:52:39', '2016-09-23 16:52:39'),
(162, '5b2f65f8f18b1b47713289d578734b1b', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '29', NULL, 12, '2016-09-23 16:57:46', '2016-09-23 16:57:46'),
(163, '5f97d938d3a207dcabb3eaa49230896d', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '29', NULL, 12, '2016-09-23 16:57:46', '2016-09-23 16:57:46'),
(164, '1e6c62eb4e650afa28d5c7edaee38651', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '30', NULL, 12, '2016-09-27 11:28:34', '2016-09-27 11:28:34'),
(165, '2cd140468d5771a423f766b2911ff9e5', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '30', NULL, 12, '2016-09-27 11:28:34', '2016-09-27 11:28:34'),
(166, '17038526b8ff24babaf88713dc89e447', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '30', NULL, 3, '2016-09-27 11:29:55', '2016-09-27 11:29:55'),
(167, 'af336969bb8538e849c4cdb23f5f8484', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '30', NULL, 3, '2016-09-27 11:29:55', '2016-09-27 11:29:55'),
(168, 'e1d4a733beb3e80e16aa0695c93ac2c7', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '30', NULL, 12, '2016-09-27 11:33:42', '2016-09-27 11:33:42'),
(169, 'eea3d092fd6bb52d6353a92ed2f101b2', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '30', NULL, 12, '2016-09-27 11:33:42', '2016-09-27 11:33:42'),
(170, '17355d57c364bd897a63eeb6575cc2a4', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '30', NULL, 12, '2016-09-27 11:34:33', '2016-09-27 11:34:33'),
(171, '0847725c8c62e8f324eb8ad7c6f969a5', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '30', NULL, 12, '2016-09-27 11:34:33', '2016-09-27 11:34:33'),
(172, '1a44342214b78628d7a9405e8bb406b3', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '30', NULL, 12, '2016-09-27 14:06:48', '2016-09-27 14:06:48'),
(173, '7ed99e79f43ebe5006ea4ed4a02b3294', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '30', NULL, 12, '2016-09-27 14:06:49', '2016-09-27 14:06:49'),
(174, 'bb0ee5232da5ca401b16b2745376ca52', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '31', NULL, 12, '2016-09-27 14:37:06', '2016-09-27 14:37:06'),
(175, '22edc22a69626a215e0ce5fe3b77a92b', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '31', NULL, 12, '2016-09-27 14:37:06', '2016-09-27 14:37:06'),
(176, '46a2a6ace45dd98262321112100cec83', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '31', NULL, 3, '2016-09-27 14:40:08', '2016-09-27 14:40:08'),
(177, '38c7853d2666e180ed516f79770565ed', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '31', NULL, 3, '2016-09-27 14:40:08', '2016-09-27 14:40:08'),
(178, 'a43eddccb687418b8a0a8edb6fce2799', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '31', NULL, 12, '2016-09-27 14:49:05', '2016-09-27 14:49:05'),
(179, '720edf6d6eb9901440530daa0e0e1a52', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '31', NULL, 12, '2016-09-27 14:49:05', '2016-09-27 14:49:05'),
(180, 'cca562c00b77822b691912f85c58d4de', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '31', NULL, 3, '2016-09-27 14:50:36', '2016-09-27 14:50:36'),
(181, 'b9c80d18ee7b05c6951228acdfdfd851', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '31', NULL, 3, '2016-09-27 14:50:36', '2016-09-27 14:50:36'),
(182, 'b758819322583a3d79fc70c2cc93de25', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '31', NULL, 12, '2016-09-27 14:50:51', '2016-09-27 14:50:51'),
(183, '64525c2b247e0884e2176b3c6aa12402', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '31', NULL, 12, '2016-09-27 14:50:51', '2016-09-27 14:50:51'),
(184, 'ac0ded41f643ba449a1a3933acc11527', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '30', NULL, 12, '2016-09-27 14:51:55', '2016-09-27 14:51:55'),
(185, '0e61a84a27a57079c90cfd134134d37b', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '30', NULL, 12, '2016-09-27 14:51:55', '2016-09-27 14:51:55'),
(186, 'f61931806a071b60d664789dcc109695', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '31', NULL, 3, '2016-09-27 14:52:12', '2016-09-27 14:52:12'),
(187, '60109dbb2d729418da02eaacd96931c4', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '31', NULL, 3, '2016-09-27 14:52:12', '2016-09-27 14:52:12'),
(188, '9b847991e46eb101563cb7ab2910fa00', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '30', NULL, 3, '2016-09-27 14:52:21', '2016-09-27 14:52:21'),
(189, 'c3796bb71770ff0baa5c93cb18ad5f82', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '30', NULL, 3, '2016-09-27 14:52:21', '2016-09-27 14:52:21'),
(190, '6570ae898d8498f6e7cbb3272f6ad10d', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '30', NULL, 12, '2016-09-27 14:52:37', '2016-09-27 14:52:37'),
(191, '880f627873b3c9adff99b630b645abee', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '30', NULL, 12, '2016-09-27 14:52:37', '2016-09-27 14:52:37'),
(192, 'e5d59a69acc79b4c422b32f9274a778c', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '31', NULL, 12, '2016-09-27 14:52:44', '2016-09-27 14:52:44'),
(193, 'bbac8fef5c8c5f2e9a342a63e271eec3', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '31', NULL, 12, '2016-09-27 14:52:44', '2016-09-27 14:52:44'),
(194, '051ecb03788699de9cbb4eea415ea0e8', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '31', NULL, 12, '2016-09-27 15:49:54', '2016-09-27 15:49:54'),
(195, 'a789a134abbe7a57ab3fda4cbc5d9042', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '31', NULL, 12, '2016-09-27 15:49:55', '2016-09-27 15:49:55'),
(196, '7783b891f1a77ea208736d14a59d51ff', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '31', NULL, 3, '2016-09-27 15:50:10', '2016-09-27 15:50:10'),
(197, '94e5bb7c4a2357056b94caf8ca8868c8', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '31', NULL, 3, '2016-09-27 15:50:10', '2016-09-27 15:50:10'),
(198, 'd415e611e18b9e3a882cf97067449134', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '32', NULL, 15, '2016-09-28 14:07:36', '2016-09-28 14:07:36'),
(199, 'd0203a9ab42fae115a557cc2ea290fea', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '33', NULL, 15, '2016-09-28 14:09:19', '2016-09-28 14:09:19'),
(200, '94d7dcde423d1fb80cd5bd70e663a355', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '34', NULL, 12, '2016-09-28 15:42:22', '2016-09-28 15:42:22'),
(201, '1acdb7fa9cf77c6f074d26af61873b81', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '34', NULL, 12, '2016-09-28 15:42:22', '2016-09-28 15:42:22'),
(202, 'c22d3a73af6c10bc4d476c7b21ef2211', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '34', NULL, 3, '2016-09-28 15:43:58', '2016-09-28 15:43:58'),
(203, '1662460d9bbb55a18f97364dc9f6529b', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '34', NULL, 3, '2016-09-28 15:43:58', '2016-09-28 15:43:58'),
(204, '2fe1dd3b940145acf971776dbfc4b824', 'admasyst.ga@gmail.com', 'Adminasyst', 'open_bidding', 'form_update', '34', NULL, 12, '2016-09-29 09:41:31', '2016-09-29 09:41:31'),
(205, '5a4b3598bd3087022f8df609089cb613', 'admasyst.ga@gmail.com', 'Adminasyst', 'open_bidding', 'form_update', '34', NULL, 12, '2016-09-29 09:54:00', '2016-09-29 09:54:00'),
(206, 'a9181aeb5ebfa638b75cca4ce70b5332', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '34', NULL, 12, '2016-09-29 09:56:19', '2016-09-29 09:56:19'),
(207, '1ab2825c240f27582b11b9a563a55e8b', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '34', NULL, 12, '2016-09-29 09:56:19', '2016-09-29 09:56:19'),
(208, '31c875328ab1dd58fdf406b87c09661c', 'admasyst.ga@gmail.com', 'Adminasyst', 'open_bidding', 'form_update', '34', NULL, 12, '2016-09-29 09:57:43', '2016-09-29 09:57:43'),
(209, '2cbeb94dd589ef096c4273a5e64949b8', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '34', NULL, 12, '2016-09-29 10:04:52', '2016-09-29 10:04:52'),
(210, '1f32c29f79717b4d6ada00f9e1713219', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '34', NULL, 12, '2016-09-29 10:04:52', '2016-09-29 10:04:52'),
(211, '7e8ff7848ca5a239ab77d817cfcbf6ac', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '34', NULL, 12, '2016-09-29 13:55:33', '2016-09-29 13:55:33'),
(212, '1b1af9bff6afbb6a80a8e11fd38b66bf', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '34', NULL, 12, '2016-09-29 13:55:33', '2016-09-29 13:55:33'),
(213, 'fc33f140ad0d832c2772d4c7712f9099', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '34', NULL, 12, '2016-09-29 13:55:33', '2016-09-29 13:55:33'),
(214, '19eee4e5e7598020f1a731bab7e7d7b1', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '34', NULL, 3, '2016-09-29 13:56:01', '2016-09-29 13:56:01'),
(215, 'a5f6475e1ddfd0c2a4bd1dfb6be497b8', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '34', NULL, 3, '2016-09-29 13:56:06', '2016-09-29 13:56:06'),
(216, '4f1e8c2f9253f6dcd1f51f10ac6c1f2d', 'admasyst.ga@gmail.com', 'Adminasyst', 'direct_selection', 'form_update', '36', NULL, 12, '2016-09-29 18:36:37', '2016-09-29 18:36:37'),
(217, 'f2853de47308e2f00a8783fdb82c230a', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '31', NULL, 12, '2016-09-30 14:37:35', '2016-09-30 14:37:35'),
(218, 'fc588d986878fab1b3656fbcac0e8bdf', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '31', NULL, 12, '2016-09-30 14:37:35', '2016-09-30 14:37:35'),
(219, 'a58eb0f691463dec57ac2dc96bdc39a1', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '31', NULL, 12, '2016-09-30 14:37:36', '2016-09-30 14:37:36'),
(220, 'ad267294114a12d3ed9dae3492cfce53', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '31', NULL, 3, '2016-09-30 14:38:01', '2016-09-30 14:38:01'),
(221, 'e8fbd090509cd138bf9ec316ed115fbe', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '31', NULL, 3, '2016-09-30 14:38:01', '2016-09-30 14:38:01'),
(222, 'd5d17daa1ec0ee40931f8b45cecbd588', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '37', NULL, 12, '2016-10-03 09:51:51', '2016-10-03 09:51:51'),
(223, '7850d78654e0ff7e574066222582ed61', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '37', NULL, 12, '2016-10-03 09:51:58', '2016-10-03 09:51:58'),
(224, 'd85fc59a4d09ac55f5a9974f4318e24e', 'wisnu.arimurti@asyst.co.id', 'ptas67', NULL, NULL, NULL, NULL, 3, '2016-10-03 09:53:23', '2016-10-03 09:53:23'),
(225, '285a12c8d8fb04ec06c5d39002a3f789', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '30', NULL, 12, '2016-10-03 09:54:57', '2016-10-03 09:54:57'),
(226, '4f3f58b3509773c52c694f2b60910b9d', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '30', NULL, 12, '2016-10-03 09:54:57', '2016-10-03 09:54:57'),
(227, 'c6310b4b770282e643e0626a898943ca', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '30', NULL, 3, '2016-10-03 09:55:11', '2016-10-03 09:55:11'),
(228, 'eb5f60caf068109dc2fe4acbe8e74254', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '30', NULL, 3, '2016-10-03 09:55:11', '2016-10-03 09:55:11'),
(229, 'dc6da197e42db2e28d8e9ebe1d643a9c', 'wisnu.arimurti@asyst.co.id', 'ptas67', NULL, NULL, NULL, NULL, 3, '2016-10-03 09:58:23', '2016-10-03 09:58:23'),
(230, '48a7ba77747cb988e6ddbda7b3232895', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '30', NULL, 12, '2016-10-03 10:23:46', '2016-10-03 10:23:46'),
(231, '3784633e793f8a7cd6a2e3b87072fe13', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '30', NULL, 12, '2016-10-03 10:23:46', '2016-10-03 10:23:46'),
(232, '47c54145ae6c59c964674d9ab8f43f81', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '30', NULL, 3, '2016-10-03 10:24:17', '2016-10-03 10:24:17'),
(233, 'a086c853184146b06c17c3ebdcce42fd', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '30', NULL, 3, '2016-10-03 10:24:17', '2016-10-03 10:24:17'),
(234, '90ca8ce9edfb8c154f766a2da3423551', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '38', NULL, 15, '2016-10-03 10:48:53', '2016-10-03 10:48:53'),
(235, '99cb4e15718d0ecdbecc59e92b595efb', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '38', NULL, 15, '2016-10-03 10:48:53', '2016-10-03 10:48:53'),
(236, '6b8b55bd5dad1b543d1c1c2d13adfe13', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '38', NULL, 15, '2016-10-03 10:50:08', '2016-10-03 10:50:08'),
(237, '81adced590f1971e79ed94160b1b06f8', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '38', NULL, 15, '2016-10-03 10:50:08', '2016-10-03 10:50:08'),
(238, '4eb245e25754927f357f6704f17f4379', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '38', NULL, 4, '2016-10-03 10:51:04', '2016-10-03 10:51:04'),
(239, 'e523a462d4019ba8ddc479be0d2dd272', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '38', NULL, 4, '2016-10-03 10:51:04', '2016-10-03 10:51:04'),
(240, '3663764d705267ba226e8b16f27628fb', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '38', NULL, 15, '2016-10-03 10:51:18', '2016-10-03 10:51:18'),
(241, '7c4e1dc75b3ccaa0f3cd63c9f8f434af', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '38', NULL, 15, '2016-10-03 10:51:18', '2016-10-03 10:51:18'),
(242, '2a9549d9adc61eedf3c68da5d917f376', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '38', NULL, 4, '2016-10-03 10:51:29', '2016-10-03 10:51:29'),
(243, '927d2b65218dc055d65f2cad7d41b27d', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '38', NULL, 4, '2016-10-03 10:51:34', '2016-10-03 10:51:34'),
(244, '4685233ff80e742959f55096430e3542', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '39', NULL, 15, '2016-10-03 10:57:24', '2016-10-03 10:57:24'),
(245, '48c0b6d06e8c54a837a2fe67e63f4caa', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '39', NULL, 15, '2016-10-03 10:57:24', '2016-10-03 10:57:24'),
(246, 'f5861376f20186781490cd89c585a16f', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '39', NULL, 4, '2016-10-03 10:57:47', '2016-10-03 10:57:47'),
(247, 'f0a8f8042efeb47e66d4d462c6e73c08', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '39', NULL, 4, '2016-10-03 10:57:47', '2016-10-03 10:57:47'),
(248, '87dcc94fc358f1e1d0dd23a06d9629fa', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '39', NULL, 15, '2016-10-03 11:03:10', '2016-10-03 11:03:10'),
(249, 'a34ca1a1f334a108feb3a76199a8c10b', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '39', NULL, 15, '2016-10-03 11:03:10', '2016-10-03 11:03:10'),
(250, '51d2d2940eb0227cd169ce7e1d6aa16b', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '39', NULL, 4, '2016-10-03 11:03:48', '2016-10-03 11:03:48'),
(251, '7abda4d42e506f31d0ac1cf41c866a1c', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '39', NULL, 4, '2016-10-03 11:03:48', '2016-10-03 11:03:48'),
(252, '0a416604f6c752185f4f2cc823d55610', 'smibs.ga@gmail.com', 'smibs', 'blacklist', '', '', NULL, 2, '2016-10-03 13:23:35', '2016-10-03 13:23:35'),
(253, '3c9676562b036fa0c16d0f56c4d7d39d', 'smibk.ga@gmail.com', 'smibk', 'blacklist', '', '', NULL, 3, '2016-10-03 13:29:15', '2016-10-03 13:29:15'),
(254, '6838aab1b3c79ca2336df81482d47c51', 'wabimantoro@gmail.com', 'wildantest', 'blacklist', '', '', NULL, 3, '2016-10-03 13:29:15', '2016-10-03 13:29:15'),
(255, 'abda226cdbdcfd94ddd1c2ce554a1fa2', 'vpib.ga@gmail.com', 'vpib', 'blacklist', '', '', NULL, 4, '2016-10-03 13:35:58', '2016-10-03 13:35:58'),
(256, '84b7d408e3c1ca8af3680dfe1e915eb5', 'smibk.ga@gmail.com', 'smibk', 'blacklist', '', '', NULL, 3, '2016-10-03 13:45:12', '2016-10-03 13:45:12'),
(257, '5fe527b56a97238a3d501a27db375e7d', 'wabimantoro@gmail.com', 'wildantest', 'blacklist', '', '', NULL, 3, '2016-10-03 13:45:12', '2016-10-03 13:45:12'),
(258, '25f7bfecd3d5d59e763a03e21ce478a9', 'vpib.ga@gmail.com', 'vpib', 'blacklist', '', '', NULL, 4, '2016-10-03 13:54:39', '2016-10-03 13:54:39'),
(259, '9512888bff5a4033e23f7b529a4507c5', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '40', NULL, 12, '2016-10-03 14:27:01', '2016-10-03 14:27:01'),
(260, '57c421a7e3b4c21d4dab036963317b44', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '40', NULL, 12, '2016-10-03 14:27:01', '2016-10-03 14:27:01'),
(261, 'b3f386bd0e65beb0862e76027f868255', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '40', NULL, 3, '2016-10-03 14:34:23', '2016-10-03 14:34:23'),
(262, '9ff8c700597340610aa785b2504d22b1', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '40', NULL, 3, '2016-10-03 14:34:23', '2016-10-03 14:34:23'),
(263, '79a2db16c919d3b8741fbac0069481d7', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '40', NULL, 12, '2016-10-03 14:36:35', '2016-10-03 14:36:35'),
(264, '0b30dde62b212f7840ecc5507f5356fb', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '40', NULL, 12, '2016-10-03 14:36:35', '2016-10-03 14:36:35'),
(265, '05898c7a743538d5bccfc432deeef4dd', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '40', NULL, 3, '2016-10-03 14:37:07', '2016-10-03 14:37:07'),
(266, '0733bc90685f8b14b53aedf03115fbcf', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '40', NULL, 3, '2016-10-03 14:37:07', '2016-10-03 14:37:07'),
(267, 'fb8099f51fc74286437ce6e5417a8583', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '41', NULL, 12, '2016-10-03 14:42:20', '2016-10-03 14:42:20'),
(268, '96a5d700ba83a85e7ebb43644f555798', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '41', NULL, 12, '2016-10-03 14:42:20', '2016-10-03 14:42:20'),
(269, '1f7cec51486abb9bc9c841c1a6987f27', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '41', NULL, 3, '2016-10-03 14:42:52', '2016-10-03 14:42:52'),
(270, '2b4ab9c754ef549c307f5e8b0ecdc0c1', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '41', NULL, 3, '2016-10-03 14:42:52', '2016-10-03 14:42:52'),
(271, 'df7fc7d4fa37bc4b54b6460566d406df', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '42', NULL, 12, '2016-10-04 10:08:21', '2016-10-04 10:08:21'),
(272, 'd6aeb14c25011ac7a995f02b6dedfa3f', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '42', NULL, 12, '2016-10-04 10:08:21', '2016-10-04 10:08:21'),
(273, 'd49c08e1d728ef1325ab7a9b868f8a58', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '36', NULL, 12, '2016-10-04 10:24:03', '2016-10-04 10:24:03'),
(274, '372e0464e5613a396cc14ac84e98aa6e', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '36', NULL, 12, '2016-10-04 10:24:03', '2016-10-04 10:24:03'),
(275, 'dbae3e427d07f266f777ac8ce6553e38', 'wisnu.arimurti@asyst.co.id', 'ptas67', NULL, NULL, NULL, NULL, 3, '2016-10-04 10:24:49', '2016-10-04 10:24:49'),
(276, '3ef39f943e14d8f8f4ed9d0cbd555ba8', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '1', NULL, 12, '2016-10-04 11:30:25', '2016-10-04 11:30:25'),
(277, '9901c6072d7f66b98948129d2a1c6196', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '1', NULL, 12, '2016-10-04 11:30:25', '2016-10-04 11:30:25'),
(278, '813dbf2b3a94cb873af8a2e4c4dd00a9', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '1', NULL, 3, '2016-10-04 11:33:56', '2016-10-04 11:33:56'),
(279, 'eb8acfc80d4a5b5d956a6f3c96d22687', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '1', NULL, 3, '2016-10-04 11:33:56', '2016-10-04 11:33:56'),
(280, '454213233deea8b06ec48f52981ea247', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '1', NULL, 12, '2016-10-04 11:39:36', '2016-10-04 11:39:36'),
(281, 'acc336111712d1690a81d597d2855b21', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '1', NULL, 12, '2016-10-04 11:39:36', '2016-10-04 11:39:36'),
(282, 'af5e9bb8f61745238fb87709e8a0ef0d', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '1', NULL, 12, '2016-10-04 11:40:25', '2016-10-04 11:40:25'),
(283, '3cc48e7e6abb52a449b2433eb529416f', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '1', NULL, 12, '2016-10-04 11:40:25', '2016-10-04 11:40:25'),
(284, '9764befcbd1f3c013342ad3a57ceb222', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '1', NULL, 12, '2016-10-04 11:45:42', '2016-10-04 11:45:42'),
(285, '006297c3a7623095bb48263872b1d33d', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '1', NULL, 12, '2016-10-04 11:45:42', '2016-10-04 11:45:42'),
(286, 'faabeaa91ff8398835ae937e9ff23aea', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '1', NULL, 3, '2016-10-04 11:45:52', '2016-10-04 11:45:52'),
(287, '87845c3cd348652d5b3c5cd318b1e9b8', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '1', NULL, 3, '2016-10-04 11:45:52', '2016-10-04 11:45:52'),
(288, '101c1b5c195463cea77e774f58f276b8', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '2', NULL, 12, '2016-10-06 10:06:49', '2016-10-06 10:06:49'),
(289, '2f4837b6ef79f2797d83b10db5df0975', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '2', NULL, 12, '2016-10-06 10:06:49', '2016-10-06 10:06:49'),
(290, 'acf10d3b8b9d9fc613d2e842333de5b3', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '2', NULL, 12, '2016-10-06 10:10:17', '2016-10-06 10:10:17'),
(291, 'd20b0ad2eb377db58f5e1394e6a547b2', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '2', NULL, 12, '2016-10-06 10:10:17', '2016-10-06 10:10:17'),
(292, '2a9f1e803c636d39ac45f1b9a600309a', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '2/57f5c0b8d122d/7c47d27815c48db56011658ef2abd9d4111658', '2016-10-09 00:00:00', 3, '2016-10-06 10:10:48', '2016-10-06 10:10:48'),
(293, '3e792f2867e66d5a7b962d7a1959ceaf', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '2', NULL, 3, '2016-10-06 10:10:48', '2016-10-06 10:10:48'),
(294, 'fccf0a9a87fcf0ad585f4dd1db041390', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '2', NULL, 3, '2016-10-06 10:10:49', '2016-10-06 10:10:49'),
(295, 'e026b560ac4fbb37fa8e2827e5f41183', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '2', NULL, 12, '2016-10-06 10:10:58', '2016-10-06 10:10:58'),
(296, 'b5274eb1a19d5e2f0a3d23b87e79270b', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '2', NULL, 12, '2016-10-06 10:10:58', '2016-10-06 10:10:58'),
(297, '6d10207a3ffe30d22ba7754cd3653de9', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '2', NULL, 3, '2016-10-06 10:11:10', '2016-10-06 10:11:10'),
(298, 'f72a63ab4395db0221d61245d5e7b0d2', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '2', NULL, 3, '2016-10-06 10:11:10', '2016-10-06 10:11:10'),
(299, '7ea4757db08a2f22a14a37eef9005ee0', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '2', NULL, 12, '2016-10-06 10:11:21', '2016-10-06 10:11:21'),
(300, 'e43d640651df85b149d50c7827f471cb', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '2', NULL, 12, '2016-10-06 10:11:21', '2016-10-06 10:11:21'),
(301, '25609fa44b853123c69dcec6177ca7e4', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '2/57f5c0e4b39e8/0a9a04047d685586628ccd2113ee3d3f08ccd2', '2016-10-09 00:00:00', 3, '2016-10-06 10:11:32', '2016-10-06 10:11:32'),
(302, 'a1f496585f01b09e50a19a58475c6208', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '2', NULL, 3, '2016-10-06 10:11:32', '2016-10-06 10:11:32'),
(303, '92daad27b84b6cdb5872f62b4fb0c752', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '2', NULL, 3, '2016-10-06 10:11:32', '2016-10-06 10:11:32'),
(304, 'f5c70212525651f0843dbbef085a410d', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-06 13:32:31', '2016-10-06 13:32:31'),
(305, 'aea10690fcc1223305eefc0b58efb50b', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-06 13:32:31', '2016-10-06 13:32:31'),
(306, 'bdb39e6fd04ab42939c1ee8def781ddd', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-06 13:34:44', '2016-10-06 13:34:44'),
(307, '0ad22c1bd39777a2f703b5b118cbb317', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-06 13:34:44', '2016-10-06 13:34:44'),
(308, 'a1e018755d2913ae9379bc7cfff0cc6b', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-06 13:35:51', '2016-10-06 13:35:51'),
(309, '46d335937dfc60a2467e77a5e24c8bf4', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-06 13:35:51', '2016-10-06 13:35:51'),
(310, 'd5cc9f478e06fb470a4687ecad3b80e6', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57f5f0d74c9e0/3f23463260692acf8270c2c10a2ffa08370c2c', '2016-10-09 00:00:00', 3, '2016-10-06 13:36:07', '2016-10-06 13:36:07'),
(311, '1f3d4c556e82421b74cd7e99f8727dab', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-06 13:36:07', '2016-10-06 13:36:07'),
(312, '03dd1aae3ffaffa7fc0197cb33bc2c81', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-06 13:36:07', '2016-10-06 13:36:07'),
(313, '81a9c13df179cec70ce29dc2669cc2e9', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-06 14:34:09', '2016-10-06 14:34:09'),
(314, 'ee488cafc3b417b17be84c16c027e595', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-06 14:34:09', '2016-10-06 14:34:09'),
(315, 'bb2264479ce8bedaa9793dbfc8f87afc', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '4', NULL, 12, '2016-10-10 09:36:18', '2016-10-10 09:36:18'),
(316, '226e7af79e95f3af0b519fe73c0b2acd', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '4', NULL, 12, '2016-10-10 09:36:18', '2016-10-10 09:36:18'),
(317, '96f8c1a54be648b8cdaf4a1af5d9918b', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '4', NULL, 3, '2016-10-10 09:36:54', '2016-10-10 09:36:54'),
(318, 'c4865568234a0ad04402a708d68cb256', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '4', NULL, 3, '2016-10-10 09:36:54', '2016-10-10 09:36:54');
INSERT INTO `session_email` (`id`, `token`, `email`, `username`, `controller`, `method`, `parameter`, `expired_date`, `created_id`, `created_date`, `last_updated`) VALUES
(319, '65d1e7beb4c45a8b3d7e856e41b8c9ec', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '4', NULL, 12, '2016-10-10 09:41:33', '2016-10-10 09:41:33'),
(320, '1d4a137c267a311a06426dc3b831d1f7', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '4', NULL, 12, '2016-10-10 09:41:38', '2016-10-10 09:41:38'),
(321, 'f9214b8285cf686d967ff9476678e4f3', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '4', NULL, 3, '2016-10-10 09:41:58', '2016-10-10 09:41:58'),
(322, '3079da7c66b807515c498524dc37d332', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '4', NULL, 3, '2016-10-10 09:41:59', '2016-10-10 09:41:59'),
(323, '267155a96fb04f28d2bddaf0cfbf9476', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '4', NULL, 12, '2016-10-10 09:56:22', '2016-10-10 09:56:22'),
(324, 'd8215ffc3c9adc724cda272220631cdc', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '4', NULL, 12, '2016-10-10 09:56:22', '2016-10-10 09:56:22'),
(325, '4c954168da135346db2d44b68e0d5661', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '4', NULL, 3, '2016-10-10 09:57:06', '2016-10-10 09:57:06'),
(326, '47e66f64481e32413b6102551da063cf', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '4', NULL, 3, '2016-10-10 09:57:06', '2016-10-10 09:57:06'),
(327, 'd05c21acf46971e4ef37854f1e7c9f41', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '2', NULL, 12, '2016-10-10 10:07:32', '2016-10-10 10:07:32'),
(328, 'b0e733cb98a8ad3adf3c8b962ad41e39', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '2', NULL, 12, '2016-10-10 10:07:32', '2016-10-10 10:07:32'),
(329, '3a4695cfbbd52f36cf254103e62b4e79', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '2', NULL, 12, '2016-10-10 10:07:52', '2016-10-10 10:07:52'),
(330, '303049b88f763e3a68e2745bfeede1fc', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '2', NULL, 12, '2016-10-10 10:07:52', '2016-10-10 10:07:52'),
(331, 'a0ef4a7fc0b7a9007fd0abc462b6fb3a', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '2/57fb063505ab7/cf11d3ce7404461b0864ff954e9d16e7564ff9', '2016-10-13 00:00:00', 3, '2016-10-10 10:08:37', '2016-10-10 10:08:37'),
(332, '7b2ff5eb4154de5949e3faf533df3329', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '2', NULL, 3, '2016-10-10 10:08:37', '2016-10-10 10:08:37'),
(333, 'a44fc2f22961a267ce3b63ff1f65c9b6', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '2', NULL, 3, '2016-10-10 10:08:37', '2016-10-10 10:08:37'),
(334, 'fbef7b41db88d57d3191aa04008ec65b', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '2', NULL, 12, '2016-10-10 10:34:01', '2016-10-10 10:34:01'),
(335, 'a2047c23215507451faee9fe8d083c99', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '2', NULL, 12, '2016-10-10 10:34:01', '2016-10-10 10:34:01'),
(336, '03c206de67760e56b3fefec6ef678a5c', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '2', NULL, 12, '2016-10-10 10:34:43', '2016-10-10 10:34:43'),
(337, 'fbbf9cd2ab89db4850e85f76b3c3e1d5', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '2', NULL, 12, '2016-10-10 10:34:43', '2016-10-10 10:34:43'),
(338, '2b1b67d6f43b35e393e13a87a8eae364', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '2/57fb0c6ee41e2/24bf3c2ef0686587b5ebc4adedfa3a965ebc4a', '2016-10-13 00:00:00', 3, '2016-10-10 10:35:10', '2016-10-10 10:35:10'),
(339, '829f2833db40978636958ed0c40c5312', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '2', NULL, 3, '2016-10-10 10:35:11', '2016-10-10 10:35:11'),
(340, 'b02b7cba91318894fd9179fe9cfbfe2d', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '2', NULL, 3, '2016-10-10 10:35:11', '2016-10-10 10:35:11'),
(341, '6870e8b0bcc7238a105af10f9fa31276', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '5', NULL, 12, '2016-10-10 10:41:13', '2016-10-10 10:41:13'),
(342, 'ab78b49921f4c0408f35fa7ee8174df7', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '5', NULL, 12, '2016-10-10 10:41:13', '2016-10-10 10:41:13'),
(343, '7e658156acf84d06e00ae0b13d87afd1', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '5', NULL, 12, '2016-10-10 10:41:43', '2016-10-10 10:41:43'),
(344, '396986c9c7a146ce92241e72057d15be', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '5', NULL, 12, '2016-10-10 10:41:43', '2016-10-10 10:41:43'),
(345, 'cc66979f435c2830882155875394b2a3', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-12 07:45:24', '2016-10-12 07:45:24'),
(346, '02be26ca0207e52062f94b4916be6a6a', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-12 07:45:24', '2016-10-12 07:45:24'),
(347, 'b9c4dcd7be01f40616f1510f43896970', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-12 07:49:26', '2016-10-12 07:49:26'),
(348, 'c3bd52bc1e5408ef5f9e737a949797f7', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-12 07:49:26', '2016-10-12 07:49:26'),
(349, 'ac327e8651e6c85f1c857b491fdc5a92', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57fd88a1220c2/99286a9cd0b47feb58afb869354ff0b2fafb86', '2016-10-15 00:00:00', 3, '2016-10-12 07:49:37', '2016-10-12 07:49:37'),
(350, 'aacbbb4eb9d390de795102081a38169d', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-12 07:49:37', '2016-10-12 07:49:37'),
(351, 'b541e093fac0bd70519689045376b59a', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-12 07:49:37', '2016-10-12 07:49:37'),
(352, '171cc9993aef7f5142aebab25b67b9d9', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-12 07:52:40', '2016-10-12 07:52:40'),
(353, '8167b98766ce64e22705ae5899bfa3a9', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-12 07:52:40', '2016-10-12 07:52:40'),
(354, '52234c2017c30a636dd9607a3c96501c', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57fd897ed3b41/7355fb7c3637d47b079ead8df2be9baca9ead8', '2016-10-15 00:00:00', 3, '2016-10-12 07:53:18', '2016-10-12 07:53:18'),
(355, '0d5aa7b4f3c459fd15b4bd2d4adf766a', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-12 07:53:18', '2016-10-12 07:53:18'),
(356, 'be5dccaf8f52faedcec12afe9886277a', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-12 07:53:18', '2016-10-12 07:53:18'),
(357, '14db03f0c4cce4f84741470e32bf3cc9', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-12 08:12:05', '2016-10-12 08:12:05'),
(358, '2453ccbb26357322f2c9f5b18d574a29', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-12 08:12:05', '2016-10-12 08:12:05'),
(359, '769581b804666a8a72f9c083c2d50746', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57fd8dff14411/b93a50b38043f0a210bf9e0b318d3a25abf9e0', '2016-10-15 00:00:00', 3, '2016-10-12 08:12:31', '2016-10-12 08:12:31'),
(360, '37a634820f441931d4bb897129502a24', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-12 08:12:31', '2016-10-12 08:12:31'),
(361, 'a8c7214c4ae57492c899de21c862ab14', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-12 08:12:31', '2016-10-12 08:12:31'),
(362, '08dbbf8ad41892c3d5ad5cfe652ca684', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '2', NULL, 12, '2016-10-12 10:42:16', '2016-10-12 10:42:16'),
(363, 'da809b5a1c7f4d629b4afb1934ea2686', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '2', NULL, 12, '2016-10-12 10:42:16', '2016-10-12 10:42:16'),
(364, '6b8aa7cc602192c439efc5be4571e700', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '2', NULL, 12, '2016-10-12 10:43:06', '2016-10-12 10:43:06'),
(365, '899416d4fa7edf8695d455133884caf0', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '2', NULL, 12, '2016-10-12 10:43:06', '2016-10-12 10:43:06'),
(366, '04042e9ce8c210715f1f36b296865eff', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-12 12:23:02', '2016-10-12 12:23:02'),
(367, '24e4bc4a9133e70853980ca960704128', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-12 12:23:02', '2016-10-12 12:23:02'),
(368, '819f8a721123b7917965ff30d2a82cef', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57fdc8dc2f2d1/6256026e424151b9542692b7bfe28fc0c2692b', '2016-10-15 00:00:00', 3, '2016-10-12 12:23:40', '2016-10-12 12:23:40'),
(369, 'c6a61713119650a8ffc63b58bf557128', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-12 12:23:40', '2016-10-12 12:23:40'),
(370, '4104a6e42f1b13bef2455cf0f23c6288', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-12 12:23:40', '2016-10-12 12:23:40'),
(371, 'f992c225e64290b71808dbff1f57e7ad', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57fdc8dc85489/27bac8232cb0c63c12891911d0a95f7e189191', '2016-10-15 00:00:00', 3, '2016-10-12 12:23:40', '2016-10-12 12:23:40'),
(372, '0effc7f10e1770fe7eacbfc2ec161f26', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-12 12:23:40', '2016-10-12 12:23:40'),
(373, '65601a160caca9dad0cb23b24b1d8517', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-12 12:23:40', '2016-10-12 12:23:40'),
(374, 'b064e58c213e3efcf4038a9350fd48c6', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '2', NULL, 12, '2016-10-12 13:27:16', '2016-10-12 13:27:16'),
(375, 'cfdd34a35f7d1a5e506aebfbb67b726c', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '2', NULL, 12, '2016-10-12 13:27:16', '2016-10-12 13:27:16'),
(376, 'bebc717a289f0aac220f34ed9d0365ce', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '6', NULL, 12, '2016-10-12 13:31:46', '2016-10-12 13:31:46'),
(377, '85f8d468db14ec77d8798edcc16552c3', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '6', NULL, 12, '2016-10-12 13:31:47', '2016-10-12 13:31:47'),
(378, 'dd97733e4f0eb324a6f94f6e1adc9dd3', 'vendor.regis@gmail.com', 'PT Vendor Indonesia', 'register', 'form_register_invitation', '6/57fdd926d132b/3e4d7e3bccd9fba9c28be822d74fb965a8be82', '2016-10-15 00:00:00', 3, '2016-10-12 13:33:10', '2016-10-12 13:33:10'),
(379, '5b7d239826455f7bcb426e0047ed5017', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '6', NULL, 3, '2016-10-12 13:33:10', '2016-10-12 13:33:10'),
(380, 'a17fd4f1cef2dda294631aa7c7e9ed0c', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '6', NULL, 3, '2016-10-12 13:33:11', '2016-10-12 13:33:11'),
(381, '6902789693b1681549f038997604763b', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '7', NULL, 12, '2016-10-12 22:07:39', '2016-10-12 22:07:39'),
(382, 'a7560afbdc493ee27b66488ac5dfe144', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '7', NULL, 12, '2016-10-12 22:07:39', '2016-10-12 22:07:39'),
(383, 'c3806d7bcc02592cb82d3ec161bfcc95', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 08:57:32', '2016-10-13 08:57:32'),
(384, '5153e6092f1dc96f277116c46cb00eac', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 08:57:32', '2016-10-13 08:57:32'),
(385, '37ab62268017c503a94026d5d9cfa0c8', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 09:03:13', '2016-10-13 09:03:13'),
(386, '309d53e54992f262a97457effb749bb3', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 09:03:13', '2016-10-13 09:03:13'),
(387, '012b21bf35fd500505740b506e0aed20', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57feeb69a81be/2d0e2e2ae5dcbfc7b5ff4dcf979f6ab0bff4dc', '2016-10-16 00:00:00', 3, '2016-10-13 09:03:21', '2016-10-13 09:03:21'),
(388, 'fc4985f941d75fcc6615468e6edec4b4', NULL, NULL, 'register', 'form_register_invitation', '3/57feeb69a81be/2d0e2e2ae5dcbfc7b5ff4dcf979f6ab0bff4dc', '2016-10-16 00:00:00', 3, '2016-10-13 09:03:21', '2016-10-13 09:03:21'),
(389, 'edc81d199c1a5e960d94bf20ca4b5b7e', 'admasyst.ga@gmail.com', 'Adminasyst', 'news_public', 'form_update', '8', NULL, 15, '2016-10-13 10:18:15', '2016-10-13 10:18:15'),
(390, 'a5eb9280e12f80db2f197c8f6b166ce3', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '1', NULL, 12, '2016-10-13 13:29:38', '2016-10-13 13:29:38'),
(391, 'e4d5907105a7df42070c0f7b96a32227', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '1', NULL, 12, '2016-10-13 13:29:38', '2016-10-13 13:29:38'),
(392, 'e72c3ecd5a22123a5743bb24520019a5', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 13:32:31', '2016-10-13 13:32:31'),
(393, '2f4914645f4a47b416902df575fe45ee', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 13:32:31', '2016-10-13 13:32:31'),
(394, 'c5d190e5ff64b6098247fe7e1670cd2e', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 13:35:20', '2016-10-13 13:35:20'),
(395, '491af7ea4615a3e4e8c15c3276b5cd09', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 13:35:20', '2016-10-13 13:35:20'),
(396, '7d6138eb241bc464c290aa9425ef30c7', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 13:44:51', '2016-10-13 13:44:51'),
(397, 'd65bdd1bfc66419424d975b285c3436f', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 13:44:51', '2016-10-13 13:44:51'),
(398, 'f225e012bf1cd5608dd941359b25a263', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:12:13', '2016-10-13 14:12:13'),
(399, '8585690813807fdd7287e2e813b2335e', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:12:13', '2016-10-13 14:12:13'),
(400, '99f44832b13e66837a406ba2ed21d458', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:12:25', '2016-10-13 14:12:25'),
(401, 'a6c35a24c228fc268d6f00d16c6c21bc', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:12:25', '2016-10-13 14:12:25'),
(402, 'ee00e91158ec8a1913a46ac4569fc406', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:12:47', '2016-10-13 14:12:47'),
(403, '5a40a392ea2fb9361605de29950e2a06', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:12:47', '2016-10-13 14:12:47'),
(404, '6f5afaf3edd833856ed06e6e66d2bbae', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:13:08', '2016-10-13 14:13:08'),
(405, '32aab5eb59b154600a6855e36dd3e272', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:13:08', '2016-10-13 14:13:08'),
(406, '29c80161aa65d6dc84c102603b750c83', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '11', NULL, 12, '2016-10-13 14:14:50', '2016-10-13 14:14:50'),
(407, 'd1bc3ec90965edc87e023d0811214e55', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '11', NULL, 12, '2016-10-13 14:14:50', '2016-10-13 14:14:50'),
(408, '8f886cd4c4ee3bce7a437b3748e408f7', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '11', NULL, 12, '2016-10-13 14:15:48', '2016-10-13 14:15:48'),
(409, 'e8b8ba157c46d7be19d341c723e98162', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '11', NULL, 12, '2016-10-13 14:15:48', '2016-10-13 14:15:48'),
(410, '3c3146437fb315c6db34718a34529fe4', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:17:14', '2016-10-13 14:17:14'),
(411, '2f83edd82aab8c5d666c69466bcb45ca', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:17:14', '2016-10-13 14:17:14'),
(412, '21a7e467893792003421a3ee2040d7d5', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:17:29', '2016-10-13 14:17:29'),
(413, '07ec8671b52aa56a6a6cca23537aefac', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:17:29', '2016-10-13 14:17:29'),
(414, 'd03ed1b5fca42077ed79aff2f644a9d1', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:18:03', '2016-10-13 14:18:03'),
(415, '6dc8612f5f60566b678414c07b003bae', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:18:03', '2016-10-13 14:18:03'),
(416, '03fb646e3fa4502d08f24e00d6848814', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:19:47', '2016-10-13 14:19:47'),
(417, '0c9834d239896f9d3e7072b7a212735d', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:19:47', '2016-10-13 14:19:47'),
(418, 'f122fd5bc8897ddd18ca06c83e8c3d22', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '10', NULL, 12, '2016-10-13 14:20:36', '2016-10-13 14:20:36'),
(419, '356428158fc919b76f3a748c9bd323cb', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '10', NULL, 12, '2016-10-13 14:20:36', '2016-10-13 14:20:36'),
(420, 'e151784adfd2f99242cbc8a789806e9a', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '10', NULL, 12, '2016-10-13 14:23:27', '2016-10-13 14:23:27'),
(421, 'd1ac746019f55026582c5b39e26c8c9d', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '10', NULL, 12, '2016-10-13 14:23:27', '2016-10-13 14:23:27'),
(422, '86916196499f7045bb61bb7a662e5580', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:25:05', '2016-10-13 14:25:05'),
(423, '304e60d139df701a74c0245b2c160cbd', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:25:05', '2016-10-13 14:25:05'),
(424, 'e4f2f49c6bacd4e177e08141349265aa', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:25:38', '2016-10-13 14:25:38'),
(425, 'a73f42f6b42f87035df7f92035235551', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-13 14:25:38', '2016-10-13 14:25:38'),
(426, 'ed5905a0610b9a526bd3221948274ec1', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57ff37036e626/5bcbb2506996e1fa52dbb312f185c334ddbb31', '2016-10-16 00:00:00', 3, '2016-10-13 14:25:55', '2016-10-13 14:25:55'),
(427, '1e0a1e573f20c22257e87994df74e73e', NULL, NULL, 'register', 'form_register_invitation', '3/57ff37036e626/5bcbb2506996e1fa52dbb312f185c334ddbb31', '2016-10-16 00:00:00', 3, '2016-10-13 14:25:55', '2016-10-13 14:25:55'),
(428, '3242406e67df4f4ea15ad43dbacf50e6', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-13 14:26:37', '2016-10-13 14:26:37'),
(429, 'a4f53805711993bcee24a1ee1e229424', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-13 14:26:37', '2016-10-13 14:26:37'),
(430, 'ffe00a2463b144fb037693f332326ced', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '5', NULL, 12, '2016-10-13 14:27:16', '2016-10-13 14:27:16'),
(431, 'a11511a27dd44d79ccdf1fd69eb7077f', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '5', NULL, 12, '2016-10-13 14:27:16', '2016-10-13 14:27:16'),
(432, 'c0213a9197d4e90a4d2dc0a94df648bb', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '5', NULL, 3, '2016-10-13 14:27:37', '2016-10-13 14:27:37'),
(433, 'f4cbb33d0300982a8b200dd4492c6bbc', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '5', NULL, 3, '2016-10-13 14:27:38', '2016-10-13 14:27:38'),
(434, '0f8563582fe008529b38ddc63c34caf7', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '2', NULL, 12, '2016-10-13 14:28:08', '2016-10-13 14:28:08'),
(435, 'b0bf530b91ebdb431bb756c9cdbc33ec', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '2', NULL, 12, '2016-10-13 14:28:08', '2016-10-13 14:28:08'),
(436, 'ad946560b5e72660a43277a12b9369b7', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '2/57ff37902c9b5/89e5208c74d5dd1a7d00bd03284463dcd00bd0', '2016-10-16 00:00:00', 3, '2016-10-13 14:28:16', '2016-10-13 14:28:16'),
(437, '25548c7425f639a1a178e8052f739d14', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '2', NULL, 3, '2016-10-13 14:28:16', '2016-10-13 14:28:16'),
(438, 'b19379950fb6e3e7d43d7ec3beb55a09', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '2', NULL, 3, '2016-10-13 14:28:16', '2016-10-13 14:28:16'),
(439, '868d484ab9c04ebdcb7a94ad54174d5e', 'admasyst.ga@gmail.com', 'Adminasyst', 'news_public', 'form_update', '8', NULL, 15, '2016-10-13 16:02:34', '2016-10-13 16:02:34'),
(440, '6cb18c335a159d9e6b2ebf168615a16e', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57ff9c294ef1d/0a5fd00addf08b102d6cdbeddeac0e5ef6cdbe', '2016-10-16 00:00:00', 3, '2016-10-13 21:37:29', '2016-10-13 21:37:29'),
(441, '040078612259453e9b16e2b5c3ab4f73', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57ff9c89ad632/7d52127625f58243a51432704af25969114327', '2016-10-16 00:00:00', 3, '2016-10-13 21:39:05', '2016-10-13 21:39:05'),
(442, 'b8014f72b23ab060e7b92ee21500e0b2', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57ff9c987e914/f8ae78fe712bdf364d0243a7a091a84a50243a', '2016-10-16 00:00:00', 3, '2016-10-13 21:39:20', '2016-10-13 21:39:20'),
(443, '82daf6eb3d759b2ee6c0c9445fd6d971', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57ff9cd89edb9/53e308599fed57e00d4057b4f01407a174057b', '2016-10-16 00:00:00', 3, '2016-10-13 21:40:24', '2016-10-13 21:40:24'),
(444, '4c89af9eee02b0f1fda68feddf1320c7', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57ff9ce981e29/1353361b642e8129620ea99aa3b109d740ea99', '2016-10-16 00:00:00', 3, '2016-10-13 21:40:41', '2016-10-13 21:40:41'),
(445, '7401c3d1ca49098751b0715b67d090a4', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57ff9d79effe0/03973c09761c4f75c0db7391d8b614da3db739', '2016-10-16 00:00:00', 3, '2016-10-13 21:43:05', '2016-10-13 21:43:05'),
(446, '3b9e42776563f4521e23c15bfe9f6ec7', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/57ff9d9fc057a/ab842ca4856019bc59aad9d52b1bc42dcaad9d', '2016-10-16 00:00:00', 3, '2016-10-13 21:43:43', '2016-10-13 21:43:43'),
(447, 'dd1d64f1dffb4de79a26a118f03da0cf', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '8', NULL, 14, '2016-10-14 09:07:06', '2016-10-14 09:07:06'),
(448, 'c3099c0883ce7cf39db0ce4f547bedb3', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 09:09:52', '2016-10-14 09:09:52'),
(449, '782d2d59f11ad524b9016b79e55536c7', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 10:01:08', '2016-10-14 10:01:08'),
(450, '1f91c3ce8a3568075c15977185105d8f', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 10:11:41', '2016-10-14 10:11:41'),
(451, 'b964d02e46dc32722c06b5ed1c3173d3', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 10:13:23', '2016-10-14 10:13:23'),
(452, 'a1c996f48e0300fc5ba9997af083e76f', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 10:39:09', '2016-10-14 10:39:09'),
(453, '91d86f2a04116697c025e5ac453e28bb', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 10:48:01', '2016-10-14 10:48:01'),
(454, '806e9001ab581a517b4a06b2357aa3ee', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 11:10:57', '2016-10-14 11:10:57'),
(455, '132c7bab3e154404fc22905e494d6dc2', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 11:20:10', '2016-10-14 11:20:10'),
(456, 'c892ea03e2e9bcd5e5e7d79083657b1d', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 11:20:31', '2016-10-14 11:20:31'),
(457, '57c6e97d66165ff851c5c33faa0d38e5', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 11:22:31', '2016-10-14 11:22:31'),
(458, 'fe9ca0a9674a94e27010edcb623753e6', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 11:23:02', '2016-10-14 11:23:02'),
(459, 'e9bc9f625d462db8ee9403a0040e557b', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 11:23:54', '2016-10-14 11:23:54'),
(460, '9ec88e4485be32a8e7588afe5cf7b9ae', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 11:25:13', '2016-10-14 11:25:13'),
(461, '265910a86bed5745840f8fe45da2d29c', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 11:25:35', '2016-10-14 11:25:35'),
(462, '0ce324ecdc37c85997a1cb67b47dcd7a', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 11:26:33', '2016-10-14 11:26:33'),
(463, '682dc79bd18d005022cdb6aeb4e8701f', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '12', NULL, 12, '2016-10-14 11:28:05', '2016-10-14 11:28:05'),
(464, '7835feddc39b2de04a0fdf5de74f74c3', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '13', NULL, 12, '2016-10-14 11:28:50', '2016-10-14 11:28:50'),
(465, '24c5c84d2c16e334084dcfda7611066c', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '13', NULL, 12, '2016-10-14 11:28:50', '2016-10-14 11:28:50'),
(466, 'aa0427e104bc3fdcd607c090914be63a', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 13:18:02', '2016-10-14 13:18:02'),
(467, '5a036bb44186b7aa691317944072bf7d', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-14 13:18:02', '2016-10-14 13:18:02'),
(468, 'f726d3bc2760bc1abc751b8b3e93bcb1', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/580078e5cd6f1/0e285205b558eba8ba33a27296f4cb8df33a27', '2016-10-17 00:00:00', 3, '2016-10-14 13:19:17', '2016-10-14 13:19:17'),
(469, '7b7c0449cc04f3b9b25fa71913c21f79', 'zulandraririn@gmail.com', NULL, 'register', 'form_register_invitation', '3/580078e5cd6f1/0e285205b558eba8ba33a27296f4cb8df33a27', '2016-10-17 00:00:00', 3, '2016-10-14 13:19:17', '2016-10-14 13:19:17'),
(470, 'f7668ae1523d2634eb1aeec24d65e403', 'ririn.zulandra@asyst.co.id', NULL, 'register', 'form_register_invitation', '3/580078e5cd6f1/0e285205b558eba8ba33a27296f4cb8df33a27', '2016-10-17 00:00:00', 3, '2016-10-14 13:19:17', '2016-10-14 13:19:17'),
(471, '42c4f1a1c7927e933b946629eb89d732', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-14 13:19:17', '2016-10-14 13:19:17'),
(472, 'ee0be2579766fdc5ad5505581ca9a15c', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-14 13:19:17', '2016-10-14 13:19:17'),
(473, '4ed36a215dc4b08b5a21f2ff3869e5fb', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 13:53:28', '2016-10-14 13:53:28'),
(474, '28a0d361ef1ef6a0590996aa86745974', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-14 13:53:28', '2016-10-14 13:53:28'),
(475, 'fba75e87253c4efccfa8325660a89f87', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/5800811399cb8/a3fb94ab7659ea7b1d6eb1382078f7c006eb13', '2016-10-17 00:00:00', 3, '2016-10-14 13:54:11', '2016-10-14 13:54:11'),
(476, 'cbba14a9f7dff488fe618d3ff46b4d90', 'limitedbidding@gmail.com', NULL, 'register', 'form_register_invitation', '3/5800811399cb8/a3fb94ab7659ea7b1d6eb1382078f7c006eb13', '2016-10-17 00:00:00', 3, '2016-10-14 13:54:11', '2016-10-14 13:54:11'),
(477, '2e03777c79c340b6515cef06c0decad3', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-14 13:54:11', '2016-10-14 13:54:11'),
(478, 'c3af3e249ceab316b437502d1d628be9', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-14 13:54:11', '2016-10-14 13:54:11'),
(479, '98bfe97977e069a2aa408efc5fadcb37', 'admasyst.ga@gmail.com', 'Adminasyst', 'news_public', 'form_update', '14', NULL, 15, '2016-10-14 14:03:29', '2016-10-14 14:03:29'),
(480, '8b355b617b0d7288c2607c95db033050', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '14', NULL, 14, '2016-10-14 14:04:49', '2016-10-14 14:04:49'),
(481, 'a5986e9e44a7dda971a759ac60aecb1b', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '14', NULL, 14, '2016-10-14 14:04:49', '2016-10-14 14:04:49'),
(482, '5b9de7598aaba10c716815c67d894752', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '14', NULL, 4, '2016-10-14 14:08:43', '2016-10-14 14:08:43'),
(483, '17f2f3b4ee7e15c19d79a25c46c7c882', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '14', NULL, 4, '2016-10-14 14:08:43', '2016-10-14 14:08:43'),
(484, '6fdb8a93f8ce673746cb519be4058562', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '14', NULL, 15, '2016-10-14 14:12:53', '2016-10-14 14:12:53'),
(485, '882afdf33192fd93262f071f291b5984', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '14', NULL, 15, '2016-10-14 14:12:53', '2016-10-14 14:12:53'),
(486, '49ef5fc399ffd9a405be349e00eda2fb', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 14:40:44', '2016-10-14 14:40:44'),
(487, '0cc415e6b0eea601fbfa1e08522f161b', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-14 14:40:44', '2016-10-14 14:40:44'),
(488, '86455a5ca0aa67483095d33a05edb715', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/58008c2b61353/a0a9f9a3735cb21c3e7a7c735f357d23b7a7c7', '2016-10-17 00:00:00', 3, '2016-10-14 14:41:31', '2016-10-14 14:41:31'),
(489, 'a688b64da93ab376b70d72226d7e5ce4', 'limitedbidding@gmail.com', NULL, 'register', 'form_register_invitation', '3/58008c2b61353/a0a9f9a3735cb21c3e7a7c735f357d23b7a7c7', '2016-10-17 00:00:00', 3, '2016-10-14 14:41:31', '2016-10-14 14:41:31'),
(490, 'f4f258941ee9e3ce0c09a9c8461f18a3', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-14 14:41:31', '2016-10-14 14:41:31'),
(491, '116d4945cadf817ff0071801cac77da1', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-14 14:41:31', '2016-10-14 14:41:31'),
(492, '708a4d98d7fb0dc9c0ee1b20b31219fc', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 14:44:55', '2016-10-14 14:44:55'),
(493, 'eff3390d6533809a8f0cbcfe6166748c', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-14 14:44:56', '2016-10-14 14:44:56'),
(494, '8cc895428685146fb5cc9db283cc4305', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/58008d0123f58/3ddce23ce22eac6d273d856e9183fb9c13d856', '2016-10-17 00:00:00', 3, '2016-10-14 14:45:05', '2016-10-14 14:45:05'),
(495, '89bb87d0063c17cf407caa1765c1560e', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-14 14:45:10', '2016-10-14 14:45:10'),
(496, '41f28ad4b888f466f5064e979c7d9ce3', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-14 14:45:10', '2016-10-14 14:45:10'),
(497, 'a9a091851c1823cfe478d4cfc892b07e', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '3', NULL, 12, '2016-10-14 14:45:34', '2016-10-14 14:45:34'),
(498, '95de053aa390fd45f3d9bcdd252501d4', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '3', NULL, 12, '2016-10-14 14:45:34', '2016-10-14 14:45:34'),
(499, 'e939ef511ef47be6a057a1637ec97680', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '3/58008d3aee554/8406798818fcb6940e39580d4b5e4e2c139580', '2016-10-17 00:00:00', 3, '2016-10-14 14:46:02', '2016-10-14 14:46:02'),
(500, 'b70d84d43cd9fccb34fdc5774f439b89', 'limitedbidding@gmail.com', NULL, 'register', 'form_register_invitation', '3/58008d3aee554/8406798818fcb6940e39580d4b5e4e2c139580', '2016-10-17 00:00:00', 3, '2016-10-14 14:46:03', '2016-10-14 14:46:03'),
(501, '2a41eae98e55185dcf0b8c4ade730766', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '3', NULL, 3, '2016-10-14 14:46:03', '2016-10-14 14:46:03'),
(502, 'e2b53c0c4bbeece21ab187499f2a6258', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '3', NULL, 3, '2016-10-14 14:46:03', '2016-10-14 14:46:03'),
(503, '681cdacfd32515c1b114995d7ea26e03', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '4', NULL, 3, '2016-10-15 17:50:01', '2016-10-15 17:50:01'),
(504, 'dc2f1a0d07cb8450a5435d245b045193', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '4', NULL, 3, '2016-10-15 17:50:01', '2016-10-15 17:50:01'),
(505, '61da47ec8ec3872181e176f6c3724b70', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '15', NULL, 15, '2016-10-17 09:08:05', '2016-10-17 09:08:05'),
(506, '0ab7cb64ca2055623f509f9596aaa20c', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '15', NULL, 15, '2016-10-17 09:08:05', '2016-10-17 09:08:05'),
(507, '585581f1f82a40e3e417c642bd0fcf0b', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '15', NULL, 4, '2016-10-17 09:10:30', '2016-10-17 09:10:30'),
(508, '6cdfb37eb6640e32ef12463c42d5bb4f', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '15', NULL, 4, '2016-10-17 09:10:30', '2016-10-17 09:10:30'),
(509, '21978bb5a2a6f9b191bfadb67e7324c7', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '16', NULL, 15, '2016-10-17 09:16:20', '2016-10-17 09:16:20'),
(510, '674e7fc5339b7c69e85bafe248b6c31b', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '16', NULL, 15, '2016-10-17 09:16:20', '2016-10-17 09:16:20'),
(511, 'ca19f58436c1e5245721ae85bcea4163', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '16', NULL, 15, '2016-10-17 09:17:42', '2016-10-17 09:17:42'),
(512, '1337d374d2a5c47d653c7e52a68ae194', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '16', NULL, 15, '2016-10-17 09:17:42', '2016-10-17 09:17:42'),
(513, 'bafa8824a19f917c1e22edd912512450', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '15', NULL, 15, '2016-10-17 09:19:27', '2016-10-17 09:19:27'),
(514, 'a689fbc3cfd42bb390e4ccd0c3f24f3a', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '15', NULL, 15, '2016-10-17 09:19:28', '2016-10-17 09:19:28'),
(515, 'e1cdf5cc4a471463b6ae437976e05cd7', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '15', NULL, 4, '2016-10-17 09:20:24', '2016-10-17 09:20:24'),
(516, 'dd72ebacf9ec31506546d5e1d2d3b995', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '15', NULL, 4, '2016-10-17 09:20:24', '2016-10-17 09:20:24'),
(517, 'b5ce2ff5218c46bf7aebc59e1ef175a7', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '17', NULL, 12, '2016-10-17 09:33:11', '2016-10-17 09:33:11'),
(518, '887345cd521cd5c136a8557afe609257', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '17', NULL, 12, '2016-10-17 09:33:11', '2016-10-17 09:33:11'),
(519, '2358385fa1bf25a98bd2060682849c80', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '17', NULL, 3, '2016-10-17 09:35:48', '2016-10-17 09:35:48'),
(520, '55ac99ab3e6ea4f183122e49be110dd8', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '17', NULL, 3, '2016-10-17 09:35:48', '2016-10-17 09:35:48'),
(521, '0827eed31f39e7c002784da424557d1b', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '16', NULL, 15, '2016-10-17 10:33:44', '2016-10-17 10:33:44'),
(522, '86b4f5b7fa33ef435e7e8e659950929a', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '16', NULL, 15, '2016-10-17 10:33:44', '2016-10-17 10:33:44'),
(523, '3a5d5f60b0c93ec9913cefd893840c3a', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'avl', 'news_detail', '16', NULL, 4, '2016-10-17 10:34:09', '2016-10-17 10:34:09'),
(524, '5234684fea9af1b1195942893b34f104', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '16', NULL, 4, '2016-10-17 10:34:09', '2016-10-17 10:34:09'),
(525, '3385811fc61fee654733ee383096c541', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '16', NULL, 4, '2016-10-17 10:34:09', '2016-10-17 10:34:09'),
(526, '13f8c496548d15e1ed4992cf7b76433e', 'admasyst.ga@gmail.com', 'Adminasyst', 'news_public', 'form_update', '18', NULL, 15, '2016-10-17 10:57:35', '2016-10-17 10:57:35'),
(527, 'c9c657350967c472ac08811bdb6b1e8d', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '18', NULL, 14, '2016-10-17 10:58:42', '2016-10-17 10:58:42'),
(528, '7c8ea17881069c9ffd3fe67029018cc6', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '18', NULL, 14, '2016-10-17 10:58:43', '2016-10-17 10:58:43'),
(529, '28e3fe61d43f278ac55416bd45e79304', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '18', NULL, 4, '2016-10-17 10:59:56', '2016-10-17 10:59:56'),
(530, '334c9b4281e8acdc67156cced4c878f1', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '18', NULL, 4, '2016-10-17 10:59:56', '2016-10-17 10:59:56'),
(531, '2a01185297ca446c79b2d18f1e696802', 'admasyst.ga@gmail.com', 'Adminasyst', 'open_sourcing', 'form_update', '19', NULL, 12, '2016-10-17 11:05:34', '2016-10-17 11:05:34'),
(532, '4f73035d289cbbf9c3a37e6b27d60325', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '17', NULL, 12, '2016-10-17 11:12:23', '2016-10-17 11:12:23'),
(533, '5eb749ff7e03daf887b6f5284da3ebd5', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '17', NULL, 12, '2016-10-17 11:14:33', '2016-10-17 11:14:33'),
(534, '9a2f8665929ee98c4e08c44009117f30', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '17', NULL, 12, '2016-10-17 11:14:34', '2016-10-17 11:14:34'),
(535, '5fce9669717eb29b950f1ece976277c9', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '17', NULL, 12, '2016-10-17 11:24:20', '2016-10-17 11:24:20'),
(536, '131481ff5f79aaa836ab0fd506ca5978', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '17', NULL, 12, '2016-10-17 11:24:20', '2016-10-17 11:24:20'),
(537, '1671f2d6d8df5188c1901531181c1bd5', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '17', NULL, 3, '2016-10-17 11:24:55', '2016-10-17 11:24:55'),
(538, 'e6e5e04d40b29dcac9efc08237f88969', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '17', NULL, 3, '2016-10-17 11:24:55', '2016-10-17 11:24:55'),
(539, '5563e9199afe1b3386807dd1ab1a66af', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '17', NULL, 12, '2016-10-17 11:27:35', '2016-10-17 11:27:35'),
(540, 'c5262d4725954994badc7dbd60f46821', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '17', NULL, 12, '2016-10-17 11:27:35', '2016-10-17 11:27:35'),
(541, '791f4a481b2460296b21ada56c8e8488', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '17', NULL, 12, '2016-10-17 11:28:49', '2016-10-17 11:28:49'),
(542, 'd05419d7752682add60dd2cc9928a382', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '17', NULL, 12, '2016-10-17 11:28:49', '2016-10-17 11:28:49'),
(543, '1c8fd905583e10f0840c8056729ac037', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '17', NULL, 3, '2016-10-17 11:28:56', '2016-10-17 11:28:56'),
(544, 'b02296f7cae0b58e12ec10a44a555f19', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '17', NULL, 3, '2016-10-17 11:28:56', '2016-10-17 11:28:56'),
(545, '64908c5ca5a69078ebe42cda14fc78f4', 'admasyst.ga@gmail.com', 'Adminasyst', 'open_bidding', 'form_update', '20', NULL, 12, '2016-10-17 11:32:50', '2016-10-17 11:32:50'),
(546, '312cc90669a53f1bfc48acfccd57c7bb', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '21', NULL, 12, '2016-10-17 11:55:12', '2016-10-17 11:55:12'),
(547, '3fe891b7c4f14ea7935bd5ccab0ef16e', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '21', NULL, 12, '2016-10-17 11:55:12', '2016-10-17 11:55:12'),
(548, '9d1b15b5a587a9e8c62dd485aacce003', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '21', NULL, 3, '2016-10-17 11:55:27', '2016-10-17 11:55:27'),
(549, '4abcdc897d5a9b7c1116137f58b7fc8c', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '21', NULL, 3, '2016-10-17 11:55:27', '2016-10-17 11:55:27'),
(550, 'ed4a8e645c8d05a8d16ccedbb190ac70', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '21', NULL, 12, '2016-10-17 11:55:59', '2016-10-17 11:55:59'),
(551, '286fe5c4573c425533bc222ba625d43e', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '21', NULL, 12, '2016-10-17 11:55:59', '2016-10-17 11:55:59'),
(552, 'a92bfb3af5ff9f8d2045e3d690824ffe', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '21', NULL, 12, '2016-10-17 11:56:25', '2016-10-17 11:56:25'),
(553, 'd42d91044a6d62da213c026c2f301401', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '21', NULL, 12, '2016-10-17 11:56:25', '2016-10-17 11:56:25'),
(554, '503028b689711bc97386955b5fb3e499', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '21', NULL, 3, '2016-10-17 11:56:32', '2016-10-17 11:56:32'),
(555, 'e180a8a1a4e0946f4775676c47804c71', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '21', NULL, 3, '2016-10-17 11:56:32', '2016-10-17 11:56:32'),
(556, '58522186ef8ca1912598bd3dd968d637', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '18', NULL, 15, '2016-10-17 13:19:00', '2016-10-17 13:19:00'),
(557, '26e4711b216ce6555cb52584e0e5cff1', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '18', NULL, 15, '2016-10-17 13:19:00', '2016-10-17 13:19:00'),
(558, '03761a667a723a22fbae45e6787bc887', 'admasyst.ga@gmail.com', 'Adminasyst', 'news_public', 'form_update', '18', NULL, 15, '2016-10-17 13:32:10', '2016-10-17 13:32:10'),
(559, 'e1e9dbd426809d68ff2c8f47af535509', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '18', NULL, 14, '2016-10-17 13:33:00', '2016-10-17 13:33:00'),
(560, '9a599bb48fee1471ec9e229a2eb63f47', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '18', NULL, 14, '2016-10-17 13:33:00', '2016-10-17 13:33:00'),
(561, '45443e8946eb3e6c1b6e1fb90dcef7d1', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '18', NULL, 4, '2016-10-17 13:33:59', '2016-10-17 13:33:59'),
(562, 'c70f1469d0d381a6b3db7c201b919dd8', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '18', NULL, 4, '2016-10-17 13:33:59', '2016-10-17 13:33:59'),
(563, '569a84d0667909e6941f1def85d9c7f7', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '22', NULL, 15, '2016-10-17 13:45:01', '2016-10-17 13:45:01'),
(564, 'abecacfe7e2d8b3b4c48261d78c91f88', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '22', NULL, 15, '2016-10-17 13:45:01', '2016-10-17 13:45:01'),
(565, 'cc9c057751c151d2f96516d07d217e76', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '23', NULL, 15, '2016-10-17 13:46:16', '2016-10-17 13:46:16'),
(566, 'dabe23c11e4101ccdeb66bc098c30f4c', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '23', NULL, 15, '2016-10-17 13:46:16', '2016-10-17 13:46:16'),
(567, '3591c71fe0edcc36ccc583842ab6aee6', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '24', NULL, 12, '2016-10-17 13:58:28', '2016-10-17 13:58:28'),
(568, '69cacbbad683eb49e094ee3962bf4eb6', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '24', NULL, 12, '2016-10-17 13:58:29', '2016-10-17 13:58:29'),
(569, 'd5ecd671f4cd24815d1b8c703146684c', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'register', 'form_register_invitation', '24/580476d01824e/010941078883c416b46180474663d61f261804', '2016-10-20 00:00:00', 3, '2016-10-17 13:59:28', '2016-10-17 13:59:28'),
(570, '3be6cac193cc2d889d5a9f3d3a7ee776', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '24', NULL, 3, '2016-10-17 13:59:28', '2016-10-17 13:59:28'),
(571, '140f4a46a9b08adcef7ba2d10aa10db1', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '24', NULL, 3, '2016-10-17 13:59:28', '2016-10-17 13:59:28'),
(572, '394fd46447e76d053d7f481bc56e9b90', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '25', NULL, 15, '2016-10-17 14:02:22', '2016-10-17 14:02:22'),
(573, '92846dbbea3d62c8498d70ec99064933', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '25', NULL, 15, '2016-10-17 14:02:22', '2016-10-17 14:02:22'),
(574, '98a5c15508d3fbe6df04c4a1de403052', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '16', NULL, 15, '2016-10-17 14:34:10', '2016-10-17 14:34:10'),
(575, 'ae9d6f057e81ca7ec75de08d6588119e', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '16', NULL, 15, '2016-10-17 14:34:10', '2016-10-17 14:34:10'),
(576, 'a5d747ae0eb170fbe5cfda42c6f41bac', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '16', NULL, 15, '2016-10-17 14:34:33', '2016-10-17 14:34:33'),
(577, 'e553c7505a91a48769d0b7423257b7f3', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '16', NULL, 15, '2016-10-17 14:34:34', '2016-10-17 14:34:34'),
(578, '3bff77cb989ac18313c53b12eac2bb30', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'avl', 'news_detail', '16', NULL, 4, '2016-10-17 14:34:57', '2016-10-17 14:34:57'),
(579, '9b34eeedecc7883d08391a795dc10fff', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '16', NULL, 4, '2016-10-17 14:34:57', '2016-10-17 14:34:57'),
(580, '3c93e17e2db80a4d1792880b07b48a99', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '16', NULL, 4, '2016-10-17 14:34:57', '2016-10-17 14:34:57'),
(581, '2b9d4e75ee84951a00e533c22b93b7ab', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '16', NULL, 15, '2016-10-17 14:40:17', '2016-10-17 14:40:17'),
(582, '9478e46a6ec73c71c91745d3f57f2148', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '16', NULL, 15, '2016-10-17 14:40:17', '2016-10-17 14:40:17'),
(583, '215bb5bb171eb5b5ada9e97061b7c847', 'muhammad.hakim@asyst.co.id', 'pt.ae70', 'avl', 'news_detail', '16', NULL, 4, '2016-10-17 14:40:45', '2016-10-17 14:40:45'),
(584, '33388ebd77164a313ae95d684a8e5059', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'announce', 'preview', '16', NULL, 4, '2016-10-17 14:40:45', '2016-10-17 14:40:45'),
(585, '0625a6f7aa69268fe3732cfb58e9d155', 'zulandraririn@gmail.com', 'RirinZulandra', 'announce', 'preview', '16', NULL, 4, '2016-10-17 14:40:45', '2016-10-17 14:40:45'),
(586, '73e7693367309aa4da9f5459e56f9594', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '26', NULL, 12, '2016-10-17 14:44:33', '2016-10-17 14:44:33'),
(587, '121db6ec2de482b4cfcbb651ec0b3b3d', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '26', NULL, 12, '2016-10-17 14:44:33', '2016-10-17 14:44:33'),
(588, '199cf99a558ab5572e3547d199565277', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '27', NULL, 12, '2016-10-17 14:57:12', '2016-10-17 14:57:12'),
(589, '82999db5f04cae02848da219d54d56cd', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '27', NULL, 12, '2016-10-17 14:57:12', '2016-10-17 14:57:12'),
(590, 'c061f67c974e04dd9b8c1370a2aa691b', 'admasyst.ga@gmail.com', 'Adminasyst', 'open_bidding', 'form_update', '28', NULL, 12, '2016-10-17 15:34:59', '2016-10-17 15:34:59'),
(591, '82e48a2d6dec6e023453e6e3f59cc4b0', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '29', NULL, 12, '2016-10-17 15:41:24', '2016-10-17 15:41:24'),
(592, '538ccad1d184382130ff3824a04b6e75', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '29', NULL, 12, '2016-10-17 15:41:24', '2016-10-17 15:41:24'),
(593, 'd33b50799f0566f63e11662e51fcae86', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '29', NULL, 3, '2016-10-17 15:42:05', '2016-10-17 15:42:05'),
(594, '701f7ec1532c5dac30ba7cfd4ecd8fe6', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '29', NULL, 3, '2016-10-17 15:42:05', '2016-10-17 15:42:05'),
(595, '4f149d577f036d1f68a6f2916f1940a9', 'admasyst.ga@gmail.com', 'Adminasyst', 'news_public', 'form_update', '30', NULL, 15, '2016-10-18 08:45:38', '2016-10-18 08:45:38'),
(596, '458f2c64887165c6e7adb5b8fa3c50ac', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '30', NULL, 14, '2016-10-18 08:49:02', '2016-10-18 08:49:02'),
(597, '8cd52017239738bcfc334b74b08e9c64', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '30', NULL, 14, '2016-10-18 08:49:02', '2016-10-18 08:49:02'),
(598, '446549f97eeb2b25fc4ad62f47d2e866', 'admasyst.ga@gmail.com', 'Adminasyst', 'open_sourcing', 'form_update', '31', NULL, 12, '2016-10-18 08:55:18', '2016-10-18 08:55:18'),
(599, '2182fed1f924bb0ac72a0d64b9e691ef', 'admasyst.ga@gmail.com', 'Adminasyst', 'open_bidding', 'form_update', '32', NULL, 12, '2016-10-18 08:56:52', '2016-10-18 08:56:52'),
(600, '77dd234f97eb66da3e922463ea7c519d', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '33', NULL, 12, '2016-10-18 09:35:53', '2016-10-18 09:35:53'),
(601, '47dd1e4fd41d2f78b8b8f719ef12034c', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '33', NULL, 12, '2016-10-18 09:35:53', '2016-10-18 09:35:53'),
(602, '2ef3e8721710548189483a34e2a3bac9', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '33', NULL, 3, '2016-10-18 09:36:43', '2016-10-18 09:36:43'),
(603, '93d0cbdb4b0a66f026b63aaf2423ef9f', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '33', NULL, 3, '2016-10-18 09:36:43', '2016-10-18 09:36:43'),
(604, 'd03f9821e5afd25aee511d2b3a251e2c', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '34', NULL, 12, '2016-10-18 09:44:43', '2016-10-18 09:44:43'),
(605, 'fe6bc539571dfac274647389f5975e3f', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '34', NULL, 12, '2016-10-18 09:44:43', '2016-10-18 09:44:43'),
(606, 'd0ffc0bf4f3281ad12cbdf1d9b4ff410', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '34', NULL, 3, '2016-10-18 09:45:04', '2016-10-18 09:45:04'),
(607, '5bb375d546f40b564c95e87a90d2c39d', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '34', NULL, 3, '2016-10-18 09:45:04', '2016-10-18 09:45:04'),
(608, '8d6ccd7dff44bfde3c22b1a4301d49ae', 'admasyst.ga@gmail.com', 'Adminasyst', 'news_public', 'form_update', '35', NULL, 15, '2016-10-18 10:42:53', '2016-10-18 10:42:53'),
(609, 'b7bddb0baae6f2548feb174127c5d33f', 'admasyst.ga@gmail.com', 'Adminasyst', 'news_public', 'form_update', '35', NULL, 14, '2016-10-18 10:43:30', '2016-10-18 10:43:30'),
(610, '163d53a03c4e14a5d5c9f72bb604f32a', 'smibk.ga@gmail.com', 'smibk', 'announce', 'preview', '35', NULL, 14, '2016-10-18 10:43:45', '2016-10-18 10:43:45'),
(611, '168f4d1090d00594f865bbabd3e0d845', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '36', NULL, 12, '2016-10-18 14:18:00', '2016-10-18 14:18:00'),
(612, '02c9b32d393d5bbaccc7d38499c9da7d', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '36', NULL, 12, '2016-10-18 14:18:00', '2016-10-18 14:18:00'),
(613, 'b82d7bd92503acccbb8ed5b6057e8a72', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '36', NULL, 3, '2016-10-18 14:18:33', '2016-10-18 14:18:33'),
(614, '68bcbf179468aab624c1fd2ed8482d26', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '36', NULL, 3, '2016-10-18 14:18:33', '2016-10-18 14:18:33');
INSERT INTO `session_email` (`id`, `token`, `email`, `username`, `controller`, `method`, `parameter`, `expired_date`, `created_id`, `created_date`, `last_updated`) VALUES
(615, 'dfb74f9bc0437d4550d714463897a237', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '36', NULL, 12, '2016-10-18 14:23:54', '2016-10-18 14:23:54'),
(616, 'bed14e815df3b8dacb3265f1f16e3623', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '36', NULL, 12, '2016-10-18 14:23:54', '2016-10-18 14:23:54'),
(617, '1cb5b64f91c665ad78f8d254328fcb1c', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '37', NULL, 12, '2016-10-18 14:43:14', '2016-10-18 14:43:14'),
(618, '8e12d78bee0eda0c87c69ff4155ed5c3', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '37', NULL, 12, '2016-10-18 14:43:14', '2016-10-18 14:43:14'),
(619, 'aaf1a44a3e268fbb65ff35fed961140f', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '37', NULL, 3, '2016-10-18 14:43:40', '2016-10-18 14:43:40'),
(620, '1daacfe7b6d6c7b00019c088b22eb1c3', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '37', NULL, 3, '2016-10-18 14:43:41', '2016-10-18 14:43:41'),
(621, '998c2e6b2ed180471bf9ad94e29a0814', 'smibs.ga@gmail.com', 'Mitra', 'blacklist', '', '', NULL, 2, '2016-10-18 15:44:36', '2016-10-18 15:44:36'),
(622, 'f775da12dc50daa5df44f684a2e44a3b', 'smibk.ga@gmail.com', 'Ridho', 'blacklist', '', '', NULL, 3, '2016-10-18 15:46:08', '2016-10-18 15:46:08'),
(623, '6c46041e9ca67de702b2f94ae1e23a76', 'vpib.ga@gmail.com', 'Ikhsan', 'blacklist', '', '', NULL, 4, '2016-10-18 15:59:46', '2016-10-18 15:59:46'),
(624, '560b4734497efae51908b598e1749a1b', NULL, 'Test Edit', 'blacklist', '', '', NULL, 4, '2016-10-18 15:59:46', '2016-10-18 15:59:46'),
(625, 'd67d188580a7a5ccfeb4a4cdc67569a6', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '38', NULL, 12, '2016-10-18 21:38:45', '2016-10-18 21:38:45'),
(626, 'fa41ca63d8bc44f77159323e4d1bd428', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '38', NULL, 12, '2016-10-18 21:38:45', '2016-10-18 21:38:45'),
(627, '1fb98eb20aa45f6c8046c851b3f51831', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '38', NULL, 12, '2016-10-18 21:38:45', '2016-10-18 21:38:45'),
(628, '975acd666f73dbfe5aba2bb1e46e6a52', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '39', NULL, 12, '2016-10-18 22:40:55', '2016-10-18 22:40:55'),
(629, '9bd4778924fa6a7b1c7307eca356797a', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '39', NULL, 12, '2016-10-18 22:40:55', '2016-10-18 22:40:55'),
(630, '7659edc8ebb1a939e5e6eeaa874e7e91', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '39', NULL, 12, '2016-10-18 22:40:55', '2016-10-18 22:40:55'),
(631, 'f8b5ad9d3511874321d05298cf419c03', 'smibs.ga@gmail.com', 'smibs', 'announce', 'preview', '39', NULL, 12, '2016-10-18 22:57:59', '2016-10-18 22:57:59'),
(632, '5ba6cbe3cf120a97ae2a584fbb2056e8', 'vpib.ga@gmail.com', 'vpib', 'announce', 'preview', '39', NULL, 12, '2016-10-18 22:57:59', '2016-10-18 22:57:59'),
(633, '54c8f397556232513a4a507841c905f1', 'wabimantoro@gmail.com', 'wildantest', 'announce', 'preview', '39', NULL, 12, '2016-10-18 22:57:59', '2016-10-18 22:57:59'),
(634, '3cdfcd9e15933b95d515aaedd6069492', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '39', NULL, 3, '2016-10-18 23:08:37', '2016-10-18 23:08:37'),
(635, 'a9b93547cc4c0978c90d9271153823bd', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '39', NULL, 3, '2016-10-18 23:08:37', '2016-10-18 23:08:37'),
(636, 'b7b62b0508663e60180798f74835670a', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '39', NULL, 3, '2016-10-18 23:12:21', '2016-10-18 23:12:21'),
(637, 'bb33a1e2742f7d2fa8b4a745f1d640f5', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '39', NULL, 3, '2016-10-18 23:12:21', '2016-10-18 23:12:21'),
(638, '139f7a921d41800dcc895628dfd6aec2', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '39', NULL, 3, '2016-10-18 23:13:48', '2016-10-18 23:13:48'),
(639, '843c8bee30754323dc1c334ffa61e77a', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '39', NULL, 3, '2016-10-18 23:13:49', '2016-10-18 23:13:49'),
(640, '8c34002bec1a5c13d4b70cc015c85f47', 'srmadusari@naver.com', 'pt.re2', 'register', 'form_register_invitation', '39/5806506336d46/fa50e0f58de481e64d8de94e07e48e5258de94', '2016-10-21 00:00:00', 3, '2016-10-18 23:40:03', '2016-10-18 23:40:03'),
(641, '2121f43394b8c56d23d992b6d3086414', 'siti.madusari@asyst.co.id', 'pt.re8', 'register', 'form_register_invitation', '39/5806506336d46/fa50e0f58de481e64d8de94e07e48e5258de94', '2016-10-21 00:00:00', 3, '2016-10-18 23:40:03', '2016-10-18 23:40:03'),
(642, '5fb5bb0d9659fc6206c25fb9ca3da617', 'wisnu.arimurti@asyst.co.id', NULL, 'register', 'form_register_invitation', '39/5806506336d46/fa50e0f58de481e64d8de94e07e48e5258de94', '2016-10-21 00:00:00', 3, '2016-10-18 23:40:03', '2016-10-18 23:40:03'),
(643, '2697e6eed77212e4b0780939059c4f23', 'WISNU.ARIMURTI@ASYST.CO.ID', 'pted54', 'register', 'form_register_invitation', '39/5806506336d46/fa50e0f58de481e64d8de94e07e48e5258de94', '2016-10-21 00:00:00', 3, '2016-10-18 23:40:03', '2016-10-18 23:40:03'),
(644, 'cafe735ecb3a49158dc81ab19c1b8f57', 'StaffIBS.Ga@gmail.com', 'Staff_IBS', 'announce', 'preview', '39', NULL, 3, '2016-10-18 23:40:03', '2016-10-18 23:40:03'),
(645, '085148031f09e7c7a7e57fc80f82170f', '2StaffIBK.Ga@gmail.com', 'Madu', 'announce', 'preview', '39', NULL, 3, '2016-10-18 23:40:03', '2016-10-18 23:40:03'),
(646, 'cb6398bec2e42ab47bfd1284b9693ff2', 'smibs.ga@gmail.com', 'Mitra', 'blacklist', '', '', NULL, 2, '2016-10-19 09:01:22', '2016-10-19 09:01:22'),
(647, 'c755f7a61ca53d509dc1e29028640d1f', 'wabimantoro@gmail.com', 'wildanab', 'blacklist', '', '', NULL, 2, '2016-10-19 09:01:22', '2016-10-19 09:01:22'),
(648, '3995dbd3642f6c96f767ab3d9dbdcae7', 'smibk.ga@gmail.com', 'Ridho', 'blacklist', '', '', NULL, 3, '2016-10-19 09:02:02', '2016-10-19 09:02:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `session_notification`
--

CREATE TABLE IF NOT EXISTS `session_notification` (
`id` smallint(5) unsigned NOT NULL,
  `token` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `request` varchar(100) DEFAULT NULL,
  `parameter` varchar(100) DEFAULT NULL,
  `expired_date` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=106 ;

--
-- Dumping data untuk tabel `session_notification`
--

INSERT INTO `session_notification` (`id`, `token`, `email`, `username`, `request`, `parameter`, `expired_date`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 'd83f113734f7567babf5b7e428d92578', 'ucihamadera@gmail.com', 'smibs', 'redlist', '2', '2016-10-20 08:32:46', '1', 2, '2016-10-17 08:32:46', '2016-10-17 08:32:46'),
(2, '82fddb85792f447b0a34b81e7858da55', 'miminsrc@gmail.com', 'AdminSourcing', 'recommend', '101', '2016-10-20 09:18:11', '1', 3, '2016-10-17 09:18:11', '2016-10-17 09:18:11'),
(3, 'c54704cacbeb4a0dfd6a23eac1d1d99a', 'smibk.ga@gmail.com', 'smibk', 'recommend', '101', '2016-10-20 09:18:11', '1', 3, '2016-10-17 09:18:11', '2016-10-17 09:18:11'),
(4, '6777b74b633c6ceafde604f62b113d24', 'miminsrc@gmail.com', 'AdminSourcing', 'recommend', '101', '2016-10-20 09:19:38', '1', 4, '2016-10-17 09:19:38', '2016-10-17 09:19:38'),
(5, 'bf1e28609add16832d3ba8097c1733ba', 'vpib.ga@gmail.com', 'vpib', 'recommend', '101', '2016-10-20 09:19:43', '1', 4, '2016-10-17 09:19:43', '2016-10-17 09:19:43'),
(6, 'cbf7e940384578a9a59b097569163509', 'smibk.ga@gmail.com', 'smibk', 'inactive_vend', '', '2016-10-20 09:19:43', '1', 3, '2016-10-17 09:19:43', '2016-10-17 09:19:43'),
(7, '68f75a23ddd98bdf54eb5a6fd4cbbaab', 'miminsrc@gmail.com', 'AdminSourcing', 'recommend', '101', '2016-10-20 09:20:17', '1', 6, '2016-10-17 09:20:17', '2016-10-17 09:20:17'),
(8, '1287254c704a8e14c298a7db2c2653ab', 'smibk.ga@gmail.com', 'smibk', 'inactive_vend', '', '2016-10-20 09:22:15', '1', 3, '2016-10-17 09:22:15', '2016-10-17 09:22:15'),
(9, '20f1fb1b2ae5de4f3f74870093e8b781', 'miminsrc@gmail.com', 'AdminSourcing', 'bank_account', '101', '2016-10-20 09:23:35', '1', 0, '2016-10-17 09:23:35', '2016-10-17 09:23:35'),
(10, '3fa67f08a47fee1f5b576722b45ea1b4', 'asdasd@gmail.com', 'wildantest', 'inactive_vend', '', '2016-10-20 09:24:02', '1', 3, '2016-10-17 09:24:02', '2016-10-17 09:24:02'),
(11, 'b2d3205cea4b7db084c02a7d76f43e8f', 'uchihamadera@gmail.com', 'smibk', 'inactive_vend', '', '2016-10-20 09:25:39', '1', 3, '2016-10-17 09:25:39', '2016-10-17 09:25:39'),
(12, '623671a766fab9104bcb4b0cb3e43630', 'asdasd@gmail.com', 'wildantest', 'inactive_vend', '', '2016-10-20 09:25:39', '1', 3, '2016-10-17 09:25:39', '2016-10-17 09:25:39'),
(13, '5845effd6d59b3d286d6f91a3ebc45e4', 'watsap1.ga@gmail.com', 'watsap1', 'account_number', '101', '2016-10-20 09:25:46', '1', 2, '2016-10-17 09:25:46', '2016-10-17 09:25:46'),
(14, '84ccf56648e2b954316d35f27e477b2b', 'watsap2.ga@gmail.com', 'watsap2', 'account_number', '101', '2016-10-20 09:26:18', '1', 7, '2016-10-17 09:26:18', '2016-10-17 09:26:18'),
(15, '6ff022b83eac61cb49fe5517088c05f0', 'uchihamadera@gmail.com', 'smibk', 'inactive_vend', '', '2016-10-20 09:28:00', '1', 3, '2016-10-17 09:28:00', '2016-10-17 09:28:00'),
(16, '30feeadaa4d56c7c9a03e4b721d5b504', 'asdasd@gmail.com', 'wildantest', 'inactive_vend', '', '2016-10-20 09:28:00', '1', 3, '2016-10-17 09:28:00', '2016-10-17 09:28:00'),
(17, '552a76f0dd859f1ccd1c39ae99b8a848', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'account_number', '101', '2016-10-20 09:28:03', '1', 8, '2016-10-17 09:28:03', '2016-10-17 09:28:03'),
(18, 'bdf2fb9feb3106229b7a43c12e3e3cdd', 'zulandraririn@gmail.com', 'RirinZulandra', 'account_number', '101', '2016-10-20 09:28:03', '1', 8, '2016-10-17 09:28:03', '2016-10-17 09:28:03'),
(19, '656e0d50878b4f0fe5301ce591534e9d', 'uchihamadera@gmail.com', 'smibk', 'inactive_vend', '', '2016-10-20 09:29:49', '1', 3, '2016-10-17 09:29:49', '2016-10-17 09:29:49'),
(20, 'd7eaf5675b67499c4b749b1227e8e886', 'asdasd@gmail.com', 'wildantest', 'inactive_vend', '', '2016-10-20 09:29:49', '1', 3, '2016-10-17 09:29:49', '2016-10-17 09:29:49'),
(21, '2867b339d67472e312e9858c80a15e49', 'uchihamadera@gmail.com', 'smibk', 'inactive_vend', '', '2016-10-20 09:32:38', '1', 3, '2016-10-17 09:32:38', '2016-10-17 09:32:38'),
(22, 'e373abf5bf149856a0d948656aada1ba', 'asdasd@gmail.com', 'wildantest', 'inactive_vend', '', '2016-10-20 09:32:38', '1', 3, '2016-10-17 09:32:38', '2016-10-17 09:32:38'),
(23, 'e6eb3970475b008cf1a9364976f1f51e', 'uchihamadera@gmail.com', 'smibk', 'inactive_vend', '7', '2016-10-20 09:37:05', '1', 3, '2016-10-17 09:37:05', '2016-10-17 09:37:05'),
(24, '0b2558e7ba97ce047b2564897103d88e', 'asdasd@gmail.com', 'wildantest', 'inactive_vend', '7', '2016-10-20 09:37:05', '1', 3, '2016-10-17 09:37:05', '2016-10-17 09:37:05'),
(25, '37a5ac0c14c972dc55246f49a86e74f9', 'uchihamadera@gmail.com', 'smibk', 'inactive_vend', '7', '2016-10-20 09:38:05', '1', 3, '2016-10-17 09:38:05', '2016-10-17 09:38:05'),
(26, '587fc975a8ddbb5d2fcdd2067c529d7b', 'asdasd@gmail.com', 'wildantest', 'inactive_vend', '7', '2016-10-20 09:38:05', '1', 3, '2016-10-17 09:38:05', '2016-10-17 09:38:05'),
(27, '1d6506ab438a6140882f85c4668d3905', 'uchihamadera@gmail.com', 'smibk', 'inactive_vend', '7', '2016-10-20 09:40:14', '1', 3, '2016-10-17 09:40:14', '2016-10-17 09:40:14'),
(28, 'e00499c2708440c82895bbdaece017e8', 'asdasd@gmail.com', 'wildantest', 'inactive_vend', '7', '2016-10-20 09:40:14', '1', 3, '2016-10-17 09:40:14', '2016-10-17 09:40:14'),
(29, 'c38315ec2a29315fc469dde1f8d7101a', 'uchihamadera@gmail.com', 'smibk', 'inactive_vend', '7', '2016-10-20 09:41:38', '1', 3, '2016-10-17 09:41:38', '2016-10-17 09:41:38'),
(30, '0fc214378fddc35707982160297164a2', 'uchihamadera@gmail.com', 'smibk', 'inactive_vend', '7', '2016-10-20 09:42:15', '1', 3, '2016-10-17 09:42:15', '2016-10-17 09:42:15'),
(31, '6864d2c5ec74ebedb8693f568856cc32', 'smibs.ga@gmail.com', 'smibs', 'redlist', '2', '2016-10-20 09:43:18', '0', 2, '2016-10-17 09:43:18', '0000-00-00 00:00:00'),
(32, '5413b4b7e6b84bafc5cc99c395b672cc', 'asdasd@gmail.com', 'wildantest', 'redlist', '2', '2016-10-20 09:43:18', '1', 2, '2016-10-17 09:43:18', '2016-10-17 09:43:18'),
(33, 'b16dca315dd7ad3da17a1da8cd2fbcb2', 'smibk.ga@gmail.com', 'smibk', 'inactive_vend', '7', '2016-10-20 09:53:05', '1', 3, '2016-10-17 09:53:05', '2016-10-17 09:53:05'),
(34, 'c50c5acc59d7fb3744d783b1fa9745b6', 'wabimantoro@gmail.com', 'wildantest', 'inactive_vend', '7', '2016-10-20 09:53:05', '0', 3, '2016-10-17 09:53:05', '0000-00-00 00:00:00'),
(35, '67133d99a1eb2bf215b851528ebbdc6b', 'vpib.ga@gmail.com', 'vpib', 'inactive_vend', '7', '2016-10-20 10:01:52', '1', 31, '2016-10-17 10:01:52', '2016-10-17 10:01:52'),
(36, '9d0856dd4d3eee2f2fa1033c0272965d', 'ucihamadera@gmail.com', 'AdminSourcing', 'redlist', '2', '2016-10-20 10:07:09', '0', 3, '2016-10-17 10:07:09', '0000-00-00 00:00:00'),
(37, '457de5823c1cba8a859065602980852a', 'vendor.regis@gmail.com', 'PT Vendor Indonesia', 'redlist_detail', '23', '2016-10-20 10:09:09', '1', 3, '2016-10-17 10:09:09', '2016-10-17 10:09:09'),
(38, '2b8a4a9bb1920ab0059fd7a1f72af98c', 'ucihamadera@gmail.com', 'AdminSourcing', 'redlist', '2', '2016-10-20 10:09:09', '0', 3, '2016-10-17 10:09:09', '0000-00-00 00:00:00'),
(39, 'a7b36b52f49c766fb6bcb0f61e2f7da7', 'vendor.regis@gmail.com', 'PT Vendor Indonesia', 'redlist_detail', '23', '2016-10-20 10:09:41', '1', 3, '2016-10-17 10:09:41', '2016-10-17 10:09:41'),
(40, '81fe463b8f7947aad1405eca2bc69e36', 'ucihamadera@gmail.com', 'AdminSourcing', 'redlist', '2', '2016-10-20 10:09:41', '1', 3, '2016-10-17 10:09:41', '2016-10-17 10:09:41'),
(41, '26665b07c92a4b09d632b2a75035b9a7', 'smibk.ga@gmail.com', 'smibk', 'inactive_vend', '6', '2016-10-21 09:26:17', '1', 3, '2016-10-18 09:26:17', '2016-10-18 09:26:17'),
(42, 'ec27b8c8adc3b45834aca4ade4f4a552', 'wabimantoro@gmail.com', 'wildantest', 'inactive_vend', '6', '2016-10-21 09:26:18', '0', 3, '2016-10-18 09:26:18', '0000-00-00 00:00:00'),
(43, '8d98c77b069a3d10cb76002f11694079', 'miminsrc@gmail.com', 'AdminSourcing', 'inactive_vend', '7', '2016-10-21 10:02:18', '1', 6, '2016-10-18 10:02:18', '2016-10-18 10:02:18'),
(44, '84fdab8f264fec2b96a270ab711bbd9f', 'wabimantoro@gmail.com', 'wildantest', 'inactive_vend', '7', '2016-10-21 10:02:18', '1', 6, '2016-10-18 10:02:18', '2016-10-18 10:02:18'),
(45, '06cdc821ef48bd32d49c82364d376328', 'miminsrc@gmail.com', 'AdminSourcing', 'inactive_vend', '3', '2016-10-21 10:06:03', '1', 6, '2016-10-18 10:06:03', '2016-10-18 10:06:03'),
(46, '319aef5280744fd8529f7cc7f6307fc0', 'wabimantoro@gmail.com', 'wildantest', 'inactive_vend', '3', '2016-10-21 10:06:03', '0', 6, '2016-10-18 10:06:03', '0000-00-00 00:00:00'),
(47, 'acee0c772729ea94f7218b848c7edf81', 'miminsrc@gmail.com', 'AdminSourcing', 'inactive_vend', '11', '2016-10-21 14:49:41', '1', 3, '2016-10-18 14:49:41', '2016-10-18 14:49:41'),
(48, '817bf7f27b6ffc653269eca74b3b95a6', 'wabimantoro@gmail.com', 'wildantest', 'inactive_vend', '11', '2016-10-21 14:49:41', '0', 3, '2016-10-18 14:49:41', '0000-00-00 00:00:00'),
(49, '3da776637a8a0c7fe5b66df4769083e3', 'smibk.ga@gmail.com', 'smibk', 'inactive_vend', '12', '2016-10-21 14:55:30', '1', 3, '2016-10-18 14:55:30', '2016-10-18 14:55:30'),
(50, '956cc0864babe8bbfebb27f197a41346', 'smibs.ga@gmail.com', 'smibs', 'redlist', '73', '2016-10-21 15:07:38', '1', 2, '2016-10-18 15:07:38', '2016-10-18 15:07:38'),
(51, 'af72675e108014af566f6c446214fc81', 'smibs.ga@gmail.com', 'smibs', 'redlist', '73', '2016-10-21 15:34:49', '1', 2, '2016-10-18 15:34:49', '2016-10-18 15:34:49'),
(52, '8053530c30e35b93b729df98bffc020f', 'smibs.ga@gmail.com', 'smibs', 'redlist', '73', '2016-10-21 15:36:29', '1', 2, '2016-10-18 15:36:29', '2016-10-18 15:36:29'),
(53, 'a1c4f7aad495cb972c93585d783bf2fa', 'wisnu.arimurti@asyst.co.id', 'pted54', 'redlist_detail', '25', '2016-10-21 15:43:39', '1', 3, '2016-10-18 15:43:39', '2016-10-18 15:43:39'),
(54, '6a5c958ced8fe25001c49ce40b702821', 'miminsrc@gmail.com', 'AdminSourcing', 'redlist', '73', '2016-10-21 15:43:39', '1', 3, '2016-10-18 15:43:39', '2016-10-18 15:43:39'),
(55, 'de57a2eb2b3f5baf132a532f582bdecd', 'wabimantoro@gmail.com', 'wildantest', 'redlist', '73', '2016-10-21 15:43:39', '1', 3, '2016-10-18 15:43:39', '2016-10-18 15:43:39'),
(56, 'fb8a06a04e908975e74f6ed38a0f649e', 'smibk.ga@gmail.com', 'smibk', 'active_vend', '14', '2016-10-21 16:06:32', '1', 3, '2016-10-18 16:06:32', '2016-10-18 16:06:32'),
(57, '4085c2017b316bc2cc213e4cb0859bb5', 'vpib.ga@gmail.com', 'vpib', 'active_vend', '14', '2016-10-21 16:08:58', '1', 4, '2016-10-18 16:08:58', '2016-10-18 16:08:58'),
(58, '42d32c887d360a2f0aa3d02c733859e6', 'miminsrc@gmail.com', 'AdminSourcing', 'del_vend_app', '101', '2016-10-21 16:09:39', '1', 3, '2016-10-18 16:09:39', '2016-10-18 16:09:39'),
(59, '181d11440141c482ac3243a401b08c42', 'wabimantoro@gmail.com', 'wildantest', 'del_vend_app', '101', '2016-10-21 16:09:39', '0', 3, '2016-10-18 16:09:39', '0000-00-00 00:00:00'),
(60, '9ce9a731b1c3b3c7b7598600c1862961', 'miminsrc@gmail.com', 'AdminSourcing', 'active_vend', '14', '2016-10-21 16:10:09', '1', 6, '2016-10-18 16:10:09', '2016-10-18 16:10:09'),
(61, 'e23ca9d80b75a6b544875d17be303050', 'wabimantoro@gmail.com', 'wildantest', 'active_vend', '14', '2016-10-21 16:10:09', '1', 6, '2016-10-18 16:10:09', '2016-10-18 16:10:09'),
(62, '9c9d06cc22ab860e0964b1e81afc0382', 'miminsrc@gmail.com', 'AdminSourcing', 'del_vend_app', '17', '2016-10-21 16:16:02', '1', 3, '2016-10-18 16:16:02', '2016-10-18 16:16:02'),
(63, '012815c16b2ebad588c8c029f70e3a59', 'wabimantoro@gmail.com', 'wildantest', 'del_vend_app', '17', '2016-10-21 16:16:02', '1', 3, '2016-10-18 16:16:02', '2016-10-18 16:16:02'),
(64, 'a3f74bd2a186f2bf207432a71a883273', 'ucihamadera@gmail.com', 'smibs', 'redlist', '21', '2016-10-21 17:47:39', '1', 2, '2016-10-18 17:47:39', '2016-10-18 17:47:39'),
(65, 'a787628ebe4a168146e7c368eea7cd32', 'ucihamadera@gmail.com', 'AdminSourcing', 'redlist', '21', '2016-10-21 17:49:07', '1', 3, '2016-10-18 17:49:07', '2016-10-18 17:49:07'),
(66, 'b4eff08b0892ecc4f5895120d6ffc8bb', 'wabimantoro@gmail.com', 'wildantest', 'redlist', '21', '2016-10-21 17:49:07', '1', 3, '2016-10-18 17:49:07', '2016-10-18 17:49:07'),
(67, 'e0d37ffeaa3e1048b405ae64f11710b0', 'ucihamadera@gmail.com', 'AdminSourcing', 'recommend', '113', '2016-10-21 19:33:59', '0', 3, '2016-10-18 19:33:59', '0000-00-00 00:00:00'),
(68, 'fcbb533260aa06cb605f1b9023da2468', 'smibk.ga@gmail.com', 'smibk', 'recommend', '113', '2016-10-21 19:33:59', '1', 3, '2016-10-18 19:33:59', '2016-10-18 19:33:59'),
(69, 'c558093a85a7830b6c0741c59a4077ec', 'ucihamadera@gmail.com', 'AdminSourcing', 'recommend', '112', '2016-10-21 19:34:14', '1', 3, '2016-10-18 19:34:14', '2016-10-18 19:34:14'),
(70, '4e4c042d38557fcac0425be9d1d5e898', 'smibk.ga@gmail.com', 'smibk', 'recommend', '112', '2016-10-21 19:34:14', '1', 3, '2016-10-18 19:34:14', '2016-10-18 19:34:14'),
(71, 'f28cb7499be2e8f6f8e21962310dea6b', 'ucihamadera@gmail.com', 'AdminSourcing', 'recommend', '111', '2016-10-21 19:34:23', '1', 3, '2016-10-18 19:34:23', '2016-10-18 19:34:23'),
(72, '3e43ececefbada2357a5c41876e98263', 'smibs.ga@gmail.com', 'smibs', 'del_vend_req', '13', '2016-10-21 19:34:26', '1', 2, '2016-10-18 19:34:26', '2016-10-18 19:34:26'),
(73, '7c614c1fe609b1742d2852dc62b79072', 'wabimantoro@gmail.com', 'wildantest', 'del_vend_req', '13', '2016-10-21 19:34:26', '0', 2, '2016-10-18 19:34:26', '0000-00-00 00:00:00'),
(74, 'c593d5ceaa408d793d14a6c201a77a91', 'ucihamadera@gmail.com', 'AdminSourcing', 'recommend', '110', '2016-10-21 19:34:31', '1', 3, '2016-10-18 19:34:31', '2016-10-18 19:34:31'),
(75, '8ce8693789263d1aafa10bea0f7e85cb', 'smibk.ga@gmail.com', 'smibk', 'recommend', '110', '2016-10-21 19:34:31', '1', 3, '2016-10-18 19:34:31', '2016-10-18 19:34:31'),
(76, 'fd2b751445aec7038d96b1043c5a5ae2', 'ucihamadera@gmail.com', 'AdminSourcing', 'recommend', '113', '2016-10-21 19:37:47', '1', 4, '2016-10-18 19:37:47', '2016-10-18 19:37:47'),
(77, '92c9b7d6030b3ba993f67539cd4a2535', 'vpib.ga@gmail.com', 'vpib', 'recommend', '113', '2016-10-21 19:37:47', '1', 4, '2016-10-18 19:37:47', '2016-10-18 19:37:47'),
(78, 'd36096c7203a710d74ce9d2442a84bc4', 'ucihamadera@gmail.com', 'AdminSourcing', 'recommend', '112', '2016-10-21 19:37:59', '1', 4, '2016-10-18 19:37:59', '2016-10-18 19:37:59'),
(79, 'ac00f7e5e6dadee02c801749dd891b21', 'smibk.ga@gmail.com', 'smibk', 'recommend', '112', '2016-10-21 19:37:59', '1', 4, '2016-10-18 19:37:59', '2016-10-18 19:37:59'),
(80, '2539104b957ffac30aac06b960f95eb5', 'ucihamadera@gmail.com', 'AdminSourcing', 'recommend', '110', '2016-10-21 19:38:09', '1', 4, '2016-10-18 19:38:09', '2016-10-18 19:38:09'),
(81, '13535eb08711431bd1fa7767d8f785f6', 'vpib.ga@gmail.com', 'vpib', 'recommend', '110', '2016-10-21 19:38:09', '1', 4, '2016-10-18 19:38:09', '2016-10-18 19:38:09'),
(82, 'e13d4ac4f8bd07bd5aeaffb53c130473', 'smibs.ga@gmail.com', 'smibs', 'del_vend_req', '14', '2016-10-21 19:39:26', '1', 2, '2016-10-18 19:39:26', '2016-10-18 19:39:26'),
(83, '1d34532b2f87fbb542d0dc6310aca632', 'wabimantoro@gmail.com', 'wildantest', 'del_vend_req', '14', '2016-10-21 19:39:26', '0', 2, '2016-10-18 19:39:26', '0000-00-00 00:00:00'),
(84, 'd09f7e210c05757c5dfe1339aa0e10f0', 'miminsrc@gmail.com', 'AdminSourcing', 'recommend', '113', '2016-10-21 19:41:22', '1', 6, '2016-10-18 19:41:22', '2016-10-18 19:41:22'),
(85, 'cdb73fe2449f5877710cb2b1960e4f45', 'miminsrc@gmail.com', 'AdminSourcing', 'recommend', '110', '2016-10-21 19:41:34', '1', 6, '2016-10-18 19:41:34', '2016-10-18 19:41:34'),
(86, '3c1f76302c48b6ed0e1293267c85d126', 'miminsrc@gmail.com', 'AdminSourcing', 'recommend', '110', '2016-10-21 19:41:39', '1', 6, '2016-10-18 19:41:39', '2016-10-18 19:41:39'),
(87, '27b1fe50dcd8cfb06a01809d1845c0e7', 'smibs.ga@gmail.com', 'smibs', 'inactive_vend', '15', '2016-10-21 19:41:45', '1', 2, '2016-10-18 19:41:45', '2016-10-18 19:41:45'),
(88, '4cd05146e72df5b2626567be13b88a58', 'wabimantoro@gmail.com', 'wildantest', 'inactive_vend', '15', '2016-10-21 19:41:45', '0', 2, '2016-10-18 19:41:45', '0000-00-00 00:00:00'),
(89, '5ddf4d9c785a76a934d359672d181607', 'smibs.ga@gmail.com', 'smibs', 'active_vend', '16', '2016-10-21 19:45:43', '1', 2, '2016-10-18 19:45:43', '2016-10-18 19:45:43'),
(90, '9c93fb27e4450a676cfc53236b193fb0', 'wabimantoro@gmail.com', 'wildantest', 'active_vend', '16', '2016-10-21 19:45:43', '0', 2, '2016-10-18 19:45:43', '0000-00-00 00:00:00'),
(91, '584e40eae8efead32e1656b7fcbedda2', 'miminsrc@gmail.com', 'AdminSourcing', 'bank_account', '113', '2016-10-21 20:33:31', '1', 0, '2016-10-18 20:33:31', '2016-10-18 20:33:31'),
(92, '3b8004f761f9e4b01203c649475d956e', 'miminsrc@gmail.com', 'AdminSourcing', 'bank_account', '110', '2016-10-21 21:01:16', '1', 6, '2016-10-18 21:01:16', '2016-10-18 21:01:16'),
(93, '8ff75ea4a5b169dcddd754e8f16146b7', 'watsap1.ga@gmail.com', 'watsap1', 'account_number', '113', '2016-10-21 21:05:51', '1', 2, '2016-10-18 21:05:51', '2016-10-18 21:05:51'),
(94, 'c232ae734adeb91091f36251b259627f', 'watsap1.ga@gmail.com', 'watsap1', 'account_number', '110', '2016-10-21 21:06:18', '1', 2, '2016-10-18 21:06:18', '2016-10-18 21:06:18'),
(95, '223a1a9a5d77307d86b1a8c087258cf9', 'watsap2.ga@gmail.com', 'watsap2', 'account_number', '113', '2016-10-21 21:13:47', '1', 7, '2016-10-18 21:13:47', '2016-10-18 21:13:47'),
(96, '9a80b1221bd0c4224130397543219027', 'watsap2.ga@gmail.com', 'watsap2', 'account_number', '110', '2016-10-21 21:13:57', '1', 7, '2016-10-18 21:13:57', '2016-10-18 21:13:57'),
(97, '60562c844af5a3615f076ddd33738314', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'account_number', '113', '2016-10-21 21:15:08', '1', 8, '2016-10-18 21:15:08', '2016-10-18 21:15:08'),
(98, '89c7cbd2749bbed8078a5b4852b003e3', 'zulandraririn@gmail.com', 'RirinZulandra', 'account_number', '113', '2016-10-21 21:15:08', '1', 8, '2016-10-18 21:15:08', '2016-10-18 21:15:08'),
(99, '325cc7439adfeb11d73ae30627bef965', 'StaffIBK.Ga@gmail.com', 'Staff_IBK', 'account_number', '110', '2016-10-21 21:15:13', '1', 8, '2016-10-18 21:15:13', '2016-10-18 21:15:13'),
(100, '47155adaccf3e5719fba664f3273817b', 'zulandraririn@gmail.com', 'RirinZulandra', 'account_number', '110', '2016-10-21 21:15:14', '1', 8, '2016-10-18 21:15:14', '2016-10-18 21:15:14'),
(101, 'e75cee85ee50ab93f79b08533e9428a5', 'smibs.ga@gmail.com', 'smibs', 'blacklist_vend', NULL, '2016-10-22 10:32:58', '1', 2, '2016-10-19 10:32:58', '2016-10-19 10:32:58'),
(102, 'abf2d76f9a9fb02320d8c47e31181294', 'smibs.ga@gmail.com', 'smibs', 'blacklist_vend', NULL, '2016-10-22 10:33:58', '1', 2, '2016-10-19 10:33:58', '2016-10-19 10:33:58'),
(103, '6f1365580fe1c43f620422ecff87471f', 'wabimantoro@gmail.com', 'wildantest', 'blacklist_vend', NULL, '2016-10-22 10:34:03', '0', 2, '2016-10-19 10:34:03', '0000-00-00 00:00:00'),
(104, 'eac371c131fa35b07351374b6a59ee4c', 'smibs.ga@gmail.com', 'smibs', 'blacklist_vend', '8', '2016-10-22 10:42:02', '1', 2, '2016-10-19 10:42:02', '2016-10-19 10:42:02'),
(105, '8220b0ace8c139a251fea4cc63c639ba', 'wabimantoro@gmail.com', 'wildantest', 'blacklist_vend', '8', '2016-10-22 10:42:02', '0', 2, '2016-10-19 10:42:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `session_register`
--

CREATE TABLE IF NOT EXISTS `session_register` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `session` varchar(100) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `session_verification`
--

CREATE TABLE IF NOT EXISTS `session_verification` (
`id` smallint(5) unsigned NOT NULL,
  `session` varchar(50) NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `expired_date` date NOT NULL DEFAULT '0000-00-00',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Dumping data untuk tabel `session_verification`
--

INSERT INTO `session_verification` (`id`, `session`, `id_vendor`, `expired_date`, `created_id`, `created_date`, `last_updated`) VALUES
(1, '57d0e7a591dc0', 2, '2016-12-07', 6, '2016-09-08 11:23:01', '2016-09-08 11:23:01'),
(2, '57d29400f01f6', 3, '2016-12-08', 6, '2016-09-09 17:50:40', '2016-09-09 17:50:40'),
(4, '57d2a89349d36', 5, '2016-12-08', 6, '2016-09-09 19:18:27', '2016-09-09 19:18:27'),
(5, '57d947fc6e91a', 2, '2016-09-17', 4, '2016-09-14 19:52:12', '2016-09-14 19:52:12'),
(6, '57d947fc767a2', 3, '2016-09-17', 4, '2016-09-14 19:52:12', '2016-09-14 19:52:12'),
(7, '57d94d603c49f', 3, '2016-09-17', 4, '2016-09-14 20:15:12', '2016-09-14 20:15:12'),
(8, '57d94e689ff6d', 6, '2016-12-13', 6, '2016-09-14 20:19:36', '2016-09-14 20:19:36'),
(9, '57d94ed5a6c23', 3, '2016-09-17', 4, '2016-09-14 20:21:25', '2016-09-14 20:21:25'),
(10, '57d94fca91a43', 3, '2016-09-17', 4, '2016-09-14 20:25:30', '2016-09-14 20:25:30'),
(11, '57d952b32cafb', 3, '2016-09-17', 4, '2016-09-14 20:37:55', '2016-09-14 20:37:55'),
(12, '57d9f7ad73f59', 2, '2016-09-18', 4, '2016-09-15 08:21:49', '2016-09-15 08:21:49'),
(13, '57d9f7ad7fcca', 3, '2016-09-18', 4, '2016-09-15 08:21:49', '2016-09-15 08:21:49'),
(14, '57d9f98805302', 6, '2016-09-18', 4, '2016-09-15 08:29:44', '2016-09-15 08:29:44'),
(15, '57d9f98810a1e', 2, '2016-09-18', 4, '2016-09-15 08:29:44', '2016-09-15 08:29:44'),
(16, '57d9f9881dfbc', 3, '2016-09-18', 4, '2016-09-15 08:29:44', '2016-09-15 08:29:44'),
(17, '57da9b5f0fc41', 6, '2016-09-18', 4, '2016-09-15 20:00:15', '2016-09-15 20:00:15'),
(18, '57da9b5f17c26', 2, '2016-09-18', 4, '2016-09-15 20:00:15', '2016-09-15 20:00:15'),
(19, '57da9b5f1efc6', 3, '2016-09-18', 4, '2016-09-15 20:00:15', '2016-09-15 20:00:15'),
(22, '57e13e6bebdd3', 21, '2016-12-19', 6, '2016-09-20 20:49:31', '2016-09-20 20:49:31'),
(23, '57e13e8bc5ad2', 23, '2016-12-19', 6, '2016-09-20 20:50:03', '2016-09-20 20:50:03'),
(24, '57e13e97d938c', 22, '2016-12-19', 6, '2016-09-20 20:50:15', '2016-09-20 20:50:15'),
(25, '57e204353a772', 6, '2016-09-24', 4, '2016-09-21 10:53:25', '2016-09-21 10:53:25'),
(26, '57e23322a1b8d', 6, '2016-09-24', 4, '2016-09-21 14:13:38', '2016-09-21 14:13:38'),
(27, '57e2345c2fa4c', 6, '2016-09-24', 4, '2016-09-21 14:18:52', '2016-09-21 14:18:52'),
(28, '57e388330a350', 15, '2016-12-21', 6, '2016-09-22 14:28:51', '2016-09-22 14:28:51'),
(29, '57e3883fa7850', 14, '2016-12-21', 6, '2016-09-22 14:29:03', '2016-09-22 14:29:03'),
(30, '57e3884c5d682', 12, '2016-12-21', 6, '2016-09-22 14:29:16', '2016-09-22 14:29:16'),
(31, '57ea22111781a', 28, '2016-12-26', 6, '2016-09-27 14:38:57', '2016-09-27 14:38:57'),
(32, '57ea22f05adfe', 28, '2016-12-26', 1, '2016-09-27 14:42:40', '2016-09-27 14:42:40'),
(33, '57edec842b1d6', 43, '2016-12-29', 6, '2016-09-30 11:39:32', '2016-09-30 11:39:32'),
(34, '57ee19cdf0b68', 44, '2016-12-29', 6, '2016-09-30 14:52:45', '2016-09-30 14:52:45'),
(35, '57f218b11eff0', 55, '2017-01-01', 6, '2016-10-03 15:37:05', '2016-10-03 15:37:05'),
(36, '57f325e640725', 54, '2017-01-02', 6, '2016-10-04 10:45:42', '2016-10-04 10:45:42'),
(37, '57f4bb209d98b', 54, '2017-01-03', 2, '2016-10-05 15:34:40', '2016-10-05 15:34:40'),
(38, '57f7108a7799c', 61, '2016-10-17', 6, '2016-10-07 10:03:38', '2016-10-07 10:03:38'),
(39, '57fcaad958acb', 66, '2016-10-21', 6, '2016-10-11 16:03:21', '2016-10-11 16:03:21'),
(40, '57ff0e2610405', 68, '2016-10-23', 6, '2016-10-13 11:31:34', '2016-10-13 11:31:34'),
(41, '57ff2bb4c8c16', 73, '2016-10-23', 6, '2016-10-13 13:37:40', '2016-10-13 13:37:40'),
(42, '57ff2df62c845', 73, '2016-10-23', 2, '2016-10-13 13:47:18', '2016-10-13 13:47:18'),
(43, '580435618198f', 101, '2016-10-27', 6, '2016-10-17 09:20:17', '2016-10-17 09:20:17'),
(44, '580618727a584', 113, '2016-10-28', 6, '2016-10-18 19:41:22', '2016-10-18 19:41:22'),
(45, '58062a1457ce2', 110, '2016-10-28', 2, '2016-10-18 20:56:36', '2016-10-18 20:56:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `subcategory`
--

CREATE TABLE IF NOT EXISTS `subcategory` (
`id` smallint(5) unsigned NOT NULL,
  `init_code` varchar(20) NOT NULL,
  `id_cat` smallint(5) NOT NULL,
  `subcategory` varchar(30) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data untuk tabel `subcategory`
--

INSERT INTO `subcategory` (`id`, `init_code`, `id_cat`, `subcategory`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, '011', 1, 'Hotel International', '', '1', 1, '2016-06-10 04:54:18', '2016-07-29 05:34:00'),
(2, '012', 1, 'Motel 1', '', '1', 1, '2016-06-24 08:41:31', '2016-06-24 08:41:31'),
(3, '013', 3, 'Dessert 1', '', '1', 1, '2016-06-24 08:41:42', '2016-06-24 08:41:42'),
(4, '014', 6, 'Fast Food 1', '', '1', 1, '2016-06-24 08:41:53', '2016-06-24 08:41:53'),
(5, '015', 5, 'Haji', '', '1', 1, '2016-06-24 08:42:04', '2016-06-24 08:42:04'),
(6, '016', 5, 'Umroh ', '', '1', 1, '2016-06-24 08:42:18', '2016-08-09 21:26:21'),
(7, '017', 7, 'SubCategory2907', '', '1', 17, '2016-07-29 05:30:32', '2016-07-29 05:30:32'),
(8, '018', 2, 'edit', 'description', '0', 17, '2016-07-29 05:31:42', '2016-09-01 13:30:42'),
(10, '019', 8, 'test', '', '0', 1, '2016-09-20 17:34:24', '2016-09-20 17:34:24'),
(11, '010', 5, 'dOMESTIC', '', '1', 1, '2016-09-28 09:57:54', '2016-09-28 09:57:54'),
(12, '011', 5, 'international', '', '1', 1, '2016-09-28 09:58:16', '2016-09-28 09:58:16'),
(13, '012', 5, 'honeymoon', '', '1', 1, '2016-09-28 09:59:00', '2016-09-28 09:59:00'),
(14, '013', 1, 'bintang 5', '', '1', 1, '2016-09-28 09:59:21', '2016-09-28 09:59:21'),
(15, '014', 1, 'bintang 4', '', '1', 1, '2016-09-28 09:59:34', '2016-09-28 09:59:34'),
(16, '015', 1, 'bintang 3', 'isi', '1', 1, '2016-09-28 09:59:44', '2016-10-14 13:44:32'),
(17, '016', 4, 'Tes SC', 'sekarang bisa', '0', 1, '2016-10-14 13:28:28', '2016-10-14 13:30:38'),
(18, '017', 4, 'Tes satuduatigaa', 'keluarnya pas edit', '1', 1, '2016-10-14 13:29:17', '2016-10-14 13:30:56'),
(19, '018', 2, 'Coba', 'deskrisi', '1', 1, '2016-10-14 13:39:34', '2016-10-14 13:39:58'),
(20, '019', 1, 'dan', '', '1', 1, '2016-10-14 13:44:42', '2016-10-14 13:44:42'),
(21, '020', 2, 'ship', 'pingg', '1', 1, '2016-10-14 13:47:00', '2016-10-14 13:47:05'),
(22, '021', 6, 'waeawe', 'aweweew', '0', 1, '2016-10-18 17:01:11', '2016-10-18 17:01:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `subcat_notes`
--

CREATE TABLE IF NOT EXISTS `subcat_notes` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_visit` smallint(5) NOT NULL,
  `id_subcat` smallint(5) NOT NULL,
  `notes` varchar(100) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `subcat_notes`
--

INSERT INTO `subcat_notes` (`id`, `id_vendor`, `id_visit`, `id_subcat`, `notes`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 2, 1, 3, 'Sudah OK', 2, '2016-09-08 10:51:25', '2016-09-09 15:52:37'),
(2, 2, 1, 5, 'Tidak Ok saat site visit', 2, '2016-09-08 10:53:56', '2016-09-09 15:52:37'),
(3, 3, 3, 1, 'AC nya panasonic', 2, '2016-09-09 15:56:59', '2016-09-09 15:52:37'),
(4, 3, 3, 1, 'TV nya merek sanyo', 2, '2016-09-09 15:57:09', '2016-09-09 15:52:37'),
(5, 3, 3, 2, 'Kurang asyikk', 2, '2016-09-09 15:58:18', '2016-09-09 15:52:37'),
(7, 6, 6, 1, 'kantor nya sangat representatif, memadai, banyak hiburan, banyak makanan, masjid nya bagus', 2, '2016-09-14 15:50:42', '2016-09-09 15:52:37'),
(8, 26, 10, 1, 'SAYA MAU TAMASYA, KELILING KOTA, HENDAK MELIHAT2, KERAMAIAN YANG ADA\n\nNAIK KERETA KUDA, KERETA TAK B', 2, '2016-09-22 09:18:39', '2016-09-22 18:39:00'),
(10, 10, 2, 2, '', 2, '2016-09-29 09:54:57', '0000-00-00 00:00:00'),
(11, 10, 2, 2, '', 2, '2016-09-29 09:55:02', '0000-00-00 00:00:00'),
(12, 10, 2, 2, '', 2, '2016-09-29 09:55:06', '0000-00-00 00:00:00'),
(13, 55, 17, 1, 'semuanya bgus', 2, '2016-10-03 15:06:24', '2016-10-03 06:24:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `subcat_resumes`
--

CREATE TABLE IF NOT EXISTS `subcat_resumes` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_visit` smallint(5) NOT NULL,
  `id_subcat` smallint(5) NOT NULL,
  `resume` varchar(100) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `subcat_resumes`
--

INSERT INTO `subcat_resumes` (`id`, `id_vendor`, `id_visit`, `id_subcat`, `resume`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 2, 1, 3, 'OK', 2, '2016-09-08 10:53:31', '2016-09-09 15:52:37'),
(2, 2, 1, 5, 'Not Ok', 2, '2016-09-08 10:54:10', '2016-09-09 15:52:37'),
(3, 3, 3, 1, 'Intinya kantornya enak banget suasananya', 2, '2016-09-09 15:57:33', '2016-09-09 15:52:37'),
(4, 3, 3, 2, 'Ya gitu deh', 2, '2016-09-09 15:58:28', '2016-09-09 15:52:37'),
(6, 6, 6, 1, 'secara keseluruhan sangat baik,\nsangat pantas menjadi rekanan GA untuk menyediakan tempat penginapan', 2, '2016-09-14 15:51:25', '2016-09-14 19:11:00'),
(7, 26, 10, 1, 'RESUME INI BERTUJUAN UNTUK MENENTUKAN APAKAH SUATUSUB KATEGORY SAYA JUGA GA PAHAM INI BUAT APAAN SIH', 2, '2016-09-22 09:19:30', '2016-09-22 19:30:00'),
(8, 55, 17, 1, 'oke sangat', 2, '2016-10-03 15:06:42', '2016-10-03 06:42:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `subcontent_type`
--

CREATE TABLE IF NOT EXISTS `subcontent_type` (
  `id` bigint(5) unsigned NOT NULL,
  `content_id` smallint(5) NOT NULL,
  `subtype_name` varchar(50) NOT NULL,
  `is_active` enum('0','1') NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  `document_req_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `need_vendor` enum('0','1') NOT NULL,
  `behavior_id` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `subcontent_type`
--

INSERT INTO `subcontent_type` (`id`, `content_id`, `subtype_name`, `is_active`, `created_id`, `created_date`, `last_updated`, `document_req_id`, `need_vendor`, `behavior_id`) VALUES
(0, 1, 'General', '1', 1, '2016-10-17 09:55:42', '2016-10-17 09:55:42', NULL, '0', NULL),
(1, 1, 'News Public', '1', 1, '2016-10-18 17:09:20', '2016-10-18 17:09:20', NULL, '0', NULL),
(2, 1, 'News Blast', '1', 1, '2016-10-18 17:09:38', '2016-10-18 17:09:38', NULL, '0', NULL),
(3, 2, 'Open Bidding', '1', 1, '2016-10-17 08:51:31', '2016-10-17 08:51:31', NULL, '0', NULL),
(4, 2, 'Open Sourcing', '1', 1, '2016-10-17 08:51:23', '2016-10-17 08:51:23', NULL, '0', 3),
(5, 3, 'Direct Selection', '1', 1, '2016-10-17 08:58:44', '2016-10-17 08:58:44', NULL, '1', NULL),
(6, 3, 'Limited Bidding', '1', 1, '2016-10-17 08:58:31', '2016-10-17 08:58:31', NULL, '1', 2),
(7, 3, 'testing', '0', 1, '2016-10-18 17:08:18', '2016-10-18 17:08:18', NULL, '0', NULL),
(8, 1, 'test', '0', 1, '2016-10-19 10:39:02', '2016-10-19 10:39:02', NULL, '0', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `subcontent_type_old`
--

CREATE TABLE IF NOT EXISTS `subcontent_type_old` (
`id` smallint(5) unsigned NOT NULL,
  `content_id` smallint(5) NOT NULL,
  `subtype_name` varchar(50) NOT NULL,
  `is_active` enum('0','1') NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  `document_req_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `need_vendor` enum('0','1') NOT NULL,
  `behavior_id` smallint(5) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `subcontent_type_old`
--

INSERT INTO `subcontent_type_old` (`id`, `content_id`, `subtype_name`, `is_active`, `created_id`, `created_date`, `last_updated`, `document_req_id`, `need_vendor`, `behavior_id`) VALUES
(1, 1, 'News Public', '1', 17, '2016-09-01 13:35:44', '2016-09-01 13:35:44', '2', '0', NULL),
(2, 1, 'News Blast', '1', 17, '2016-07-15 08:21:01', '2016-08-12 10:52:01', '', '1', 1),
(3, 2, 'Open Bidding  ', '1', 17, '2016-05-25 13:51:37', '2016-08-12 10:54:19', '2', '', 2),
(4, 2, 'Open Sourcing', '1', 1, '2016-10-17 09:41:25', '2016-10-17 09:41:25', '', '0', NULL),
(5, 3, 'Direct Selection', '1', 17, '2016-05-25 13:51:53', '2016-08-26 09:44:58', '2', '1', 4),
(6, 3, 'Limited Bidding', '1', 17, '2016-09-01 10:31:15', '2016-09-02 19:43:17', '2', '1', 1),
(7, 3, 'Testing eProc', '0', 1, '2016-10-17 09:40:05', '2016-10-17 09:40:05', NULL, '0', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier_category`
--

CREATE TABLE IF NOT EXISTS `supplier_category` (
`id` smallint(5) unsigned NOT NULL,
  `id_cat` smallint(5) NOT NULL,
  `id_subcat` smallint(5) NOT NULL,
  `status` enum('0','1') DEFAULT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_docs`
--

CREATE TABLE IF NOT EXISTS `table_docs` (
`id` smallint(5) unsigned NOT NULL,
  `table` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_date` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `table_docs`
--

INSERT INTO `table_docs` (`id`, `table`, `status`, `created_date`, `last_updated`) VALUES
(1, 'correspondence', '1', '2016-10-03 00:00:00', '2016-10-03 00:00:00'),
(2, 'dd_docs', '1', '2016-10-03 00:00:00', '2016-10-03 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_affiliates`
--

CREATE TABLE IF NOT EXISTS `temp_affiliates` (
`id` smallint(5) unsigned NOT NULL,
  `id_tbl` smallint(5) DEFAULT NULL COMMENT 'id table original',
  `id_vendor` smallint(5) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `competency` varchar(50) NOT NULL,
  `contact` smallint(5) NOT NULL,
  `remark` varchar(100) NOT NULL,
  `status` enum('1','0','-1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('rejected','approved','delete','add') CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `temp_affiliates`
--

INSERT INTO `temp_affiliates` (`id`, `id_tbl`, `id_vendor`, `company_name`, `competency`, `contact`, `remark`, `status`, `created_id`, `created_date`, `last_updated`, `approval`) VALUES
(6, NULL, 23, 'oejf;qj', ';jfq;ej', 0, ';jq;eg', '0', 2, '2016-09-26 11:41:44', '2016-09-26 11:41:44', 'add'),
(7, NULL, 17, 'lkjhjkljlkj', 'klj', 0, 'lkj', '1', 2, '2016-09-30 11:16:20', '2016-09-30 11:16:20', 'add'),
(8, NULL, 22, 'WIJAYA KAYA', 'CONSTRUCTION', 0, 'GAGAL', '-1', 2, '2016-10-12 13:59:52', '2016-10-12 13:59:52', 'add');

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_affiliates_avl`
--

CREATE TABLE IF NOT EXISTS `temp_affiliates_avl` (
`id` smallint(5) unsigned NOT NULL,
  `id_tbl` smallint(5) DEFAULT NULL COMMENT 'id table original',
  `id_vendor` smallint(5) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `competency` varchar(50) NOT NULL,
  `contact` smallint(5) NOT NULL,
  `remark` varchar(100) NOT NULL,
  `status` enum('2','1','0','wait','rejected') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('delete','add') CHARACTER SET latin1 DEFAULT NULL,
  `sess_name` smallint(5) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `temp_affiliates_avl`
--

INSERT INTO `temp_affiliates_avl` (`id`, `id_tbl`, `id_vendor`, `company_name`, `competency`, `contact`, `remark`, `status`, `created_id`, `created_date`, `last_updated`, `approval`, `sess_name`) VALUES
(2, NULL, 17, 'klj', 'lkj', 0, 'lkj', '2', 19, '2016-09-30 11:20:13', '2016-09-30 11:20:13', 'add', 2),
(3, 8, 17, 'klj', 'lkj', 0, 'lkj', '2', 19, '2016-09-30 11:20:13', '2016-09-30 11:21:55', 'delete', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_contact_person`
--

CREATE TABLE IF NOT EXISTS `temp_contact_person` (
`id` smallint(5) unsigned NOT NULL,
  `id_tbl` smallint(5) DEFAULT NULL COMMENT 'id table original',
  `id_vendor` smallint(5) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status` enum('1','0','-1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('rejected','approved','delete','add') CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data untuk tabel `temp_contact_person`
--

INSERT INTO `temp_contact_person` (`id`, `id_tbl`, `id_vendor`, `fullname`, `position`, `mobile`, `email`, `status`, `created_id`, `created_date`, `last_updated`, `approval`) VALUES
(24, NULL, 23, 'arif', 'SA', '085485284849a', 'wsn.ff@nflkasef.com', '0', 2, '2016-09-26 11:02:34', '2016-09-26 11:02:34', 'add'),
(25, NULL, 17, 'hkjhk', 'jhjk', 'hkjh', 'kjh', '1', 2, '2016-09-30 11:06:30', '2016-09-30 11:06:30', 'add'),
(26, 44, 17, 'hkjhk', 'jhjk', 'hkjh', 'kjh', '1', 2, '2016-09-30 11:06:30', '2016-09-30 11:06:38', 'delete'),
(27, NULL, 22, 'ERWIN', 'hrd', '8723486289', 'JGAJHF@FAEKJF.COM', '1', 2, '2016-10-12 13:52:54', '2016-10-12 13:52:54', 'add');

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_contact_person_avl`
--

CREATE TABLE IF NOT EXISTS `temp_contact_person_avl` (
`id` smallint(5) unsigned NOT NULL,
  `id_tbl` smallint(5) DEFAULT NULL COMMENT 'id table original',
  `id_vendor` smallint(5) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status` enum('2','1','0','wait','rejected') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('delete','add') CHARACTER SET latin1 DEFAULT NULL,
  `sess_name` smallint(5) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `temp_contact_person_avl`
--

INSERT INTO `temp_contact_person_avl` (`id`, `id_tbl`, `id_vendor`, `fullname`, `position`, `mobile`, `email`, `status`, `created_id`, `created_date`, `last_updated`, `approval`, `sess_name`) VALUES
(5, NULL, 17, 'lkj', 'lkj', 'kj', 'lk', '2', 19, '2016-09-30 11:19:29', '2016-09-30 11:19:29', 'add', 2),
(6, 46, 17, 'lkj', 'lkj', 'kj', 'lk', '2', 19, '2016-09-30 11:19:29', '2016-09-30 11:21:55', 'delete', 3),
(7, 45, 17, 'lkj', 'lkj', 'kj', 'lk', '2', 19, '2016-09-30 11:19:29', '2016-09-30 11:21:38', 'delete', 4),
(8, NULL, 73, 'ARIF ABDUL HAKIM', 'ANALIS', '018373801398', 'MUHAMMAD.HAKIM@ASYST.CO.ID', '2', 36, '2016-10-13 15:31:36', '2016-10-13 15:31:36', 'add', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_correspondence`
--

CREATE TABLE IF NOT EXISTS `temp_correspondence` (
`id` smallint(5) unsigned NOT NULL,
  `id_tbl` smallint(5) DEFAULT NULL COMMENT 'id table original',
  `id_vendor` smallint(5) NOT NULL,
  `doc_name` varchar(50) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `desc` varchar(100) NOT NULL,
  `eff_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `remark` varchar(100) NOT NULL,
  `filetype` varchar(50) NOT NULL,
  `status` enum('1','0','-1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('rejected','approved','delete','add') CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data untuk tabel `temp_correspondence`
--

INSERT INTO `temp_correspondence` (`id`, `id_tbl`, `id_vendor`, `doc_name`, `doc_num`, `desc`, `eff_date`, `exp_date`, `remark`, `filetype`, `status`, `created_id`, `created_date`, `last_updated`, `approval`) VALUES
(25, NULL, 23, 'ktp', '3374062606900005', 'yotaot', '2016-09-01', '2016-09-21', 'hfouahfhf', 'jpg', '0', 2, '2016-09-26 11:24:46', '2016-09-26 11:24:46', 'add'),
(26, NULL, 23, 'ktp', '3374062606900005', 'yotaot', '2016-09-01', '2016-09-21', 'hfouahfhf', 'jpg', '0', 2, '2016-09-26 11:33:57', '2016-09-26 11:33:57', 'add'),
(27, NULL, 22, 'KTP', '3374062606900005', 'KTP ASLI LHO', '2016-10-11', '2016-10-26', 'INPUT KTP', 'jpg', '1', 2, '2016-10-12 13:58:27', '2016-10-12 13:58:27', 'add');

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_correspondence_avl`
--

CREATE TABLE IF NOT EXISTS `temp_correspondence_avl` (
`id` smallint(5) unsigned NOT NULL,
  `id_tbl` smallint(5) DEFAULT NULL COMMENT 'id table original',
  `id_vendor` smallint(5) NOT NULL,
  `doc_name` varchar(50) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `desc` varchar(100) NOT NULL,
  `eff_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `remark` varchar(100) NOT NULL,
  `filetype` varchar(50) NOT NULL,
  `status` enum('2','1','0','wait','rejected') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('delete','add') CHARACTER SET latin1 DEFAULT NULL,
  `sess_name` smallint(5) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_organization`
--

CREATE TABLE IF NOT EXISTS `temp_organization` (
`id` smallint(5) unsigned NOT NULL,
  `id_tbl` smallint(5) DEFAULT NULL COMMENT 'id table original',
  `id_vendor` smallint(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `position` varchar(50) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('1','0','-1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('rejected','approved','delete','add') CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `temp_organization`
--

INSERT INTO `temp_organization` (`id`, `id_tbl`, `id_vendor`, `name`, `address`, `phone`, `position`, `remark`, `status`, `created_id`, `created_date`, `last_updated`, `approval`) VALUES
(14, NULL, 23, 'udin', 'jaya', '08475383929', 'Direktur', 'test', '0', 2, '2016-09-26 11:11:52', '2016-09-26 11:11:52', 'add'),
(15, NULL, 17, 'lk', 'jklj', 'klj', 'klj', 'klj', '1', 2, '2016-09-30 11:15:47', '2016-09-30 11:15:47', 'add'),
(16, NULL, 22, 'WISNU', 'PEMBANGUNAN JAYA', '28751038', 'SMIBS', 'TAMBAH PENGURUS', '1', 2, '2016-10-12 13:57:06', '2016-10-12 13:57:06', 'add');

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_organization_avl`
--

CREATE TABLE IF NOT EXISTS `temp_organization_avl` (
`id` smallint(5) unsigned NOT NULL,
  `id_tbl` smallint(5) DEFAULT NULL COMMENT 'id table original',
  `id_vendor` smallint(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `position` varchar(50) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('2','1','0','wait','rejected') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('delete','add') CHARACTER SET latin1 DEFAULT NULL,
  `sess_name` smallint(5) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `temp_organization_avl`
--

INSERT INTO `temp_organization_avl` (`id`, `id_tbl`, `id_vendor`, `name`, `address`, `phone`, `position`, `remark`, `status`, `created_id`, `created_date`, `last_updated`, `approval`, `sess_name`) VALUES
(4, NULL, 17, 'lkj', 'lkj', 'lkj', 'klj', 'lkjl', '2', 19, '2016-09-30 11:19:59', '2016-09-30 11:19:59', 'add', 2),
(5, 8, 17, 'lkj', 'lkj', 'lkj', 'klj', 'lkjl', '2', 19, '2016-09-30 11:19:59', '2016-09-30 11:21:55', 'delete', 3),
(6, NULL, 73, 'BROTO', 'KARAWACI', '857293750187', 'LEAD', 'TESTING', '0', 36, '2016-10-13 15:49:37', '2016-10-13 15:49:37', 'add', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_owner`
--

CREATE TABLE IF NOT EXISTS `temp_owner` (
`id` smallint(5) unsigned NOT NULL,
  `id_tbl` smallint(5) DEFAULT NULL COMMENT 'id table original',
  `id_vendor` smallint(5) NOT NULL,
  `owner_name` varchar(50) NOT NULL,
  `owner_address` varchar(100) NOT NULL,
  `owner_phone` varchar(20) NOT NULL,
  `owner_position` varchar(50) NOT NULL,
  `owner_shared` smallint(5) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('1','0','-1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('rejected','approved','delete','add') CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `temp_owner`
--

INSERT INTO `temp_owner` (`id`, `id_tbl`, `id_vendor`, `owner_name`, `owner_address`, `owner_phone`, `owner_position`, `owner_shared`, `remark`, `status`, `created_id`, `created_date`, `last_updated`, `approval`) VALUES
(8, NULL, 23, 'Udin', 'Jaya 1', '081295057000', 'Direktur', 0, 'test', '0', 2, '2016-09-26 11:10:16', '2016-09-26 11:10:16', 'add'),
(9, NULL, 17, 'hhjkh', 'kjhkj', 'hkj', 'hkj', 0, 'h', '1', 2, '2016-09-30 11:15:23', '2016-09-30 11:15:23', 'add'),
(10, NULL, 22, 'RICHO', 'M1', '9837T5198', 'HEAD', 50, 'NoneNUNONI', '1', 2, '2016-10-12 13:49:02', '2016-10-12 13:49:02', 'add');

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_owner_avl`
--

CREATE TABLE IF NOT EXISTS `temp_owner_avl` (
`id` smallint(5) unsigned NOT NULL,
  `id_tbl` smallint(5) DEFAULT NULL COMMENT 'id table original',
  `id_vendor` smallint(5) NOT NULL,
  `owner_name` varchar(50) NOT NULL,
  `owner_address` varchar(100) NOT NULL,
  `owner_phone` varchar(20) NOT NULL,
  `owner_position` varchar(50) NOT NULL,
  `owner_shared` smallint(5) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('2','1','0','wait','rejected') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('delete','add') CHARACTER SET latin1 DEFAULT NULL,
  `sess_name` smallint(5) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `temp_owner_avl`
--

INSERT INTO `temp_owner_avl` (`id`, `id_tbl`, `id_vendor`, `owner_name`, `owner_address`, `owner_phone`, `owner_position`, `owner_shared`, `remark`, `status`, `created_id`, `created_date`, `last_updated`, `approval`, `sess_name`) VALUES
(5, NULL, 17, 'j', 'kjkl', 'jkl', 'jlk', 0, 'jl', '2', 19, '2016-09-30 11:19:51', '2016-09-30 11:19:51', 'add', 2),
(6, 10, 17, 'j', 'kjkl', 'jkl', 'jlk', 0, 'jl', '2', 19, '2016-09-30 11:19:51', '2016-09-30 11:21:55', 'delete', 3),
(7, 9, 17, 'j', 'kjkl', 'jkl', 'jlk', 0, 'jl', '2', 19, '2016-09-30 11:19:51', '2016-09-30 11:21:38', 'delete', 5),
(8, NULL, 73, 'ARCHI', 'CILEDUG', '38751735', 'TECHNICAL WRITER', 32767, 'PEMILIK SAH', 'rejected', 36, '2016-10-13 15:43:37', '2016-10-13 15:43:37', 'add', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_references`
--

CREATE TABLE IF NOT EXISTS `temp_references` (
`id` smallint(5) unsigned NOT NULL,
  `id_tbl` smallint(5) DEFAULT NULL COMMENT 'id table original',
  `id_vendor` smallint(5) NOT NULL,
  `cust_name` varchar(50) NOT NULL,
  `project` varchar(50) NOT NULL,
  `point` smallint(5) NOT NULL,
  `date` date NOT NULL,
  `remark` varchar(255) NOT NULL,
  `status` enum('1','0','-1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('rejected','approved','delete','add') CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `temp_references`
--

INSERT INTO `temp_references` (`id`, `id_tbl`, `id_vendor`, `cust_name`, `project`, `point`, `date`, `remark`, `status`, `created_id`, `created_date`, `last_updated`, `approval`) VALUES
(11, NULL, 23, 'joni', 'eproc', 0, '2016-09-26', 'jf,aeljf', '0', 2, '2016-09-26 11:35:49', '2016-09-26 11:35:49', 'add'),
(12, NULL, 17, 'jkl', 'jklj', 0, '2016-09-30', 'kl', '1', 2, '2016-09-30 11:16:13', '2016-09-30 11:16:13', 'add'),
(13, NULL, 22, 'ARCHI', 'PEMBANGUNAN MRT', 95, '2016-10-10', 'SU FAR SO GOOD', '1', 2, '2016-10-12 13:58:59', '2016-10-12 13:58:59', 'add');

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_references_avl`
--

CREATE TABLE IF NOT EXISTS `temp_references_avl` (
`id` smallint(5) unsigned NOT NULL,
  `id_tbl` smallint(5) DEFAULT NULL COMMENT 'id table original',
  `id_vendor` smallint(5) NOT NULL,
  `cust_name` varchar(50) NOT NULL,
  `project` varchar(50) NOT NULL,
  `point` smallint(5) NOT NULL,
  `date` date NOT NULL,
  `remark` varchar(255) NOT NULL,
  `status` enum('2','1','0','wait','rejected') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('delete','add') CHARACTER SET latin1 DEFAULT NULL,
  `sess_name` smallint(5) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `temp_references_avl`
--

INSERT INTO `temp_references_avl` (`id`, `id_tbl`, `id_vendor`, `cust_name`, `project`, `point`, `date`, `remark`, `status`, `created_id`, `created_date`, `last_updated`, `approval`, `sess_name`) VALUES
(2, NULL, 17, 'kljlkj', 'lkjlk', 0, '2016-09-30', 'lkj', '2', 19, '2016-09-30 11:20:06', '2016-09-30 11:20:06', 'add', 2),
(3, 7, 17, 'kljlkj', 'lkjlk', 0, '2016-09-30', 'lkj', '2', 19, '2016-09-30 11:20:06', '2016-09-30 11:21:55', 'delete', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_vendor_category`
--

CREATE TABLE IF NOT EXISTS `temp_vendor_category` (
`id` mediumint(11) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_category` smallint(5) NOT NULL,
  `id_subcat` smallint(5) NOT NULL,
  `status` enum('1','0','-1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('rejected','approved','delete','add') DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Dumping data untuk tabel `temp_vendor_category`
--

INSERT INTO `temp_vendor_category` (`id`, `id_vendor`, `id_category`, `id_subcat`, `status`, `created_id`, `created_date`, `last_updated`, `approval`) VALUES
(29, 23, 2, 8, '0', 2, '2016-09-26 11:00:05', '2016-09-26 11:00:05', 'add'),
(30, 23, 3, 3, '0', 2, '2016-09-26 11:00:22', '2016-09-26 11:00:22', 'add'),
(31, 23, 6, 4, '0', 2, '2016-09-26 11:00:34', '2016-09-26 11:00:34', 'add'),
(32, 23, 6, 4, '0', 2, '2016-09-26 11:00:52', '2016-09-26 11:00:52', 'add'),
(33, 17, 3, 3, '1', 2, '2016-09-30 10:49:24', '2016-09-30 10:49:24', 'add'),
(34, 17, 3, 3, '1', 2, '2016-09-30 10:49:24', '2016-09-30 10:51:12', 'delete'),
(35, 17, 5, 5, '1', 2, '2016-09-30 10:59:03', '2016-09-30 10:59:03', 'add'),
(36, 17, 2, 8, '1', 2, '2016-09-30 11:02:40', '2016-09-30 11:02:40', 'add'),
(37, 17, 2, 8, '1', 2, '2016-09-30 11:02:42', '2016-09-30 11:02:42', 'add'),
(38, 17, 2, 8, '1', 2, '2016-09-30 11:02:42', '2016-09-30 11:03:01', 'delete'),
(39, 17, 2, 8, '1', 2, '2016-09-30 11:02:40', '2016-09-30 11:04:03', 'delete'),
(40, 17, 5, 5, '1', 2, '2016-09-30 10:59:03', '2016-09-30 10:59:09', 'delete'),
(41, 17, 2, 8, '-1', 2, '2016-09-30 11:05:55', '2016-09-30 11:05:55', 'add'),
(42, 17, 5, 12, '1', 2, '2016-09-30 11:06:03', '2016-09-30 11:06:03', 'add'),
(43, 22, 1, 1, '1', 2, '2016-10-12 11:10:54', '2016-10-12 11:10:54', 'add'),
(44, 22, 1, 2, '1', 2, '2016-10-12 11:11:03', '2016-10-12 11:11:03', 'add');

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_vendor_category_avl`
--

CREATE TABLE IF NOT EXISTS `temp_vendor_category_avl` (
`id` mediumint(11) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_category` smallint(5) NOT NULL,
  `id_subcat` smallint(5) NOT NULL,
  `status` enum('2','1','0','wait','rejected') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `approval` enum('delete','add') DEFAULT NULL,
  `sess_name` smallint(5) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `temp_vendor_category_avl`
--

INSERT INTO `temp_vendor_category_avl` (`id`, `id_vendor`, `id_category`, `id_subcat`, `status`, `created_id`, `created_date`, `last_updated`, `approval`, `sess_name`) VALUES
(5, 17, 3, 3, '2', 19, '2016-09-30 11:19:19', '2016-09-30 11:19:19', 'add', 2),
(6, 17, 3, 3, '2', 19, '2016-09-30 11:19:19', '2016-09-30 11:21:55', 'delete', 3),
(7, 17, 3, 3, '2', 19, '2016-09-30 11:19:19', '2016-09-30 11:21:38', 'delete', 3),
(8, 17, 5, 12, 'rejected', 2, '2016-09-30 11:06:03', '2016-09-30 11:06:09', 'delete', 4),
(9, 17, 5, 12, '2', 2, '2016-09-30 11:06:03', '2016-09-30 11:06:09', 'delete', 5),
(10, 73, 2, 8, '2', 36, '2016-10-13 15:31:05', '2016-10-13 15:31:05', 'add', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `is_admin` enum('1','0') COLLATE utf8_bin NOT NULL DEFAULT '0',
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_vendor` smallint(5) NOT NULL,
  `id_functional` smallint(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=44 ;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `is_admin`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`, `id_vendor`, `id_functional`) VALUES
(1, 'admin', '$2a$08$xuHbfCZMoxsGp7Lp3uTEUeNl9VY1gKHJbHsGCdOKxUvoGEP8cC8/G', 'admin@admin.com', '1', 1, 0, NULL, NULL, NULL, NULL, '49c42e38d2b2f7cd40af7498dc3be0c4', '172.25.138.217', '2016-10-19 08:58:05', '2016-04-05 10:01:52', '2016-10-19 01:58:05', 0, 0),
(2, 'AdminSourcing', '$2a$08$doNfqw02zHSkkF12lU3hie8VjCAxNBQEX6G5JfP7rNrYujjoTp8hq', 'miminsrc@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, 'f718ffc4b1a9d7e7dc90d3ceb41d85ce', '172.25.138.200', '2016-10-19 10:39:35', '2016-09-07 16:43:06', '2016-10-19 03:39:35', 0, 0),
(3, 'smibs', '$2a$08$Ej71QBtE8KL1SIvD52HvUu8yEyoSjIRJJvPvXD5GVEgj7t5iA2u3K', 'smibs.ga@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, '36831bd1006cf454baa2358a355e92e7', '172.25.139.91', '2016-10-19 09:57:52', '2016-09-07 16:45:51', '2016-10-19 02:57:52', 0, 0),
(4, 'smibk', '$2a$08$/.vucpkKuxFNl6JNiTspyOLu/kwCDqxetYuuUw./.XIj/ARNfTK9C', 'smibk.ga@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, '47054c8d050db28e481d328938dde007', '172.25.139.57', '2016-10-19 10:26:45', '2016-09-07 16:47:27', '2016-10-19 03:26:45', 0, 0),
(5, 'smibr', '$2a$08$Wz1.T.3bgKPQPeZFKljPDeKkg6lA6pyaDkRHz.HPtgWle6vm9/2Wi', 'smibr.ga@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, '764fb0bf1b927b59819c73dd98720d64', '172.25.139.16', '2016-09-09 17:46:28', '2016-09-07 16:51:43', '2016-09-09 10:46:28', 0, 0),
(6, 'vpib', '$2a$08$ChWkCwSqX8MqsLMx8k5JuuvREVBkj1cBhXp7kf0c191IlFlxt69ZO', 'vpib.ga@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, '9682e5c58c3a6cf829ce99d718175960', '172.25.139.132', '2016-10-18 20:52:04', '2016-09-07 16:52:57', '2016-10-18 13:52:04', 0, 0),
(7, 'watsap1', '$2a$08$fGmTimB0btad2uBdR9n/h.j94wSxQ6Cb.iN0DrRZkWmZPRLjmzuga', 'watsap1.ga@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, '660f0f94775c5e21d0bcd5f64f18c4b1', '172.25.139.132', '2016-10-18 21:07:55', '2016-09-07 16:54:12', '2016-10-18 14:07:55', 0, 0),
(8, 'watsap2', '$2a$08$4zPvRraiLxcxSuHj.tp3POxMlwQ6sb3ha0SdLxbLbvsBuos/a5f92', 'watsap2.ga@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, 'd3d9b6b9aa21d8c166e899be7a8dae9c', '172.25.139.132', '2016-10-18 21:14:12', '2016-09-07 16:58:00', '2016-10-18 14:14:12', 0, 0),
(10, 'PT Vendor Indonesia', '$2a$08$cKnT4STItDbiNnQBWr2NveHmE88HOhOVasLzla2C97IpJnvkMwIMu', 'vendor.regis@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, 'e9dd7ccebaa199783e14aba519464fad', '172.25.138.144', '2016-09-09 09:16:19', '2016-09-08 16:13:38', '2016-10-03 07:01:31', 2, 0),
(11, 'pt.ar82', '$2a$08$WgzMfGvquLBlNx7n01UXOeSwFu7Kh0FwA8wq4KFUpaPdkV1gZpZWS', 'anj4r.7070@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, 'bdfc0d8bf97d50f0f64b431b735152af', '172.25.139.16', '2016-09-09 18:11:55', '2016-09-09 18:02:14', '2016-09-09 11:11:55', 4, 0),
(12, 'Staff_IBS', '$2a$08$IJzHKCJ.USjvoAu5d..I0.3cYmBrsiKAugnxlawx2GcXzc0XSqwl.', 'StaffIBS.Ga@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, 'bae315fe4faef523f5a854116c64399f', '172.25.138.106', '2016-10-18 23:40:21', '2016-09-14 14:00:10', '2016-10-18 16:40:21', 0, 0),
(14, 'Adminasyst', '$2a$08$bUYrp3n1TcjHHhwJBwUq8uflKEcXokGaL5jH1OrkR6uLNzHcRpeAe', 'admasyst.ga@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, '1f2ceef805686007beb9d48de206a70d', '172.25.138.183', '2016-10-18 10:43:13', '2016-09-14 14:13:04', '2016-10-18 03:43:13', 0, 0),
(15, 'Staff_IBK', '$2a$08$BJDkz5QL2.2EBi/K/lppTup.FpK/QBqCMso8IyQ1/Iyn1b.s8v52u', 'StaffIBK.Ga@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, '54f3ff2530ff18020cc7c1708dede231', '172.25.139.132', '2016-10-18 21:19:48', '2016-09-14 18:41:53', '2016-10-18 14:19:48', 0, 0),
(16, 'ptas67', '$2a$08$zaRmlVBx7x/GRGmLufsskuXc5O/kUIFrK9WmxKOLzM3IDtb.aV2mu', 'wisnu.arimurti@asyst.co.idd', '0', 1, 0, NULL, NULL, NULL, NULL, 'a62be284b623a4f2099ee1d37c5e641a', '172.25.220.221', '2016-10-03 10:32:00', '2016-09-14 20:39:20', '2016-10-13 07:40:01', 6, 0),
(17, 'staff', '$2a$08$XiCk8cfy6ZUDk9HpaWsnAO8yKhLTR2zOV5VlWHhNVCSMZ6kG/qRim', 'nurul.apriyani@gmail.com', '0', 0, 0, NULL, NULL, NULL, NULL, '928c0fe221e747bfb846d02bbb55b255', '172.25.220.221', '2016-09-19 14:14:32', '2016-09-16 11:03:54', '2016-09-20 01:05:26', 0, 0),
(19, 'ririn', '$2a$08$doNfqw02zHSkkF12lU3hie8VjCAxNBQEX6G5JfP7rNrYujjoTp8hq', 'ririn.zulandra@asyst.co.id', '0', 1, 0, NULL, NULL, NULL, NULL, 'a62be284b623a4f2099ee1d37c5e641a', '172.25.138.156', '2016-10-19 09:24:09', '2016-09-14 20:39:20', '2016-10-19 02:24:09', 17, 0),
(20, 'RirinZulandra', '$2a$08$e8gEjcufohKVKyJw77rnouD0kRZ0KvnEypwOcOSHnsDBduV7n7/jS', 'zulandraririn@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, 'ef8db105fb9380a39205194414841b0c', '172.25.220.221', '2016-10-14 13:46:26', '2016-09-20 15:02:31', '2016-10-14 06:46:26', 0, 0),
(21, 'ptav3', '$2a$08$5mKVbQsbb/4dOp1EV1YMm.EreeJgoHiE2qUE5uZMkN7QcDEMhKReW', 'srmadusari@naver.com', '0', 0, 0, NULL, NULL, NULL, NULL, '6d650214c167839404d096dde4565c16', '172.25.139.65', '0000-00-00 00:00:00', '2016-09-20 21:06:29', '2016-10-18 14:27:05', 0, 0),
(22, 'ptav93', '$2a$08$8CMWqjqGaC0YG.fqgI.vi.t1ExoQUftM2JcwvNk0hJ6yuj04FOiES', 'srmadusari@daum.nets', '0', 0, 0, NULL, NULL, NULL, NULL, '5dad70e1e36ddb33d204e9a752257c78', '172.25.139.65', '0000-00-00 00:00:00', '2016-09-20 21:06:36', '2016-10-18 14:38:24', 0, 0),
(23, 'ptin38', '$2a$08$/Tre5bNqutat/ibllQZVeeOzwRJNbjIv8hYL7Vo8FxNlJaTz2EVNO', 'madusama@hotmail.com', '0', 0, 0, NULL, NULL, NULL, NULL, '92281776ed3b41c62505702ee10d6b40', '172.25.139.65', '0000-00-00 00:00:00', '2016-09-20 21:06:48', '2016-09-20 14:06:48', 0, 0),
(24, 'ptbl100', '$2a$08$58RMgsgy4dWhsANirG.oH.S3NOhuVkUbAmfSD2R8zbxFsfJovQZmm', 'madudan182@yahoo.co.id', '0', 0, 0, NULL, NULL, NULL, NULL, '816c2bed0ae7c0f488b0a93ce177f5bb', '172.25.139.65', '0000-00-00 00:00:00', '2016-09-20 21:08:36', '2016-09-20 14:08:36', 0, 0),
(25, 'ptre67', '$2a$08$vvGMrnukU0HVjgGyGJZ7S.NDRHOeT041HCZ4zPJfHwvRKb9vS9e8e', 'madudesu@yahoo.co.jp', '0', 0, 0, NULL, NULL, NULL, NULL, 'a760a6621811e17c1a0a92773ab8eb6a', '172.25.139.65', '0000-00-00 00:00:00', '2016-09-20 21:09:20', '2016-09-20 14:09:20', 0, 0),
(26, 'Madu', '$2a$08$xTS.UfNtLks1yUjdR5b7Ges.W/Rjv/nDSGrKTA/yu08ZKMm0..aPO', '2StaffIBK.Ga@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, '88823edf2cfca4dca5c5165a5c420338', '172.25.138.110', '2016-09-26 15:47:50', '2016-09-22 13:10:20', '2016-09-26 08:47:50', 0, 0),
(28, 'pt.av25', '$2a$08$3Y/tAm0r.33hqJP6.GgIPONrvDiIZ3wDpfLpvnLmHtRwhZCgGwV/a', 'wsn.arimurti@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, 'da5d6cdd45b741fd6ef745dd60aa7ede', '172.25.138.51', '0000-00-00 00:00:00', '2016-09-22 15:40:06', '2016-09-22 08:40:06', 12, 0),
(29, 'srctest', '$2a$08$doNfqw02zHSkkF12lU3hie8VjCAxNBQEX6G5JfP7rNrYujjoTp8hq', 'asdasd@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, 'f718ffc4b1a9d7e7dc90d3ceb41d85ce', '172.25.139.91', '2016-09-23 09:28:35', '2016-09-07 16:43:06', '2016-10-17 02:45:36', 0, 0),
(30, 'pt.ae70', '$2a$08$BkAirDsRJzxbES/aNnD5GONE.1VlncmEV.4Niif2fx8C1AUKWIvHy', 'muhammad.hakim@asyst.co.id', '0', 1, 0, NULL, NULL, NULL, NULL, 'dac420fdac01b117dfc6ac88a1dc483f', '172.25.138.183', '2016-10-18 15:34:02', '2016-09-27 14:52:59', '2016-10-18 08:34:02', 28, 0),
(31, 'wildantest', '$2a$08$Ej71QBtE8KL1SIvD52HvUu8yEyoSjIRJJvPvXD5GVEgj7t5iA2u3K', 'wabimantoro@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, '36831bd1006cf454baa2358a355e92e7', '172.25.139.210', '2016-09-28 20:46:15', '2016-09-07 16:45:51', '2016-10-17 02:45:43', 0, 0),
(32, 'pt.ve53', '$2a$08$SuzfbqesTA6s.3L9TvLuFewFPSRr9tzKomV5x7mU.3O.1uhWx1E3W', 'sr.madusari@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, '86e2980d373b031c65ce8d27028738eb', '172.25.139.132', '2016-10-18 13:58:43', '2016-09-28 16:02:11', '2016-10-18 06:58:43', 14, 0),
(33, 'superadmin', '$2a$08$Oi.Hi4dYqgvWkmTV.1tE2.9XDSseV/OQ8KpZNOYABtCkL5pNC2OHe', 'superadmin@gmail.com', '0', 1, 0, NULL, NULL, NULL, NULL, 'a1fc8c942fe42742e577480cd9141199', '172.25.138.179', '2016-10-06 14:45:41', '2016-10-06 14:21:06', '2016-10-06 07:45:41', 0, 5),
(36, 'pted54', '$2a$08$a/Kq7K03GSDz9He09zMQ8enmuZmyf7DICbCCn1ISiEGd/HPhI5lti', 'wisnu.arimurti@asyst.co.id', '0', 1, 0, NULL, NULL, NULL, NULL, 'aaca81753b183b87c563b14f11daad7f', '172.25.139.59', '2016-10-13 14:43:20', '2016-10-13 14:40:49', '2016-10-13 07:43:20', 73, 0),
(38, 'anggun', '$2a$08$Fp4y2I8I0BxlWD2bOziBCePhZiN5.XexlM26YbRdbsB.FUqaA.uD2', '-', '0', 1, 0, NULL, NULL, NULL, NULL, '32513a67d42466bab9f8448eccbf10c4', '172.25.139.120', '2016-10-14 18:55:37', '2016-10-14 18:55:16', '2016-10-17 03:42:15', 0, 4),
(39, 'ptas24', '$2a$08$ydDmW/fh9gL373utiKPDzuBKr9DOK.nsvGK.jQqAw9qDGAcJFC6SS', 'anggun.wibowo@asyst.co.id', '0', 1, 0, NULL, NULL, NULL, NULL, 'cd8224d6db8c2b96c4c37d288153ebbe', '172.25.138.183', '2016-10-18 15:51:20', '2016-10-17 09:29:52', '2016-10-18 08:51:20', 101, 0),
(41, 'cobaa', '$2a$08$JKekjSvQ1ZAdBmc2gdFOT.bC3yA0FvhXzO2.bqbNcJ4GJN.GKsKoW', 'siti.madusari@asyst.co.ids', '0', 1, 0, NULL, NULL, NULL, NULL, '7054bc4453575c0bc6e56a4f99125628', '172.25.138.175', '2016-10-18 20:55:14', '2016-10-18 20:42:49', '2016-10-18 14:29:05', 0, 0),
(42, 'pt.re2', '$2a$08$6qDk3kAgJW1C7n2.cOIvxu/PtdYmpA04H74sWqtC5Y/O423/oDEPW', 'srmadusari@naver.com', '0', 1, 0, NULL, NULL, NULL, NULL, '7ea7f1898a7cd2aff850651cb754b282', '172.25.139.132', '0000-00-00 00:00:00', '2016-10-18 21:25:14', '2016-10-18 14:25:14', 110, 0),
(43, 'pt.re8', '$2a$08$jHrC0gWsn2/10Sk6QG7L5OXWjXNLHXJdrsWkphrO3yqG3LTRKqN.O', 'siti.madusari@asyst.co.id', '0', 1, 0, NULL, NULL, NULL, NULL, '0bdccec03513ffbc31cec17a0af7ba72', '172.25.139.132', '0000-00-00 00:00:00', '2016-10-18 21:29:15', '2016-10-18 14:29:15', 113, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 11, 1, '2016-09-07 09:16:09', '2016-09-07 09:16:12'),
(2, 6, 2, '2016-09-07 16:43:25', '2016-09-07 16:43:25'),
(3, 5, 3, '2016-09-07 16:46:12', '2016-09-07 16:46:12'),
(4, 4, 4, '2016-09-07 16:47:59', '2016-09-07 16:47:59'),
(5, 10, 5, '2016-09-07 16:52:12', '2016-09-07 16:52:12'),
(6, 7, 6, '2016-09-07 16:53:11', '2016-09-07 16:53:11'),
(7, 13, 7, '2016-09-07 16:54:29', '2016-09-07 16:54:29'),
(8, 14, 8, '2016-09-07 16:58:16', '2016-09-07 16:58:16'),
(9, 16, 9, '2016-09-08 08:27:18', '2016-09-08 08:27:18'),
(10, 8, 2, '2016-09-08 16:13:38', '2016-09-08 16:13:38'),
(11, 8, 2, '2016-09-09 18:02:14', '2016-09-09 18:02:14'),
(12, 21, 12, '2016-09-14 14:00:43', '2016-10-03 10:04:43'),
(13, 21, 13, '2016-09-14 14:04:55', '2016-09-14 14:04:55'),
(14, 22, 14, '2016-09-14 14:13:38', '2016-09-14 14:19:42'),
(15, 16, 15, '2016-09-14 18:43:12', '2016-09-14 18:43:12'),
(16, 8, 2, '2016-09-14 20:39:20', '2016-09-14 20:39:20'),
(19, 8, 19, '2016-09-20 13:34:05', '2016-09-20 13:34:05'),
(20, 16, 20, '2016-09-20 15:02:57', '2016-09-20 15:02:57'),
(21, 8, 2, '2016-09-20 21:06:29', '2016-09-20 21:06:29'),
(22, 8, 2, '2016-09-20 21:06:36', '2016-09-20 21:06:36'),
(23, 8, 2, '2016-09-20 21:06:48', '2016-09-20 21:06:48'),
(24, 8, 2, '2016-09-20 21:08:36', '2016-09-20 21:08:36'),
(25, 8, 2, '2016-09-20 21:09:20', '2016-09-20 21:09:20'),
(26, 21, 1, '2016-09-22 13:10:20', '2016-09-22 13:10:20'),
(27, 7, 26, '2016-09-22 13:11:50', '2016-10-12 10:07:48'),
(28, 8, 2, '2016-09-22 15:40:06', '2016-09-22 15:40:06'),
(29, 6, 2, '2016-09-07 16:43:25', '2016-09-07 16:43:25'),
(30, 8, 2, '2016-09-27 14:52:59', '2016-09-27 14:52:59'),
(31, 5, 2, '2016-09-28 15:50:29', '2016-09-28 15:50:29'),
(32, 8, 2, '2016-09-28 16:02:11', '2016-09-28 16:02:11'),
(33, 11, 33, '2016-10-06 14:43:24', '2016-10-06 14:43:24'),
(34, 2, 34, '2016-10-12 10:31:16', '2016-10-12 10:32:06'),
(35, 6, 1, '2016-10-12 11:19:21', '2016-10-12 11:19:21'),
(36, 8, 2, '2016-10-13 14:40:49', '2016-10-13 14:40:49'),
(37, 6, 37, '2016-10-13 16:56:07', '2016-10-13 16:56:07'),
(38, 2, 1, '2016-10-14 18:55:16', '2016-10-14 18:55:16'),
(39, 8, 2, '2016-10-17 09:29:52', '2016-10-17 09:29:52'),
(40, 23, 1, '2016-10-18 20:27:33', '2016-10-18 20:27:33'),
(41, 24, 40, '2016-10-18 20:42:49', '2016-10-18 20:42:49'),
(42, 8, 2, '2016-10-18 21:25:14', '2016-10-18 21:25:14'),
(43, 8, 2, '2016-10-18 21:29:15', '2016-10-18 21:29:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `user_autologin`
--

INSERT INTO `user_autologin` (`key_id`, `user_id`, `user_agent`, `last_ip`, `last_login`) VALUES
('01125c95a18483ad4a786e974c9e95c8', 25, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '172.25.118.136', '2016-08-07 15:28:42'),
('079494f3915c5ce33f04348dfb5d6665', 1, 'Mozilla/5.0 (Windows NT 6.1; rv:45.0) Gecko/20100101 Firefox/45.0', '172.25.135.56', '2016-05-20 08:06:19'),
('0aa71c83784f635e65cae848ea149102', 1, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0', '192.168.9.20', '2016-07-18 07:54:15'),
('104a851e66ee84bd9987e8eda6853871', 1, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '192.168.9.185', '2016-05-20 07:05:51'),
('167b83e29339f4b0040f5db7906210f9', 1, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0', '192.168.215.137', '2016-06-22 08:03:21'),
('1fadb286d95aa6c92cc807d0b8f29b5d', 17, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0', '172.25.135.162', '2016-08-10 03:41:53'),
('2dbd498727974c9f9e4053b8b9d43589', 17, 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '172.25.135.83', '2016-08-04 01:06:58'),
('30ff81006f487e82673e43a346ada35e', 1, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '172.25.135.35', '2016-07-28 03:13:52'),
('34951a7fdae71d2cd0703b07985e196d', 1, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '172.25.135.103', '2016-08-09 01:01:24'),
('3a6f464be9a9c745c81f78d363e3819e', 1, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0', '192.168.205.253', '2016-06-03 04:03:37'),
('3e50ef53a69e3c718824370b5dae53ab', 1, 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0', '172.25.135.232', '2016-08-09 08:20:35'),
('49bbd4f1d8a62dda613461dc31ee4674', 20, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '172.25.135.35', '2016-07-28 06:33:18'),
('49d6979174671257dcfc436c54a88338', 14, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '172.25.135.100', '2016-08-12 03:57:15'),
('52f790022e3acaf7a8b52662c69ccefc', 11, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '172.25.135.225', '2016-08-10 00:55:36'),
('5921f957d65112bc49982ccbade54bfe', 12, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:42.0) Gecko/20100101 Firefox/42.0', '172.25.138.232', '2016-08-29 13:37:47'),
('5aa41e720795d3b58dd5e655fd658214', 1, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0', '192.168.9.138', '2016-05-20 07:01:33'),
('60fc20cccaaddf2180d53620ba01457d', 1, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', '172.25.220.221', '2016-05-25 00:29:44'),
('66c79d3c8933da9ce64c1c9cf5c34375', 17, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '172.25.139.44', '2016-08-26 02:44:36'),
('6ddbcb85eea1a3c508bd189da2df6ae0', 4, 'Mozilla/5.0 (Windows NT 6.1; rv:48.0) Gecko/20100101 Firefox/48.0', '172.25.135.104', '2016-07-26 02:59:18'),
('77b26d731fc02fd37b0edb9a7389661a', 1, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:48.0) Gecko/20100101 Firefox/48.0', '172.25.220.221', '2016-08-25 07:19:20'),
('7f14801976d4792b6d3078f4089d8ba2', 17, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0', '192.168.244.178', '2016-08-10 01:50:26'),
('85e935aab350239ef3696ea1a3e3ab25', 15, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '172.25.135.194', '2016-07-14 01:44:10'),
('8cad0cc49aa930f2a0ba3132b851b5b0', 17, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:42.0) Gecko/20100101 Firefox/42.0', '192.168.244.143', '2016-08-10 01:07:02'),
('9159292a1fe9e81e2940bbfb6d0d0c7c', 1, 'Mozilla/5.0 (Windows NT 6.1; rv:41.0) Gecko/20100101 Firefox/41.0', '172.25.135.161', '2016-08-10 06:51:14'),
('947b5ae7d20099d23aae8296a550ad55', 17, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0', '192.168.9.208', '2016-08-11 03:36:57'),
('94960a6e508d1fc7640d42377f0e124c', 17, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '172.25.135.225', '2016-08-10 00:47:52'),
('97c2fd9434d143b6917e54f0dcc22748', 14, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0', '192.168.201.249', '2016-08-08 07:45:51'),
('9a5b2da0641d300676ae611e3664672c', 17, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '172.25.135.65', '2016-08-04 01:02:05'),
('a78dd5861c6808196168652c9772575a', 17, 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.2.2743.82 Safari/537.36', '172.25.220.221', '2016-08-09 01:38:54'),
('ac1745128f5e53b5f6e84b880d9f07a3', 1, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0', '192.168.186.15', '2016-07-20 10:00:50'),
('ac67f6bbf2aff49b35db7d67d3d50dca', 40, 'Mozilla/5.0 (Windows NT 6.1; rv:46.0) Gecko/20100101 Firefox/46.0', '172.25.135.125', '2016-08-09 19:26:36'),
('ace0d60d854291bfcd80370744814d6e', 12, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '172.25.135.122', '2016-08-08 04:11:39'),
('af550e4fa6ab6b2a8202830f474f3fc2', 1, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', '192.168.186.46', '2016-06-07 06:56:33'),
('c041e715922a18277d51192ae8e36172', 1, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20120101 Firefox/33.0', '172.25.135.161', '2016-08-10 07:17:46'),
('c75507a8d29a2782130cbb3ae1b4ba2e', 17, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '172.25.135.135', '2016-08-05 08:08:26'),
('d968dda5239a3c060865ea5b9bbf9c51', 24, 'Mozilla/5.0 (Windows NT 6.1; rv:48.0) Gecko/20100101 Firefox/48.0', '172.25.118.151', '2016-08-07 15:24:47'),
('e0d247bc986feb050c84dca685693b2d', 17, 'Mozilla/5.0 (Windows NT 6.1; rv:49.0) Gecko/20100101 Firefox/49.0', '172.25.220.221', '2016-08-30 02:34:40'),
('e12ee21614d8d4104431cd8f12906344', 12, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '192.168.205.189', '2016-07-26 08:34:48'),
('e387e8ef7b7301be2fa8e2b34a3a2661', 1, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '172.25.135.106', '2016-07-27 03:15:01'),
('e9dafba7311d3047195df786ba908a42', 13, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', '172.25.220.221', '2016-08-09 18:38:46'),
('f0a6f2bd3ebe76ecb8902ccdff7ee0f2', 1, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0', '192.168.9.198', '2016-06-27 03:44:03'),
('f37fba81bfd403a39f1cc7ec92ed2883', 14, 'Mozilla/5.0 (Windows NT 6.1; rv:46.0) Gecko/20100101 Firefox/46.0', '172.25.135.125', '2016-08-09 19:53:55'),
('f38b85c029125469d22a7d40af9c25cc', 17, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0', '172.25.135.134', '2016-08-05 03:18:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `country`, `website`) VALUES
(1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor`
--

CREATE TABLE IF NOT EXISTS `vendor` (
`id` smallint(5) unsigned NOT NULL,
  `register_num` varchar(20) NOT NULL,
  `id_stsreg` smallint(1) NOT NULL DEFAULT '1',
  `approval` enum('4','3','2','1','0') NOT NULL DEFAULT '0',
  `vendor_type` enum('new','reject','shortlist','avl') NOT NULL DEFAULT 'new',
  `vendor_num` varchar(20) NOT NULL DEFAULT '',
  `vendor_name` varchar(40) NOT NULL,
  `vendor_address` varchar(50) NOT NULL,
  `postcode` varchar(15) DEFAULT NULL,
  `phone` varchar(30) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `overseas` enum('1','0') NOT NULL DEFAULT '0',
  `npwp` varchar(20) NOT NULL,
  `npwp_address` varchar(50) DEFAULT NULL,
  `npwp_postcode` varchar(15) DEFAULT NULL,
  `web` varchar(50) DEFAULT NULL,
  `status` enum('blacklist','redlist','active','inactive') NOT NULL DEFAULT 'active',
  `created_id` smallint(5) NOT NULL DEFAULT '0',
  `approved_date` datetime DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=114 ;

--
-- Dumping data untuk tabel `vendor`
--

INSERT INTO `vendor` (`id`, `register_num`, `id_stsreg`, `approval`, `vendor_type`, `vendor_num`, `vendor_name`, `vendor_address`, `postcode`, `phone`, `fax`, `email`, `overseas`, `npwp`, `npwp_address`, `npwp_postcode`, `web`, `status`, `created_id`, `approved_date`, `created_date`, `last_updated`) VALUES
(2, 'SR16000002', 7, '4', 'avl', '10060', 'PT. Vendor', 'Lebak Bulus', NULL, '085720519000', '123254', 'wisnu.arimurti@asyst.co.id', '0', '1234435', NULL, NULL, NULL, 'redlist', 0, '2016-09-09 00:00:00', '2016-09-08 10:14:56', '2016-09-08 16:13:38'),
(3, 'SR16000003', 7, '4', 'avl', '10061', 'PT. Tukang Kompor', 'Pamulang', NULL, '02198765432', '02134567832', 'ditya.firmansyah@garuda-indonesia.com', '0', '9887766532422', NULL, NULL, NULL, 'blacklist', 0, '2016-10-14 20:06:44', '2016-09-09 15:42:03', '2016-09-16 09:09:17'),
(5, 'SR16000005', 5, '1', 'reject', '', 'PT. Sejahtera Abadi', 'Bandung', NULL, '0215503170', '0215502135', 'anj4r.7070@gmail.com', '0', '909080801011', NULL, NULL, NULL, 'active', 2, '2016-09-15 18:52:04', '2016-09-09 18:53:22', '2016-09-15 18:52:04'),
(6, 'SR16000004', 7, '4', 'avl', '10062', 'PT ASI', 'Jalan Pembangunan I', '150650', '081295057000', '0246701644', 'wisnu.arimurti@asyst.co.id', '0', '756718129377AF', 'Jalan Pembangunan', '8475891', 'www.garuda-indonesia.com', 'redlist', 2, '2016-09-14 20:39:20', '2016-09-14 10:35:15', '2016-09-14 20:39:20'),
(10, 'SR16000005', 4, '0', 'new', '', 'PT ADES (AVL-INACTIVE)', 'AVL Inactive', NULL, '0818575849', '0246701644', 'wsn.arimurti@gmail.com', '1', '6583890', NULL, NULL, NULL, 'active', 0, '2016-10-14 20:06:44', '2016-09-16 10:13:18', '2016-09-22 11:23:40'),
(11, 'SR16000006', 5, '1', 'new', '', 'PT BENG2', 'Vendor ini digunakan untuk mengetes flow detil AVL', NULL, '08359375', '83524759', 'wisnu.arimurti@asyst.co.id', '0', '35928729357', NULL, NULL, NULL, 'active', 0, '2016-10-14 20:06:44', '2016-09-16 10:36:22', '2016-09-30 10:44:36'),
(12, 'SR16000007', 7, '4', 'avl', '10069', 'PT. AVL INACTIVE', 'Jl. Pergundangan 5', NULL, '02199887766', '66554433', 'wsn.arimurti@gmail.com', '0', '123234345', NULL, NULL, NULL, 'inactive', 0, '2016-10-14 20:06:44', '2016-09-16 13:56:35', '2016-09-22 15:40:06'),
(13, 'SR16000008', 3, '0', 'new', '', 'COBA SUM TOTAL', 'afd', NULL, '235534', '546456', 'richo.mahardika@asyst.co.id', '0', '123542357289', NULL, NULL, NULL, 'active', 0, '2016-10-14 20:06:44', '2016-09-16 14:23:16', '2016-09-28 09:48:11'),
(14, 'SR16000009', 8, '4', 'avl', '10074', 'PT. Vendor Jaya (AVL-REDLIST)', 'Jalan Address', NULL, '02158456', '02154215', 'sr.madusari@gmail.com', '0', '1234567890', NULL, NULL, NULL, 'redlist', 0, '2016-09-28 16:02:11', '2016-09-19 16:36:02', '2016-09-28 16:02:11'),
(15, 'SR16000010', 7, '4', 'avl', '10069', 'PT. Vendor Jaya (AVL-ACTIVE-1)', 'Jl. Kalijati 7', NULL, '021837366281', '0219384746', 'ririn.zulandra@asyst.co.id', '0', '99809776', NULL, NULL, NULL, 'redlist', 2, '2016-10-14 20:06:44', '2016-09-20 10:34:46', '2016-09-22 15:31:06'),
(17, 'SR16000012', 1, '4', 'avl', '', 'GGGGGGGGGGGG', 'kjhjkhkj', '123456', '90898908', '78978978', 'ririn.zulandra@asyst.co.id', '0', '123456', 'Jakarta', '123456', 'www.avl.com', 'blacklist', 19, '2016-10-14 20:06:44', '2016-09-20 13:27:23', '2016-10-18 16:00:11'),
(18, 'AR16000012', 3, '0', 'new', '', 'PT. TOP', 'Jl. Kuningan', 'JFY', 'JJHGK', '021834939', 'WSN.ARIMURTI@GMAIL.COM', '0', '9808034534', 'GKJG', ',J,JH', 'JFLQHF', 'active', 2, '2016-10-14 20:06:44', '2016-09-20 14:19:32', '2016-09-28 14:02:14'),
(21, 'SR16000015', 7, '4', 'avl', '10064', 'PT INACTIVE', 'EFGQLJ', NULL, '357823075', '52745897', 'MADUSAMA@HOTMAIL.COM', '1', '6528765', NULL, NULL, NULL, 'inactive', 0, '2016-10-14 20:06:44', '2016-09-20 20:12:30', '2016-09-20 21:06:48'),
(22, 'SR16000016', 7, '4', 'avl', '10064', 'PT BLACKLIST', 'EIJHFLIEQHJ', 'KITA TAMBAHKAN ', '87523895', '827589', 'sr.madusari@gmail.com', '0', '52362', 'JALAN PEMBANGUNAN', '746582765', 'COBA EDIT VENDOR BY ADMIN', 'blacklist', 2, '2016-10-14 20:06:44', '2016-09-20 20:18:37', '2016-10-12 11:20:19'),
(23, 'SR16000017', 7, '4', 'avl', '10064', 'PT REDLIST', 'WRHQH', NULL, '289675892', '5874589', 'sr.madusari@gmail.com', '1', '65782', NULL, NULL, NULL, 'redlist', 0, '2016-10-14 20:06:44', '2016-09-20 20:20:25', '2016-09-20 21:09:20'),
(26, 'AR16000018', 6, '2', 'shortlist', '10069', 'PT SHORTLIST (AR)', 'DKGAKJ', '1', '6248972', '475897348', 'wisnu.arimurti@asyst.co.id', '1', '523592378', '1', '1', '1', 'active', 2, '2016-09-23 09:46:43', '2016-09-21 11:02:01', '2016-09-23 09:46:43'),
(27, 'AR16000019', 3, '0', 'new', '', 'PT Baru (Add by Admin)', 'karawang', NULL, '02139870', '09865432', 'baru@kantor.com', '0', '21234555666', NULL, NULL, NULL, 'active', 2, '2016-10-14 20:06:44', '2016-09-27 08:50:58', '2016-09-28 10:05:32'),
(28, 'SR16000020', 8, '4', 'avl', '10073', 'PT. Aero System', 'Cengkareng', NULL, '02134556780', '02134456701', 'muhammad.hakim@asyst.co.id', '0', '90878786787', NULL, NULL, NULL, 'active', 0, '2016-09-27 14:52:59', '2016-09-27 14:21:50', '2016-09-27 14:52:59'),
(29, 'SO16000021', 1, '0', 'new', '', 'rr', '1', NULL, '1', '1', 'richo.m17@gmail.com', '0', '1', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-27 17:02:25', '2016-09-27 17:02:25'),
(30, 'SO16000022', 1, '0', 'reject', '', 'PT. abrakadabra', 'jl. Pekalipan', NULL, '200020020220', '020202020202', 'wisnu.arimurti@asyst.co.id', '0', '321312232', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-28 08:28:25', '2016-10-03 09:29:01'),
(31, 'SO16000023', 1, '0', 'new', '', 'PT. Sarung Tenun', 'jl. Pekalipan', NULL, '200020020220', '020202020202', 'kalibangka@gmail.com', '0', '2323231', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-28 08:30:39', '2016-09-28 08:30:39'),
(32, 'SO16000024', 1, '0', 'new', '', 'PT CSL', 'Kemang', NULL, '0224585522', '024554', 'zulandraririn@gmail.com', '0', '25478852', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-28 15:31:22', '2016-09-28 15:31:22'),
(33, 'SO16000025', 4, '1', 'new', '', 'PT CSL', 'Kemang', NULL, '161545', '445', 'zulandraririn@gmail.com', '0', '534356454', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-28 15:32:52', '2016-10-03 09:58:58'),
(34, 'SO16000026', 5, '1', 'new', '', '1', '1', NULL, '1', '1', 'wisnu.arimurti@asyst.co.id', '0', '1234567', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-28 15:51:27', '2016-10-03 09:56:22'),
(35, 'SO16000027', 5, '1', 'new', '', '1', '1', NULL, '1', '1', 'wisnu.arimurti@asyst.co.id', '0', '45664567876567', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-28 16:01:51', '2016-10-03 09:55:30'),
(36, 'SO16000028', 5, '1', 'new', '', '1', '1', NULL, '1', '1', 'wisnu.arimurti@asyst.co.id', '0', '87787656764567', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-28 16:09:45', '2016-10-03 09:54:29'),
(37, 'SO16000029', 5, '1', 'new', '', '2', '2', NULL, '2', '2', 'wisnu.arimurti@asyst.co.id', '0', '2', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-28 16:23:51', '2016-10-03 09:53:33'),
(38, 'SO16000030', 5, '1', 'new', '', '33', '33', NULL, '33', '33', 'wisnu.arimurti@asyst.co.id', '0', '765675678765', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-28 16:26:07', '2016-10-03 09:52:07'),
(39, 'SO16000031', 5, '1', 'new', '', '44', '44', NULL, '44', '44', 'wisnu.arimurti@asyst.co.id', '0', '445456675677', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-28 16:31:28', '2016-10-03 09:50:17'),
(40, 'SO16000032', 4, '1', 'new', '', '77', '77', NULL, '77', '77', 'wisnu.arimurti@asyst.co.id', '0', '77', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-28 16:40:39', '2016-10-03 10:15:01'),
(41, 'SO16000033', 1, '0', 'new', '', 'PT OCS', 'sarinah', NULL, '0252415', '3585852', 'zulandraririn@gmail.com', '0', '02155874', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-29 10:03:39', '2016-09-29 10:03:39'),
(42, 'SR16000034', 4, '0', 'new', '', 'PT DIMO', 'Sarinah', NULL, '0255111', '36354102', 'zulandraririn@gmail.com', '0', '021541', NULL, NULL, NULL, 'active', 0, '2016-10-14 20:06:44', '2016-09-29 10:32:35', '2016-10-11 13:52:37'),
(43, 'SR16000035', 5, '4', 'new', '', 'PT. Redlist 02', 'Tangerang', NULL, '085322344512', '021232454', 'siti.madusari@gmail.com', '0', '245673565798', NULL, NULL, NULL, 'active', 0, '2016-10-14 20:06:44', '2016-09-30 11:30:24', '2016-09-30 11:39:32'),
(44, 'AR16000036', 6, '4', 'new', '', 'PT. Vendor Redlist', 'Cengkareng', NULL, '08213452455', '021354235', 'siti.madusari@asyst.co.id', '0', '133235354212', NULL, NULL, NULL, 'active', 2, '2016-10-14 20:06:44', '2016-09-30 11:46:07', '2016-10-05 11:08:36'),
(45, 'SO16000037', 1, '0', 'new', '', 'PT ABC', 'Kemang', NULL, '4234234', '34324', 'zulandraririn@gmail.com', '0', '413423412', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-09-30 14:43:50', '2016-09-30 14:43:50'),
(46, 'BD16000038', 1, '0', 'new', '', 'PT Asyst', 'Tangerang', NULL, '1154441', '4144411', 'zulandraririn@gmail.com', '0', '2222222', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-03 09:31:50', '2016-10-03 09:31:50'),
(47, 'SO16000039', 1, '0', 'new', '', 'PT GHI', 'Depok', NULL, '634534', '2345346', 'zulandraririn@gmail.com', '0', '4123537', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-03 10:28:11', '2016-10-03 10:28:11'),
(48, 'BD16000040', 5, '2', 'new', '', 'PT. abrakadabra', 'jl. Pekalipan', NULL, '200020020220234', '2342', 'kalibangka@gmail.com', '0', '321312232232', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-03 11:21:55', '2016-10-05 11:22:45'),
(49, 'SO16000041', 1, '0', 'new', '', 'PT ada deh', 'Kemang', NULL, '3143524', '13423524', 'zulandraririn@gmail.com', '0', '42314', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-03 11:35:21', '2016-10-03 11:35:21'),
(50, 'SO16000042', 1, '0', 'new', '', 'PT ada deh', 'Kemang', NULL, '134143', '431241', 'zulandraririn@gmail.com', '0', '5123412', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-03 11:35:59', '2016-10-03 11:35:59'),
(51, 'SO16000043', 3, '0', 'new', '', 'PT. SarungTenun', 'jl. Pekalipan', NULL, '200020020220', '020202020202', 'kalibangka@gmail.com', '0', '34565767', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-03 11:38:59', '2016-10-14 15:55:50'),
(52, 'SO16000044', 4, '0', 'new', '10073', 'PT. abrakadabraasdasasd', 'jl. Pekalipan', NULL, '200020020220', '020202020202', 'kalibangka@gmail.com', '0', '3422349923223', '', '', '', 'active', 2, '2016-10-14 20:06:44', '2016-10-18 08:56:47', '2016-10-18 08:56:47'),
(53, 'SO16000045', 5, '2', 'shortlist', '', 'PT. Sarung Sampah', 'jl. Pekalipan', NULL, '1', '2342', 'kalibangka@gmail.com', '0', '233232323222222', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-03 13:35:31', '2016-10-11 09:33:19'),
(54, 'SO16000046', 5, '4', 'new', '', 'Acd', 'Kemang', '', '32656', '6564', 'wsn.arimurti@gmail.com', '0', '216525', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-03 13:36:27', '2016-10-04 10:45:42'),
(55, 'SO16000047', 5, '4', 'new', '', 'PT. Testing', 'Kemang', '', '232432542', '34243252', 'zulandraririn@gmail.com', '0', '1453656353', '', '', 'pempekpalembang.com', 'active', 0, '2016-10-14 20:06:44', '2016-10-03 14:47:33', '2016-10-03 15:37:04'),
(56, 'SR16000048', 4, '0', 'reject', '', 'PT NGETES DI SERVER 180', 'JAYA', NULL, '35897238957', '380561873590', 'wisnu.arimurti@asyst.co.id', '1', '34234', NULL, NULL, NULL, 'active', 0, '2016-10-14 20:06:44', '2016-10-06 08:10:25', '2016-10-11 09:26:28'),
(57, 'LB16000049', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'inactive', 0, '2016-10-14 20:06:44', '2016-10-06 13:32:31', '2016-10-06 13:32:31'),
(58, 'LB16000050', 1, '0', 'new', '', '-', '', '', '', '', 'ririn.zulandra@asyst.co.id', '0', '', '', '', '', 'inactive', 0, '2016-10-14 20:06:44', '2016-10-06 13:32:31', '2016-10-06 13:32:31'),
(59, 'LB16000051', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'inactive', 0, '2016-10-14 20:06:44', '2016-10-06 13:32:31', '2016-10-06 13:32:31'),
(60, 'LB16000052', 1, '0', 'new', '', '-', '', '', '', '', 'ririn.zulandra@asyst.co.id', '0', '', '', '', '', 'inactive', 0, '2016-10-14 20:06:44', '2016-10-06 13:32:31', '2016-10-06 13:32:31'),
(61, 'SR16000053', 7, '4', 'new', '', 'PT. Pancasila', 'Jakarta Selatan', NULL, '021 8234283', '021 8234283', 'watsap1.ga@gmail.com', '0', '12345678910', NULL, NULL, NULL, 'active', 0, '2016-10-14 20:06:44', '2016-10-06 14:02:43', '2016-10-10 10:18:16'),
(62, 'BD16000054', 2, '0', 'new', '', '1', '1', NULL, '1', '1', '1@1.com', '0', '76556745678987654', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-07 13:08:59', '2016-10-13 18:59:57'),
(63, 'SO16000055', 1, '0', 'new', '', 'PT Testing2', 'Kemang', NULL, '978428', '30482', 'zulandraririn@gmail.com', '0', '99999999', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-10 09:47:44', '2016-10-10 09:47:44'),
(64, 'SO16000056', 4, '0', 'new', '', 'PT ABCD', 'Cengkareng', NULL, '245356', '563654', 'zulandraririn@gmail.com', '0', '9756867', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-10 10:03:52', '2016-10-12 07:37:10'),
(65, 'SR16000055', 4, '0', 'reject', '', 'PT. Sinar Sosro', 'Jakarta Selatan', NULL, '021 8234283', '021 8234283', 'gunwbw@gmail.com', '0', '231123123123', NULL, NULL, NULL, 'active', 0, '2016-10-14 20:06:44', '2016-10-10 15:11:36', '2016-10-11 09:40:18'),
(66, 'SR16000056', 5, '4', 'new', '', 'PT Plengkung', 'M1', NULL, '8769843763', '798376983', 'd.setiawan@garuda-indonesia.com', '0', '842769874326', NULL, NULL, NULL, 'active', 0, '2016-10-14 20:06:44', '2016-10-11 15:27:41', '2016-10-11 16:03:21'),
(67, 'LB16000057', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-12 07:45:24', '2016-10-12 07:45:24'),
(68, 'BD16000058', 5, '4', 'new', '', 'abcde', 'Kemang', NULL, '123545', '43223', 'zulandraririn@gmail.com', '0', '942384929', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-12 08:34:25', '2016-10-13 11:31:33'),
(69, 'LB16000059', 1, '0', 'new', '', '-', '', '', '', '', 'masasih@masasih.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-12 22:07:39', '2016-10-12 22:07:39'),
(70, 'LB16000060', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 08:57:32', '2016-10-13 08:57:32'),
(71, 'LB16000061', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 09:03:13', '2016-10-13 09:03:13'),
(72, 'LB16000062', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com ', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:32:31', '2016-10-13 13:32:31'),
(73, 'SR16000063', 8, '4', 'avl', '10074', 'PT VENDOR NYA MAU NGEDIT PROFILE SENDIRI', 'JALAN PEMBANGUNAN JAYA 1', '15060', '0328765427', '385727', 'WISNU.ARIMURTI@ASYST.CO.ID', '0', '2384091384981', 'PAMULANG JAYA', '15417', 'GARUDA-INDONESIA.COM', 'active', 36, '2016-10-13 14:40:49', '2016-10-13 13:33:18', '2016-10-13 16:01:51'),
(74, 'LB16000064', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:35:20', '2016-10-13 13:35:20'),
(75, 'LB16000065', 1, '0', 'new', '', '-', '', '', '', '', 'ririn.zulandra@asyst.co.id', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:35:20', '2016-10-13 13:35:20'),
(76, 'LB16000066', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:41:00', '2016-10-13 13:41:00'),
(77, 'LB16000067', 1, '0', 'new', '', '-', '', '', '', '', 'ririn.zulandra@asyst.co.id', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:41:00', '2016-10-13 13:41:00'),
(78, 'LB16000068', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:41:47', '2016-10-13 13:41:47'),
(79, 'LB16000069', 1, '0', 'new', '', '-', '', '', '', '', 'ririn.zulandra@asyst.co.id', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:41:47', '2016-10-13 13:41:47'),
(80, 'LB16000070', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:43:12', '2016-10-13 13:43:12'),
(81, 'LB16000071', 1, '0', 'new', '', '-', '', '', '', '', 'ririn.zulandra@asyst.co.id', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:43:12', '2016-10-13 13:43:12'),
(82, 'LB16000072', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:44:27', '2016-10-13 13:44:27'),
(83, 'LB16000073', 1, '0', 'new', '', '-', '', '', '', '', 'ririn.zulandra@asyst.co.id', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:44:27', '2016-10-13 13:44:27'),
(86, 'LB16000076', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:46:54', '2016-10-13 13:46:54'),
(87, 'LB16000077', 1, '0', 'new', '', '-', '', '', '', '', 'ririn.zulandra@asyst.co.id', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:46:54', '2016-10-13 13:46:54'),
(88, 'LB16000078', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:47:02', '2016-10-13 13:47:02'),
(89, 'LB16000079', 1, '0', 'new', '', '-', '', '', '', '', 'ririn.zulandra@asyst.co.id', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 13:47:02', '2016-10-13 13:47:02'),
(94, 'LB16000081', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-13 14:15:48', '2016-10-13 14:15:48'),
(95, 'SR16000081', 1, '0', 'new', '', '1', '1', NULL, '1', '1', '1@1.com', '0', '87776787', NULL, NULL, NULL, 'active', 0, '2016-10-14 20:06:44', '2016-10-13 22:26:00', '2016-10-13 22:26:00'),
(96, 'LB16000082', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-14 11:28:05', '2016-10-14 11:28:05'),
(97, 'LB16000083', 1, '0', 'new', '', '-', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-14 11:28:50', '2016-10-14 11:28:50'),
(100, 'LB16000082', 1, '0', 'new', '', '-', '', '', '', '', 'limitedbidding@gmail.com', '0', '', '', '', '', 'active', 0, '2016-10-14 20:06:44', '2016-10-14 14:45:34', '2016-10-14 14:45:34'),
(101, 'SR16000083', 8, '4', 'avl', '10075', 'PT ASYST', 'jwelfjq', NULL, '2587289', '82765892', 'muhammad.hakim@asyst.co.id', '0', '525782', NULL, NULL, NULL, 'active', 0, '2016-10-17 09:29:52', '2016-10-17 09:08:10', '2016-10-17 09:29:52'),
(102, 'SO16000084', 1, '0', 'new', '', 'PT acak adut', 'Cengkareng', NULL, '3325435', '546235', 'zulandraririn@gmail.com', '0', '0 000000000000', '', '', '', 'active', 0, NULL, '2016-10-17 09:53:59', '2016-10-17 09:53:59'),
(103, 'SO16000085', 1, '0', 'new', '', 'wer', 'tgfwgf', NULL, '3254', '545454', 'zulandraririn@gmail.com', '0', '5687987', '', '', '', 'active', 0, NULL, '2016-10-17 11:33:28', '2016-10-17 11:33:28'),
(104, 'BD16000086', 1, '0', 'new', '', 'abc', 'gre', NULL, '4356', '4654', 'zulandraririn@gmail.com', '0', '5769879', '', '', '', 'active', 0, NULL, '2016-10-17 12:02:39', '2016-10-17 12:02:39'),
(105, 'SO16000087', 1, '0', 'new', '', 'aaa', 'Ga', NULL, '5234', '324', 'test@gmail.com', '0', '3245', '', '', '', 'active', 0, NULL, '2016-10-17 14:46:11', '2016-10-17 14:46:11'),
(106, 'SO16000088', 1, '0', 'new', '', 'aaaaa', 'Gaserd', NULL, '4222', '6354', 'zulandraririn@gmail.com', '0', '2354236', '', '', '', 'active', 0, NULL, '2016-10-18 10:07:13', '2016-10-18 10:07:13'),
(107, 'SO16000089', 1, '0', 'new', '', 'qqq', 'twefr', NULL, '2453', '653', 'zulandraririn@gmail.com', '0', '542436', '', '', '', 'active', 0, NULL, '2016-10-18 10:56:21', '2016-10-18 10:56:21'),
(108, 'LB16000090', 0, '0', '', '', '', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, NULL, '2016-10-18 14:18:00', '2016-10-18 14:18:00'),
(109, 'LB16000091', 0, '0', '', '', '', '', '', '', '', 'zulandraririn@gmail.com', '0', '', '', '', '', 'active', 0, NULL, '2016-10-18 14:23:54', '2016-10-18 14:23:54'),
(110, 'SR16000092', 8, '4', 'avl', '10075', 'PT. Req Blacklist', 'Sulawesi Selatan', NULL, '08222112192', '0212541625', 'srmadusari@naver.com', '0', '56462fw345t2', NULL, NULL, NULL, 'active', 0, '2016-10-18 21:25:14', '2016-10-18 18:46:30', '2016-10-18 21:25:14'),
(111, 'SR16000093', 5, '1', 'reject', '', 'PT. Req Rejected', 'Makassar', NULL, '08292342142', '234523682', 'madusama@hotmail.com', '0', '2412513', NULL, NULL, NULL, 'active', 0, '2016-10-18 21:30:58', '2016-10-18 18:52:02', '2016-10-18 21:30:58'),
(112, 'SR16000094', 5, '2', 'shortlist', '10078', 'PT. Req Shortlist', 'Karawaci', NULL, '54735234', '21524723', 'srmadusari@daum.net', '0', '352347624', NULL, NULL, NULL, 'active', 0, '2016-10-18 21:42:55', '2016-10-18 18:54:01', '2016-10-18 21:42:55'),
(113, 'SR16000095', 8, '4', 'avl', '10076', 'PT. Req Active', 'Batu Sangkar', NULL, '2141361235', '23524674', 'siti.madusari@asyst.co.id', '0', '124123mk135', NULL, NULL, NULL, 'active', 0, '2016-10-18 21:29:15', '2016-10-18 18:59:50', '2016-10-18 21:29:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_account`
--

CREATE TABLE IF NOT EXISTS `vendor_account` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `recon_number` varchar(30) DEFAULT NULL,
  `sap_number` varchar(30) DEFAULT NULL,
  `srm_username` varchar(30) DEFAULT '',
  `srm_password` varchar(50) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data untuk tabel `vendor_account`
--

INSERT INTO `vendor_account` (`id`, `id_vendor`, `recon_number`, `sap_number`, `srm_username`, `srm_password`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 2, '7890', '1234', 'test', '12345', '0', 7, '2016-09-08 15:53:21', '2016-09-08 16:06:59'),
(2, 1, '12129909', '12345', '123123', '123456', '0', 1, '2016-09-09 13:19:21', '2016-09-13 11:08:47'),
(3, 5, '1234567890', '12345678', NULL, NULL, '0', 7, '2016-09-09 19:24:23', '2016-09-13 11:09:57'),
(5, 6, '765432-IB', '76947466894-IBK', 'PT asi', '123456', '0', 7, '2016-09-14 20:29:27', '2016-09-14 20:35:54'),
(7, 23, NULL, '23', 'STAFFIBK.GA@GMAIL.com', '123456', '0', 8, '2016-09-20 21:02:44', '2016-09-20 21:04:32'),
(8, 22, NULL, '34', 'STAFFIBK.GA@GMAIL.com', '123456', '0', 8, '2016-09-20 21:02:52', '2016-09-20 21:05:22'),
(9, 21, NULL, '45', 'STAFFIBK.GA@GMAIL.com', '123456', '0', 8, '2016-09-20 21:02:59', '2016-09-20 21:05:27'),
(11, 15, 'REKON-123', 'SAP-123', 'STAFFIBK.GA@gmail.com', '123456', '0', 7, '2016-09-22 15:35:41', '2016-09-22 15:37:24'),
(12, 14, 'REKON-234', 'SAP-234', 'STAFFIBK.GA@gmail.com', '123456', '0', 7, '2016-09-22 15:35:51', '2016-09-22 15:37:28'),
(13, 12, 'REKON-345', 'SAP-345', 'STAFFIBK.GA@gmail.com', '123456', '0', 7, '2016-09-22 15:36:00', '2016-09-22 15:37:33'),
(14, 26, 'REKON-567', 'SAP-5432', 'SRM Username', '123456', '0', 7, '2016-09-23 09:41:12', '2016-09-28 08:21:34'),
(15, 28, '12345678', '12345678', 'ASI', '123456', '0', 7, '2016-09-27 14:45:35', '2016-09-27 14:52:10'),
(16, 44, '130194', '940113', NULL, NULL, '0', 7, '2016-10-05 11:20:07', '2016-10-05 11:20:40'),
(17, 61, 'REKON_XYZ_123', 'SAP Number - 1234', 'PT Testing', '123456', '0', 7, '2016-10-07 14:43:12', '2016-10-10 10:18:16'),
(18, 73, 'REKONSILIASI OLEH WA-1', 'nOMOR vENDOR saP nya', 'WISNU', '123456', '0', 7, '2016-10-13 14:10:57', '2016-10-13 14:34:31'),
(19, 101, 'rekonsiliasi-123456', 'SAP-345678', 'PTASYST.com', '123456', '0', 7, '2016-10-17 09:26:18', '2016-10-17 09:29:03'),
(20, 113, 'SAPPPP23456', '23492', '123qwe', '123qwe', '0', 7, '2016-10-18 21:13:47', '2016-10-18 21:21:17'),
(21, 110, 'SAPPPP23456', '23492', '123qwe', '123qwe', '0', 7, '2016-10-18 21:13:57', '2016-10-18 21:21:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_approval`
--

CREATE TABLE IF NOT EXISTS `vendor_approval` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `type` enum('none','reject','shortlist','avl') NOT NULL DEFAULT 'none',
  `note` varchar(100) NOT NULL,
  `level` smallint(1) NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_blacklist`
--

CREATE TABLE IF NOT EXISTS `vendor_blacklist` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `start_date` date NOT NULL,
  `remark` varchar(50) NOT NULL,
  `doc_name` text,
  `doc_type` tinytext,
  `approval` enum('0','1','2','3') NOT NULL,
  `status` varchar(10) NOT NULL COMMENT 'Wait, Approved, Cancel, Rejected',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `vendor_blacklist`
--

INSERT INTO `vendor_blacklist` (`id`, `id_vendor`, `start_date`, `remark`, `doc_name`, `doc_type`, `approval`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(2, 3, '2016-09-16', 'Bikin Panas', 'blacklist_320160916085355', 'jpg', '3', 'Approved', 2, '2016-09-16 08:53:55', '2016-09-16 09:09:17'),
(3, 22, '2016-10-07', 'Testing Blacklist', 'blacklist_2220161003132335', 'jpg', '3', 'Approved', 2, '2016-10-03 13:43:18', '2016-10-03 13:55:05'),
(4, 17, '2016-10-26', 'Req to Blacklist', '', '', '3', 'Approved', 2, '2016-10-18 15:44:36', '2016-10-18 16:00:11'),
(5, 110, '2016-10-19', 'request', 'blacklist_110201610190901221,blacklist_110201610190901222', 'jpg,jpg', '1', 'Wait', 2, '2016-10-19 09:01:22', '2016-10-19 09:02:02'),
(8, 6, '2016-10-19', 'Document sudah expired', '', '', '0', 'Wait', 2, '2016-10-19 10:42:02', '2016-10-19 10:42:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_category`
--

CREATE TABLE IF NOT EXISTS `vendor_category` (
`id` mediumint(11) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_category` smallint(5) NOT NULL,
  `id_subcat` smallint(5) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=207 ;

--
-- Dumping data untuk tabel `vendor_category`
--

INSERT INTO `vendor_category` (`id`, `id_vendor`, `id_category`, `id_subcat`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 1, 5, 5, '0', 0, '2016-09-07 17:01:14', '2016-09-07 17:01:14'),
(2, 1, 5, 6, '0', 0, '2016-09-07 17:01:14', '2016-09-07 17:01:14'),
(3, 1, 7, 7, '0', 0, '2016-09-07 17:01:14', '2016-09-07 17:01:14'),
(4, 2, 3, 3, '0', 0, '2016-09-08 10:15:14', '2016-09-08 10:15:14'),
(5, 2, 5, 5, '0', 0, '2016-09-08 10:15:14', '2016-09-08 10:15:14'),
(6, 2, 5, 6, '0', 0, '2016-09-08 10:15:14', '2016-09-08 10:15:14'),
(7, 3, 1, 1, '1', 0, '2016-09-09 15:42:11', '2016-09-09 15:42:11'),
(8, 3, 1, 2, '0', 0, '2016-09-09 15:42:11', '2016-09-09 15:42:11'),
(11, 5, 1, 1, '1', 0, '2016-09-09 18:53:29', '2016-09-09 18:53:29'),
(12, 5, 1, 2, '1', 0, '2016-09-09 18:53:29', '2016-09-09 18:53:29'),
(13, 6, 1, 1, '1', 0, '2016-09-14 10:35:23', '2016-09-14 10:35:23'),
(14, 6, 1, 2, '1', 0, '2016-09-14 10:35:23', '2016-09-14 10:35:23'),
(15, 6, 5, 5, '1', 0, '2016-09-14 10:35:23', '2016-09-14 10:35:23'),
(16, 6, 5, 6, '1', 0, '2016-09-14 10:35:23', '2016-09-14 10:35:23'),
(17, 6, 3, 3, '1', 2, '2016-09-14 13:15:29', '2016-09-14 13:15:29'),
(18, 6, 2, 8, '1', 2, '2016-09-14 13:15:38', '2016-09-14 13:15:38'),
(19, 7, 2, 8, '1', 0, '2016-09-15 15:35:05', '2016-09-15 15:35:05'),
(20, 7, 3, 3, '1', 0, '2016-09-15 15:35:05', '2016-09-15 15:35:05'),
(21, 8, 1, 1, '0', 0, '2016-09-15 15:52:01', '2016-09-15 15:52:01'),
(22, 8, 1, 2, '0', 0, '2016-09-15 15:52:01', '2016-09-15 15:52:01'),
(23, 8, 2, 8, '0', 0, '2016-09-15 15:52:01', '2016-09-15 15:52:01'),
(24, 9, 5, 5, '0', 0, '2016-09-15 18:58:20', '2016-09-15 18:58:20'),
(25, 9, 5, 6, '0', 0, '2016-09-15 18:58:20', '2016-09-15 18:58:20'),
(26, 9, 6, 4, '0', 0, '2016-09-15 18:58:20', '2016-09-15 18:58:20'),
(29, 11, 1, 1, '1', 0, '2016-09-16 10:36:42', '2016-09-16 10:36:42'),
(30, 11, 1, 2, '1', 0, '2016-09-16 10:36:42', '2016-09-16 10:36:42'),
(31, 11, 5, 5, '1', 0, '2016-09-16 10:36:42', '2016-09-16 10:36:42'),
(32, 11, 5, 6, '0', 0, '2016-09-16 10:36:42', '2016-09-16 10:36:42'),
(33, 12, 3, 3, '1', 0, '2016-09-16 13:56:40', '2016-09-16 13:56:40'),
(34, 12, 5, 5, '0', 0, '2016-09-16 13:56:40', '2016-09-16 13:56:40'),
(36, 14, 1, 1, '1', 0, '2016-09-19 16:36:22', '2016-09-19 16:36:22'),
(37, 14, 3, 3, '1', 0, '2016-09-19 16:36:22', '2016-09-19 16:36:22'),
(38, 15, 1, 1, '1', 2, '2016-09-20 10:34:46', '2016-09-20 10:34:46'),
(39, 15, 3, 3, '1', 2, '2016-09-20 10:34:46', '2016-09-20 10:34:46'),
(40, 17, 5, 6, '0', 0, '2016-09-20 13:27:30', '2016-09-20 13:27:30'),
(41, 17, 6, 4, '0', 0, '2016-09-20 13:27:30', '2016-09-20 13:27:30'),
(47, 19, 7, 7, '0', 0, '2016-09-20 18:59:07', '2016-09-20 18:59:07'),
(49, 21, 6, 4, '1', 0, '2016-09-20 20:12:37', '2016-09-20 20:12:37'),
(50, 22, 2, 8, '1', 2, '2016-09-20 20:18:47', '2016-10-12 11:26:55'),
(51, 23, 5, 5, '1', 0, '2016-09-20 20:20:34', '2016-09-20 20:20:34'),
(52, 24, 5, 6, '0', 0, '2016-09-20 20:33:19', '2016-09-20 20:33:19'),
(54, 26, 1, 1, '1', 2, '2016-09-21 11:02:01', '2016-09-21 11:02:01'),
(55, 26, 5, 5, '1', 2, '2016-09-21 11:02:01', '2016-09-21 11:02:01'),
(56, 26, 2, 8, '1', 2, '2016-09-21 11:03:38', '2016-09-21 11:03:38'),
(57, 26, 7, 7, '1', 2, '2016-09-21 11:03:44', '2016-09-21 11:03:44'),
(61, 28, 1, 1, '1', 0, '2016-09-27 14:21:55', '2016-09-27 14:21:55'),
(62, 28, 1, 2, '1', 0, '2016-09-27 14:21:55', '2016-09-27 14:21:55'),
(63, 29, 1, 1, '1', 0, '2016-09-27 17:02:25', '2016-09-27 17:02:25'),
(64, 30, 1, 1, '1', 0, '2016-09-28 08:28:25', '2016-09-28 08:28:25'),
(65, 31, 1, 1, '1', 0, '2016-09-28 08:30:39', '2016-09-28 08:30:39'),
(70, 11, 2, 8, '1', 2, '2016-09-28 09:53:52', '2016-09-28 09:53:52'),
(76, 10, 1, 15, '1', 2, '2016-09-28 10:11:07', '2016-09-28 10:11:07'),
(79, 10, 5, 12, '1', 2, '2016-09-28 10:11:35', '2016-09-28 10:11:35'),
(84, 13, 1, 2, '1', 2, '2016-09-28 11:26:53', '2016-09-28 11:26:53'),
(90, 32, 1, 1, '1', 0, '2016-09-28 15:31:22', '2016-09-28 15:31:22'),
(91, 33, 1, 1, '1', 0, '2016-09-28 15:32:52', '2016-09-28 15:32:52'),
(92, 34, 1, 1, '1', 0, '2016-09-28 15:51:27', '2016-09-28 15:51:27'),
(93, 35, 1, 1, '1', 0, '2016-09-28 16:01:51', '2016-09-28 16:01:51'),
(94, 36, 1, 1, '1', 0, '2016-09-28 16:09:45', '2016-09-28 16:09:45'),
(95, 37, 1, 1, '1', 0, '2016-09-28 16:23:51', '2016-09-28 16:23:51'),
(96, 38, 1, 1, '1', 0, '2016-09-28 16:26:07', '2016-09-28 16:26:07'),
(97, 39, 1, 1, '1', 0, '2016-09-28 16:31:28', '2016-09-28 16:31:28'),
(98, 40, 1, 1, '1', 0, '2016-09-28 16:40:39', '2016-09-28 16:40:39'),
(99, 10, 5, 11, '1', 2, '2016-09-29 09:01:16', '2016-09-29 09:01:16'),
(100, 10, 5, 13, '1', 2, '2016-09-29 09:02:52', '2016-09-29 09:02:52'),
(102, 10, 1, 2, '1', 2, '2016-09-29 09:53:56', '2016-09-29 09:53:56'),
(103, 41, 1, 1, '1', 0, '2016-09-29 10:03:39', '2016-09-29 10:03:39'),
(104, 42, 1, 1, '1', 0, '2016-09-29 10:33:00', '2016-09-29 10:33:00'),
(112, 43, 3, 3, '1', 0, '2016-09-30 11:30:28', '2016-09-30 11:30:28'),
(113, 43, 6, 4, '1', 0, '2016-09-30 11:30:28', '2016-09-30 11:30:28'),
(114, 44, 1, 16, '1', 2, '2016-09-30 11:46:07', '2016-09-30 11:46:07'),
(115, 44, 3, 3, '1', 2, '2016-09-30 11:46:07', '2016-09-30 11:46:07'),
(116, 45, 1, 1, '1', 0, '2016-09-30 14:43:50', '2016-09-30 14:43:50'),
(117, 46, 1, 1, '1', 0, '2016-10-03 09:31:50', '2016-10-03 09:31:50'),
(118, 47, 3, 3, '1', 0, '2016-10-03 10:28:11', '2016-10-03 10:28:11'),
(119, 48, 1, 1, '1', 0, '2016-10-03 11:21:55', '2016-10-03 11:21:55'),
(120, 49, 3, 3, '1', 0, '2016-10-03 11:35:21', '2016-10-03 11:35:21'),
(121, 50, 3, 3, '1', 0, '2016-10-03 11:35:59', '2016-10-03 11:35:59'),
(122, 51, 3, 3, '1', 0, '2016-10-03 11:38:59', '2016-10-03 11:38:59'),
(123, 52, 3, 3, '1', 0, '2016-10-03 13:32:58', '2016-10-03 13:32:58'),
(124, 53, 3, 3, '1', 0, '2016-10-03 13:35:31', '2016-10-03 13:35:31'),
(125, 54, 3, 3, '1', 0, '2016-10-03 13:36:27', '2016-10-03 13:36:27'),
(126, 55, 1, 1, '1', 0, '2016-10-03 14:47:33', '2016-10-03 14:47:33'),
(127, 55, 1, 2, '1', 2, '2016-10-03 14:55:24', '2016-10-03 14:55:24'),
(128, 55, 1, 14, '1', 2, '2016-10-03 14:55:32', '2016-10-03 14:55:32'),
(129, 56, 1, 1, '1', 0, '2016-10-06 08:10:37', '2016-10-06 08:10:37'),
(130, 56, 1, 15, '1', 0, '2016-10-06 08:10:37', '2016-10-06 08:10:37'),
(131, 56, 1, 14, '1', 0, '2016-10-06 08:10:37', '2016-10-06 08:10:37'),
(132, 56, 1, 16, '1', 0, '2016-10-06 08:10:37', '2016-10-06 08:10:37'),
(133, 56, 2, 8, '1', 0, '2016-10-06 08:10:37', '2016-10-06 08:10:37'),
(134, 57, 3, 3, '0', 12, '2016-10-06 13:32:31', '2016-10-06 13:32:31'),
(135, 58, 3, 3, '0', 12, '2016-10-06 13:32:31', '2016-10-06 13:32:31'),
(136, 59, 3, 3, '0', 12, '2016-10-06 13:32:31', '2016-10-06 13:32:31'),
(137, 60, 3, 3, '0', 12, '2016-10-06 13:32:31', '2016-10-06 13:32:31'),
(138, 61, 1, 15, '1', 0, '2016-10-06 14:02:53', '2016-10-06 14:02:53'),
(139, 61, 1, 14, '1', 0, '2016-10-06 14:02:53', '2016-10-06 14:02:53'),
(140, 62, 1, 2, '1', 0, '2016-10-07 13:08:59', '2016-10-07 13:08:59'),
(141, 63, 1, 1, '1', 0, '2016-10-10 09:47:44', '2016-10-10 09:47:44'),
(142, 64, 1, 1, '1', 0, '2016-10-10 10:03:52', '2016-10-10 10:03:52'),
(143, 65, 3, 3, '1', 0, '2016-10-10 15:11:40', '2016-10-10 15:11:40'),
(144, 66, 1, 1, '1', 0, '2016-10-11 15:27:46', '2016-10-11 15:27:46'),
(145, 66, 1, 15, '1', 0, '2016-10-11 15:27:46', '2016-10-11 15:27:46'),
(146, 66, 2, 8, '1', 0, '2016-10-11 15:27:46', '2016-10-11 15:27:46'),
(147, 67, 3, 3, '0', 12, '2016-10-12 07:45:24', '2016-10-12 07:45:24'),
(148, 68, 1, 2, '1', 0, '2016-10-12 08:34:25', '2016-10-12 08:34:25'),
(149, 22, 1, 2, '1', 2, '2016-10-12 11:11:03', '2016-10-12 13:51:08'),
(150, 22, 1, 1, '1', 2, '2016-10-12 11:10:54', '2016-10-12 13:51:10'),
(151, 69, 1, 14, '0', 12, '2016-10-12 22:07:39', '2016-10-12 22:07:39'),
(152, 70, 3, 3, '0', 12, '2016-10-13 08:57:32', '2016-10-13 08:57:32'),
(153, 71, 3, 3, '0', 12, '2016-10-13 09:03:13', '2016-10-13 09:03:13'),
(154, 72, 3, 3, '0', 12, '2016-10-13 13:32:31', '2016-10-13 13:32:31'),
(155, 73, 6, 4, '1', 36, '2016-10-13 13:33:21', '2016-10-13 16:01:46'),
(156, 73, 5, 6, '1', 36, '2016-10-13 13:33:21', '2016-10-13 16:01:46'),
(157, 73, 5, 5, '1', 36, '2016-10-13 13:33:21', '2016-10-13 16:01:46'),
(158, 74, 3, 3, '0', 12, '2016-10-13 13:35:20', '2016-10-13 13:35:20'),
(159, 75, 3, 3, '0', 12, '2016-10-13 13:35:20', '2016-10-13 13:35:20'),
(160, 76, 3, 3, '0', 12, '2016-10-13 13:41:00', '2016-10-13 13:41:00'),
(161, 77, 3, 3, '0', 12, '2016-10-13 13:41:00', '2016-10-13 13:41:00'),
(162, 78, 3, 3, '0', 12, '2016-10-13 13:41:47', '2016-10-13 13:41:47'),
(163, 79, 3, 3, '0', 12, '2016-10-13 13:41:47', '2016-10-13 13:41:47'),
(164, 80, 3, 3, '0', 12, '2016-10-13 13:43:12', '2016-10-13 13:43:12'),
(165, 81, 3, 3, '0', 12, '2016-10-13 13:43:12', '2016-10-13 13:43:12'),
(166, 82, 3, 3, '0', 12, '2016-10-13 13:44:27', '2016-10-13 13:44:27'),
(167, 83, 3, 3, '0', 12, '2016-10-13 13:44:27', '2016-10-13 13:44:27'),
(168, 84, 3, 3, '0', 12, '2016-10-13 13:44:51', '2016-10-13 13:44:51'),
(169, 85, 3, 3, '0', 12, '2016-10-13 13:44:51', '2016-10-13 13:44:51'),
(170, 86, 1, 1, '0', 12, '2016-10-13 13:46:54', '2016-10-13 13:46:54'),
(171, 87, 1, 1, '0', 12, '2016-10-13 13:46:54', '2016-10-13 13:46:54'),
(172, 88, 1, 1, '0', 12, '2016-10-13 13:47:02', '2016-10-13 13:47:02'),
(173, 89, 1, 1, '0', 12, '2016-10-13 13:47:02', '2016-10-13 13:47:02'),
(174, 90, 3, 3, '0', 12, '2016-10-13 14:12:25', '2016-10-13 14:12:25'),
(175, 91, 3, 3, '0', 12, '2016-10-13 14:12:47', '2016-10-13 14:12:47'),
(176, 92, 3, 3, '0', 12, '2016-10-13 14:13:08', '2016-10-13 14:13:08'),
(177, 93, 1, 1, '0', 12, '2016-10-13 14:14:50', '2016-10-13 14:14:50'),
(178, 94, 1, 1, '0', 12, '2016-10-13 14:15:48', '2016-10-13 14:15:48'),
(179, 73, 2, 8, '1', 36, '2016-10-13 15:31:05', '2016-10-13 16:01:54'),
(180, 95, 1, 15, '0', 0, '2016-10-13 22:26:03', '2016-10-13 22:26:03'),
(181, 95, 1, 14, '0', 0, '2016-10-13 22:26:03', '2016-10-13 22:26:03'),
(182, 96, 1, 1, '0', 12, '2016-10-14 11:28:05', '2016-10-14 11:28:05'),
(183, 97, 1, 1, '0', 12, '2016-10-14 11:28:50', '2016-10-14 11:28:50'),
(184, 96, 3, 3, '0', 12, '2016-10-14 13:53:28', '2016-10-14 13:53:28'),
(185, 96, 3, 3, '0', 12, '2016-10-14 14:40:44', '2016-10-14 14:40:44'),
(186, 96, 3, 3, '0', 12, '2016-10-14 14:45:34', '2016-10-14 14:45:34'),
(187, 101, 1, 1, '1', 0, '2016-10-17 09:08:22', '2016-10-17 09:08:22'),
(188, 101, 1, 2, '1', 0, '2016-10-17 09:08:22', '2016-10-17 09:08:22'),
(189, 101, 1, 20, '1', 0, '2016-10-17 09:08:22', '2016-10-17 09:08:22'),
(190, 102, 1, 1, '1', 0, '2016-10-17 09:53:59', '2016-10-17 09:53:59'),
(191, 103, 1, 1, '1', 0, '2016-10-17 11:33:28', '2016-10-17 11:33:28'),
(192, 104, 3, 3, '1', 0, '2016-10-17 12:02:39', '2016-10-17 12:02:39'),
(193, 105, 1, 1, '1', 0, '2016-10-17 14:46:11', '2016-10-17 14:46:11'),
(194, 106, 1, 1, '1', 0, '2016-10-18 10:07:13', '2016-10-18 10:07:13'),
(195, 107, 1, 1, '1', 0, '2016-10-18 10:56:21', '2016-10-18 10:56:21'),
(196, 108, 1, 1, '0', 12, '2016-10-18 14:18:00', '2016-10-18 14:18:00'),
(197, 109, 1, 1, '0', 12, '2016-10-18 14:23:54', '2016-10-18 14:23:54'),
(198, 110, 5, 13, '1', 0, '2016-10-18 18:46:34', '2016-10-18 18:46:34'),
(199, 110, 5, 6, '1', 0, '2016-10-18 18:46:34', '2016-10-18 18:46:34'),
(200, 110, 5, 5, '1', 0, '2016-10-18 18:46:34', '2016-10-18 18:46:34'),
(201, 111, 1, 15, '1', 0, '2016-10-18 18:52:06', '2016-10-18 18:52:06'),
(202, 111, 1, 16, '1', 0, '2016-10-18 18:52:06', '2016-10-18 18:52:06'),
(203, 112, 1, 14, '1', 0, '2016-10-18 18:54:04', '2016-10-18 18:54:04'),
(204, 112, 1, 2, '1', 0, '2016-10-18 18:54:04', '2016-10-18 18:54:04'),
(205, 113, 5, 12, '1', 0, '2016-10-18 18:59:54', '2016-10-18 18:59:54'),
(206, 113, 5, 6, '1', 0, '2016-10-18 18:59:54', '2016-10-18 18:59:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_changedata`
--

CREATE TABLE IF NOT EXISTS `vendor_changedata` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `table` varchar(30) DEFAULT NULL,
  `index` smallint(5) DEFAULT NULL COMMENT 'id table',
  `fieldname` varchar(30) DEFAULT NULL,
  `before` varchar(50) DEFAULT '',
  `after` varchar(50) DEFAULT NULL,
  `status` enum('request','approved','rejected') NOT NULL DEFAULT 'request',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

--
-- Dumping data untuk tabel `vendor_changedata`
--

INSERT INTO `vendor_changedata` (`id`, `id_vendor`, `table`, `index`, `fieldname`, `before`, `after`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(2, 3, 'vendor', NULL, 'web', NULL, 'hgjh', 'request', 2, '2016-09-16 09:57:42', '2016-09-16 09:57:42'),
(3, 14, 'vendor', NULL, 'vendor_address', 'Jalan Address', 'Jalan Jaya 1', 'request', 2, '2016-09-26 10:48:22', '2016-09-26 10:48:22'),
(4, 14, 'vendor', NULL, 'postcode', NULL, '8725823756', 'request', 2, '2016-09-26 10:48:22', '2016-09-26 10:48:22'),
(5, 14, 'vendor', NULL, 'npwp_address', NULL, 'Jalan Maju 1', 'request', 2, '2016-09-26 10:48:22', '2016-09-26 10:48:22'),
(6, 14, 'vendor', NULL, 'npwp_postcode', NULL, '678436', 'request', 2, '2016-09-26 10:48:22', '2016-09-26 10:48:22'),
(7, 14, 'vendor', NULL, 'web', NULL, 'https://garuda-indonesia.com', 'request', 2, '2016-09-26 10:48:22', '2016-09-26 10:48:22'),
(8, 15, 'vendor', NULL, 'postcode', NULL, '832759265', 'request', 2, '2016-09-26 10:49:41', '2016-09-26 10:49:41'),
(9, 15, 'vendor', NULL, 'npwp_address', NULL, 'owehfwen', 'request', 2, '2016-09-26 10:49:41', '2016-09-26 10:49:41'),
(10, 15, 'vendor', NULL, 'npwp_postcode', NULL, '74983157', 'request', 2, '2016-09-26 10:49:41', '2016-09-26 10:49:41'),
(11, 15, 'vendor', NULL, 'web', NULL, 'www.jaya.com', 'request', 2, '2016-09-26 10:49:41', '2016-09-26 10:49:41'),
(12, 23, 'contact_person', 23, 'position', '-', 'tester', 'request', 2, '2016-09-26 11:01:48', '2016-09-26 11:01:48'),
(13, 23, 'bank_account', NULL, 'acc_number', 'none', 'Noneefihfeh', 'request', 2, '2016-09-26 11:03:41', '2016-09-26 11:03:41'),
(14, 23, 'bank_account', NULL, 'acc_name', 'none', 'Noneafhlal', 'request', 2, '2016-09-26 11:03:41', '2016-09-26 11:03:41'),
(15, 23, 'bank_account', NULL, 'bank_name', 'none', 'Nonelehflqf', 'request', 2, '2016-09-26 11:03:41', '2016-09-26 11:03:41'),
(16, 23, 'bank_account', NULL, 'branch_name', 'none', 'Noneeflqi', 'request', 2, '2016-09-26 11:03:41', '2016-09-26 11:03:41'),
(17, 23, 'akta', NULL, 'notaris_name', 'none', 'Noneewogywegoyq', 'request', 2, '2016-09-26 11:06:54', '2016-09-26 11:06:54'),
(18, 23, 'akta', NULL, 'notaris_address', 'none', 'fojqehqeg', 'request', 2, '2016-09-26 11:06:54', '2016-09-26 11:06:54'),
(19, 23, 'akta', NULL, 'notaris_phone', 'none', 'efheg', 'request', 2, '2016-09-26 11:06:54', '2016-09-26 11:06:54'),
(20, 23, 'akta', NULL, 'notaris_number', 'none', 'Nonefqhp', 'request', 2, '2016-09-26 11:06:54', '2016-09-26 11:06:54'),
(21, 23, 'akta', NULL, 'notaris_date', '2016-09-26', '2016-03-23', 'request', 2, '2016-09-26 11:06:54', '2016-09-26 11:06:54'),
(22, 23, 'akta', NULL, 'remark', 'none', 'Noneeojgfqeq', 'request', 2, '2016-09-26 11:06:54', '2016-09-26 11:06:54'),
(23, 17, 'vendor', NULL, 'postcode', NULL, '787989080', 'approved', 2, '2016-09-29 15:55:19', '2016-09-29 15:55:19'),
(24, 17, 'vendor', NULL, 'npwp_postcode', NULL, 'jkljkkj', 'approved', 2, '2016-09-29 15:55:19', '2016-09-29 15:55:19'),
(25, 17, 'vendor', NULL, 'postcode', NULL, '787989080', 'approved', 2, '2016-09-29 15:57:03', '2016-09-29 15:57:03'),
(26, 17, 'vendor', NULL, 'npwp_postcode', NULL, 'jkljkkj', 'rejected', 2, '2016-09-29 15:57:03', '2016-09-29 15:57:03'),
(27, 17, 'vendor', NULL, 'web', NULL, 'testing.com', 'approved', 2, '2016-09-30 10:50:14', '2016-09-30 10:50:14'),
(28, 17, 'vendor', NULL, 'phone', '908989089089080', '908989089', 'approved', 2, '2016-09-30 10:50:49', '2016-09-30 10:50:49'),
(29, 17, 'vendor', NULL, 'fax', '78978979879879', '789789788', 'approved', 2, '2016-09-30 10:50:49', '2016-09-30 10:50:49'),
(30, 17, 'bank_account', NULL, 'acc_number', 'none', 'acc number', 'approved', 2, '2016-09-30 11:14:40', '2016-09-30 11:14:40'),
(31, 17, 'bank_account', NULL, 'acc_name', 'none', 'acc name', 'approved', 2, '2016-09-30 11:14:40', '2016-09-30 11:14:40'),
(32, 17, 'akta', NULL, 'notaris_name', 'none', 'not name', 'approved', 2, '2016-09-30 11:15:04', '2016-09-30 11:15:04'),
(33, 17, 'akta', NULL, 'notaris_address', 'none', 'not addr', 'approved', 2, '2016-09-30 11:15:04', '2016-09-30 11:15:04'),
(34, 17, 'vendor', NULL, 'npwp_address', 'jakarta', 'Jakaruta', 'approved', 2, '2016-10-03 08:40:42', '2016-10-03 08:40:42'),
(35, 17, 'vendor', NULL, 'web', 'www.erer.com', 'www.sfsdfsdfsdf.com', 'approved', 2, '2016-10-03 08:40:42', '2016-10-03 08:40:42'),
(36, 17, 'vendor', NULL, 'postcode', '787989080', '78798908', 'approved', 2, '2016-10-03 08:49:59', '2016-10-03 08:49:59'),
(37, 17, 'vendor', NULL, 'npwp', '78897897897', '78897897', 'approved', 2, '2016-10-03 08:49:59', '2016-10-03 08:49:59'),
(38, 17, 'vendor', NULL, 'web', 'www.sfsdfsdfsdf.com', 'www.avl.com', 'approved', 2, '2016-10-03 08:50:30', '2016-10-03 08:50:30'),
(39, 22, 'vendor', NULL, 'postcode', NULL, 'KITA TAMBAHKAN POSTCODE', 'approved', 2, '2016-10-12 11:06:02', '2016-10-12 11:06:02'),
(40, 22, 'vendor', NULL, 'npwp_address', NULL, 'JALAN PEMBANGUNAN', 'approved', 2, '2016-10-12 11:06:02', '2016-10-12 11:06:02'),
(41, 22, 'vendor', NULL, 'npwp_postcode', NULL, '746582765', 'approved', 2, '2016-10-12 11:06:02', '2016-10-12 11:06:02'),
(42, 22, 'vendor', NULL, 'web', NULL, 'COBA EDIT VENDOR BY ADMIN', 'approved', 2, '2016-10-12 11:06:02', '2016-10-12 11:06:02'),
(43, 22, 'vendor_category', 50, 'id_category', '6', '2', 'approved', 2, '2016-10-12 11:10:43', '2016-10-12 11:10:43'),
(44, 22, 'vendor_category', 50, 'id_subcat', '4', '8', 'approved', 2, '2016-10-12 11:10:43', '2016-10-12 11:10:43'),
(45, 22, 'vendor_category', 50, 'id_category', '6', '1', 'approved', 2, '2016-10-12 11:15:09', '2016-10-12 11:15:09'),
(46, 22, 'vendor_category', 50, 'id_subcat', '4', '1', 'approved', 2, '2016-10-12 11:15:09', '2016-10-12 11:15:09'),
(47, 22, 'contact_person', 22, 'fullname', 'AJEHTQH', 'WISNU', 'approved', 2, '2016-10-12 13:41:05', '2016-10-12 13:41:05'),
(48, 22, 'contact_person', 22, 'position', '-', 'TESTER OSE 3', 'approved', 2, '2016-10-12 13:41:05', '2016-10-12 13:41:05'),
(49, 22, 'contact_person', 22, 'mobile', '2356', '081295057000', 'approved', 2, '2016-10-12 13:41:05', '2016-10-12 13:41:05'),
(50, 22, 'contact_person', 22, 'email', 'siti.madusari@asyst.co.id', 'WISNU.ARIMURTI@ASYST.CO.ID', 'approved', 2, '2016-10-12 13:41:05', '2016-10-12 13:41:05'),
(51, 22, 'bank_account', NULL, 'acc_number', 'none', 'NoneNUNONI', 'approved', 2, '2016-10-12 13:44:39', '2016-10-12 13:44:39'),
(52, 22, 'bank_account', NULL, 'acc_name', 'none', 'NoneNUNONI', 'approved', 2, '2016-10-12 13:44:39', '2016-10-12 13:44:39'),
(53, 22, 'bank_account', NULL, 'acc_address', 'none', 'NoneNUNONI', 'approved', 2, '2016-10-12 13:44:39', '2016-10-12 13:44:39'),
(54, 22, 'bank_account', NULL, 'bank_name', 'none', 'NoneNUNONI', 'approved', 2, '2016-10-12 13:44:40', '2016-10-12 13:44:40'),
(55, 22, 'bank_account', NULL, 'branch_name', 'none', 'NoneNUNONI', 'approved', 2, '2016-10-12 13:44:40', '2016-10-12 13:44:40'),
(56, 22, 'akta', NULL, 'notaris_name', 'none', 'NoneNoneNUNONI', 'approved', 2, '2016-10-12 13:45:03', '2016-10-12 13:45:03'),
(57, 22, 'akta', NULL, 'notaris_address', 'none', 'NoneNUNONI', 'approved', 2, '2016-10-12 13:45:03', '2016-10-12 13:45:03'),
(58, 22, 'akta', NULL, 'notaris_phone', 'none', 'NoneNUNONI', 'approved', 2, '2016-10-12 13:45:03', '2016-10-12 13:45:03'),
(59, 22, 'akta', NULL, 'notaris_number', 'none', 'NoneNUNONI', 'approved', 2, '2016-10-12 13:45:03', '2016-10-12 13:45:03'),
(60, 22, 'akta', NULL, 'notaris_date', '2016-10-12', '2016-10-06', 'approved', 2, '2016-10-12 13:45:03', '2016-10-12 13:45:03'),
(61, 22, 'akta', NULL, 'remark', 'none', 'NoneNoneNUNONI', 'approved', 2, '2016-10-12 13:45:03', '2016-10-12 13:45:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_delete`
--

CREATE TABLE IF NOT EXISTS `vendor_delete` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `register_num` varchar(20) NOT NULL,
  `vendor_num` varchar(20) NOT NULL,
  `vendor_name` varchar(40) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('request','approved','start','completed') NOT NULL DEFAULT 'request',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data untuk tabel `vendor_delete`
--

INSERT INTO `vendor_delete` (`id`, `id_vendor`, `register_num`, `vendor_num`, `vendor_name`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(6, 4, 'SR16000004', '2002', 'PT. Arif Bijaksana', 'test delete arif', 'completed', 2, '2016-09-13 16:22:49', '2016-09-13 16:23:40'),
(7, 3, 'SR16000003', '0', 'PT. Tukang Kompor', 'hapus aja', 'request', 2, '2016-09-14 09:49:24', '2016-09-14 09:49:24'),
(8, 23, 'SR16000017', '0', 'PT REDLIST', 'hapus VENDOR ini', 'approved', 2, '2016-09-30 16:19:53', '2016-10-18 15:56:43'),
(9, 20, 'SR16000014', '0', 'PT AVL ACTIVE-2', 'Hapus yg ini', 'start', 2, '2016-10-10 10:59:51', '2016-10-10 11:00:42'),
(10, 25, 'SR16000018', '0', 'PT AVL ACTIVE-1', 'yg ini juga', 'completed', 2, '2016-10-10 11:09:00', '2016-10-10 11:11:10'),
(11, 101, 'SR16000083', '0', 'PT ASYST', 'delete', 'approved', 2, '2016-10-18 15:06:47', '2016-10-18 16:09:39'),
(12, 17, 'SR16000012', '0', 'GGGGGGGGGGGG', 'mohon dihapus, supaya bisa daftar', 'approved', 2, '2016-10-18 16:12:00', '2016-10-18 16:16:02'),
(13, 65, 'SR16000055', '0', 'PT. Sinar Sosro', 'cobain delete 1', 'request', 2, '2016-10-18 19:34:26', '2016-10-18 19:34:26'),
(14, 73, 'SR16000063', '0', 'PT VENDOR NYA MAU NGEDIT PROFILE SENDIRI', 'hapus ajalah....', 'request', 2, '2016-10-18 19:39:26', '2016-10-18 19:39:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_docs`
--

CREATE TABLE IF NOT EXISTS `vendor_docs` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_table` smallint(5) NOT NULL,
  `id_docs` smallint(5) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `filetype` varchar(50) NOT NULL,
  `filesize` mediumint(11) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=70 ;

--
-- Dumping data untuk tabel `vendor_docs`
--

INSERT INTO `vendor_docs` (`id`, `id_vendor`, `id_table`, `id_docs`, `filename`, `filetype`, `filesize`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(63, 2, 1, 19, 'FIle upload document baru', 'pdf', 2014759, 'upload doc baru', '1', 2, '2016-10-04 08:31:57', '2016-10-04 08:31:57'),
(64, 2, 1, 20, 'cobain upload', 'pdf', 2014759, 'srhstgserg', '1', 2, '2016-10-04 08:33:31', '2016-10-04 08:33:31'),
(69, 2, 2, 18, 'Test upload DD docs', 'pdf', 2014759, 'esorinh[', '1', 2, '2016-10-04 15:36:52', '2016-10-04 15:36:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_inactive`
--

CREATE TABLE IF NOT EXISTS `vendor_inactive` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `approval` enum('3','2','1') DEFAULT '1',
  `process` enum('active','inactive') NOT NULL DEFAULT 'inactive',
  `reason` varchar(50) NOT NULL,
  `status` enum('request','approved','rejected') NOT NULL DEFAULT 'request',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `vendor_inactive`
--

INSERT INTO `vendor_inactive` (`id`, `id_vendor`, `approval`, `process`, `reason`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(2, 6, '1', 'inactive', 'saya ga suka', 'rejected', 2, '2016-09-19 16:01:32', '2016-09-23 14:45:10'),
(3, 12, '3', 'inactive', 'Dijadikan Inactive', 'approved', 2, '2016-09-23 09:30:36', '2016-10-18 10:06:03'),
(4, 21, '3', 'inactive', 'test inactive', 'approved', 2, '2016-09-26 09:31:49', '2016-09-26 13:56:50'),
(5, 21, '2', 'active', 'jadikan aktif', 'request', 2, '2016-09-28 10:52:35', '2016-10-17 08:44:41'),
(6, 14, '2', 'inactive', 'change to inactive vendor PT.VJ', 'request', 1, '2016-09-29 10:55:31', '2016-10-18 09:26:17'),
(7, 28, '3', 'inactive', 'Set to inactive2', 'approved', 2, '2016-09-30 09:10:24', '2016-10-18 10:02:18'),
(8, 22, '2', 'inactive', 'Set to inactive 3', 'request', 2, '2016-09-30 09:10:41', '2016-10-17 09:19:43'),
(11, 53, '1', 'inactive', 'tidak valid', 'rejected', 2, '2016-10-18 14:40:13', '2016-10-18 14:49:41'),
(12, 53, '2', 'inactive', 'tolong di approve', 'request', 2, '2016-10-18 14:54:10', '2016-10-18 14:55:30'),
(13, 101, '1', 'inactive', 'ehgieqw', 'request', 2, '2016-10-18 15:05:31', '2016-10-18 15:05:31'),
(14, 28, '3', 'active', 'mohon dijadikan active', 'approved', 2, '2016-10-18 16:05:36', '2016-10-18 16:10:09'),
(15, 28, '1', 'inactive', 'jadiin inactive aja...', 'request', 2, '2016-10-18 19:41:45', '2016-10-18 19:41:45'),
(16, 12, '1', 'active', 'Testing buat aktif 10', 'request', 2, '2016-10-18 19:45:43', '2016-10-18 19:45:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_logs`
--

CREATE TABLE IF NOT EXISTS `vendor_logs` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `user_id` smallint(5) NOT NULL,
  `description` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `action` enum('redlist','blacklist','active','inactive') DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=115 ;

--
-- Dumping data untuk tabel `vendor_logs`
--

INSERT INTO `vendor_logs` (`id`, `id_vendor`, `user_id`, `description`, `reason`, `status`, `created_date`, `action`) VALUES
(2, 3, 2, 'add vendor to blacklist', 'Bikin Panas', '1', '2016-09-16 08:53:55', 'blacklist'),
(3, 3, 3, 'Approve Blacklist step 1', '', '1', '2016-09-16 08:57:18', 'blacklist'),
(4, 3, 3, 'Rejected From Blacklist', '', '1', '2016-09-16 08:57:23', 'blacklist'),
(5, 3, 3, 'Approve Blacklist step 1', '', '1', '2016-09-16 08:57:34', 'blacklist'),
(6, 3, 3, 'Rejected From Blacklist', '', '1', '2016-09-16 08:57:39', 'blacklist'),
(7, 3, 3, 'Approve Blacklist step 1', '', '1', '2016-09-16 08:57:56', 'blacklist'),
(8, 3, 4, 'Approve Blacklist step 2', '', '1', '2016-09-16 09:01:15', 'blacklist'),
(9, 3, 6, 'Rejected From Blacklist', '', '1', '2016-09-16 09:02:09', 'blacklist'),
(10, 3, 6, 'Approve Blacklist step 3', '', '1', '2016-09-16 09:09:17', 'blacklist'),
(11, 6, 3, 'Rejected inactive process', 'saya ga suka', '1', '2016-09-23 14:45:10', 'inactive'),
(12, 12, 3, 'Approved and process to SM IBK', 'Dijadikan Inactive', '1', '2016-09-23 14:46:23', 'inactive'),
(13, 21, 3, 'Approved and process to SM IBK', 'test inactive', '1', '2016-09-26 09:33:19', 'inactive'),
(14, 21, 4, 'Approved and process to VP IB', 'test inactive', '1', '2016-09-26 13:54:08', 'inactive'),
(15, 21, 6, 'Approved to inactive', 'test inactive', '1', '2016-09-26 13:56:50', 'inactive'),
(16, 12, 4, 'Approved and process to VP IB', 'Dijadikan Inactive', '1', '2016-09-26 14:08:59', 'inactive'),
(17, 12, 6, 'Approved to inactive', 'Dijadikan Inactive', '1', '2016-09-26 14:09:44', 'inactive'),
(18, 23, 2, 'add vendor to redlist', 'testing vendor redlist', '1', '2016-09-26 15:32:16', 'redlist'),
(19, 23, 2, 'update data redlist vendor', 'testing vendor redlist', '1', '2016-09-26 15:32:33', 'redlist'),
(20, 14, 2, 'add vendor to redlist', 'rejected', '1', '2016-09-28 16:08:56', 'redlist'),
(21, 14, 4, 'rejected redlist vendor', 'rejected', '1', '2016-09-28 16:09:33', 'redlist'),
(22, 23, 4, 'approved redlist vendor', 'testing vendor redlist', '1', '2016-09-28 16:15:46', 'redlist'),
(23, 6, 1, 'add vendor to redlist', 'bmn', '1', '2016-09-28 18:30:02', 'redlist'),
(24, 6, 1, 'add vendor to redlist', 'jhkjhkj', '1', '2016-09-28 18:31:48', 'redlist'),
(25, 6, 1, 'add vendor to redlist', 'jhkjhkj', '1', '2016-09-28 18:32:15', 'redlist'),
(26, 6, 1, 'add vendor to redlist', 'jhkjhkj', '1', '2016-09-28 18:33:30', 'redlist'),
(27, 6, 1, 'add vendor to redlist', 'jhkjhkj', '1', '2016-09-28 18:36:24', 'redlist'),
(28, 6, 1, 'approved redlist vendor', 'bmn', '1', '2016-09-28 18:37:41', 'redlist'),
(29, 6, 1, 'approved redlist vendor', 'jhkjhkj', '1', '2016-09-28 18:38:45', 'redlist'),
(30, 6, 1, 'approved redlist vendor', 'jhkjhkj', '1', '2016-09-28 18:39:17', 'redlist'),
(31, 2, 1, 'add vendor to redlist', 'kjhkj', '1', '2016-09-28 20:06:56', 'redlist'),
(32, 2, 1, 'approved redlist vendor', 'kjhkj', '1', '2016-09-28 20:09:19', 'redlist'),
(33, 2, 1, 'rejected redlist vendor', 'kjhkj', '1', '2016-09-28 20:10:46', 'redlist'),
(34, 2, 1, 'rejected redlist vendor', 'kjhkj', '1', '2016-09-28 20:12:31', 'redlist'),
(35, 15, 2, 'add vendor to redlist', 'jhjkhj', '1', '2016-09-29 09:38:37', 'redlist'),
(36, 2, 2, 'add vendor to redlist', 'jkhjk', '1', '2016-09-29 10:30:01', 'redlist'),
(37, 2, 2, 'cancelled redlist vendor', 'cancelled redlist vendor', '1', '2016-09-29 14:39:21', 'redlist'),
(38, 14, 2, 'add vendor to redlist', 'Coba email', '1', '2016-09-29 15:10:56', 'redlist'),
(39, 14, 3, 'approved redlist vendor', 'Coba email', '1', '2016-09-29 15:47:00', 'redlist'),
(40, 15, 3, 'approved redlist vendor', 'jhjkhj', '1', '2016-09-29 15:50:24', 'redlist'),
(41, 2, 1, 'add vendor to redlist', 'fdsdf', '1', '2016-09-29 16:04:43', 'redlist'),
(42, 2, 1, 'add vendor to redlist', 'jkhkj', '1', '2016-09-29 16:08:35', 'redlist'),
(43, 2, 1, 'add vendor to redlist', 'jkhkj', '1', '2016-09-29 16:11:39', 'redlist'),
(44, 2, 1, 'add vendor to redlist', 'jkhkj', '1', '2016-09-29 16:12:41', 'redlist'),
(45, 2, 2, 'add vendor to redlist', 'aaaaaa', '1', '2016-09-29 16:17:22', 'redlist'),
(46, 2, 2, 'add vendor to redlist', 'kjkl', '1', '2016-09-30 09:33:23', 'redlist'),
(47, 2, 2, 'add vendor to redlist', 'kjhkhkj', '1', '2016-09-30 09:37:07', 'redlist'),
(48, 2, 2, 'cancelled redlist vendor', 'cancelled redlist vendor', '1', '2016-09-30 09:41:51', 'redlist'),
(49, 2, 3, 'approved redlist vendor', 'kjkl', '1', '2016-09-30 09:42:30', 'redlist'),
(50, 2, 3, 'rejected redlist vendor', 'aaaaaa', '1', '2016-09-30 09:43:32', 'redlist'),
(51, 2, 3, 'rejected redlist vendor', 'jkhkj', '1', '2016-09-30 09:44:55', 'redlist'),
(52, 2, 3, 'approved redlist vendor', 'jkhkj', '1', '2016-09-30 09:45:41', 'redlist'),
(53, 2, 3, 'approved redlist vendor', 'jkhkj', '1', '2016-09-30 09:46:57', 'redlist'),
(54, 2, 3, 'approved redlist vendor', 'fdsdf', '1', '2016-09-30 09:47:28', 'redlist'),
(55, 22, 2, 'add vendor to blacklist', 'Testing Blacklist', '1', '2016-10-03 13:23:35', 'blacklist'),
(56, 22, 3, 'Approve Blacklist step 1', '', '1', '2016-10-03 13:29:15', 'blacklist'),
(57, 22, 4, 'Approve Blacklist step 2', '', '1', '2016-10-03 13:35:58', 'blacklist'),
(58, 22, 6, 'Approve Blacklist step 3', '', '1', '2016-10-03 13:36:36', 'blacklist'),
(59, 22, 2, 'update data blacklist vendor', 'Testing Blacklist', '1', '2016-10-03 13:43:06', 'blacklist'),
(60, 22, 2, 'update data blacklist vendor', 'Testing Blacklist', '1', '2016-10-03 13:43:18', 'blacklist'),
(61, 22, 3, 'Approve Blacklist step 1', '', '1', '2016-10-03 13:45:12', 'blacklist'),
(62, 22, 4, 'Approve Blacklist step 2', '', '1', '2016-10-03 13:54:39', 'blacklist'),
(63, 22, 6, 'Approve Blacklist step 3', '', '1', '2016-10-03 13:55:05', 'blacklist'),
(64, 2, 1, 'approved redlist vendor', 'kjhkj', '1', '2016-10-03 13:59:13', 'redlist'),
(65, 2, 1, 'approved redlist vendor', 'kjhkj', '1', '2016-10-03 14:01:00', 'redlist'),
(66, 2, 1, 'update data redlist vendor', 'kjhkj', '1', '2016-10-03 14:40:26', 'redlist'),
(67, 2, 1, 'update data redlist vendor', 'kjhkj', '1', '2016-10-05 16:05:06', 'redlist'),
(68, 2, 1, 'update data redlist vendor', 'kjhkj', '1', '2016-10-05 16:05:32', 'redlist'),
(69, 2, 1, 'update data redlist vendor', 'kjhkj', '1', '2016-10-05 16:05:53', 'redlist'),
(70, 21, 2, 'add vendor to redlist', 'aaaaa', '1', '2016-10-06 14:24:30', 'redlist'),
(71, 21, 2, 'cancelled redlist vendor', 'cancelled redlist vendor', '1', '2016-10-12 15:27:09', 'redlist'),
(72, 2, 2, 'add vendor to redlist', 'fsdfs', '1', '2016-10-17 08:32:46', 'redlist'),
(73, 21, 3, 'Approved and process to SM IBK', 'jadikan aktif', '1', '2016-10-17 08:44:41', 'active'),
(74, 22, 3, 'Approved and process to SM IBK', 'Set to inactive 3', '1', '2016-10-17 09:19:43', 'inactive'),
(75, 28, 3, 'Approved and process to SM IBK', 'Set to inactive2', '1', '2016-10-17 09:22:15', 'inactive'),
(76, 28, 3, 'Approved and process to SM IBK', 'Set to inactive2', '1', '2016-10-17 09:24:02', 'inactive'),
(77, 28, 3, 'Approved and process to SM IBK', 'Set to inactive2', '1', '2016-10-17 09:25:39', 'inactive'),
(78, 28, 3, 'Approved and process to SM IBK', 'Set to inactive2', '1', '2016-10-17 09:28:00', 'inactive'),
(79, 28, 3, 'Approved and process to SM IBK', 'Set to inactive2', '1', '2016-10-17 09:29:49', 'inactive'),
(80, 28, 3, 'Approved and process to SM IBK', 'Set to inactive2', '1', '2016-10-17 09:32:38', 'inactive'),
(81, 28, 3, 'Approved and process to SM IBK', 'Set to inactive2', '1', '2016-10-17 09:37:05', 'inactive'),
(82, 28, 3, 'Approved and process to SM IBK', 'Set to inactive2', '1', '2016-10-17 09:38:05', 'inactive'),
(83, 28, 3, 'Approved and process to SM IBK', 'Set to inactive2', '1', '2016-10-17 09:40:14', 'inactive'),
(84, 28, 3, 'Approved and process to SM IBK', 'Set to inactive2', '1', '2016-10-17 09:41:38', 'inactive'),
(85, 28, 3, 'Approved and process to SM IBK', 'Set to inactive2', '1', '2016-10-17 09:42:15', 'inactive'),
(86, 2, 2, 'add vendor to redlist', 'jlkjl', '1', '2016-10-17 09:43:18', 'redlist'),
(87, 28, 3, 'Approved and process to SM IBK', 'Set to inactive2', '1', '2016-10-17 09:53:05', 'inactive'),
(88, 28, 31, 'Approved and process to VP IB', 'Set to inactive2', '1', '2016-10-17 10:01:52', 'inactive'),
(89, 2, 3, 'rejected redlist vendor', 'kjhkj', '1', '2016-10-17 10:07:09', 'redlist'),
(90, 2, 3, 'approved redlist vendor', 'jlkjl', '1', '2016-10-17 10:09:09', 'redlist'),
(91, 2, 3, 'approved redlist vendor', 'jlkjl', '1', '2016-10-17 10:09:41', 'redlist'),
(92, 14, 3, 'Approved and process to SM IBK', 'change to inactive vendor PT.VJ', '1', '2016-10-18 09:26:17', 'inactive'),
(93, 28, 6, 'Approved to inactive', 'Set to inactive2', '1', '2016-10-18 10:02:18', 'inactive'),
(94, 12, 6, 'Approved to inactive', 'Dijadikan Inactive', '1', '2016-10-18 10:06:03', 'inactive'),
(95, 53, 3, 'Rejected inactive process', 'tidak valid', '1', '2016-10-18 14:49:41', 'inactive'),
(96, 53, 3, 'Approved and process to SM IBK', 'tolong di approve', '1', '2016-10-18 14:55:30', 'inactive'),
(97, 73, 2, 'add vendor to redlist', 'bandel', '1', '2016-10-18 15:07:38', 'redlist'),
(98, 73, 2, 'cancelled redlist vendor', 'cancelled redlist vendor', '1', '2016-10-18 15:34:49', 'redlist'),
(99, 73, 2, 'add vendor to redlist', 'jelek', '1', '2016-10-18 15:36:29', 'redlist'),
(100, 73, 3, 'approved redlist vendor', 'jelek', '1', '2016-10-18 15:43:39', 'redlist'),
(101, 17, 2, 'add vendor to blacklist', 'Req to Blacklist', '1', '2016-10-18 15:44:36', 'blacklist'),
(102, 17, 3, 'Approve Blacklist step 1', '', '1', '2016-10-18 15:46:08', 'blacklist'),
(103, 17, 4, 'Approve Blacklist step 2', '', '1', '2016-10-18 15:59:46', 'blacklist'),
(104, 17, 6, 'Approve Blacklist step 3', '', '1', '2016-10-18 16:00:11', 'blacklist'),
(105, 28, 3, 'Approved and process to SM IBK', 'mohon dijadikan active', '1', '2016-10-18 16:06:32', 'active'),
(106, 28, 4, 'Approved and process to VP IB', 'mohon dijadikan active', '1', '2016-10-18 16:08:58', 'active'),
(107, 28, 6, 'Approved to active', 'mohon dijadikan active', '1', '2016-10-18 16:10:09', 'active'),
(108, 21, 2, 'add vendor to redlist', 'sdfdsf', '1', '2016-10-18 17:47:39', 'redlist'),
(109, 21, 3, 'rejected redlist vendor', 'sdfdsf', '1', '2016-10-18 17:49:07', 'redlist'),
(110, 110, 2, 'add vendor to blacklist', 'request', '1', '2016-10-19 09:01:22', 'blacklist'),
(111, 110, 3, 'Approve Blacklist step 1', '', '1', '2016-10-19 09:02:02', 'blacklist'),
(112, 28, 2, 'add vendor to blacklist', 'Expired document..', '1', '2016-10-19 10:32:58', 'blacklist'),
(113, 101, 2, 'add vendor to blacklist', 'Jadikan blacklist aja', '1', '2016-10-19 10:33:58', 'blacklist'),
(114, 6, 2, 'add vendor to blacklist', 'Document sudah expired', '1', '2016-10-19 10:42:02', 'blacklist');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_promote`
--

CREATE TABLE IF NOT EXISTS `vendor_promote` (
`id` int(11) NOT NULL,
  `id_vendor` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` smallint(6) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `vendor_promote`
--

INSERT INTO `vendor_promote` (`id`, `id_vendor`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(9, 52, '1', 2, '2016-10-18 08:56:47', '2016-10-18 08:56:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_recomend`
--

CREATE TABLE IF NOT EXISTS `vendor_recomend` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_category` smallint(5) DEFAULT NULL,
  `id_subcat` smallint(5) DEFAULT NULL,
  `result` enum('reject','shortlist','avl') NOT NULL,
  `note` varchar(100) NOT NULL,
  `check` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=82 ;

--
-- Dumping data untuk tabel `vendor_recomend`
--

INSERT INTO `vendor_recomend` (`id`, `id_vendor`, `id_category`, `id_subcat`, `result`, `note`, `check`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 1, 5, 5, 'avl', 'test', '1', 2, '2016-09-08 10:14:46', '2016-09-08 11:46:21'),
(2, 1, 5, 6, 'avl', 'test', '1', 2, '2016-09-08 10:14:46', '2016-09-08 11:46:21'),
(3, 2, 3, 3, 'avl', 'Rekomendasi menjadi AVL untuk sub category Dessert 1 ', '1', 2, '2016-09-08 11:10:59', '2016-09-08 11:10:59'),
(4, 3, 1, 1, 'avl', 'lumayan lah dari pada ga ada vendor', '1', 2, '2016-09-09 16:02:29', '2016-09-09 16:02:29'),
(6, 5, 1, 1, 'reject', 'Rekomendasi', '1', 2, '2016-09-09 19:04:42', '2016-09-15 18:51:14'),
(7, 5, 1, 2, 'reject', 'Rekomendasi', '1', 2, '2016-09-09 19:04:42', '2016-09-15 18:51:14'),
(8, 6, 1, 1, 'avl', 'kalo aku sih yes!', '1', 2, '2016-09-14 19:08:15', '2016-09-14 19:08:15'),
(9, 6, 1, 2, 'avl', 'kalo aku sih yes!', '1', 2, '2016-09-14 19:08:15', '2016-09-14 19:08:15'),
(10, 6, 5, 5, 'avl', 'kalo aku sih yes!', '1', 2, '2016-09-14 19:08:15', '2016-09-14 19:08:15'),
(11, 6, 5, 6, 'avl', 'kalo aku sih yes!', '1', 2, '2016-09-14 19:08:15', '2016-09-14 19:08:15'),
(12, 6, 3, 3, 'avl', 'kalo aku sih yes!', '1', 2, '2016-09-14 19:08:15', '2016-09-14 19:08:15'),
(13, 6, 2, 8, 'avl', 'kalo aku sih yes!', '1', 2, '2016-09-14 19:08:15', '2016-09-14 19:08:15'),
(14, 7, 2, 8, 'avl', '', '1', 2, '2016-09-15 15:49:07', '2016-09-15 15:49:07'),
(15, 7, 3, 3, 'avl', '', '1', 2, '2016-09-15 15:49:07', '2016-09-15 15:49:07'),
(16, 14, 1, 1, 'avl', '', '1', 2, '2016-09-19 16:50:23', '2016-09-20 10:11:53'),
(17, 14, 3, 3, 'avl', '', '1', 2, '2016-09-19 16:50:23', '2016-09-20 10:11:53'),
(18, 12, 3, 3, 'avl', '', '1', 2, '2016-09-20 10:25:28', '2016-09-20 10:25:28'),
(19, 15, 1, 1, 'avl', 'SAYA SETUJU', '1', 2, '2016-09-20 20:07:23', '2016-09-20 20:07:23'),
(20, 15, 3, 3, 'avl', 'SAYA SETUJU', '1', 2, '2016-09-20 20:07:23', '2016-09-20 20:07:23'),
(23, 23, 5, 5, 'avl', 'SETUJU', '1', 2, '2016-09-20 20:38:13', '2016-09-20 20:38:13'),
(24, 22, 6, 4, 'avl', 'SAWTUJU', '1', 2, '2016-09-20 20:38:57', '2016-09-20 20:38:57'),
(25, 21, 6, 4, 'avl', 'SETUJU', '1', 2, '2016-09-20 20:39:26', '2016-09-20 20:39:26'),
(26, 26, 1, 1, 'shortlist', 'okedeh', '1', 2, '2016-09-22 10:33:01', '2016-09-22 10:33:01'),
(27, 26, 5, 5, 'shortlist', 'okedeh', '1', 2, '2016-09-22 10:33:01', '2016-09-22 10:33:01'),
(28, 26, 2, 8, 'shortlist', 'okedeh', '1', 2, '2016-09-22 10:33:01', '2016-09-22 10:33:01'),
(29, 26, 7, 7, 'shortlist', 'okedeh', '1', 2, '2016-09-22 10:33:01', '2016-09-22 10:33:01'),
(30, 28, 1, 1, 'avl', 'Rekomendasi', '1', 2, '2016-09-27 14:32:34', '2016-09-27 14:32:34'),
(31, 28, 1, 2, 'avl', 'Rekomendasi', '1', 2, '2016-09-27 14:32:34', '2016-09-27 14:32:34'),
(32, 43, 3, 3, 'avl', 'Okay', '1', 2, '2016-09-30 11:34:56', '2016-09-30 11:34:56'),
(33, 43, 6, 4, 'avl', 'Okay', '1', 2, '2016-09-30 11:34:56', '2016-09-30 11:34:56'),
(34, 44, 1, 16, 'avl', 'ok', '1', 2, '2016-09-30 14:51:13', '2016-09-30 14:51:13'),
(35, 44, 3, 3, 'avl', 'ok', '1', 2, '2016-09-30 14:51:13', '2016-09-30 14:51:13'),
(36, 11, 1, 1, 'avl', 'tes hanya 4 category yg di terima', '1', 2, '2016-09-30 16:12:48', '2016-09-30 16:12:48'),
(37, 11, 1, 2, 'avl', 'tes hanya 4 category yg di terima', '1', 2, '2016-09-30 16:12:48', '2016-09-30 16:12:48'),
(38, 11, 5, 5, 'avl', 'tes hanya 4 category yg di terima', '1', 2, '2016-09-30 16:12:48', '2016-09-30 16:12:48'),
(39, 11, 2, 8, 'avl', 'tes hanya 4 category yg di terima', '1', 2, '2016-09-30 16:12:48', '2016-09-30 16:12:48'),
(40, 40, 1, 1, 'reject', 'reject by admin sourcing', '1', 2, '2016-10-03 09:48:14', '2016-10-03 10:11:02'),
(41, 39, 1, 1, 'reject', 'ewqihg', '1', 2, '2016-10-03 09:50:49', '2016-10-03 09:50:49'),
(42, 38, 1, 1, 'avl', 'AVL - Reject by SM IBS', '1', 2, '2016-10-03 09:53:00', '2016-10-03 09:53:00'),
(43, 37, 1, 1, 'avl', 'AVL Reject by SM IBK', '1', 2, '2016-10-03 09:54:01', '2016-10-03 09:54:01'),
(44, 36, 1, 1, 'avl', 'AVL Reject by VP IB', '1', 2, '2016-10-03 09:54:53', '2016-10-03 09:54:53'),
(45, 35, 1, 1, 'shortlist', 'shortlist Reject by SM IBS', '1', 2, '2016-10-03 09:55:55', '2016-10-03 09:55:55'),
(46, 34, 1, 1, 'shortlist', 'Shortlist Reject By SM IBK', '1', 2, '2016-10-03 09:56:49', '2016-10-03 09:56:49'),
(47, 33, 1, 1, 'shortlist', 'shortlist reject by VP IB', '1', 2, '2016-10-03 09:58:17', '2016-10-03 09:58:17'),
(48, 42, 1, 1, 'avl', 'reject by admin', '1', 2, '2016-10-03 10:21:03', '2016-10-11 11:20:30'),
(49, 55, 1, 1, 'avl', 'hanya 2 yg oke', '1', 2, '2016-10-03 15:17:16', '2016-10-03 15:17:16'),
(50, 55, 1, 2, 'avl', 'hanya 2 yg oke', '1', 2, '2016-10-03 15:17:16', '2016-10-03 15:17:16'),
(51, 54, 3, 3, 'avl', 'jadi AVL', '1', 2, '2016-10-04 10:43:42', '2016-10-04 10:43:42'),
(52, 48, 1, 1, 'avl', 'qwerty', '1', 2, '2016-10-05 11:21:58', '2016-10-05 11:21:58'),
(53, 61, 1, 15, 'avl', 'AVL', '1', 2, '2016-10-07 09:33:38', '2016-10-07 09:33:38'),
(54, 61, 1, 14, 'avl', 'AVL', '1', 2, '2016-10-07 09:33:38', '2016-10-07 09:33:38'),
(55, 56, 1, 1, 'reject', '', '1', 2, '2016-10-11 08:13:24', '2016-10-11 08:59:16'),
(56, 56, 1, 15, 'reject', '', '1', 2, '2016-10-11 08:13:24', '2016-10-11 08:59:16'),
(57, 56, 1, 14, 'reject', '', '1', 2, '2016-10-11 08:13:24', '2016-10-11 08:59:16'),
(58, 56, 1, 16, 'reject', '', '1', 2, '2016-10-11 08:13:24', '2016-10-11 08:59:16'),
(59, 56, 2, 8, 'reject', '', '1', 2, '2016-10-11 08:13:24', '2016-10-11 08:59:16'),
(60, 53, 3, 3, 'shortlist', 'coba di shortlist', '1', 2, '2016-10-11 08:48:26', '2016-10-11 09:25:36'),
(61, 65, 3, 3, 'reject', 'xzczxczxczc', '1', 2, '2016-10-11 09:39:09', '2016-10-11 09:39:09'),
(62, 52, 3, 3, 'shortlist', '', '1', 2, '2016-10-11 10:40:12', '2016-10-11 10:40:12'),
(63, 66, 1, 1, 'avl', 'promote to AVL', '1', 2, '2016-10-11 15:49:52', '2016-10-11 15:49:52'),
(64, 66, 1, 15, 'avl', 'promote to AVL', '1', 2, '2016-10-11 15:49:52', '2016-10-11 15:49:52'),
(65, 66, 2, 8, 'avl', 'promote to AVL', '1', 2, '2016-10-11 15:49:52', '2016-10-11 15:49:52'),
(66, 68, 1, 2, 'avl', 'sdfadfsdfsdf', '1', 2, '2016-10-13 11:30:08', '2016-10-13 11:30:08'),
(67, 73, 1, 1, 'avl', '', '1', 2, '2016-10-13 13:35:44', '2016-10-13 13:35:44'),
(68, 73, 1, 15, 'avl', '', '1', 2, '2016-10-13 13:35:44', '2016-10-13 13:35:44'),
(69, 73, 1, 14, 'avl', '', '1', 2, '2016-10-13 13:35:44', '2016-10-13 13:35:44'),
(70, 101, 1, 1, 'avl', 'Trusted', '1', 2, '2016-10-17 09:15:26', '2016-10-17 09:15:26'),
(71, 101, 1, 2, 'avl', 'Trusted', '1', 2, '2016-10-17 09:15:26', '2016-10-17 09:15:26'),
(72, 101, 1, 20, 'avl', 'Trusted', '1', 2, '2016-10-17 09:15:26', '2016-10-17 09:15:26'),
(73, 113, 5, 12, 'avl', 'ok', '1', 2, '2016-10-18 19:02:02', '2016-10-18 19:02:02'),
(74, 113, 5, 6, 'avl', 'ok', '1', 2, '2016-10-18 19:02:02', '2016-10-18 19:02:02'),
(75, 112, 1, 14, 'shortlist', 'ok', '1', 2, '2016-10-18 19:02:37', '2016-10-18 19:02:37'),
(76, 112, 1, 2, 'shortlist', 'ok', '1', 2, '2016-10-18 19:02:37', '2016-10-18 19:02:37'),
(77, 111, 1, 15, 'reject', 'reject', '1', 2, '2016-10-18 19:03:41', '2016-10-18 21:31:07'),
(78, 111, 1, 16, 'reject', 'reject', '1', 2, '2016-10-18 19:03:41', '2016-10-18 21:31:07'),
(79, 110, 5, 13, 'avl', 'black', '1', 2, '2016-10-18 19:04:12', '2016-10-18 19:04:12'),
(80, 110, 5, 6, 'avl', 'black', '1', 2, '2016-10-18 19:04:12', '2016-10-18 19:04:12'),
(81, 110, 5, 5, 'avl', 'black', '1', 2, '2016-10-18 19:04:12', '2016-10-18 19:04:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_recommend`
--

CREATE TABLE IF NOT EXISTS `vendor_recommend` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `type` enum('reject','shortlist','avl') NOT NULL,
  `note` varchar(100) NOT NULL,
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_redlist`
--

CREATE TABLE IF NOT EXISTS `vendor_redlist` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `start_date` date NOT NULL,
  `remark` varchar(50) NOT NULL,
  `counter` smallint(5) NOT NULL,
  `doc_name` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT NULL,
  `status` enum('wait','approved','cancel','rejected') NOT NULL DEFAULT 'wait',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data untuk tabel `vendor_redlist`
--

INSERT INTO `vendor_redlist` (`id`, `id_vendor`, `start_date`, `remark`, `counter`, `doc_name`, `doc_type`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(2, 17, '2016-09-07', 'nnn', 2, NULL, NULL, 'approved', 3, '2016-09-07 00:00:00', '2016-09-22 00:00:00'),
(3, 23, '2016-09-26', 'testing vendor redlist', 2, '0', '0', 'approved', 2, '2016-09-26 15:32:33', '2016-09-26 15:32:33'),
(4, 14, '2016-09-28', 'rejected', 2, '0', '0', 'rejected', 2, '2016-09-28 16:08:56', '2016-09-28 16:08:56'),
(5, 6, '2016-09-29', 'bmn', 2, 'redlist_620160928183002', 'xlsx', 'approved', 1, '2016-09-28 18:30:02', '2016-09-28 18:30:02'),
(10, 2, '2016-09-28', 'kjhkj', 2, 'redlist_220160928200656', 'xlsx', 'wait', 1, '2016-10-05 16:05:53', '2016-10-05 16:05:53'),
(11, 15, '2016-09-29', 'jhjkhj', 2, '0', '0', 'approved', 2, '2016-09-29 09:38:37', '2016-09-29 09:38:37'),
(13, 14, '2016-09-29', 'Coba email', 2, 'redlist_1420160929151056', 'txt', 'approved', 2, '2016-09-29 15:10:56', '2016-09-29 15:10:56'),
(21, 21, '2016-10-06', 'aaaaa', 2, NULL, NULL, 'cancel', 2, '2016-10-06 14:24:30', '2016-10-06 14:24:30'),
(23, 2, '2016-10-17', 'jlkjl', 2, NULL, NULL, 'approved', 2, '2016-10-17 09:43:18', '2016-10-17 09:43:18'),
(24, 73, '2016-10-19', 'bandel', 2, NULL, NULL, 'cancel', 2, '2016-10-18 15:07:38', '2016-10-18 15:07:38'),
(25, 73, '2016-10-26', 'jelek', 2, NULL, NULL, 'approved', 2, '2016-10-18 15:36:29', '2016-10-18 15:36:29'),
(26, 21, '2016-10-18', 'sdfdsf', 2, NULL, NULL, 'rejected', 2, '2016-10-18 17:47:39', '2016-10-18 17:47:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_register`
--

CREATE TABLE IF NOT EXISTS `vendor_register` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `session` varchar(100) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Dumping data untuk tabel `vendor_register`
--

INSERT INTO `vendor_register` (`id`, `id_vendor`, `session`, `status`, `created_date`, `last_updated`) VALUES
(1, 1, '57cfe56a89883', '1', '2016-09-07 17:01:14', '2016-09-07 17:01:14'),
(2, 2, '57d0d7c2079c3', '0', '2016-09-08 10:15:14', '2016-09-08 10:18:55'),
(3, 3, '57d275e393fcb', '0', '2016-09-09 15:42:11', '2016-09-09 15:45:41'),
(5, 5, '57d2a2b969a61', '0', '2016-09-09 18:53:29', '2016-09-09 18:55:03'),
(6, 6, '57d8c57bab0fc', '0', '2016-09-14 10:35:23', '2016-09-14 11:42:25'),
(7, 7, '57da5d391f574', '0', '2016-09-15 15:35:05', '2016-09-15 15:42:04'),
(8, 8, '57da61315bd5d', '0', '2016-09-15 15:52:01', '2016-09-15 15:52:59'),
(9, 9, '57da8cdc809dc', '0', '2016-09-15 18:58:20', '2016-09-15 19:45:13'),
(10, 10, '57db63686c828', '0', '2016-09-16 10:13:44', '2016-09-16 13:56:44'),
(11, 11, '57db68ca5ff35', '0', '2016-09-16 10:36:42', '2016-09-26 14:39:09'),
(12, 12, '57db97a811625', '0', '2016-09-16 13:56:40', '2016-09-16 13:56:40'),
(13, 13, '57db9df42072b', '0', '2016-09-16 14:23:32', '2016-09-28 09:48:11'),
(14, 14, '57dfb197012f3', '0', '2016-09-19 16:36:23', '2016-09-19 16:47:15'),
(15, 15, '57e0ae56928de', '0', '2016-09-20 10:34:46', '2016-09-20 13:27:38'),
(16, 17, 'd5ff6301b354ed8227136049c7e0db77', '0', '2016-09-20 13:27:30', '2016-09-20 13:27:30'),
(17, 18, '57e0e30420fd5', '0', '2016-09-20 14:19:32', '2016-09-20 17:22:12'),
(18, 19, 'de8b815d7ac0af0a783579a6969926a1', '0', '2016-09-20 18:59:07', '2016-09-20 20:21:04'),
(20, 21, '7da6a703e0252512cb2a20dc0bc42298', '0', '2016-09-20 20:12:37', '2016-09-20 20:39:10'),
(21, 22, 'f3fb64a1cd4144dbf0fa4d3a97a73574', '0', '2016-09-20 20:18:47', '2016-09-20 20:38:41'),
(22, 23, 'cfef4adcf4642c2ac57e2fbef9df465c', '0', '2016-09-20 20:20:34', '2016-09-20 20:37:57'),
(23, 24, '6f3adcbe6a07d6374512035344252f36', '1', '2016-09-20 20:33:19', '2016-09-20 20:33:19'),
(25, 26, '57e206393d754', '0', '2016-09-21 11:02:01', '2016-09-21 11:02:52'),
(26, 27, '57e9d082b95ee', '0', '2016-09-27 08:50:58', '2016-09-28 10:05:32'),
(27, 28, '24045374b9cf216f5641302c4951abf0', '0', '2016-09-27 14:21:55', '2016-09-27 14:26:02'),
(28, 42, '9b562b84167984fe9288e2bf21a14568', '0', '2016-09-29 10:33:00', '2016-10-03 10:20:18'),
(29, 43, '693ef8b75cc196cf8921023187a0b822', '0', '2016-09-30 11:30:28', '2016-09-30 11:34:05'),
(30, 44, '57edee0f5c990', '0', '2016-09-30 11:46:07', '2016-09-30 11:46:18'),
(31, 56, '5fcb602d78d2218042ddc3ef865bb729', '0', '2016-10-06 08:10:37', '2016-10-06 13:32:33'),
(32, 61, '2f9aea3065040f9d2d69a0016afd65f8', '0', '2016-10-06 14:02:53', '2016-10-06 14:35:34'),
(33, 65, '852833c636a9952bd3329a55fb1606ee', '0', '2016-10-10 15:11:40', '2016-10-11 09:38:13'),
(34, 66, '187caaa68a9a4d5f3bf9b93b683bd198', '0', '2016-10-11 15:27:46', '2016-10-11 15:34:01'),
(35, 73, '7719ef895c810ac985497cf3f3a18469', '0', '2016-10-13 13:33:21', '2016-10-13 13:34:06'),
(36, 95, 'a3e4c1d30bde3957e9ce6f034bfc58fa', '1', '2016-10-13 22:26:03', '2016-10-13 22:26:03'),
(37, 101, '263c890aac2ef3bc3d443d3c233e2d2d', '0', '2016-10-17 09:08:22', '2016-10-17 09:09:55'),
(38, 110, 'e97a1680da5f92a5246f39d754102ceb', '0', '2016-10-18 18:46:34', '2016-10-18 19:03:54'),
(39, 111, '17bdcc98bdf7cc0844d76e45c83776d1', '0', '2016-10-18 18:52:06', '2016-10-18 19:02:56'),
(40, 112, '95fa078fefd178d82d4bfa3336bd5160', '0', '2016-10-18 18:54:04', '2016-10-18 19:02:15'),
(41, 113, '57b7446d4b0e402e596fe503d593d46d', '0', '2016-10-18 18:59:54', '2016-10-18 19:01:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_register_temp`
--

CREATE TABLE IF NOT EXISTS `vendor_register_temp` (
`id` mediumint(11) unsigned NOT NULL,
  `reg_num` varchar(20) NOT NULL,
  `status` enum('10','9','8','7','6','5','4','3','2','1') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_register_tracking`
--

CREATE TABLE IF NOT EXISTS `vendor_register_tracking` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `id_stsreg` smallint(5) NOT NULL DEFAULT '1',
  `notes` varchar(100) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=278 ;

--
-- Dumping data untuk tabel `vendor_register_tracking`
--

INSERT INTO `vendor_register_tracking` (`id`, `id_vendor`, `id_stsreg`, `notes`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 13, 2, 'Change status with General Review', '1', 2, '2016-09-28 09:48:01', '2016-09-28 09:48:01'),
(2, 13, 3, 'Change status with Administration Review', '1', 2, '2016-09-28 09:48:11', '2016-09-28 09:48:11'),
(3, 27, 2, 'Change status with General Review', '1', 2, '2016-09-28 10:05:27', '2016-09-28 10:05:27'),
(4, 27, 3, 'Change status with Administration Review', '1', 2, '2016-09-28 10:05:32', '2016-09-28 10:05:32'),
(5, 23, 7, 'Registration', '1', 0, '2016-09-28 15:31:22', '2016-09-28 15:31:22'),
(6, 25, 7, 'Registration', '1', 0, '2016-09-28 15:51:27', '2016-09-28 15:51:27'),
(7, 26, 6, 'Registration', '1', 0, '2016-09-28 16:01:51', '2016-09-28 16:01:51'),
(8, 14, 8, 'Vendor Finalization', '1', 2, '2016-09-28 16:02:11', '2016-09-28 16:02:11'),
(9, 27, 3, 'Registration', '1', 0, '2016-09-28 16:09:45', '2016-09-28 16:09:45'),
(10, 28, 8, 'Registration', '1', 0, '2016-09-28 16:23:51', '2016-09-28 16:23:51'),
(11, 29, 1, 'Registration', '1', 0, '2016-09-28 16:26:07', '2016-09-28 16:26:07'),
(12, 30, 1, 'Registration', '1', 0, '2016-09-28 16:31:28', '2016-09-28 16:31:28'),
(13, 31, 1, 'Registration', '1', 0, '2016-09-28 16:40:39', '2016-09-28 16:40:39'),
(14, 32, 1, 'Registration', '1', 0, '2016-09-29 10:03:39', '2016-09-29 10:03:39'),
(15, 42, 1, 'Registration', '1', 0, '2016-09-29 10:33:00', '2016-09-29 10:33:00'),
(16, 11, 4, 'Change status with Due-diligence', '1', 2, '2016-09-30 10:44:36', '2016-09-30 10:44:36'),
(17, 43, 1, 'Registration', '1', 0, '2016-09-30 11:30:28', '2016-09-30 11:30:28'),
(18, 43, 2, 'Change status with General Review', '1', 2, '2016-09-30 11:34:04', '2016-09-30 11:34:04'),
(19, 43, 3, 'Change status with Administration Review', '1', 2, '2016-09-30 11:34:05', '2016-09-30 11:34:05'),
(20, 43, 4, 'Change status with Due-diligence', '1', 2, '2016-09-30 11:34:07', '2016-09-30 11:34:07'),
(21, 43, 4, 'Avl Vendor Recomendation', '1', 2, '2016-09-30 11:34:56', '2016-09-30 11:34:56'),
(22, 43, 5, 'Avl Vendor Approved', '1', 3, '2016-09-30 11:35:56', '2016-09-30 11:35:56'),
(23, 43, 5, 'Avl Vendor Approved', '1', 4, '2016-09-30 11:36:48', '2016-09-30 11:36:48'),
(24, 43, 5, 'Avl Vendor Approved', '1', 6, '2016-09-30 11:39:32', '2016-09-30 11:39:32'),
(25, 44, 1, 'Registration', '1', 2, '2016-09-30 11:46:07', '2016-09-30 11:46:07'),
(26, 44, 2, 'Change status with General Review', '1', 2, '2016-09-30 11:46:17', '2016-09-30 11:46:17'),
(27, 44, 3, 'Change status with Administration Review', '1', 2, '2016-09-30 11:46:18', '2016-09-30 11:46:18'),
(28, 44, 4, 'Change status with Due-diligence', '1', 2, '2016-09-30 11:46:20', '2016-09-30 11:46:20'),
(29, 44, 4, 'Avl Vendor Recomendation', '1', 2, '2016-09-30 14:51:13', '2016-09-30 14:51:13'),
(30, 44, 5, 'Avl Vendor Approved', '1', 3, '2016-09-30 14:51:42', '2016-09-30 14:51:42'),
(31, 44, 5, 'Avl Vendor Approved', '1', 4, '2016-09-30 14:52:23', '2016-09-30 14:52:23'),
(32, 44, 5, 'Avl Vendor Approved', '1', 6, '2016-09-30 14:52:45', '2016-09-30 14:52:45'),
(33, 11, 4, 'Avl Vendor Recomendation', '1', 2, '2016-09-30 16:12:48', '2016-09-30 16:12:48'),
(34, 30, 1, 'Rejected Vendor Registration', '1', 2, '2016-10-03 09:29:01', '2016-10-03 09:29:01'),
(35, 40, 2, 'Change status with General Review', '1', 2, '2016-10-03 09:43:04', '2016-10-03 09:43:04'),
(36, 40, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 09:43:13', '2016-10-03 09:43:13'),
(37, 40, 4, 'Change status with Due-diligence', '1', 2, '2016-10-03 09:45:09', '2016-10-03 09:45:09'),
(38, 40, 4, 'Reject Vendor Recomendation', '1', 2, '2016-10-03 09:48:14', '2016-10-03 09:48:14'),
(39, 39, 2, 'Change status with General Review', '1', 2, '2016-10-03 09:50:11', '2016-10-03 09:50:11'),
(40, 39, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 09:50:14', '2016-10-03 09:50:14'),
(41, 39, 4, 'Change status with Due-diligence', '1', 2, '2016-10-03 09:50:17', '2016-10-03 09:50:17'),
(42, 39, 4, 'Reject Vendor Recomendation', '1', 2, '2016-10-03 09:50:49', '2016-10-03 09:50:49'),
(43, 38, 2, 'Change status with General Review', '1', 2, '2016-10-03 09:52:03', '2016-10-03 09:52:03'),
(44, 38, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 09:52:04', '2016-10-03 09:52:04'),
(45, 38, 4, 'Change status with Due-diligence', '1', 2, '2016-10-03 09:52:07', '2016-10-03 09:52:07'),
(46, 38, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-03 09:53:00', '2016-10-03 09:53:00'),
(47, 37, 2, 'Change status with General Review', '1', 2, '2016-10-03 09:53:29', '2016-10-03 09:53:29'),
(48, 37, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 09:53:30', '2016-10-03 09:53:30'),
(49, 37, 4, 'Change status with Due-diligence', '1', 2, '2016-10-03 09:53:33', '2016-10-03 09:53:33'),
(50, 37, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-03 09:54:01', '2016-10-03 09:54:01'),
(51, 36, 2, 'Change status with General Review', '1', 2, '2016-10-03 09:54:25', '2016-10-03 09:54:25'),
(52, 36, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 09:54:26', '2016-10-03 09:54:26'),
(53, 36, 4, 'Change status with Due-diligence', '1', 2, '2016-10-03 09:54:29', '2016-10-03 09:54:29'),
(54, 36, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-03 09:54:53', '2016-10-03 09:54:53'),
(55, 35, 2, 'Change status with General Review', '1', 2, '2016-10-03 09:55:25', '2016-10-03 09:55:25'),
(56, 35, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 09:55:27', '2016-10-03 09:55:27'),
(57, 35, 4, 'Change status with Due-diligence', '1', 2, '2016-10-03 09:55:30', '2016-10-03 09:55:30'),
(58, 35, 4, 'Shortlist Vendor Recomendation', '1', 2, '2016-10-03 09:55:55', '2016-10-03 09:55:55'),
(59, 34, 2, 'Change status with General Review', '1', 2, '2016-10-03 09:56:15', '2016-10-03 09:56:15'),
(60, 34, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 09:56:17', '2016-10-03 09:56:17'),
(61, 34, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 09:56:19', '2016-10-03 09:56:19'),
(62, 34, 4, 'Change status with Due-diligence', '1', 2, '2016-10-03 09:56:22', '2016-10-03 09:56:22'),
(63, 34, 4, 'Shortlist Vendor Recomendation', '1', 2, '2016-10-03 09:56:49', '2016-10-03 09:56:49'),
(64, 33, 2, 'Change status with General Review', '1', 2, '2016-10-03 09:57:43', '2016-10-03 09:57:43'),
(65, 33, 2, 'Change status with General Review', '1', 2, '2016-10-03 09:57:45', '2016-10-03 09:57:45'),
(66, 33, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 09:57:47', '2016-10-03 09:57:47'),
(67, 33, 4, 'Change status with Due-diligence', '1', 2, '2016-10-03 09:57:50', '2016-10-03 09:57:50'),
(68, 33, 4, 'Shortlist Vendor Recomendation', '1', 2, '2016-10-03 09:58:17', '2016-10-03 09:58:17'),
(69, 33, 2, 'Change status with General Review', '1', 2, '2016-10-03 09:58:54', '2016-10-03 09:58:54'),
(70, 33, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 09:58:56', '2016-10-03 09:58:56'),
(71, 33, 4, 'Change status with Due-diligence', '1', 2, '2016-10-03 09:58:58', '2016-10-03 09:58:58'),
(72, 40, 5, 'masih bisa di toleransi', '1', 3, '2016-10-03 10:10:30', '2016-10-03 10:10:30'),
(73, 40, 4, 'Reject Vendor Recomendation', '1', 2, '2016-10-03 10:11:02', '2016-10-03 10:11:02'),
(74, 40, 2, 'Change status with General Review', '1', 2, '2016-10-03 10:14:56', '2016-10-03 10:14:56'),
(75, 40, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 10:14:59', '2016-10-03 10:14:59'),
(76, 40, 4, 'Change status with Due-diligence', '1', 2, '2016-10-03 10:15:01', '2016-10-03 10:15:01'),
(77, 42, 2, 'Change status with General Review', '1', 2, '2016-10-03 10:20:16', '2016-10-03 10:20:16'),
(78, 42, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 10:20:18', '2016-10-03 10:20:18'),
(79, 42, 4, 'Change status with Due-diligence', '1', 2, '2016-10-03 10:20:20', '2016-10-03 10:20:20'),
(80, 42, 4, 'Reject Vendor Recomendation', '1', 2, '2016-10-03 10:21:03', '2016-10-03 10:21:03'),
(81, 42, 5, 'masih valid', '1', 3, '2016-10-03 10:21:46', '2016-10-03 10:21:46'),
(82, 42, 4, 'masih valid', '1', 3, '2016-10-03 10:21:50', '2016-10-03 10:21:50'),
(83, 42, 4, 'Reject Vendor Recomendation', '1', 2, '2016-10-03 10:23:46', '2016-10-03 10:23:46'),
(84, 42, 4, 'Reject Vendor Approved', '1', 3, '2016-10-03 10:24:51', '2016-10-03 10:24:51'),
(85, 47, 1, 'Registration', '1', 0, '2016-10-03 10:28:11', '2016-10-03 10:28:11'),
(86, 54, 0, 'Registration', '1', 0, '2016-10-03 13:35:31', '2016-10-03 13:35:31'),
(87, 55, 0, 'Registration', '1', 0, '2016-10-03 13:36:27', '2016-10-03 13:36:27'),
(88, 56, 0, 'Registration', '1', 0, '2016-10-03 14:47:33', '2016-10-03 14:47:33'),
(89, 55, 2, 'Change status with General Review', '1', 2, '2016-10-03 14:53:24', '2016-10-03 14:53:24'),
(90, 55, 3, 'Change status with Administration Review', '1', 2, '2016-10-03 14:54:45', '2016-10-03 14:54:45'),
(91, 55, 4, 'Change status with Due-diligence', '1', 2, '2016-10-03 14:56:36', '2016-10-03 14:56:36'),
(92, 55, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-03 15:17:16', '2016-10-03 15:17:16'),
(93, 55, 5, 'Avl Vendor Approved', '1', 3, '2016-10-03 15:24:37', '2016-10-03 15:24:37'),
(94, 55, 5, 'Avl Vendor Approved', '1', 3, '2016-10-03 15:24:42', '2016-10-03 15:24:42'),
(95, 55, 5, 'Avl Vendor Approved', '1', 6, '2016-10-03 15:37:04', '2016-10-03 15:37:04'),
(96, 53, 2, 'Change status with General Review', '1', 2, '2016-10-04 10:05:54', '2016-10-04 10:05:54'),
(97, 53, 3, 'Change status with Administration Review', '1', 2, '2016-10-04 10:05:56', '2016-10-04 10:05:56'),
(98, 53, 4, 'Change status with Due-diligence', '1', 2, '2016-10-04 10:06:00', '2016-10-04 10:06:00'),
(99, 48, 2, 'Change status with General Review', '1', 2, '2016-10-04 10:25:03', '2016-10-04 10:25:03'),
(100, 48, 3, 'Change status with Administration Review', '1', 2, '2016-10-04 10:25:04', '2016-10-04 10:25:04'),
(101, 48, 3, 'Change status with Administration Review', '1', 2, '2016-10-04 10:25:07', '2016-10-04 10:25:07'),
(102, 48, 4, 'Change status with Due-diligence', '1', 2, '2016-10-04 10:25:09', '2016-10-04 10:25:09'),
(103, 54, 2, 'Change status with General Review', '1', 2, '2016-10-04 10:42:30', '2016-10-04 10:42:30'),
(104, 54, 3, 'Change status with Administration Review', '1', 2, '2016-10-04 10:42:32', '2016-10-04 10:42:32'),
(105, 54, 4, 'Change status with Due-diligence', '1', 2, '2016-10-04 10:43:02', '2016-10-04 10:43:02'),
(106, 54, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-04 10:43:42', '2016-10-04 10:43:42'),
(107, 54, 5, 'Avl Vendor Approved', '1', 3, '2016-10-04 10:44:43', '2016-10-04 10:44:43'),
(108, 54, 5, 'Avl Vendor Approved', '1', 4, '2016-10-04 10:45:13', '2016-10-04 10:45:13'),
(109, 54, 5, 'Avl Vendor Approved', '1', 6, '2016-10-04 10:45:42', '2016-10-04 10:45:42'),
(110, 52, 2, 'Change status with General Review', '1', 2, '2016-10-04 10:57:12', '2016-10-04 10:57:12'),
(111, 52, 3, 'Change status with Administration Review', '1', 2, '2016-10-04 10:57:13', '2016-10-04 10:57:13'),
(112, 52, 4, 'Change status with Due-diligence', '1', 2, '2016-10-04 10:57:15', '2016-10-04 10:57:15'),
(113, 44, 6, 'Vendor Verification', '1', 2, '2016-10-05 11:08:36', '2016-10-05 11:08:36'),
(114, 44, 6, 'Completed Reconciliation Account', '1', 7, '2016-10-05 11:20:07', '2016-10-05 11:20:07'),
(115, 44, 6, 'Completed Vendor Nr. SAP', '1', 8, '2016-10-05 11:20:40', '2016-10-05 11:20:40'),
(116, 48, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-05 11:21:58', '2016-10-05 11:21:58'),
(117, 48, 5, 'Avl Vendor Approved', '1', 3, '2016-10-05 11:22:45', '2016-10-05 11:22:45'),
(118, 56, 1, 'Registration', '1', 0, '2016-10-06 08:10:37', '2016-10-06 08:10:37'),
(119, 56, 2, 'Change status with General Review', '1', 1, '2016-10-06 13:31:27', '2016-10-06 13:31:27'),
(120, 56, 3, 'Change status with Administration Review', '1', 2, '2016-10-06 13:32:33', '2016-10-06 13:32:33'),
(121, 56, 4, 'Change status with Due-diligence', '1', 1, '2016-10-06 13:49:02', '2016-10-06 13:49:02'),
(122, 61, 1, 'Registration', '1', 0, '2016-10-06 14:02:53', '2016-10-06 14:02:53'),
(123, 61, 2, 'Change status with General Review', '1', 2, '2016-10-06 14:24:12', '2016-10-06 14:24:12'),
(124, 61, 3, 'Change status with Administration Review', '1', 2, '2016-10-06 14:35:33', '2016-10-06 14:35:33'),
(125, 61, 4, 'Change status with Due-diligence', '1', 2, '2016-10-06 14:37:22', '2016-10-06 14:37:22'),
(126, 61, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-07 09:33:38', '2016-10-07 09:33:38'),
(127, 61, 5, 'Avl Vendor Approved', '1', 3, '2016-10-07 09:57:17', '2016-10-07 09:57:17'),
(128, 61, 5, 'Avl Vendor Approved', '1', 4, '2016-10-07 09:59:50', '2016-10-07 09:59:50'),
(129, 61, 5, 'Avl Vendor Approved', '1', 6, '2016-10-07 10:03:38', '2016-10-07 10:03:38'),
(130, 8, 0, 'Registration', '1', 0, '2016-10-07 13:08:59', '2016-10-07 13:08:59'),
(131, 61, 6, 'Vendor Verification', '1', 2, '2016-10-07 14:28:12', '2016-10-07 14:28:12'),
(132, 61, 6, 'Completed Reconciliation Account', '1', 7, '2016-10-07 14:43:12', '2016-10-07 14:43:12'),
(133, 61, 6, 'Completed Vendor Nr. SAP', '1', 8, '2016-10-10 09:10:42', '2016-10-10 09:10:42'),
(134, 9, 0, 'Registration', '1', 0, '2016-10-10 09:47:44', '2016-10-10 09:47:44'),
(135, 10, 4, 'Registration', '1', 0, '2016-10-10 10:03:52', '2016-10-10 10:03:52'),
(136, 61, 6, 'Completed SRM Username dan Password', '1', 15, '2016-10-10 10:18:16', '2016-10-10 10:18:16'),
(137, 65, 1, 'Registration', '1', 0, '2016-10-10 15:11:40', '2016-10-10 15:11:40'),
(138, 56, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-11 08:13:24', '2016-10-11 08:13:24'),
(139, 56, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-11 08:13:50', '2016-10-11 08:13:50'),
(140, 56, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-11 08:14:36', '2016-10-11 08:14:36'),
(141, 56, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-11 08:14:47', '2016-10-11 08:14:47'),
(142, 56, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-11 08:16:03', '2016-10-11 08:16:03'),
(143, 56, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-11 08:17:53', '2016-10-11 08:17:53'),
(144, 56, 5, 'coba di reject, apakah ', '1', 3, '2016-10-11 08:32:49', '2016-10-11 08:32:49'),
(145, 53, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-11 08:48:26', '2016-10-11 08:48:26'),
(146, 53, 5, 'Avl Vendor Approved', '1', 3, '2016-10-11 08:49:57', '2016-10-11 08:49:57'),
(147, 53, 5, 'SM IBK mereject nih', '1', 4, '2016-10-11 08:53:25', '2016-10-11 08:53:25'),
(148, 56, 4, 'Reject Vendor Recomendation', '1', 2, '2016-10-11 08:59:16', '2016-10-11 08:59:16'),
(149, 56, 4, 'Reject Vendor Approved', '1', 3, '2016-10-11 09:00:17', '2016-10-11 09:00:17'),
(150, 53, 4, 'Shortlist Vendor Recomendation', '1', 2, '2016-10-11 09:17:27', '2016-10-11 09:17:27'),
(151, 53, 5, 'Shortlist Vendor Approved', '1', 3, '2016-10-11 09:20:46', '2016-10-11 09:20:46'),
(152, 53, 5, 'Permohonan Shortlist di tolak', '1', 4, '2016-10-11 09:23:35', '2016-10-11 09:23:35'),
(153, 53, 4, 'Shortlist Vendor Recomendation', '1', 2, '2016-10-11 09:25:36', '2016-10-11 09:25:36'),
(154, 56, 4, 'Vendor Finalization', '1', 2, '2016-10-11 09:26:01', '2016-10-11 09:26:01'),
(155, 56, 4, 'Vendor Finalization', '1', 2, '2016-10-11 09:26:20', '2016-10-11 09:26:20'),
(156, 56, 4, 'Vendor Finalization', '1', 2, '2016-10-11 09:26:28', '2016-10-11 09:26:28'),
(157, 53, 5, 'Shortlist Vendor Approved', '1', 3, '2016-10-11 09:30:40', '2016-10-11 09:30:40'),
(158, 53, 5, 'Shortlist Vendor Approved', '1', 4, '2016-10-11 09:31:12', '2016-10-11 09:31:12'),
(159, 53, 5, 'Vendor Finalization', '1', 2, '2016-10-11 09:33:19', '2016-10-11 09:33:19'),
(160, 65, 2, 'Change status with General Review', '1', 2, '2016-10-11 09:37:29', '2016-10-11 09:37:29'),
(161, 65, 3, 'Change status with Administration Review', '1', 2, '2016-10-11 09:38:13', '2016-10-11 09:38:13'),
(162, 65, 4, 'Change status with Due-diligence', '1', 2, '2016-10-11 09:38:22', '2016-10-11 09:38:22'),
(163, 65, 4, 'Reject Vendor Recomendation', '1', 2, '2016-10-11 09:39:09', '2016-10-11 09:39:09'),
(164, 65, 4, 'Reject Vendor Approved', '1', 3, '2016-10-11 09:39:50', '2016-10-11 09:39:50'),
(165, 65, 4, 'Vendor Finalization', '1', 2, '2016-10-11 09:40:18', '2016-10-11 09:40:18'),
(166, 52, 4, 'Shortlist Vendor Recomendation', '1', 2, '2016-10-11 10:40:12', '2016-10-11 10:40:12'),
(167, 52, 5, 'Shortlist Vendor Approved', '1', 3, '2016-10-11 10:41:38', '2016-10-11 10:41:38'),
(168, 52, 5, 'Shortlist Vendor Approved', '1', 4, '2016-10-11 10:42:41', '2016-10-11 10:42:41'),
(169, 52, 5, 'Vendor Finalization', '1', 2, '2016-10-11 10:45:30', '2016-10-11 10:45:30'),
(170, 42, 4, 'Shortlist Vendor Recomendation', '1', 2, '2016-10-11 11:05:33', '2016-10-11 11:05:33'),
(171, 42, 5, 'Shortlist Vendor Recomendation', '1', 2, '2016-10-11 11:05:33', '2016-10-11 11:05:33'),
(172, 42, 5, '', '1', 3, '2016-10-11 11:13:04', '2016-10-11 11:13:04'),
(173, 42, 4, 'e2ouhfo2', '1', 3, '2016-10-11 11:13:05', '2016-10-11 11:13:05'),
(174, 42, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-11 11:20:24', '2016-10-11 11:20:24'),
(175, 42, 5, 'Avl Vendor Recomendation', '1', 2, '2016-10-11 11:20:30', '2016-10-11 11:20:30'),
(176, 42, 5, 'Avl Vendor Approved', '1', 3, '2016-10-11 11:28:16', '2016-10-11 11:28:16'),
(177, 42, 5, 'Avl Vendor Approved', '1', 4, '2016-10-11 13:30:35', '2016-10-11 13:30:35'),
(178, 42, 5, 'VP me-reject rekomendasi AVL', '1', 6, '2016-10-11 13:52:37', '2016-10-11 13:52:37'),
(179, 66, 1, 'Registration', '1', 0, '2016-10-11 15:27:46', '2016-10-11 15:27:46'),
(180, 66, 2, 'Change status with General Review', '1', 2, '2016-10-11 15:29:53', '2016-10-11 15:29:53'),
(181, 66, 2, 'Change status with General Review', '1', 2, '2016-10-11 15:29:54', '2016-10-11 15:29:54'),
(182, 66, 3, 'Change status with Administration Review', '1', 2, '2016-10-11 15:34:01', '2016-10-11 15:34:01'),
(183, 66, 4, 'Change status with Due-diligence', '1', 2, '2016-10-11 15:35:25', '2016-10-11 15:35:25'),
(184, 66, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-11 15:49:52', '2016-10-11 15:49:52'),
(185, 66, 5, 'Avl Vendor Approved', '1', 3, '2016-10-11 16:00:08', '2016-10-11 16:00:08'),
(186, 66, 5, 'Avl Vendor Approved', '1', 4, '2016-10-11 16:01:42', '2016-10-11 16:01:42'),
(187, 66, 5, 'Avl Vendor Approved', '1', 6, '2016-10-11 16:03:21', '2016-10-11 16:03:21'),
(188, 64, 2, 'Change status with General Review', '1', 2, '2016-10-12 07:36:50', '2016-10-12 07:36:50'),
(189, 64, 3, 'Change status with Administration Review', '1', 2, '2016-10-12 07:36:57', '2016-10-12 07:36:57'),
(190, 64, 4, 'Change status with Due-diligence', '1', 2, '2016-10-12 07:37:10', '2016-10-12 07:37:10'),
(191, 68, 2, 'Change status with General Review', '1', 2, '2016-10-13 11:29:03', '2016-10-13 11:29:03'),
(192, 68, 3, 'Change status with Administration Review', '1', 2, '2016-10-13 11:29:05', '2016-10-13 11:29:05'),
(193, 68, 4, 'Change status with Due-diligence', '1', 2, '2016-10-13 11:29:07', '2016-10-13 11:29:07'),
(194, 68, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-13 11:30:08', '2016-10-13 11:30:08'),
(195, 68, 5, 'Avl Vendor Approved', '1', 3, '2016-10-13 11:30:44', '2016-10-13 11:30:44'),
(196, 68, 5, 'Avl Vendor Approved', '1', 4, '2016-10-13 11:31:09', '2016-10-13 11:31:09'),
(197, 68, 5, 'Avl Vendor Approved', '1', 6, '2016-10-13 11:31:33', '2016-10-13 11:31:33'),
(198, 73, 1, 'Registration', '1', 0, '2016-10-13 13:33:21', '2016-10-13 13:33:21'),
(199, 73, 2, 'Change status with General Review', '1', 2, '2016-10-13 13:33:58', '2016-10-13 13:33:58'),
(200, 73, 3, 'Change status with Administration Review', '1', 2, '2016-10-13 13:34:06', '2016-10-13 13:34:06'),
(201, 73, 4, 'Change status with Due-diligence', '1', 2, '2016-10-13 13:34:11', '2016-10-13 13:34:11'),
(202, 73, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-13 13:35:44', '2016-10-13 13:35:44'),
(203, 73, 5, 'Avl Vendor Approved', '1', 3, '2016-10-13 13:36:15', '2016-10-13 13:36:15'),
(204, 73, 5, 'Avl Vendor Approved', '1', 4, '2016-10-13 13:36:55', '2016-10-13 13:36:55'),
(205, 73, 5, 'Avl Vendor Approved', '1', 6, '2016-10-13 13:37:40', '2016-10-13 13:37:40'),
(206, 73, 6, 'Vendor Verification', '1', 2, '2016-10-13 14:07:26', '2016-10-13 14:07:26'),
(207, 73, 6, 'Completed Reconciliation Account', '1', 7, '2016-10-13 14:10:57', '2016-10-13 14:10:57'),
(208, 73, 6, 'Completed Vendor Nr. SAP', '1', 8, '2016-10-13 14:16:03', '2016-10-13 14:16:03'),
(209, 73, 6, 'Completed SRM Username dan Password', '1', 15, '2016-10-13 14:34:31', '2016-10-13 14:34:31'),
(210, 73, 8, 'Vendor Finalization', '1', 2, '2016-10-13 14:40:50', '2016-10-13 14:40:50'),
(211, 62, 2, 'Change status with General Review', '1', 2, '2016-10-13 18:59:42', '2016-10-13 18:59:42'),
(212, 62, 2, 'Change status with General Review', '1', 2, '2016-10-13 18:59:46', '2016-10-13 18:59:46'),
(213, 62, 2, 'Change status with General Review', '1', 2, '2016-10-13 18:59:51', '2016-10-13 18:59:51'),
(214, 62, 2, 'Change status with General Review', '1', 2, '2016-10-13 18:59:53', '2016-10-13 18:59:53'),
(215, 62, 2, 'Change status with General Review', '1', 2, '2016-10-13 18:59:53', '2016-10-13 18:59:53'),
(216, 62, 2, 'Change status with General Review', '1', 2, '2016-10-13 18:59:54', '2016-10-13 18:59:54'),
(217, 62, 2, 'Change status with General Review', '1', 2, '2016-10-13 18:59:56', '2016-10-13 18:59:56'),
(218, 62, 2, 'Change status with General Review', '1', 2, '2016-10-13 18:59:57', '2016-10-13 18:59:57'),
(219, 62, 2, 'Change status with General Review', '1', 2, '2016-10-13 18:59:57', '2016-10-13 18:59:57'),
(220, 51, 2, 'Change status with General Review', '1', 2, '2016-10-14 15:55:47', '2016-10-14 15:55:47'),
(221, 51, 3, 'Change status with Administration Review', '1', 2, '2016-10-14 15:55:50', '2016-10-14 15:55:50'),
(222, 51, 3, 'Change status with Administration Review', '1', 2, '2016-10-14 15:55:50', '2016-10-14 15:55:50'),
(223, 101, 1, 'Registration', '1', 0, '2016-10-17 09:08:22', '2016-10-17 09:08:22'),
(224, 101, 2, 'Change status with General Review', '1', 2, '2016-10-17 09:09:37', '2016-10-17 09:09:37'),
(225, 101, 3, 'Change status with Administration Review', '1', 2, '2016-10-17 09:09:55', '2016-10-17 09:09:55'),
(226, 101, 4, 'Change status with Due-diligence', '1', 2, '2016-10-17 09:13:37', '2016-10-17 09:13:37'),
(227, 101, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-17 09:15:26', '2016-10-17 09:15:26'),
(228, 101, 5, 'Avl Vendor Approved', '1', 3, '2016-10-17 09:18:11', '2016-10-17 09:18:11'),
(229, 101, 5, 'Avl Vendor Approved', '1', 4, '2016-10-17 09:19:38', '2016-10-17 09:19:38'),
(230, 101, 5, 'Avl Vendor Approved', '1', 6, '2016-10-17 09:20:17', '2016-10-17 09:20:17'),
(231, 101, 6, 'Vendor Verification', '1', 2, '2016-10-17 09:25:46', '2016-10-17 09:25:46'),
(232, 101, 6, 'Completed Reconciliation Account', '1', 7, '2016-10-17 09:26:18', '2016-10-17 09:26:18'),
(233, 101, 6, 'Completed Vendor Nr. SAP', '1', 8, '2016-10-17 09:28:03', '2016-10-17 09:28:03'),
(234, 101, 6, 'Completed SRM Username dan Password', '1', 15, '2016-10-17 09:29:03', '2016-10-17 09:29:03'),
(235, 101, 8, 'Vendor Finalization', '1', 2, '2016-10-17 09:29:52', '2016-10-17 09:29:52'),
(236, 105, 1, 'Registration', '1', 0, '2016-10-17 14:46:11', '2016-10-17 14:46:11'),
(237, 106, 1, 'Registration', '1', 0, '2016-10-18 10:07:13', '2016-10-18 10:07:13'),
(238, 107, 1, 'Registration', '1', 0, '2016-10-18 10:56:27', '2016-10-18 10:56:27'),
(239, 110, 1, 'Registration', '1', 0, '2016-10-18 18:46:34', '2016-10-18 18:46:34'),
(240, 111, 1, 'Registration', '1', 0, '2016-10-18 18:52:06', '2016-10-18 18:52:06'),
(241, 112, 1, 'Registration', '1', 0, '2016-10-18 18:54:04', '2016-10-18 18:54:04'),
(242, 113, 1, 'Registration', '1', 0, '2016-10-18 18:59:54', '2016-10-18 18:59:54'),
(243, 113, 2, 'Change status with General Review', '1', 2, '2016-10-18 19:01:29', '2016-10-18 19:01:29'),
(244, 113, 3, 'Change status with Administration Review', '1', 2, '2016-10-18 19:01:31', '2016-10-18 19:01:31'),
(245, 113, 4, 'Change status with Due-diligence', '1', 2, '2016-10-18 19:01:35', '2016-10-18 19:01:35'),
(246, 113, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-18 19:02:02', '2016-10-18 19:02:02'),
(247, 112, 2, 'Change status with General Review', '1', 2, '2016-10-18 19:02:14', '2016-10-18 19:02:14'),
(248, 112, 3, 'Change status with Administration Review', '1', 2, '2016-10-18 19:02:15', '2016-10-18 19:02:15'),
(249, 112, 4, 'Change status with Due-diligence', '1', 2, '2016-10-18 19:02:17', '2016-10-18 19:02:17'),
(250, 112, 4, 'Shortlist Vendor Recomendation', '1', 2, '2016-10-18 19:02:37', '2016-10-18 19:02:37'),
(251, 111, 2, 'Change status with General Review', '1', 2, '2016-10-18 19:02:56', '2016-10-18 19:02:56'),
(252, 111, 3, 'Change status with Administration Review', '1', 2, '2016-10-18 19:02:56', '2016-10-18 19:02:56'),
(253, 111, 4, 'Change status with Due-diligence', '1', 2, '2016-10-18 19:02:58', '2016-10-18 19:02:58'),
(254, 111, 4, 'Reject Vendor Recomendation', '1', 2, '2016-10-18 19:03:41', '2016-10-18 19:03:41'),
(255, 110, 2, 'Change status with General Review', '1', 2, '2016-10-18 19:03:53', '2016-10-18 19:03:53'),
(256, 110, 3, 'Change status with Administration Review', '1', 2, '2016-10-18 19:03:54', '2016-10-18 19:03:54'),
(257, 110, 4, 'Change status with Due-diligence', '1', 2, '2016-10-18 19:03:55', '2016-10-18 19:03:55'),
(258, 110, 4, 'Avl Vendor Recomendation', '1', 2, '2016-10-18 19:04:12', '2016-10-18 19:04:12'),
(259, 113, 5, 'Avl Vendor Approved', '1', 3, '2016-10-18 19:33:59', '2016-10-18 19:33:59'),
(260, 112, 5, 'Shortlist Vendor Approved', '1', 3, '2016-10-18 19:34:14', '2016-10-18 19:34:14'),
(261, 111, 4, 'Reject Vendor Approved', '1', 3, '2016-10-18 19:34:23', '2016-10-18 19:34:23'),
(262, 110, 5, 'Avl Vendor Approved', '1', 3, '2016-10-18 19:34:31', '2016-10-18 19:34:31'),
(263, 113, 5, 'Avl Vendor Approved', '1', 4, '2016-10-18 19:37:47', '2016-10-18 19:37:47'),
(264, 112, 5, 'Shortlist Vendor Approved', '1', 4, '2016-10-18 19:37:59', '2016-10-18 19:37:59'),
(265, 110, 5, 'Avl Vendor Approved', '1', 4, '2016-10-18 19:38:09', '2016-10-18 19:38:09'),
(266, 113, 5, 'Avl Vendor Approved', '1', 6, '2016-10-18 19:41:22', '2016-10-18 19:41:22'),
(267, 110, 5, 'Avl Vendor Approved', '1', 6, '2016-10-18 19:41:34', '2016-10-18 19:41:34'),
(268, 110, 5, 'Avl Vendor Approved', '1', 6, '2016-10-18 19:41:39', '2016-10-18 19:41:39'),
(269, 113, 6, 'Completed Reconciliation Account', '1', 7, '2016-10-18 21:13:47', '2016-10-18 21:13:47'),
(270, 110, 6, 'Completed Reconciliation Account', '1', 7, '2016-10-18 21:13:57', '2016-10-18 21:13:57'),
(271, 113, 6, 'Completed Vendor Nr. SAP', '1', 8, '2016-10-18 21:15:08', '2016-10-18 21:15:08'),
(272, 110, 6, 'Completed Vendor Nr. SAP', '1', 8, '2016-10-18 21:15:14', '2016-10-18 21:15:14'),
(273, 113, 6, 'Completed SRM Username dan Password', '1', 15, '2016-10-18 21:21:17', '2016-10-18 21:21:17'),
(274, 110, 6, 'Completed SRM Username dan Password', '1', 15, '2016-10-18 21:21:31', '2016-10-18 21:21:31'),
(275, 110, 8, 'Vendor Finalization', '1', 2, '2016-10-18 21:25:14', '2016-10-18 21:25:14'),
(276, 113, 8, 'Vendor Finalization', '1', 2, '2016-10-18 21:29:15', '2016-10-18 21:29:15'),
(277, 111, 4, 'Reject Vendor Recomendation', '1', 2, '2016-10-18 21:31:07', '2016-10-18 21:31:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_rejected`
--

CREATE TABLE IF NOT EXISTS `vendor_rejected` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `approval` enum('3','2','1') NOT NULL DEFAULT '1',
  `notes` varchar(100) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `vendor_rejected`
--

INSERT INTO `vendor_rejected` (`id`, `id_vendor`, `approval`, `notes`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 1, '1', 'au ah\n', '1', 3, '2016-09-08 11:45:46', '2016-09-08 11:45:46'),
(2, 1, '', 'au ah\n', '1', 3, '2016-09-08 11:45:47', '2016-09-08 11:45:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_search`
--

CREATE TABLE IF NOT EXISTS `vendor_search` (
  `id` smallint(5) NOT NULL,
  `register_num` varchar(20) NOT NULL,
  `id_stsreg` smallint(1) NOT NULL DEFAULT '1',
  `approval` enum('3','2','1','0') NOT NULL DEFAULT '0',
  `vendor_type` enum('new','fail','shortlist','avl') NOT NULL DEFAULT 'new',
  `vendor_num` varchar(20) NOT NULL DEFAULT '',
  `vendor_name` varchar(40) NOT NULL,
  `vendor_address` varchar(50) NOT NULL,
  `postcode` varchar(15) DEFAULT NULL,
  `phone` varchar(30) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `npwp` varchar(20) NOT NULL,
  `npwp_address` varchar(50) DEFAULT NULL,
  `npwp_postcode` varchar(15) DEFAULT NULL,
  `web` varchar(50) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `created_id` smallint(5) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `session` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_temp`
--

CREATE TABLE IF NOT EXISTS `vendor_temp` (
`id` smallint(5) unsigned NOT NULL,
  `sess_reg` varchar(50) NOT NULL,
  `register_num` varchar(20) NOT NULL DEFAULT '',
  `vendor_num` varchar(20) NOT NULL,
  `vendor_type` enum('new','fail','shortlist','avl') NOT NULL DEFAULT 'new',
  `vendor_name` varchar(40) NOT NULL,
  `subcategory` varchar(100) NOT NULL,
  `vendor_address` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `overseas` enum('1','0') NOT NULL DEFAULT '0',
  `npwp` varchar(20) NOT NULL,
  `web` varchar(50) DEFAULT NULL,
  `cp_name` varchar(30) DEFAULT NULL,
  `cp_mobile` varchar(30) DEFAULT NULL,
  `cp_email` varchar(30) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `created_id` smallint(5) NOT NULL DEFAULT '0',
  `approved_date` datetime DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

--
-- Dumping data untuk tabel `vendor_temp`
--

INSERT INTO `vendor_temp` (`id`, `sess_reg`, `register_num`, `vendor_num`, `vendor_type`, `vendor_name`, `subcategory`, `vendor_address`, `phone`, `fax`, `email`, `overseas`, `npwp`, `web`, `cp_name`, `cp_mobile`, `cp_email`, `status`, `created_id`, `approved_date`, `created_date`, `last_updated`) VALUES
(7, '57d917d0dfc65', 'SR16000005', '', 'new', 'PT. Doa Ibu', '5,6', 'jalan panjang menuju langit biru', '0987654321', '123456789', 'doa@ibu.com', '0', '123409876', NULL, 'nadita dwi', '1234567890', 'test@gmail.com', '0', 0, NULL, '2016-09-14 16:26:40', '2016-09-14 16:26:40'),
(8, '57d91a8fdfa7e', 'SR16000005', '', 'new', 'PT. Doa Ibu', '5', 'Jalan Panjang Menuju Langit Biru', '1234509876', '0987612345', 'doa@ibu.com', '0', '768594030', NULL, 'riko ratata', '8347238537497', 'riko@ratata.com', '0', 0, NULL, '2016-09-14 16:38:23', '2016-09-14 16:38:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_visit`
--

CREATE TABLE IF NOT EXISTS `vendor_visit` (
`id` mediumint(11) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `office_type` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `pic` varchar(50) NOT NULL,
  `telp` varchar(30) NOT NULL,
  `fax` varchar(30) NOT NULL,
  `visit_date` date NOT NULL,
  `visit_time` varchar(20) NOT NULL,
  `remark` varchar(100) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data untuk tabel `vendor_visit`
--

INSERT INTO `vendor_visit` (`id`, `id_vendor`, `office_type`, `address`, `pic`, `telp`, `fax`, `visit_date`, `visit_time`, `remark`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 2, 'Kantor Cabang', 'Cinere', 'Arif', '0986371676', '1234254', '2016-09-09', '09:30', 'Site Visit', '1', 2, '2016-09-08 10:39:07', '2016-09-08 10:39:07'),
(2, 2, 'Kantor Utama', 'Cingkareng', 'Archi', '1243', '134324', '2016-09-08', '13:30', 'Site visit kedua', '1', 2, '2016-09-08 10:55:02', '2016-09-08 10:55:02'),
(3, 3, 'Head Office', 'Pamulang raya', 'Ditya Firmansyah', '009898098', '0989898787', '2016-09-09', '12:30', 'Silaturahim', '1', 2, '2016-09-09 15:53:57', '2016-09-09 15:53:57'),
(4, 5, 'Kantor Pusat', 'Bandung', 'Somad', '021101010', '0211234567', '2016-09-09', '10:30', 'Visit', '1', 2, '2016-09-09 19:02:24', '2016-09-09 19:02:24'),
(6, 6, 'kantor Pusat', 'Sudirman', 'Wisnu', '081295057000', '901475817', '2016-09-16', '13:45', 'Sidak', '1', 2, '2016-09-14 14:42:11', '2016-09-14 14:45:01'),
(7, 6, 'Gudang', 'pluit', 'joni', '08546718293', '93475738', '2016-09-19', '09:00', 'sidak', '1', 2, '2016-09-14 14:44:07', '2016-09-14 14:44:07'),
(8, 8, 'Branch Office', 'jakarta', 'wewqeq', '021-23423423', '021-23423423', '2016-09-15', '18:00', 'qweqweq', '1', 2, '2016-09-15 16:10:50', '2016-09-15 16:10:50'),
(9, 5, 'srhsrh', 'fhsfhs', 'sfhs', 'shsfh', 'sgsg', '2016-09-16', '20:00', 'sgs', '1', 2, '2016-09-15 18:50:59', '2016-09-15 18:50:59'),
(10, 26, 'branch', 'sudirman', 'wisnu', '756248967', '59829058', '2016-09-26', '11:00', 'sidak pertama', '1', 2, '2016-09-21 14:34:55', '2016-09-21 14:35:21'),
(11, 28, 'Pusat', 'Dadap', 'Arif', '0819901021', '0211234567', '2016-09-27', '14:27', 'DuDil', '1', 2, '2016-09-27 14:27:59', '2016-09-27 14:27:59'),
(12, 10, 'cabang', 'fjkn', 'adljvbalj', '3758937', '32578290', '2016-09-01', '08:27', 'gewrh', '1', 2, '2016-09-28 08:29:05', '2016-09-28 08:29:05'),
(13, 10, 'heiufhq', 'fowhoughw', 'hdgouhw', '527859289', '2974265', '2016-09-29', '15:35', 'fhakdfuk', '1', 2, '2016-09-29 15:35:21', '2016-09-29 15:35:21'),
(14, 11, 'PUSAT', 'SUDIRMAN', 'WISNU', '93475729', '4967482', '2016-09-30', '10:45', 'TUTUP', '1', 2, '2016-09-30 10:48:01', '2016-09-30 10:48:01'),
(15, 40, 'pusat', 'slkdjf', 'ljflq', '342134', '4234', '2016-10-03', '09:47', 'fqh', '1', 2, '2016-10-03 09:47:55', '2016-10-03 09:47:55'),
(17, 55, 'ousat', 'jaya 1', 'arif', '23857289lkdjfljaf', 'lqgl', '2016-10-05', '18:00', 'kunjungan', '1', 2, '2016-10-03 15:00:29', '2016-10-03 15:00:29'),
(18, 48, 'Branch Office', 'jakarta', 'budi', '021-23423423', '021-23423423', '2016-09-30', '14:00', 'asdadasd', '1', 2, '2016-10-04 10:36:39', '2016-10-04 10:36:39'),
(19, 61, 'grwergre', 'ergerg', 'rgerg', '45435', '45645654', '2016-10-06', '14:37', 'ghrgjh', '1', 2, '2016-10-06 14:38:20', '2016-10-06 14:38:20'),
(21, 61, 'Branch Office', 'sdfsdf', 'budi', '021-23423423', '021-23423423', '2016-10-06', '14:45', 'sdfsdfds', '1', 2, '2016-10-06 14:45:12', '2016-10-06 14:45:12'),
(22, 33, 'HO', 'jakarta', 'test', '897897979', '798798798', '2016-10-10', '14:14', 'site visit', '1', 2, '2016-10-10 14:15:18', '2016-10-10 14:15:18'),
(23, 33, 'BO', 'Cikarang', 'testing 2', '878979879', '8789798798', '2016-10-10', '14:15', 'testing 2', '1', 2, '2016-10-10 14:16:07', '2016-10-10 14:16:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vendor_visit_status`
--

CREATE TABLE IF NOT EXISTS `vendor_visit_status` (
`id` smallint(5) unsigned NOT NULL,
  `id_vendor` smallint(5) NOT NULL,
  `status` smallint(1) NOT NULL DEFAULT '1',
  `created_id` smallint(5) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `vendor_visit_status`
--

INSERT INTO `vendor_visit_status` (`id`, `id_vendor`, `status`, `created_id`, `created_date`, `last_updated`) VALUES
(1, 48, 1, 2, '2016-10-04 10:31:47', '2016-10-04 10:36:48'),
(2, 53, 2, 2, '2016-10-04 10:38:12', '2016-10-04 10:38:12'),
(3, 54, 2, 2, '2016-10-04 10:43:28', '2016-10-04 10:43:28'),
(4, 56, 2, 1, '2016-10-06 13:51:15', '2016-10-06 13:51:15'),
(5, 61, 2, 2, '2016-10-07 09:32:30', '2016-10-07 09:32:30'),
(6, 65, 2, 2, '2016-10-11 09:38:54', '2016-10-11 09:38:54'),
(7, 52, 2, 2, '2016-10-11 10:40:05', '2016-10-11 10:40:05'),
(8, 42, 2, 2, '2016-10-11 11:05:20', '2016-10-11 11:05:20'),
(9, 66, 2, 2, '2016-10-11 15:43:07', '2016-10-11 15:59:31'),
(10, 68, 2, 2, '2016-10-13 11:29:54', '2016-10-13 11:29:54'),
(11, 73, 1, 2, '2016-10-13 13:35:32', '2016-10-13 13:35:32'),
(12, 101, 1, 2, '2016-10-17 09:15:04', '2016-10-17 09:15:04'),
(13, 113, 1, 2, '2016-10-18 19:01:42', '2016-10-18 19:01:42'),
(14, 112, 1, 2, '2016-10-18 19:02:23', '2016-10-18 19:02:23'),
(15, 111, 1, 2, '2016-10-18 19:03:02', '2016-10-18 19:03:02'),
(16, 110, 1, 2, '2016-10-18 19:03:58', '2016-10-18 19:03:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `affiliates`
--
ALTER TABLE `affiliates`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `akta`
--
ALTER TABLE `akta`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announce`
--
ALTER TABLE `announce`
 ADD PRIMARY KEY (`id`,`code`);

--
-- Indexes for table `announcement_level_approval`
--
ALTER TABLE `announcement_level_approval`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announce_join_participant`
--
ALTER TABLE `announce_join_participant`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announce_newsblast_participant`
--
ALTER TABLE `announce_newsblast_participant`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announce_news_detail`
--
ALTER TABLE `announce_news_detail`
 ADD PRIMARY KEY (`id`,`id_announce`,`id_subcontent`);

--
-- Indexes for table `approval_level`
--
ALTER TABLE `approval_level`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `avl_changedata`
--
ALTER TABLE `avl_changedata`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_account`
--
ALTER TABLE `bank_account`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_account_mail`
--
ALTER TABLE `bank_account_mail`
 ADD PRIMARY KEY (`id`,`id_vendor`);

--
-- Indexes for table `behavior`
--
ALTER TABLE `behavior`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`id`,`init_code`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
 ADD PRIMARY KEY (`id`,`code`);

--
-- Indexes for table `contact_person`
--
ALTER TABLE `contact_person`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_type`
--
ALTER TABLE `content_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `correspondence`
--
ALTER TABLE `correspondence`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dd_assesment`
--
ALTER TABLE `dd_assesment`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dd_conclutions`
--
ALTER TABLE `dd_conclutions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dd_docs`
--
ALTER TABLE `dd_docs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dd_notes`
--
ALTER TABLE `dd_notes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dd_participant`
--
ALTER TABLE `dd_participant`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dd_summary`
--
ALTER TABLE `dd_summary`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dd_vdr_assesment`
--
ALTER TABLE `dd_vdr_assesment`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departemen`
--
ALTER TABLE `departemen`
 ADD PRIMARY KEY (`id`,`dept_code`);

--
-- Indexes for table `departemen_`
--
ALTER TABLE `departemen_`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `direct_bidding`
--
ALTER TABLE `direct_bidding`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `groups_name_unique` (`id_announce`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_req`
--
ALTER TABLE `document_req`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_requirement`
--
ALTER TABLE `document_requirement`
 ADD PRIMARY KEY (`id`,`counter`);

--
-- Indexes for table `due_dilligence`
--
ALTER TABLE `due_dilligence`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file_type`
--
ALTER TABLE `file_type`
 ADD PRIMARY KEY (`id`,`type`);

--
-- Indexes for table `function`
--
ALTER TABLE `function`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `functional`
--
ALTER TABLE `functional`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ga_employee`
--
ALTER TABLE `ga_employee`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`,`code`), ADD UNIQUE KEY `groups_name_unique` (`name`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu1`
--
ALTER TABLE `menu1`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu2`
--
ALTER TABLE `menu2`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu3`
--
ALTER TABLE `menu3`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_feature`
--
ALTER TABLE `menu_feature`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `non_visit_files`
--
ALTER TABLE `non_visit_files`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `non_visit_notes`
--
ALTER TABLE `non_visit_notes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
 ADD PRIMARY KEY (`id`,`notif_number`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
 ADD PRIMARY KEY (`id`,`office_code`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner`
--
ALTER TABLE `owner`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
 ADD PRIMARY KEY (`id`), ADD KEY `permissions_code_unique` (`code`);

--
-- Indexes for table `permissions_server`
--
ALTER TABLE `permissions_server`
 ADD PRIMARY KEY (`id`), ADD KEY `code` (`code`);

--
-- Indexes for table `policy_agreement`
--
ALTER TABLE `policy_agreement`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_agreement_`
--
ALTER TABLE `policy_agreement_`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_agreement_old`
--
ALTER TABLE `policy_agreement_old`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_agreement_temp`
--
ALTER TABLE `policy_agreement_temp`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
 ADD PRIMARY KEY (`id`,`post_code`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `redlist_docs`
--
ALTER TABLE `redlist_docs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `references`
--
ALTER TABLE `references`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register_history`
--
ALTER TABLE `register_history`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register_status`
--
ALTER TABLE `register_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reminder_doc`
--
ALTER TABLE `reminder_doc`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
 ADD PRIMARY KEY (`id`,`section_code`);

--
-- Indexes for table `session_email`
--
ALTER TABLE `session_email`
 ADD PRIMARY KEY (`id`,`token`);

--
-- Indexes for table `session_notification`
--
ALTER TABLE `session_notification`
 ADD PRIMARY KEY (`id`,`token`);

--
-- Indexes for table `session_register`
--
ALTER TABLE `session_register`
 ADD PRIMARY KEY (`id`,`session`);

--
-- Indexes for table `session_verification`
--
ALTER TABLE `session_verification`
 ADD PRIMARY KEY (`id`,`session`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
 ADD PRIMARY KEY (`id`,`init_code`);

--
-- Indexes for table `subcat_notes`
--
ALTER TABLE `subcat_notes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcat_resumes`
--
ALTER TABLE `subcat_resumes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcontent_type`
--
ALTER TABLE `subcontent_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcontent_type_old`
--
ALTER TABLE `subcontent_type_old`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_category`
--
ALTER TABLE `supplier_category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_docs`
--
ALTER TABLE `table_docs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_affiliates`
--
ALTER TABLE `temp_affiliates`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_affiliates_avl`
--
ALTER TABLE `temp_affiliates_avl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_contact_person`
--
ALTER TABLE `temp_contact_person`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_contact_person_avl`
--
ALTER TABLE `temp_contact_person_avl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_correspondence`
--
ALTER TABLE `temp_correspondence`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_correspondence_avl`
--
ALTER TABLE `temp_correspondence_avl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_organization`
--
ALTER TABLE `temp_organization`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_organization_avl`
--
ALTER TABLE `temp_organization_avl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_owner`
--
ALTER TABLE `temp_owner`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_owner_avl`
--
ALTER TABLE `temp_owner_avl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_references`
--
ALTER TABLE `temp_references`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_references_avl`
--
ALTER TABLE `temp_references_avl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_vendor_category`
--
ALTER TABLE `temp_vendor_category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_vendor_category_avl`
--
ALTER TABLE `temp_vendor_category_avl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
 ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `user_autologin`
--
ALTER TABLE `user_autologin`
 ADD PRIMARY KEY (`key_id`,`user_id`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
 ADD PRIMARY KEY (`id`,`register_num`,`vendor_num`);

--
-- Indexes for table `vendor_account`
--
ALTER TABLE `vendor_account`
 ADD PRIMARY KEY (`id`,`id_vendor`);

--
-- Indexes for table `vendor_approval`
--
ALTER TABLE `vendor_approval`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_blacklist`
--
ALTER TABLE `vendor_blacklist`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_category`
--
ALTER TABLE `vendor_category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_changedata`
--
ALTER TABLE `vendor_changedata`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_delete`
--
ALTER TABLE `vendor_delete`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_docs`
--
ALTER TABLE `vendor_docs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_inactive`
--
ALTER TABLE `vendor_inactive`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_logs`
--
ALTER TABLE `vendor_logs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_promote`
--
ALTER TABLE `vendor_promote`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_recomend`
--
ALTER TABLE `vendor_recomend`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_recommend`
--
ALTER TABLE `vendor_recommend`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_redlist`
--
ALTER TABLE `vendor_redlist`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_register`
--
ALTER TABLE `vendor_register`
 ADD PRIMARY KEY (`id`,`session`);

--
-- Indexes for table `vendor_register_temp`
--
ALTER TABLE `vendor_register_temp`
 ADD PRIMARY KEY (`id`,`reg_num`);

--
-- Indexes for table `vendor_register_tracking`
--
ALTER TABLE `vendor_register_tracking`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_rejected`
--
ALTER TABLE `vendor_rejected`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_temp`
--
ALTER TABLE `vendor_temp`
 ADD PRIMARY KEY (`id`,`register_num`,`vendor_num`,`sess_reg`);

--
-- Indexes for table `vendor_visit`
--
ALTER TABLE `vendor_visit`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_visit_status`
--
ALTER TABLE `vendor_visit_status`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `affiliates`
--
ALTER TABLE `affiliates`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `akta`
--
ALTER TABLE `akta`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `announce`
--
ALTER TABLE `announce`
MODIFY `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `announcement_level_approval`
--
ALTER TABLE `announcement_level_approval`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `announce_join_participant`
--
ALTER TABLE `announce_join_participant`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=169;
--
-- AUTO_INCREMENT for table `announce_newsblast_participant`
--
ALTER TABLE `announce_newsblast_participant`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `announce_news_detail`
--
ALTER TABLE `announce_news_detail`
MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `approval_level`
--
ALTER TABLE `approval_level`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `avl_changedata`
--
ALTER TABLE `avl_changedata`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `bank_account`
--
ALTER TABLE `bank_account`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `bank_account_mail`
--
ALTER TABLE `bank_account_mail`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `behavior`
--
ALTER TABLE `behavior`
MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contact_person`
--
ALTER TABLE `contact_person`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `content_type`
--
ALTER TABLE `content_type`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `correspondence`
--
ALTER TABLE `correspondence`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `dd_assesment`
--
ALTER TABLE `dd_assesment`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `dd_conclutions`
--
ALTER TABLE `dd_conclutions`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dd_docs`
--
ALTER TABLE `dd_docs`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `dd_notes`
--
ALTER TABLE `dd_notes`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `dd_participant`
--
ALTER TABLE `dd_participant`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `dd_summary`
--
ALTER TABLE `dd_summary`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `dd_vdr_assesment`
--
ALTER TABLE `dd_vdr_assesment`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `departemen`
--
ALTER TABLE `departemen`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `departemen_`
--
ALTER TABLE `departemen_`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `direct_bidding`
--
ALTER TABLE `direct_bidding`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `document_req`
--
ALTER TABLE `document_req`
MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `due_dilligence`
--
ALTER TABLE `due_dilligence`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `file_type`
--
ALTER TABLE `file_type`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `function`
--
ALTER TABLE `function`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `functional`
--
ALTER TABLE `functional`
MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ga_employee`
--
ALTER TABLE `ga_employee`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=483;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `menu1`
--
ALTER TABLE `menu1`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `menu2`
--
ALTER TABLE `menu2`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `menu3`
--
ALTER TABLE `menu3`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `menu_feature`
--
ALTER TABLE `menu_feature`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `non_visit_files`
--
ALTER TABLE `non_visit_files`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `non_visit_notes`
--
ALTER TABLE `non_visit_notes`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `owner`
--
ALTER TABLE `owner`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `permissions_server`
--
ALTER TABLE `permissions_server`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `policy_agreement`
--
ALTER TABLE `policy_agreement`
MODIFY `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `policy_agreement_`
--
ALTER TABLE `policy_agreement_`
MODIFY `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `policy_agreement_old`
--
ALTER TABLE `policy_agreement_old`
MODIFY `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `policy_agreement_temp`
--
ALTER TABLE `policy_agreement_temp`
MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `redlist_docs`
--
ALTER TABLE `redlist_docs`
MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT for table `references`
--
ALTER TABLE `references`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `register_history`
--
ALTER TABLE `register_history`
MODIFY `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `register_status`
--
ALTER TABLE `register_status`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `reminder_doc`
--
ALTER TABLE `reminder_doc`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `session_email`
--
ALTER TABLE `session_email`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=649;
--
-- AUTO_INCREMENT for table `session_notification`
--
ALTER TABLE `session_notification`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `session_register`
--
ALTER TABLE `session_register`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `session_verification`
--
ALTER TABLE `session_verification`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `subcat_notes`
--
ALTER TABLE `subcat_notes`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `subcat_resumes`
--
ALTER TABLE `subcat_resumes`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `subcontent_type_old`
--
ALTER TABLE `subcontent_type_old`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `supplier_category`
--
ALTER TABLE `supplier_category`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `table_docs`
--
ALTER TABLE `table_docs`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `temp_affiliates`
--
ALTER TABLE `temp_affiliates`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `temp_affiliates_avl`
--
ALTER TABLE `temp_affiliates_avl`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `temp_contact_person`
--
ALTER TABLE `temp_contact_person`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `temp_contact_person_avl`
--
ALTER TABLE `temp_contact_person_avl`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `temp_correspondence`
--
ALTER TABLE `temp_correspondence`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `temp_correspondence_avl`
--
ALTER TABLE `temp_correspondence_avl`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `temp_organization`
--
ALTER TABLE `temp_organization`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `temp_organization_avl`
--
ALTER TABLE `temp_organization_avl`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `temp_owner`
--
ALTER TABLE `temp_owner`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `temp_owner_avl`
--
ALTER TABLE `temp_owner_avl`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `temp_references`
--
ALTER TABLE `temp_references`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `temp_references_avl`
--
ALTER TABLE `temp_references_avl`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `temp_vendor_category`
--
ALTER TABLE `temp_vendor_category`
MODIFY `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `temp_vendor_category_avl`
--
ALTER TABLE `temp_vendor_category_avl`
MODIFY `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `vendor_account`
--
ALTER TABLE `vendor_account`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `vendor_approval`
--
ALTER TABLE `vendor_approval`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vendor_blacklist`
--
ALTER TABLE `vendor_blacklist`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `vendor_category`
--
ALTER TABLE `vendor_category`
MODIFY `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=207;
--
-- AUTO_INCREMENT for table `vendor_changedata`
--
ALTER TABLE `vendor_changedata`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `vendor_delete`
--
ALTER TABLE `vendor_delete`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `vendor_docs`
--
ALTER TABLE `vendor_docs`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `vendor_inactive`
--
ALTER TABLE `vendor_inactive`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `vendor_logs`
--
ALTER TABLE `vendor_logs`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `vendor_promote`
--
ALTER TABLE `vendor_promote`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `vendor_recomend`
--
ALTER TABLE `vendor_recomend`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `vendor_recommend`
--
ALTER TABLE `vendor_recommend`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendor_redlist`
--
ALTER TABLE `vendor_redlist`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `vendor_register`
--
ALTER TABLE `vendor_register`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `vendor_register_temp`
--
ALTER TABLE `vendor_register_temp`
MODIFY `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendor_register_tracking`
--
ALTER TABLE `vendor_register_tracking`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=278;
--
-- AUTO_INCREMENT for table `vendor_rejected`
--
ALTER TABLE `vendor_rejected`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vendor_temp`
--
ALTER TABLE `vendor_temp`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `vendor_visit`
--
ALTER TABLE `vendor_visit`
MODIFY `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `vendor_visit_status`
--
ALTER TABLE `vendor_visit_status`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
