<div class="page-header">
  <h3>Dashboard</h3>
</div>

<div id="page-wrapper" style="min-height: 319px;">
	<div class="row">
	    <div class="col-lg-6">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                <i class="fa fa-bar-chart-o fa-fw"></i> Account Information
	            </div>
	            <div class="panel-body">
	            	<ul class="list-inline">
	            		<li class="col-md-6 text-right"><strong>Username</strong></li>
	            		<li class="col-md-6 col-sm-6"><?php echo $this->session->userdata('username'); ?></li>
	            	</ul>
	            	<ul class="list-inline">
	            		<li class="col-md-6 text-right"><strong>Last Login</strong></li>
	            		<li class="col-md-6 col-sm-6">11 May 2016 09:04:37</li>
	            	</ul>
                </div>
                <div class="panel-footer text-center">
                    <a href="#" class="btn btn-primary" style="margin-bottom:0;">Change Password</a>
                </div>
            </div>

            <div class="panel panel-default">
	            <div class="panel-heading">
	                <i class="fa fa-bar-chart-o fa-fw"></i> Latest News
	            </div>
	            <div class="panel-body">
            	<?php foreach ($getLatestNews as $row) { ?>
            		<ul class="col-md-12 list-unstyled">
            			<li><strong><?php echo date_format(date_create($row->publish_date),"F d, Y"); ?></strong></li>
            			<li class="panel panel-default panel-body" style="margin-top: 10px;padding:10px;">
            				<a href="#" data-toggle="modal" data-target="#<?php echo $row->file;?>"><?php echo $row->title_eng;?></a>
            			</li>
            		</ul>
            		<div id="<?php echo $row->file;?>" class="modal fade" role="dialog">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <h4 class="modal-title"><?php echo $row->title_eng;?></h4>
					      </div>
					      <div class="modal-body">
					        <p><?php $ctpath = "./uploads/contents/".$row->file."_".$this->session->userdata('lang').".txt"; echo file_exists($ctpath) ? file_get_contents($ctpath) : "-"; unset($ctpath); ?></p>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-danger" style="margin-bottom:0;" data-dismiss="modal">Close</button>
					      </div>
					    </div>
					  </div>
					</div>
            	<?php }?>
                </div>
            </div>
            <div class="panel panel-default">
	            <div class="panel-heading">
	                <i class="fa fa-bar-chart-o fa-fw"></i> Latest Invitation(s)
	            </div>
	            <div class="panel-body">
	            <?php foreach ($getLatestInvitation as $row) { ?>
            		<ul class="col-md-12 list-unstyled">
            			<li><strong><?php echo date_format(date_create($row->publish_date),"F d, Y"); ?></strong></li>
            			<li class="panel panel-default panel-body" style="margin-top: 10px;padding:10px;">
            				<a href="#" data-toggle="modal" data-target="#<?php echo $row->file;?>"><?php echo $row->title_eng;?></a>
            			</li>
            		</ul>
            		<div id="<?php echo $row->file;?>" class="modal fade" role="dialog">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <h4 class="modal-title"><?php echo $row->title_eng;?></h4>
					      </div>
					      <div class="modal-body">
					        <p><?php $ctpath = "./uploads/contents/".$row->file."_".$this->session->userdata('lang').".txt"; echo file_exists($ctpath) ? file_get_contents($ctpath) : "-"; unset($ctpath); ?></p>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-danger" style="margin-bottom:0;" data-dismiss="modal">Close</button>
					      </div>
					    </div>
					  </div>
					</div>
            	<?php }?>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                <i class="fa fa-bar-chart-o fa-fw"></i> User Profile
	            </div>
	            <div class="panel-body">
	            	<ul class="list-inline">
	            		<li class="col-md-6 text-right"><strong>Nama</strong></li>
	            		<li class="col-md-6 col-sm-6"><?php echo $getProfile->fullname?$getProfile->fullname:'-'; ?></li>
	            	</ul>
	            	<ul class="list-inline">
	            		<li class="col-md-6 text-right"><strong>Vendor Office</strong></li>
	            		<li class="col-md-6 col-sm-6"><?php echo $getProfile->vendor_name?$getProfile->vendor_name:'-'; ?></li>
	            	</ul>
	            	<ul class="list-inline">
	            		<li class="col-md-6 text-right"><strong>Email</strong></li>
	            		<li class="col-md-6 col-sm-6"><?php echo $getProfile->email?$getProfile->email:'-'; ?></li>
	            	</ul>
	            	<ul class="list-inline">
	            		<li class="col-md-6 text-right"><strong>Mobile</strong></li>
	            		<li class="col-md-6 col-sm-6"><?php echo $getProfile->phone?$getProfile->phone:'-'; ?></li>
	            	</ul>
	            	<ul class="list-inline">
	            		<li class="col-md-6 text-right"><strong>Phone</strong></li>
	            		<li class="col-md-6 col-sm-6"><?php echo $getProfile->phone?$getProfile->phone:'-'; ?></li>
	            	</ul>
	            	<ul class="list-inline">
	            		<li class="col-md-6 text-right"><strong>Fax</strong></li>
	            		<li class="col-md-6 col-sm-6"><?php echo $getProfile->fax?$getProfile->fax:'-'; ?></li>
	            	</ul>
                </div>
                <div class="panel-footer text-center">
                    <a href="#" class="btn btn-primary" style="margin-bottom:0;">Edit Profile</a>
                </div>
            </div>
            <div class="panel panel-default">
	            <div class="panel-heading">
	                <i class="fa fa-bar-chart-o fa-fw"></i> Latest Inbox
	            </div>
	            <div class="panel-body">
	            	<ul class="col-md-12 list-unstyled">
	            		<li class="panel panel-default panel-body" style="padding:10px;">
	            			<ul class="list-inline">
			            		<li class="col-md-2"><strong>User 1</strong></li>
			            		<li class="col-md-10">20 May 2015 12:32:25</li>
			            	</ul>
			            	<ul class="list-inline">
			            		<li class="col-md-2"><strong>SUBJECT</strong></li>
			            		<li class="col-md-10">Lorem ipsum dolor sit amet</li>
			            	</ul>
	            		</li>
	            	</ul>
	            	<ul class="col-md-12 list-unstyled">
	            		<li class="panel panel-default panel-body" style="padding:10px;">
	            			<ul class="list-inline">
			            		<li class="col-md-2"><strong>User 1</strong></li>
			            		<li class="col-md-10">20 May 2015 12:32:25</li>
			            	</ul>
			            	<ul class="list-inline">
			            		<li class="col-md-2"><strong>SUBJECT</strong></li>
			            		<li class="col-md-10">Lorem ipsum dolor sit amet</li>
			            	</ul>
	            		</li>
	            	</ul>
	            	<ul class="col-md-12 list-unstyled">
	            		<li class="panel panel-default panel-body" style="padding:10px;">
	            			<ul class="list-inline">
			            		<li class="col-md-2"><strong>User 1</strong></li>
			            		<li class="col-md-10">20 May 2015 12:32:25</li>
			            	</ul>
			            	<ul class="list-inline">
			            		<li class="col-md-2"><strong>SUBJECT</strong></li>
			            		<li class="col-md-10">Lorem ipsum dolor sit amet</li>
			            	</ul>
	            		</li>
	            	</ul>
                </div>
            </div>
        </div>
    </div>
</div>
