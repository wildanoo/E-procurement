<html>
<head>
<title></title>
<meta name="" content="">
</head>
<body>
Dengan hormat,<br/>
<b>Bapak/Ibu <?php echo ucwords($to); ?></b><br/><br/>

Dengan ini, menginformasikan bahwa : <br/>

Nama Perusahaan : <?php echo ucwords($vendor_name); ?><br/>
ID Vendor : <?php echo $vendor_num; ?><br/>

Perusahaan tersebut telah mengisi data bank account sebagai salah satu syarat menjadi Rekanan PT Garuda Indonesia (Persero) Tbk ("Garuda�).<br/>
Info lebih lengkap dapat diakses pada link berikut ini : <br/>
<?php //echo base_url()."auth/login"; ?> <br/><br/>

Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/><br/>

PT Garuda Indonesia (Persero) Tbk <br/> 
Business Support / JKTIBGA <br/>
Gedung Management Garuda, Lantai Dasar <br/>
Garuda City,  Bandara Soekarno Hatta <br/>
Cengkareng 19120, PO BOX 1004 TNG BUSH

</body>
</html>