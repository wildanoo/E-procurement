<script type="text/javascript"> 
    $(function(){  
        var baseUrl = "<?php echo base_url(); ?>";
        
        var session = "<?php echo $this->session->userdata('sess_reg'); ?>";
        if(session) { $.post( baseUrl + 'register/delete_session', { session : session } );  }    
        
        $('#overseas').change(function(){  
            var id_ovs = $('#overseas').val(); 
           	if(id_ovs=="1")  $('#npwp').attr( 'placeholder','COD');
           	else $('#npwp').attr( 'placeholder','NPWP');
        }); 
        
        $('#btn_register').click(function(){
        	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
        	var token   = '<?php echo $this->security->get_csrf_hash(); ?>';    
            var values  = $("#form_register").serializeArray();                  
            values.push({name: csrf,value: token}); values = jQuery.param(values);          
                $.ajax({
                   url  : baseUrl + 'register/validate',
                   type : "POST",                                  
                   data : values,
                   success: function(resp){ //$('#debug').html(resp);
                        var json = $.parseJSON(resp); 
                         if(json.status=="false"){
                            $('#msg1').html(json.msg1); $('#msg6').html(json.msg6);
                            $('#msg2').html(json.msg2); $('#msg7').html(json.msg7);
                            $('#msg8').html(json.msg8);
                            $('#msg4').html(json.msg4); $('#msg9').html(json.msg9);
                            $('#msg5').html(json.msg5); $('#msg10').html(json.msg10);
                            $('#msg11').html(json.msg11);                       
                         } else {                    
							$.post( baseUrl + 'register/create', values ).done(function( resp2 ) {
							     $("#btn_next").trigger("click");
							});                         
                         }
                   }
                });
        });

		$('#btn_confirm').click(function(){ 
				var checked = $('#agree').is(':checked');
				if(checked){				
					$.post( baseUrl+'register/confirmation',function(resp){
							$('#content-1').html( resp ); $("#btn_next2").hide();
							$("#btn_return").trigger("click");	
					}); 
				} else { alert(' Please accept term and condition ');	}
		}); 

		$('#btn_check').click(function(){  
	   		var reg_num = $('#reg_num').val(); 
	   		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
	            $.ajax({
	               url  : baseUrl + 'register/register_validation',
	               type : "POST",              		               
	               data : csrf +'='+ token +'&reg_num='+reg_num,
	               success: function(resp){      	                	  
	               	  var json = $.parseJSON(resp);
	               	  if(json.status=="false"){ 	               	  
	               	  $("#msg").addClass("alert alert-danger");
                      $("#msg").html('<strong>Warning</strong>!, ' + json.msg); 
	               	  } else { 	
					  $.post( baseUrl + 'register/set_default_vendor', { csrf: token, reg_num: reg_num })
					  .done(function( resp2 ) { 
					   
					   $("#msg").removeClass("alert alert-danger");
                       $("#msg").html('');				  
					  
					  $('#status').html(resp2); });  }
	               }
	            });
	    }); 
	           
}); 
</script>


<div class="about-content-wrap pull-left" id="#">
    <div class="head" style="margin-top: 25px;">
        <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;">Registration</h1>
        <a href="#" class="next-section about animate2">
            <i class="fa fa-angle-double-down"></i>
        </a>
    </div>
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 content-tabs-wrap wow fadeInUp animated" data-wow-delay="1.7s" data-wow-offset="200">
            <div class="col-xs-12 col-sm-12 col-md-12 content-tabs no-pad">            
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab-home" data-toggle="tab">Register</a></li>
                  <li><a href="#tab-profile" data-toggle="tab">Check Registration Status</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-home">
                        <div class = "row">
                            <div class="col-md-12 tab-content">
                                <div class="box2 box-info2 tab-pane fade in active" id="registrasi-step-1">
                                    <form class="form-horizontal">
                                        <div class="box-body">
                                            <div class="about-intro-wrap pull-left">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 faq-tabs-wrap">
                                                        <div class="tabbable tabs-left">
                                                            <div class="tab-content col-md-12 col-sm-12 col-xs-12">
                                                             <!-- start content-1 -->
                                                            	<div id="content-1">
                                                           
                                                                <div class="tab-pane active" id="a">
                                                                    <div class="dept-title-tabs">Vendor Policy</div>
                                                                    <div class="background-vendor">
                                                                        <div style="padding:20px;">
                                                                        	<h2><strong>General</strong></h2>
                                                                            <?php /*foreach ($policy_agreements as $policy_agreement) {
                                                                                if ($policy_agreement->type != 0) {
                                                                                    echo "<h2><strong>".$policy_agreement->subtype_name."</strong></h2>";
                                                                                }else{
                                                                                    echo "<h2><strong>General</strong></h2>";
                                                                                }
                                                                                  /*$root = $_SERVER['DOCUMENT_ROOT'];   $path = $root."/uploads/policy_agreement/open_bidding_".$this->session->userdata('lang').".txt";
                                                                                if( file_exists($path)) {   echo file_get_contents($path);          
                                                                                } else { echo ""; } ini yg bener */
                                                                                /*$root = base_url();   $path = $root."uploads/policy_agreement/".$policy_agreement->file."_".$this->session->userdata('lang').".txt";
                                                                                if( file_exists($path)) {   echo file_get_contents($path);
                                                                                } else { echo file_get_contents($path); }
                                                                                echo "<hr/>";
                                                                            }*/
                               
							                               $lang = $this->session->userdata('lang');                                             
							                               $path = "./uploads/policy_agreement/general_".$lang.".txt";
							                               if( file_exists($path)) {  echo file_get_contents($path); } 	?> <hr/>
							                               
                                                                        </div>
                                                                    </div>
                                                                </div>																
                                                            
																</div>
															 <!-- end content-1 -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <table>
                                            	<?php                                             	
                                            	$dwld_ico = '<img src="'.base_url().'assets/images/download.png" width="30" height="25"/>'; 
                                            	$prop1    = array('class'=>'default_link','title'=>'Vendor Policy','style'=>' text-decoration: none;');
                                            	$prop2    = array('class'=>'default_link','title'=>'Petunjuk Teknis','style'=>' text-decoration: none;'); ?>
                                            	<tr>
                                            		<td><?php echo anchor('register/download_vendor_policy/',$dwld_ico."Vendor Policy",$prop1);  ?></td>
                                            		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            		<td><?php echo anchor('register/download_petunujuk_teknis/',$dwld_ico."Petunjuk Teknis",$prop2);  ?></td>
                                            	</tr>
                                            </table>                                                 
                                            
                                            <div class="pull-left" style="margin-right:30px;">
                                                <button type="button" class="btn button-custom" data-toggle="tab" id="btn_next2" href="#registrasi-step-2">Next</button>
                                            </div> 
                                        </div>
                                    </form>
                                </div>
                                <div id="debug"></div>
                                <div class="box2 box-info2 tab-pane fade" id="registrasi-step-2">
                                    <!--form class="form-horizontal"-->
                                    <?php echo form_open('register/create',array('id'=>'form_register','class'=>'form-horizontal')); ?>
                                        <div class="box-body">
                                            <div class="dept-title-tabs">Registration Form</div>
                                            <div class = "container2">
                                                <div class = "row2" style="margin-top:20px;">
                                                    <div class="appointment-form col-xs-12 col-md-12 no-pad wow fadeInRight animated" data-wow-delay="1s" data-wow-offset="200">
                                                        <div class="row background-form">
                                                            <div class="appointment-form-title"><i class="fa fa-building fa-1x" style="color: #fff;margin: 0 20px;"></i>Company</div>
                                                            
                                                            <div>
                                                                <div class="appt-form">
                                                                    <input type="text" class="appt-form-txt" name="vendor_name" id="vendor_name" placeholder="Vendor Name" />
                                                                    <div id="msg1" style="color: red;"></div>
                                                                    <select class="appt-form-select" id="public-vendor-select" name="id_subcat[]" multiple="multiple">
                                                                    <?php foreach($category as $group=>$row){ ?>
                                                                        <optgroup label="<?php echo $group; ?>">
                                                                        <?php foreach ($row as $val){ ?>                                
                                                                            <option value="<?php echo $val->id; ?>"><?php echo $val->subcategory; ?></option>                              
                                                                        <?php } ?>                                
                                                                        </optgroup>
                                                                    <?php } ?>
                                                                    </select>
                                                                    <div id="msg4" style="color: red;"></div><br/>
                                                                    <input type="text" class="appt-form-txt" placeholder="Supplier Address" name="vendor_address" id="vendor_address"/>
                                                                    <div id="msg2" style="color: red;"></div>
                                                                    <input type="text" class="appt-form-txt" placeholder="Phone Number" name="phone" id="phone"/>
                                                                    <div id="msg6" style="color: red;"></div>
                                                                    <input type="text" class="appt-form-txt" placeholder="Fax Number" name="fax" id="fax"/>
                                                                    <div id="msg7" style="color: red;"></div>
                                                                    <input type="email" class="appt-form-txt" placeholder="Email Address" name="email" id="email"/>
                                                                    <div id="msg8" style="color: red;"></div>

																																			  
																		  <select class="appt-form-select" name="overseas" id="overseas">																											    <option value="0">Indonesia</option>
																		    <option value="1">Others</option>														    
																		  </select>
																		

                                                                    <input type="text" class="appt-form-txt" placeholder="NPWP" name="npwp" id="npwp"/>
                                                                    <div id="msg9" style="color: red;"></div>
                                                                </div>
                                                            </div>
                                                            <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>Contact Person</div>
                                                            <div class="appt-form">
                                                                <input type="text" class="appt-form-txt" placeholder="Contact Person" name="cp_name" id="cp_name"/>
                                                                <div id="msg5" style="color: red;"></div>
                                                                <input type="text" class="appt-form-txt" placeholder="Mobile Phone" name="cp_mobile" id="cp_mobile"/>
                                                                <div id="msg10" style="color: red;"></div>
                                                                <input type="email" class="appt-form-txt" placeholder="Email" name="cp_email" id="cp_email"/>
                                                                <div id="msg11" style="color: red;"></div>
                                                            </div>
                                                            <button type="button" id="btn_next" data-toggle="tab" style="visibility:hidden;" href="#registrasi-step-3" />                                                              </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-left" style="margin-top:30px;">
                                                <button type="button" class="btn button-custom-back" data-toggle="tab" href="#registrasi-step-1">Back</button>
                                                <!--button type="button" class="btn button-custom" data-toggle="tab" href="#registrasi-step-3">Next</button-->
                                                <button type="button" class="btn button-custom" id="btn_register">Next</button>
                                            </div>
                                             
                                        </div>
                                    <?php echo form_close(); ?>
                                    <!--/form-->
                                </div>
                                <div class="box2 box-info2 tab-pane fade" id="registrasi-step-3">
                                    <div class="row background-terms">
                                        <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>Terms and Condition</div>
                                        <div class="terms-custom">
                                            <div class="form-group terms-text">
                                              <?php   $scnd_path = "./uploads/cms/terms_".$lang.".txt";
							                          if( file_exists($scnd_path)) {  echo file_get_contents($scnd_path); }   ?>
                                                <br>
                                                <br>
                                                <input type="checkbox" name="agree" id="agree" value="1" style="margin-right:10px;">I accept the Terms and Conditions
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-left" style="margin-top:10px;">
                                        <button type="button" class="btn button-custom-back" id="btn_return" data-toggle="tab" href="#registrasi-step-1">Back</button>
                                        <button type="button" class="btn button-custom" id="btn_confirm">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade no-pad" id="tab-profile">
                        <div class="row background-terms">
                            <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>Tracking Registration Status</div>
                            <?php echo form_open('#',array('id'=>'form_check')); ?>
                            <div class="col-md-12 col-sm-12 terms-custom">
                                <div class="form-group terms-text">
                                    Please Enter Registration Number Then Click "Search"
                                    <br><br>
                                    <input type="text" class="appt-form-txt" name="reg_num" id="reg_num" placeholder="Registration Number" style="width:40%;" />
                                    <br/><br/><div id="msg"></div>
                                    <button type="button" class="btn button-search" id="btn_check" data-toggle="tab" href="#registrasi-step-2">Search</button>
                                </div>
                            </div>
							<?php echo form_close(); ?>						
                        </div>                        
                        <div id="status"></div>
                    </div>  
                </div>      
            </div>
        </div>
    </div>
</div>