<div id="logs-data">
	
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th colspan="7">Logs Data</th>
			</tr>
			<tr>
				<th>No</th>
				<th>Data</th>
				<th>Before</th>
				<th>After</th>
				<th>Created By</th>
				<th>Created Date</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=$offset+1; foreach ($logs as $key => $value) { ?>
			<tr>
				<td><?=$no?></td>
				<td><?=$value->fieldname?></td>
				<td><?=$value->before?></td>
				<td><?=$value->after?></td>
				<td><?=$value->username?></td>
				<td><?=$value->created_date?></td>
				<td><?=ucfirst($value->status)?></td>
			</tr>
			<?php $no++; } ?>

		</tbody>
	</table>
	<?php echo $this->ajax_pagination->create_links(); ?>

	<br>
	<br>
	<br>
	<button type="button" onclick="hide_logs('<?=$tb_name?>')" class="btn btn-warning">Hide Logs</button>

</div>

