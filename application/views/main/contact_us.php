<div class="about-intro-wrap pull-left">
    <a name="contact"><span></a>
    <div class="head" style="margin-top: 85px;">
        <h1><?php echo $this->lang->line('section4');?></h1>
        <a href="#contact" class="next-section about animate2">
            <i class="fa fa-angle-double-down"></i>
        </a>
    </div>
    <div class="container" style="padding-top: 30px;">
        <div class="row">
            <div class="col-xs-12 col-lg-12  col-sm-12 col-md-12 pull-left contact2-wrap">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 no-pad wow fadeInLeft" data-wow-delay="0.5s" data-wow-offset="100">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="side-bar-contact">
                            <div class="form-title-text wow fadeInRight dept-title-tabs" data-wow-delay="0.5s" data-wow-offset="100"><?php echo $this->lang->line('section4_sub3');?></div>
                            <ul class="contact-page-list wow fadeInRight" data-wow-delay="0.5s" data-wow-offset="100">
                                <li>
                                    <i class="icon-globe contact-side-icon iside-icon-contact" style="height:100px;"></i>
                                    <!--span class="contact-side-txt">PT. Garuda Indonesia<?php //echo $this->lang->line('section4_sub1'); ?><br/>
                                        Business Support / JKTIBGA<br/>
                                        <?php //echo $this->lang->line('section4_sub1');?><br/>
                                        <?php //echo $this->lang->line('section4_sub2');?><br/>
                                    </span>-->
									<span>
									<?php echo file_get_contents($this->lang->line('contact_us_file')); ?>
									</span>
                                </li>
                                <li>
                                    <i class="icon-mail contact-side-icon"></i>
                                    <span class="contact-side-txt">Email: <a href="mailto:business-support@garuda-indonesia.com">business-support@garuda-indonesia.com</a></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <form class="contact2-page-form col-lg-8 col-sm-12 col-md-8 col-xs-12 no-pad contact-v2" id="contactForm">
                        <div class="overlay" onclick="style.pointerEvents='none'"></div>
                        <iframe style="width:100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.9838043279096!2d106.64042241419267!3d-6.132877961835144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a02682025eb33%3A0x840e34d171919f67!2sGaruda+Indonesia+City+Center!5e0!3m2!1sid!2sid!4v1464577237176" height="260" frameborder="0" style="border:0" allowfullscreen></iframe> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>