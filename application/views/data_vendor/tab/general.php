<div id="container1">

<script type="text/javascript">
 $(function () {
   	var baseUrl  = "<?php echo base_url(); ?>";
   	$('#update1').hide();
    $('#form :input[type="text"]').prop('disabled',true);
   	$('#change1').click(function(){
   		$('#form :input[type="text"]').each(function(index, el) {
	    	$(this).prop('disabled',false);
   		});

   		$('#update1').show();
   		$('#change1').hide();

   	});

   	$('#update1').click(function(){
		var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>';
        var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
   	    var values = $("#form").serialize()+'&'+csrf+':'+token;
			$.ajax({
               url  : baseUrl + 'data_vendor/general_update',
               type : "POST",
               data : values,
               success: function(resp){
               	  $("#container1").load(baseUrl+'data_vendor/load_general');
               }
            });

   		$('#update1').hide();
   		$('#change1').show();
   	});


 });


</script>
<?php
$ff = array('vendor_name','vendor_address','postcode','npwp','npwp_address','npwp_postcode','phone','fax','email','web');

foreach ($dc_vendor as $key => $value)
{
  foreach ($ff as $ffv)
  {
    if($value->fieldname == $ffv)
    {
      $dc[$ffv] = $value->status;
      $label[$ffv] = ($value->status=='request') ? 'warning':'success';
    }
  }

}

?>
<div id="debug"></div>
<?php echo form_open('#',array('id'=>'form','class'=>'form-hoizontal','role'=>'form')); ?>
  <div class="form-group">
    <label class="col-sm-2 control-label">Vendor Type</label>
    <div class="col-sm-10">
       <p class="form-control-static"><?=ucfirst($vendor->type)?></p>
    </div>
  </div>

  <div class="form-group" >
   <label for="vname" class="col-sm-2 control-label">Vendor Name</label>
   <div class="col-sm-10">
      <div class="input-group"  style="margin-bottom:3px;">
        <input type="text" value="<?=ucfirst($vendor->name)?>" class="form-control" id="vendor_name" name="vendor_name" disabled>
        <span class="input-group-addon label-<?=$label[vendor_name]?>"><?=$dc['vendor_name']?></span>
     </div>
   </div>
 </div>

 <div class="form-group">
  <label for="vname" class="col-sm-2 control-label">Vendor Address</label>
  <div class="col-sm-10">
    <div class="input-group" style="margin-bottom:3px;">
      <input type="text" value="<?=ucwords($vendor->address)?>" class="form-control" id="vendor_address" name="vendor_address" disabled>
      <span class="input-group-addon label-<?=$label[vendor_address]?>"><?=$dc['vendor_address']?></span>
    </div>
  </div>
</div>

<div class="form-group">
 <label for="vname" class="col-sm-2 control-label">Postcode</label>
 <div class="col-sm-10">
 <div class="input-group" style="margin-bottom:3px;">
   <input type="text" value="<?=ucwords($vendor->postcode)?>" class="form-control" id="vendor_postcode" name="vendor_postcode" disabled>
   <span class="input-group-addon label-<?=$label[postcode]?>"><?=$dc['postcode']?></span>
 </div>
 </div>
</div>
<div class="form-group">
 <label for="vname" class="col-sm-2 control-label">NPWP</label>
 <div class="col-sm-10">
 <div class="input-group" style="margin-bottom:3px;">
   <input type="text" value="<?=strtoupper($vendor->npwp)?>" class="form-control" id="npwp" name="npwp" disabled>
   <span class="input-group-addon label-<?=$label[npwp]?>"><?=$dc['npwp']?></span>
 </div>
 </div>
</div>
<div class="form-group">
 <label for="vname" class="col-sm-2 control-label">NPWP Address</label>
 <div class="col-sm-10">
 <div class="input-group" style="margin-bottom:3px;">
   <input type="text" value="<?=ucwords($vendor->npwp_address)?>" class="form-control" id="npwp_address" name="npwp_address" disabled>
   <span class="input-group-addon label-<?=$label[npwp_address]?>"><?=$dc['npwp_address']?></span>
 </div>
 </div>
</div>
<div class="form-group">
 <label for="vname" class="col-sm-2 control-label">NPWP Postcode</label>
 <div class="col-sm-10">
 <div class="input-group" style="margin-bottom:3px;">
   <input type="text" value="<?=ucwords($vendor->npwp_postcode)?>" class="form-control" id="npwp_postcode" name="npwp_postcode" disabled>
   <span class="input-group-addon label-<?=$label[npwp_postcode]?>"><?=$dc['npwp_postcode']?></span>
 </div>
 </div>
</div>
<div class="form-group">
 <label for="vname" class="col-sm-2 control-label">Phone</label>
 <div class="col-sm-10">
 <div class="input-group" style="margin-bottom:3px;">
   <input type="text" value="<?=$vendor->phone?>" class="form-control" id="phone" name="phone" disabled>
   <span class="input-group-addon label-<?=$label[phone]?>"><?=$dc['phone']?></span>
 </div>
 </div>
</div>
<div class="form-group">
 <label for="vname" class="col-sm-2 control-label">Fax</label>
 <div class="col-sm-10">
 <div class="input-group" style="margin-bottom:3px;">
   <input type="text" value="<?=$vendor->fax?>" class="form-control" id="fax" name="fax" disabled>
   <span class="input-group-addon label-<?=$label[fax]?>"><?=$dc['fax']?></span>
 </div>
 </div>
</div>
<div class="form-group">
 <label for="vname" class="col-sm-2 control-label">Email</label>
 <div class="col-sm-10">
 <div class="input-group" style="margin-bottom:3px;">
   <input type="text" value="<?=$vendor->email?>" class="form-control" id="email" name="email" disabled>
   <span class="input-group-addon label-<?=$label[email]?>"><?=$dc['email']?></span>
 </div>
 </div>
</div>
<div class="form-group">
 <label for="vname" class="col-sm-2 control-label">Web</label>
 <div class="col-sm-10">
 <div class="input-group" style="margin-bottom:3px;">
   <input type="text" value="<?=$vendor->web?>" class="form-control" id="web" name="web" disabled>
   <span class="input-group-addon label-<?=$label[web]?>"><?=$dc['web']?></span>
 </div>
 </div>
</div>

<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <?php if($this->permit->upven)  { ?>
      <button type="button" id="change1" class="btn btn-info btn-primary">Change</button>
      <button type="button" id="update1" class="btn btn-info btn-primary">Update</button>
      <a href="javascript:void(0)" onclick="view_logs('vendor')" type="button" id="log1" class="btn btn-info btn-info">View Log</a>
      <?php } ?>
  </div>
</div>
<?php echo form_close();  ?>

<br><br>

</div>
