<div class="about-content-wrap pull-left" id="#">
  <div class="head" style="margin-top: 25px;">
    <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;">News</h1>
    <!-- <a href="#news" class="next-section about animate2">
      <i class="fa fa-angle-double-down"></i>
    </a> -->
  </div>
  <div class="container">
    <div class="row">
      <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pull-left blgo-full-wrap no-pad">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 side-bar-blog">
          <?php $this->load->view('main/recent_post') ?>
          </div>
          <div class="blog-box single-post">
            <div class="container">
              <div class="row">
                <div class="col-md-9 blog-side">
                  <div class="blog-post triggerAnimation animated" data-animate="fadeInUp">
                    <img alt="" src="images/news-dummy.jpg">
                    <div class="date-post">
                      <ul class="post-tags">
                        <li><a href="#"><i class="fa fa-list-alt"></i><?php echo $this->lang->line('global_category_tag');?>: <?php echo $browse->subtype_name; ?></a></li>
                        <li><a href="#"><i class="fa fa-clock-o"></i><?php 
                        $date = str_replace('/', '-', $browse->publish_date);
                        echo date('M d / Y', strtotime($date));
                        echo '';?></a></li>
                      </ul>
                    </div>
                    <h2 class="padding-custom"><a href="single-post.html"><?php echo $browse->title; ?></a></h2>
                    <?php if ($browse->content_type != 1) { ?>
                    <div class="row">
                      <div class="col-md-12 col-sm-12">
                        <div class="block-row td-post-next-prev">
                          <div class="col-md-6 td-post-prev-post">
                            <div class="post-next-prev-content"><span style="color:#428bca;"><?php echo $browse->subtype_name; ?></span>
                              <p>
                              <?php echo $browse->code; ?><br>
                              <?php echo $this->lang->line('global_category_tag');?>: <?php echo array_key_exists($browse->id_cat, $category)?$category[$browse->id_cat]:'-';?><br>
                              <?php echo $this->lang->line('global_startdate_tag');?>: <?php 
                              $date = str_replace('/', '-', $browse->first_valdate);
                              echo date('d/m/Y h:i:s', strtotime($date));
                              echo '';?><br>
                              Status: <?php echo array_key_exists($browse->status, $status)?$status[$browse->status]:'-';?>
                              </p>
                            </div>
                          </div>
                          <div class="next-prev-separator"></div>
                          <div class="col-md-6 td-post-next-post">
                            <div class="post-next-prev-content post-next-content"><span>&nbsp;</span>
                              <p>
                              <?php echo $this->lang->line('global_publishdate_tag');?>: <?php 
                              $date = str_replace('/', '-', $browse->publish_date);
                              echo date('d/m/Y', strtotime($date));
                              echo '';?><br>
                              <?php echo $this->lang->line('global_subcategory_tag');?>: <?php echo array_key_exists($browse->id_subcat, $subcategory)?$subcategory[$browse->id_subcat]:'-'?><br>
                              <?php echo $this->lang->line('global_enddate_tag');?>: <?php 
                              $date = str_replace('/', '-', $browse->end_valdate);
                              echo date('d/m/Y h:i:s', strtotime($date));
                              echo '';?>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>
                    <?php } ?>
                    <div class="padding-custom">
                      <?php echo isset($contents)? $contents:'' ; ?>
                    </div>
                    <?php if ($attachment) { ?>
                    <div class="padding-custom">
                      <div class=""><strong>Attachment: </strong></div>
                      <div class="">
                        <span><a href="<?php echo base_url().'webcontent/download/'.$browse->file.'.pdf' ?>"><?php echo $browse->file.'.pdf'; ?></a></span>
                      </div>
                    </div>
                    <?php }?>
                    <?php if ($browse->content_type != '1' && $btn_join) { ?>
                    <?php //if (true) { ?>
                    <hr/>
                    <div class="text-center">
                      <a href="<?php echo base_url().'webcontent/registration/'.$this->uri->segment(3) ?>" id="btn_approve" style="margin-top: 0px;" class="btn button-custom">Join</a>
                    </div>
                    <hr/>
                    <?php } ?>
                  </div>
                  <?php if (count($articles)>0) { ?>
                    <div class="row" style="margin-top: 25px;">
                      <div class="col-md-12 col-sm-12">
                        <div class="block-row td-post-next-prev">
                          <div class="col-md-6 td-post-prev-post no-pad">
                            <div class="post-next-prev-content"><span><?php echo $articles['string1'];?></span><a href="#"><?php echo $articles['title1'];?></a></div>
                          </div>
                          <?php echo $separator?>
                          <div class="col-md-6 td-post-next-post no-pad">
                            <div class="post-next-prev-content post-next-content"><span><?php echo $articles['string2'];?></span><a href="#"><?php echo $articles['title2'];?></a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php }?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>