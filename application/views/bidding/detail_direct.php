<script type="text/javascript">
$(document).ready(function(){
   var checkboxes = $(".chwinner");
   var checkboxesinitial = $(".chinitial");
   var numxwin 	  = $("#numxwin").val();
   var is_all = 0;
   $('#btn-cancel-bidding').click(function(e){
   		if (!confirm('Are you sure?')) {
   			e.preventDefault();
            return false;
        }
   });

    checkboxes.click(function() {
        $("#cbawinner").removeAttr("checked");
    });

    $("#cbawinner").click(function() {
        if ($("#cbawinner").is(":checked")) {
            checkboxes.prop("checked",true);
        }else{
            checkboxes.prop("checked",false);
        }
    });

    checkboxesinitial.click(function() {
        $("#cbainitial").removeAttr("checked");
    });

    $("#cbainitial").click(function() {
        if ($("#cbainitial").is(":checked")) {
            checkboxesinitial.prop("checked",true);
        }else{
            checkboxesinitial.prop("checked",false);
        }
    });

    $("#cwainitial").click(function() {
        if ($("#cwainitial").is(":checked")) {
            checkboxesinitial.prop("checked",true);
        }else{
            checkboxesinitial.prop("checked",false);
        }
    });

    $(".btn-edit").click(function() {
        var arg,regnum,venname,aprvdate,vtype;
        arg      = $(this).attr("data");
        regnum   = $('#regnum'+arg).html();
        venname  = $('#vendorname'+arg).html();
        aprvdate = $('#approveddate'+arg).html();
        vtype    = $('#vendortype'+arg).html();
        vstatus  = $('#vendorstatus'+arg).html();

		$('#edit_vendor_reg').val(regnum);
		$('#edit_company_name').val(venname);
		$('#edit_type').val(vtype);
		$('#edit_join_date').val(aprvdate);
		$('#edit_invitation_status').val(vstatus);
		$('#edit_vendor_id').val(arg);
    });

    $('#btn_approve_selected,#btn_reject_selected,#btn_delete_selected').click(function(){
    	var condition = $(this).val().toLowerCase().replace(/\s+/g, '');
    	var status;
    	var value = [];
    	var id_announce = $("#id_announce").val();

    	if(confirm('Are you sure?')){
	    	if (condition == "approveselected") {status = 1;}
	    	else if (condition == "rejectselected"){status = 0;}
	    	else if (condition == "deleteselected"){status = 99;}

	    	$(".chinitial:checked").each(function(){
	    		value.push($(this).val());
	    	});

	    	$.ajax({
			   type: "POST",
			   data: {value:value,status:status,id_announce:id_announce},
			   url: baseUrl + 'bidding/confirm_status',
			   success: function(msg){
			     return false;
			   }
			});

			location.reload();
	    }else{
    		return false;
    	}
    });

    $('.process-winner').click(function(){
    	var value = [];
    	var id_announce = $("#id_announce").val();
    	var pagedecider = $("#pagedecider").attr("data-val");
    	var permitdecider = $("#permitdecider").attr("data-val");
		var status_winner = $(this).attr('id');

    	if(confirm('Are you sure?')){

	    	if(pagedecider && permitdecider == 2){
		    	$(".chwinner").each(function(){
			    	value.push( $(this).val() );
			    });
	    	}else{
	    		$(".chwinner:checked").each(function(){
		    		value.push($(this).val());
		    	});
	    	}

	    	$.ajax({
			   type: "POST",
			   data: {value:value,status_winner:status_winner,id_announce:id_announce},
			   url: baseUrl + 'bidding/decide_winner',
			   beforeSend: function(){
				 $('.statusjoin').html('<img src="' + baseUrl + '/assets/images/spinner.gif"/> Please Wait') ;
			   },
			   success: function(msg){
			     location.reload();
			     return false;
			   }
			});
	    }else{
    		return false;
    	}
    });


	$("#cancel-upload-attachment").on("click", function(){
		if ($(".ajax-file-upload-statusbar").length){
		  	$.ajax({    
		    	type : 'POST',
		    	url  : baseUrl+'bidding/deleteAttachment',
		    	data : 'file_attachment=max',
		    	success :  function(data){
		      		$('#winner-section').modal('toggle');
		      		$('.ajax-file-upload-statusbar').remove();
		    	}
		  	});
		  	return false;
		}
	});

	
	if(numxwin!=0){
		for (var i = 1; i <= numxwin; i++) {
			venid = $("#ven"+i).attr("data_lable");
			$("#attachment-uploader_"+i).uploadFile({
				url: baseUrl+'bidding/upload_attachment/'+venid,
				multiple:false,
				dragDrop:true,
				//maxFileCount:1,
				allowedTypes:"pdf",
				fileName:"pdffile[]",
				onSuccess:function(files,data,xhr,pd)
				{	var coutainerfileupload = $('.ajax-file-upload-container > *').length ;	
					var is_all =0 ;
					for (i=1; i <= numxwin; i++) {
						is_select = $("#select"+i).val();
						if(is_select!='') { is_all = parseInt(is_all) + parseInt(1) }
					}
					if(numxwin==is_all && coutainerfileupload==numxwin) { $("#submit-winner").show(); } else { $("#submit-winner").hide(); }
				}
			});
			$("#attachment-uploader_"+i).on("change",function(){
				$(".ajax-file-upload-container").attr("style","width:200px;");
				$(".ajax-file-upload-filename").attr("style","width:200px;");
				$(".ajax-file-upload-progress").attr("style","width:100%;");
			});
		};
	}

	$("#winningTab").on("click", function(){
		
		var coutainerfileupload = $('.ajax-file-upload-container > *').length ;
		var is_all =0 ;
		for (i=1; i <= numxwin; i++) {
			is_select = $("#select"+i).val();
			if(is_select!='') { is_all = parseInt(is_all) + parseInt(1) }
		}
		if(numxwin==is_all && coutainerfileupload==numxwin) { $("#submit-winner").show(); } else { $("#submit-winner").hide(); }

		$.ajax({ type:'POST', url:baseUrl+'bidding/winningTab_click' });
	});
			
}); 


function deleteparticipant(id_announcex, idvenx)
{
	if(confirm('Are you sure to delete this vendor ?')){
    	$.ajax({
		   	type: "POST",
		   	data: {idannounce:id_announcex,idven:idvenx},
		   	url: baseUrl + 'bidding/delete_participant',
		   	success: function(){
		     	location.reload();
		   	}
		});
    }
}

function selectchange(thisid, rowtable)
{	var coutainerfileupload = $('.ajax-file-upload-container > *').length ;
	var idselect = $("#"+thisid).val();
	var identy = idselect.split("-");
	var numxwin = $("#numxwin").val();
	var is_all = 0;
	if(identy[0]=='3'){
		$("#"+rowtable).attr("class","info");
	}else if(identy[0]=='4'){
		$("#"+rowtable).attr("class","danger");
	}else{
		$("#"+rowtable).attr("class","");
	}

	for (i=1; i <= numxwin; i++) {
		is_select = $("#select"+i).val();
		if(is_select!='') { is_all = parseInt(is_all) + parseInt(1) }
	}

	if(numxwin==is_all && coutainerfileupload==numxwin) { $("#submit-winner").show(); } else { $("#submit-winner").hide(); }
}

function deletedocreq(thisid)
{	
    var arg;
    var answer = confirm("Are you sure ?");
  	if(answer)
  	{
        arg = $("#"+thisid).attr("data");
    	$.ajax({
		   type: "POST",
		   data: {arg:arg},
		   url: baseUrl + 'bidding/deleteDocumentReq',
		   success: function(msg){
		     location.reload();
		   }
		});
	}
}  

</script>
<div class="page-header" style="vertical-align: middle">
  <h3 style="display:inline-block">Detail <?php echo $form; ?></h3>
</div> 

<div id="debug"></div>
<style type="text/css">
.control-label{
	line-height: 2.6;
}
.menu-btn .btn{
	margin-right: 2px;
}
.form-100{
	width: 100%;
}
</style>
<meta name="name" id="pagedecider" data-val="<?php echo $paramAproval ?>" content="content">
<meta name="name" id="permitdecider" data-val="<?php echo $permit->sw?1:2 ?>" content="content">
<input class="form-control" value="<?php echo $this->uri->segment(3); ?>" type="hidden" name="id_announce" id="id_announce" />
<div class="col-sm-12">
	<label class="control-label col-sm-2" for="invitation_number"><strong>Invitation Number</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo $def->code; ?>" type="text" name="invitation_number" id="invitation_number" readonly/>
	</div>

	<label class="control-label col-sm-2" for="space"> </label>
	<div class="col-sm-4">
	</div>
</div>

<div class="col-sm-12">
	<label class="control-label col-sm-2" for="category_header"><strong>Category</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo $def->category; ?>" type="text" name="category_header" id="category_header" readonly/>
	</div>

	<label class="control-label col-sm-2" for="subcontent_type"><strong>Sub Category</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo $def->subcategory; ?>" type="text" name="subcategory" id="subcategory" readonly/>
	</div>
</div>

<div class="col-sm-12">
	<label class="control-label col-sm-2" for="start_date"><strong>Start Date</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo $def->first_valdate; ?>" type="text" name="start_date" id="start_date" readonly/>
	</div>

	<label class="control-label col-sm-2" for="end_date"><strong>End Date</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo $def->end_valdate; ?>" type="text" name="end_date" id="end_date" readonly/>
	</div>
</div>

<div class="col-sm-12">
	<label class="control-label col-sm-2" for="start_date"><strong>Company Name</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="Garuda Indonesia" type="text" name="start_date" id="start_date" readonly/>
	</div>

	<label class="control-label col-sm-2" for="end_date"><strong>Branch Name</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo array_key_exists($def->id_office, $offices)?$offices[$def->id_office]:'-'; ?>" type="text" name="end_date" id="end_date" readonly/>
	</div>
</div>

<div class="col-sm-12">
	<?php
	if ($def->status == 7){$status = 4;}
	elseif (date('Y-m-d H:i:s') < $def->publish_date){$status = 0;}
	elseif (date('Y-m-d H:i:s') >= $def->publish_date && date('Y-m-d H:i:s') < $def->first_valdate){$status = 1;}
	elseif (date('Y-m-d H:i:s') >= $def->first_valdate && date('Y-m-d H:i:s') <= $def->end_valdate){$status = 2;}
	elseif ($def->status == 8){$status = 99;}
	else {$status = 3;}
	?>
	<label class="control-label col-sm-2" for="start_date"><strong>Status</strong></label>
	<div class="col-sm-4">
		<input class="form-control" value="<?php echo array_key_exists($status, $status_ref)?$status_ref[$status]:'-'; ?>" type="text" name="start_date" id="start_date" readonly/>
	</div>
</div>
</div>
<div class="container" style="float:left;padding-left:0px;"><hr/></div>
<div class="container">
	<div class="col-sm-12 form_tab_content" style="float:left;padding-left:0px;">
		<div>
			<ul class="nav nav-tabs">
			    <li class="active"><a data-toggle="tab" href="#english_side" aria-expanded="false"><strong>Content</strong></a></li>
			</ul>
			<div class="tab-content">
				<div id="english_side" class="tab-pane fade active in" style="padding:15px 15px 0;">
					<div class="col-sm-12">
						<label class="control-label" for="end_date"><strong>Title</strong></label>
						<input class="form-control form-100" value="<?php echo $def->title_eng; ?>" type="text" name="title_eng" id="title_eng" readonly/>
					</div>
					<div class="col-sm-12"><hr></div>
					<div class="col-sm-12">
						<label class="control-label" for="end_date"><strong>Description</strong></label>
						<span class="form-control form-100" style="margin-bottom: 9px;max-height: 300px;min-height:100px;overflow-y:auto;"><?php echo $content_eng; ?></span>
					</div>
					<div class="col-sm-12"><hr></div>
					<div class="col-sm-12">
						<label class="control-label" for="end_date"><strong>Aanwijzing</strong></label>
						<span class="form-control form-100" style="margin-bottom: 9px;max-height: 300px;min-height:100px;overflow-y:auto;"><?php echo $aanwidzing_eng; ?></span>
						<br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
<div class="container" style="float:left;padding-left:0px;margin-left:-10px;"><hr/></div>
<div class="container">
	<div class="col-md-12">
			<ul class="list-inline">
				<li><strong>RFP: </strong></li>
				<?php 
				echo $rfpfile?
				"<li><a href='".base_url().'announce/openFileRFP/'.$rfpfile."' target='_blank'>".$rfpfile.".pdf</a></li>":
				"<li>-</li>";
				?>
			</ul>
	</div>
</div>
<div class="container" style="padding-left:0px;margin-left:-10px;"><hr/></div>
<div class="col-sm-12 form_tab_content" style="padding-left:0px;">
	<div>
		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#list_document" aria-expanded="true"><strong>Document Required</strong></a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane fade active in" style="padding:15px 15px 0;">
				<table id="browse" class="table table-striped table-bordered" style="font-size: 13px">
					<thead>
				        <tr>
				            <th>No</th>
				            <th>Document Name</th>
				            <th>Description</th>
				            <th>Remarks</th>
				            <th>Created Date</th>
				            <th>Attachment</th>         
				            <th>Action</th>
				        </tr>
					</thead>
				    <tbody>
				    	<?php
						if(count($documents)>0){ $num = 1; $i=0;
							foreach($documents as $row){ $i++; ?>
								<tr>
									<td><?php echo $num; ?></td>
									<td><?php echo $row->filename.'_'.$row->counter; ?></td>
									<td><?php echo $row->description; ?></td>
									<td><?php echo $row->remarks; ?></td>
									<td><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
									<?php 
									$docreqs 	 = "./uploads/attach/".$row->filename."_".$row->counter.".pdf";
									$docreqs_url = $row->filename."_".$row->counter;
									echo file_exists($docreqs)?
									"<td><a target='_blank' href=".base_url().'uploads/attach/'.$docreqs_url.'.pdf'.">View</a></td>":
									"<td>-</td>";
									?>
									<td width="9%" class="text-center">
										<input onclick="deletedocreq(this.id)" type="button" data="<?php echo $docreqs_url;?>" id="btn_delete<?php echo $i;?>" class="btn btn-info btn-sm btn_delete" title="Delete" value="Delete" style="width:80px;" >
									</td>
								</tr>
						<?php } $num++; ?>
						<?php } else { ?><tr><td colspan="7" style="text-align:center">[ No Documents Entry ]</td></tr><?php } ?>
					</tbody>
				</table>
		    </div>
		</div>
	</div>
</div>
</div>
<div class="container" style="padding-left:0px;"><hr/></div>
<div class="container">
	<div class="col-sm-12 form_tab_content" style="padding-left:0px;">
	<div>
		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#list_participant" aria-expanded="true"><strong>List of Invited Participants</strong></a></li>
		    <li style="position:absolute; right: 16px; border-left: 1px solid #ddd;" class=""><a data-toggle="modal" data-target="#winner-section" style="background-color: #DC3601;color:#fff;cursor:pointer;" id="winningTab" aria-expanded="false"><strong>WINNING PROCESS</strong></a></li>
		</ul>
		<div class="tab-content">
			<div id="list_participant" class="tab-pane fade active in" style="padding:15px 15px 0;">
				<table id="browse" class="table table-striped table-bordered" style="font-size: 13px">
					<thead>
				        <tr>
				            <th>Vendor Registration</th>
				            <th>Company Name</th>
				            <th>Vendor Type</th>
				            <th>Approved Date</th>
				            <th>Status</th>    
				            <th><i class="glyphicon glyphicon-paperclip"></i></th>    
				            <?php if($paramAproval && $permit->sw ){ ?>
				            <th>Action</th>
				            <?php } ?>
				        </tr>
					</thead>
				    <tbody>
				    	<?php			
						if($def_detail){
							foreach($def_detail as $row){ 

								$suratkep = $row->suratkep ;
								$k = explode(',', $suratkep) ;
								$attacment = '' ;
								foreach ($k as $values) {
									if($suratkep!='')
									$attacment .= '<a href="'.base_url("bidding/download/".$values).'" target="_blank"><i class="fa fa-file-pdf-o"></i></a>' ;
								}

								?>	
								<tr class="<?php echo $row->status_vendor == 'redlist'?'tr-shortlist':'' ?>">
									<td id="<?php echo 'regnum'.$row->vendor_id ?>"><?php echo $row->register_num ?></td>
									<td id="<?php echo 'vendorname'.$row->vendor_id ?>"><?php echo $row->vendor_name ?></td>
									<td><?php echo $vendor_type[$row->vendor_type] ?></td>
									<td id="<?php echo 'approveddate'.$row->vendor_id ?>"><?php echo date( 'M d Y', strtotime($row->last_updated)) ?></td>
									<td style="display: none;" id="<?php echo 'vendortype'.$row->vendor_id ?>"><?php echo $row->vendor_type ?></td>
									<td style="display: none;" id="<?php echo 'vendorstatus'.$row->vendor_id ?>"><?php echo $row->status_vendor != 'redlist'?$status_reference[$row->status]:"Redlist" ?></td>
									<td style="display: none;" id="<?php echo 'vendorstatus'.$row->vendor_id ?>"><?php echo $row->stt ?></td>
									<td><div class="statusjoin"><?php echo $row->status_vendor != 'redlist'?$status_reference[$row->stt]:"Redlist" ?></div></td>
									<td><?php echo $attacment ?></td>
									<?php if($paramAproval && $permit->sw){ ?>
									<td width="9%" class="text-center">
										<?php
										if ($def->status == 7){$status = 4;}
										elseif (date('Y-m-d H:i:s') < $def->publish_date){$status = 0;}
										elseif (date('Y-m-d H:i:s') >= $def->publish_date && date('Y-m-d H:i:s') < $def->first_valdate){$status = 1;}
										elseif (date('Y-m-d H:i:s') >= $def->first_valdate && date('Y-m-d H:i:s') <= $def->end_valdate){$status = 2;}
										elseif ($def->status == 8){$status = 99;}
										else {$status = 3;}
										if (($row->status_vendor != 'redlist' AND $row->stt != '2') AND $status != 4) { ?>
										<input type="button" data-toggle="modal" data-target="#edit-section" id="btn_edit" class="btn btn-info btn-sm btn-edit" title="Edit" data="<?php echo $row->vendor_id;?>" value="Edit" style="width:80px;">
										<?php }?>
									</td>
									<?php } ?>
								</tr>
						<?php } ?>
						<?php } else { ?><tr><td colspan="7">-</td></tr><?php } ?>
					</tbody>
				</table>
		    </div>
		    <div id="edit-section" class="modal fade" role="dialog">
			    <div class="modal-dialog">
			      <div class="modal-content">
			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal">&times;</button>
			          <h4 class="modal-title">Edit Participant</h4>
			        </div>
			        <?php echo form_open_multipart('bidding/update_status/'.$this->uri->segment(3),array('id'=>'form-search'));?>
			        <div class="modal-body">
			          	<div class="col-sm-12">
			          		<?php echo form_input(array('name' => 'edit_vendor_id', 'type'=>'hidden', 'id' =>'edit_vendor_id')); ?>
							<label class="control-label col-sm-4" for="edit_vendor_reg">Vendor Reg. Number:</label>
							<div class="col-sm-8">
								<input class="form-control" type="text" name="edit_vendor_reg" id="edit_vendor_reg" readonly/>
							</div>
							<label class="control-label col-sm-4" for="edit_company_name">Company Name:</label>
							<div class="col-sm-8">
								<input class="form-control" type="text" name="edit_company_name" id="edit_company_name" readonly/>
							</div>
							<label class="control-label col-sm-4" for="edit_type">Type:</label>
							<div class="col-sm-8">
								<input class="form-control" type="text" name="edit_type" id="edit_type" readonly/>
							</div>
							<label class="control-label col-sm-4" for="edit_join_date">Join Date:</label>
							<div class="col-sm-8">
								<input class="form-control" type="text" name="edit_join_date" id="edit_join_date" readonly/>
							</div>
							<label class="control-label col-sm-4" for="edit_invitation_status">Invitation Status:</label>
							<div class="col-sm-8">
								<?php echo form_dropdown('edit_invitation_status', $status_select, '','class="form-control" id="edit_invitation_status"'); ?>
							</div>
						</div>
			        </div>
			        <div class="modal-footer">
			          <button type="submit" class="btn btn-primary" name="button-process" value="reject" style="margin-bottom:0;">Submit</button>
			          <button type="button" class="btn btn-danger" style="margin-bottom:0;" data-dismiss="modal">Cancel</button>
			        </div>
			        <?php form_close();?>
			      </div>
			    </div>
		  	</div>

		    <div id="winner-section" class="modal fade" role="dialog">
			  <div class="modal-dialog" style="width:800px">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Select Winner</h4>
			      </div>
			      <div class="modal-body">
			        <table id="browse" class="table table-striped table-bordered" style="font-size: 13px">
						<thead>
					        <tr>
					        	<?php if($paramAproval && $permit->sw){ ?>
					        	<th><input type="checkbox" name="cbawinner" id="cbawinner"></th>
					        	<?php } ?>
					            <th>Reg_num</th>
					            <th>Company Name</th>
					            <th>Vendor Type</th>
					            <?php if($paramAproval && $permit->sw){ ?>
					            <th>Join Date</th>
					            <?php } ?>
					            <th>Status</th>
					            <?php if($paramAproval && $permit->cw){ ?>
					            <th>Select Winner</th>
					            <th>Attacment</th>
					            <?php } ?>
					        </tr>
						</thead>
					    <tbody>
					    	<?php
							if($def_winner_list){ $i = 0;
								foreach($def_winner_list as $row){ $i++; ?>	
									<tr id="row<?php echo $i ?>">
										<?php if($paramAproval && $permit->sw){ ?>
										<td><input type="checkbox" name="chwinner[]" value="<?php echo $row->vendor_id;?>" class="chwinner"></td>
										<?php } ?>
										<td data_lable="<?php echo $row->vendor_id;?>" id="ven<?php echo $i;?>"><?php echo $row->register_num ?></td>
										<td><b><?php echo $row->vendor_name ?></b></td>
										<?php if($paramAproval && $permit->sw){ ?>
										<td><?php echo $vendor_type[$row->vendor_type] ?></td>
										<td><?php echo date( 'M d Y', strtotime($row->last_updated)) ?></td>
										<?php } ?>
										<td><?php echo $status_reference[$row->status] ?></td>
										<?php if($paramAproval && $permit->cw){ ?>
										<td>
											<select name="chwinner[]" class="form-control chwinner" style="width:90px" id="select<?php echo $i ?>" onchange="selectchange('select<?php echo $i ?>','row<?php echo $i ?>');">
												<option value="" selected="selected" >-----</option>
												<option value="3-<?php echo $row->vendor_id;?>">Winner</option>
												<option value="4-<?php echo $row->vendor_id;?>">Reject</option>
											</select> 
										</td>
										<td>
											<div id="attachment-uploader_<?php echo $i; ?>" style="width:200px;">
							      			</div>
							      			<i>*.pdf</i>
										</td>
										<?php } ?>
									</tr>
							<?php } ?>
							<?php } else { ?><tr><td colspan="7">-</td></tr><?php } echo '<input type="hidden" id="numxwin" value="'.$i.'"/>' ;?>
						</tbody>
					</table>
			      </div>
			      <div class="modal-footer">
			      	<?php if($paramAproval && $permit->cw){ ?>
			      	<button type="button" class="btn btn-primary process-winner" id="submit-winner" style="margin-bottom:0;" data-dismiss="modal" >Submit</button>
			      	<?php }elseif($paramAproval && $permit->sw){ ?>
			      	<button type="button" class="btn btn-primary process-winner" id="select-winner" style="margin-bottom:0;" data-dismiss="modal">Submit</button>
			      	<?php } ?>
			        <button type="button" class="btn btn-warning" style="margin-bottom:0;" data-dismiss="modal" id="cancel-upload-attachment">Cancel</button>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>
</div>
<div class="container" style="float:left;padding-left:0px;margin-left:-15px"><hr/></div>
<div style="float:right;" class="btn-group btn-group-lg menu-btn">
	<?php if($permit->uc == '1' && $deptId->id_dept == $def->id_departement){ ?>
	<a class="btn btn-primary" href="<?php echo base_url().$controller[$def->subcontent_type].'/form_update_content/'.$this->uri->segment(3)?>">Edit Content</a>
	<a class="btn btn-info" href="<?php echo base_url().$controller[$def->subcontent_type].'/form_rfp_aanwijzing/'.$this->uri->segment(3)?>">Edit RFP Or Aanwijzing</a>
	<?php } if($permit->cb == '1' && $deptId->id_dept == $def->id_departement){ ?>
	<a class="btn btn-warning" id="btn-cancel-bidding" href="<?php echo base_url().'bidding/cancelBidding/'.$this->uri->segment(3)?>">Cancel Bidding</a>
	<?php } ?>
</div>