<div class="catagory-list wow fadeInLeft" data-wow-delay="0.5s" data-wow-offset="0">
  <div class="side-blog-title"><?php echo $this->lang->line('global_recent_post_tag');?></div>
  <ul>
    <?php foreach ($recent_post as $post) { ?>
      <li style="text-align:justify;">
        <a href="<?php echo $post->id;?>" class="headline"><?php echo $post->title;?></a>
      </li>
    <?php } ?>
  </ul>
</div>