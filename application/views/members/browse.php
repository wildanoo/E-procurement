<script type="text/javascript"> 	
$(function(){
	  var baseUrl   = "<?php echo base_url(); ?>";    
	 
	  $("#srch-term").autocomplete({
			source: baseUrl + 'category/get_cat_tags',
			minLength:1
	   });
	   
	  $("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'category/set_sess_search',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){               		            
				   document.location.href= baseUrl + 'category/';
               }
            });
        });  
	 
	  $('#btn_create').click(function(){  form_create(); });	
});

	function form_create(){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
			BootstrapDialog.show({
			title: 'Add New Member',
            message: $('<div></div>').load(baseUrl + "member/form_create"),          
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){		                	       		                    
		                    var values = $("#form").serializeArray();
				                values.push({name: csrf,value: token});
								values = jQuery.param(values);																	
									 $.ajax({
						                url  : baseUrl + 'member/validate',
						                type : "POST",              		               
						                data : values,
						                success: function(resp){										
											var json = $.parseJSON(resp);
											if(json.status=="false"){											
												$("#msg1").html(json.msg1);	
												$("#msg2").html(json.msg2);	
												$("#msg3").html(json.msg3);	
												$("#msg4").html(json.msg4);	
												$("#msg5").html(json.msg5);	
												$("#msg6").html(json.msg6);	
												$("#msg7").html(json.msg7);	
												$("#msg8").html(json.msg8);
												$("#msg9").html(json.msg9);
												$("#msg10").html(json.msg10);
											} else {
												$("#form").submit();
												dialogRef.close(); 	}
											
										}
					            	  });           
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                    dialogRef.close(); 
				                }
			        }]
        });
	}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Member',
            message: $('<div></div>').load(baseUrl + "member/form_update/" + id),  
            buttons: [{			            	
		                label: 'Update',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){         		                		                    
		                     		var values = $("#form").serializeArray();
				               		values.push({name: csrf,value: token});
									values = jQuery.param(values);	
									 $.ajax({
						                url  : baseUrl + 'member/update_validate',
						                type : "POST",              		               
						                data : values,
						                success: function(resp){										
											var json = $.parseJSON(resp);
											if(json.status=="false"){											
												$("#msg1").html(json.msg1);	
												$("#msg2").html(json.msg2);	
												$("#msg3").html(json.msg3);	
												$("#msg4").html(json.msg4);	
												$("#msg5").html(json.msg5);	
												$("#msg6").html(json.msg6);	
												
											} else {
												$("#form").submit();
												dialogRef.close(); 	
											}										
											
										}
					            	  }); 
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	//document.location.href= baseUrl + 'category/';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}

</script>

<div id="debug"></div>
<div class="page-header"><h3>Members</h3></div> 
 
		<div class="col-sm-3 col-md-3 pull-right">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" id="btn_search" ><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        </div>

<input type="button" id="btn_create" class="btn btn-primary btn-sm" value="Add New" style="width:130px;"/>

<table  class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px;" width="55%">
	<thead>
        <tr>
            <th style="text-align: center;" >No</th>                 
            <th>Member Name</th>  
            <th>Office</th> 
            <th>Region</th>  
            <th>Depart.</th>        
            <th>Level</th>
            <th>Register Date</th>     
            <th>Action</th>  
        </tr>       
	</thead>
    <tbody>
<?php	
if($browse){ $i=1;
	foreach($browse as $row){ ?>	
			<tr>
				<td style="text-align: center;"><?php  echo $num = $num+1;; ?></td>
				<td><?php 	echo ucwords($row->fullname) ?></td>		
				<td><?php 	echo $row->office ?></td>
				<td><?php echo $row->region; ?></td>	
				<td><?php echo $row->dept_name; ?></td>
				<td><?php echo $row->level; ?></td>			
									
				<td style="text-align: center;"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>
				<td style="text-align: center;">
					<?php   
					$del_ico   = '<img src="'.base_url().'assets/images/delete.png" width="20" height="20"/>'; 
					$act_ico   = '<img src="'.base_url().'assets/images/active.png" width="25" height="25"/>'; 
					
					echo $act_ico;
					
					echo '<img src="'.base_url().'assets/images/edit.png" width="20" height="20" onclick="form_update('.$row->id.')"/>';
					
					$js   = "if(confirm('Are you sure to delete record ?')){ return true; } else {return false; }";
							echo anchor('member/delete/'.$row->id, $del_ico,
							array('class'=>'default_link','title'=>'Delete','onclick'=>$js));?>
				</td>		
				
			</tr>
<?php	$i++;  } ?>	
<tr><td colspan="8" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?>
	<tr><td colspan="8" align="center">-</td></tr>
<?php } ?>
    </tbody>
</table>

