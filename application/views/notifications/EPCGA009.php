Dengan hormat,<br/>
<b>Bapak/Ibu <?php echo ucwords($to); ?></b><br/><br/>

Dengan ini, menginformasikan bahwa : <br/><br/>

<table cellpadding="2" cellspacing="2" width="100%">
	<tr><td>Nama Perusahaan</td><td>:</td><td><?php echo ucwords($vendor_name); ?></td></tr>
	<tr><td>Registration Number</td><td>:</td><td><?php echo $reg_num; ?></td></tr>
</table><br/><br/>

Belum melakukan konfirmasi dengan mengisi data bank account sebagai salah satu syarat menjadi Rekanan PT Garuda Indonesia (Persero) Tbk ("Garuda")<br/> 
sampai waktu berakhirnya masa validasi pengisian data bank account yaitu 10 (sepuluh) hari sejak dikirimkannya email beserta link data bank account. <br/><br/>
Info lebih lengkap dapat diakses pada link berikut ini : <br/>
<?php echo $link; ?><br/><br/>

Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br/><br/>
