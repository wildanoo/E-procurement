<script type="text/javascript"> 
    $(function(){
        var baseUrl   = "<?php echo base_url(); ?>";

        /*$('#btnCheckNext').click(function(event){
            event.preventDefault();
            var vend    = $('#vendor_name').val();
            var addr    = $('#vendor_address').val(); 
            var phone   = $('#phone').val(); 
            var fax     = $('#fax').val();  
            var npwp    = $('#npwp').val();
            var cp_name = $('#cp_name').val(); 
            var cp_mobil= $('#cp_mobile').val();
            var cp_email= $('#cp_email').val(); 

            if( vend=='' || addr=='' || phone=='' || fax=='' || npwp=='' || cp_name=='' || cp_mobil=='' || cp_email=='' )
            {   alert('Beberapa Inputan mungkin bermasalah, Cek Formulir Registrasi Anda');
                return false;
            }else{
                return true;
            }
        });*/

        $('#btnCheckNext').click(function(event){
            event.preventDefault();
            /*if(document.getElementById('i_accept').checked && document.getElementById("file_loi").files.length != 0)
            {*/
                var values  = $("#form").serializeArray();          
                var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
                var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
                values.push({name: csrf,value: token}); values = jQuery.param(values);          
                $.ajax({
                   url  : baseUrl + 'joinbiddingsourcing/validate_join_sourcing',
                   type : "POST",                                  
                   data : values,
                   success: function(resp){ //$('#debug').html(resp);
                        var json = $.parseJSON(resp); 
                         if(json.status=="false"){
                            $('#msg1').html(json.msg1); $('#msg6').html(json.msg6);
                            $('#msg2').html(json.msg2); $('#msg7').html(json.msg7);
                            $('#msg3').html(json.msg3);
                            $('#msg8').html(json.msg8);
                            $('#msg4').html(json.msg4); 
                            $('#msg9').html(json.msg9);
                            $('#msg5').html(json.msg5); $('#msg10').html(json.msg10);
                            $('#msg11').html(json.msg11);
                            /*alert('Beberapa Inputan mungkin bermasalah, Cek Formulir Registrasi Anda');  */                 
                         } else {   $("#btn_next").trigger("click"); }
                   }
                });

            /*}else{
                alert('Silakan cek syarat dan ketentuan dan sertakan Dokumen LOI Anda');
                return false;
            }*/
        });

        $('#btnsubmit').click(function(){
            if(!document.getElementById('i_accept').checked){
                alert(' Please accept term and condition ');
            }else if(document.getElementById("file_loi").files.length == 0){
                alert('Please include your LOI document');
            }
            else{
                $("#form").submit();
            }
            return false;
        });

        $("input[type=file]").change(function(){
            var filename = $(this).val() ;
            filex = filename.split(".");
            tipe = filex[filex.length-1] ;

            if(tipe!='pdf') { $(this).val(""); alert('Dukumen harus berupa file PDF'); }
        });
        
    }); 
</script>

<div class="about-content-wrap pull-left" id="#">
    <div class="head" style="margin-top: 25px;">
        <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;">Registrasi</h1>
        <a href="#" class="next-section about animate2">
            <i class="fa fa-angle-double-down"></i>
        </a>
    </div>
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 content-tabs-wrap wow fadeInUp animated" data-wow-delay="1.7s" data-wow-offset="200">
            <div class="col-xs-12 col-sm-12 col-md-12 content-tabs no-pad">            
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#" data-toggle="tab"> Undangan Terbatas </a></li>
                  <li><a>&nbsp;</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-home">
                        <div class = "row">
                            <div class="col-md-12 tab-content">
                                <div class="box2 box-info2 tab-pane fade in active" id="registrasi-step-1">
                                    <div class="box-body">
                                        <div class="about-intro-wrap pull-left">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 faq-tabs-wrap">
                                                    <div class="tabbable tabs-left">
                                                        <div class="tab-content col-md-12 col-sm-12 col-xs-12">
                                                            <div class="tab-pane active" id="a">
                                                                <div class="dept-title-tabs">Detail Pengumuman</div>
                                                                <div class="">
                                                                    <div style="padding:5px;">

                                                                        <div class="col-sm-12">
                                                                            <h3><?php echo $announce->title_ind; ?></h3>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <span class="control-label" style="color:red"><strong>Undangan Pengadaan</strong></span>
                                                                        </div>

                                                                        <div class="col-sm-12">
                                                                            <span class="control-label col-sm-7"><?php echo $announce->code; ?></span>
                                                                            <div class="col-sm-5">
                                                                                <span class="control-label col-sm-5">Tanggal Publis</span>
                                                                                <span class="control-label col-sm-7"  style="color:red">: <?php echo date('d F Y',strtotime($announce->publish_date)); ?> </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-7">
                                                                                <span class="control-label col-sm-4">Kategory</span>
                                                                                <span class="control-label col-sm-8" style="color:red">: <?php echo $categoryx; ?> </span>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <span class="control-label col-sm-5">Tanggal Mulai</span>
                                                                                <span class="control-label col-sm-7" style="color:red">: <?php echo date('d F Y',strtotime($announce->first_valdate)); ?> </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-7">
                                                                                <span class="control-label col-sm-4">Sub Kategory</span>
                                                                                <span class="control-label col-sm-8" style="color:red">: <?php echo $subcategoryx; ?> </span>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <span class="control-label col-sm-5">Tanggal Berakhir</span>
                                                                                <span class="control-label col-sm-7" style="color:red">: <?php echo date('d F Y',strtotime($announce->end_valdate)); ?> </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-7">
                                                                                <span class="control-label col-sm-4">Status</span>
                                                                                <span class="control-label col-sm-8">: <?php echo array_key_exists($announce->status, $status_ref)?$status_ref[$announce->status]:'-'; ?></span>
                                                                            </div>
                                                                        </div>


                                                                        <p>&nbsp;</p>

                                                                        <div class="col-sm-12" align="justify">
                                                                            <span>
                                                                                <?php echo $content_ind; ?>
                                                                                <br><br>
                                                                                <?php echo $aanwidzing_ind; ?>
                                                                            </span>
                                                                        </div>

                                                                        <p>&nbsp;</p>

                                                                        <div class="col-sm-12">
                                                                            <?php 
                                                                                if(($announce->file)!=null) { $filename = '<a href="#"><i class="fa fa-download"> '.$announce->file.'</i></a>';} else { $filename = ''; }
                                                                            ?>
                                                                            <span >
                                                                                Lampiran : <?php echo $filename; ?>
                                                                            </span>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-left" style="margin-right:30px;">
                                            <button type="button" class="btn button-custom" data-toggle="tab" href="#registrasi-step-2">Join</button>
                                        </div> 
                                    </div>
                                </div>
                                <div class="box2 box-info2 tab-pane fade" id="registrasi-step-2">
                                    <div class="box-body">
                                        <div class="about-intro-wrap pull-left">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 faq-tabs-wrap">
                                                    <div class="tabbable tabs-left">
                                                        <div class="tab-content col-md-12 col-sm-12 col-xs-12">
                                                            <div class="tab-pane active" id="a">
                                                                <div class="dept-title-tabs">Kebijakan Vendor</div>
                                                                <div class="background-vendor">
                                                                    <div style="padding:20px;">
                                                                        <?php foreach ($policy_agreements as $policy_agreement) {
                                                                            if ($policy_agreement->type != 0) {
                                                                                echo "<h2><strong>".$policy_agreement->subtype_name."</strong></h2>";
                                                                            }else{
                                                                                echo "<h2><strong>General</strong></h2>";
                                                                            }
                                                                              /*$root = $_SERVER['DOCUMENT_ROOT'];   $path = $root."/uploads/policy_agreement/open_bidding_".$this->session->userdata('lang').".txt";
                                                                            if( file_exists($path)) {   echo file_get_contents($path);          
                                                                            } else { echo ""; } ----> ini yg bener */
                                                                            $root = base_url();   $path = $root."uploads/policy_agreement/".$policy_agreement->file."_".$this->session->userdata('lang').".txt";
                                                                            if( file_exists($path)) {   echo file_get_contents($path);
                                                                            } else { echo file_get_contents($path); }
                                                                            echo "<hr/>";
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-left" style="margin-right:30px;">
                                            <button type="button" class="btn button-custom-back" data-toggle="tab" href="#registrasi-step-1">Kembali</button>
                                            <button type="button" class="btn button-custom" data-toggle="tab" href="#registrasi-step-3">Lanjut</button>
                                        </div> 
                                    </div>
                                </div>
                                <div class="box2 box-info2 tab-pane fade" id="registrasi-step-3">
                                    <div class="box-body">
                                        <div class="dept-title-tabs">Formulir Registrasi </div>
                                        <div class = "container2">
                                            <div class = "row2" style="margin-top:20px;">
                                                <div class="appointment-form col-xs-12 col-md-12 no-pad wow fadeInRight animated" data-wow-delay="1s" data-wow-offset="200">
                                                    <div class="row background-form">
                                                        <div class="appointment-form-title"><i class="fa fa-building fa-1x" style="color: #fff;margin: 0 20px;"></i>Perusahaan</div>
                                                        <?php echo form_open_multipart('joinbiddingsourcing/update_unregistered_vendor',array('id'=>'form')); ?>
                                                        
                                                        <input type="hidden" name="id_subcat"    value="<?php echo $token ;?>" />
                                                        <input type="hidden" name="token"        value="<?php echo $token ;?>" />
                                                        
                                                        <div>
                                                            <div class="appt-form">
                                                                <div id="msg1" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        Nama Vendor
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="appt-form-txt" id="vendor_name" name="vendor_name" placeholder="Nama Vendor" />
                                                                    </div>
                                                                </div>

                                                                <div id="msg4" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        Kategori
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="appt-form-txt" value="<?php echo $categoryx; ?> -> Subcategory <?php echo $subcategoryx; ?>" style="background-color:#EEF" readonly/>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div id="msg2" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        Alamat
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="appt-form-txt" placeholder="Alamat" id="vendor_address" name="vendor_address" />
                                                                    </div>
                                                                </div>
                                                                
                                                                <div id="msg6" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        Nomor Telp
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="appt-form-txt" placeholder="Nomor Telp" id="phone" name="phone" />
                                                                    </div>
                                                                </div>
                                                                
                                                                <div id="msg7" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        Nomor Fax
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="appt-form-txt" placeholder="Nomor Fax" id="fax" name="fax" />
                                                                    </div>
                                                                </div>
                                                                
                                                                <div id="msg8" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        Email
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <?php 
                                                                        if($email!=null) 
                                                                        {
                                                                            echo '<input type="email" class="appt-form-txt" name="email" value="'.$email.'" style="background-color:#EEF" readonly />';
                                                                        }
                                                                        else
                                                                        {
                                                                            echo '<input type="email" class="appt-form-txt" placeholder="Email" name="email" />';
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div id="msg9" style="color: red; font-style: italic; "></div>
                                                                <div class="row">
                                                                    <div class="col-sm-2" style="top:5px">
                                                                        NPWP
                                                                    </div>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="appt-form-txt"placeholder="NPWP" id="npwp" name="npwp" />
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>Kontak Person</div>
                                                        <div class="appt-form">
                                                            <div id="msg5" style="color: red; font-style: italic; "></div>
                                                            <div class="row">
                                                                <div class="col-sm-2" style="top:5px">
                                                                    Kontak Person
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="appt-form-txt" placeholder="Kontak Person" id="cp_name" name="cp_name" />
                                                                </div>
                                                            </div>

                                                            <div id="msg10" style="color: red; font-style: italic; "></div>
                                                            <div class="row">
                                                                <div class="col-sm-2" style="top:5px">
                                                                    Nomor Handphone
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="appt-form-txt" placeholder="Nomor Handphone" id="cp_mobile" name="cp_mobile" />
                                                                </div>
                                                            </div>
                                                            
                                                            <div id="msg11" style="color: red; font-style: italic; "></div>
                                                            <div class="row">
                                                                <div class="col-sm-2" style="top:5px">
                                                                    Email
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <input type="email" class="appt-form-txt" placeholder="Email" id="cp_email" name="cp_email" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php 
                                                            $blank_post = array('id_lang','id_country','id_region','district','postcode','city',
                                                                                'street','house_num','sprefix','ssuffix','building','floor',
                                                                                'room','bank_name','branch_detail','bckey','acc_holder','acc_num','bank_region');                       
                                                            foreach($blank_post as $field){ echo form_hidden($field,""); }
                                                        ?>
                                                        
                                                    
                                                    </div>
                                                    <button type="button" id="btn_next" data-toggle="tab" style="visibility:hidden;" href="#registrasi-step-4" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-left" style="margin-top:30px;">
                                            <button type="button" class="btn button-custom-back" data-toggle="tab" href="#registrasi-step-2">Kembali</button>
                                            <button type="button" class="btn button-custom" id="btnCheckNext">Lanjut</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="box2 box-info2 tab-pane fade" id="registrasi-step-4">
                                    <div class="row background-terms">
                                        <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>Syarat dan Ketentuan</div>
                                        <div class="terms-custom">
                                            <div class="form-group">
                                                
                                                Kebijakan Privasi ini mengatur ketentuan Garuda Indonesia menampung, menggunakan, memelihara dan menangkap informasi yang dikumpulkan dari pengguna (semua pengguna) dari https://www.eproc.garuda-indonesia.com/ . Kebijakan privasi ini berlaku untuk semua produk dan Jasa layanan yang ditawarkan oleh Garuda Indonesia
                                                <br>
                                                <br>
                                                <label>
                                                <input type="checkbox" name="i_accept" id="i_accept" style="margin-right:10px;">Saya menerima syarat dan ketentuan yang berlaku
                                                </label>
                                                <br/>
                                                <br>
                                                <ul class="list-inline">
                                                    
                                                    <li style="width:155px">Lampirkan LOI <i>(*.pdf)</i></li>
                                                    <li><input type="file" name="file_loi" id="file_loi" style="margin-right:10px;"></li>
                                                    
                                                </ul>
                                                <br>
                                                <br>
                                                </span>
                                                <?php echo form_close(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-left" style="margin-top:10px;">
                                        <button type="button" class="btn button-custom-back" data-toggle="tab" href="#registrasi-step-3">Kembali</button>
                                        <button type="button" class="btn button-custom" id="btnsubmit" href="#">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade no-pad" id="tab-profile">
                        <div class="row background-terms">
                            <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>Lihat Registrasi Status</div>
                            <div class="col-md-12 col-sm-12 terms-custom">
                                <div class="form-group terms-text">
                                    Please Enter Registration Number Then Click "Search"
                                    <br>
                                    <br>
                                    <input type="text" class="appt-form-txt" placeholder="Registration Number" style="width:40%;" /><button type="button" class="btn button-search" data-toggle="tab" href="#registrasi-step-6">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>      
            </div>
        </div>
    </div>
</div>