// $(document).ready(function(){
//     var path = window.location.pathname.split( '/' );

//     $('#lang_eng').click(function(){
//         var lang     = 'eng';

//         $.ajax({
//             url  : '/auth/set_lang',
//             type : "POST",
//             data : 'lang='+lang,
//             success: function(resp){
//                 location.reload();
//             }
//         });
//     });

//     $('#lang_ind').click(function(){
//         var lang     = 'ind';

//         $.ajax({
//             url  : '/auth/set_lang',
//             type : "POST",
//             data : 'lang='+lang,
//             success: function(resp){
//                 location.reload();
//             }
//         });
//     });
// });

$(document).ready(function(){
	var site_position = $('.site-position').attr('content');
    $('.sidebar-position#'+site_position);

	$('#lang_eng').click(function(){
		var lang 	 = 'eng';
		var base_url = window.location.origin;
		var dir 	 = window.location.pathname.split( '/' );

		$.ajax({
        	// url  : base_url + '/' + dir[1] + '/auth/set_lang',
        	url  : base_url + '/' + dir[0] + '/auth/set_lang',
        	type : "POST",
        	data : 'lang='+lang,
        	success: function(resp){
           		location.reload();
			}
    	});
	});

	$('#lang_ind').click(function(){
		var lang 	 = 'ind';
		var base_url = window.location.origin;
		var dir 	 = window.location.pathname.split( '/' );

		$.ajax({
        	// url  : base_url + '/' + dir[1] + '/auth/set_lang',
        	url  : base_url + '/' + dir[0] + '/auth/set_lang',
        	type : "POST",
        	data : 'lang='+lang,
        	success: function(resp){
           		location.reload();
			}
    	});
	});
});

$(document).ready(function(){
    $("#statuscoba").click(function(){
        $("#check-status").trigger("click");
    });
    $("#registercoba").click(function(){
        $("#register-custom").trigger("click");
    });

    if (location.hash == "#check") {
		$("#check-status").trigger("click");
	}else if (location.hash == "#regist") {
		$("#register-custom").trigger("click");
	}
});