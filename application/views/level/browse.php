<div class="page-header">
  <h3>Sub Content Level</h3>
</div>
<?php echo form_open("level/update",array('class'=>'form-horizontal')); ?>
<div class="accordion" id="accordion">
						<?php 
						$n=1;
						foreach($content as $rs=>$key) { 
							foreach($key as $name=>$val) { ?>

								<div class="accordion-group">
									<div class="accordion-heading">
										<a class="accordion-toggle target" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $n ?>" ><?php echo $name; ?></a>
									</div>

									<?php //echo $perm; ?>
									<div id="collapse<?php echo $n ?>" class="accordion-body collapse">
										<div class="accordion-inner" style="padding: 0;">
											<?php 
											echo form_hidden("id",$rs);
											echo form_hidden("ttl",count($content));
											$i = 1;
											$idc = "class='checkall'"; ?>
											<b>
												<div class="box-body" style="padding: 0;">
													<table id="example2" class="table table-bordered2 table-hover text-center">
														<!-- thead>
															<tr>
																<th><?php echo form_checkbox('checkall','accept',false,$idc);?></th>
																<th>Check All</th>
															</tr>              	
														</thead> -->
														<tbody>
															<?php
															foreach($val as $row){
																$value = $row["id"];
						 	//$function = $row["id_function"];
						 	//if ($function == 1){ ?>
																<tr>
																	<td style="border-top: none;text-align: center;"><?php echo form_checkbox('check[]',$value, $row["checked"], false); ?></td>
																	<td style="border-top: none;text-align: center;"><?php echo $row["subtype_name"];} ?></td>
																</tr>
																<?php  $i++; }  //} ?>
															</tbody>
														</table>
													</div>
												</b>
											</div>
										</div>
									</div>
									<?php $n++;} ?>
								</div>
			<div class="box-footer" style="padding-left: 30px;">
				<div>
				<?php  $prop = array('class'=>'btn btn-sm btn-default','title'=>'Cancel', 'style'=>'margin-bottom: 0px'); ?>					 
					<?php  //echo anchor('level/groups','Cancel',$prop); ?>
					<?php  echo form_submit('submit', 'Update','class="btn btn-sm btn-primary"'); ?>
				</div>
			</div><!-- /.box-footer -->
			<?php echo form_close(); ?>