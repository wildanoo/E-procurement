<div id="container8">

	<script type="text/javascript">
		$(function () { 
			var baseUrl  = "<?php echo base_url(); ?>";	

			$('#btn_create8').click(function(){  
				$("#container8").load(baseUrl+'vendor/form_correspondence');   		
			});

		}); 

		function download(id){

			var baseUrl  = "<?php echo base_url(); ?>";
			var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
			var token    = '<?php echo $this->security->get_csrf_hash(); ?>';	
			$.post( baseUrl+ "vendor/download_file", { csrf: token, id: id } );

		}

		function update_corresp(id){

			$("#container8").load(baseUrl+'vendor/form_update_correspondence/'+id); 
		}

		function delete_corresp(id){

			var baseUrl  = "<?php echo base_url(); ?>";
			var csrf     = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
			var token    = '<?php echo $this->security->get_csrf_hash(); ?>';
			if (confirm('Are you sure you want to delete this?')) {
				$.post( baseUrl+ "vendor/delete_document", { csrf: token, id: id })
				.done(function( resp ) {
					$("#container8").load(baseUrl+'vendor/load_correspondence');
				});
			}	
		}

		function remind_act(id){
			var cek = confirm('Kirim reminder expired document sekarang?');
			if(cek){
				$.ajax({
					url : baseUrl+'reminder/reminder_manual/'+id,
					success: function(){
						window.location="<?php echo base_url('register/detail') ?>";
					}
				});
			}
		}

	</script>
	<div class="text-center text-warning">
		<?=$this->session->flashdata('msg'); ?>
	</div>

		<div style="text-align: right;">
			<?php if($this->permit->rembtn){if(!empty($remindbut) && $remindbtn == 'yes'){ ?>
				<button type="button" class="btn btn-success" id="btn_remind" onclick="remind_act(<?php echo $this->session->userdata("venID") ?>)">Send Reminder</button>
				<?php  } }?>
				<?php if($this->global->active){ ?>
					<button type="button" id="btn_create8" class="btn btn-info btn-primary">Add New</button>
					<?php } ?>
				</div>
				<table  class="table table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>Document Name</th>
							<th>Document Number</th>
							<th>Description</th>
							<th>Effective Date</th>
							<th>Expired Date</th>		
							<th>Remark</th>
							<th>Remainder</th>
							<th>Notified</th>
							<?php if($this->global->active){ ?><th>Action</th><?php } ?>
						</tr>
					</thead>
					<tbody>

						<?php  
						if($correspondence){ $i=1;
							foreach($correspondence as $row){
								$today1 = date('Y-m-d');
								$range1 = str_replace('-', '/', $row->exp_date);
								$range1 = date('Y-m-d',strtotime($range1 . "-90 days"));
								if($today1 >= $range1 && $today1 <= $row->exp_date){$notifi = "style='background-color: #ff0 !important'";}
								elseif($row->exp_date <= $today1){$notifi = "style='background-color: #f00 !important'";}
								else{$notifi = "style='background-color: #f9f9f9 !important'";}
								?>
								<tr <?php echo $notifi ?>>
									<td><?php echo $i; ?></td>
									<td><?php echo $row->doc_name; ?></td>	
									<td><?php echo $row->doc_num; ?></td>
									<td><?php echo $row->desc; ?></td>
									<td><?php echo $row->eff_date; ?></td>
									<td><?php echo $row->exp_date; ?></td>
									<td><?php echo $row->exp_date; ?></td>
									<td class='reminds' style="text-align: center;"><?php $check = $row->remainder==0 ? "No" : "Yes";  echo $check; ?></td>
									<td><?php if($row->remindcount > 1){echo $row->remindcount." times";}elseif(empty($row->remindcount)){echo " - ";}else{echo $row->remindcount." time";} ?></td>
									<?php if($this->global->active){ ?>
										<td style="text-align: center;">			
											<?php  			
											$icon = '<i class="glyphicon glyphicon-download"></i>';			
											$prop = array('title'=>'Download'); 				 
											echo anchor('vendor/download_file/'.$row->id,$icon,$prop);

											echo '&nbsp;&nbsp;&nbsp;';	
											echo '<i class="fa fa-pencil" onclick="update_corresp('.$row->id.')"></i>'; echo '&nbsp;&nbsp;&nbsp;';					
											echo '<i class="fa fa-trash"  onclick="delete_corresp('.$row->id.')"></i>';	
											?></td>	
											<?php } ?>										
										</tr>
										<?php	$i++;  } ?>	
										<?php } else { ?>
											<tr><td colspan="8">-</td></tr>
											<?php } ?>
										</tbody>
									</table>

								</div>