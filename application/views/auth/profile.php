<div class="col-sm-3 col-md-3 pull-right">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
        <div class="input-group-btn">
            <button class="btn btn-default" id="btn_search" ><i class="glyphicon glyphicon-search"></i></button>
        </div>
    </div>
</div>
<div class="page-header">
  <h3>My Profile</h3>
</div>
<section id="browse" class="" style="font-size: 13px">
	<div>
        <div class="form_tab_content" width="75%;">
            <div style="margin-top: -5px;">
                <ul class="nav nav-tabs test2">
                    <li class="active"><a data-toggle="tab" href="#t_info" aria-expanded="true">Personal Info</a></li>
                    <li class=""><a data-toggle="tab" href="#t_password" aria-expanded="false">Change Password</a></li>
                </ul>

                <div class="tab-content">
                    <div id="t_info" class="tab-pane fade active in" style="padding:15px;">
                        <table width="100%" border="0">
                            <tr>
                                <td style="width: 15%;"><label><strong>Profile Name</strong></label></td><td>:</td>
                                <td>
                                    <input class="form-control" type="text" name="profile" id="profile" placeholder="Profile Name" class="form-control" value="<?php echo $this->session->userdata('username'); ?>" readonly/>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;"><label><strong>System Role</strong></label></td><td>:</td>
                                <td>
                                    <input class="form-control" type="text" name="role" id="role" placeholder="Role" class="form-control" value="<?php echo $browse->name; ?>" readonly/>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;"><label><strong>Last Login</strong></label></td><td>:</td>
                                <td>
                                    <label>11 May 2016 09:04:37</label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="t_password" class="tab-pane fade" style="padding:15px;">
                      <table width="100%" border="0">
                            <tr>
                                <td style="width: 15%;"><label><strong>Current Password*</strong></label></td><td>:</td>
                                <td>
                                    <input class="form-control" type="text" name="cur_password" id="cur_password" placeholder="Current Password" class="form-control" required />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;"><label><strong>New Password*</strong></label></td><td>:</td>
                                <td>
                                    <input class="form-control" type="text" name="new_password" id="new_password" placeholder="New Password" class="form-control" required />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;"><label><strong>Re-Type New Password*</strong></label></td><td>:</td>
                                <td>
                                    <input class="form-control" type="text" name="re_password" id="re_password" placeholder="Re-Type New Password" class="form-control" required />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;"><label><i>*Required Fields</i></label></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: right;">
                                    <input type="submit" value="Save" class="btn btn-primary btn-sm">
                                                         
                                    <a href="#" class="btn btn-primary btn-sm" title="Cancel">Cancel</a>       </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
