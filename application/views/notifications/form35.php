<?php $this->load->view('notifications/header');?>

<b>Kepada Yth.</b><br/>
Calon rekanan PT. Garuda Indonesia.<br/><br/>
Dengan hormat,<br/><br/>
Bersama ini kami sampaikan undangan terbatas <br/>
<b><a href="<?php echo $link;?>">Link Registrasi</a></b><br/>
<br>
Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br>
<br>
<?php echo $sender;?>.

<?php $this->load->view('notifications/footer');?>