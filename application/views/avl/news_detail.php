<div class="page-header">
  <h3 style="display:inline-block">News Detail</h3>
</div> 
<section class="form_tab_content" width="75%;">
  <div style="margin-top: -5px;">
    <ul class="nav nav-tabs test2">
        <li class="active"><a data-toggle="tab" href="#t_eng" aria-expanded="true">News Title</a></li>
    </ul>
    <div class="tab-content">
        <div id="t_eng" class="tab-pane fade active in" style="padding:15px;">
          <div class="page-header col-md-12" style="border: none;">
            <h2><?php echo $browse->title_eng; ?></h2>
            <h6 style="font-size: 13px; text-transform: none;"><?php echo $browse->subtype_name; ?> - <?php 
            $date = str_replace('/', '-', $browse->publish_date);
            echo date('M d, Y H:i A', strtotime($date));
            echo '';?></h6>
          </div>
          <div class="col-md-12 page-content">
            <div class="thumbnail col-md-8" style="margin: 0 auto !important;float: none !important;">
                <img alt="" src="<?php echo base_url(); ?>uploads/images/<?php echo $browse->file;?>.jpg">
            </div>
            <br>
            <p><?php echo isset($contents_eng)? $contents_eng:'[no description]' ; ?></p>
            <br>
            <?php if ($attachment) { ?>
            <hr />
            <section class="row attachment-section" style="margin-left: -15px;">
              <div class="col-md-2"><strong>Attachment: </strong></div>
              <div class="col-md-10 row">
                <span><a href="<?php echo base_url().'avl/open_file/'.$browse->file.'.pdf' ?>"><?php echo $browse->file.'.pdf'; ?></a></span>
              </div>
            </section>
            <?php }?>
            <hr />
          </div>
        </div>
        <div class="col-md-12 page-button-section text-right">
          <a href="<?php echo base_url('avl/news'); ?>" style="vertical-align: baseline;" class="btn btn-primary btn-sm"/>Back</a>
          <br>
          <br>
        </div>
    </div>
  </div>
</section>