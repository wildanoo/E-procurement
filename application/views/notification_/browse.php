<script src="<?php echo base_url(); ?>assets/js/sorttable.js"></script>
</head>
<body>
<div id="debug"></div>
 <div class="row">
      <div class="col-md-12">
        <div class="widget widget-nopad">
          <div class="col-md-12">
            <div class="box box-info">
              
              <div class="box-header with-border">
                <h3 class="box-title">Manage Document</h3>
              </div><!-- /.box-header -->
                <br>
                
                    <?php if($this->session->flashdata('msg_warning')!=null) { ?>
                	<div class="alert alert-warning">
  						<strong>Warning!</strong> <?php echo $this->session->flashdata('msg_warning'); ?>
					</div>
					<?php } ?>
<div class="col-md-2 pull-left">                
<td><a href="<?php echo base_url('notification/form_create'); ?>" type="button" class="btn btn-primary btn-sm">Create New</a></td>
</div>

<?php $msg = $this->session->flashdata('message');
	if(strpos($msg,'failed')) $color="#bc0505";
	else $color = "#487952"; ?>	
<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>	

                <div class="col-sm-3 col-md-3 pull-right" style="margin-right: 120px;">
		        <div class="input-group pull-right" style="width: 150px;">
		            <div class="input-group-btn">
		            <?php echo form_open_multipart('notification/search',array('id'=>'form-search','novalidate' => 'true'));?>
		            <input type="text" class="form-control" placeholder="Search by keyword" value="" class="form-control" id="search_term" name="search_term" style="height: 28px;">
		                <button class="btn btn-default" type="submit" title="Search"><i class="glyphicon glyphicon-search"></i></button>
		     			<a href="<?php echo base_url()?>notification" class="btn btn-srch btn-warning btn-sm">Show All</a>
		            </div>
		        </div>
		        </div>
		        
<div class="box-body">
<table id="example2" class="table table-bordered table-hover text-center sortable">
	<thead>
		<tr>
            <th class="sorttable_sorted_reverse">No</th>
            <th class="">Description Indonesia</th>
            <th class="">Description Englsih</th>
            <!--th class="">Status</th>-->
            <th class="">Created Date</th>
            <th class="">Updated Date</th>
            <th class="">Updated By</th>
            <th>Action</th>
        </tr>
     </thead>
     <tbody>
 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td style="text-align:center;"><?php echo $num = $num+1; ?></td>
			<td><?php echo $row->desc_ind; ?></td>	
			<td><?php echo $row->desc_eng; ?></td>
			<!--td><?php echo $row->status==1 ? 'Active' : 'Not Active'; ?></td>-->
 			<td align="center"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>		
 			<td align="center"><?php echo date('M d, Y',strtotime($row->last_updated)); ?></td>	
 			<td align="center"><?php echo $row->created_id; ?></td>					
				<?php	$del_ico   = '<img src="'.base_url().'assets/images/delete.png" width="20" height="20"/>'; 
						$edit_ico  = '<img src="'.base_url().'assets/images/edit.png" width="20" height="20"/>'; ?>				
				<td style="text-align: center;">			
					<?php
					$pencil = '<i class="fa fa-pencil"></i>';
					echo anchor('notification/form_update/'.$row->id, $pencil,array('class'=>'default_link','title'=>'Edit')); ?>&nbsp;
					<?php									
					$trash  = '<i class="fa fa-trash"></i>';
					$js     = "if(confirm('Are you sure to delete data ?')){ return true; } else {return false; }";
					echo anchor('notification/delete/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete','onclick'=>$js));?>
				</td>						
		</tr>
<?php	$i++;  } ?>	
</table>
<tr><div colspan="9" style="text-align: center;"><?php echo $pagination;?></div></tr>
<?php } else { ?>
	<tr><td colspan="7">-</td></tr>
<?php } ?>  
	</table>  
               </div><!-- /.box-body -->
            </div><!-- /.box-info-->
          </div><!-- /col-md-12 -->
        </div><!-- /.widget -->
      </div><!-- /.col-md-12-->
    </div><!-- /row -->
