</div>
</div>
</div>
</div>
<a href="javascript:;" class="page-quick-sidebar-toggler">
  <i class="icon-login"></i>
</a>
<div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
  <div class="page-quick-sidebar">
  </div>
</div>
</div>

<footer class="page-footer">
    <div class="container page-footer-inner"> Copyright &copy; 2016 PT Garuda Indonesia (Persero) Tbk. All right reserved. Powered by &nbsp;<a href="http://asyst.co.id/"><img src="<?php echo base_url()?>assets_public/images/logo-asyst.png" title="Aero Systems Indonesia" style="vertical-align: middle;"></a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</footer>

<!--JS Inclution-->
<script type="text/javascript" src="<?php echo base_url()?>assets_public/js/jquery-ui-1.10.3.custom.min.js"></script>  
<script type="text/javascript" src="<?php echo base_url()?>assets_public/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets_public/plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets_public/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets_public/js/jquery.scrollUp.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets_public/js/jquery.sticky.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets_public/js/wow.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets_public/js/jquery.flexisel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets_public/js/jquery.imedica.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets_public/js/custom-imedicajs.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets_public/js/custom.js"></script>
<script type='text/javascript'>
$(window).load(function(){
   $('#loader-overlay').fadeOut(2000);
   $("html").css("overflow","visible");
   $('#public-vendor-select').searchableOptionList();
});
</script>

</body>
</html>
