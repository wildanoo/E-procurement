<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Notification extends Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_user');
		$this->load->model('codec');
		islogged_in();
		$this->m_user->authenticate(array(41));
	}	

	function index() {		
			
			$data ['user_id'] = $this->tank_auth->get_user_id ();
			$data ['username'] = $this->tank_auth->get_username ();
			
			$sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
				
			
			$field      = $sess_field != '' ? $sess_field : "notifications";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
			
			if($sess_cat != ''){	$where = array('notifications'=>$sess_cat,'status'=>'1');
			} else { $where = array('status'=>'1');	}
			
			$table = "notifications";
			$page = $this->uri->segment ( 3 );
			$per_page = 10;
			$offset = $this->crud->set_offset ( $page, $per_page, $where);
			$total_rows = $this->crud->get_total_record ("",$table,$where);
			$set_config = array (
					'base_url' => base_url() . '/notification/index',
					'total_rows' => $total_rows,
					'per_page' => $per_page,
					'uri_segment' => 3 
			);
			$config = $this->crud->set_config ( $set_config );
			
			$this->load->library ( 'pagination' );
			$this->pagination->initialize ( $config );
			$paging = $this->pagination->create_links ();
			
			$order = array (
					'field' => 'indeks',
					'order' => 'ASC' 
			);
			$data ['pagination'] = $paging;
			$data ['num'] = $offset;
			$select = "id,indeks,notifications,path,editable,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";

			$sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
				
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
			$data['browse'] = $browse;

	 		$data['view']	= "notification/browse"; 		
 			$this->load->view('layout/template',$data);
			
	}
	
	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
	
		$search_conditions = array(
				'indeks'			=> $_POST['search_term'],
				'notifications'		=> $_POST['search_term'],
				'path'				=> $_POST['search_term'],
				'editable'			=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		$data['view'] = "notification/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{
	
		/* get data for table view from database with pagination */
	
		$table 	     = "notifications t1";
		$select 	 = "t1.id,indeks,notifications,path,editable,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'notification/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
	
		/* get data for table view from database with pagination */
	
		$table 	     = "notifications t1";
		$select 	 = "t1.id,indeks,notifications,path,editable,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,'t1.status = "1"',$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'notification/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	function validate_index(){
	
		$index    = $_POST['indeks'];
		$is_exist = $this->crud->is_exist("","notifications","id",array('indeks'=>$index,'status'=>'1'));
		$status   = $is_exist || !$index ? "false" : "true";
	
		$result['status']  =  $status;
		echo json_encode($result);
	
	}
	
	function is_exist_(){
		$notifications	= $_POST['notifications'];
		$path			= $_POST['path'];
		$editable 		= $_POST['editable'];
		$is_exist		= $this->crud->is_exist('','notifications','id',array('notifications'=>$notifications,'path'=>$path,'editable'=>$editable));
		$status			= $is_exist || $notifications || $path || $editable ? "false" : "true";
		
		$result['status']	= $status;
		echo json_encode($result);
	}
	
	function is_exist(){
		$notifications	= $_POST['notifications'];
		$path			= $_POST['path'];
		$editable 		= $_POST['editable'];
		$is_exist['1']= !$notifications ? "false" : "true";
		$is_exist['2']= !$path 			? "false" : "true";
		$is_exist['3']= !$editable 		? "false" : "true";
		$msg['1']	  = !$notifications ? "notification required" : "";
		$msg['2']	  = !$path			? "path required" : "";
		$msg['3']	  = !$editable 		? "editable required" : "";
				
		if ($is_exist['1']=='true' &&  $is_exist['2']=='true' && $is_exist['3']=='true'){
				$where    = array('notifications'=>$notifications, 'path'=>$path, 'editable'=>$editable,'status'=>'1');
				$checked = $this->crud->is_exist("","position","id",$where);
				$is_exist['1']    = !$checked ? "true" : "false";
				$is_exist['2']    = !$checked ? "true" : "false";
				$is_exist['3']    = !$checked ? "true" : "false";
				$msg['1']	= $checked ? "duplicate notification" : "";
				$msg['2']	= $checked ? "duplicate path" : "";
				$msg['3']	= $checked ? "duplicate editable" : "";
					
		}
		
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2'], 'msg3'=>$msg['3']);
		
		echo json_encode($result);
	}
	
	function form_create() {
		$data ['user_id'] = $this->tank_auth->get_user_id ();
		$data ['username'] = $this->tank_auth->get_username ();
		$data ['view'] = "notification/form_create";	
 		$this->load->view('layout/template',$data);
	}
	
	function create(){
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
		$index = $this->codec->index_notif();
		$data 		= array('id'=>null,
							'indeks'=>$index,
							'notifications'=>$_POST['notifications'],
							'path'=>$_POST['path'],
					  		'editable'=>$_POST['editable'],
							'created_id'=>$userID,
							'created_date'=>$curr_date,
							'last_updated'=>$curr_date);
// 		$where   = array('notifications'=>$_POST['notifications'],'status'=>'0');
// 		$checked = $this->crud->is_exist("","notifications","id",$where);
	
// 		if ($checked){ 
// 			$id = $this->crud->browse("","notifications","notifications",$_POST['notifications'],"false","id")->id;
// 			$update = array('status'=>'1','last_updated'=>$curr_date);
// 			$this->crud->update("","notifications","id",$id,$update);
// 		} else 
		//{ 
			//$id = $this->crud->insert("","notifications",$data);
			$where2 	     = array('notifications'=>$_POST['notifications'],'path'=>$_POST['path'],'editable'=>$_POST['editable']);
			$is_exist2    = $this->crud->is_exist("","notifications","id",$where2);
			
			if(!$is_exist){
				$this->crud->insert("","notifications",$data); }
					
				$msg1 = "succesfully add new notification";
				$msg2 = "duplicate notifications name";
				$msg  = !$is_exist ? $msg1 : $msg2;
			$this->session->set_flashdata('message',$msg);
		
		//}
 		redirect('notification/','refresh');	
	}
	

	function form_update(){		
		$data ['user_id'] = $this->tank_auth->get_user_id ();
		$data ['username'] = $this->tank_auth->get_username ();
		$id   = $this->uri->segment(3);
		$select = "id,notifications,path,editable";
		$data ['def'] = $this->crud->browse ( "", "notifications", "id", $id, "false", $select);
		print_r($data['def']);
		$data ['view'] = "notification/form_update";	
 		$this->load->view('layout/template',$data);
	}
	
	function update(){
		$curr_date 	= date('Y-m-d H:i:s');
		$userID = $this->tank_auth->get_user_id();
		$id   = $this->uri->segment(3);
		$data = array('notifications'=>$_POST['notifications'],'path'=>$_POST['path'],'editable'=>$_POST['editable'],
					  'status'=>'1','last_updated'=> $curr_date,'created_id'=>$userID);
		$this->crud->update("","notifications","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
		redirect('notification/','refresh');	
	}
	
	function delete(){
	
		$id = $this->uri->segment(3);
		$status = array('status'=>'0');
		$this->crud->update("","notifications","id",$id,$status);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('notification/','refresh');
	}
    
    	function delete_(){
		$data ['username'] = $this->tank_auth->get_username ();
		$id = $this->uri->segment(3);
		
		$where1   = array('id_country'=>$id);
		$checked1 = $this->crud->is_exist("","region","id",$where1);
		
		//$checked2 = $this->crud->is_exist("","vendor","id",$where1);
		//if ($checked1 || $checked2){
		if ($checked1){
			$this->session->set_flashdata('msg_warning','This data is already in use');
		}
		else {
			$this->crud->delete("","country","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
		redirect('country/','refresh');
		}
		
			function doc_req(){
		
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				
				$sess_cat   = $this->session->userdata('sess_cat');
				$sess_field   = $this->session->userdata('sess_field');
				
					
				$field      = $sess_field != '' ? $sess_field : "notification";
				$id_cat     = $sess_cat != '' ? $sess_cat  : "";
					
				if($sess_cat != ''){	$where = array('notification'=>$sess_cat,'status'=>'1');
				} else { $where = array('status'=>'1');	}
					
				$table 	    = "notification";
				$page       = $this->uri->segment(3);
				$per_page   = 10;
				$offset     = $this->crud->set_offset($page,$per_page,$where);
				$total_rows = $this->crud->get_total_record("",$table,$where);
				$set_config = array('base_url'=> base_url().'/notification/doc_req','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
				$config     = $this->crud->set_config($set_config);
					
				$this->load->library('pagination');
				$this->pagination->initialize($config);
				$paging = $this->pagination->create_links();
		
				$order 			     = array('field'=>'created_date','order'=>'DESC');
				$data['pagination']  = $paging;
				$data['num']         = $offset;
				$select 			 = "id,desc_eng,desc_ind,status,
								(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
				$sessID  = $this->session->flashdata('anID');
				$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
		
				if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
				else  $browse  = $this->crud->browse_with_paging("",$table." l",'status','1',"true",$select,$where,$order,$config['per_page'],$offset);
				$data['browse'] = $browse;
					
				$data['view']	= "doc_req/browse";
				$this->load->view('layout/template',$data);
			}
			
			private function search_input2($search_dateranges = array(),$search_conditions = array()){
			
				if ($this->session->userdata('search_dateranges') != $search_dateranges) {
					$this->session->set_userdata('search_dateranges',$search_dateranges);
				}else{
					$splits = $this->session->userdata('search_dateranges');
			
					foreach ($splits as $key => $value) {
						$search_dateranges[$key] = $splits[$key];
					}
				}
			
				if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
					$this->session->set_userdata('search_conditions',$search_conditions);
				}else{
					$splits = $this->session->userdata('search_conditions');
			
					foreach ($splits as $key => $value) {
						$search_conditions[$key] = $splits[$key];
					}
				}
			
				$getData = array($search_dateranges,$search_conditions);
			
				return $getData;
			}
		
		
			function search2(){
		
				/* initiate search inputs */
		
				$search_conditions = array(
						'desc_ind'		=> $_POST['search_term'],
						'desc_eng'		=> $_POST['search_term']
				);
		
				$where_conditions  = $this->search_input2("",$search_conditions);
		
				/* ==== */
		
				/* get data from defined function for table view */
		
				$extract = $this->getDataTablesSearch2(10,$where_conditions[0],$where_conditions[1]);
		
				/* ==== */
		
				/* preparing data for display */
		
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
		
				$data['browse']		= $extract['getData'];
				$data['pagination']	= $extract['pagination'];
				$data['num']		= $extract['num'];
		
				$data['view'] = "doc_req/browse";
		
				$this->load->view('layout/template',$data);
		
				/* ==== */
		
			}
		
			private function getDataTables2($limit = NULL)
			{
				/* get data for table view from database with pagination */
		
				$table 	     = "notification t1";
				$select 	 = "t1.id,desc_ind,desc_eng,t1.status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
				$uri_segment = 3;
				$page        = $this->uri->segment($uri_segment);
				$per_page    = $limit;
				$offset      = $this->crud->set_offset($page,$per_page);
		
				$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
		
				$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
				// }
		
				$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
				$set_config = array('base_url'=> base_url().'notification/doc_req','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
				$config     = $this->crud->set_config($set_config);
		
				/** setup for pagination **/
		
				$this->load->library('pagination');
				$this->pagination->initialize($config);
		
				$paging = $this->pagination->create_links();
		
				/** ===== **/
				/* ===== */
		
				/* setup variable to used in another functions */
		
				$data['getData'] 	= $getData;
				$data['pagination'] = $paging;
				$data['num']        = $offset;
		
				/* ===== */
		
				return $data;
		
			}
		
			private function getDataTablesSearch2($limit = NULL,$dateranges = array(),$wherearray = array())
			{
		
				/* get data for table view from database with pagination */
		
				$table 	     = "notification t1";
				$select 	 = "t1.id,desc_ind,desc_eng,status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
				$uri_segment = 3;
				$page        = $this->uri->segment($uri_segment);
				$per_page    = $limit;
				$offset      = $this->crud->set_offset($page,$per_page);
		
				$count_rows  = $this->crud->search_browse('',$table,"COUNT(t1.id) AS COUNT",$where,$dateranges,$wherearray);
		
				$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,'t1.status = "1"',$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
		
				$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
				$set_config = array('base_url'=> base_url().'notification/search2/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
				$config     = $this->crud->set_config($set_config);
		
				/** setup for pagination **/
		
				$this->load->library('pagination');
				$this->pagination->initialize($config);
		
				$paging = $this->pagination->create_links();
		
				/** ===== **/
				/* ===== */
		
				/* setup variable to used in another functions */
		
				$data['getData'] 	= $getData;
				$data['pagination'] = $paging;
				$data['num']        = $offset;
		
				/* ===== */
		
				return $data;
		
			}
		
			function form_create2(){
		
				$data ['user_id'] = $this->tank_auth->get_user_id();
				$data ['username'] = $this->tank_auth->get_username();
				$data['view'] = 'doc_req/form_create';
				$this->load->view('layout/template',$data);
		
			}
		
			function create2(){
		
				$curr_date 	= date('Y-m-d H:i:s');
				$userID = $this->tank_auth->get_user_id();
				$data = array(	'desc_ind'		=>$_POST['desc_ind'],
						'desc_eng'		=>$_POST['desc_eng'],
						'status'		=>'1',
						'created_id'	=>$userID,
						'created_date'	=>$curr_date,
						'last_updated'	=>$curr_date
				);

				$where   = array('desc_ind'=>$_POST['desc_ind'],'status'=>'0');
				$checked = $this->crud->is_exist("","notification","id",$where);
				
				if ($checked){
					$id = $this->crud->browse("","notification","desc_ind",$_POST['desc_ind'],"false","id")->id;
					$update = array('status'=>'1','last_updated'=>$curr_date);
					$this->crud->update("","notification","id",$id,$update);
					$this->session->set_flashdata('message','1 data success insert');
				} else {
					$where2	     = array('desc_ind'=>$_POST['desc_ind']);
					$is_exist    = $this->crud->is_exist("","notification","id",$where2);
				
					if(!$is_exist){
						$this->crud->insert("","notification",$data); }
				
						$msg1 = "succesfully add new document requirement group";
						$msg2 = "duplicate document requirement";
						$msg  = !$is_exist ? $msg1 : $msg2;
				
						$this->session->set_flashdata('message',$msg);
				}
				redirect('notification/doc_req','refresh');
			}
		
			function form_update2(){
		
				$id = $this->uri->segment(3);
				$select = 'id,desc_eng,desc_ind,status,created_id,created_date,last_updated';
				$def = $this->crud->browse('','notification','id',$id,$select);
				$data['def'] = $def;
				$data['view'] = 'doc_req/form_update';
				$this->load->view('layout/template',$data);
			}
		
			function update2(){
		
				$id = $_POST['id'];
				$curr_date 	= date('Y-m-d H:i:s');
				$userID = $this->tank_auth->get_user_id();
				$desc_ind = $_POST['desc_ind'];
				$desc_eng = $_POST['desc_eng'];
				$select = array('desc_ind'=>$desc_ind,'desc_eng'=>$desc_eng,'status'=>'1','created_id'=>$userID,'last_updated'=>$curr_date);
				
				$where	     = array('desc_ind'=>$_POST['desc_ind'],'desc_eng'=>$_POST['desc_eng']);
				$is_exist    = $this->crud->is_exist("","notification","id",$where);
				
				if(!$is_exist){
					
					$this->crud->update("","notification","id",$id,$select); }
				
					$msg1 = "succesfully update document requirement";
					$msg2 = "this data has been used";
					$msg  = !$is_exist ? $msg1 : $msg2;
				
					$this->session->set_flashdata('message',$msg);
				//$this->session->set_flashdata('message','1 data success updated');
				//print_r($_POST);exit;
				redirect('notification/doc_req','refresh');
			}
		
			function delete2(){
				$id = $this->uri->segment(3);
				$status = array('status'=>'0');
				$this->crud->update("","notification","id",$id,$status);
				$this->session->set_flashdata('message','1 data success deleted');
				redirect('notification/doc_req','refresh');
			}
		}