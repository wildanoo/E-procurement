<div id="container11">

	<script type="text/javascript">
		$(function () { 
			var baseUrl  = "<?php echo base_url(); ?>";	
			
			$('#btn_cancel').click(function(){ 
				$("#container11").load(baseUrl+'due_diligence/load_visit_form');   		
			}); 

			$('#btn_add').click(function(){
				$("#container11").load(baseUrl+'due_diligence/add_category_form');
			});

			$('#btn_remove').click(function(){
				onSubmit();
			});

			$('#checkall').click(function(){
				$(this).closest('table').find(':checkbox').prop('checked', this.checked);
			});
			
		}); 

		function onSubmit(){ 
			var fields = $("input[name='ck_list[]']").serializeArray(); 
			if (fields.length === 0){ 
				alert('Nothing selected'); 
				return false;
			}else{ 
				var cek = confirm("Are you sure want to delete category/subcategory?");
				var csrf      	= '<?php echo $this->security->get_csrf_token_name(); ?>'; 
				var token     	= '<?php echo $this->security->get_csrf_hash(); ?>';
				var delcat		= baseUrl+'due_diligence/remove_categories';
				var values 		= $('#catform').serializeArray();
				values.push({name: csrf, value: token});
				values = jQuery.param(values);
				if(cek){
					$.post(delcat, values).done(function(){
						$('#container11').load(baseUrl+'due_diligence/view_category');
					});
				}
			}
		}
		
		function add_assesment(id_subcat){
			var baseUrl  = "<?php echo base_url(); ?>";	
			var id_visit = $("#id_visit").val(); 
			$("#container11").load(baseUrl+'due_diligence/load_assesment/'+id_visit+'/'+id_subcat); 	
		}
		
		function show_notes(id_subcat){
			var baseUrl  = "<?php echo base_url(); ?>";	
			var id_visit = $("#id_visit").val(); 
			$("#container11").load(baseUrl+'due_diligence/show_subcat_notes/'+id_visit+'/'+id_subcat); 	
		}
		
		function add_resumes(id_subcat){
			var baseUrl  = "<?php echo base_url(); ?>";	
			var id_visit = $("#id_visit").val(); 
			$("#container11").load(baseUrl+'due_diligence/form_subcat_resume/'+id_visit+'/'+id_subcat); 	
		}

	</script>
	<div class="col-md-12">
		<button type="button" id="btn_cancel" class="btn btn-default pull-right"><i class="fa fa-backward"></i>&nbsp;&nbsp;Previous</button>
		<?php if($this->permit->md3){ ?><button type="button" class="btn btn-danger pull-right" id="btn_remove">Remove Category</button>
		<button type="button" class="btn btn-success pull-right" id="btn_add">Add Category</button><?php } ?>
	</div>
	<?php echo form_open('#',array('id'=>'catform')); ?>
	<input type="hidden" name="id_visit" id="id_visit" value="<?php echo $id_visit;  ?>"/>
	<input type="hidden" name="id_vendor" value="<?php echo $this->session->userdata("venID"); ?>">
	<table  class="table table-striped table-bordered">
		<thead>
			<tr>
				<th style="text-align: center">No</th>
				<th><input type="checkbox" id="checkall" value="1"></th>
				<th>Category</th>
				<th>Sub Category</th>		
				<?php if($this->permit->md3){ ?><th width="150px">Action</th><?php } ?>
			</tr>
		</thead>
		<tbody>
			<?php 
			if($categories){ $i=1;
				foreach($categories as $row){ ?>
					<tr>
						<td width="30px" style="text-align: center"><?php echo $i; ?></td>
						<td width="30px"><input type="checkbox" name="ck_list[]" id="ck<?php $i ?>" value="<?= $row->id_subcat?>"></td>
						<td ><?php echo $row->category; ?></td>	
						<td><?php echo $row->subcategory; ?></td>
						<?php if($this->permit->md3){ ?>			
							<td style="text-align: center;">
								
								<div class="btn-group" role="group" aria-label="Button group with nested dropdown">
									<div class="btn-group" role="group">
										<button id="btnGroupDrop1" type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" 
										aria-haspopup="true" aria-expanded="false" style="width: 100px" >
										Action&nbsp;&nbsp;<i class="fa fa-chevron-circle-down"></i>
									</button>
									<div class="dropdown-menu btn-secondary" aria-labelledby="btnGroupDrop1">		    	
										<a class="dropdown-item" href="#" onclick="<?php echo "add_assesment($row->id_subcat)";?>">Assesment</a>
										<a class="dropdown-item" href="#" onclick="<?php echo "show_notes($row->id_subcat)";?>">Notes</a>
										<a class="dropdown-item" href="#" onclick="<?php echo "add_resumes($row->id_subcat)";?>">Resumes</a>				     
									</div>
								</div>
							</div>
							
						</td>	
						<?php } ?>										
					</tr>
					<?php	$i++;  } ?>	
					<?php } else { ?>
						<tr><td colspan="3">-</td></tr>
						<?php } ?>
					</tbody>
				</table>
				<?php echo form_close(); ?>
			</div>