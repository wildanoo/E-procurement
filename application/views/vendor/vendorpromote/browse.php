<script type="text/javascript">
	$(function() {
		var baseUrl   = "<?php echo base_url(); ?>";
		var csrf      = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token     = '<?php echo $this->security->get_csrf_hash(); ?>';

		$('#idsubcat').change(function(){
			$('#form_search').submit();
		});

		$('#sortstatus').change(function(){
			$('#form_search').submit();
		});

		$('#form_search').submit(function(){
			var sdata=$(this).serialize();
			var v_url = baseUrl+'vendorpromote/searchprom';
			$.post(v_url, sdata, function(data){
				$('#v_tab').html(data);
			});
		});

		$('#start_date, #end_date').datepicker({
			changeMonth: true, changeYear: true, dateFormat:"yy-mm-dd"
		});

		$('#excelbtn').click(function(){
			var fields = $("input[name='ck_list[]']").serializeArray(); 
			if (fields.length === 0){ 
				alert('Select one or more data!'); 
				return false;
			}else{ 
				$('#myform').submit();
			}
		});

	});

</script>

<div id="main_container">
	<div class="page-header">
		<h3>Promote Vendor To AVL</h3>
	</div>
	
	<div class="table">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-search"></i> <strong>Search Section</strong>
			</div>
			<style type="text/css">
				.list-inline li{
					vertical-align: middle !important;
				}
				.btn-srch{
					margin-bottom: 0;
				}
			</style>
			
			<div class="panel-body">
			<?php echo form_open('',array('id'=>'form_search','novalidate'=>'true','onsubmit'=>'return false')); ?>
				<ul class="list-inline">

					<li style="width:34%"><div class="input-group" style="width:100%"><input placeholder="search by vendor name..." type="text" value="" class="form-control" id="search_term" name="search_term"></div></li>
					<li style="width:7%"><div class="input-group"><button type="submit" class="btn btn-srch btn-primary btn-sm" title="Search" id="searchbtn">Search</button></div></li>
					<li style="width:7%"><div class="input-group"><a href="<?php echo base_url() ?>vendorpromote" class="btn btn-srch btn-warning btn-sm">View All</a></div></li>
					<li style="width: 50%"><button class="btn btn-success pull-right" type="button"  id="excelbtn" onclick="submit_export()" title="Export Vendor List" style="margin-bottom: 0px;"><i class="fa fa-file-excel-o" style="color:#FFFFFF"></i>Export Excel</button></li>
				</ul>
				<?php echo form_close(); ?>
			</div>
			
		</div>
	</div>
	<hr>

	<!-- search part -->

	
	<?php echo form_open('vendorpromote/export_excel',array('id'=>'myform')); ?>
	

	<?php require_once('v_data.php'); ?>

	<?php echo form_close(); ?>
</div>