<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Joinbiddingsourcing extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('m_register','m_vendor','codec','m_user'));
		$this->permit = new stdClass(); $this->global = new stdClass();	
		$this->permit = $this->crud->get_permissions("1"); /* 1 adalah function untuk registrasi  */			
		
		$venID   = $this->session->userdata("venID"); $stsID = 0;	
		if($venID) $stsID = $this->crud->browse("","vendor","id",$venID,"false","id_stsreg")->id_stsreg;		
		$this->global->register = $stsID;
		$this->global->active   = ($this->permit->uvd && $stsID=="3" ) ? true : false;
		$this->permit->md3      = ($this->permit->md3 && $stsID=="4" ) ? true : false;	//print_r($this->permit);

		define(TEXT_PATTERN, 	"/^[a-zA-Z0-9_+ .-]+$/");
		define(ADDRESS_PATTERN, "/^[a-zA-Z0-9_\()+ .-]+$/");
		define(PHONE_PATTERN, 	"/^[0-9 ]+$/");
		define(MOBILE_PATTERN, 	"/^[0-9 ]+$/");
		define(EMAIL_PATTERN, 	'/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/');
    }
    
	function index(){
	 	redirect(base_url()); 
	}


	function validate_join_sourcing(){ 		

	 	/*   Modif By Dedi  */
	 	
	 	$vendor_name 	= $_POST['vendor_name']; 
	 	$vendor_address = $_POST['vendor_address'];
	 	$id_subcat		= $this->input->post('id_subcat');
	 	$result     	= $this->m_register->category_validation($id_subcat);	
	 	$cp_name		= $_POST['cp_name'];
	 	$cp_mobile  	= $_POST['cp_mobile'];
	 	$cp_email		= $_POST['cp_email'];  	
	 	$phone			= $_POST['phone'];
	 	$fax			= $_POST['fax']; 
	 	$email			= $_POST['email']; 
	 	$npwp 			= $_POST['npwp']; 

	 	$vdr_exist      		= $this->crud->is_exist("","vendor","id",array('npwp'=>$npwp));
	 	$val['vendor_name'] 	= $vendor_name && preg_match(TEXT_PATTERN , $vendor_name)? "true" : "false";
	// 	$val['vendor_name'] 	= !$vendor_name 	? "false" : "true"; 
	 	$val['vendor_address'] 	= $vendor_address && preg_match(ADDRESS_PATTERN , $vendor_address)? "true" : "false";
	// 	$val['vendor_address']  = !$vendor_address	? "false" : "true"; 
	 	$val['id_subcat'] 		= (!$id_subcat || !$result)	? "false" : "true";
	 	$val['cp_name'] 		= $cp_name && preg_match(TEXT_PATTERN , $cp_name)? "true" : "false";
	// 	$val['cp_name'] 		= !$cp_name			? "false" : "true";
	 	$val['cp_mobile'] 		= $cp_mobile && preg_match(MOBILE_PATTERN , $cp_mobile)		? "true" : "false";
	 	$val['cp_email'] 		= preg_match(EMAIL_PATTERN , $cp_email)	? "true" : "false"; 	
	 	$val['phone'] 			= preg_match(PHONE_PATTERN , $phone)	? "true" : "false";
	 	$val['fax'] 			= preg_match(PHONE_PATTERN , $fax)		? "true" : "false"; 	
	 	$val['email'] 			= preg_match(EMAIL_PATTERN , $email)	? "true" : "false"; 
	 	$val['npwp']  			= !$vdr_exist && preg_match(PHONE_PATTERN , $npwp)? "true" : "false";
	// 	$val['npwp']  			= !$npwp || $vdr_exist	? "false" : "true"; 

	 	$msg['msg1']  = !$vendor_name 	|| !preg_match(TEXT_PATTERN , $vendor_name)	? "Supplier name required or wrong" : ""; 
	// 	$msg['msg1']  = !$vendor_name 		? "Supplier name required" : ""; 
	 	$msg['msg2']  = !$vendor_address 	|| !preg_match(ADDRESS_PATTERN , $vendor_address)	? "Address required or wrong" : ""; 
	// 	$msg['msg2']  = !$vendor_address	? "Address required" : ""; 
	 	$msg['msg4']  = (!$id_subcat || !$result) ? "Sub category required ( 2 category @4 sub category )" : "";  	
	 	$msg['msg5']  = !$cp_name 	|| !preg_match(ADDRESS_PATTERN , $cp_name)	? "Contact person name required or wrong" : ""; 
	// 	$msg['msg5']  = !$cp_name			? "Contact person name required" : ""; 
	 	$msg['msg11'] = !preg_match(EMAIL_PATTERN , $cp_email)	? "Wrong email" : "";  	
	 	$msg['msg6']  = preg_match(PHONE_PATTERN , $phone)	? "" : "Phone required or wrong";
	 	$msg['msg7']  = preg_match(PHONE_PATTERN , $fax)		? "" : "Fax required or wrong"; 
	 	$msg['msg10'] = $cp_mobile && preg_match(MOBILE_PATTERN , $cp_mobile)		? "" : "Mobile phone required / wrong"; 
	 	$msg['msg8']  = !preg_match(EMAIL_PATTERN , $email)	? "Wrong email" : ""; 
	 	$msg['msg9']  = !$npwp || $vdr_exist || !preg_match(PHONE_PATTERN , $npwp)? "NPWP required or exist or blocked" : ""; 
	 	$msg['status']= in_array("false",$val) ? "false" : "true"; 

	 	echo json_encode($msg);

	}

	// added by richo
	function validate_join(){
	 	
	 	$vendor_name 	= $_POST['vendor_name']; 
	 	$vendor_address = $_POST['vendor_address'];
	 	$id_subcat		= $this->input->post('id_subcat');
	 	$result     	= $this->m_register->category_validation($id_subcat);	
	 	$cp_name		= $_POST['cp_name'];
	 	$cp_mobile  	= $_POST['cp_mobile'];
	 	$cp_email		= $_POST['cp_email'];  	
	 	$phone			= $_POST['phone'];
	 	$fax			= $_POST['fax']; 
	 	$email			= $_POST['email']; 
	 	$npwp 			= $_POST['npwp']; 

	 	$vdr_exists = $this->crud->browse("","vendor","npwp",$npwp,'false','vendor_type,status');
	 	
	 	if (count($vdr_exists)<=0) {
	 		$vdr_exist = "" ;
	 	}elseif ($vdr_exists->vendor_type == 'avl' && $vdr_exists->status == 'active') {
	 		$vdr_exist = 1;
	 	}elseif (($vdr_exists->vendor_type == 'shortlist' && $vdr_exists->status == 'active') || ($vdr_exists->vendor_type == 'reject' && $vdr_exists->status == 'active')) {
	 		$vdr_exist = 2;
	 	}else{
	 		$vdr_exist = 3;
	 	} 		

	 	// $vdr_exist      		= $this->crud->is_exist("","vendor","id",array('npwp'=>$npwp)); 	
	 	$val['vendor_name'] 	= $vendor_name && preg_match(TEXT_PATTERN , $vendor_name)? "true" : "false";
	 	$val['vendor_address'] 	= $vendor_address && preg_match(ADDRESS_PATTERN , $vendor_address)? "true" : "false";
	 	$val['id_subcat'] 		= (!$id_subcat || !$result)	? "false" : "true"; 	 
	 	$val['cp_name'] 		= $cp_name && preg_match(TEXT_PATTERN , $cp_name)? "true" : "false";
	 	$val['cp_mobile'] 		= $cp_mobile && preg_match(MOBILE_PATTERN , $cp_mobile)		? "true" : "false";
	 	$val['cp_email'] 		= preg_match(EMAIL_PATTERN , $cp_email)	? "true" : "false"; 	
	 	$val['phone'] 			= preg_match(PHONE_PATTERN , $phone)	? "true" : "false";
	 	$val['fax'] 			= preg_match(PHONE_PATTERN , $fax)		? "true" : "false"; 	
	 	$val['email'] 			= preg_match(EMAIL_PATTERN , $email)	? "true" : "false"; 
	 	//$val['npwp']  			= $vdr_exist == 3 ? "" : "true"; 
	 	$val['npwp']  			= $vdr_exist != 3 && preg_match(PHONE_PATTERN , $npwp)? "true" : "false";
	 	
	 	$msg['msg1']  = !$vendor_name 	|| !preg_match(TEXT_PATTERN , $vendor_name)	? "Supplier name required or wrong" : ""; 
	 	$msg['msg2']  = !$vendor_address 	|| !preg_match(ADDRESS_PATTERN , $vendor_address)	? "Address required or wrong" : ""; 
	 	$msg['msg4']  = (!$id_subcat || !$result) ? "Sub category required ( 2 category @4 sub category )" : "";  	
	 	$msg['msg5']  = !$cp_name 	|| !preg_match(ADDRESS_PATTERN , $cp_name)	? "Contact person name required or wrong" : ""; 
	 	$msg['msg11'] = !preg_match(EMAIL_PATTERN , $cp_email)	? "Wrong email" : "";  	
	 	$msg['msg6']  = preg_match(PHONE_PATTERN , $phone)	? "" : "Phone required or wrong";
	 	$msg['msg7']  = preg_match(PHONE_PATTERN , $fax)		? "" : "Fax required or wrong"; 
	 	$msg['msg10'] = $cp_mobile && preg_match(MOBILE_PATTERN , $cp_mobile)		? "" : "Mobile phone required / wrong"; 
	 	$msg['msg8']  = !preg_match(EMAIL_PATTERN , $email)	? "Wrong email" : ""; 
	 	$msg['msg9']  = $vdr_exist == 3 || !preg_match(PHONE_PATTERN , $npwp)? "NPWP required or exist or blocked" : $vdr_exist;
	 	$msg['status']= in_array("false",$val) ? "false" : "true";

	 	echo json_encode($msg);
	}

 	/////////////////// Dedi Rudiyanto ///////////////////////

	function form_register_invitation()
	{	
		$token 		 = $this->uri->segment(4); if($token==''){ redirect($_SERVER['HTTP_REFERER']); }
		$id_announce = $this->uri->segment(3);
		
		$unixcode1	 = $this->uri->segment(5);

		$this->session->unset_userdata('token');
		$this->session->unset_userdata('id_announce');
		$this->session->unset_userdata('id_vendor');
		$this->session->unset_userdata('id_subcat');

		$unixcode2	= md5( $id_announce . $token ) ;
		$unixcode2	= substr($unixcode2,5,1) . $unixcode2 . substr($unixcode2,-15,5);
		if($unixcode1!=$unixcode2) { redirect($_SERVER['HTTP_REFERER']); exit(); }

		$select 	= "s.expired_date, s.id_vendor, v.email, v.vendor_type, v.is_unregister";
 		$joins[0][0]= 'vendor v';
		$joins[0][1]= 's.id_vendor = v.id';
		$joins[0][2]= 'left';
		$is_exist   = $this->crud->browse_join("","session_verification s","s.session",$token,"false",$select,$joins,"","","","");

		$today		= date('Y-m-d');
		if($is_exist)
		{	
			if( ($is_exist->expired_date) >= $today )
			{ 
				$venID = $is_exist->id_vendor ;
				// $select2 	 = "a.id AS idcat, a.category, b.id AS idsub, b.subcategory";
		 	// 	$order2 	 = array('field'=>'a.id','order'=>'ASC');
		 	// 	$joins2[0][0]= 'category a';
				// $joins2[0][1]= 'a.id = b.id_cat';
				// $joins2[0][2]= 'left';
				$id_subcat	 = $this->crud->browse("","vendor_category","id_vendor",$venID,"false","id_subcat");
				$announce	 = $this->crud->browse("","announce","id",$id_announce,"false");
					
				$catx	 	 = $this->crud->browse("","category","id",$announce->id_cat,"false","category");
				$subcatx 	 = $this->crud->browse("","subcategory","id",$announce->id_subcat,"false","subcategory");

				$ctpath_id  = "./uploads/contents/".$announce->file."_ind.txt";
				$ctpath_eng = "./uploads/contents/".$announce->file."_eng.txt";
				$aapath_id  = "./uploads/aanwijzing/".$announce->file."_ind.txt";
				$aapath_eng = "./uploads/aanwijzing/".$announce->file."_eng.txt";
				$rfpfile	= "./uploads/rfp/".$announce->file.".pdf";
				$data['content_ind'] 	= file_exists($ctpath_id) ? file_get_contents($ctpath_id) : "-";
		 		$data['content_eng'] 	= file_exists($ctpath_eng) ? file_get_contents($ctpath_eng) : "-";
		 		$data['aanwijzing_ind'] = file_exists($aapath_id) ? file_get_contents($aapath_id) : "-";
		 		$data['aanwijzing_eng'] = file_exists($aapath_eng) ? file_get_contents($aapath_eng) : "-";
		 		$data['attachment'] 	= $this->getAttachmentByFilename($announce->file);
		 		$data['rfpfile'] 		= file_exists($rfpfile) ? $announce->file : false;

		 		$this->session->set_userdata('token', $token);
		 		$this->session->set_userdata('id_announce', $id_announce);
		 		$this->session->set_userdata('id_vendor', $is_exist->id_vendor);
				$this->session->set_userdata('id_subcat', $id_subcat->id_subcat);

				$data['token']   	= $token ;
				//$data['id_subcat']  = $id_subcat->id_subcat ;
				$data['email']		= $is_exist->email ;
				//$data['id_vendor']	= $is_exist->id_vendor ;
				//$data['id_announce']= $id_announce ;
				$data['announce']	  = $announce ;
				$data['categoryx']	  = $catx->category ;
				$data['subcategoryx'] = $subcatx->subcategory ;
				$data['status_ref']   = $this->getStatusCondition();
				$data['vendor_id']    = $venID;
				$data['announce_id']  = $announce->id;

				$data['policy_agreements'] = $this->getPolicyAgreement(1);

				if ($this->session->userdata('lang') == 'ind') {
					if ($is_exist->vendor_type == 'new' AND $is_exist->is_unregister == '1' AND $announce->subcontent_type == '6') {
						$data['view'] = 'join_content/limited_bidding_eng';
					}elseif ($is_exist->vendor_type == 'shortlist' AND $announce->subcontent_type == '6') {
						$data['view'] = 'join_content/bidding_shortlist_eng';
					}elseif ($is_exist->vendor_type == 'shortlist' AND $announce->subcontent_type == '5') {
						$data['view'] = 'join_content/direct_shortlist_eng';
					}
				}else{
					if ($is_exist->vendor_type == 'new' AND $is_exist->is_unregister == '1' AND $announce->subcontent_type == '6') {
						$data['view'] = 'join_content/limited_bidding_eng';
					}elseif ($is_exist->vendor_type == 'shortlist' AND $announce->subcontent_type == '6') {
						$data['view'] = 'join_content/bidding_shortlist_eng';
					}elseif ($is_exist->vendor_type == 'shortlist' AND $announce->subcontent_type == '5') {
						$data['view'] = 'join_content/direct_shortlist_eng';
					}
				}					
			}
			else
			{ 	$data['notiv'] 	  = 'Attention'; 
				$data['view'] 	  = 'join_content/tokenexpired'; 
			}

			$this->load->view('layout/template_public',$data);

		}
		else
		{	
			redirect(base_url())	; 
		}
		
	}

	private function getAttachmentByFilename($filename = ''){
	 	$lists   = $this->crud->browse('','document_requirement','filename',$filename,'true');
	 	$attpath = array();

	 	foreach ($lists as $val) {
	 		array_push($attpath, $val->filename.'_'.$val->counter);
	 	}
	 		
	 	return $attpath;
	}

	private function getPolicyAgreement($type){
		if ($this->session->userdata('lang') == 'eng') {
			$lang = "eng";
		}elseif ($this->session->userdata('lang') == 'ind') {
			$lang = "ind";
		}

		$joins[0][0] = 'subcontent_type st';
		$joins[0][1] = 'st.id = type';
		$joins[0][2] = 'left';

		$where 	 = "status = '1'";
		$select  = "type, file, title";
		$order 	 = array('field'=>'type','sort'=>'DESC');

		$getData = $this->crud->browse("","policy_agreement","type",$type,"false",$select);

		return $getData;
	}

	private function getStatusCondition(){
		$data = array();
		if ($this->session->userdata('lang') == 'ind') {
			$data = array(
					0  => '<span class="label label-default">Baru</span>',
					1  => '<span class="label label-default">Publis</span>',
					2  => '<span class="label label-primary">Dibuka untuk join</span>',
					3  => '<span class="label label-primary">Sedang dilaksanakan</span>',
					4  => '<span class="label label-warning">Ditutup</span>',
					99 => '<span class="label label-danger">Dibatalkan</span>');
		}else{
			$data = array(
					0  => '<span class="label label-default">New</span>',
					1  => '<span class="label label-default">Published</span>',
					2  => '<span class="label label-primary">Open for Joining</span>',
					3  => '<span class="label label-primary">Ongoing</span>',
					4  => '<span class="label label-warning">Closed</span>',
					99 => '<span class="label label-danger">Canceled</span>');
		}

		return $data;
	}

	function update_unregistered_vendor()
	{	
		$id_vendor 		= $this->session->userdata('id_vendor');
		$id_announce	= $this->session->userdata('id_announce');
		$fixed_subcat 	= $this->session->userdata('id_subcat');
		$vendor_name 	= $this->input->post('vendor_name');
		$vendor_address = $this->input->post('vendor_address');
		//$postcode 	= $this->input->post('postcode');
		$phone 			= $this->input->post('phone');
		$fax 			= $this->input->post('fax');
		$email 			= $this->input->post('email');
		$npwp 			= $this->input->post('npwp');

		$cp_name 		= $this->input->post('cp_name');
		$cp_mobile 		= $this->input->post('cp_mobile');
		$cp_email 		= $this->input->post('cp_email');
		$subcat 		= $this->input->post('id_subcat');
		$token 			= $this->input->post('token');
		$token2 		= $this->session->userdata('token');

		//////////// jika dilakukan secara paksa (Inspect Element) MAKA cek lewat disini //////////////////////
		$val['a'] 		= preg_match(TEXT_PATTERN 	, $vendor_name) 	&& $vendor_name 	? "true" : "false";
	 	$val['b'] 		= preg_match(ADDRESS_PATTERN, $vendor_address) 	&& $vendor_address 	? "true" : "false";
	 	$val['c'] 		= preg_match(PHONE_PATTERN 	, $phone)								? "true" : "false";
	 	$val['d'] 		= preg_match(PHONE_PATTERN 	, $fax)									? "true" : "false"; 	
	 	$val['e'] 		= preg_match(EMAIL_PATTERN 	, $email)			&& $email			? "true" : "false"; 
	 	$val['f']  		= preg_match(PHONE_PATTERN 	, $npwp)								? "true" : "false";
	 	$val['g'] 		= preg_match(TEXT_PATTERN 	, $cp_name) 		&& $cp_name 		? "true" : "false";
	 	$val['h'] 		= preg_match(MOBILE_PATTERN , $cp_mobile) 		&& $cp_mobile 		? "true" : "false";
	 	$val['i'] 		= preg_match(EMAIL_PATTERN 	, $cp_email)							? "true" : "false"; 
	 	$checkvalue 	= in_array("false",$val) 											? "false": "true" ;
	 	if( $checkvalue	=='false' ) { redirect( base_url() ,'refresh'); exit; }
	 	///////////////////////////////////////////////////////////////////////////////////////////////////////

		$this->session->unset_userdata('token');
		$this->session->unset_userdata('id_announce');
		$this->session->unset_userdata('id_vendor');
		$this->session->unset_userdata('id_subcat');

		if($token!=$token2){ redirect( base_url() )	; }
		
		$upload_file = $_FILES['file_loi']['name'];

		$curr_date		= date('Y-m-d H:i:s'); 
		
		$announce = $this->crud->browse("","announce","id",$id_announce,"false","file");
		$vendor = $this->crud->browse("","vendor","id",$id_vendor,"false","register_num");
 		$filename1=	$announce->file;				
 		$filename2=	$vendor->register_num.'.pdf';

 		

 		$path = 'uploads' ; 
 		if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }
	    
	    $path .= '/LOI' ; 
	    if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }

	    $path .= '/'.$filename1 ; 
	    if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }

 		$path .= '/'.$filename2 ; 
		$tipe = $_FILES["file_loi"]["type"];
		

		if($tipe=='application/pdf')
			$upload_file = move_uploaded_file($_FILES['file_loi']['tmp_name'], $path) ;

		if( !empty($id_vendor) && !empty($vendor_name) && !empty($vendor_address) && !empty($phone) && !empty($npwp) && !empty($upload_file) )
		{	
			$this->db->trans_start();
			$this->db->trans_strict();
			
			$data = array('vendor_name'=> $vendor_name,
					  'vendor_address' => $vendor_address,
					  //'postcode'	   => $postcode,
					  'phone' 	   	   => $phone,
					  'fax'		       => $fax,
					  'npwp' 	       => $npwp,
					  // 'status' 	   => 'active',
					  'last_updated'   => $curr_date);

	 				  $this->crud->update("","vendor","id",$id_vendor, $data);
	 				  $flag	= 'true';

	 		if( !empty($cp_name) && !empty($cp_mobile) && !empty($cp_email) )
	 		{
		 		$data2 = array(	'id'		=> null,
		 					'id_vendor'		=> $id_vendor,	
		 					'fullname'		=> $cp_name,
		 					'position'		=> '',
							'mobile'		=> $cp_mobile,					
							'email'			=> $cp_email,					
							'status'		=> '1',
							'created_id'	=> '',
							'created_date'	=> $curr_date,
							'last_updated'	=> $curr_date);

							$this->crud->insert("","contact_person",$data2);
			}

			foreach ($subcat as $id_subcat) {
				if($id_subcat!=$fixed_subcat)
				{
				$id_cat= $this->crud->browse("","subcategory","id",$id_subcat,"false","id_cat");
				$data3 = array(	'id'		=> null,
		 					'id_vendor'		=> $id_vendor,	
		 					'id_category'	=> $id_cat->id_cat,
		 					'id_subcat'		=> $id_subcat,					
							'status'		=> '1',
							'created_id'	=> '',
							'created_date'	=> $curr_date,
							'last_updated'	=> $curr_date);

							$this->crud->insert("","vendor_category",$data3);
				}
			}

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE || !$upload_file)
			{
			    $this->db->trans_rollback();
			    redirect($_SERVER['HTTP_REFERER']);
			}
			else
			{
				Send_Email_Tovendor_from_openBidding_announcement($id_vendor);
			    $this->db->trans_commit();
			    
			    if( !empty($token) )
				{
					$this->crud->delete("","session_verification","session",$token);
				}

				$data['notiv'] = 'Success'; 
				$data['view']  = 'join_content/tokenexpired'; 
				$this->load->view('layout/template_public',$data);
			}

		}
		else
		{	
			redirect($_SERVER['HTTP_REFERER'])	;
		}
		
	}


	function insert_vendor_from_open_bidding_announcement()
	{
		$unikform1		= $this->input->post('unikform');
		$unikform2   	= $this->session->userdata('unikform');
		$this->session->unset_userdata('unikform');

		if($unikform1==$unikform2){
			$id_announce	= $this->session->userdata('id_announce');
			$id_cat 		= $this->session->userdata('id_cat');
			$id_subcat 		= $this->session->userdata('id_subcat');
							  $this->session->unset_userdata('id_announce');
							  $this->session->unset_userdata('id_cat');
							  $this->session->unset_userdata('id_subcat');

			$vendor_name 	= $this->input->post('vendor_name');
			$vendor_address = $this->input->post('vendor_address');
			$phone 			= $this->input->post('phone');
			$fax 			= $this->input->post('fax');
			$email 			= $this->input->post('email');
			$npwp 			= $this->input->post('npwp');

			$cp_name 		= $this->input->post('cp_name');
			$cp_mobile 		= $this->input->post('cp_mobile');
			$cp_email 		= $this->input->post('cp_email');

			//////////// jika dilakukan secara paksa (Inspect Element) MAKA cek lewat disini //////////////////////
			$val['a'] 		= preg_match(TEXT_PATTERN 	, $vendor_name) 	&& $vendor_name 	? "true" : "false";
		 	$val['b'] 		= preg_match(ADDRESS_PATTERN, $vendor_address) 	&& $vendor_address 	? "true" : "false";
		 	$val['c'] 		= preg_match(PHONE_PATTERN 	, $phone)								? "true" : "false";
		 	$val['d'] 		= preg_match(PHONE_PATTERN 	, $fax)									? "true" : "false"; 
		 	$val['e'] 		= preg_match(EMAIL_PATTERN 	, $email)			&& $email			? "true" : "false"; 
		 	$val['f']  		= preg_match(PHONE_PATTERN 	, $npwp)								? "true" : "false";
		 	$val['g'] 		= preg_match(TEXT_PATTERN 	, $cp_name) 		&& $cp_name 		? "true" : "false";
		 	$val['h'] 		= preg_match(MOBILE_PATTERN , $cp_mobile) 		&& $cp_mobile 		? "true" : "false";
		 	$val['i'] 		= preg_match(EMAIL_PATTERN 	, $cp_email)							? "true" : "false"; 
		 	$checkvalue 	= in_array("false",$val) 											? "false": "true" ;
		 	if( $checkvalue	=='false' ) { redirect( base_url() ,'refresh'); exit; }
		 	///////////////////////////////////////////////////////////////////////////////////////////////////////

			$regNum 		= $this->codec->register(2);

			$upload_file 	= $_FILES['file_loi']['name'];

			$curr_date		= date('Y-m-d H:i:s'); 
			
			$announce = $this->crud->browse("","announce","id",$id_announce,"false","file,subcontent_type");
	 		$filename1=	$announce->file;				
	 		$filename2=	$regNum.'.pdf';

	 		$path = 'uploads' ; 
	 		if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }
		    
		    $path .= '/LOI' ; 
		    if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }

		    $path .= '/'.$filename1 ; 
		    if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }

	 		$path .= '/'.$filename2 ; 
			$tipe = $_FILES["file_loi"]["type"];

			if($tipe=='application/pdf')
				$upload_file = move_uploaded_file($_FILES['file_loi']['tmp_name'], $path) ;

			if( !empty($vendor_name) && !empty($vendor_address) && !empty($phone) && !empty($email) && !empty($npwp) && !empty($upload_file) )
			{
				$vdr_exist = $this->crud->is_exist("","vendor","id",array('npwp'=>$npwp));
				
				if(!$vdr_exist)
				{
					$data = array( 'id'		   => null,
							  'register_num'   => $regNum,
							  'id_stsreg' 	   => '1',
							  'approval' 	   => '0',
							  'vendor_type'    => 'new',
							  'vendor_num'     => '',
							  'vendor_name'	   => $vendor_name,
							  'vendor_address' => $vendor_address,
							  'postcode'	   => $postcode,
							  'phone' 	   	   => $phone,
							  'fax'		       => $fax,
							  'email' 	   	   => $email,
							  'npwp' 	       => $npwp,
							  'npwp_address'   => '',
							  'npwp_postcode'  => '',
							  'web' 		   => 	'',
							  'status' 		   => 'active',
							  'created_id' 	   => '0',
							  'created_date'   => $curr_date,
							  'last_updated'   => $curr_date);

			 		$vdrID = $this->crud->insert("","vendor", $data);
			 		$vendor= $this->crud->browse("","vendor","register_num",$regNum,"false","id");
			 		
			 		$datacategori = array( 'id'=> null,
							  'id_vendor'      => $vendor->id,
							  'id_category'    => $id_cat,
							  'id_subcat' 	   => $id_subcat,
							  'status'    	   => '1',
							  'created_id' 	   => '0',
							  'created_date'   => $curr_date,
							  'last_updated'   => $curr_date);

			 		$this->crud->insert("","vendor_category", $datacategori);
			 		
			 		if( !empty($cp_name) && !empty($cp_mobile) && !empty($cp_email) )
			 		{
				 		$data2 = array(	'id'		=> null,
				 					'id_vendor'		=> $vendor->id,	
				 					'fullname'		=> $cp_name,
				 					'position'		=> '',
									'mobile'		=> $cp_mobile,					
									'email'			=> $cp_email,					
									'status'		=> '1',
									'created_id'	=> '',
									'created_date'	=> $curr_date,
									'last_updated'	=> $curr_date);

						$this->crud->insert("","contact_person",$data2);
					}
					
					$jointopartisipant = array(	'id'=> null,
		 							'id_announce'	=> $id_announce,
		 							'id_vendor'		=> $vendor->id,
		 							'id_subcontent'	=> $announce->subcontent_type,					
									'status'		=> '6',
									'created_id'	=> '',
									'created_date'	=> $curr_date,
									'last_updated'	=> $curr_date);

					$this->crud->insert("","announce_join_participant",$jointopartisipant);

					$data['notiv'] = 'Success'; 
					$data['view']  = 'join_content/tokenexpired';

					Send_Email_Tovendor_from_openBidding_announcement($vendor->id);

					$this->m_vendor->register_tracking($vdrID,"Registration");
				}
				else
				{
					$vendor= $this->crud->browse("","vendor","npwp",$npwp,"false");
					
					// if($vendor->status == 'blacklist' || $vendor->status == 'redlist' || $vendor->status == 'inactive')
					if($vendor->status == 'active' || $vendor->vendor_type == 'shortlist')
					{
						$where    = array('id_vendor'=>($vendor->id), 'id_announce'=>$id_announce,'id_subcontent'=>($announce->subcontent_type)); 
						$getcategory_exist = $this->crud->is_exist("","announce_join_participant","id",$where);

						if(!$getcategory_exist){
							$jointopartisipant = array(	'id'=> null,
				 							'id_announce'	=> $id_announce,
				 							'id_vendor'		=> $vendor->id,
				 							'id_subcontent'	=> $announce->subcontent_type,					
											'status'		=> '6',
											'created_id'	=> '',
											'created_date'	=> $curr_date,
											'last_updated'	=> $curr_date);

							$this->crud->insert("","announce_join_participant",$jointopartisipant);
							$data['notiv'] = 'Success';
							$data['view']  = 'join_content/statusjoin';

							Send_Email_Tovendor_from_openBidding_announcement($vendor->id);
						}
						else
						{
							$data['notiv'] = 'Attention'; 
							$data['view']  = 'join_content/statusjoin';
						}
					}
					else
					{
						$data['notiv'] = 'Attention';
						$data['view']  = 'join_content/statusjoin';
					}
				}

 	
				$this->load->view('layout/template_public',$data);
			}
			else
			{	
				redirect( base_url() );
			}
		}
		else
		{	
			redirect( base_url() );
		}
		
	}

	function joinShortlist(){
		$id 	   = $_POST['announce_id'];
		$vendor_id = $_POST['vendor_id'];
		$loi_file  = $_FILES['file_loi']["error"] != 4?$_FILES['file_loi']:false;
		$where	   = 'id_announce = "'.$id.'" AND id_vendor = "'.$vendor_id.'"';

		if ($id == '6') {
			$data2 = array('status' => '6');
		}else{
			$data2 = array('status' => '82');
		}
		
		$this->crud->multiple_update("","announce_join_participant",$where,$data2);
		if ($loi_file) {
			$this->uploadLOI($loi_file,$id);
		}
		$data['notiv'] = 'Success';

		$data['view']  = 'join_content/statusjoin';
			
		$this->load->view('layout/template_public',$data);
	}

	private function uploadLOI($files="",$id=""){
		$regNum 	 = $this->codec->register('5');
		$upload_file = $files['name'];
		$curr_date	 = date('Y-m-d H:i:s');
		
		$announce  = $this->crud->browse("","announce","id",$id,"false","file,subcontent_type");
			
 		$filename1 = $announce->file;				
 		$filename2 = $regNum.'.pdf';

 		$path = 'uploads' ; 
 		if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }
	    
	    $path .= '/LOI' ; 
	    if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }

	    $path .= '/'.$filename1 ; 
	    if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }

 		$path .= '/'.$filename2 ; 
		$tipe = $files["type"];			

		if($tipe=='application/pdf')
			$upload_file = move_uploaded_file($files['tmp_name'], $path) ;
	}

	function update_vendor_from_open_bidding_announcement_bylogin()
	{
		$unikform1		= $this->input->post('unikform');
		$unikform2   	= $this->session->userdata('unikform');
		$this->session->unset_userdata('unikform');

		if($unikform1==$unikform2){
			$id_announce	= $this->session->userdata('id_announce');
			$id_cat 		= $this->session->userdata('id_cat');
			$id_subcat 		= $this->session->userdata('id_subcat');
							  $this->session->unset_userdata('id_announce');
							  $this->session->unset_userdata('id_cat');
							  $this->session->unset_userdata('id_subcat');

			$upload_file 	= $_FILES['file_loi']['name'];

			//$regNum 		= $this->codec->register(2);
			$user_id		= $this->tank_auth->get_user_id();

			$curr_date		= date('Y-m-d H:i:s'); 
			$ven_id = $this->crud->browse("","users","id",$user_id,"false","id_vendor");
			$vendor = $this->crud->browse("","vendor","id",$ven_id->id_vendor,"false");
			$announce = $this->crud->browse("","announce","id",$id_announce,"false","file,subcontent_type");
	 		$filename1=	$announce->file;				
	 		$filename2=	$vendor->register_num.'.pdf';

	 		$path = 'uploads' ; 
	 		if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }
		    
		    $path .= '/LOI' ; 
		    if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }

		    $path .= '/'.$filename1 ; 
		    if (!is_dir($path)) { mkdir('./'.$path , 0777, true);  }

	 		$path .= '/'.$filename2 ; 
			$tipe = $_FILES["file_loi"]["type"];			

			if($tipe=='application/pdf')
				$uploaded_file = move_uploaded_file($_FILES['file_loi']['tmp_name'], $path) ;

			if( $uploaded_file )
			{	
				$data = array('last_updated' => $curr_date);

		 		$this->crud->update("","vendor","id",$vendor->id, $data);
		 		
		 		$jointopartisipant = array(	'id'=> null,
		 						'id_announce'		=> $id_announce,
		 						'id_vendor'			=> $ven_id->id_vendor,
		 						'id_subcontent'		=> $announce->subcontent_type,
								'status'			=> '6',
								'created_id'		=> '',
								'created_date'		=> $curr_date,
								'last_updated'		=> $curr_date);

				$this->crud->insert("","announce_join_participant",$jointopartisipant);

				Send_Email_Tovendor_from_openBidding_announcement($vendor->id);

				$data['notiv'] = 'Success';
				$data['view']  = 'join_content/tokenexpired'; 
				$this->load->view('layout/template_public',$data);
			}
			else
			{	
				redirect( base_url() );
			}
		}
		else
		{	
			redirect( base_url() );
		}
		
	}



	function insert_vendor_from_sourcing_announcement()
	{
		$unikform1		= $this->input->post('unikform');
		$unikform2   	= $this->session->userdata('unikform');

		$this->session->unset_userdata('unikform');
							
		if($unikform1==$unikform2){
			$id_announce	= $this->session->userdata('id_announce');
			$id_cat 		= $this->session->userdata('id_cat');
			$id_subcat 		= $this->session->userdata('id_subcat');
							  $this->session->unset_userdata('id_announce');
							  $this->session->unset_userdata('id_cat');
							  $this->session->unset_userdata('id_subcat');

			$vendor_name 	= $this->input->post('vendor_name');
			$vendor_address = $this->input->post('vendor_address');
			$phone 			= $this->input->post('phone');
			$fax 			= $this->input->post('fax');
			$email 			= $this->input->post('email');
			$npwp 			= $this->input->post('npwp');

			$cp_name 		= $this->input->post('cp_name');
			$cp_mobile 		= $this->input->post('cp_mobile');
			$cp_email 		= $this->input->post('cp_email');

			//////////// jika dilakukan secara paksa (Inspect Element) MAKA cek lewat disini //////////////////////
			$val['a'] 		= preg_match(TEXT_PATTERN 	, $vendor_name) 	&& $vendor_name 	? "true" : "false";
		 	$val['b'] 		= preg_match(ADDRESS_PATTERN, $vendor_address) 	&& $vendor_address 	? "true" : "false";
		 	$val['c'] 		= preg_match(PHONE_PATTERN 	, $phone)								? "true" : "false";
		 	$val['d'] 		= preg_match(PHONE_PATTERN 	, $fax)									? "true" : "false"; 	
		 	$val['e'] 		= preg_match(EMAIL_PATTERN 	, $email)			&& $email			? "true" : "false"; 
		 	$val['f']  		= preg_match(PHONE_PATTERN 	, $npwp)								? "true" : "false";
		 	$val['g'] 		= preg_match(TEXT_PATTERN 	, $cp_name) 		&& $cp_name 		? "true" : "false";
		 	$val['h'] 		= preg_match(MOBILE_PATTERN , $cp_mobile) 		&& $cp_mobile 		? "true" : "false";
		 	$val['i'] 		= preg_match(EMAIL_PATTERN 	, $cp_email)							? "true" : "false"; 
		 	$checkvalue 	= in_array("false",$val) 											? "false": "true" ;
		 	if( $checkvalue	=='false' ) { redirect( base_url() ,'refresh'); exit; }
		 	///////////////////////////////////////////////////////////////////////////////////////////////////////

			$regNum 		= $this->codec->register(3);

			$curr_date		= date('Y-m-d H:i:s'); 
			
			$announce = $this->crud->browse("","announce","id",$id_announce,"false","subcontent_type");


			if( !empty($vendor_name) && !empty($vendor_address) && !empty($phone) && !empty($email) && !empty($npwp) )
			{	
				$data = array( 'id'		   => null,
						  'register_num'   => $regNum,
						  'id_stsreg' 	   => '1',
						  'approval' 	   => '0',
						  'vendor_type'    => 'new',
						  'vendor_num'     => '',
						  'vendor_name'	   => $vendor_name,
						  'vendor_address' => $vendor_address,
						  'postcode'	   => $postcode,
						  'phone' 	   	   => $phone,
						  'fax'		       => $fax,
						  'email' 	   	   => $email,
						  'npwp' 	       => $npwp,
						  'npwp_address'   => '',
						  'npwp_postcode'  => '',
						  'web' 		   => '',
						  'status' 		   => 'active',
						  'created_id' 	   => '0',
						  'created_date'   => $curr_date,
						  'last_updated'   => $curr_date);

		 		$vdrID = $this->crud->insert("","vendor", $data);
		 		$vendor= $this->crud->browse("","vendor","register_num",$regNum,"false","id");

				$datacategori = array( 'id'=> null,
						  'id_vendor'      => $vendor->id,
						  'id_category'    => $id_cat,
						  'id_subcat' 	   => $id_subcat,
						  'status'    	   => '1',
						  'created_id' 	   => '0',
						  'created_date'   => $curr_date,
						  'last_updated'   => $curr_date);

		 		$this->crud->insert("","vendor_category", $datacategori);


		 		if( !empty($cp_name) && !empty($cp_mobile) && !empty($cp_email) )
		 		{
			 		$data2 = array(	'id'		=> null,
			 					'id_vendor'		=> $vendor->id,	
			 					'fullname'		=> $cp_name,
			 					'position'		=> '',
								'mobile'		=> $cp_mobile,					
								'email'			=> $cp_email,					
								'status'		=> '1',
								'created_id'	=> '',
								'created_date'	=> $curr_date,
								'last_updated'	=> $curr_date);

					$this->crud->insert("","contact_person",$data2);
				}

				$jointopartisipant = array(	'id'=> null,
		 						'id_announce'		=> $id_announce,
		 						'id_vendor'			=> $vendor->id,
		 						'id_subcontent'		=> $announce->subcontent_type,					
								'status'			=> '6',
								'created_id'		=> '',
								'created_date'		=> $curr_date,
								'last_updated'		=> $curr_date);

				$this->crud->insert("","announce_join_participant",$jointopartisipant);

				Send_Email_Tovendor_from_sourcing_announReg($vendor->id);

				$this->m_vendor->register_tracking($vdrID,"Registration");

				$data['notiv'] = 'Success'; 
				$data['view']  = 'join_content/tokenexpired'; 
				$this->load->view('layout/template_public',$data);
			}
			else
			{	
				redirect( base_url() );
			}
		}
		else
		{	
			redirect( base_url() );
		}
	}
	////////////////////////////////////////////////////////
	 
}