<?php 

require_once APPPATH.'./libraries/PHPExcel.php';
$objPHPExcel = new PHPExcel();

$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFont()->setBold(true);


$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);

// Add some data
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Vendor Blacklist');
$objPHPExcel->getActiveSheet()->SetCellValue('A4', 'No');
$objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Registration No.');
$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Vendor No');
$objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Vendor Name');
$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Start Date');
$objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Reason');
$objPHPExcel->getActiveSheet()->SetCellValue('G4', 'Status');

$no=1;
$numb = 5;
foreach ($blacklist as $row) 
{
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$numb, $no);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$numb, $row->register_num);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$numb, $row->vendor_num);
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$numb, $row->vendor_name);
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$numb, $row->start_date);
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$numb, $row->remark);
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$numb, $row->stt );

	$numb++;
	$no++;
}



$objPHPExcel->setActiveSheetIndex(0);
$filename='vendor_blacklist_'.date('YmdHis').'.xlsx';

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//send to browser
$objWriter->save('php://output');
//save to local folder
// $objWriter->save(str_replace('excel.php', $filename, __FILE__));

exit;


 ?>