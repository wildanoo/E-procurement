<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class News_blast extends Controller {

	function __construct()
	{
		parent::__construct();		
		$this->load->helper('file');
		$this->load->model(array('m_announce','codec'));
		$this->permit = new stdClass();

		/* get function islogged_in() from helpers - to check if user already logged in */
		islogged_in();
		/* ---------------- */

		$this->permit = $this->crud->get_permissions('9');
		define(EPROC_IMAGE_PATH, "./uploads/images/");
		define(EPROC_ATTACHMENT_PATH, "./uploads/attach/");
		define(EPROC_CONTENT_DESC_PATH, "./uploads/contents/");
		define(EPROC_ATTACHMENT_TEMP_PATH, "./uploads/tmp_attach/");
	}

	function form_create(){

	 	$data['user_id']  = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();

	 	$data['company'] 	  = $this->getCompany();
	 	$data['areas'] 	 	  = $this->getAreas();
	 	$data['offices'] 	  = $this->getOffices();
	 	$data['vendor_lists'] = $this->get_vendor();

	 	// set view
	 	$data['view']	= "news_blast/form_create";
	 	$this->clear_cache();
	 	$this->load->view('layout/template',$data);

	}

	function create(){

		/* initial variable */
	 	$filename 		   	  = $this->m_announce->file_name("announce");
	 	$curr_date 		   	  = date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
	 	$dept	 		   	  = afterloginGetDeptId();
	 	$description_field 	  = isset($_POST['editor'])?$_POST['editor']:false;
	 	$image_field	   	  = $_FILES['imagefile']["error"] != 4?$_FILES['imagefile']:false;
	 	$contentfileeng_field = $_FILES['contentfileeng_field']["error"] != 4?$_FILES['contentfileeng_field']:false;
	 	$attachment_field	  = $_FILES['pdffile']["error"] != 4?$_FILES['pdffile']:false;
	 	/* ---------------- */

	 	/* initial data to insert/update DB */
	 	$data 	= array('id'			  => null,
					 	'content_type'	  => '1',
					 	'subcontent_type' => '2',
					 	'id_company'	  => $_POST['id_company'],
					 	'id_region'	  	  => $_POST['id_area'],
					 	'id_office'		  => $_POST['id_office'],
					 	'title_eng'		  => $_POST['title_eng'],
					 	'title_ind'		  => '',
					 	'detail_eng'	  => $_POST['detail_eng'],
					 	'detail_ind'	  => '',
					 	'publish_date'	  => $_POST['publish_date'],
					 	'creator'		  => $_POST['creator'],
					 	'file'			  => $filename,
					 	'status'		  => '2',
					 	'created_id'	  => $userID,
					 	'created_date'	  => $curr_date,
					 	'id_departement'  => $dept?$dept->id_dept:'',
					 	'last_updated'	  => $curr_date);
	 	/* ---------------- */

	 	/* making a file and uploaded if content description inserted by WYSWYG */
	 	if($description_field){
	 		$path 	 = "/uploads/contents/";
			$this->upload_text_into_file($description_field[0],'',$path,$filename."_eng",'txt');
	 	}
	 	/* ---------------- */

	 	/* upload content description file */
	 	if ($contentfileeng_field) {
	 		$ctnfile[0]  = $contentfileeng_field;
	 		$path 	  	 = './uploads/contents/';
	 		$extension 	 = 'txt';
	 		$checker 	 = 0;

 			if ($this->crud->is_valid_to_upload($ctnfile[0],array($extension),5000000,'text')) {
 				$checker++;
		 	}else{
			    redirect('news_blast/form_create','refresh');
 			}

		 	if ($checker == 1) {
	 			$this->upload_text_file($ctnfile[0],$path,$filename."_eng",$extension);
		 	}
	 	}
	 	/* ---------------- */
	 	
	 	/* upload image file */
	 	if($image_field){
	 		unset($_FILES['imagefile']);unset($_FILES['contentfileeng']);
	 		$path 	   = './uploads/images/';
	 		$extension = array('jpg','png','gif');

	 		if ($this->crud->is_valid_to_upload($image_field,$extension,5000000,'picture')) {
		 		$this->upload_image_file($image_field,$path,$filename,$extension);
	 		}else{
			    redirect('news_blast/form_create','refresh');
 			}
	 	}
	 	/* ---------------- */
	 	
	 	/* upload attachment file */
	 	if($attachment_field){
	 		unset($_FILES['imagefile']);unset($_FILES['contentfileeng']);unset($_FILES['pdffile']);
	 		$path 	   = './uploads/attach/';
	 		$extension = 'pdf';

	 		if ($this->crud->is_valid_to_upload($attachment_field,array($extension),5000000,'text')) {
		 		$this->upload_attachment_file($attachment_field,$path,$filename,$extension);
		 	}else{
			    redirect('news_blast/form_create','refresh');
 			}
	 	}
	 	/* ---------------- */

	 	/* inserting/updating data into DB */
	 	$id = $this->crud->insert("","announce",$data);
	 	/* ---------------- */

		/* inserting vendor as participants */
		$this->insertVendorAsRecipients($_POST['vendor_multiselect'],$id);
		/* ---------------- */
		
		/* inserting content history */
		$level_where = "id_group = ".$this->session->userdata('user_group')." AND id_subcontent = '1'";
		$level_id = $this->crud->browse("",'level',"id_group",$this->session->userdata('user_group'),"false","id",$level_where);

	 	$data_history = array('id'	 			=> null,
						 	  'id_announcement' => $id,
						 	  'id_level'		=> $level_id->id,
						 	  'status' 			=> '2',
						 	  'created_date'	=> $curr_date);
	 	$this->crud->insert("","announcement_level_approval",$data_history);
	 	/* ---------------- */

	 	/* send email */
	 	emailSendContentApproval($id);
		/* ---------------- */

	 	/* finalize */
	 	$this->session->set_flashdata('message','1 data successfully inserted');
	 	redirect('announce/','refresh');
	 	/* ---------------- */
	}

	function form_update(){

	 	$id  = $this->uri->segment(3);
	 	$def =  $this->crud->browse("","announce","id",$id,"false");
	 	$data['def'] = $def;
	 	
	 	$data['user_id']  = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
		
		$data['company'] = $this->getCompany();
		$data['areas'] 	 = $this->getAreas();
	 	$data['offices'] = $this->getOffices();
	 	
	 	if($def->file){
	 		$images    = array('jpg','gif','png'); $imgfile = "false";
	 		foreach($images as $imgext){
	 			$imgpath  = "./uploads/images/".$def->file.".".$imgext;
				if(file_exists($imgpath)){	
				$imgfile = $def->file.".".$imgext; 	break;	}
			}
			
			$attpath = "./uploads/attach/".$def->file.".pdf";

			$ctpath_id     = "./uploads/contents/".$def->file."_ind.txt";
			$ctpath_eng    = "./uploads/contents/".$def->file."_eng.txt";
			
	 		$attfile 	   = file_exists($attpath) ? "true" : "false";
	 		$contents[0]   = file_exists($ctpath_id) ? file_get_contents($ctpath_id) : "-";
	 		$contents[1]   = file_exists($ctpath_eng) ? file_get_contents($ctpath_eng) : "-";
	 	}

		$data['attfile']  	  = $attfile=="false" ? "-"  : $def->file.".pdf";
		$data['imgfile']  	  = $imgfile=="false" ? "-"  : $imgfile;	 
	 	$data['contents'] 	  = !$contents ? "-" : $contents;
	 	$data['view']	 	  = "news_blast/form_update";
	 	$data['vendor_lists'] = $this->get_vendor_object($def->id);

	 	$this->clear_cache();
	 	$this->load->view('layout/template',$data);
	}
 
	function get_office(){
	 	$id_area = $_POST['id_area']; //echo "=>".$id_comp;
	 	$select  = " id,office_name";
	 	$getDatas = $this->crud->browse("","offices l","id_region",$id_area,"true",$select); 	
		echo "<option selected='selected' value=''>-- Select Office --</option>";
		foreach ($getDatas as $row){
			echo "<option value='$row->id'>$row->office_name</option>";	
		}
	}
 
	function update(){

		/* initial variable */
		$id 				  = $_POST['id'];
	 	$def   				  = $this->crud->browse("","announce","id",$id,"false");
	 	$filename 			  = !$def->file ? $this->m_announce->file_name("announce") : $def->file;
	 	$curr_date 		   	  = date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();
	 	$dept	 		   	  = afterloginGetDeptId();
	 	$description_field 	  = isset($_POST['editor'])?$_POST['editor']:false;
	 	$image_field	   	  = $_FILES['imagefile']["error"] != 4?$_FILES['imagefile']:false;
	 	$contentfileeng_field = $_FILES['contentfileeng']["error"] != 4?$_FILES['contentfileeng']:false;
	 	$attachment_field	  = $_FILES['pdffile']["error"] != 4?$_FILES['pdffile']:false;
	 	/* ---------------- */

	 	/* initial data to insert/update DB */
	 	$data 		= array('id_company'	 => !$_POST['id_company'] 	? $def->id_company : $_POST['id_company'],
	 						'id_region'	  	 => !$_POST['id_area'] 		? $def->id_region : $_POST['id_area'],
						 	'id_office'		 => !$_POST['id_office']  	? $def->id_office : $_POST['id_office'],
						 	'title_eng'		 => !$_POST['title_eng']    ? $def->title_eng : $_POST['title_eng'],
						 	'detail_eng'	 => !$_POST['detail_eng']   ? $def->detail_eng : $_POST['detail_eng'],
						 	'publish_date'	 => !$_POST['publish_date'] ? $def->publish_date : $_POST['publish_date'],
						 	'creator'		 => !$_POST['creator']      ? $def->creator : $_POST['creator'],
						 	'file'			 => $filename,
						 	'code'			 => null,
						 	'status'		 => '2',
						 	'created_id'	 => $userID,
						 	'created_date'	 => $curr_date,
						 	'code'			 => $reg_num,
						 	'id_departement' => $dept?$dept->id_dept:$def->id_departement,
						 	'last_updated'	 => $curr_date);
		/* ---------------- */
			
		/* making a file and uploaded if content description inserted by WYSWYG */
		if($description_field){
	 		$path 	 = "/uploads/contents/";
			$this->upload_text_into_file($description_field[0],'',$path,$filename."_eng",'txt');
	 	}
	 	/* ---------------- */

	 	/* upload content description file */
	 	if ($contentfileeng_field) {
	 		$ctnfile[0]  = $contentfileeng_field;
	 		$path 	  	 = './uploads/contents/';
	 		$extension 	 = 'txt';
	 		$checker 	 = 0;

 			if ($this->crud->is_valid_to_upload($ctnfile[0],array($extension),5000000,'text')) {
 				$checker++;
		 	}else{
			    redirect('news_blast/form_update/'.$id,'refresh');
 			}

		 	if ($checker == 1) {
	 			$this->upload_text_file($ctnfile[0],$path,$filename."_eng",$extension);
		 	}
	 	}
	 	/* ---------------- */
	 	
	 	/* upload image file */
	 	if($image_field){
	 		unset($_FILES['imagefile']);unset($_FILES['contentfileeng']);
	 		$path 	   = './uploads/images/';
	 		$extension = array('jpg','png','gif');

	 		if ($this->crud->is_valid_to_upload($image_field,$extension,5000000,'picture')) {
		 		$this->upload_image_file($image_field,$path,$filename,$extension);
	 		}else{
			    redirect('news_blast/form_update/'.$id,'refresh');
 			}
	 	}
	 	/* ---------------- */
	 	
	 	/* upload attachment file */
	 	if($attachment_field){
	 		unset($_FILES['imagefile']);unset($_FILES['contentfileeng']);unset($_FILES['pdffile']);
	 		$path 	   = './uploads/attach/';
	 		$extension = 'pdf';

	 		if ($this->crud->is_valid_to_upload($attachment_field,array($extension),5000000,'text')) {
		 		$this->upload_attachment_file($attachment_field,$path,$filename,$extension);
		 	}else{
			    redirect('news_blast/form_update/'.$id,'refresh');
 			}
	 	}
	 	/* ---------------- */

	 	/* inserting vendor as participants */
	 	$this->crud->delete("","announce_join_participant","id_announce",$id);
	 	$this->insertVendorAsRecipients($_POST['vendor_multiselect'],$id);
	 	/* ---------------- */

 		/* inserting/updating data into DB */
 	 	$this->crud->update("","announce","id",$_POST['id'],$data);
 	 	/* ---------------- */

 	 	/* send email */
	 	emailSendContentApproval($id);
	 	/* ---------------- */

	 	/* finalize */
	 	$this->session->set_flashdata('message','1 data successfully updated');
	 	redirect('announce/','refresh');
	 	/* ---------------- */
	}

	function get_tags(){
		$term   = $_GET['term'];
		$order  = array('field'=>'code','sort'=>'ASC'); $select = "code as name";
		$result = $this->crud->autocomplete("","announce",$select,"code",$term,"",$order);
		echo json_encode($result);
	}

	function getCompany(){
	 	
	 	$select2 	 = "id,code,company_name as name";

	 	$getDatas = $this->crud->browse("","company l","","","true",$select2); 
	 	if(!$getDatas) $getDatas = array();
	 	foreach($getDatas as $val){ $options[$val->id] = $val->name; } 

	 	$getData = $options;

	 	return $getData;
	}

	function get_vendor(){

	 	$id_subcat  = $_POST['id_subcat'];
	   	
	   	$select = "vendor.id,vendor_name,subcategory,vendor_category.id_subcat,vendor.status,vendor.vendor_type";
	   	$order['field']	= "subcategory.id";
	   	$order['sort']	= "DESC";
	   	if ($id_subcat == 'blast') {
	 		$where = "(vendor.vendor_type='avl') AND (vendor.status <> 'blacklist' OR vendor.status <> 'inactive')";
	 	}else{
	 		$where = "(vendor_type='avl') AND vendor.status NOT IN ('blacklist', 'inactive')";
	 	}

	   	$joins[0][0] = 'vendor';
	   	$joins[0][1] = 'vendor.id = vendor_category.id_vendor';
	   	$joins[0][2] = 'right';
	   	$joins[1][0] = 'subcategory';
	   	$joins[1][1] = 'vendor_category.id_subcat = subcategory.id';
	   	$joins[1][2] = 'left';	   		

	   	$this->db->distinct();
	   	$vendor  = $this->crud->browse_join("","vendor_category","","","true",$select,$joins,$where,$order);
	   		
	   	$element = '';
	   	if (count($vendor) > 0) {
	   		$subcat 	  = $vendor[0]->id_subcat;
	   		$group_subcat = $vendor[0]->subcategory;
	   		$optg_open 	  = "<optgroup label='".$group_subcat."'>";
	   		$optg_close   = "";
	   		$incr 		  = 1;
	   		$element 	 .= $optg_open;
	   		foreach ($vendor as $row){
		  		if ($subcat != $row->id_subcat ) {
		  			$subcat 	  = $row->id_subcat;
	   				$group_subcat = $row->subcategory;
	   				$optg_open  = "<optgroup label='".$group_subcat."'>";
	   				$optg_close = "</optgroup>";
	   			}else{
	   				$optg_open  = "";
	   				$optg_close = "";
	   			}
	   			$element .= $optg_close.$optg_open."<option data-increment='".$incr."' value='".$row->id."'>".$row->vendor_name.' ('.$row->vendor_type.' - '.$row->status.')'."</option>";
	   			$incr++;
	  		}
	  			
	   	}else{
	   		$element .= "<li>&nbsp;&nbsp;&nbsp;&nbsp;
	 				--- No vendor(s) available for this category ---
	 			</li>";
	   	}

	  	return $element;
	}

	function get_vendor_object($announce_id){

		// ========================================== //

	   	$objects = $this->crud->browse("","announce_join_participant","","","true","id_vendor","id_announce = ".$announce_id);
	   	$object  = array();
	   	foreach ($objects as $val){array_push($object, $val->id_vendor);}

	   	// ========================================== //

	   	$subcontent_details = $this->crud->browse("","announce","id",$announce_id,"false","subcontent_type");
		foreach ($subcontent_details as $subcontent_detail) {}

		// ========================================== //

		$select = "vendor.id,vendor_name,subcategory,vendor_category.id_subcat";
	   	$order['field']	= "subcategory.id";
	   	$order['sort']	= "DESC";

	   	$id_subtype = $subcontent_detail;
	   	
	   	$select = "vendor.id,vendor_name,subcategory,vendor_category.id_subcat";
	   	$order['field']	= "subcategory.id";
	   	$order['sort']	= "DESC";
	   	if ($id_subtype != '6' && $id_subtype != '5') {
	 		$where = "(vendor.vendor_type='avl')";
	 	}else{
	 		$where = "(vendor_type='avl' OR vendor.vendor_type='shortlist')";
	 	}	 		

	   	$joins[0][0] = 'vendor';
	   	$joins[0][1] = 'vendor.id = vendor_category.id_vendor';
	   	$joins[0][2] = 'right';
	   	$joins[1][0] = 'subcategory';
	   	$joins[1][1] = 'vendor_category.id_subcat = subcategory.id';
	   	$joins[1][2] = 'left';	   		

	   	$this->db->distinct();
	   	$vendor = $this->crud->browse_join("","vendor_category","","","true",$select,$joins,$where,$order);
	   		
	   	$element = '';
	   	if (count($vendor) > 0) {
	   		$subcat 	  = $vendor[0]->id_subcat;
	   		$group_subcat = $vendor[0]->subcategory;
	   		$optg_open 	  = "<optgroup label='".$group_subcat."'>";
	   		$optg_close   = "";
	   		$incr 		  = 1;
	   		$element 	 .= $optg_open;

	   		foreach ($vendor as $row){
		  		if ($subcat != $row->id_subcat) {
		  			$subcat 	  = $row->id_subcat;
	   				$group_subcat = $row->subcategory;
	   				$optg_open  = "<optgroup label='".$group_subcat."'>";
	   				$optg_close = "</optgroup>";
	   			}else{
	   				$optg_open  = "";
	   				$optg_close = "";
	   			}
	   			if (in_array($row->id, $object)) {
	   				$element .= $optg_close.$optg_open."<option selected='selected' data-increment='".$incr."' value='".$row->id."'>".$row->vendor_name."</option>";
	   			}else{
	   				$element .= $optg_close.$optg_open."<option data-increment='".$incr."' value='".$row->id."'>".$row->vendor_name."</option>";
	   			}
	   			$incr++;
	  		}
	   	}else{
	   		$element .= "<li>&nbsp;&nbsp;&nbsp;&nbsp;
	 				--- No vendor(s) available for this category ---
	 			</li>";
	   	}

	   	return $element;
   	}
 
	function set_browse_session(){
		$code     = $_POST['search'];
		$announce = $this->crud->browse("","announce","code",$code,"false","id");
		if($announce) $this->session->set_flashdata('anID',$announce->id);
	}

	function reArrayFiles(&$file_post) {

	    $file_ary = array();
	    $file_count = array_keys($file_post['name']);
	    $file_keys = array_keys($file_post);

	    foreach ($file_count as $value) {
	    	foreach ($file_keys as $key) {
	    	    $file_ary[$value][$key] = $file_post[$key][$value];
	    	}
	    }

	    return $file_ary;
	}

	////// * Utility Methods * //////

	private function insertVendorAsRecipients($lists,$id){
		$array_vendor_select = array_unique($lists);			

	 	foreach ($array_vendor_select as $val_vendor) {
	 		$direct_bidding = array(
						 		'id_announce'	=> $id,
							 	'id_vendor'		=> $val_vendor,
							 	'created_id'	=> $this->tank_auth->get_user_id(),
	 						 	'created_date'	=> date('Y-m-d H:i:s'),
	 						 	'id_subcontent'	=> '2',
	 						 	'status'		=> '82',
	 						 	'last_updated'	=> date('Y-m-d H:i:s'));

			$id_direct_bidding = $this->crud->insert("","announce_join_participant",$direct_bidding);
		}
	}

	private function getOffices(){
	 	$options[""] = "-- Select Office --";
	 	$select2 	 = "id,office_name,(SELECT region_name FROM region r WHERE r.id=l.id_region) as region";

	 	$offices = $this->crud->browse("","offices l","id_region","","true",$select2); 
	 	if(!$offices) $offices = array();
	 	foreach($offices as $val){ $options[$val->id] = $val->office_name." / ".$val->region; } 

	 	$getData = $options;

	 	return $getData;
	}

	private function getAreas(){
	 	$options[""] = "-- Select Area --";
	 	$select2 	 = "id,region_name as name";

	 	$getDatas = $this->crud->browse("","region l","","","true",$select2); 
	 	if(!$getDatas) $getDatas = array();
	 	foreach($getDatas as $val){ $options[$val->id] = $val->name; } 

	 	$getData = $options;	 		

	 	return $getData;
	}

	private function upload_text_into_file($textfield = '',$root = '',$path = '',$filename = '',$extension = ''){
	 	if ($root == '') {
	 		$root = $_SERVER['DOCUMENT_ROOT'];
	 	}
	 	// $pathfixed = $root.SRC_PATH.$path;	 		
	 	$pathfixed = $root.$path;	 		

	 	$myfile = fopen($pathfixed.$filename.".".$extension, "w");
 		fwrite($myfile,$textfield);
	}

	private function upload_image_file($fileresource = '',$path = '',$filename = '',$extension = ''){
 		$_FILES['userfile'] = $fileresource;
 		$array = explode('.', $fileresource['name']);
		$extension = end($array);
 		$config = $this->m_announce->set_config($filename.".".$extension,$path,'jpg|gif|png');
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config);
 		 		 		
 		$msg['error_'.$extension] = "";  $msg['success_'.$extension] = array();		
		if ( ! $this->upload->do_upload()){
			$msg['error_'.$extension] = $this->upload->display_errors();
		} else {
			$msg['success_'.$extension] = $this->upload->data(); }
	}

	private function upload_attachment_file($fileresource = '',$path = '',$filename = '',$extension = ''){
 		$_FILES['userfile'] = $fileresource;
 		$config = $this->m_announce->set_config($filename.".".$extension,$path,'pdf');
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config);
 		 		 		
 		$msg['error_'.$extension] = "";  $msg['success_'.$extension] = array();		
		if ( ! $this->upload->do_upload()){
			$msg['error_'.$extension] = $this->upload->display_errors();
		} else {
			$msg['success_'.$extension] = $this->upload->data(); }
	}

	private function upload_text_file($fileresource = '',$path = '',$filename = '',$extension = ''){
 		$_FILES['userfile'] = $fileresource;
 		$config = $this->m_announce->set_config($filename.".".$extension,$path,'txt');
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config);
 		 		 		
 		$msg['error_'.$extension] = "";  $msg['success_'.$extension] = array();		
		if ( ! $this->upload->do_upload()){
			$msg['error_'.$extension] = $this->upload->display_errors();
		} else {
			$msg['success_'.$extension] = $this->upload->data(); }
	}

	private function clear_cache(){
		header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
	}
 
}
?>