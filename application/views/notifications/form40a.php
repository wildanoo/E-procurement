<?php $this->load->view('notifications/header');?>

<br>
Dengan hormat,
<br>
<b><?php echo $venName;?></b>
<br>
<br>
Dengan ini kami informasikan bahwa perusahaan Bapak/Ibu telah berhasil terpilih sebagai peserta pengadaaan di PT. Garuda Indonesia (Persero). Tbk sebagai vendor untuk kategori <b><?php echo $subcategory;?></b>
<br>
<br>
Untuk proses selanjutnya akan kami informasikan agenda aanwijzing pada:
<br>
Waktu		:
<br>
Tempat		:
<br>
<br>
Dokumen RFP dapat diakses pada link berikut ini:
<br>
<b><a href="<?php echo $linkRFP;?>">(link RFP)</a></b>
<br>
<br>
Note: link RFP hanya aktif sampai tanggal ________ 
<br>
<br>
Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih<br>
<br>

<?php $this->load->view('notifications/footer');?>