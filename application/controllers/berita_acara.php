<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Berita_acara extends Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('m_vendor','m_announce','m_berita_acara','m_redlist'));	islogged_in();
		$this->permit = new stdClass();
		$this->permit->cba  = $this->m_vendor->is_permit(array(54)); //cetak-berita-acara:54;
	}

	function index(){
		if($this->permit->cba){
			$id_visit  = $this->uri->segment(3);
			redirect('/berita_acara/generate/' + $id_visit);
		} else {
			$this->session->set_flashdata('message','user not authorized');
			redirect('/auth/login/');	}
	}

	function generate($id_visit=null,$is_saved=null)
		{
			if(empty($id_visit))
			{
				$id_visit = $this->uri->segment(3);
			}
			$venID   = $this->session->userdata("venID");
			$select	 = "vendor_name as name,register_num as reg_num,vendor_type as type,vendor_num as num,vendor_address as address, phone, fax,

						(SELECT status FROM register_status WHERE id=id_stsreg) as reg_sts,id_stsreg,
	 					 postcode,web,phone,fax,email,npwp,npwp_address,npwp_postcode";

	 		$where    = array('id_visit'=> $id_visit);
			$data['vendor'] 	= $this->crud->browse("","vendor l","id",$venID,"false",$select);
	 		$data['participant'] = $this->crud->browse("","dd_participant","id_vendor",$venID,"true","", $where );
	 		$data['notes'] = $this->crud->browse("","dd_notes","id_vendor",$venID,"true","", $where);

			$default          = array('id'=>'','summary'=>'');
	 		$summary          = $this->crud->browse("","dd_summary","id_vendor",$venID,"false","",$where);
	 		$data['summary']  = !$summary ? (object) $default : $summary;

	 		$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
				   (SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";
	 		$data['categories'] = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2);
	 		$data['visit_docs']  = $this->crud->browse("","dd_docs","id_vendor",$venID,"true","",$where);
	 		$data['vendor_visit'] = $this->crud->browse("","vendor_visit","id_vendor",$venID,"false","",array('id'=>$id_visit));

	 		$assessment = $this->m_berita_acara->get_assesment($venID, $id_visit);

			$data['assessment'] = $assessment;
			$data['limit1'] = $this->m_berita_acara->get_data_assesment();
			$data['limit2'] = $this->m_berita_acara->get_limit2($venID,$id_visit);
			$data['is_saved'] = $is_saved;
			$this->load->view('due_diligence/cetak/berita_acara', $data, FALSE);

		}

		function get_subcatnotes($venID,$id_visit,$id_subcat)
		{
			 return $data = $this->db->get_where('subcat_notes', array('id_vendor' => $venID , 'id_visit'=>$id_visit, 'id_subcat'=>$id_subcat ) )->result_array();

		}

		function generate_evaluation($id_vendor, $is_saved=null)
		{
			if(empty($id_vendor))
			{
				$id_vendor = $this->uri->segment(3);
			}

			$venID   = $this->session->userdata("venID");
			if(empty($venID))
			{
				$venID = $id_vendor;
			}
			$select	 = "vendor_name as name,register_num as reg_num,vendor_type as type,vendor_num as num,vendor_address as address, phone, fax,
							(SELECT result FROM vendor_recomend t WHERE t.id_vendor=l.id LIMIT 1) as result,(SELECT note FROM vendor_recomend t WHERE t.id_vendor=l.id LIMIT 1) as result_note,
						(SELECT status FROM register_status WHERE id=id_stsreg) as reg_sts,id_stsreg,
						 postcode,web,phone,fax,email,npwp,npwp_address,npwp_postcode";

			$where    = array('id_visit'=> $id_visit);
			$data['vendor'] 	= $this->crud->browse("","vendor l","id",$venID,"false",$select);
			$data['cp'] = $this->crud->browse("","contact_person","id_vendor",$venID,"true");
			$akta           = $this->crud->browse("","akta","id_vendor",$venID,"false");
			$data['akta']   = !$akta ? (object) $none_akta : $akta;
			$data['owner']    = $this->crud->browse("","owner","id_vendor",$venID,"true");
			$data['pengurus']   = $this->crud->browse("","organization","id_vendor",$venID,"true");
			$data['surleg'] = $this->crud->browse("","correspondence","id_vendor",$venID,"true");
			$data['ref']   = $this->crud->browse("","references","id_vendor",$venID,"true");
			$data['aff']   = $this->crud->browse("","affiliates","id_vendor",$venID,"true");


			$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
					 (SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";
			$data['categories'] = $this->crud->browse("","vendor_recomend l","id_vendor",$venID,"true",$select2);
				$data['is_saved'] = $is_saved;
			$this->load->view('due_diligence/cetak/evaluasi', $data, FALSE);

		}

		function generate_verification()
		{
			$venID = $this->session->userdata('venID');
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			$select	 = "id as vendor_id, vendor_name as name,register_num as reg_num,vendor_type as type,vendor_num as num,vendor_address as address,
			(SELECT status FROM register_status WHERE id=id_stsreg) as reg_sts,approval,
			id_stsreg,web,phone,fax,email,postcode,npwp,npwp_address,npwp_postcode";
			$data['vendor']  = $this->crud->browse("","vendor l","id",$venID,"false",$select);

			$none_bank 		= array('acc_number'	=> "none",
			'acc_name'		=> "none",
			'acc_address'	=> "none",
			'bank_name'		=> "none",
			'branch_name'	=> "none");

			$bank           = $this->crud->browse("","bank_account","id_vendor",$venID,"false");
			$data['bank']   = !$bank ? (object) $none_bank : $bank;
			$this->load->view('due_diligence/cetak/verification', $data, FALSE);
		}

		function download_batch_berita_acara()
		{
			$path  = 'due_diligence_attachment/';

			$venID = $this->session->userdata('venID');
			$vendor_name = $this->db->get_where('vendor', array('id'=>$venID) )->row()->vendor_name;
			$data = $this->db->get_where('vendor_visit', array('id_vendor'=>$venID))->result();
			// print_r($data);
			if(empty($data))
			{
				echo "<script>window.close()</script>";
				exit();
			}
			$this->generate_evaluation($venID,'true');
			$data2 = array();
			foreach ($data as $key => $value)
			{
				$this->generate($value->id, 1);
				$data2[] = str_replace(array(' ', ':'), '_', $path.'lampiran_berita_acara_'.$vendor_name.'_'.$value->visit_date.$value->visit_time.'.pdf');
			}

			$data2[] = str_replace(array(' ', ':'), '_', $path.'berita_acara_evaluasi_'.$vendor_name.'.pdf');

			$filename = 'lampiran_due_diligence_'.str_replace(' ', '', $vendor_name).'.zip';
			if(file_exists($path.$filename))
			{
				unlink($path.$filename);
			}

			$this->m_redlist->create_zip($data2,$path.$filename);

			array_map('unlink', glob("$path/*.pdf"));

	 		$this->load->helper('download');
	 		$data3 = file_get_contents($path.$filename);
			force_download($filename, $data3);
		}


}
