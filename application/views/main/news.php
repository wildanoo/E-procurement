<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<title>E-Procurement Garuda</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/skins/red-blue/blue.css" id="colors" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/layerslider.css"/> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/component.css" />
	<!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/iCheck/square/blues.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reset.css">
	<link rel="icon" href="<?php echo base_url(); ?>assets/favicon-grd.png" sizes="16x16">
</head>
<body>
<div id="wrapper">
	<header id="header" class="container-fluid">
	  <div class="header-bg">
		<div class="container">
			<div class="decoration dec-left visible-desktop"></div>
			<div class="decoration dec-right visible-desktop"></div>	  
			<div class="row">
				<div id="logo" class="span5"><a href="index.html" class="logo" alt="E-Procurement Garuda Indonesia"></a></div>
				<div class="span7">
					
					<ul class="social-icons">
						<li class="eng" title="English"><a href="#">English</a></li>
						<li class="idn" title="Indonesian"><a href="#">Indonesian</a></li>
					</ul>				
				</div>	
			</div>

			<div class="row">
				<div class="span12" style="font-size: 13px;">
					<div class="select-menu hidden-desktop">
						<select id="selectMenu">
							<option selected value="<?php echo base_url(); ?>auth/login">Home</option>			
							<option value="<?php echo base_url(); ?>main/news">News</option>						
							<option value="#">FAQs</option>
							<option value="<?php echo base_url(); ?>main/contact_us">Contact us</option>
						</select>
					</div>
					<ul id="menu" class="visible-desktop">
						<li><?php  echo anchor('auth/login','HOME'); ?></li>
						<li><?php  echo anchor('main/news','NEWS'); ?></li>
						<li><a href="#">FAQs</a></li>
						<li><a href="<?php echo base_url(); ?>main/contact_us">CONTACT US</a></li>
						<!-- Search Form -->
						<li class="search-form visible-dekstop" style="float: right;top: 10px;">
							<form method="get" action="#">
								<input type="text" value="Search" class="search-text-box"/>
								<input type="submit" value="" class="search-text-submit"/>
							</form>
						</li>
					</ul>
				</div>
			</div>
			<div class="row visible-desktop">
				<div class="span12 main-slider"><!-- SLIDER --></div>
			</div>
		</div>
	  </div>
	</header>
	<section id="content" class="container-fluid">
	<?php if($archived){ ?>
	<div class="container">
		<!-- <div id="headline-page">
			<h1 style="margin-top:12px;">PROCUREMENT POLICY</h1>
			<div id="crumbs"><a href="index.html">Home</a> / <a href="#" class="active">News</a></div> 
		</div> -->
		<div class="row">
			<div class="span8">
				<div class="post">
					<div class="title"><h2><?php echo $read->title; ?></h2></div>
					<div class="blog-line">
						<a href="#" class="date item"><?php echo date('d M, Y',strtotime($read->created_date));	?></a>
						<a href="#" class="category item">Category: <?php echo $category[$read->type];?></a>
					</div>
					<div class="thumbnail">
						<div id="myCarousel" class="carousel slide">
							<div class="carousel-inner">						
							<?php 
							$i=1; 							
							foreach($archived as $val){
								$imgfile   = "null.jpg";
								$images    = array('jpg','gif','png'); 
						 		foreach($images as $ext){
						 			$checkpath  = $imgpath.$val->file.".".$ext;	
									if(file_exists($checkpath)){ 										
										$imgfile = $val->file.".".$ext; break;	
									}
								}								
								$status = ($read->file==$val->file) ? "item active" : "item"; ?>															
								<div class="<?php echo $status; ?>">							
									<img alt="" src=<?php echo $srcpath.$imgfile; ?>>
									<div class="carousel-caption visible-desktop">
										<p><?php echo ucfirst($val->title); ?></p>
									</div>
								</div>																
							 <?php $i++; }  ?>								
								<!--div class="item">
									<img alt="" src="<?php echo base_url(); ?>assets/images/advertising_medium.jpg" >
									<div class="carousel-caption visible-desktop">
										<p>&nbsp;</p>
									</div>
								</div>
								
								<div class="item">
									<img alt="" src="<?php echo base_url(); ?>assets/images/advertising_medium.jpg" >
									<div class="carousel-caption visible-desktop">
										<p>&nbsp;</p>
									</div>
								</div-->
								
							</div>
						<a class="left carousel-control hidden-phone" href="#myCarousel" data-slide="prev">&lsaquo;</a>
						<a class="right carousel-control hidden-phone" href="#myCarousel" data-slide="next">&rsaquo;</a>						
						</div>
					</div>
					<p style="text-align:justify;">
					<?php if($read){  
							$path     = "./uploads/contents/".$read->file.".txt";			
							$contents = file_exists($path) ? file_get_contents($path) : "Content Not Exist";
							echo $contents; } 
					?></p>
				</div>			
			</div>
			<div class="span4">
				<div class="headline"><h4>Archive</h4></div>
				<ul class="link-list" style="list-style: none;"> 
					<li><a href="#">2016</a></li>
					<li><a href="#">2015</a></li>
					<li><a href="#">2014</a></li>
				</ul>
				<!-- <div class="headline"><h4>News</h4></div>
				<p style="text-align:justify;padding-bottom: 15px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent elementum augue ultrices elit posuere commodo. Donec a scelerisque lorem. Fusce pharetra sagittis tempus. In faucibus turpis et risus laoreet posuere. Vivamus in metus metus, sed adipiscing magna.</p> -->
				<div class="headline"><h4>Recent Posts</h4></div>
				<ul class="post-list" style="list-style: none;">
					<li style="text-align:justify;">
						<a href="#" class="headline">Registration Client and Procurement Policy in the Environment PT Garuda Indonesia (Persero) Tbk.</a>
						<time datetime="2012-09-01">02/20/2012</time>
					</li>
					<li style="text-align:justify;">
						<a href="#" class="headline">Implementation of e-Auction process Contractor Electrical Work Building Development Management GITC</a>
						<time datetime="2012-09-01">02/10/2012</time>
					</li>
					<li style="text-align:justify;">
						<a href="#" class="headline">Implementation of e-Auction process Civil Works Architecture Building Construction Management GITC</a>
						<time datetime="2012-09-01">02/02/2012</time>
					</li>
				</ul>			
			</div>
		</div>
	</div>
	<?php } ?>
	</section>
</div>
<footer class="main-footer footer-line container-fluid text-center">
	Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
</footer>
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.color.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/SmoothScroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<script src="<?php echo base_url(); ?>assets/js/classie.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sidebarEffects.js"></script>
<script src="<?php echo base_url(); ?>assets/js/index.js"></script>

</body>
</html>