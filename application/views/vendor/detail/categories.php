<div id="container2">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
   var token   = '<?php echo $this->security->get_csrf_hash(); ?>';	
   
   $('#btn_create2').click(function(){   		
   		  $.ajax({
               url  : baseUrl + 'register/validate_category',
               type : "POST",              		               
               data : csrf+'='+token,
               success: function(resp){
               	  if(resp=="true"){
				  		$("#container2").load(baseUrl+'vendor/form_categories');   	
				  } else {  $("#msg2").html('Sub category required ( 2 category @4 sub category )'); }
               }
            });
   });             	
   
 }); 
 
 function update_cat(id){
 	
 	$("#container2").load(baseUrl+'vendor/form_update_category/'+id); 
 }
 
 function delete_cat(id){
 	var baseUrl = "<?php echo base_url(); ?>";
 	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
 	
 	if (confirm('Are you sure you want to delete vendor category?')) {	
		$.post( baseUrl + "vendor/delete_subcategory", { csrf: token, id: id } );
		$("#container2").load(baseUrl+'vendor/load_categories');		
	}
	
 } 
  
 </script>
 
 <div id="debug2"></div>
 <div id="msg2"></div>
 
 <div style="text-align: right">
 <?php if($this->global->active){ ?>
<button type="button" id="btn_create2" class="btn btn-info btn-primary">Add New</button> <?php } ?>
</div>
<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Category</th>
		<th>Sub Category</th>		
		<?php if($this->global->active){ ?><th>Action</th><?php } ?>
	</tr>
	</thead>
	<tbody>
 <?php  
 if($categories){ $i=1;
 	foreach($categories as $row){ ?>
 		<tr>
 			<td style="text-align: center"><?php echo $i; ?></td>
			<td><?php echo $row->category; ?></td>	
			<td><?php echo $row->subcategory; ?></td>
			<?php if($this->global->active){ ?>			
			<td style="text-align: center;"><?php
					echo '<i class="fa fa-pencil" onclick="update_cat('.$row->id.')"></i>'; echo '&nbsp;&nbsp;&nbsp;';					
					echo '<i class="fa fa-trash"  onclick="delete_cat('.$row->id.')"></i>';	
			?></td>	
			<?php } ?>										
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="4">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>