<html lang="en-US">
<head>
  <title>E-Procurement Garuda</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets_/style.css">
  <link rel="icon" href="<?php echo base_url(); ?>assets/favicon-grd.png" sizes="16x16">
</head>
<body class="hold-transition login-bgcolor">  
  <div class="row content-afterregis">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="col-md-3"></div>
      <div class="col-md-6 box box-info" id = "white-header">
        <div class="box-header with-border">
          <h2 class="box-title col-left" style="margin-top: 10px;"><strong style="color: #368ccc;font-size:20px;">Success</strong></h2>
        </div><!-- /.box-header -->
        <!-- form start -->
        <hr style="margin-top: 0;">
        <form class="form-horizontal">
          <div class="box-body">
            <div class = "col-md-12 col-left">
              <p class = "text-justify">Thank you for registering. <br><br>
                You have successfully registered. To check your registration status, please keep your registration number below for your records.
              </p>
              <br>
              <p>Registration number: <strong style="color:#368ccc;font-size:23px;"><?php echo $registration_number;?></strong></p>
              
            </div>

          </div>
          <div class="box-footer pad-footer">
            <!-- Trigger the modal with a button -->
            <!--button type="button" class="btn-primary btn-customs vcenter" style="margin-left:15px;">Check status</button-->
            <?php  $prop = array('class'=>'btn-primary btn-customs vcenter','style'=>'margin-left:15px;'); ?>					 
			<?php  echo anchor('register/set_default_vendor/'.$registration_number,'Check status',$prop); ?>
            
            
            
            <span class="col-md-1 text-center vcenter"> or </span>
            <a class="col-md-6 row vcenter" id="btn_cancel" href="<?php echo base_url();?>">back to home</a>
          </div><!-- /.box-footer -->
        </div>
        <div class = "col-md-3"></div>

      </div><!-- /.box-body -->
    </div>
    <footer class="main-footer footer-line container-fluid text-center">
      Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
    </footer>
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.color.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-easing-1.3.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/SmoothScroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/classie.js"></script>

    <!-- jangan lupa dibuka lagi -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarEffects.js"></script>
    <!-- jangan lupa dibuka lagi -->

    <script src="<?php echo base_url(); ?>assets/js/index.js"></script>

  </body>
  </html>