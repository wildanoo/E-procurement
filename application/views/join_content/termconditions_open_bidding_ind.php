<script type="text/javascript"> 
    $(function(){

        $("input[type=file]").change(function(){
            var filename = $(this).val() ;
            filex = filename.split(".");
            tipe = filex[filex.length-1] ;

            if(tipe!='pdf') { $(this).val(""); alert('Harus file pdf'); }
        });
        
    }); 
</script>

<div class="about-content-wrap pull-left" id="#">
    <div class="head" style="margin-top: 25px;">
        <h1 style="color:#fff;font-size: 35px;font-family: Roboto;font-weight: 300;text-transform: uppercase;">Verifikasi</h1>
        <a href="#" class="next-section about animate2">
            <i class="fa fa-angle-double-down"></i>
        </a>
    </div>
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 content-tabs-wrap wow fadeInUp animated" data-wow-delay="1.7s" data-wow-offset="200">
            <div class="col-xs-12 col-sm-12 col-md-12 content-tabs no-pad">            
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#" data-toggle="tab"> Open Bidding </a></li>
                  <li><a>&nbsp;</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-home">
                        <div class = "row">
                            <div class="col-md-12 tab-content">
                                
                                <div class="box2 box-info2 tab-pane fade in active" id="registrasi-step-1">
                                    <div class="row background-terms">
                                        <div class="appointment-form-title"><i class="icon-hospital2 appointment-form-icon"></i>Syarat dan Ketentuan</div>
                                        <div class="terms-custom">
                                            <div class="form-group">
                                                <?php echo form_open_multipart('joinbiddingsourcing/update_vendor_from_open_bidding_announcement_bylogin',array('id'=>'form')); ?>
                                                <input type="hidden" name="unikform" value="<?php echo $unikform ;?>" />
                                                Kebijakan Privasi ini mengatur ketentuan Garuda Indonesia menampung, menggunakan, memelihara dan menangkap informasi yang dikumpulkan dari pengguna (semua pengguna) dari https://www.eproc.garuda-indonesia.com/ . Kebijakan privasi ini berlaku untuk semua produk dan Jasa layanan yang ditawarkan oleh Garuda Indonesia
                                                <br>
                                                <br>
                                                <label>
                                                <input type="checkbox" name="i_accept" id="i_accept" style="margin-right:10px;">Saya menerima syarat dan ketentuan yang berlaku
                                                </label>
                                                <br/>
                                                <br>
                                                <ul class="list-inline">
                                                    
                                                    <li style="width:155px">Lampirkan LOI <i>(*.pdf)</i></li>
                                                    <li><input type="file" name="file_loi" id="file_loi" style="margin-right:10px;"></li>
                                                    
                                                </ul>
                                                <br>
                                                <br>
                                                </span>
                                                <?php echo form_close(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-left" style="margin-top:10px;">
                                        <a type="button" class="btn button-custom-back" href="<?php echo base_url();?>">Batal</a>
                                        <button type="button" class="btn button-custom" id="btnsubmit" href="#">Daftarkan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        </div>
    </div>
</div>