<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('m_vendor','m_announce'));	
		islogged_in();
    }
    
 function index(){
 	if ($this->m_vendor->authenticate()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$venID = $this->session->flashdata('id_vendor'); 
		$field = !$venID ? "" : "id";
		$ID    = !$venID ? "" : $venID;
		
		
		$sess_sts = $this->session->userdata('sess_sts');		
		if($venID) $where =  array($field=>$ID,'id_stsreg'=>$sess_sts);
		else $where = array('id_stsreg'=>$sess_sts);
		
		if($sess_sts=="0") unset($where['id_stsreg']);

 		$table 	    = "vendor";		
		$page       = $this->uri->segment(3);
		$per_page   = 10;		
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table,$where);
		$set_config = array('base_url'=>base_url().'vendor/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);		
			 
		$this->load->library('pagination');			 
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();			
			
		$order 			     = array('field'=>'created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;		
		$select          	 = " id,reg_num,vendor_num,vendor_name,vendor_type,id_stsreg,							
								(SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as reg_sts,
								created_date,last_updated";		
		
		if($venID){ $browse = $this->crud->browse("",$table." l",$field,$ID,"true",$select);	
		} else { $browse = $this->crud->browse_with_paging("",$table." l",$field,$ID,"true",$select,$where,$order,$config['per_page'],$offset); }

		$dbstatus = $this->crud->browse("","register_status","","","false","id,status");
		foreach($dbstatus as $val){ $status[$val->id] = ucwords($val->status); }

		$data['status'] = $status; 
		$data['browse'] = $browse;		
		$data['view']	= "vendor/browse"; 		
 		$this->load->view('layout/template',$data); 
 		
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/'); }
 	
 }
 
 function get_tags(){ 	
 	$term   = $_GET['term'];  	
 	$order  = array('field'=>'vendor_name','sort'=>'ASC'); $select = "reg_num as name";
 	$result = $this->crud->autocomplete("","vendor",$select,"reg_num",$term,"",$order);  	
 	echo json_encode($result);   	 		
 } 
 
 function set_browse_session(){
 	
 	$code   = $_POST['search'];	
 	$vendor = $this->crud->browse("","vendor","reg_num",$code,"false","id");
 	if($vendor) $this->session->set_flashdata('id_vendor',$vendor->id); 
 	
 }
 
 function set_browse_status(){ 	
 
 	$id_sts = $_POST['id_sts'];
 	if($id_sts == "0") $this->session->unset_userdata('sess_sts');
 	else $this->session->set_userdata('sess_sts',$id_sts);
 	
 	$sess_sts = $this->session->userdata('sess_sts');
 	
 	echo $sess_sts;
 }

 function set_sess_vendor(){
 	$id = $this->uri->segment(3);
 	$this->session->set_userdata("venID",$id);
 	redirect("vendor/detail");
 	
 }
 
 function review_category(){
 	$id = $this->uri->segment(3);
 	$data['user_id']	= $this->tank_auth->get_user_id();
 	$data['username']	= $this->tank_auth->get_username();
 	$select				= "vendor_name, vendor_address,
 						  (SELECT category FROM category WHERE id=id_category) as category,
						  (SELECT subcategory FROM subcategory WHERE id=id_subcat) as subcategory,
 						  contact_person,phone,fax,email,npwp";
 	//browse($db="",$table,$field="",$id="",$multi="",$select="",$where="",$order="",$group_by="",$like="")
 	$data['data'] = $this->crud->browse("","vendor","id",$id,"false",$select);
 	$this->load->view('vendor/detail/category',$data);
 }

 function detail(){
 	
 	if ($this->m_vendor->authenticate()) { 	
 		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$venID   = $this->session->userdata("venID");	
		$select	 = "vendor_name as name,vendor_type as type,vendor_address as address,
 					(SELECT category FROM category WHERE id=id_category) as category,
					(SELECT subcategory FROM subcategory WHERE id=id_subcat) as subcategory,
					(SELECT status FROM vendor_register WHERE reg_num=l.reg_num) as reg_sts,
					(SELECT status FROM register_status WHERE id=id_stsreg) as reg_sts,id_stsreg,
 					postcode,web,contact_person,phone,fax,email,npwp,npwp_address,npwp_postcode";
 					
	 	  $none_bank 		= array('acc_number'	=> "none",
								 	'acc_name'		=> "none",
								 	'acc_address'	=> "none",
								 	'bank_name'		=> "none",
								 	'branch_name'	=> "none");
								 	
		 $none_akta 		= array('notaris_name'		=> "none",
								 	'notaris_address'	=> "none",
								 	'notaris_phone'		=> "none",
								 	'notaris_number'	=> "none",
								 	'notaris_date'		=> date('Y-m-d'),
								 	'remark'			=> "none");							 	
							 	
		$owner          = $this->crud->browse("","owner","id_vendor",$venID,"true");					 	
 		$bank           = $this->crud->browse("","bank_account","id_vendor",$venID,"false");
 		$akta           = $this->crud->browse("","akta","id_vendor",$venID,"false");
 		$data['bank']   = !$bank ? (object) $none_bank : $bank;		
 		$data['akta']   = !$akta ? (object) $none_akta : $akta;
 		$data['owner']  = $owner; 		
 		
 		$data['correspondence'] = $this->crud->browse("","correspondence","id_vendor",$venID,"true");	
 		$data['org']   = $this->crud->browse("","organization","id_vendor",$venID,"true");
 		$data['ref']   = $this->crud->browse("","references","id_vendor",$venID,"true");
 		$data['aff']   = $this->crud->browse("","affiliates","id_vendor",$venID,"true");
 		
 		$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
					(SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";					
 		$data['categories'] = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2); 		
 		$data['vendor'] = $this->crud->browse("","vendor l","id",$venID,"false",$select);		
		$data['browse'] = $this->crud->browse("","contact_person","id_vendor",$venID,"true");		
		$data['status'] = $this->crud->browse("","register_status","","","true","id,status");
						
		$data['view']	= "vendor/tab"; 		
 		$this->load->view('layout/template',$data); 
	
	} else { 
		$this->session->set_flashdata('message','user not authorized'); 
		redirect('/auth/login/'); 
	}	
 	
 }
 
 function change_register_status(){
 	
 	$venID   = $this->session->userdata("venID");	
	//$vendor  = $this->crud->browse("","vendor","id",$venID,"false","reg_num");
	$opt     = $_POST['opt'];
	$update  = array('id_stsreg'=>$opt, 'last_updated'=>date('Y-m-d H:i:s'));
	if($opt <= 4 && $opt!=0) $this->crud->update("","vendor","id",$venID,$update); 	
 	
 }
 
 function load_status_info(){
 	
 		$venID   = $this->session->userdata("venID");	
		$select	 = "vendor_name as name,vendor_type as type,vendor_address as address,
 					(SELECT category FROM category WHERE id=id_category) as category,
					(SELECT subcategory FROM subcategory WHERE id=id_subcat) as subcategory,id_stsreg,
					
					(SELECT status FROM register_status r WHERE r.id=l.id_stsreg) as reg_sts,
 					postcode,web,contact_person,phone,fax,email,npwp,npwp_address,npwp_postcode";
 					
 	$data['vendor'] = $this->crud->browse("","vendor l","id",$venID,"false",$select); 	
 	$data['status'] = $this->crud->browse("","register_status","","","true","id,status");
 	$this->load->view('vendor/register_status2',$data);
 }

 function general_update(){
  	
  	$venID = $this->session->userdata("venID");	
  	$post = array(	'vendor_name'		=> $_POST['vendor_name'],
					'vendor_address'	=> $_POST['vendor_address'],	
					'postcode'			=> $_POST['vendor_postcode'],
					'npwp'				=> $_POST['npwp'],
					'npwp_address'		=> $_POST['npwp_address'],
					'npwp_postcode'		=> $_POST['npwp_postcode'],
					'phone'				=> $_POST['phone'],
					'fax'				=> $_POST['fax'],
					'email'				=> $_POST['email'],								
					'web'			    => $_POST['web'],
					'last_updated'	    => date('Y-m-d H:i:s'));
					
	$this->crud->update("","vendor","id",$venID,$post); 					
 } 
 
 function load_general(){
 	
 		$venID   = $this->session->userdata("venID");	
		$select	 = "vendor_name as name,vendor_type as type,vendor_address as address,
 					(SELECT category FROM category WHERE id=id_category) as category,
					(SELECT subcategory FROM subcategory WHERE id=id_subcat) as subcategory,
 					postcode,web,contact_person,phone,fax,email,npwp,npwp_address,npwp_postcode";
 					
		$data['vendor'] = $this->crud->browse("","vendor","id",$venID,"false",$select);	
		$data['browse'] = $this->crud->browse("","contact_person","id_vendor",$venID,"true");	
		$this->load->view('vendor/detail/general',$data); 
 }
 
 function load_bank_account(){ 	
 		$venID   = $this->session->userdata("venID");	
		$data['bank'] = $this->crud->browse("","bank_account","id_vendor",$venID,"false");	
		$this->load->view('vendor/detail/bank_account',$data); 
 }
 
 function load_akta(){ 	
 		$venID   = $this->session->userdata("venID"); 		
 		$none_akta 		= array('notaris_name'		=> "none",
							 	'notaris_address'	=> "none",
							 	'notaris_phone'		=> "none",
							 	'notaris_number'	=> "none",
							 	'notaris_date'		=> date('Y-m-d'),
							 	'remark'			=> "none");
 		
 		$akta  = $this->crud->browse("","akta","id_vendor",$venID,"false");	
 		$data['akta']   = !$akta ? (object) $none_akta : $akta;	 	
 			
		
		$this->load->view('vendor/detail/akta',$data); 
 }

 function form_create(){
 	$venID  = $this->session->userdata("venID");
 	$data['vendor'] = $this->crud->browse("","vendor","id",$venID,"false","vendor_name as name");		
 	$this->load->view('vendor/detail/general/form_create',$data);
 }
 
 function form_update(){
 	$contactID  = $this->uri->segment(3);
 	$venID      = $this->session->userdata("venID");
 	
 	$data['default'] = $this->crud->browse("","contact_person","id",$contactID,"false"); 	
 	$data['vendor'] = $this->crud->browse("","vendor","id",$venID,"false","vendor_name as name");		
 	$this->load->view('vendor/detail/general/form_update',$data);
 }
 
 function add_contact(){	

 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('id'			=> null, 						
					 	'id_vendor'		=> $venID,
					 	'fullname'		=> $_POST['name'],
					 	'position'		=> $_POST['position'],
					 	'mobile'		=> $_POST['mobile'],
					 	'email'			=> $_POST['email'],
					 	'status'		=> '1',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date);
					 	
	$this->crud->insert("","contact_person",$data); 					 	
 }
 
 function update_contact(){	

 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('fullname'		=> $_POST['name'],
					 	'position'		=> $_POST['position'],
					 	'mobile'		=> $_POST['mobile'],
					 	'email'			=> $_POST['email'],					 					 	
					 	'last_updated'	=> $curr_date);	
					 				
	$this->crud->update("","contact_person","id",$_POST['id'],$data); 				 	
					 	
 }
 
 function delete_contact(){ 
 	
 	$this->crud->delete("","contact_person","id",$_POST['id']); 	
 	
 }

 function bank_update(){
 	$venID      = $this->session->userdata("venID");
 	$where      = array('id_vendor'=>$venID);
 	$is_exist   = $this->crud->is_exist("","bank_account","id",$where); 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('id_vendor'		=> $venID,
 						'acc_number'	=> $_POST['acc_number'],
					 	'acc_name'		=> $_POST['acc_name'],
					 	'acc_address'	=> $_POST['acc_address'],
					 	'bank_name'		=> $_POST['bank_name'],
					 	'branch_name'	=> $_POST['branch_name'],					 	
					 	'created_id'	=> $this->tank_auth->get_user_id(),		
					 	'created_date'  => $curr_date,							 					 	
					 	'last_updated'	=> $curr_date);	
					 	
	if($is_exist){
		unset($data['id_vendor']); unset($data['created_id']); unset($data['created_date']);
		$this->crud->update("","bank_account","id_vendor",$venID,$data); 
	} else { $this->crud->insert("","bank_account",$data); }		 	
 	
 }

 function akta_update(){
 	
 	$venID      = $this->session->userdata("venID");
 	$where      = array('id_vendor'=>$venID);
 	$is_exist   = $this->crud->is_exist("","akta","id",$where); 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$data 		= array('id_vendor'		    => $venID,
 						'notaris_name'		=> $_POST['notaris_name'],
					 	'notaris_address'	=> $_POST['notaris_address'],
					 	'notaris_phone'		=> $_POST['notaris_phone'],
					 	'notaris_number'	=> $_POST['notaris_number'],
					 	'notaris_date'		=> $_POST['notaris_date'],
					 	'remark'			=> $_POST['remark'],					 	
					 	'created_id'		=> $this->tank_auth->get_user_id(),		
					 	'created_date'  	=> $curr_date,							 					 	
					 	'last_updated'		=> $curr_date);	
					 	
	if($is_exist){ 
		unset($data['id_vendor']); unset($data['created_id']); unset($data['created_date']);
		$this->crud->update("","akta","id_vendor",$venID,$data); 
	} else { $this->crud->insert("","akta",$data); }
 	
 }

 function form_owner(){
 	
 	$this->load->view('vendor/detail/owner/form_create'); 
 }
 
 function load_owner(){
 	
 	$venID   = $this->session->userdata("venID");	
	$data['owner'] = $this->crud->browse("","owner","id_vendor",$venID,"true");		
	$this->load->view('vendor/detail/owner2',$data); 
 	
 	
 }
 
 function add_owner(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('id'			=> null, 						
					 	'id_vendor'		=> $venID,
					 	'owner_name'	=> $_POST['owner_name'],
					 	'owner_address'	=> $_POST['owner_address'],
					 	'owner_phone'	=> $_POST['owner_phone'],
					 	'owner_position'=> $_POST['owner_position'],
					 	'owner_shared'	=> $_POST['owner_shared'],
					 	'remark'		=> $_POST['remark'],
					 	'status'		=> '1',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date);					 	
					 	
	$this->crud->insert("","owner",$data); 		
 	
 }

 function form_update_owner(){
 	
 	$prsnID  = $this->uri->segment(3);
 	$venID   = $this->session->userdata("venID"); 	
 	$data['default'] = $this->crud->browse("","owner","id",$prsnID,"false"); 	 	

 	$this->load->view('vendor/detail/owner/form_update',$data);
 	
 }
 
 function update_owner(){
 	
 	$venID      = $this->session->userdata("venID"); 	
 	$curr_date 	= date('Y-m-d H:i:s'); 	
 	
 	$data 		= array('owner_name'	=> $_POST['owner_name'],
					 	'owner_address'	=> $_POST['owner_address'],
					 	'owner_phone'	=> $_POST['owner_phone'],
					 	'owner_position'=> $_POST['owner_position'],
					 	'owner_shared'	=> $_POST['owner_shared'],
					 	'remark'		=> $_POST['remark'],						 							 					 	
					 	'last_updated'	=> $curr_date);
					 	
	$this->crud->update("","owner","id",$_POST['id'],$data); 
 	
 }

 function delete_owner(){
 	
 	$this->crud->delete("","owner","id",$_POST['id']); 
 }

 function owner_update(){
 	
 	$venID      = $this->session->userdata("venID");
 	$where      = array('id_vendor'=>$venID);
 	$is_exist   = $this->crud->is_exist("","owner","id",$where); 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
	
 	$data 		= array('id_vendor'		=> $venID,
 						'owner_name'	=> $_POST['owner_name'],
					 	'owner_address'	=> $_POST['owner_address'],
					 	'owner_phone'	=> $_POST['owner_phone'],
					 	'owner_position'=> $_POST['owner_position'],
					 	'owner_shared'	=> $_POST['owner_shared'],
					 	'remark'		=> $_POST['remark4'],	
					 	'status'		=> "1",				 	
					 	'created_id'	=> $this->tank_auth->get_user_id(),		
					 	'created_date'  => $curr_date,							 					 	
					 	'last_updated'	=> $curr_date);	
					 	
	if($is_exist){
		unset($data['id_vendor']); unset($data['created_id']); unset($data['created_date']);
		$this->crud->update("","owner","id_vendor",$venID,$data); 
	} else { $this->crud->insert("","owner",$data); }
 	
 }

 //---------------------------------------------------

 function form_correspondence(){  	
 	$this->load->view('vendor/detail/correspondence/form_create'); 	
 } 
 
 function form_update_correspondence(){ 
  	
  	$id = $this->uri->segment(3);
  	$data['default'] = $this->crud->browse("","correspondence","id",$id,"false");	  	
  	
 	$this->load->view('vendor/detail/correspondence/form_update',$data); 	
 } 
 
 function add_document(){ 	
 	
 	$venID      = $this->session->userdata("venID");
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$file_type  = "-";
 	
 	if($_FILES){
	$file_type = $this->m_vendor->get_file_type($_FILES); }
    
	 $insert = array( 	'id_vendor' => $venID,
						'doc_name'  => $_POST['doc_name'],
						'doc_num'   => $_POST['doc_num'],
						'desc'      => $_POST['desc'],
						'eff_date'  => $_POST['eff_date'],
						'exp_date'  => $_POST['exp_date'],
						'remark'    => $_POST['remark'],
						'filetype'  => $file_type,
						'status'    => "1",
						'created_id'   => $userID,
						'created_date' => $curr_date,
						'last_updated' => $curr_date);
						
	 $where1    = array('doc_num'=>$_POST['doc_num'],'id_vendor' => $venID);
	 $where2    = array('doc_num'=>$_POST['doc_name'],'id_vendor' => $venID);
	 	 
 	 $is_exist1 = $this->crud->is_exist("","correspondence","id",$where1);	 
 	 $is_exist2 = $this->crud->is_exist("","correspondence","id",$where2);  
     $docID     = $this->crud->insert("","correspondence",$insert);	
 	
 	if($_FILES && $docID && !$is_exist1 && !$is_exist2){ 	
 	     	    
 	    $where3    = array('type'=>$file_type);
 	    $is_exist3 = $this->crud->is_exist("","file_type","id",$where3); 	    
 	    if(!$is_exist3) $this->crud->insert("","file_type",array('type'=>$file_type)); 	    
 	    $ftype = $this->crud->browse("","file_type","","","true","type"); 	    
 	    foreach($ftype as $val){ $type[] =  $val->type;	}
 	    $type = implode("|",$type);
 	 	 		
 		$filename =	$docID.".".$file_type;			
 		$path 	  = './uploads/documents/'; 		
 		$config   = $this->m_announce->set_config($filename,$path,$type);
 		
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config); 
 		 		 		
 		$msg['error']     = array();
 		$msg['success']   = array();		
		if ( ! $this->upload->do_upload()){
		$msg['error']   = $this->upload->display_errors();
		} else {
		$msg['success'] = $this->upload->data(); }			
 	}
 	
 	$this->session->set_userdata('tab','menu5');
 	redirect("vendor/detail");
 }
 
 function load_correspondence(){ 	
 	$venID   = $this->session->userdata("venID");	
	$data['correspondence'] = $this->crud->browse("","correspondence","id_vendor",$venID,"true");		
	$this->load->view('vendor/detail/correspondence',$data); 
 }

 function update_document(){
 	
 	$docID      = $_POST['docID'];
 	$doc_name   = $_POST['doc_name'];
	$doc_num    = $_POST['doc_num']; 
	$attcfile   = $_POST['attcfile'];
	$file_type  = "-";	
	$upload_file = $_FILES['userfile']['name'];
 	
 	if($upload_file && $attcfile){  	    	
 		$file_type = $this->m_vendor->get_file_type($_FILES); 	
	 	$path 	   = "./uploads/documents/".$attcfile; 		
		if (file_exists($path)) unlink($path); 
	}
	 $update = array( 	'doc_name'  => $_POST['doc_name'],
						'doc_num'   => $_POST['doc_num'],
						'desc'      => $_POST['desc'],
						'eff_date'  => $_POST['eff_date'],
						'exp_date'  => $_POST['exp_date'],
						'remark'    => $_POST['remark'],
						'filetype'  => $file_type,
						'status'    => "1",						
						'last_updated' => date('Y-m-d H:i:s'));	
						
	 if($file_type=="-") unset($update['filetype']);				 
 	 
 	 $is_exist  = $this->m_vendor->validate_upd_vendor($_POST); 	 
     if(!$is_exist){ 
     	$this->crud->update("","correspondence","id",$docID,$update); }	
 	
 	if($upload_file && !$is_exist){ 	
 	    
 	    $where3    = array('type'=>$file_type);
 	    $is_exist3 = $this->crud->is_exist("","file_type","id",$where3); 	    
 	    if(!$is_exist3) $this->crud->insert("","file_type",array('type'=>$file_type)); 	    
 	    $ftype = $this->crud->browse("","file_type","","","true","type"); 	    
 	    foreach($ftype as $val){ $type[] =  $val->type;	}
 	    $type = implode("|",$type);
 	 	 		
 		$filename =	$docID.".".$file_type;			
 		$path 	  = './uploads/documents/'; 		
 		$config   = $this->m_announce->set_config($filename,$path,$type);
 		
 		$this->load->library('upload', $config);
 		$this->upload->initialize($config); 
 		 		 		
 		$msg['error']     = array();
 		$msg['success']   = array();		
		if ( ! $this->upload->do_upload()){
		$msg['error']   = $this->upload->display_errors();
		} else {
		$msg['success'] = $this->upload->data(); }			
 	}
 	
 	$this->session->set_userdata('tab','menu5');
 	redirect("vendor/detail");	
 	
 }

 function delete_document(){
 	$docID     = $_POST['id'];
 	$type      = $this->crud->browse("","correspondence","id",$docID,"false","filetype as type")->type;
 	$filename  = $docID.".".$type;
 	$path 	   = "./uploads/documents/".$filename; 		
	if (file_exists($path)) unlink($path); 
 	$this->crud->delete("","correspondence","id",$docID); 
 }

 function form_org(){
 	
 	$this->load->view('vendor/detail/org/form_create'); 
 }
 
 function load_org(){ 
 	
 	$venID   = $this->session->userdata("venID");	
	$data['org'] = $this->crud->browse("","organization","id_vendor",$venID,"true");		
	$this->load->view('vendor/detail/org',$data);  	
 }
 
 function add_person_org(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('id'			=> null, 						
					 	'id_vendor'		=> $venID,
					 	'name'			=> $_POST['prsn_name'],
					 	'address'		=> $_POST['prsn_addrss'],
					 	'phone'			=> $_POST['prsn_phone'],
					 	'position'		=> $_POST['prsn_post'],
					 	'remark'		=> $_POST['remark3'],
					 	'status'		=> '1',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date);
				 	
	$this->crud->insert("","organization",$data); 		
 }

 function form_update_prsn(){
 	
 	$prsnID  = $this->uri->segment(3);
 	$venID      = $this->session->userdata("venID");
 	
 	$data['default'] = $this->crud->browse("","organization","id",$prsnID,"false"); 	 	
 	$data['vendor'] = $this->crud->browse("","vendor","id",$venID,"false","vendor_name as name");		
 	$this->load->view('vendor/detail/org/form_update',$data);
 	
 }
 
 function update_person_org(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$data 		= array('name'		    => $_POST['prsn_name'],
 						'address'		=> $_POST['prsn_addrs'],
 						'phone'		    => $_POST['prsn_phone'],
					 	'position'		=> $_POST['prsn_post'],
					 	'remark'		=> $_POST['remark'],					 					 					 	
					 	'last_updated'	=> $curr_date);	
					 				
	$this->crud->update("","organization","id",$_POST['id'],$data); 
	
 }

 function delete_person(){ 
 	
 	$this->crud->delete("","organization","id",$_POST['id']); 	
 	
 }

 function form_ref(){
 	
 	$this->load->view('vendor/detail/ref/form_create');
 	 
 }
 
 function load_ref(){ 
 	
 	$venID   = $this->session->userdata("venID");	
	$data['ref'] = $this->crud->browse("","references","id_vendor",$venID,"true");		
	$this->load->view('vendor/detail/reference',$data);  	
	
 }

 function add_reference(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('id'			=> null, 						
					 	'id_vendor'		=> $venID,
					 	'cust_name'		=> $_POST['cust_name'],
					 	'project'		=> $_POST['project'],
					 	'point'			=> $_POST['point'],
					 	'date'			=> $_POST['pjdate'],
					 	'remark'		=> $_POST['remark'],
					 	'status'		=> '1',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date);
				 	
	$this->crud->insert("","references",$data); 
	
 }

 function form_update_ref(){
 	
 	$refID     = $this->uri->segment(3); 	
 	$data['default'] = $this->crud->browse("","references","id",$refID,"false"); 
 	$this->load->view('vendor/detail/ref/form_update',$data);
 }
 
 function update_reference(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$data 		= array('cust_name'		=> $_POST['cust_name'],
					 	'project'		=> $_POST['project'],
					 	'point'			=> $_POST['point'],
					 	'date'			=> $_POST['pjdate'],
					 	'remark'		=> $_POST['remark'],					 	
					 	'last_updated'	=> $curr_date);
					 	
	$this->crud->update("","references","id",$_POST['id'],$data); 
 }
 
 function delete_ref(){ 
 	
 	$this->crud->delete("","references","id",$_POST['id']); 	
 	
 }

 function form_aff(){
 	
 	$this->load->view('vendor/detail/aff/form_create');
 	 
 }
 
 function load_aff(){  	
 	$venID   = $this->session->userdata("venID");	
	$data['aff'] = $this->crud->browse("","affiliates","id_vendor",$venID,"true");		
	$this->load->view('vendor/detail/affiliate',$data);  	
 }
 
 function add_affiliate(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('id'			=> null, 						
					 	'id_vendor'		=> $venID,
					 	'company_name'	=> $_POST['company_name'],
					 	'competency'	=> $_POST['competency'],
					 	'contact'		=> $_POST['contact'],					 	
					 	'remark'		=> $_POST['remark'],
					 	'status'		=> '1',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date);
				 	
	$this->crud->insert("","affiliates",$data); 
 	
 }

 function form_update_aff(){
 	
 	$affID  = $this->uri->segment(3); 	
 	$data['default'] = $this->crud->browse("","affiliates","id",$affID,"false"); 
 	$this->load->view('vendor/detail/aff/form_update',$data); 	
 }
 
 function update_affiliate(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$data 		= array('company_name'	=> $_POST['company_name'],
					 	'competency'	=> $_POST['competency'],
					 	'contact'		=> $_POST['contact'],					 	
					 	'remark'		=> $_POST['remark'],					 						 	
					 	'last_updated'	=> $curr_date);
					 	
	$this->crud->update("","affiliates","id",$_POST['id'],$data); 
 	
 }
 
 function delete_aff(){
 	
 	$this->crud->delete("","affiliates","id",$_POST['id']); 	
 	
 }

 function load_categories(){
 	
 	$venID   = $this->session->userdata("venID");	
 	$select2 = "id,(SELECT category FROM category r WHERE r.id=l.id_category) as category,
			   (SELECT subcategory FROM subcategory s WHERE s.id=l.id_subcat) as subcategory";					
 	$data['categories'] = $this->crud->browse("","vendor_category l","id_vendor",$venID,"true",$select2);	
 	
 	$this->load->view('vendor/detail/categories',$data);
 	
 }

 function form_categories(){
 	
 	$default  = array(''=>'--Select Category--');
 	$dbcat = $this->crud->browse("","category","","","true","id,category");
 	foreach($dbcat as $val){ $category[$val->id] = $val->category; }
 	$data['category'] = $default + $category;
 	
 	$this->load->view('vendor/detail/categories/form_create',$data);
 	
 }
 
 function is_exist_category(){

	$venID      = $this->session->userdata("venID");
	$id_cat     = $_POST['id_category'];
	$id_subcat  = $_POST['id_subcat'];

 	$where    = array('id_vendor'=>$venID,'id_category'=>$id_cat,'id_subcat'=>$id_subcat);
 	$is_exist = $this->crud->is_exist("","vendor_category","id",$where);
 	$result   = (!$is_exist  && $id_subcat) ? "true" : "false"; 
 	
 	$array['status']  =  $result;	
	echo json_encode($array);

 }
 
 function get_subcategory(){
 	$id_cat  = $_POST['id_cat']; $select = "id,subcategory as subcat"; 	
 	$subcat  = $this->crud->browse("","subcategory","id_cat",$id_cat,"true",$select); 	
	echo "<option value=''>-- Select Sub Category --</option>";
	foreach ($subcat as $row){
		echo "<option value='$row->id'>$row->subcat</option>";	
	}
 }
 
 function create_category(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$venID      = $this->session->userdata("venID");
 	$data 		= array('id'			=> null, 						
					 	'id_vendor'		=> $venID,
					 	'id_category'	=> $_POST['id_category'],
					 	'id_subcat'		=> $_POST['id_subcat'],					 	
					 	'status'		=> '1',
					 	'created_id'	=> $userID,
					 	'created_date'	=> $curr_date,
					 	'last_updated'	=> $curr_date);
				 	
	$this->crud->insert("","vendor_category",$data); 
 	
 }

 function form_update_category(){
 	
 	$id      = $this->uri->segment(3); 
 	$select  = "id,id_category as id_cat,id_subcat,(SELECT subcategory FROM subcategory r WHERE r.id=l.id_subcat) as subcategory";	
 	$default = $this->crud->browse("","vendor_category l","id",$id,"false",$select);
 	
 	$subcat  = array($default->id_subcat => $default->subcategory);
 	
 	$dbcat = $this->crud->browse("","category","","","true","id,category");
 	foreach($dbcat as $val){ $category[$val->id] = $val->category; }
 	
 	$data['default']     =  $default;
 	$data['category']    =  $category;
 	$data['subcategory'] =  $subcat;
 	
 	$this->load->view('vendor/detail/categories/form_update',$data);
 	
 }

 function update_category(){
 	
 	$curr_date 	= date('Y-m-d H:i:s'); 
 	$userID     = $this->tank_auth->get_user_id();
 	$data 		= array('id_category'	=> $_POST['id_category'],
					 	'id_subcat'		=> $_POST['id_subcat'],					 						 						 	
					 	'last_updated'	=> $curr_date);
					 	
	$this->crud->update("","vendor_category","id",$_POST['id'],$data); 
 	
 }  
 
 function delete_subcategory(){
 	
 	$this->crud->delete("","vendor_category","id",$_POST['id']); 	
 	
 	
 }

 function download_file(){
 	
 	$id = $this->uri->segment(3);
 	$this->load->helper('download');
 	
 	$file     = $this->crud->browse("","correspondence","id",$id,"false","id,filetype as type"); 	
 	$filename = $file->id.".".$file->type; 
 	$path 	  = './uploads/documents/'.$filename;			
 	$is_exist = file_exists($path) ? "true" : "false";
 	
 	if($is_exist){
		$data = file_get_contents($path); 
		force_download($filename, $data);	} 		
	
	$this->session->set_userdata('tab','menu5');
 	redirect("vendor/detail");	
 	
 }

}

