<div id="#main_container">

<script type="text/javascript">
$(document).ready(function(){
   var baseUrl  = "<?php echo base_url(); ?>";
   $(function () {
    $(".js-example-basic-single").select2();
		$(".js-example-basic-multiple").select2();

   });

   $('#sd').datepicker({
          changeMonth: true, changeYear: true,
      dateFormat: "yy-mm-dd", minDate: 0
     });
   $('#btn_save').click(function(event) {
      var red_id = $('#red_id').val();
      if(red_id == '')
      {

      var p_url = baseUrl+'redlist/validate_vendor';
      var id = $("#ven_id").val();
      $.post(p_url, {id_ven : id}, function(data, textStatus, xhr) {
           if(data == 'false')
           {
              alert('Vendor yang anda pilih sedang dalam proses lain. silakan pilih vendor lain');
              $('#ven_id').val('');
              $('#ven_id').change();
           }
           else
           {
              $( "#btn_submit" ).trigger( "click" );
           }
      });

      return false;
      }
      else
      {
        $( "#btn_submit" ).trigger( "click" );
      }

   });

});

function remove_file(elm)
{
  $(elm).parent('div').remove();
}
function delete_file(elm)
{
  $(elm).parent('div').hide();
  $(elm).parent('div').find('input').attr('name', "deleted_file[]");
}

function add_more_file()
{
  var new_file = '<div><input type="file" name="userfile[]"/><button onclick="remove_file(this)" type="button">Remove</button></div>';
  $('#more_file').append(new_file);
}

</script>
<div class="page-header">
  <h3>Vendor Redlist</h3>
</div>
<div id="debug"></div>
<?php echo form_open_multipart('#',array('id'=>'form', 'class'=>'form-horizontal','role'=>'form'));	?>
  <input type="hidden" id="red_id" name="red_id" value="<?php echo (isset($redlist->id_redlist))?$redlist->id_redlist:''; ?>">
  <input type="hidden" name="status" value="<?php echo (isset($redlist->red_state))?$redlist->red_state:''; ?>">
  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Select Vendor</label>
    <div class="col-sm-8">
    	<select name="ven_id" id="ven_id" class="form-control js-example-basic-single" required="required">

        <?php
        if(isset($redlist->id_redlist))
        {
          echo "<option value='$redlist->id'>$redlist->vendor_name</option>";
        }
        else
        {
      		  echo "<option value=''>--Select Vendor--</option>";
          foreach ($vendor as $ven)
          {
            echo "<option value='$ven->id'>$ven->vendor_name</option>";
          }
        }

        ?>
    	</select>
    </div>
  </div>

  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Start Date</label>
    <div class="col-sm-8">
      <input type="text" value="<?php echo (isset($redlist->start_date))?$redlist->start_date:''; ?>" required name="start_date" id="sd" class="form-control" placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Counter</label>
    <div class="col-sm-8">
      <input style="border:none;" readonly type="text" value="<?php echo (isset($redlist->counter))?$redlist->counter: 2 ; ?>" required name="counter" class="form-control"  placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label for="input1" class="col-sm-2 control-label">Reason</label>
    <div class="col-sm-8">
    	<textarea required name="remark" id="remark" class="form-control" rows="3"><?php echo (isset($redlist->remark))?$redlist->remark:''; ?></textarea>
    </div>
  </div>

  <div class="form-group">
    <label for="input1"  class="col-sm-2 control-label">Attachment</label>
    <button type="button" onclick="add_more_file()" class="btn btn-default pull-left">add more attachment</button><br/><br/><br/>
    <div class="col-sm-8">
    <div id='more_file'>
    <input type="file" name="userfile[]">
      
    </div>
    <?php
        foreach ($redlist_docs as $key => $value) 
        {
          echo '<div><button onclick="delete_file(this)" type="button">Remove</button><input type="hidden"  value="'.$value->id.'">'.$value->filename.'</div>';
        }
     ?>
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <a type="button" href="javascript:void(0)" onclick="load_data()" class="btn btn-warning">Back</a>
      <button type="button" id="btn_save" class="btn btn-primary">Save</button>
      <button id="btn_submit" type="submit" style="display:none;">submit</button>
    </div>
  </div>
<?php echo form_close(); ?>

</div>