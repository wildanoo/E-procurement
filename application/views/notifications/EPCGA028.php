Dengan hormat,<br>
<b>Bapak/Ibu <?php echo $to ?></b><br><br>
Dengan ini kami informasikan, untuk data di bawah ini :<br><br>
<b>Nama Perusahaan :</b><?php echo $vendor_name ?><br>
<b>ID Vendor :</b><?php echo $ven_num ?><br>
<b>Kategori yang terdaftar :</b><br>
<table cellpadding="2" cellspacing="2" width="100%" border="1">
		<tr>
			<th style="text-align: left;">No</th>
			<th style="text-align: left;">Category</th>	
			<th style="text-align: left;">Sub Category</th>	
		</tr>
		
		<?php if($category){ $i=1;
			foreach($category as $row){ ?>
				<tr>
					<td><?php echo $i; ?></td>	
					<td><?php echo $row->category; ?></td>	
					<td><?php echo $row->subcategory; ?></td>		
				</tr>
				<?php	$i++;  } ?>	
				<?php } else { ?>
					<tr><td colspan="8">-</td></tr>
					<?php } ?>	
				</table>
				<br/>
<b>Alasan :</b> <br>
<?php echo $reason ?><br><br>
<b>Link vendor:</b><br>
<?php echo $link; ?><br><br>
Dimohon untuk divaluasi kembali<br>
Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih <br><br>
(&nbsp;<b><?php echo ucfirst( $this->tank_auth->get_username()); ?></b>&nbsp;)
<br><br/>
