<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";          
     
   $('#btn_cancel5').click(function(){ 
   		 $("#container5").load(baseUrl+'vendor/load_organization');
     });     
	   
   });

</script>
<div id="debug5"></div>
<?php echo form_open_multipart('vendor/update_organization',array('id'=>'form_org')); //$current_date = date('Y-m-d'); ?>
<input type="hidden" name="orgID" id="orgfID" value="<?php echo $default->id; ?>"/>
<table width="50%">
<tr>
	<td><label>Organization Name</label></td><td>:</td>
	<td><input type="text" name="name" id="name" value="<?php echo $default->name; ?>" placeholder="Organization Name" class="form-control" required/></td>
	<td id="msg1">&nbsp;</td>	
</tr>
<tr>
	<td><label>Address</label></td><td>:</td>
	<td><input type="text" name="address" id="address" value="<?php echo $default->address; ?>" placeholder="Address" class="form-control" required/></td>
	<td id="msg2">&nbsp;</td>	
</tr>
<tr>
	<td><label>Phone</label></td><td>:</td>
	<td><input type="text" name="phone" id="phone" value="<?php echo $default->phone; ?>"placeholder="Phone" class="form-control" required/></td>
</tr>
<tr>
	<td><label>Position</label></td><td>:</td>
	<td><input type="text" name="position" id="position" value="<?php echo $default->position; ?>" placeholder="Position" class="form-control" /></td>
</tr>
<tr>
	<td><label>Remark</label></td><td>:</td>
	<td><input type="text" name="remark" id="remark" value="<?php echo $default->remark; ?>" placeholder="Remark" class="form-control" required/></td>
</tr>
<tr>
	<td colspan="3">
		<input type="button" id="btn_cancel5" value="Cancel" class="btn btn-default btn-sm"/>
		<input type="submit"  value="Update" class="btn btn-primary btn-sm"/>		
	</td>	
</tr>
</table><br/><br/>
<?php echo  form_close(); ?>


