
Dengan hormat,<br/>
<b><?php echo $vendor_name; ?></b><br/><br/>

Sehubungan dengan proses registrasi perusahaan Bapak/Ibu, bahwa perusahaan Bapak/Ibu termasuk dalam kategori perusahaan yang kami butuhkan.<br/>
Selanjutnya mohon agar Bapak/Ibu dapat menyampaikan dokumen-dokumen berikut untuk dapat kami evaluasi :<br/><br/>

<!--1.	Salinan Akta Pendirian Perusahaan beserta perubahannya.<br/>
2.	Salinan SIUP (Surat Ijin Usaha Perdagangan) atau SIUJK (Surat Ijin Usaha Jasa Kontruksi) dan SBU (Sertifikat Badan Usaha) Jasa Konstruksi untuk perusahaan yang bergerak di bidang Konstruksi.<br/> 
3.	Salinan NPWP (Nomor Pokok Wajib Pajak).<br/>
4.	Salinan Surat Penetapan sebagai PKP (Pengusaha Kena Pajak). <br/>
5.	Salinan Surat Keterangan Domisili yang masih berlaku. <br/>
6.	Salinan bukti pembayaran pajak 2 tahun terakhir.<br/> 
7.	Salinan Tanda Daftar Perusahaan (TDP) yang masih berlaku.<br/> 
8.	Salinan Laporan Keuangan perusahaan 2 tahun terakhir. Diutamakan yang sudah diaudit oleh Akuntan Publik. <br/>
9.	Salinan Sertifikasi dari badan standarisasi sesuai spesifikasi dan sub bidang yang dikeluarkan oleh departemen atau badan standarisasi resmi nasional dan internasional (bila ada). <br/><br/-->
<?php 
	$i = 1; 	
	foreach($require_doc as $val){
		echo $i.". ".$val->point."<br/>";		
	$i++; }
?><br/><br/>
Dokumen agar dapat disampaikan dalam bentuk hardcopy dan softcopy berupa Compact Disc (CD) dikirimkan ke alamat :<br/><br/>

