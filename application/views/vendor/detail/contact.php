<div id="container3">

<script type="text/javascript">
$(function () { 
   var baseUrl  = "<?php echo base_url(); ?>"; 
   
   $('#btn_create3').click(function(){ 
   		$("#container3").load(baseUrl+'vendor/form_create');   		
    }); 
   
 });  
 
  function form_update(id){
  	   var baseUrl  = "<?php echo base_url(); ?>";
  	   $("#container3").load(baseUrl+'vendor/form_update/'+id);
  } 

  function delete_contact(id){
 	var baseUrl = "<?php echo base_url(); ?>";
 	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
	var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
 	
 	if (confirm('Are you sure you want to delete contact?')) {	
		$.post( baseUrl + "vendor/delete_contact", { csrf: token, id: id } );
		$("#container3").load(baseUrl+'vendor/load_contact');		
	}	
 }
  
</script>

 <div style="text-align: right">
 
 <?php if($this->global->active){ ?><button type="button" id="btn_create3" class="btn btn-info btn-primary">Add Contact</button><?php } ?>

</div>

<table  class="table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>Name</th>
		<th>Position</th>
		<th>Mobile</th>
		<th>Email</th>
		<?php  if($this->global->active){ ?><th>Action</th><?php } ?>
	</tr>
	</thead>
	<tbody>

 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td><?php echo $i; ?></td>
			<td><?php echo $row->fullname; ?></td>	
			<td><?php echo $row->position; ?></td>
			<td><?php echo $row->mobile; ?></td>
			<td><?php echo $row->email; ?></td>
			<?php  if($this->global->active){ ?>
			<td style="text-align: center;">
			<?php	echo '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"></i>'; echo '&nbsp;&nbsp;&nbsp;';					
					echo '<i class="fa fa-trash"  onclick="delete_contact('.$row->id.')"></i>';	
			?></td>	
			<?php } ?>								
		</tr>
<?php	$i++;  } ?>	
<?php } else { ?>
	<tr><td colspan="6">-</td></tr>
<?php } ?>
     </tbody>
</table>

</div>