<table id="attachment-section" class="table table-striped table-bordered">
	<thead>
        <tr>
            <th>No</th>
            <th>Description</th>
            <th>Remarks</th>
            <th>Created Date</th>
            <th>Attachment</th>
            <th>Action</th>
        </tr>
	</thead>
    <tbody id="attachment-body">
    	<?php
		if(count($attachments)>0){
		$count = 1;
		foreach($attachments as $row){ ?>
		<tr>
			<td class="text-center"><?php echo $count; ?></td>
			<td><?php echo $row->description ?></td>
			<td><?php echo $row->remarks ?></td>
			<td><?php echo date( 'M d Y', strtotime($row->created_date)) ?></td>
			<?php if($row->filename != null){?>
			<td><a href="<?php echo base_url('direct_selection/open_file/'.$this->uri->segment(2).'/'.$row->filename.'_'.$row->counter);?>" target="_blank">View</a></td>
			<td><a id="meh" href="#" onClick="return false;" class="btn btn-danger delete-file" data="<?php echo $row->filename;?>" data-file="<?php echo $row->filename.'_'.$row->counter;?>">Delete</a></td>
			<?php } elseif($row->filename == null){?>
			<td><a href="<?php echo base_url('direct_selection/open_file/'.$this->uri->segment(2).'/'.$row->id.'_'.$row->counter);?>" target="_blank">View</a></td>
			<td><a id="meh" href="#" onClick="return false;" class="btn btn-danger delete-file" data="<?php echo $row->id;?>" data-file="<?php echo $row->id.'_'.$row->counter;?>">Delete</a></td>
			<?php } ?>
		</tr>
		<?php $count++;} ?>
		<?php } else { ?><tr><td colspan="7">-</td></tr><?php } ?>
	</tbody>
</table>