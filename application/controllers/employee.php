<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Employee extends Controller {

	function __construct()
	{
		parent::__construct();	$this->load->model('m_user');					
	}

	function index(){
		
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
			
		$table 	    = "ga_employee";
		$page       = $this->uri->segment(3);
		$per_page   = 10;
		$offset     = $this->crud->set_offset($page,$per_page);
		$total_rows = $this->crud->get_total_record("",$table);
		$set_config = array('base_url'=> base_url().'/employee/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
		$config     = $this->crud->set_config($set_config);
			
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();
	
		$where 			     = array('status'=>'1');
		$order 			     = array('field'=>'created_date','order'=>'DESC');
		$data['pagination']  = $paging;
		$data['num']         = $offset;
		$select 			 = "id,nopeg,fullname,(SELECT office_name FROM offices WHERE id=id_office)as office_name,
								(SELECT division_name FROM division WHERE id=id_div)as division_name,
								(SELECT dept_name FROM departemen WHERE id=id_dept)as dept_name,
								(SELECT position_name FROM position WHERE id=id_position)as position_name,
								(SELECT functional FROM functional WHERE id=id_functional)as functional,
								(SELECT username FROM users WHERE id=created_id)as created_id,
								status,(SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";
		$sessID  = $this->session->flashdata('anID');
		$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
	
		if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true","",$where);
		else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
		$data['browse'] = $browse;
			
		$data['view']	= "employee/browse";
		$this->load->view('layout/template',$data);
	}
	
	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
	
		$search_conditions = array(
				'nopeg'			=> $_POST['search_term'],
				'fullname'		=> $_POST['search_term'],
				'office_name'	=> $_POST['search_term'],
				'division_name'	=> $_POST['search_term'],
				'dept_name'		=> $_POST['search_term'],
				'position_name'	=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		$data['view'] = "employee/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{
		/* get data for table view from database with pagination */
	
		$table 	     = "ga_employee t1";
		$select 	 = "t1.id,t1.nopeg,t1.fullname,(SELECT office_name FROM offices WHERE id=t1.id_office)as office_name,
								(SELECT division_name FROM division WHERE id=t1.id_div)as division_name,
								(SELECT dept_name FROM departemen WHERE id=t1.id_dept)as dept_name,
								(SELECT position_name FROM position WHERE id=t1.id_position)as position_name,
								t1.status,(SELECT username FROM users WHERE id=t1.created_id)as created_id,t1.created_date,t1.last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'employee/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
	
		/* get data for table view from database with pagination */
		
		$joins[0][0] = 'offices t2';
		$joins[0][1] = 't2.id = t1.id_office';
		$joins[0][2] = 'left';		
		$joins[1][0] = 'division t3';
		$joins[1][1] = 't3.id = t1.id_div';
		$joins[1][2] = 'left';
		$joins[2][0] = 'departemen t4';
		$joins[2][1] = 't4.id = t1.id_dept';
		$joins[2][2] = 'left';
		$joins[3][0] = 'position t5';
		$joins[3][1] = 't5.id = t1.id_position';
		$joins[3][2] = 'left';
	
		$table 	     = "ga_employee t1";
		$select 	 = "t1.id,t1.nopeg,t1.fullname,(SELECT office_name FROM offices WHERE id=t1.id_office)as office_name,
								(SELECT division_name FROM division WHERE id=t1.id_div)as division_name,
								(SELECT dept_name FROM departemen WHERE id=t1.id_dept)as dept_name,
								(SELECT position_name FROM position WHERE id=t1.id_position)as position_name,
								t1.status,(SELECT username FROM users WHERE id=t1.created_id)as created_id,t1.created_date,t1.last_updated";
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse_join('',$table,"COUNT(t1.id) AS COUNT",$joins,$where,$dateranges,$wherearray);
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,$where,$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'employee/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	function get_division(){
		$id_office = $_POST['id_office'];
		$select  = " id,division_name";
		$division  = $this->crud->browse("","division","id_office",$id_office,"true",$select);
		echo "<option>-- Select --</option>";
		foreach ($division as $row){
			echo "<option value='$row->id'>$row->division_name</option>";
		}
	}
	
	function get_department(){
		$id_division = $_POST['id_div'];
		$select  = " id,dept_name";
		$department  = $this->crud->browse("","departemen","id_division",$id_division,"true",$select);
		echo "<option>-- Select --</option>";
		foreach ($department as $row){
			echo "<option value='$row->id'>$row->dept_name</option>";
		}
	}
	
	function form_create(){
		
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$department = $this->crud->browse("","departemen","","","true","id,dept_name");
		if(!$department) $department = array();
		$select = array('0'=>'-- Select --');
		foreach($department as $val){ $option_dept[$val->id] = $val->dept_name; }
		$data['department'] = $select + $option_dept;
			
		$office = $this->crud->browse("","offices","","","true","id,office_name");
		if(!$office) $office = array();
		$select_office = array('0'=>'-- Select --');
		foreach($office as $val){ $option_office[$val->id] = $val->office_name; }
		$data['office'] = $select_office + $option_office;
			
		$division = $this->crud->browse("","division","","","true","id,division_name");
		if(!$division) $division = array();
		$select_division = array('0'=>'-- Select --');
		foreach($division as $val){ $option_div[$val->id] = $val->division_name; }
		$data['division'] = $select_division + $option_div;
			
		$position = $this->crud->browse("","position","","","true","id,position_name");
		if(!$position) $position = array();
		$select_position = array('0'=>'-- Select --');
		foreach($position as $val){ $option_position[$val->id] = $val->position_name; }
		$data['position'] = $select_position + $option_position;
		
		$data['view']	= "employee/form_create";
		$this->load->view('layout/template',$data);
	}
	
	function create(){
		
		$userID = $this->tank_auth->get_user_id();
		$curr_date 	= date('Y-m-d H:i:s');
		$data = array('id'=>null,
				'userID'=>$userID,
				'nopeg'=>$_POST['nopeg'],
				'fullname'=>$_POST['fullname'],
				'id_office'=>$_POST['id_office'],
				'id_div'=>$_POST['id_div'],
				'id_dept'=>$_POST['id_dept'],
				'id_position'=>$_POST['id_position'],
				'created_id'=>$userID,
				'created_date'=>$curr_date,
				'last_updated'=>$curr_date
					
		);
		
		$where 	     = array('fullname'=>$_POST['fullname']);
		$is_exist    = $this->crud->is_exist("","ga_employee","id",$where);
		
		if(!$is_exist){
			$this->crud->insert("","ga_employee",$data); }
				
			$msg1 = "succesfully add new employee";
			$msg2 = "duplicate employee name";
			$msg  = !$is_exist ? $msg1 : $msg2;
		
		//$this->crud->insert("","ga_employee",$data);
		$this->session->set_flashdata('message','1 data success added');
		redirect('employee/','refresh');
		
	}
	
	function form_update(){
		
		$id 					= $this->uri->segment(3);
		
		$def					= $this->crud->browse('','ga_employee','id',$id,'false','id,nopeg,fullname');
		$data['def']			= $def;
		
		$def_office 			= $this->crud->browse("","ga_employee l","id",$id,"false","(SELECT id FROM offices WHERE id=id_office) as id_office");
		$data['def_office'] 	= $def_office ;
		
		$office 				= $this->crud->browse("","offices","","","true","id,office_name");
		if(!$office) $office	= array();
		$select_office 			= array(''=>'-- Select --');
		foreach($office as $val_office){ $options_office[$val_office->id] = $val_office->office_name; }
		$data['office'] 		= $select_office + $options_office;
		
		$def_div 	= $this->crud->browse("","ga_employee l","id",$id,"false","(SELECT id FROM division WHERE id=id_div) as id_div");
		$data['def_div'] = $def_div ;
		
		$division = $this->crud->browse("","division","id",$def_div->id_div,"true","id,division_name");
		if(!$division) $division = array();
		$select_division = array(''=>'-- Select --');
		foreach($division as $val_division){ $options_division[$val_division->id] = $val_division->division_name; }
		$data['division'] = $select_division + $options_division;
		
		$def_dept 	= $this->crud->browse("","ga_employee l","id",$id,"false","(SELECT id FROM departemen WHERE id=id_dept) as id_dept");
		$data['def_dept'] = $def_dept ;
		
		$department = $this->crud->browse("","departemen","id_division",$def_div->id_div,"true","id,dept_name");
		if(!$department) $department = array();
		$select_dept = array(''=>'-- Select --');
		foreach($department as $val_dept){ $options_dept[$val_dept->id] = $val_dept->dept_name; }
		$data['department'] = $select_dept + $options_dept;
		
		$def_position 	= $this->crud->browse("","ga_employee l","id",$id,"false","(SELECT id FROM position WHERE id=id_position) as id_position");
		$data['def_position'] = $def_position ;
		$position = $this->crud->browse("","position","","","true","id,position_name");
		if(!$position) $position = array();
		$select_position = array(''=>'-- Select --');
		foreach($position as $val_position){ $options_position[$val_position->id] = $val_position->position_name; }
		$data['position'] = $select_position + $options_position;
		
		$def_functional 	= $this->crud->browse("","ga_employee l","id",$id,"false","(SELECT id FROM functional WHERE id=id_functional) as id_functional");
		$data['def_functional'] = $def_functional ;
		$functional = $this->crud->browse("","functional","","","true","id,functional");
		if(!$functional) $functional = array();
		$select_functional = array('0'=>'-- Others --');
		foreach($functional as $val_functional){ $options_functional[$val_functional->id] = $val_functional->functional; }
		$data['functional'] = $select_functional + $options_functional;
		//print_r($data['functional']);
		
		$data['view']	= "employee/form_update";
		$this->load->view('layout/template',$data);
		
	}
	
	function validate($selfID,$nopeg){
		$db="db";
		$this->$db->db_select();
		$state  = "WHERE nopeg='$nopeg' AND id!='$selfID' ";
		$query  = "SELECT EXISTS(SELECT id FROM ga_employee $state) as jum ";  //echo $query; exit;
		$result =  $this->$db->query($query);
		$hasil  =  $result->row()->jum;
		//echo '<pre>'; print_r($this->$db->last_query()); echo '</pre>';
		if ($hasil > 0){ return true;
		} else {  return false;  }
	
	}
	
	function validate_nopeg(){
		//print_r($_POST);
		$is_exist = $this->validate($_POST['id'],$_POST['nopeg']);
		$result = $is_exist ? "false" : "true";
		echo $result;
	}
	
	function validate_update(){	

		$checked = array('fullname','id_office','id_div','id_dept','id_position'); $i=1;
		foreach($checked as $val){ 		
			$chk 	 = $_POST[$val]; 		
			$cond1   = !$chk || $chk==0; $cond2   = !$chk ; 
			$checked = $val=="fullname" ? $cond2 : $cond1;
			$validate[$i] = $checked ? "false" : "true";
			$result["msg".$i] = $checked ? "field is required !" : "";
		$i++; }
		
		$result["status"] = in_array("false",$validate) ? "false" : "true";		
		//print_r($validate);  echo "<br/><br/>";
		//print_r($result);
		echo json_encode($result);
			
	}
	
	function update(){ 	
		$footer = array( 'created_id'	=> $this->tank_auth->get_user_id(),
						 'created_date'	=> date('Y-m-d H:i:s'),
						 'last_updated'	=> date('Y-m-d H:i:s') );								
		$id    = $_POST['id']; 
		$data  = array(	'nopeg'		=> $_POST['nopeg'],
						'fullname'	=> $_POST['fullname'],
						'id_office'	=> $_POST['id_office'],
						'id_div'	=> $_POST['id_div'],
						'id_dept'	=> $_POST['id_dept'],
						'id_position'	=> $_POST['id_position'],
						'id_functional'	=> $_POST['id_functional'],
						'status'		=> '1',
						'created_id'	=> $this->tank_auth->get_user_id(),
						'last_updated'	=> date('Y-m-d H:i:s'));

		$this->crud->update("","ga_employee",'id',$id,$data);
		
		$userID    = $this->crud->browse("","ga_employee","id",$id,"false","userID")->userID;				
		$user      = $this->m_user->get_user_group($userID);	
		$is_exist  = $this->crud->browse("","groups","code",$user->group_code,"false","id");

		if($is_exist){
			$update  = array('name'=>$user->group_name,'last_updated'=>date('Y-m-d H:i:s'));
			$this->crud->update("","groups","id",$is_exist->id,$update); $group_id = $is_exist->id;
		} else {
			$insert2  = array('code'=>$user->group_code,'name'=>$user->group_name,'permissions'=>"");							
			$group_id = $this->crud->insert("","groups",$insert2 + $footer); 
		}				
			
		$where      = array('user_id'=>$userID);
 		$usr_exist  = $this->crud->is_exist("","users_groups","user_id",$where);				
		if($usr_exist){					
			$current = $this->crud->browse("","users_groups","user_id",$userID,"false","group_id");			
			if($current->group_id!=$group_id){						
				$update  = array('group_id'=>$group_id,'last_updated'=>date('Y-m-d H:i:s'));
				$this->crud->update("","users_groups","user_id",$userID,$update); 
			}					
		} else {
			$insert2  = array('user_id'=>$userID,'group_id'=>$group_id);							
			$this->crud->insert("","users_groups",$insert2 + $footer); 
		}	

		$this->session->set_flashdata('message','1 data success update');
		redirect('employee/','refresh');
	}
	
	function delete(){
		$id     = $this->uri->segment(3);
		$userID = $this->crud->browse("","ga_employee","id",$id,"false","userID")->userID; 
		$this->crud->update("","users","id",$userID,array('activated'=>'0'));
		$this->crud->update("","ga_employee","id",$id,array('status'=>'0'));
		$this->session->set_flashdata('message','1 data success deleted');
		redirect("employee/","refresh");
	}
}