<div id="container9">
<script type="text/javascript">
  $(function () { 
   var baseUrl  = "<?php echo base_url(); ?>";
   
	$('#pjdate').datepicker({  
        changeMonth: true, changeYear: true,
		dateFormat: "yy/mm/dd" ,
    });     
   
   $('#btn_create9').click(function(){
    	var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
		var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
		var values  = $("#form_ref").serializeArray();
				values.push({name: csrf,value: token});
				values = jQuery.param(values); 
						$.ajax({
						    url  : baseUrl + 'data_vendor/update_reference',
						    type : "POST",              		               
						    data : values,
						    success: function(resp){
						        $("#container9").load(baseUrl+'data_vendor/load_ref', function(){$('#ref_msg').html('Data akan ditampilkan setelah proses approval disetujui');});
						    }
						});							     	
     }); 
     
   $('#btn_cancel9').click(function(){ 
   		 $("#container9").load(baseUrl+'data_vendor/load_ref');   		 
     });     
	   
   });

</script>
<div id="debug9"></div>
<?php echo form_open('#',array('id'=>'form_ref')); $current_date = date('Y-m-d'); ?>
<table width="50%">
<input type="hidden" name="id" id="id" value="<?php echo $default->id; ?>" />
<tr>
	<td><label>Customer Name</label></td><td>:</td>
	<td><input type="text" name="cust_name" id="cust_name" value="<?php echo $default->cust_name; ?>" placeholder="Customer Name" class="form-control" required/></td>
	<td id="msg1">&nbsp;</td>	
</tr>
<tr>
	<td><label>Project Name</label></td><td>:</td>
	<td><input type="text" name="project" id="project" value="<?php echo $default->project; ?>" placeholder="Project Name" class="form-control" required/></td>
	<td id="msg2">&nbsp;</td>	
</tr>
<tr>
	<td><label>Point</label></td><td>:</td>
	<td><input type="text" name="point" id="point" value="<?php echo $default->point; ?>" placeholder="Point" class="form-control" required/></td>
</tr>

<tr>
	<td><label>Project Date</label></td><td>:</td>
	<td><input type="text" name="pjdate" id="pjdate" value="<?php echo $default->date; ?>" class="form-control" /></td>
</tr>
<tr>
	<td><label>Remark</label></td><td>:</td>
	<td><input type="text" name="remark" id="remark" value="<?php echo $default->remark; ?>" placeholder="Remark" class="form-control" required/></td>
</tr>

</table>

<div style="text-align: right;">
	<input type="button" id="btn_cancel9" value="Cancel" class="btn btn-default btn-sm"/>
	<input type="button" id="btn_create9" value="Update" class="btn btn-primary btn-sm"/>	
</div>
<br/>
<?php echo  form_close(); ?>
</div>


