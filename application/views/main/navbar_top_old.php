<header class="clearfix header5">
    <div class="container">
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img alt="" src="<?php echo base_url()?>assets_public/images/garuda-logo.png">
                        <p class="cos-custom"><span style="color:#428bca">e-Procurement</span> Garuda Indonesia</p>
                    </a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right2">
                        <li class="active"><a href="<?php echo base_url()?>auth/login#login">LOGIN</a></li>
                        <li><a href="<?php echo base_url()?>auth/registration"><?php echo $this->lang->line('global_registration_tag');?></a></li>
                        <li><a href="registration.html#top">VENDOR POLICY</a></li>
                    </ul>
                    <ul class="social-icons">
                        <?php if ($this->session->userdata('lang') == 'eng') { ?>
                        <li class="lang eng img-lang" data="lang-eng" title="English"><a style="" href="#"></a></li>
                        <li class="lang ind img-lang" data="lang-ind" title="Indonesian"><a class="inactive" href="#" id="lang_ind"></a></li>
                        <?php }elseif ($this->session->userdata('lang') == 'ind') { ?>
                        <li class="lang eng img-lang" data="lang-eng" title="English"><a class="inactive" href="#" id="lang_eng"></a></li>
                        <li class="lang ind img-lang" data="lang-ind" title="Indonesian"><a style="" href="#"></a></li>
                        <?php }?>
                        <!-- <li class="eng img-lang" title="English"><a href="index.html"></a></li>
                        <li class="ind img-lang" title="Indonesian"><a href="index.html"></a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>