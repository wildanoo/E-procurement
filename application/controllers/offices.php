<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Offices extends Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_user');
		$this->load->model('m_category');
		islogged_in();		
		$this->m_user->authenticate(array(41));
	}	
		
	function index(){
		//if ($this->tank_auth->is_logged_in()) {

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			
			//$where =  array('status'=>'1');
			
			//$sess_like  = $this->session->flashdata('like');
            
            $sess_cat   = $this->session->userdata('sess_cat');
			$sess_field   = $this->session->userdata('sess_field');
			
				
			$field      = $sess_field != '' ? $sess_field : "office_name";
			$id_cat     = $sess_cat != '' ? $sess_cat  : "";
				
			if($sess_cat != ''){	$where = array('office_name'=>$sess_cat,'status'=>'1');
			} else { $where = array('status'=>'1');	}  
			
	 		$table 	    = "offices";		
			$page       = $this->uri->segment(3);
			$per_page   = 10;		
			$offset     = $this->crud->set_offset($page,$per_page,$where);
			$total_rows = $this->crud->get_total_record("",$table,$where);
			$set_config = array('base_url'=> base_url().'/offices/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>3);
			$config     = $this->crud->set_config($set_config);		
				 
			$this->load->library('pagination');			 
			$this->pagination->initialize($config);
			$paging = $this->pagination->create_links();			
				
			$order 			     = array('field'=>'created_date','order'=>'DESC');
			$data['pagination']  = $paging;
			$data['num']         = $offset; 
			$select 			 = "id,office_name,office_code,(SELECT region_name FROM region r WHERE r.id=l.id_region) as region, 
									(SELECT id_country FROM region r WHERE r.id=l.id_region) as id_country, 
								    (SELECT country_name FROM country s WHERE s.id=id_country) as country,email,phone,fax,status,
								    (SELECT username FROM users WHERE id=created_id)as created_id,created_date,last_updated";			
			//if($sess_like){
			//$like   = array('col'=>'office_name','field'=>$sess_like);
			//$browse = $this->crud->browse("",$table." l","","","true",$select,"","","",$like);	} else {
			//$browse = $this->crud->browse_with_paging("",$table." l","","","true",$select,$where,$order,$config['per_page'],$offset); }			
            $sessID  = $this->session->flashdata('anID');
			$browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,"",$order,$config['per_page'],$offset);
				
			if($sessID)	$browse = $this->crud->browse("",$table,"id",$sessID,"true");
			else  $browse  = $this->crud->browse_with_paging("",$table." l",$field,$id_cat,"true",$select,$where,$order,$config['per_page'],$offset);
			$data['browse'] = $browse;

			$office = $this->crud->browse("","offices","","","true","id,office_name");
			if(!$office) $office = array();
			$select = array(''=>'All'); $options = array();
			foreach($office as $val){ $options[$val->id] = $val->office_name; }
			$data['office'] = $select + $options;
					
			//$data['browse'] = $browse;
	 		$data['view']	= "offices/browse"; 		
 			$this->load->view('layout/template',$data);
 		
	 		
// 		} else {  
// 			$this->session->set_flashdata('message','user not authorized'); 
// 			redirect('/auth/login/'); 	 }
	}

	private function search_input($search_dateranges = array(),$search_conditions = array()){
	
		if ($this->session->userdata('search_dateranges') != $search_dateranges) {
			$this->session->set_userdata('search_dateranges',$search_dateranges);
		}else{
			$splits = $this->session->userdata('search_dateranges');
	
			foreach ($splits as $key => $value) {
				$search_dateranges[$key] = $splits[$key];
			}
		}
	
		if ($_POST['search_term'] != $this->session->userdata('search_conditions') && $_POST['search_term']!='') {
			$this->session->set_userdata('search_conditions',$search_conditions);
		}else{
			$splits = $this->session->userdata('search_conditions');
	
			foreach ($splits as $key => $value) {
				$search_conditions[$key] = $splits[$key];
			}
		}
	
		$getData = array($search_dateranges,$search_conditions);
	
		return $getData;
	}
	
	
	function search(){
	
		/* initiate search inputs */
	
		//$search_dateranges = array('publish_date',$_POST['start_date'],$_POST['end_date']);
		$search_conditions = array(
				'office_code'		=> $_POST['search_term'],
				'office_name'		=> $_POST['search_term'],
				'country_name'			=> $_POST['search_term'],
				'region_name'			=> $_POST['search_term']
		);
	
		$where_conditions  = $this->search_input("",$search_conditions);
	
		/* ==== */
	
		/* get data from defined function for table view */
	
		$extract = $this->getDataTablesSearch(10,$where_conditions[0],$where_conditions[1]);
	
		/* ==== */
	
		/* preparing data for display */
	
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
	
		$data['browse']		= $extract['getData'];
		$data['pagination']	= $extract['pagination'];
		$data['num']		= $extract['num'];
	
		// 		$data['category'] 		 = $this->getCategories();
		// 		$data['subcategory'] 	 = $this->getSubcategories();
		// 		$data['subcontent_type'] = $this->getSubcontentTypes();
		// 		$data['status_ref'] 	 = $this->getStatusCondition();
	
	
		$data['view'] = "offices/browse";
	
		$this->load->view('layout/template',$data);
	
		/* ==== */
	
	}
	
	private function getDataTables($limit = NULL)
	{
		// 		$subcontent_allowed = globalNeedAanwidzing();
	
		// 		$sess_cat = $this->session->userdata('sess_cat');
		// 		$sessID   = $this->session->flashdata('anID');
			
		// 		$field    = $sess_cat ? "id_cat" : "";
		// 		$id_cat   = $sess_cat ? $sess_cat  : "";
	
		// 		if($sess_cat){
		// 			$where = "t1.subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'";
		// 		} else {
		// 			$where = !$this->session->userdata('is_admin') ? "subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'":"";
		// 		}
	
		/* get data for table view from database with pagination */
	
		$table 	     = "offices t1";
		$select 	 = "t1.id, t1.id_company,(SELECT country_name FROM country WHERE id=id_country) as country,(SELECT region_name FROM region r WHERE r.id=l.id_region) as region,
						t1.office_code, t1.office_name, t1.email, t1.phone, t1.fax, t1.status, t1.id_created, t1.created_date, t1.last_updated";
		//$select      = "t1.id,(SELECT country_name FROM country WHERE id=id_country) as t1.country_name,region_name,t1.remark,(SELECT username FROM users WHERE id=created_id)as t1.created_id,t1.created_date,t1.last_updated";
		//$order 		 = array('field'=>'t1.publish_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->browse('',$table,'','','false',"COUNT(t1.id) AS COUNT",$where);
	
		// if($sessID)	$getData = $this->crud->browse("",$table,"id",$sessID,"true",$select,$where);
		// else  {
		$joins[0][0] = 'country t2';
		$joins[0][1] = 't2.id = t1.id_country';
		$joins[0][2] = 'left';
		$joins[1][0] = 'region t3';
		$joins[1][1] = 't3.id = t1.id_region';
		$joins[1][2] = 'left';
	
		//SELECT t1.id_country, t2.country_name FROM region t1 left join country t2 on t1.id_country = t2.id
	
		// $where		 = !$this->session->userdata('is_admin') ? "t1.subcontent_type IN ('4','5') AND t1.status = '3'":"";
	
		$getData 	 = $this->crud->browse_join_with_paging("",$table,$field,$id_cat,"true",$select,$joins,$where,"",$per_page,$offset,"t1.id");
		// }
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'offices/index','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
	
		return $data;
	
	}
	
	private function getDataTablesSearch($limit = NULL,$dateranges = array(),$wherearray = array())
	{
		// 		$subcontent_allowed = globalNeedAanwidzing();
	
		// 		$sess_cat = $this->session->userdata('sess_cat');
		// 		$sessID   = $this->session->flashdata('anID');
			
		// 		$field    = $sess_cat ? "id_cat" : "";
		// 		$id_cat   = $sess_cat ? $sess_cat  : "";
	
		// 		if($sess_cat){
		// 			$where = "t1.subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'";
		// 		} else {
		// 			$where = !$this->session->userdata('is_admin') ? "subcontent_type IN (".$subcontent_allowed.") AND t1.status = '3'":"";
		// 		}
		$joins[0][0] = 'country t2';
		$joins[0][1] = 't2.id = t1.id_country';
		$joins[0][2] = 'left';
		$joins[1][0] = 'region t3';
		$joins[1][1] = 't3.id = t1.id_region';
		$joins[1][2] = 'left';
		/* get data for table view from database with pagination */
	
		$table 	     = "offices t1";
		$select 	 = "t1.id, t1.id_company,(SELECT country_name FROM country WHERE id=t1.id_country) as country,(SELECT region_name FROM region r WHERE r.id=t1.id_region) as region,
						t1.office_code, t1.office_name, t1.email, t1.phone, t1.fax, t1.status, (SELECT username FROM users WHERE id=t1.created_id) as created_id, t1.created_date,t1.last_updated";
		//$select      = "t1.id,(SELECT country_name FROM country WHERE id=id_country) as t1.country_name,region_name,t1.remark,(SELECT username FROM users WHERE id=created_id)as t1.created_id,t1.created_date,t1.last_updated";
		//$order 		 = array('field'=>'t1.publish_date','order'=>'DESC');
		$uri_segment = 3;
		$page        = $this->uri->segment($uri_segment);
		$per_page    = $limit;
		$offset      = $this->crud->set_offset($page,$per_page);
	
		$count_rows  = $this->crud->search_browse_join('',$table,"COUNT(t1.id) AS COUNT",$joins,$where,$dateranges,$wherearray);
	
		// 		$joins[0][0] = 'announcement_level_approval t2';
		// 		$joins[0][1] = 't2.id_announcement = t1.id';
		// 		$joins[0][2] = 'left';
		// 		$joins[1][0] = 'level t3';
		// 		$joins[1][1] = 't2.id_level = t3.id';
		// 		$joins[1][2] = 'left';
	
	
		$getData 	 = $this->crud->search_browse_join_with_paging("",$table,$select,$joins,'t1.status = "1"',$dateranges,$wherearray,"",$per_page,$offset,"t1.id");
	
		$total_rows = count($count_rows)>0?$count_rows[0]->COUNT:0;
		$set_config = array('base_url'=> base_url().'offices/search/','total_rows'=>$total_rows,'per_page'=>$per_page,'uri_segment'=>$uri_segment);
		$config     = $this->crud->set_config($set_config);
	
		/** setup for pagination **/
	
		$this->load->library('pagination');
		$this->pagination->initialize($config);
	
		$paging = $this->pagination->create_links();
	
		/** ===== **/
		/* ===== */
	
		/* setup variable to used in another functions */
	
		$data['getData'] 	= $getData;
		$data['pagination'] = $paging;
		$data['num']        = $offset;
	
		/* ===== */
		return $data;
	
	}
	
	function get_office_tags(){ 	
	 	$term   = $_GET['term'];  	
	 	$order  = array('field'=>'office_name','sort'=>'ASC'); $select = "office_name as name";
	 	$result = $this->crud->autocomplete("","offices",$select,"office_name",$term,"",$order);  	
	 	echo json_encode($result);   	 		
	}
	
	function set_sess_search(){
		$like = $_POST['search']; 	
	 	$this->session->set_flashdata('like',$like); 
	}
    
    function set_sess_office(){
	
		$type = $_POST['office_name'];
		if($type!='') 	{ $this->session->set_userdata('sess_cat',$type);
		} else { $this->session->unset_userdata('sess_cat'); }
	
		$type = $_POST['field'];
		if($type!='') 	{ $this->session->set_userdata('sess_field',$type);
		} else { $this->session->unset_userdata('sess_field'); }
	
	}
	
	function get_region(){
		$id_country = $_POST['id_country'];
		$select  = " id,region_name";
		$region  = $this->crud->browse("","region","id_country",$id_country,"true",$select);
		echo "<option>-- Select Office --</option>";
		foreach ($region as $row){
			echo "<option value='$row->id'>$row->region_name</option>";
		}
	}

	
	function is_exist(){
		$code     = $_POST['office_code'];
		$office	  = $_POST['office_name'];
		$region	  = $_POST['id_region'];
		$country  = $_POST['id_country'];
		$email	  = $_POST['email'];
		$phone	  = $_POST['phone'];
		$fax	  = $_POST['fax'];
		$id 	  = $this->input->post('id');
		//$remark	  = $_POST['remark'];
		$is_exist['1']= !$code ? "false" : "true";
		$is_exist['2']= !$office ? "false" : "true";
		$is_exist['3']= (!$country || $country==0) ? "false" : "true";
		$is_exist['4']= (!$region || $region==0) ? "false" : "true";
		$is_exist['5']= !$email ? "false" : "true";
		$is_exist['6']= !$phone ? "false" : "true";
		$is_exist['7']= !$fax ? "false" : "true";
		$msg['1']	  = !$code ? "office code required" : "";
		$msg['2']	  = !$office ? "office name required" : "";
		$msg['3']	  = (!$country || $country==0) ? "country name required" : "";
		$msg['4']	  = (!$region || $region==0) ? "region name required" : "";
		$msg['5']	  = !$email ? "email required" : "";
		$msg['6']	  = !$phone ? "phone required" : "";
		$msg['7']	  = !$fax ? "fax name required" : "";
	
		if ($is_exist['1']=='true' && $is_exist['2']=='true' && $is_exist['3']=='true' && $is_exist['4']=='true' && $is_exist['5']=='true' && $is_exist['6']=='true' && $is_exist['7']=='true'){
			// $where   = array('office_code'=>$code,'office_name'=>$office, 'id_region'=>$region, 'id_country'=>$country, 'email'=>$email, 'phone'=>$phone, 'fax'=>$fax, 'status'=>'1');
			// $checked = $this->crud->is_exist("","offices","id",$where);
			
			$this->db->where( array('office_code' => $code, 'status' => '1') );
			if(!empty($id))
			{
				$this->db->where('id !=', $id);
			}

			$checked = $this->db->get('offices')->result();
			

			$is_exist['1']    = empty($checked) ? "true" : "false";
			
			$msg['1']	= !empty($checked) ? "duplicate office code" : "";
			

	
		}

		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2'], 'msg3'=>$msg['3'], 'msg4'=>$msg['4'] , 'msg5'=>$msg['5'] , 'msg6'=>$msg['6'] , 'msg7'=>$msg['7']);
	
		echo json_encode($result);
	}
	
	function is_exist_old(){
		$code     = $_POST['office_code'];
		$office	  = $_POST['office_name'];
		$region	  = $_POST['id_region'];
		$country  = $_POST['id_country'];
		$email	  = $_POST['email'];
		$phone	  = $_POST['phone'];
		$fax	  = $_POST['fax'];
		//$remark	  = $_POST['remark'];
		$is_exist['1']= !$code ? "false" : "true";
		$is_exist['2']= !$office ? "false" : "true";
		$is_exist['3']= (!$country || $country==0) ? "false" : "true";
		$is_exist['4']= (!$region || $region==0) ? "false" : "true";
		$is_exist['5']= !$email ? "false" : "true";
		$is_exist['6']= !$phone ? "false" : "true";
		$is_exist['7']= !$fax ? "false" : "true";
		$msg['1']	  = !$code ? "office code required" : "";
		$msg['2']	  = !$office ? "office name required" : "";
		$msg['3']	  = (!$country || $country==0) ? "country name required" : "";
		$msg['4']	  = (!$region || $region==0) ? "region name required" : "";
		$msg['5']	  = !$email ? "email required" : "";
		$msg['6']	  = !$phone ? "phone required" : "";
		$msg['7']	  = !$fax ? "fax name required" : "";
	
		if ($is_exist['1']=='true' && $is_exist['2']=='true' && $is_exist['3']=='true' && $is_exist['4']=='true' && $is_exist['5']=='true' && $is_exist['6']=='true' && $is_exist['7']=='true'){
			$where   = array('office_code'=>$code,'office_name'=>$office, 'id_region'=>$region, 'id_country'=>$country, 'email'=>$email, 'phone'=>$phone, 'fax'=>$fax, 'status'=>'1');
			$checked = $this->crud->is_exist("","offices","id",$where);
			$is_exist['1']    = !$checked ? "true" : "false";
			
			$msg['1']	= $checked ? "duplicate office code" : "";
			

	
		}
		$status = in_array('false', $is_exist) ? "false" : "true";
		$result = array('status'=>$status,'msg1' =>$msg['1'], 'msg2' =>$msg['2'], 'msg3'=>$msg['3'], 'msg4'=>$msg['4'] , 'msg5'=>$msg['5'] , 'msg6'=>$msg['6'] , 'msg7'=>$msg['7']);
	
		echo json_encode($result);
	}

	
	function form_create(){
		
		$country = $this->crud->browse("","country","","","true","id,country_name"); 
	 	if(!$country) $country = array(); 	
	 	$select = array(''=>'-- Select --');
	 	foreach($country as $val){ $options[$val->id] = $val->country_name; } 
	 	$data['country'] = $select + $options; 
	 		
		$this->load->view('offices/form_create',$data);		
		
	}
	
	function create(){
		$office_code= $this->m_category->category_code("offices");
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();		
		$data 		= array(
							'id_company'=>'1',
							'id_country'=>$_POST['id_country'],
							'id_region'=>$_POST['id_region'],
							'office_code'=>$_POST['office_code'],
							'office_name'=>$_POST['office_name'],
							'status'=>'1',
							'email'=>$_POST['email'],
							'phone'=>$_POST['phone'],
							'fax'=>$_POST['fax'],
					 		'created_id'=>$userID,
							'created_date'=>$curr_date,
							'last_updated'=>$curr_date);
		$where   = array('office_code'=>$_POST['office_code'],'status'=>'0');
		$checked = $this->crud->is_exist("","offices","id",$where);
	
		if ($checked){ 
			$id = $this->crud->browse("","offices","office_code",$_POST['office_code'],"false","id")->id;
			// $update = array('status'=>'1','last_updated'=>$curr_date);
			$this->crud->update("","offices","id",$id,$data);
		} else {	$id = $this->crud->insert("","offices",$data);	}
		print $this->db->last_query();
		exit();
		$this->session->set_flashdata('message','1 data success insert');		
	}
	
	function form_update(){
		$id = $this->uri->segment(3);
		$select 			 = "id,office_code,office_name,
									(SELECT id FROM region r WHERE r.id=l.id_region) as id_region,  
									(SELECT id_country FROM region r WHERE r.id=l.id_region) as id_country, 
								    (SELECT country_name FROM country s WHERE s.id=id_country) as country,
								    created_id,created_date,last_updated,email,fax,phone, ";
		$def = $this->crud->browse("","offices l","id",$id,"false",$select);
		$data['def'] = $def ; 
        
		$country = $this->crud->browse("","country","","","true","id,country_name"); 
	 	if(!$country) $country = array(); 	
	 	$select_country = array(''=>'-- Select --');
	 	foreach($country as $val){ $option_country[$val->id] = $val->country_name; } 
	 	$data['country'] = $select_country + $option_country; 
	 	 
        $region = $this->crud->browse("","region","","","true","id,region_name"); 
        if(!$country) $country = array();
        $select_region = array(''=>'-- Select --');
        foreach($region as $val){ $option_region[$val->id] = $val->region_name; } 
		$data['region'] = $select_region + $option_region; 	
        
 		$this->load->view('offices/form_update',$data);
 		
	}
	
	function update(){
		$curr_date 	= date('Y-m-d H:i:s'); $userID = $this->tank_auth->get_user_id();		
		$data 		= array('id_country'	=>$_POST['id_country'],
							'id_region'		=> $_POST['id_region'],
							'office_code'	=> $_POST['office_code'],
							'office_name'	=> $_POST['office_name'],
							'email'			=> $_POST['email'],
							'phone'			=> $_POST['phone'],
							'fax'			=> $_POST['fax'],
							'status'		=> '1',
							'created_id'	=> $userID,
							'last_updated'	=> $curr_date);
					 		
		$this->crud->update("","offices","id",$_POST['id'],$data);
		$this->session->set_flashdata('message','1 data success update');
	}
	
	function delete(){
	
		$id = $this->uri->segment(3);
		$status = array('status'=>'0');
		$this->crud->update("","offices","id",$id,$status);
		$this->session->set_flashdata('message','1 data success deleted');
		redirect('offices/','refresh');
	}	
	
	function delete_(){
	
		$id = $this->uri->segment(3);
		
		$where    = array('id_office'=>$id);
		$checked1 = $this->crud->is_exist("","announce","id",$where);
		$checked2 = $this->crud->is_exist("","profiles","id",$where);
		$checked3 = $this->crud->is_exist("","member","id",$where);
		
		if ($checked1 || $checked2 || $checked3) {
			$this->session->set_flashdata('msg_warning','This data is already in use');
		}
		else {
			$this->crud->delete("","offices","id",$id);
			$this->session->set_flashdata('message','1 data success deleted');
		}
		
		//$data = array('status'=>'0');
		//$this->crud->update("","offices","id",$id,$data);
		redirect('offices/','refresh');
	}
	
}
?>