<?php

require_once APPPATH.'./libraries/PHPExcel.php';
$objPHPExcel = new PHPExcel();
// Set properties
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Vendor List Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Vendor List Document");
$objPHPExcel->getProperties()->setDescription("Vendor List document for Office 2007 XLSX, generated using PHP classes.");

// Set fonts
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getFont()->setBold(true);
// Set column widths
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(27);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(7);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);

// ADD Data
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Vendor List');
$objPHPExcel->getActiveSheet()->SetCellValue('A4', 'No');
$objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Registration Num.');
$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Vendor Num.');
$objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Vendor Name');
$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Category');
$objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Subcategory');
$objPHPExcel->getActiveSheet()->SetCellValue('G4', 'Type');
$objPHPExcel->getActiveSheet()->SetCellValue('H4', 'Vendor Status');

$no = 1;
$number = 5;

foreach($vendorlist as $row){
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$number, $no);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$number, $row->register_num);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$number, $row->vendor_num);
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$number, $row->vendor_name);
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$number, $row->category);
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$number, $row->subcategory);
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$number, $row->vendor_type);
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$number, $row->status);

	$number++;
	$no++;

}

//set border
$limit = count($vendorlist);
$limit += 4;
$objPHPExcel->getActiveSheet()->getStyle('A4:H'.$limit)->getBorders()->applyFromArray(
	array(
		'allborders'	=> array(
			'style' 	=> PHPExcel_Style_Border::BORDER_THIN,
			'color'	 	=> array(
				'rgb'	=> '000000')
			)
		)
	);

$objPHPExcel->setActiveSheetIndex(0);
$filename='Vendor_list_'.date('YmdHis').'.xlsx';

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//send to browser
$objWriter->save('php://output');
//save to local folder
// $objWriter->save(str_replace('excel.php', $filename, __FILE__));

exit;

?>
