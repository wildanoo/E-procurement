<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<title>E-Procurement Garuda</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/skins/red-blue/blue.css" id="colors" />
<!-- 	<link rel="stylesheet" type="text/css" href="css/component.css" /> -->
	<link rel="icon" href="favicon-grd.png" sizes="16x16">
</head>
<body>
<div id="wrapper">
	<header id="header" class="container-fluid">
	  <div class="header-bg">
		<div class="container">	  
			<div class="row">
				<div id="logo" class="col-md-5 col-md-offset-1"><a href="login" class="logo" alt="E-Procurement Garuda Indonesia"></a></div>
				<div class="col-md-4 col-md-offset-1">
					<ul class="social-icons">
					<?php if ($this->session->userdata('lang') == 'eng') { ?>
						<li class="lang eng" data="lang-eng" title="English"><a style="top:0 !important;" href="#">English</a></li>
						<li class="lang idn" data="lang-ind" title="Indonesian"><a class="inactive" href="#" id="lang_ind">Indonesian</a></li>
					<?php }elseif ($this->session->userdata('lang') == 'ind') { ?>
						<li class="lang eng" data="lang-eng" title="English"><a class="inactive" href="#" id="lang_eng">English</a></li>
						<li class="lang idn" data="lang-ind" title="Indonesian"><a style="top:0 !important;" href="#">Indonesian</a></li>
					<?php }?>
					</ul>	
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="select-menu hidden-lg">
						<select id="selectMenu">
							<option selected value="<?php echo base_url(); ?>">Home</option>			
							<option value="<?php echo base_url(); ?>webcontent/archive/news">News</option>
							<option value="#">FAQs</option>
							<option value="#">Supplier Registration</option>
							<option value="login.html">Log In</option>
							<option value="<?php echo base_url(); ?>main/contact_us">Contact us</option>
						</select>
					</div>
					<ul id="menu" class="visible-lg" style="font-size: 13px;">
						<li>
							<a href="<?php echo base_url(); ?>">HOME</a>
						</li>
						<li><?php  echo anchor('webcontent/archive/news','News'); ?></li>
						<li><a href="#">FAQs</a></li>
						<li id="st-trigger-effects2">
							<a data-effect="st-effect-2">SUPPLIER REGISTRATION</a>
						</li>
						<li id="st-trigger-effects1">
							<a data-effect="st-effect-1">LOG IN</a>
						</li>
						<li><?php  echo anchor('main/contact_us','CONTACT US'); ?></li>
						<!-- Search Form -->
						<li class="search-form visible-desktop" style="float: right;top: 10px;"\>
							<form method="get" action="#">
								<input type="text" value="Search" class="search-text-box"/>
								<input type="submit" value="" class="search-text-submit"/>
							</form>
						</li>
					</ul>
				</div>
			</div>
		</div>
	  </div>
	</header>
	<section id="content" class="container-fluid">
	<div class="container" style="width: 940px;margin: 0 auto;padding:0;">
		<div class="row">
			<div class="col-md-8" style="position: relative;">
			<div class="headline"><h4>Content of <?php echo $title?></h4></div>
			<hr>
			<table id="browse" class="table table-striped table-bordered" style="font-size: 13px">
				<thead>
			        <tr>
			            <th>No</th>
			            <th>Title</th>
			            <th>Start Date</th>
			            <th>End Date</th>
			            <th>Category - Subcategory</th>
			            <th>Type of Invitation</th>
			            <th>Status</th>
			        </tr>
				</thead>
			    <tbody>
			<?php if (count($browse) > 0) {
				foreach($browse as $row){ ?>
					<tr>
						<td><?php echo $row->code; ?></td>
						<td><a href="<?php echo base_url();?>webcontent/page/<?php echo $row->id;?>"> <?php echo $row->title ? $row->title:'<strong>-</strong>'; ?> </a></td>
						<td><?php echo str_replace("-","/",$row->first_valdate);?></td>
						<td><?php echo str_replace("-","/",$row->end_valdate);?></td>
						<td><?php echo $category[$row->id_cat].' - '.$subcategory[$row->id_subcat]; ?></td>
						<td><?php echo $subcontent[$row->subcontent_type]; ?></td>
						<td></td>
					</tr>
			<?php } ?>	
			<tr><td colspan="9" style="text-align: center;"><?php echo $pagination;?></td></tr>
			<?php } else { ?><tr><td colspan="9">No Event(s) yet.</td></tr><?php } ?>
			    </tbody>
			</table>
			</div>
			<?php $this->load->view('content_public/recent_post',$recent_post); ?>
		</div>
	</div>
	</section>
</div>
<footer class="main-footer footer-line container-fluid text-center">
	Copyright &copy; 2016 <strong><a href="http://www.garuda-indonesia.com/" style = "text-decoration: none;">Garuda Indonesia</a>.</strong> All rights reserved. Powered by &nbsp; <a href="http://asyst.co.id/"><img src = "<?php echo base_url(); ?>assets/images/gallery/logo-asyst.png" style = "vertical-align: middle;"/></a>
</footer>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.color.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/SmoothScroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>
</html>