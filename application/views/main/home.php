<!-- Section 1 - begin BANNER/ABOUT US -->
<section class="container2 full-width-container ihome-banner">
<?php $this->load->view('main/banner');?>
</section>
<!-- Section 1 - end BANNER/ABOUT US -->

<!-- Section 2 - begin NEWS & ANNOUNCEMENT -->
<section class="about-content-wrap pull-left" id="#">
<?php $this->load->view('main/news_announcement');?>
</section>
<!-- Section 2 - end NEWS & ANNOUNCEMENT -->

<!-- Section 3 - begin FAQ -->
<section class="about-content-wrap pull-left" id="#">
<?php $this->load->view('main/faq');?>
</section>
<!-- Section 3 - end FAQ -->

<!-- Section 4 - begin CONTACT US -->
<section class="complete-content content-footer-space">
<?php $this->load->view('main/contact_us');?>
</section>
<!-- Section 4 - end CONTACT US -->

<!-- Section 5 - begin LOGIN & REGISTRATION -->
<section class="complete-content content-footer-space">
<?php $this->load->view('main/login_registration');?>
</section>
<!-- Section 5 - end LOGIN & REGISTRATION -->