<html>
<head>
<title>Browse</title>
<style type="text/css"></style>
</head>
<script type="text/javascript"> 	
$(document).ready(function(){
	 var baseUrl   = "<?php echo base_url(); ?>";		 
	 $(function(){  });
	 
	 $("#srch-term").autocomplete({
			source: baseUrl + 'category/get_cat_tags',
			minLength:1
	   });
	   
	   $("#btn_search").click(function(){
            var search = $("#srch-term").val();  
            var csrf   = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token  = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajax({
               url  : baseUrl + 'category/set_sess_search',
               type : "POST",              		               
               data : csrf +'='+ token +'&search='+search,
               success: function(resp){               		            
				   document.location.href= baseUrl + 'category/supplier_category';
               }
            });
        });  
	   
	 
	 $('#btn_create').click(function(){ 
	 	  form_create();	 	  
	 });	
});

	function form_create(){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>';
			BootstrapDialog.show({
			title: 'Create Group Category',
            message: $('<div></div>').load(baseUrl + "category/form_group_category"),           
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){
		                	var category     = $('#category').val();	
		                	var subcategory  = $('#subcategory').val();	
		                	 $.post( baseUrl + 'category/is_exist_group', { csrf: token , category : category, subcategory : subcategory })
							  .done(function( data ) {
							    	if(data=="true"){
										$.ajax({
							               url  : baseUrl + 'category/create_suppcat',
							               type : "POST",              		               
							               data : csrf +'='+ token +'&category='+category+'&subcategory='+subcategory,
							               success: function(resp){
							               		document.location.href= baseUrl + 'category/supplier_category';				               		  
									       }
							            });           
									} else { alert('Data has been exist !!');	}
							 });		                	
		                	
		                    
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'category/supplier_category';
				                    dialogRef.close(); 
				                }
			        }]
        });
	}
	
	function form_update(id){
			var baseUrl = "<?php echo base_url(); ?>";
			var csrf    = '<?php echo $this->security->get_csrf_token_name(); ?>'; 
            var token   = '<?php echo $this->security->get_csrf_hash(); ?>'; 
         
			BootstrapDialog.show({
			title: 'Update Group Category',
            message: $('<div></div>').load(baseUrl + "category/form_update_group/" + id),  
            buttons: [{			            	
		                label: 'Submit',
		                cssClass: 'btn btn-primary btn-sm',		               
		                action: function(dialogRef){
		                	var id     = $('#id').val();
		                	var category     = $('#category').val();	
		                	var subcategory  = $('#subcategory').val();	                		                    
		                    $.ajax({
				               url  : baseUrl + 'category/update_suppcat',
				               type : "POST",              		               
				               data : csrf +'='+ token +'&id='+id+'&category='+category+'&subcategory='+subcategory,
				               success: function(resp){
				               		document.location.href= baseUrl + 'category/supplier_category';	
				               		//alert(resp);			               		  
						    	}
				            });            
		                }
		            }, {
		            	label: 'Close',cssClass: 'btn btn-primary btn-sm',
				                action: function(dialogRef) {
				                	document.location.href= baseUrl + 'category/supplier_category';
				                    dialogRef.close(); 
				                }
			        }]
        	});
	}


</script>

<body>
<div id="debug"></div>
<div class="page-header"> <h3>Vendor Category</h3></div>
<input type="button" id="btn_create" class="btn btn-primary btn-sm" value="Create" style="width:80px;"/>

<div class="col-sm-3 col-md-3 pull-right">
        <!--form class="navbar-form" role="search"-->
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" id="btn_search"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        <!--/form-->
        </div>

<?php $msg = $this->session->flashdata('message');
	if(strpos($msg,'failed')) $color="#bc0505";
	else $color = "#487952"; ?>	
<font color='<?php echo $color; ?>'><i><? echo $msg; ?></i></font>
	
<table border="1" id="browse" class="table table-striped table-bordered" cellspacing="0" width="80%" style="font-size: 13px">
	<thead>
		<tr>
            <th style="width:4%;text-align:center;">No</th>
            <th>Category</th>
            <th>Sub Category</th>
            <th>Status</th>
            <th>Created Date</th>
            <th style="text-align: center;">Action</th>
        </tr>
     </thead>
     <tbody>
 <?php 
 if($browse){ $i=1;
 	foreach($browse as $row){ ?>
 		<tr>
 			<td style="text-align:center;"><?php echo $num = $num+1; ?></td>
			<td><?php echo $row->category; ?></td>
			<td><?php echo $row->subcategory; ?></td>
			<td align="center"><?php   if($row->status==1) echo "Active";  else echo "Disabled"; ?></td>
 			<td align="center"><?php echo date('M d, Y',strtotime($row->created_date)); ?></td>		
				
				<td style="text-align: center;">
				<?php
					echo  '<i class="fa fa-pencil" onclick="form_update('.$row->id.')"/>&nbsp;&nbsp;';
					$trash  = '<i class="fa fa-trash"></i>';
					$js = "if(confirm('Are you sure to delete record ?')){ return true; } else {return false; }";
					echo anchor('category/delete_suppcat/'.$row->id, $trash,array('class'=>'default_link','title'=>'Delete','onclick'=>$js));			
				?>				
				</td>						
		</tr>
<?php	$i++;  } ?>	
<tr><td colspan="8" style="text-align: center;"><?php echo $pagination;?></td></tr>
<?php } else { ?><tr><td colspan="8">-</td></tr><?php } ?>
     </tbody>
</table>
</body>
</html>
